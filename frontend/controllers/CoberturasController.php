<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Coberturas;
use frontend\models\search\CoberturasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\Productos;

/**
 * CoberturasController implements the CRUD actions for Coberturas model.
 */
class CoberturasController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Coberturas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CoberturasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Coberturas model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Coberturas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Coberturas();
        $tipoProductos = Productos::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_cobertura]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'tipoProductos' => $tipoProductos
            ]);
        }
    }

    /**
     * Updates an existing Coberturas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $tipoProductos = Productos::find()->all();
        
        $model->productos = \yii\helpers\ArrayHelper::getColumn(
			$model->getInterProdCobs()->asArray()->all(),
			'id_producto'
		);
        
        if ($model->load(Yii::$app->request->post())) {
			if (!isset($_POST['Coberturas']['productos'])) {
				$model->productos = [];
			}
			if ($model->save()) {
				return $this->redirect(['view', 'id' => $model->id_cobertura]);
			}
		} else {
			return $this->render('update', [
				'model' => $model,
				'tipoProductos' => $tipoProductos
			]);
		}

        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_cobertura]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }*/
    }

    /**
     * Deletes an existing Coberturas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Coberturas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Coberturas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Coberturas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
