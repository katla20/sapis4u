<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Poliza;
use frontend\models\PolizaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\base\Model;
use yii\web\Response;
use yii\widgets\Pjax;
use frontend\models\Cliente;
use frontend\models\Intermediario;
use frontend\models\Persona;
use frontend\models\Coberturas;
use frontend\models\CoberturasView;
use frontend\models\Aseguradoras;
use frontend\models\InterAsegRamo;
use frontend\models\InterAsegProducto;
use frontend\models\Productos;
use frontend\models\Recibo;
use frontend\models\Certificado;
use frontend\models\CertificadoDetalle;
use frontend\models\InterProdCob;





/**
 * PolizaController implements the CRUD actions for Poliza model.
 */
class PolizaSaludController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Poliza models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PolizaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Poliza model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Poliza model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
public function actionCreate()
{
        $model = new Poliza();
        $recibo = new Recibo();
        $certificado= new Certificado();
        $certificadoDetalle= new CertificadoDetalle();

        $post=Yii::$app->request->post();

         // ajax validation
        /* if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
               Yii::$app->response->format = Response::FORMAT_JSON;
                 return ActiveForm::validate($model);
          }*/



        if ($model->load(Yii::$app->request->post())){//validar post

            $model->id_contratante=$model->id_contratante_hidden;
            $model->id_tipo=1;
            $model->fecha_registro=date('Y-m-d H:i:s');
            $model->id_user_registro=isset(Yii::$app->user->identity->id)?Yii::$app->user->identity->id:1;
			      $model->id_ramo=1;

          if($bandera=$model->save(false)){//validar que guarde la poliza

           //exit(var_dump($model));
            $recibo->nro_recibo=$model->nro_recibo;
            $recibo->id_poliza=$model->id_poliza;
            $recibo->tipo_recibo=$model->tipo_recibo;
            $recibo->fecha_registro=$model->fecha_registro;
            $recibo->id_user_registro=$model->id_user_registro;
            $recibo->fecha_vigenciadesde=$model->fecha_vigenciadesde;
            $recibo->fecha_vigenciahasta=$model->fecha_vigenciahasta;
            $recibo->id_vendedor=$post['Poliza']['id_vendedor'];
            $recibo->id_productor=2;//OJO LLENARLO CON EL ID DEL USUARIO DEL ASESOR
			      $recibo->comision_total=$post['Poliza']['comision_total'];
			      $recibo->comision_vendedor=$post['Poliza']['comision_vendedor'];

            if($bandera=$recibo->save(false)){//validar que guarde el recibo

                  //validar certificados
                  $certificado->id_recibo=$recibo->id_recibo;
                  $certificado->nro_certificado=$model->nro_certificado;
                  $certificado->id_automovil=$post['Poliza']['id_automovil_hidden'];
                  $certificado->fecha_registro=$model->fecha_registro;
                  $certificado->id_user_registro=$model->id_user_registro;
                  $certificado->id_producto=$model->producto;
                  $certificado->id_titular=10;
				          $certificado->deducible=$post['Poliza']['deducible'];


                  if($bandera=$certificado->save(false)){

          					  $cobBox=Yii::$app->request->post('cobBox');
          					  $suma=Yii::$app->request->post('suma');
          					  $tasa=Yii::$app->request->post('tasa');
          					  $prima=Yii::$app->request->post('prima');

            					  foreach ($cobBox as $clave => $valor){

            						  $certificadoDetalle= new CertificadoDetalle();

            							$certificadoDetalle->id_certificado=$certificado->id_certificado;
            							$certificadoDetalle->suma_asegurada=$suma[$clave];
            							$certificadoDetalle->prima=$prima[$clave];
            							$certificadoDetalle->comision=0;
            							$certificadoDetalle->fecha_registro=$model->fecha_registro;
            							$certificadoDetalle->id_user_registro=$model->id_user_registro;
            							$certificadoDetalle->id_cobertura=$clave;
            							//  echo 'tasa'.$tasa[$clave];
            							if(!$certificadoDetalle->save(false)){
            								var_dump($recibo->getErrors());
            							}
            					  }

          					  if($bandera){
          					       return $this->redirect(['view', 'id' => $model->id_poliza]);
          					  }else{
          						       var_dump($recibo->getErrors());
          					  }

						  /*["poliza-monto_comision-disp"]=> string(4) "0,00"
							 ["id_vendedor"]=>
							 ["tipo_vendedor"]=> */
					    //  var_dump(Yii::$app->request->post());

                  }else{
                    var_dump($recibo->getErrors());
                  }

            }else{

              var_dump($recibo->getErrors());

            }
          }

        } else {
             // var_dump($recibo->getErrors());
            return $this->render('create', [
                'model' => $model,
            ]);
        }
}

      public function actionBuscarCoberturas()
      {
        Yii::$app->response->format = Response::FORMAT_JSON;
         return ArrayHelper::map(CoberturasView::find()
                                      ->where("cobertura_estatus = :status AND producto_estatus =:status AND aseguradora_estatus =:status
                                                AND id_aseguradora = :aseguradora AND id_producto = :producto ",
                                                [':status' => 1,
                                                 ':aseguradora' => Yii::$app->request->post('aseguradora'),
                                                 ':producto' => Yii::$app->request->post('producto')])
                                      ->all(),'id_cobertura', 'nombre_cobertura');


      }

     public function actionBuscarCliente()
        {
         Yii::$app->response->format = Response::FORMAT_JSON;
                            return Cliente::find()
                                       ->joinwith('idPersona')
                                        ->where("cliente.estatus = :status AND Cliente.id_cliente = :id_cliente",
                                        [':status' => 1,':id_cliente' => Yii::$app->request->post('id')])
                                        ->select("(nombre||' '||segundo_nombre||' '||apellido|| ' ' ||segundo_apellido || ' ' || identificacion) AS nombre_completo ,id_cliente")
                                        ->all();
        }


  public function actionBuscarAsegurado(){
        /*SELECT CONCAT(nombre, ' ', apellido) AS nombre_completo, identificacion FROM cliente
       LEFT JOIN persona ON cliente.id_persona= persona.id_persona WHERE cliente.estatus
       = 1 AND cliente.id_cliente = '2'*/

      Yii::$app->response->format = Response::FORMAT_JSON;
        return Cliente::find()->joinWith(['idPersona'])
       ->select(["*","CONCAT(nombre, ' ', apellido) AS nombre_completo"])
       ->where("cliente.estatus = :status AND cliente.id_cliente = :id_cliente",
       [':status' => 1,':id_cliente' => Yii::$app->request->get('id')])->asArray()->all();
       //->groupBy('seller_id')
       //->orderBy(['cnt' => 'DESC'])

  }

      public function actionBuscarVendedor()
      {
            Yii::$app->response->format = Response::FORMAT_JSON;
                    return Intermediario::find()
                                 ->joinwith('idPersona')
                                 ->where("intermediario.estatus = :status AND intermediario.id_intermediario = :intermediario AND intermediario.id_tipo_intermediario=2",
                                 [':status' => 1,':intermediario' => Yii::$app->request->post('id')])
                                 //->select("(nombre||' '||segundo_nombre||' '||apellido|| ' ' ||segundo_apellido) AS nombre_completo,id_intermediario,codigo,comision")
                                 ->select(['*'])
                                 ->all();
     }

    /**
     * Updates an existing Poliza model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_poliza]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Poliza model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Poliza model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Poliza the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Poliza::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
