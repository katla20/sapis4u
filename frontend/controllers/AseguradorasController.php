<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Aseguradoras;
use frontend\models\search\AseguradorasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\Ramos;

/**
 * AseguradorasController implements the CRUD actions for Aseguradoras model.
 */
class AseguradorasController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Aseguradoras models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AseguradorasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Aseguradoras model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Aseguradoras model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Aseguradoras();
        $tipoRamos = Ramos::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_aseguradora]);
        } else {
            return $this->render('create', [
                'model' => $model,
                
                'tipoRamos' => $tipoRamos
            ]);
        }
    }

    /**
     * Updates an existing Aseguradoras model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $tipoRamos = Ramos::find()->all();
        
        $model->ramos = \yii\helpers\ArrayHelper::getColumn(
			$model->getInterAsegRamos()->asArray()->all(),
			'id_ramo'
		);
        
        if ($model->load(Yii::$app->request->post())) {
			if (!isset($_POST['Aseguradoras']['ramos'])) {
				$model->ramos = [];
			}
			if ($model->save()) {
				return $this->redirect(['view', 'id' => $model->id_aseguradora]);
			}
		} else {
			return $this->render('update', [
				'model' => $model,
				'tipoRamos' => $tipoRamos
			]);
		}

        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_aseguradora]);
        } else {
            return $this->render('update', [
                'model' => $model,
                
                'tipoRamos' => $tipoRamos
            ]);
        }*/
    }

    /**
     * Deletes an existing Aseguradoras model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Aseguradoras model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Aseguradoras the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Aseguradoras::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
