<?php


namespace frontend\controllers;

use yii\web\Controller;
use common\models\HtmlHelpers;
use frontend\models\Pais;
use frontend\models\Estado;
use frontend\models\Ciudad;
use frontend\models\Zona;
use frontend\models\Productos;
use frontend\models\Aseguradoras;
use frontend\models\InterAsegRamo;
use frontend\models\InterAsegProducto;
use yii\helpers\Json; //keyla bullon

class DependentDropdownController extends Controller
{
	public $id_modelo;
	public function actionIndex()
	{
		$this->render('index');
	}


	public function actionChildAccount($db,$cmpo_dep,$cmpo_mostrar,$id_cmpo) {
	     $out = [];


	    if (isset($_POST['depdrop_parents'])) {

	         $id = end($_POST['depdrop_parents']);

			 //echo $id;

			     if(!empty($db) && !empty($cmpo_dep) && !empty($cmpo_mostrar) && !empty($id_cmpo)){


					    $modeldb = new $db();//creando el objeto de base de datos

						 $list = $modeldb::find()->andWhere([$cmpo_dep=>(string)$id])->asArray()->all();




				        $selected  = null;
				        if ($id != null && count($list) > 0) {
				            $selected = '0';

				            foreach ($list as $i => $valores) {
											 //aqui se lista un arreglo con el nombre del campo clave y el y los valores que se mostraran en el select
				                //$out[] = [$cmpo_mostrar.'_id' => (string)$valores[$cmpo_mostrar.'_id'], $cmpo_mostrar => $valores[$cmpo_mostrar]];
												  $out[] = ['id' => (string)$valores[$id_cmpo], 'name' => (string)$valores[$cmpo_mostrar]];
				                if ($i == 0) {
				                    //$selected = $valores[$cmpo_mostrar.'_id'];
			                           $selected = (string)$valores[$id_cmpo];
									}
				            }//foreach ($list as $i => $valores)

										//var_dump($out);exit();
				            // Shows how you can preselect a value
				            return Json::encode(['output' => $out, 'selected'=>$selected]);
				        }//  if ($id != null && count($list) > 0)

					 }// fin  if(!empty($modelo) && !empty($cmpo_dep) && !empty($cmpo_mostrar) )

	    }
	    return Json::encode(['output' => '', 'selected'=>'']);
	}//fin function


    public function actionAnio($db)
    {

		if (isset($_POST['depdrop_parents'])) {

	          $id = end($_POST['depdrop_parents']);


				$connection = \Yii::$app->db;
				$sql = "SELECT DISTINCT ON (anio) anio, id_modelo2 FROM version
				WHERE id_modelo LIKE '$id%' ORDER BY anio, id_modelo";

				$command = $connection->createCommand($sql);
				//$command->bindValue(":id", $id);
				$result = $command->queryAll();

				$selected  = null;
				        if ($id != null && count($result) > 0) {
				            $selected = '0';

				            foreach ($result as $i => $valores) {
											 //aqui se lista un arreglo con el nombre del campo clave y el y los valores que se mostraran en el select
				                //$out[] = [$cmpo_mostrar.'_id' => (string)$valores[$cmpo_mostrar.'_id'], $cmpo_mostrar => $valores[$cmpo_mostrar]];
												  $out[] = ['id' => (string)$valores['anio'].'-'.(string)$valores['id_modelo2'], 'name' => (string)$valores['anio']];
				                if ($i == 0) {
				                    //$selected = $valores[$cmpo_mostrar.'_id'];
			                           $selected = (string)$valores['anio'].'-'.(string)$valores['id_modelo2'];
									}
				            }//foreach ($list as $i => $valores)

										//var_dump($out);exit();
				            // Shows how you can preselect a value
				            return Json::encode(['output' => $out, 'selected'=>$selected]);
				        }//  if ($id != null && count($list) > 0)

		}
    }

	public function actionVersion($db)
    {

		if (isset($_POST['depdrop_parents'])) {

	        $id = end($_POST['depdrop_parents']);

			$datos=explode("-",$id);




			if(!empty($db)){

				$connection = \Yii::$app->db;
				$sql = "SELECT nombre,id_version FROM version
			WHERE  id_modelo LIKE '$datos[1]%' and anio='$datos[0]' ORDER BY anio, id_modelo";

				$command = $connection->createCommand($sql);
				//$command->bindValue(":id", $id);
				$result = $command->queryAll();

				$selected  = null;
				        if ($id != null && count($result) > 0) {
				            $selected = '0';

				            foreach ($result as $i => $valores) {
											 //aqui se lista un arreglo con el nombre del campo clave y el y los valores que se mostraran en el select
				                //$out[] = [$cmpo_mostrar.'_id' => (string)$valores[$cmpo_mostrar.'_id'], $cmpo_mostrar => $valores[$cmpo_mostrar]];
												  $out[] = ['id' => (string)$valores['id_version'], 'name' => (string)$valores['nombre']];
				                if ($i == 0) {
				                    //$selected = $valores[$cmpo_mostrar.'_id'];
			                           $selected = (string)$valores['id_version'];
									}
				            }//foreach ($list as $i => $valores)

										//var_dump($out);exit();
				            // Shows how you can preselect a value
				            return Json::encode(['output' => $out, 'selected'=>$selected]);
				        }//  if ($id != null && count($list) > 0)


			}

		}
    }




		public function actionChildAccountJoin($db,$join,$cmpo_dep,$cmpo_mostrar,$id_cmpo,$cmpo_adic,$valor_adic) {
		     $out = [];


		    if (isset($_POST['depdrop_parents'])) {

		         $id = end($_POST['depdrop_parents']);

				 //echo $id;

				     if(!empty($db) && !empty($cmpo_dep) && !empty($cmpo_mostrar) && !empty($id_cmpo) && !empty($join)){
                $where_adic="";
               if(isset($cmpo_adic) && !empty($cmpo_adic) ){
                 $where_adic=" AND $cmpo_adic=$valor_adic";
							 }

						    $modeldb = new $db();//creando el objeto de base de datos
                   /*extrayend de las variables pasadas por la funcion el nombre de los campos que ensambla el json */
										$id_=explode('.',$id_cmpo);
										$id_=$id_[1];
										$mostrar_=explode('.',$cmpo_mostrar);
										$mostar_=$mostrar_[1];

									  $estatus="{$modeldb->tableName()}.estatus";

								    $list = Productos::find()
								           ->joinwith($join)
								           ->where("$estatus = :status $where_adic",[':status' => 1])
								           ->andWhere([$cmpo_dep=>(string)$id])->asArray()
								           ->select("$cmpo_mostrar,$id_cmpo,$cmpo_dep")
								           ->all();

					        $selected  = null;
					        if ($id != null && count($list) > 0) {
					            $selected = '0';

					            foreach ($list as $i => $valores) {
												 //aqui se lista un arreglo con el nombre del campo clave y el y los valores que se mostraran en el select
					                //$out[] = [$cmpo_mostrar.'_id' => (string)$valores[$cmpo_mostrar.'_id'], $cmpo_mostrar => $valores[$cmpo_mostrar]];
													  $out[] = ['id' => (string)$valores[$id_], 'name' => (string)$valores[$mostar_]];
					                if ($i == 0) {
					                    //$selected = $valores[$cmpo_mostrar.'_id'];
				                           $selected = (string)$valores[$id_];
										}
					            }//foreach ($list as $i => $valores)

											//var_dump($out);exit();
					            // Shows how you can preselect a value
					            return Json::encode(['output' => $out, 'selected'=>$selected]);
					        }//  if ($id != null && count($list) > 0)

						 }// fin  if(!empty($modelo) && !empty($cmpo_dep) && !empty($cmpo_mostrar) )

		    }
		    return Json::encode(['output' => '', 'selected'=>'']);
		}//fin function


}
