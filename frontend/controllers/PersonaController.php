<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Cliente;
use frontend\models\Persona;
use frontend\models\Direccion;
use frontend\models\CargaFamiliar;
use frontend\models\search\PersonaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Model;
use yii\web\Response;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\ActiveField;
use yii\helpers\ArrayHelper;





/**
 * PersonaController implements the CRUD actions for persona model.
 */
class PersonaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all persona models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PersonaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single persona model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new persona model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($submit = false)
    {


        $model = new persona();
        $modelDireccion = [new Direccion];
        $modelCliente = [new Cliente];
        $modelCargaFamiliar= [new CargaFamiliar];

        $modelMult = new persona();
        $modelClienteMult = [new Cliente];


        $modelDireccion = Model::createMultiple(Direccion::classname());
        Model::loadMultiple($modelDireccion, Yii::$app->request->post());

        $modelCargaFamiliar = Model::createMultiple(CargaFamiliar::classname());
        Model::loadMultiple($modelCargaFamiliar, Yii::$app->request->post());


         // ajax validation
         if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
             Yii::$app->response->format = Response::FORMAT_JSON;
             return ArrayHelper::merge(
                 ActiveForm::validateMultiple($modelDireccion),
                 ActiveForm::validateMultiple($modelCargaFamiliar),
                 ActiveForm::validate($model)
             );
         }


        if ($model->load(Yii::$app->request->post())) {
          /*aqui iba ajax validacion y */
          /*$modelDireccion = Model::createMultiple(Direccion::classname());
          Model::loadMultiple($modelDireccion, Yii::$app->request->post());

          $modelCargaFamiliar = Model::createMultiple(CargaFamiliar::classname());
          Model::loadMultiple($modelCargaFamiliar, Yii::$app->request->post());*/

           // validate all models
           $valid = $model->validate();
           $valid = Model::validateMultiple($modelDireccion) && $valid;
           //$valid = Model::validateMultiple($modelCargaFamiliar) && $valid2 ;

           if ($valid) {
               $transaction = \Yii::$app->db->beginTransaction();
               try {
                      //REGISTRANDO AL TITULAR POR EL MODELO PERSONA
                   if ($flag = $model->save(false)) {
                               //SETEAR LOS VALORES EN EL MODELO DE CLIENTE ANTES DE GUARDAR
                               foreach ($modelCliente as $modelCliente) {
                                       $modelCliente->id_persona = $model->id_persona;
                                       $modelCliente->fecha_registro=date('Y-m-d H:i:s');
                                       //para que funcione debe estar logueado
                                       $modelCliente->id_user_registro =isset(Yii::$app->user->identity->id)?Yii::$app->user->identity->id:1;

                                   if (! ($flag = $modelCliente->save(false))) {////REGISTRANDO AL TITULAR POR EL MODELO CLIENTE
                                       $transaction->rollBack();
                                       break;
                                   }
                               }

                      //CARGANDOLE LAS DIRECCIONES A LA PERSONA
                        foreach ($modelDireccion as $modelDireccion) {
                               $modelDireccion->id_persona = $model->id_persona;
                            if (! ($flag = $modelDireccion->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }

                    }    //REGISTRANDO AL TITULAR POR EL MODELO PERSONA

                           foreach ($modelCargaFamiliar as $modelCargaFamiliar) { //RECORRIENDO LOS BENEFICIARIOS

                                   //SETEANDO LOS VALORES EN EL MODELO DE PERSONA
                                          $modelMult->identificacion = $modelCargaFamiliar->identificacion;
                                          $modelMult->nombre = $modelCargaFamiliar->nombre;
                                          $modelMult->segundo_nombre = $modelCargaFamiliar->segundo_nombre;
                                          $modelMult->apellido = $modelCargaFamiliar->apellido;
                                          $modelMult->segundo_apellido = $modelCargaFamiliar->segundo_apellido;
                                          $modelMult->sexo = $modelCargaFamiliar->sexo;
                                          $modelMult->fecha_nacimiento=date('Y-m-d',strtotime($modelCargaFamiliar->fecha_nacimiento));//formateo de fecha antes de guardar
                                          $modelMult->tipo="V";

                                       if (! ($flag = $modelMult->save(false))) {////REGISTRANDO A LOS BENEFICIARIOS PÓR EL MODULO DE PERSONA
                                               $transaction->rollBack();
                                               break;
                                       }else{

                                              //SETANDO LOS VALORES DEL LOS BENEFICIARIOS DEL MODELO CLIENTE
                                            //  foreach ($modelClienteMult as $modelClienteMult) {

                                                  //$modelClienteMult->id_persona = $modelMult->id_persona;
                                                  //$modelClienteMult->fecha_registro=date('Y-m-d H:i:s');
                                                  //para que funcione debe estar logueado
                                                  //$modelClienteMult->id_user_registro =isset(Yii::$app->user->identity->id)?Yii::$app->user->identity->id:1;

                                                    //if (! ($flag = $modelClienteMult->save(false))) {////REGISTRANDO AL TITULAR POR EL MODELO CLIENTE
                                                      //  $transaction->rollBack();
                                                      //  break;
                                                    //}else{
                                                           //SETANDO LOS VALORES DE LOS BENEFICIARIOS EN EL MODELO CARGA FAMILIAR
                                                           $modelCargaFamiliar->id_titular = $modelCliente->id_cliente;  //CARGANDO EL TITULAR
                                                          // $modelCargaFamiliar->id_beneficiario = $modelClienteMult->id_cliente;//CARGANDO LOS BENEFICIARIOS
                                                           $modelCargaFamiliar->id_beneficiario = $modelMult->id_persona;//CARGANDO LOS BENEFICIARIOS


                                                           $modelCargaFamiliar->fecha_registro=date('Y-m-d H:i:s');
                                                           $modelCargaFamiliar->id_user_registro =isset(Yii::$app->user->identity->id)?Yii::$app->user->identity->id:1;

                                                          if (! ($flag = $modelCargaFamiliar->save(false))) {
                                                              $transaction->rollBack();
                                                            break;
                                                          }


                                                  //  }//FIN ELSE

                                            //  }//RECORRIENDO EL MODELO CLIENTE PARA GUARDAR LOS BENEFIIARIOS

                                          }//FIN ELSE

                          } //RECORRIENDO LOS BENEFICIARIOS

                   if ($flag){
                       $transaction->commit();
                         //$model->refresh();
                          Yii::$app->response->format = Response::FORMAT_JSON;
                          return [
                              'message' => '¡Éxito!',
                              'id_contratante'=>$modelCliente->id_cliente,
                              'nombre_completo'=>$model->nombre.' '.$model->apellido,
                              'identificacion'=>(!empty($model->identificacion))?$model->identificacion:'N/A',
                              'identificacion2'=>(!empty($model->identificacion2))?$model->identificacion2:'N/A',
                            ];

                       //return $this->redirect(['view', 'id' => $model->id_persona]);
                   }else{

                     Yii::$app->response->format = Response::FORMAT_JSON;
                          return ArrayHelper::merge(
                             ActiveForm::validateMultiple($modelDireccion),
                             ActiveForm::validateMultiple($modelCargaFamiliar),
                             ActiveForm::validate($model)
                         );


                   }

               } catch (Exception $e) {
                  $transaction->rollBack();
               }

           } //if ($valid)

     }else{

         return $this->renderAjax('create', [
             'model' => $model,
             'modelDireccion' => (empty($modelDireccion)) ? [new Direccion] : $modelDireccion,
             'modelCliente' => (empty($modelCliente)) ? [new Cliente] : $modelCliente,
             'modelCargaFamiliar' => (empty($modelCargaFamiliar)) ? [new CargaFamiliar] : $modelCargaFamiliar


         ]);
     } //if ($model->load(Yii::$app->request->post()))

    }

    /**
     * Updates an existing persona model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

      public function actionUpdate($id)
    {

		       $model = $this->findModel($id);
           $modelDireccion = $model->direccions;
           $modelCliente = $model->clientes;

           $modelCargaFamiliar= [new CargaFamiliar];

            foreach ($modelCliente as $modelCliente) {

                // $modelCargaFamiliar = CargaFamiliarView::find()->where(['id_titular' => $modelCliente->id_cliente])->all();
              /*$query = (new \yii\db\Query())
                      ->select(['nombre','segundo_nombre','apellido','segundo_apellido','parentesco','identificacion','fecha_nacimiento','sexo'])
                      ->from('carga_familiar')
                      ->where('carga_familiar.id_titular=:id', array(':id'=>$modelCliente->id_cliente))
                      ->leftJoin('persona', 'persona.id_persona = carga_familiar.id_beneficiario')
                      ->createCommand();*/
                //return $query->queryAll();
                    /*$sql="SELECT * FROM carga_familiar
                           JOIN persona ON persona.id_persona = carga_familiar.id_titular
                           WHERE carga_familiar.id_titular= {$modelCliente->id_cliente}";
                      $query = (new \yii\db\Query())->createCommand($sql);*/




            }

              //$modelCargaFamiliar= $query->queryAll();

              var_dump($modelCargaFamiliar);


              //exit();


        if ($model->load(Yii::$app->request->post())) {


            $oldIDs = ArrayHelper::map($modelDireccion, 'id', 'id');
            $modelDireccion = Model::createMultiple(Direccion::classname(), $modelDireccion);
            Model::loadMultiple($modelDireccion, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelDireccion, 'id', 'id')));


            $oldIDs_CF = ArrayHelper::map($modelCargaFamiliar, 'id_carga_familiar', 'id_carga_familiar');
            $modelCargaFamiliar = Model::createMultiple(CargaFamiliar::classname());
            Model::loadMultiple($modelCargaFamiliar, Yii::$app->request->post());

            $deletedID_CF = array_diff($oldIDs_CF, array_filter(ArrayHelper::map($modelCargaFamiliar, 'id_carga_familiar', 'id_carga_familiar')));

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelDireccion),
                    //ActiveForm::validateMultiple($modelCargaFamiliar),
                    ActiveForm::validate($model)
                );
            }

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelDireccion) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (! empty($deletedIDs) && ! empty($deletedIDs) ) {
                            Direccion::deleteAll(['id' => $deletedIDs]);
                            //CargaFamiliar::deleteAll(['id_carga_familiar' => $deletedID_CF]);

                        }

                        foreach ($modelDireccion as $modelDireccion) {
                            $modelDireccion->id_persona = $model->id_persona;
                            if (! ($flag = $modelDireccion->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id_persona]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->renderAjax('update', [
            'model' => $model,
            'modelDireccion' => (empty($modelDireccion)) ? [new Direccion] : $modelDireccion,
            'modelCliente' => (empty($modelCliente)) ? [new Cliente] : $modelCliente,
            'modelCargaFamiliar' => (empty($modelCargaFamiliar)) ? [new CargaFamiliar] : $modelCargaFamiliar
        ]);
    }


    /**
     * Deletes an existing persona model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**/

    public function actionCargarContrante(){

      return ArrayHelper::map( Cliente::find()
                                   ->joinwith('idPersona')
                                   ->where("cliente.estatus = :status", [':status' => 1])
                                   ->select("(nombre||' '||segundo_nombre||' '||apellido|| ' ' ||segundo_apellido || ' ' || identificacion) AS nombre_completo ,id_cliente")
                                   ->all(),'id_cliente', 'nombre_completo');


    }

    /**
     * Finds the persona model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return persona the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = persona::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
