<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Automovil;
use frontend\models\Documentos;
use frontend\models\search\SearchAutomovil;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\widgets\MaskedInput;
use yii\bootstrap\Tabs;
use yii\web\Response;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\ActiveField;
use yii\helpers\ArrayHelper;
use kartik\icons\Icon;
use kartik\tabs\TabsX;
use kartik\nav\NavX;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Json;



/**
 * AutomovilController implements the CRUD actions for Automovil model.
 */
class AutomovilController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Automovil models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchAutomovil();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Automovil model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Automovil model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($submit = false)
    {
        $model = new Automovil();
        $documentos= new Documentos();

        $ruta=Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/uploads/';

        // ajax validation
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
             return ActiveForm::validate($model);
        }

        if ($model->save()) {

            $doc_files= UploadedFile::getInstances($documentos, 'doc_files');

            //$documentos->documentos =UploadedFile::getInstances($documentos, 'documentos');
            //$documentos->upload('auto',$model->placa,$model->id_automovil);//subiendo imagenes

            if($documentos->load(Yii::$app->request->post())){

              $doc_files= UploadedFile::getInstances($documentos, 'doc_files');
              $documentos->doc_files=null;
               $nfiles=0;
               foreach ($doc_files as $file) {

                      $documentos= new Documentos();

                      $archivo=$ruta. $model->placa.'_'.$nfiles.'.' . $file->extension;
                      $documentos->extension=$file->extension;
                      $documentos->nombre_archivo=$model->placa.'_'.$nfiles;
                      $documentos->categoria='auto';
                      $documentos->id_automovil=$model->id_automovil;
                    //  $documentos->id_documento= NULL; //primary key(auto increment id) id


                        if(!$documentos->save(false)){
                            Yii::$app->getSession()->setFlash('error', ' error Guardando los archivos.');
                        }else{
                          if(!$file->saveAs($archivo)){
                             Yii::$app->getSession()->setFlash('error', 'Error subiendo los archivos.');
                         }else{

                           //return $this->redirect(['view', 'id' => $model->id_automovil]);
                           Yii::$app->response->format = Response::FORMAT_JSON;
                           return [
                               'message' => '¡Éxito!',
                              // 'id_contratante'=>$modelCliente->id_cliente,
                              // 'nombre_completo'=>$model->nombre.' '.$model->apellido,
                             ];


                         }
                      }
                      $nfiles++;
                }

             }

              //return $this->redirect(['view', 'id' => $model->id_automovil]);
              Yii::$app->response->format = Response::FORMAT_JSON;
              return [
                  'message' => '¡Éxito!',
                 // 'id_contratante'=>$modelCliente->id_cliente,
                 // 'nombre_completo'=>$model->nombre.' '.$model->apellido,
              ];


        } else {


            return $this->renderAjax('create', [
                'model' => $model,
                'documentos'=>$documentos,
            ]);
        }
    }


    public function actionUploadFile(){

      $documentos= new Documentos();
      $model= new Automovil();
      $ruta=Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/uploads/';

      $doc_files= UploadedFile::getInstances($documentos, 'doc_files');
      $documentos->doc_files=null;
      $aleatorio=rand(4,4);

      if($model->load(Yii::$app->request->post())){


          foreach ($doc_files as $file) {


                 $archivo=$ruta. $model->placa.'_'.$file->name.'.' . $file->extension;
                 $documentos->extension=$file->extension;
                 $documentos->nombre_archivo=$model->placa.'_'.$file->name;
                 $documentos->categoria='auto';
                 $documentos->id_automovil=$model->id_automovil;

                   if(!$documentos->save(false)){
                         echo json_encode(['error'=>$files]);
                   }else{
                     if(!$file->saveAs($archivo)){
                          echo json_encode(['error'=>$files]);
                    }else{

                        echo  json_encode(['successful'=>$file]);

                    }
                 }
           }


      }








     }

    /**
     * Updates an existing Automovil model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

     //http://stackoverflow.com/questions/29292377/yii2-kartik-file-input-update/34666757
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_automovil]);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
                'documentos'=>$documentos,
            ]);
        }
    }

    /**
     * Deletes an existing Automovil model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Automovil model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Automovil the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Automovil::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
