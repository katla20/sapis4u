<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class LocateAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
      'css/kendo_2015/kendo.common-material.min.css',
      'css/kendo_2015/kendo.material.min.css',
      'css/kendo_2015/kendo.material.mobile.min.css',
      'css/kendo_2015/kendo.rtl.min.css',

    ];
    public $js = [
    ];
    public $depends = [
    ];
}
