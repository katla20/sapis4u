<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "persona".
 *
 * @property string $nombre
 * @property string $segundo_nombre
 * @property string $apellido
 * @property string $fecha_nacimiento
 * @property string $identificacion
 * @property string $identificacion_2
 * @property string $sexo
 * @property string $correo
 * @property string $tipo
 * @property integer $id_persona
 * @property string $segundo_apellido
 *
 * @property Cliente[] $clientes
 * @property Direccion[] $direccions
 */
class Persona extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
     var $codigo,$comision;
    public static function tableName()
    {
        return 'persona';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'identificacion'], 'required'],
            [['fecha_nacimiento'], 'safe'],
            [['nombre', 'segundo_nombre', 'apellido', 'segundo_apellido'], 'string', 'max' => 300],
            [['identificacion', 'identificacion_2'], 'string', 'max' => 12],
            [['sexo', 'tipo'], 'string', 'max' => 1],
            [['correo'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nombre' => Yii::t('app', 'Nombre'),
            'segundo_nombre' => Yii::t('app', 'Segundo Nombre'),
            'apellido' => Yii::t('app', 'Apellido'),
            'fecha_nacimiento' => Yii::t('app', 'Fecha Nacimiento'),
            'identificacion' => Yii::t('app', 'Identificacion'),
            'identificacion_2' => Yii::t('app', 'Identificacion 2'),
            'sexo' => Yii::t('app', 'Sexo'),
            'correo' => Yii::t('app', 'Correo'),
            'tipo' => Yii::t('app', 'Tipo'),
            'id_persona' => Yii::t('app', 'Id Persona'),
            'segundo_apellido' => Yii::t('app', 'Segundo Apellido'),
            'comision' => Yii::t('app', 'Porcentaje de comision'),
            'codigo' => Yii::t('app', 'Codigo del Intermediario')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientes()
    {
        return $this->hasMany(Cliente::className(), ['id_persona' => 'id_persona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDireccions()
    {
        return $this->hasMany(Direccion::className(), ['id_persona' => 'id_persona']);
    }
}
