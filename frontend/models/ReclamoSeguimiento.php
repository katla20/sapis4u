<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "reclamo_seguimiento".
 *
 * @property integer $id_reclamo_seguimiento
 * @property integer $id_reclamo
 * @property string $comentario
 * @property string $monto
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property integer $estatus
 * @property string $estatus_reclamo
 *
 * @property Reclamos $idReclamo
 */
class ReclamoSeguimiento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reclamo_seguimiento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_reclamo', 'id_user_registro', 'id_user_actualizacion', 'estatus'], 'integer'],
            [['comentario'], 'required'],
            [['comentario'], 'string'],
            [['monto'], 'number'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['estatus_reclamo'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_reclamo_seguimiento' => Yii::t('app', 'Id Reclamo Seguimiento'),
            'id_reclamo' => Yii::t('app', 'Id Reclamo'),
            'comentario' => Yii::t('app', 'Comentario'),
            'monto' => Yii::t('app', 'Monto'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'estatus' => Yii::t('app', 'Estatus'),
            'estatus_reclamo' => Yii::t('app', 'Estatus Reclamo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdReclamo()
    {
        return $this->hasOne(Reclamos::className(), ['id_reclamo' => 'id_reclamo']);
    }
}
