<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "certificado_detalle".
 *
 * @property integer $id_certificado_detalle
 * @property integer $id_certificado
 * @property string $suma_asegurada
 * @property integer $estatus
 * @property string $prima
 * @property string $comision
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property integer $id_cobertura
 *
 * @property Certificado $idCertificado
 * @property Coberturas $idCobertura
 */
class CertificadoDetalle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'certificado_detalle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_certificado', 'id_cobertura'], 'required'],
            [['id_certificado', 'estatus', 'id_user_registro', 'id_user_actualizacion', 'id_cobertura'], 'integer'],
            [['suma_asegurada', 'prima', 'comision'], 'number'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_certificado_detalle' => Yii::t('app', 'Id Certificado Detalle'),
            'id_certificado' => Yii::t('app', 'Id Certificado'),
            'suma_asegurada' => Yii::t('app', 'Suma Asegurada'),
            'estatus' => Yii::t('app', 'Estatus'),
            'prima' => Yii::t('app', 'Prima'),
            'comision' => Yii::t('app', 'Comision'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'id_cobertura' => Yii::t('app', 'Id Cobertura'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCertificado()
    {
        return $this->hasOne(Certificado::className(), ['id_certificado' => 'id_certificado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCobertura()
    {
        return $this->hasOne(Coberturas::className(), ['id_cobertura' => 'id_cobertura']);
    }

    /**
     * @inheritdoc
     * @return CertificadoDetalleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CertificadoDetalleQuery(get_called_class());
    }
}
