<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "zona".
 *
 * @property integer $id_zona
 * @property integer $id_ciudad
 * @property string $estatus
 * @property string $nombre
 * @property string $fecha_registro
 * @property string $fecha_modificacion
 * @property string $descripcion
 *
 * @property Sector[] $sectors
 * @property Ciudad $idCiudad
 */
class Zona extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zona';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_ciudad', 'estatus', 'nombre'], 'required'],
            [['id_ciudad'], 'integer'],
            [['nombre', 'descripcion'], 'string'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_zona' => Yii::t('app', 'Id Zona'),
            'id_ciudad' => Yii::t('app', 'Id Ciudad'),
            'estatus' => Yii::t('app', 'Estatus'),
            'nombre' => Yii::t('app', 'Nombre'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Modificacion'),
            'descripcion' => Yii::t('app', 'Descripcion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSectors()
    {
        return $this->hasMany(Sector::className(), ['id_zona' => 'id_zona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCiudad()
    {
        return $this->hasOne(Ciudad::className(), ['id_ciudad' => 'id_ciudad']);
    }
}
