<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "cliente".
 *
 * @property integer $id_persona
 * @property string $estatus
 * @property integer $id_cliente
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 *
 * @property Persona $idPersona
 */
class Cliente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

     public $nombre_completo;
    public static function tableName()
    {
        return 'cliente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_persona', 'fecha_registro', 'id_user_registro'], 'required'],
            [['id_persona', 'id_user_registro', 'id_user_actualizacion'], 'integer'],
            [['estatus'], 'number'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_persona' => Yii::t('app', 'Id Persona'),
            'estatus' => Yii::t('app', 'Estatus'),
            'id_cliente' => Yii::t('app', 'Id Cliente'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPersona()
    {
        return $this->hasOne(Persona::className(), ['id_persona' => 'id_persona']);
    }
}
