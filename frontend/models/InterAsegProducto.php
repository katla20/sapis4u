<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "inter_aseg_producto".
 *
 * @property integer $id_aseguradora
 * @property integer $id_producto
 *
 * @property Aseguradoras $idAseguradora
 * @property Productos $idProducto
 */
class InterAsegProducto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inter_aseg_producto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_aseguradora', 'id_producto'], 'required'],
            [['id_aseguradora', 'id_producto'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_aseguradora' => Yii::t('app', 'Id Aseguradora'),
            'id_producto' => Yii::t('app', 'Id Producto'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAseguradora()
    {
        return $this->hasOne(Aseguradoras::className(), ['id_aseguradora' => 'id_aseguradora']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProducto()
    {
        return $this->hasOne(Productos::className(), ['id_producto' => 'id_producto']);
    }
}
