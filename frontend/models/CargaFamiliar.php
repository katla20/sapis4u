<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "carga_familiar".
 *
 * @property integer $id_titular
 * @property integer $id_beneficiario
 * @property integer $id_carga_familiar
 * @property string $parentesco
 * @property string $fecha_registro
 * @property integer $id_user_registro
 * @property string $fecha_actualizacion
 * @property string $id_user_actualizacion
 * @property string $estatus
 *
 * @property Cliente $idTitular
 * @property Persona $idBeneficiario
 */
class CargaFamiliar extends \yii\db\ActiveRecord
{
      public $nombre,$segundo_nombre,$apellido,$segundo_apellido,$identificacion,$fecha_nacimiento,$sexo;


    public static function tableName()
    {
        return 'carga_familiar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_titular', 'id_beneficiario', 'parentesco', 'fecha_registro', 'id_user_registro',
               'nombre','apellido','identificacion','fecha_nacimiento','sexo'], 'required'],
            [['id_titular', 'id_beneficiario', 'id_user_registro'], 'integer'],
            [['fecha_registro', 'fecha_actualizacion', 'id_user_actualizacion'], 'safe'],
            [['estatus'], 'number'],
            [['parentesco'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_titular' => Yii::t('app', 'Id Titular'),
            'id_beneficiario' => Yii::t('app', 'Id Beneficiario'),
            'id_carga_familiar' => Yii::t('app', 'Id Carga Familiar'),
            'parentesco' => Yii::t('app', 'Parentesco'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'estatus' => Yii::t('app', 'Estatus'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTitular()
    {
        return $this->hasOne(Cliente::className(), ['id_cliente' => 'id_titular']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdBeneficiario()
    {
        return $this->hasOne(Persona::className(), ['id_persona' => 'id_beneficiario']);
    }

    public function getCargaFamiliarForm(){

      $query = (new \yii\db\Query())
              ->select(['nombre','segundo_nombre','apellido','segundo_apellido','parentesco','identificacion','fecha_nacimiento','sexo'])
              ->from('carga_familiar')
              ->where('carga_familiar.id_titular=:id', array(':id'=>$modelCliente->id_cliente))
              ->leftJoin('persona', 'persona.id_persona = carga_familiar.id_beneficiario')
              ->createCommand();
        return $query->queryAll();
         //echo $query->sql;exit();

    }
}
