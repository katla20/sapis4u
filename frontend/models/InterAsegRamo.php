<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "inter_aesg_ramo".
 *
 * @property integer $id_aseguradora
 * @property integer $id_ramo
 *
 * @property Aseguradoras $idAseguradora
 * @property Ramos $idRamo
 */
class InterAsegRamo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inter_aseg_ramo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_aseguradora', 'id_ramo'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_aseguradora' => Yii::t('app', 'Id Aseguradora'),
            'id_ramo' => Yii::t('app', 'Id Ramo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAseguradora()
    {
        return $this->hasOne(Aseguradoras::className(), ['id_aseguradora' => 'id_aseguradora']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRamo()
    {
        return $this->hasOne(Ramos::className(), ['id_ramo' => 'id_ramo']);
    }
}
