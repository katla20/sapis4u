<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "aseguradoras".
 *
 * @property integer $id_aseguradora
 * @property string $nombre
 * @property string $direccion
 * @property string $telefono
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property integer $estatus
 * @property string $rif
 *
 * @property InterAsegProducto[] $interAsegProductos
 * @property InterAsegRamo[] $interAsegRamos
 * @property Poliza[] $polizas
 */
class Aseguradoras extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aseguradoras';
    }

    /**
     * @inheritdoc
     */
        public $ramos;
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['direccion'], 'string'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['id_user_registro', 'id_user_actualizacion', 'estatus'], 'integer'],
            [['nombre'], 'string', 'max' => 200],
            [['telefono'], 'string', 'max' => 20],
            [['rif'], 'string', 'max' => 12]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_aseguradora' => Yii::t('app', 'Id Aseguradora'),
            'nombre' => Yii::t('app', 'Nombre'),
            'direccion' => Yii::t('app', 'Direccion'),
            'telefono' => Yii::t('app', 'Telefono'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'estatus' => Yii::t('app', 'Estatus'),
            'rif' => Yii::t('app', 'Rif'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterAsegProductos()
    {
        return $this->hasMany(InterAsegProducto::className(), ['id_aseguradora' => 'id_aseguradora']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterAsegRamos()
    {
        return $this->hasMany(InterAsegRamo::className(), ['id_aseguradora' => 'id_aseguradora']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolizas()
    {
        return $this->hasMany(Poliza::className(), ['id_aseguradora' => 'id_aseguradora']);
    }
}
