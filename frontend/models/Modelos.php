<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "modelos".
 *
 * @property string $id_modelo
 * @property string $id_marca
 * @property string $nombre
 * @property integer $ws_inhabilitado
 * @property string $fecha_registro
 * @property string $fecha_modificacion
 * @property integer $id_user_registro
 * @property string $id_user_actualizazcion
 *
 * @property Marcas $idMarca
 */
class Modelos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'modelos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_modelo'], 'required'],
            [['ws_inhabilitado', 'id_user_registro'], 'integer'],
            [['fecha_registro', 'fecha_actualizacion', 'id_user_actualizacion'], 'safe'],
            [['id_modelo', 'id_marca'], 'string', 'max' => 15],
            [['nombre'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_modelo' => Yii::t('app', 'Id Modelo'),
            'id_marca' => Yii::t('app', 'Id Marca'),
            'nombre' => Yii::t('app', 'Nombre'),
            'ws_inhabilitado' => Yii::t('app', 'Ws Inhabilitado'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Modificacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
        ];
    }
	
	

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMarca()
    {
        return $this->hasOne(Marcas::className(), ['id_marca' => 'id_marca']);
    }
	
	public function getVersions()
    {
        return $this->hasMany(Modelos::className(), ['id_modelo' => 'id_modelo']);
    }
}
