<?php
namespace backend\models;

use yii;

use yii\helpers\Url;//linea para el asistente de url keyla bullon


class SeguimientoR {

    public static function getMostrar($id)
    {
		$res=array();
        $connection = \Yii::$app->db;
        $sql = "select DISTINCT (to_char(fecha_registro,'mm-yyyy')) AS anio_mes 
                FROM reclamo_seguimiento 
                where id_reclamo=:id 
                order by anio_mes";
        $command = $connection->createCommand($sql);
        $command->bindValue(":id", $id);
        $result = $command->queryAll();

        if (count($result)!= 0){
           
           foreach($result as $row){ 
               echo '<li class="time-label">';
                    echo'<span class="bg-green">';
                        echo  $row['anio_mes'];       
                    echo'</span>';
               echo '</li>';
               SeguimientoR::comentarios($id,$row['anio_mes']);
          }     
        } 
    }
    
    
  public static function comentarios($id,$anio_mes){
			   
        $res=array();
		$connection = \Yii::$app->db;
		$sql = "SELECT 
                rs.comentario,
                rs.monto,
                to_char(rs.fecha_registro, 'dd-mm-yyyy') fecha,
                to_char(rs.fecha_registro, 'HH12:MI:SS') hora ,
                rs.estatus_reclamo,
                u.username 
                FROM reclamo_seguimiento rs
                INNER JOIN ".'"user"'." AS u on (u.id=rs.id_user_registro)
                WHERE id_reclamo=:id AND to_char(rs.fecha_registro,'mm-yyyy')=:anio_mes";
		$command = $connection->createCommand($sql);
			//$command->bindValue(":user", $user);
		$user=(isset(Yii::$app->user->identity->id))? Yii::$app->user->identity->id:1;
		$command->bindValue(":id", $id);
		$command->bindValue(":anio_mes", $anio_mes);
		$result = $command->queryAll();   
			
		if (count($result)!= 0){
			foreach($result as $row){
			 
			echo '<li>';
             echo '<i class="fa fa-comments bg-yellow"></i>';
        
             echo '<div class="timeline-item">';
             
               echo '<span class="time"><i class="fa fa-clock-o"></i>'.$row['hora'].'</span>';
               echo '<span class="time">'.$row['fecha'].'</span>';
        
               echo '<h3 class="timeline-header"><i class="fa fa-user bg-aqua"></i>  '.$row['username'].'</h3>';
        
               echo '<div class="timeline-body">
                  '.$row['comentario'].'
                </div>';
               echo '</div>
            </li>';
             
				
			}					
		}	         
	}

}
