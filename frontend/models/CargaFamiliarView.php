<?php

namespace frontend\models;
namespace app\models;
use yii\base\Model;

use Yii;

/**
 * This is the model class for table "carga_familiar_view".
 *
 * @property string $nombre
 * @property string $segundo_nombre
 * @property string $apellido
 * @property string $segundo_apellido
 * @property string $parentesco
 * @property string $identificacion
 * @property string $fecha_nacimiento
 * @property string $sexo
 * @property integer $id_titular
 * @property integer $id_beneficiario
 */
class CargaFamiliarView extends Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'carga_familiar_view';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha_nacimiento'], 'safe'],
            [['id_titular', 'id_beneficiario'], 'integer'],
            [['nombre', 'segundo_nombre', 'apellido', 'segundo_apellido'], 'string', 'max' => 300],
            [['parentesco'], 'string', 'max' => 20],
            [['identificacion'], 'string', 'max' => 12],
            [['sexo'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nombre' => Yii::t('app', 'Nombre'),
            'segundo_nombre' => Yii::t('app', 'Segundo Nombre'),
            'apellido' => Yii::t('app', 'Apellido'),
            'segundo_apellido' => Yii::t('app', 'Segundo Apellido'),
            'parentesco' => Yii::t('app', 'Parentesco'),
            'identificacion' => Yii::t('app', 'Identificacion'),
            'fecha_nacimiento' => Yii::t('app', 'Fecha Nacimiento'),
            'sexo' => Yii::t('app', 'Sexo'),
            'id_titular' => Yii::t('app', 'Id Titular'),
            'id_beneficiario' => Yii::t('app', 'Id Beneficiario'),
        ];
    }
}
