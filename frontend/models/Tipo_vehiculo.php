<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tipo_vehiculo".
 *
 * @property integer $id_tipo_vehiculo
 * @property string $nombre
 * @property string $descripcion
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property integer $estatus
 *
 * @property Automovil[] $automovils
 */
class Tipo_vehiculo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_vehiculo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['descripcion'], 'string'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['id_user_registro', 'id_user_actualizacion', 'estatus'], 'integer'],
            [['nombre'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_tipo_vehiculo' => Yii::t('app', 'Id Tipo Vehiculo'),
            'nombre' => Yii::t('app', 'Nombre'),
            'descripcion' => Yii::t('app', 'Descripcion'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'estatus' => Yii::t('app', 'Estatus'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutomovils()
    {
        return $this->hasMany(Automovil::className(), ['id_tipo_vehiculo' => 'id_tipo_vehiculo']);
    }
}
