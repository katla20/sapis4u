<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "certificado".
 *
 * @property integer $id_certificado
 * @property integer $nro_certificado
 * @property integer $id_recibo
 * @property integer $id_titular
 * @property integer $estatus
 * @property integer $id_automovil
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property integer $id_persona
 * @property integer $id_producto
 * @property string $deducible
 *
 * @property Automovil $idAutomovil
 * @property Recibo $idRecibo
 * @property CertificadoDetalle[] $certificadoDetalles
 */
class Certificado extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'certificado';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nro_certificado', 'id_recibo', 'id_titular'], 'required'],
            [['nro_certificado', 'id_recibo', 'id_titular', 'estatus', 'id_automovil', 'id_user_registro', 'id_user_actualizacion', 'id_persona', 'id_producto'], 'integer'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['deducible'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_certificado' => Yii::t('app', 'Id Certificado'),
            'nro_certificado' => Yii::t('app', 'Nro Certificado'),
            'id_recibo' => Yii::t('app', 'Id Recibo'),
            'id_titular' => Yii::t('app', 'Id Titular'),
            'estatus' => Yii::t('app', 'Estatus'),
            'id_automovil' => Yii::t('app', 'Id Automovil'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'id_persona' => Yii::t('app', 'Id Persona'),
            'id_producto' => Yii::t('app', 'Id Producto'),
            'deducible' => Yii::t('app', 'Deducible'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAutomovil()
    {
        return $this->hasOne(Automovil::className(), ['id_automovil' => 'id_automovil']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRecibo()
    {
        return $this->hasOne(Recibo::className(), ['id_recibo' => 'id_recibo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCertificadoDetalles()
    {
        return $this->hasMany(CertificadoDetalle::className(), ['id_certificado' => 'id_certificado']);
    }

    /**
     * @inheritdoc
     * @return CertificadoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CertificadoQuery(get_called_class());
    }
}
