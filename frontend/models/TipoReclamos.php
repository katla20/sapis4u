<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tipo_reclamos".
 *
 * @property integer $id_tipo_reclamo
 * @property string $nombre
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property integer $estatus
 *
 * @property Reclamos[] $reclamos
 */
class TipoReclamos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_reclamos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['id_user_registro', 'id_user_actualizacion', 'estatus'], 'integer'],
            [['nombre'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_tipo_reclamo' => Yii::t('app', 'Id Tipo Reclamo'),
            'nombre' => Yii::t('app', 'Nombre'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'estatus' => Yii::t('app', 'Estatus'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReclamos()
    {
        return $this->hasMany(Reclamos::className(), ['id_tipo_reclamo' => 'id_tipo_reclamo']);
    }
}
