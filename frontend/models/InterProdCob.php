<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "inter_prod_cob".
 *
 * @property integer $id_cobertura
 * @property integer $id_producto
 *
 * @property Coberturas $idCobertura
 * @property Productos $idProducto
 */
class InterProdCob extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inter_prod_cob';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_cobertura', 'id_producto'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_cobertura' => Yii::t('app', 'Id Cobertura'),
            'id_producto' => Yii::t('app', 'Id Producto'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCobertura()
    {
        return $this->hasOne(Coberturas::className(), ['id_cobertura' => 'id_cobertura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProducto()
    {
        return $this->hasOne(Productos::className(), ['id_producto' => 'id_producto']);
    }
}
