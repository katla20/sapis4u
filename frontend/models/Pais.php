<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "pais".
 *
 * @property integer $id_pais
 * @property string $estatus
 * @property string $nombre
 * @property string $fecha_registro
 * @property string $fecha_modificacion
 * @property string $descripcion
 *
 * @property Estado[] $estados
 */
class Pais extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pais';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['estatus', 'nombre',], 'required'],
            [['nombre', 'descripcion'], 'string'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pais' => Yii::t('app', 'Id Pais'),
            'estatus' => Yii::t('app', 'Estatus'),
            'nombre' => Yii::t('app', 'Nombre'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Modificacion'),
            'descripcion' => Yii::t('app', 'Descripcion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstados()
    {
        return $this->hasMany(Estado::className(), ['id_pais' => 'id_pais']);
    }
}
