<?php

namespace frontend\models;

/**
 * This is the ActiveQuery class for [[CertificadoDetalle]].
 *
 * @see CertificadoDetalle
 */
class CertificadoDetalleQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return CertificadoDetalle[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CertificadoDetalle|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}