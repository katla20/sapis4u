<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "direccion".
 *
 * @property integer $id
 * @property integer $id_persona
 * @property string $calle_avenida
 * @property string $sector
 * @property string $nro
 * @property string $codigo_postal
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_zona
 * @property string $telefono
 * @property string $telefono2
 * @property string $telefono3
 * @property string $nombre_direccion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property string $direccion
 * @property integer $id_ciudad
 *
 * @property Ciudad $idCiudad
 * @property Persona $idPersona
 * @property User $idUserRegistro
 * @property User $idUserActualizacion
 * @property Zona $idZona
 */
class Direccion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	 
	public $id_pais,$id_estado; 
	 
    public static function tableName()
    {
        return 'direccion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_persona', 'id_zona', 'id_user_registro', 'id_user_actualizacion', 'id_ciudad'], 'integer'],
            [['calle_avenida', 'nombre_direccion', 'direccion'], 'string'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['nombre_direccion'], 'required'],
            [['sector'], 'string', 'max' => 250],
            [['nro', 'codigo_postal', 'telefono', 'telefono2', 'telefono3'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_persona' => Yii::t('app', 'Id Persona'),
            'calle_avenida' => Yii::t('app', 'Calle Avenida'),
            'sector' => Yii::t('app', 'Sector'),
            'nro' => Yii::t('app', 'Nro'),
            'codigo_postal' => Yii::t('app', 'Codigo Postal'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_zona' => Yii::t('app', 'Id Zona'),
            'telefono' => Yii::t('app', 'Telefono'),
            'telefono2' => Yii::t('app', 'Telefono2'),
            'telefono3' => Yii::t('app', 'Telefono3'),
            'nombre_direccion' => Yii::t('app', 'Nombre Direccion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'direccion' => Yii::t('app', 'Direccion'),
            'id_ciudad' => Yii::t('app', 'Id Ciudad'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCiudad()
    {
        return $this->hasOne(Ciudad::className(), ['id_ciudad' => 'id_ciudad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPersona()
    {
        return $this->hasOne(Persona::className(), ['id_persona' => 'id_persona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUserRegistro()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user_registro']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUserActualizacion()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user_actualizacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdZona()
    {
        return $this->hasOne(Zona::className(), ['id_zona' => 'id_zona']);
    }
}
