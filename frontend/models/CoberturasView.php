<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "coberturas_view".
 *
 * @property string $nombre_aseguradora
 * @property integer $id_aseguradora
 * @property integer $aseguradora_estatus
 * @property string $nombre_producto
 * @property integer $id_producto
 * @property integer $producto_estatus
 * @property integer $id_cobertura
 * @property string $nombre_cobertura
 * @property integer $cobertura_estatus
 */
class CoberturasView extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coberturas_view';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_aseguradora', 'aseguradora_estatus', 'id_producto', 'producto_estatus', 'id_cobertura', 'cobertura_estatus'], 'integer'],
            [['nombre_aseguradora'], 'string', 'max' => 200],
            [['nombre_producto', 'nombre_cobertura'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nombre_aseguradora' => Yii::t('app', 'Nombre Aseguradora'),
            'id_aseguradora' => Yii::t('app', 'Id Aseguradora'),
            'aseguradora_estatus' => Yii::t('app', 'Aseguradora Estatus'),
            'nombre_producto' => Yii::t('app', 'Nombre Producto'),
            'id_producto' => Yii::t('app', 'Id Producto'),
            'producto_estatus' => Yii::t('app', 'Producto Estatus'),
            'id_cobertura' => Yii::t('app', 'Id Cobertura'),
            'nombre_cobertura' => Yii::t('app', 'Nombre Cobertura'),
            'cobertura_estatus' => Yii::t('app', 'Cobertura Estatus'),
        ];
    }

    /**
     * @inheritdoc
     * @return CoberturasViewQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CoberturasViewQuery(get_called_class());
    }
}
