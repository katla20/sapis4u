<?php

namespace frontend\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;

/**
 * This is the model class for table "documentos".
 *
 * @property integer $id_documento
 * @property string $fecha_registro
 * @property integer $id_user_registro
 * @property integer $estatus
 * @property string $extension
 * @property string $nombre_archivo
 * @property string $categoria
 * @property integer $id_user_actualizacion
 * @property string $fecha_actualizacion
 * @property string $ruta
 * @property string $descripcion
 * @property integer $id_automovil
 * @property integer $id_persona
 *
 * @property Automovil $idAutomovil
 * @property Persona $idPersona
 */
class Documentos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $doc_files;

    public static function tableName()
    {
        return 'documentos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          [['doc_files'], 'file',
            'skipOnEmpty' =>  true,//importante para que traiga los datos
            'maxSize' => 1024*1024*1, //1 MB
            'tooBig' => 'El tamaño máximo permitido es 1MB', //Error
            'minSize' => 10, //10 Bytes
            'tooSmall' => 'El tamaño mínimo permitido son 10 BYTES', //Error
            'extensions' => 'jpg , gif , png',
            'wrongExtension' => 'El archivo {file} no contiene una extensión permitida {extensions}', //Error
            'maxFiles' => 4,
            'tooMany' => 'El máximo de archivos permitidos son {limit}', //Error
          ],
            [['fecha_registro', 'fecha_actualizacion','doc_files'], 'safe'],
            [['id_user_registro', 'estatus', 'id_user_actualizacion', 'id_automovil', 'id_persona'], 'integer'],
            [['ruta', 'descripcion'], 'string'],
            [['extension'], 'string', 'max' => 4],
            [['nombre_archivo'], 'string', 'max' => 100],
            [['categoria'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_documento' => Yii::t('app', 'Id Documento'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'estatus' => Yii::t('app', 'Estatus'),
            'extension' => Yii::t('app', 'Extension'),
            'nombre_archivo' => Yii::t('app', 'Nombre Archivo'),
            'categoria' => Yii::t('app', 'Categoria'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'ruta' => Yii::t('app', 'Ruta'),
            'descripcion' => Yii::t('app', 'Descripcion'),
            'id_automovil' => Yii::t('app', 'Id Automovil'),
            'id_persona' => Yii::t('app', 'Id Persona'),
            'doc_files' => Yii::t('app', 'Seleccionar archivos'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAutomovil()
    {
        return $this->hasOne(Automovil::className(), ['id_automovil' => 'id_automovil']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPersona()
    {
        return $this->hasOne(Persona::className(), ['id_persona' => 'id_persona']);
    }

    /**
     * @inheritdoc
     * @return DocumentosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DocumentosQuery(get_called_class());
    }

    public function upload($tipo,$placa,$id)
      {
            $ruta=Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/uploads/';
            //$this->documentos= UploadedFile::getInstances($this, 'documentos');
          //  $ruta2=Yii::$app->params['uploadUrl'] = Yii::$app->urlManager->baseUrl . '/uploads/';
           if(!empty($this->doc_files)){

                 if ($this->validate()) {
                      $i=1;
                     foreach ($this->doc_files as $file) {

                           $file->saveAs( $ruta. $placa.'_'.$i.'.' . $file->extension);
                           $i++;
                           $this->extension=$file->extension;
                           $this->nombre_archivo=$placa.'_'.$i;
                           $this->categoria=$tipo;
                           $this->ruta="";
                           if($tipo=='auto'){
                               $this->id_automovil=$id;
                           }else{
                               $this->id_persona=$id;
                           }

                          /* if($this->save()){
                             return true;
                           }else {
                             return false;
                           }*/

                 }
                     return true;
                 } else {
                     return false;
                 }

           }

      }

}
