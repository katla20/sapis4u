<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ciudad".
 *
 * @property integer $id_ciudad
 * @property integer $id_estado
 * @property string $estatus
 * @property string $nombre
 * @property string $fecha_registro
 * @property string $fecha_modificacion
 * @property string $ciudad_zona
 * @property string $descripcion
 *
 * @property Estado $idEstado
 * @property Zona[] $zonas
 */
class Ciudad extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ciudad';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_estado', 'estatus', 'nombre'], 'required'],
            [['id_estado'], 'integer'],
            [['nombre', 'ciudad_zona', 'descripcion'], 'string'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_ciudad' => Yii::t('app', 'Id Ciudad'),
            'id_estado' => Yii::t('app', 'Id Estado'),
            'estatus' => Yii::t('app', 'Estatus'),
            'nombre' => Yii::t('app', 'Nombre'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Modificacion'),
            'ciudad_zona' => Yii::t('app', 'Ciudad Zona'),
            'descripcion' => Yii::t('app', 'Descripcion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstado()
    {
        return $this->hasOne(Estado::className(), ['id_estado' => 'id_estado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZonas()
    {
        return $this->hasMany(Zona::className(), ['id_ciudad' => 'id_ciudad']);
    }
}
