<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "uso".
 *
 * @property integer $id_uso
 * @property string $nombre
 * @property string $tipo
 * @property string $tasa
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property integer $estatus
 * @property string $comentario
 *
 * @property Automovil[] $automovils
 */
class Uso extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'uso';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'tipo', 'tasa'], 'required'],
            [['nombre', 'comentario'], 'string'],
            [['tasa'], 'number'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['id_user_registro', 'id_user_actualizacion', 'estatus'], 'integer'],
            [['tipo'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_uso' => Yii::t('app', 'Id Uso'),
            'nombre' => Yii::t('app', 'Nombre'),
            'tipo' => Yii::t('app', 'Tipo'),
            'tasa' => Yii::t('app', 'Tasa'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'estatus' => Yii::t('app', 'Estatus'),
            'comentario' => Yii::t('app', 'Comentario'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutomovils()
    {
        return $this->hasMany(Automovil::className(), ['id_uso' => 'id_uso']);
    }
}
