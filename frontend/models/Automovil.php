<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "automovil".
 *
 * @property integer $id_automovil
 * @property integer $id_version
 * @property string $placa
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property string $color
 * @property string $serial_motor
 * @property string $serial_carroceria
 * @property integer $id_tipo_vehiculo
 * @property integer $id_uso
 * @property integer $id_modelo
 *
 * @property TipoVehiculo $idTipoVehiculo
 * @property Uso $idUso
 * @property Version $idVersion
 * @property Modelo $idModelo
 */
class Automovil extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

	public $id_marca,$id_modelo,$id_anio;

    public static function tableName()
    {
        return 'automovil';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
				[['id_marca','placa',/*'id_uso', 'id_tipo_vehiculo'*/], 'required'],
            [['id_version', 'id_user_registro', 'id_user_actualizacion', 'id_tipo_vehiculo', 'id_uso'], 'integer'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['serial_motor'], 'string'],
            [['placa'], 'string', 'max' => 10],
            [['color'], 'string', 'max' => 20],
            [['serial_carroceria'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_automovil' => Yii::t('app', 'Automovil'),
            'id_version' => Yii::t('app', 'Version'),
            'placa' => Yii::t('app', 'Placa'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'color' => Yii::t('app', 'Color'),
            'serial_motor' => Yii::t('app', 'Serial del Motor'),
            'serial_carroceria' => Yii::t('app', 'Serial de la Carroceria'),
            'id_tipo_vehiculo' => Yii::t('app', 'Tipo Vehiculo'),
            'id_uso' => Yii::t('app', 'Uso'),
			'id_marca' => Yii::t('app', 'Marca'),
			'id_modelo' => Yii::t('app', 'Modelo'),
			'id_anio' => Yii::t('app', 'Año'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTipoVehiculo()
    {
        return $this->hasOne(TipoVehiculo::className(), ['id_tipo_vehiculo' => 'id_tipo_vehiculo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUso()
    {
        return $this->hasOne(Uso::className(), ['id_uso' => 'id_uso']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdVersion()
    {
        return $this->hasOne(Version::className(), ['id_version' => 'id_version']);
    }
	 public function getIdModelo()
    {
        return $this->hasOne(Modelo::className(), ['id_modelo' => 'id_modelo']);
    }
}
