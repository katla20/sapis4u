<?php

namespace frontend\models;

use Yii;
use frontend\models\InterProdCob;
use frontend\models\Productos;

/**
 * This is the model class for table "coberturas".
 *
 * @property integer $id_cobertura
 * @property string $nombre
 *
 * @property InterProdCob[] $interProdCobs
 */
class Coberturas extends \yii\db\ActiveRecord
{
    public $productos;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coberturas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 50],
            ['productos', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */

  public function afterSave($insert, $changedAttributes){
		\Yii::$app->db->createCommand()->delete('inter_prod_cob', 'id_cobertura = '.(int) $this->id_cobertura)->execute();

		foreach ($this->productos as $id) {
			$ro = new InterProdCob();
			$ro->id_cobertura = $this->id_cobertura;
			$ro->id_producto = $id;
			$ro->save();
		}
	}
    public function attributeLabels()
    {
        return [
            'id_cobertura' => Yii::t('app', 'Id Cobertura'),
            'nombre' => Yii::t('app', 'Nombre'),
			'estatus' => Yii::t('app', 'Estatus'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterProdCobs()
    {
        return $this->hasMany(InterProdCob::className(), ['id_cobertura' => 'id_cobertura']);
    }

    public function getProductos()
	{
		return $this->hasMany(Productos::className(), ['id_producto' => 'id_producto'])
			->viaTable('inter_prod_cob', ['id_cobertura' => 'id_cobertura']);
	}

	public function getProductosList()
	{
		return $this->getProductos()->asArray();
	}
}
