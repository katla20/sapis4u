<?php

namespace frontend\models;

/**
 * This is the ActiveQuery class for [[Certificado]].
 *
 * @see Certificado
 */
class CertificadoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Certificado[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Certificado|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}