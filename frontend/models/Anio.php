<?php
namespace frontend\models;

use yii;


class Anio {

    public static function getAnio($id_modelo)
    {
		
        $connection = \Yii::$app->db;
        $sql = 'SELECT DISTINCT ON (anio) anio, id_modelo FROM version
		WHERE id_modelo LIKE ":id_modelo%" ORDER BY anio, id_modelo';
        $command = $connection->createCommand($sql);
        $command->bindValue(":id_modelo", $id_modelo);
        $result = $command->queryAll();
		
		

        if (count($result)!= 0){
			$res=$result;
            return $res;
        } else {
            return 1;
        }
    }

}