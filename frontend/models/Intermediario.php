<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "intermediario".
 *
 * @property integer $id_intermediario
 * @property integer $id_persona
 * @property string $codigo
 * @property integer $comision
 * @property integer $id_tipo_intermediario
 * @property integer $estatus
 * @property string $fecha_registro
 * @property integer $id_user_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_actualizacion
 *
 * @property Persona $idPersona
 * @property TipoIntermediario $idTipoIntermediario
 * @property User $idUserRegistro
 * @property User $idUserActualizacion
 */
class Intermediario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
     var $nombre_completo;
    public static function tableName()
    {
        return 'intermediario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_persona'], 'required'],
            [['id_persona', 'comision', 'id_tipo_intermediario', 'estatus', 'id_user_registro', 'id_user_actualizacion'], 'integer'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['codigo'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_intermediario' => Yii::t('app', 'Id Intermediario'),
            'id_persona' => Yii::t('app', 'Id Persona'),
            'codigo' => Yii::t('app', 'Codigo'),
            'comision' => Yii::t('app', 'Comision'),
            'id_tipo_intermediario' => Yii::t('app', 'Id Tipo Intermediario'),
            'estatus' => Yii::t('app', 'Estatus'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPersona()
    {
        return $this->hasOne(Persona::className(), ['id_persona' => 'id_persona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTipoIntermediario()
    {
        return $this->hasOne(TipoIntermediario::className(), ['id_tipo_intermediario' => 'id_tipo_intermediario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUserRegistro()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user_registro']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUserActualizacion()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user_actualizacion']);
    }
}
