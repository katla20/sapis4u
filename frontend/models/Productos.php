<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property integer $id_producto
 * @property string $nombre
 * @property integer $id_ramo
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property integer $estatus
 *
 * @property InterAsegProducto[] $interAsegProductos
 * @property InterProdCob[] $interProdCobs
 * @property Poliza[] $polizas
 * @property Ramos $idRamo
 */
class Productos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_ramo', 'id_user_registro', 'id_user_actualizacion', 'estatus'], 'integer'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['nombre'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_producto' => Yii::t('app', 'Id Producto'),
            'nombre' => Yii::t('app', 'Nombre'),
            'id_ramo' => Yii::t('app', 'Id Ramo'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'estatus' => Yii::t('app', 'Estatus'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterAsegProductos()
    {
        return $this->hasMany(InterAsegProducto::className(), ['id_producto' => 'id_producto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterProdCobs()
    {
        return $this->hasMany(InterProdCob::className(), ['id_producto' => 'id_producto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolizas()
    {
        return $this->hasMany(Poliza::className(), ['id_producto' => 'id_producto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRamo()
    {
        return $this->hasOne(Ramos::className(), ['id_ramo' => 'id_ramo']);
    }
}
