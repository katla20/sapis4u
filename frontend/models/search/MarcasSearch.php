<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Marcas;

/**
 * MarcasSearch represents the model behind the search form about `frontend\models\Marcas`.
 */
class MarcasSearch extends Marcas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_marca', 'nombre', 'fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['ws_inhabilitado', 'id_user_registro', 'id_user_actualizacion'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Marcas::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ws_inhabilitado' => $this->ws_inhabilitado,
            'fecha_registro' => $this->fecha_registro,
            'fecha_actualizacion' => $this->fecha_actualizacion,
            'id_user_registro' => $this->id_user_registro,
            'id_user_actualizacion' => $this->id_user_actualizacion,
        ]);

        $query->andFilterWhere(['like', 'id_marca', $this->id_marca])
            ->andFilterWhere(['like', 'nombre', $this->nombre]);

        return $dataProvider;
    }
}
