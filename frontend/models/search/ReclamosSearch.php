<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Reclamos;

/**
 * ReclamosSearch represents the model behind the search form about `frontend\models\Reclamos`.
 */
class ReclamosSearch extends Reclamos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_reclamo', 'id_recibo', 'id_tipo_reclamo', 'id_user_registro', 'id_user_actualizacion', 'estatus'], 'integer'],
            [['descripcion', 'fecha_registro', 'fecha_actualizacion', 'fecha_ocurrencia', 'fecha_notificacion'], 'safe'],
            [['monto'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Reclamos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_reclamo' => $this->id_reclamo,
            'id_recibo' => $this->id_recibo,
            'id_tipo_reclamo' => $this->id_tipo_reclamo,
            'monto' => $this->monto,
            'fecha_registro' => $this->fecha_registro,
            'fecha_actualizacion' => $this->fecha_actualizacion,
            'id_user_registro' => $this->id_user_registro,
            'id_user_actualizacion' => $this->id_user_actualizacion,
            'estatus' => $this->estatus,
            'fecha_ocurrencia' => $this->fecha_ocurrencia,
            'fecha_notificacion' => $this->fecha_notificacion,
        ]);

        $query->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}
