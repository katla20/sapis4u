<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Poliza;

/**
 * PolizaSearch represents the model behind the search form about `frontend\models\Poliza`.
 */
class PolizaSearch extends Poliza
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_poliza', 'id_contratante', 'id_user_registro', 'id_user_actualizacion'], 'integer'],
            [['poliza_numero', 'fecha_vigenciadesde', 'fecha_vigenciahasta', 'fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['tipo_poliza', 'estatus'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Poliza::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_poliza' => $this->id_poliza,
            'tipo_poliza' => $this->tipo_poliza,
            'fecha_vigenciadesde' => $this->fecha_vigenciadesde,
            'fecha_vigenciahasta' => $this->fecha_vigenciahasta,
            'estatus' => $this->estatus,
            'id_contratante' => $this->id_contratante,
            'fecha_registro' => $this->fecha_registro,
            'fecha_actualizacion' => $this->fecha_actualizacion,
            'id_user_registro' => $this->id_user_registro,
            'id_user_actualizacion' => $this->id_user_actualizacion,
        ]);

        $query->andFilterWhere(['like', 'numero_poliza', $this->numero_poliza]);

        return $dataProvider;
    }
}
