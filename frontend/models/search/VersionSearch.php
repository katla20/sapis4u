<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Version;

/**
 * Versionsearch represents the model behind the search form about `frontend\models\Version`.
 */
class Versionsearch extends Version
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_modelo2', 'nombre', 'tipo', 'caja', 'motor', 'id_modelo', 'fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['anio', 'id_version', 'carga', 'pasajeros', 'peso', 'lista', 'ws_inhabilitado', 'id_user_registro', 'id_user_actualizacion'], 'integer'],
            [['valor'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Version::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'anio' => $this->anio,
            'id_version' => $this->id_version,
            'valor' => $this->valor,
            'carga' => $this->carga,
            'pasajeros' => $this->pasajeros,
            'peso' => $this->peso,
            'lista' => $this->lista,
            'ws_inhabilitado' => $this->ws_inhabilitado,
            'fecha_registro' => $this->fecha_registro,
            'fecha_actualizacion' => $this->fecha_actualizacion,
            'id_user_registro' => $this->id_user_registro,
            'id_user_actualizacion' => $this->id_user_actualizacion,
        ]);

        $query->andFilterWhere(['like', 'id_modelo2', $this->id_modelo2])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'tipo', $this->tipo])
            ->andFilterWhere(['like', 'caja', $this->caja])
            ->andFilterWhere(['like', 'motor', $this->motor])
            ->andFilterWhere(['like', 'id_modelo', $this->id_modelo]);

        return $dataProvider;
    }
}
