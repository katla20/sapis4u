<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Zona;

/**
 * ZonaSearch represents the model behind the search form about `frontend\models\Zona`.
 */
class ZonaSearch extends Zona
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_zona', 'estatus', 'id_ciudad', 'id_user_registro', 'id_user_actualizacion'], 'integer'],
            [['nombre', 'fecha_registro', 'fecha_actualizacion', 'descripcion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Zona::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		$query->joinWith(['idCiudad']);

        $query->andFilterWhere([
            'id_zona' => $this->id_zona,
            'id_ciudad' => $this->id_ciudad,
            'fecha_registro' => $this->fecha_registro,
            'fecha_actualizacion' => $this->fecha_actualizacion,
            'id_user_registro' => $this->id_user_registro,
            'id_user_actualizacion' => $this->id_user_actualizacion,
			'estatus' => $this->estatus,
            
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}
