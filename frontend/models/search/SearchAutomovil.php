<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Automovil;

/**
 * SearchAutomovil represents the model behind the search form about `frontend\models\Automovil`.
 */
class SearchAutomovil extends Automovil
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_automovil', 'id_version', 'id_user_registro', 'id_user_actualizacion', 'id_tipo_vehiculo', 'id_uso'], 'integer'],
            [['placa', 'fecha_registro', 'fecha_actualizacion', 'color', 'serial_motor', 'serial_carroceria'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Automovil::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_automovil' => $this->id_automovil,
            'id_version' => $this->id_version,
            'fecha_registro' => $this->fecha_registro,
            'fecha_actualizacion' => $this->fecha_actualizacion,
            'id_user_registro' => $this->id_user_registro,
            'id_user_actualizacion' => $this->id_user_actualizacion,
            'id_tipo_vehiculo' => $this->id_tipo_vehiculo,
            'id_uso' => $this->id_uso,
        ]);

        $query->andFilterWhere(['like', 'placa', $this->placa])
            ->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'serial_motor', $this->serial_motor])
            ->andFilterWhere(['like', 'serial_carroceria', $this->serial_carroceria]);

        return $dataProvider;
    }
}
