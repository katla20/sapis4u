<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Ciudad;

/**
 * CiudadSearch represents the model behind the search form about `frontend\models\Ciudad`.
 */
class CiudadSearch extends Ciudad
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_ciudad', 'estatus', 'id_estado'], 'integer'],
            [['nombre', 'fecha_registro', 'fecha_actualizacion', 'ciudad_zona', 'descripcion', 'id_user_registro', 'id_user_actualizacion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ciudad::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		$query->joinWith(['idEstado']);

        $query->andFilterWhere([
            'id_ciudad' => $this->id_ciudad,
            'estado.id_estado' => $this->id_estado,
            'fecha_registro' => $this->fecha_registro,
            'fecha_actualizacion' => $this->fecha_actualizacion,
			'ciudad.estatus' =>  $this->estatus,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'ciudad_zona', $this->ciudad_zona])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'id_user_registro', $this->id_user_registro])
            ->andFilterWhere(['like', 'id_user_actualizacion', $this->id_user_actualizacion]);

        return $dataProvider;
    }
}
