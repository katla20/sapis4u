<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Productos;

/**
 * ProductosSearch represents the model behind the search form about `frontend\models\Productos`.
 */
class ProductosSearch extends Productos
{
	public $ramo;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_producto', 'id_ramo', 'estatus'], 'integer'],
            [['nombre','ramo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Productos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        

        if (!($this->load($params) && $this->validate())) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		$query->joinWith(['idRamo']);

        $query->andFilterWhere([
            'id_producto' => $this->id_producto,
            'id_ramo' => $this->id_ramo,
			'estatus' => $this->estatus,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre]);

        return $dataProvider;
    }
}
