<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Recibo;

/**
 * ReciboSearch represents the model behind the search form about `\frontend\models\Recibo`.
 */
class ReciboSearch extends Recibo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_recibo', 'nro_recibo', 'id_poliza', 'estatus', 'id_user_registro', 'id_user_actualizacion', 'id_subcontratante', 'id_vendedor', 'id_productor'], 'integer'],
            [['fecha_desde', 'fecha_hasta', 'fecha_registro', 'fecha_actualizacion', 'estatus_recibo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Recibo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_recibo' => $this->id_recibo,
            'nro_recibo' => $this->nro_recibo,
            'id_poliza' => $this->id_poliza,
            'fecha_desde' => $this->fecha_desde,
            'fecha_hasta' => $this->fecha_hasta,
            'estatus' => $this->estatus,
            'fecha_registro' => $this->fecha_registro,
            'fecha_actualizacion' => $this->fecha_actualizacion,
            'id_user_registro' => $this->id_user_registro,
            'id_user_actualizacion' => $this->id_user_actualizacion,
            'id_subcontratante' => $this->id_subcontratante,
            'id_vendedor' => $this->id_vendedor,
            'id_productor' => $this->id_productor,
        ]);

        $query->andFilterWhere(['like', 'estatus_recibo', $this->estatus_recibo]);

        return $dataProvider;
    }
}
