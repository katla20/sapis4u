<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ramos".
 *
 * @property integer $id_ramo
 * @property string $nombre
 *
 * @property InterAesgRamo[] $interAesgRamos
 * @property Productos[] $productos
 */
class Ramos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ramos';
    }
    
    //public $id_ramo;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
			[['estatus',], 'integer'],
            [['nombre'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_ramo' => Yii::t('app', 'Id Ramo'),
            'nombre' => Yii::t('app', 'Nombre'),
			'estaus' => Yii::t('app', 'Estatus'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterAesgRamos()
    {
        return $this->hasMany(InterAsegRamo::className(), ['id_ramo' => 'id_ramo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(Productos::className(), ['id_ramo' => 'id_ramo']);
    }
}
