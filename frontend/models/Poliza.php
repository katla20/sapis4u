<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "poliza".
 *
 * @property integer $id_poliza
 * @property string $numero_poliza
 * @property string $fecha_vigenciadesde
 * @property string $fecha_vigenciahasta
 * @property integer $estatus
 * @property integer $id_tipo
 * @property integer $id_contratante
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property integer $id_aseguradora
 * @property string $estatus_poliza
 * @property integer $id_ramo
 *
 * @property Aseguradoras $idAseguradora
 * @property Cliente $idContratante
 * @property Recibo[] $recibos
 */
class Poliza extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public $tipo_recibo,$nro_recibo,$producto,$id_automovil,$id_vendedor,$id_beneficiario,
           $id_contratante_hidden,$id_vendedor_hidden,$id_automovil_hidden,$suma,$hi,$nro_certificado,
           $comision_total,$comision_vendedor,$comision_porcentaje,$tipo_vendedor,$asegurados,$beneficiarios,$deducible;
    public static function tableName()
    {
        return 'poliza';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_aseguradora','numero_poliza','nro_recibo','tipo_recibo' ,'fecha_vigenciadesde', 'fecha_vigenciahasta','producto','nro_certificado'], 'required'],
            [['id_contratante_hidden'], 'required','message'=>'El contratante no puede estar vacío'],
            [['id_automovil_hidden'], 'required','message'=>'El Automovil no puede estar vacío'],
            [['estatus', 'id_tipo', 'id_contratante', 'id_user_registro', 'id_user_actualizacion', 'id_aseguradora'], 'integer'],
            [['fecha_vigenciadesde', 'fecha_vigenciahasta', 'fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['estatus_poliza'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_poliza' => Yii::t('app', 'Poliza'),
            'numero_poliza' => Yii::t('app', 'Numero Poliza'),
            'fecha_vigenciadesde' => Yii::t('app', 'Fecha Vigenciadesde'),
            'fecha_vigenciahasta' => Yii::t('app', 'Fecha Vigenciahasta'),
            'estatus' => Yii::t('app', 'Estatus'),
            'id_tipo' => Yii::t('app', 'Tipo'),
            'id_contratante' => Yii::t('app', 'Contratante'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Usuario Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Usuario Actualizacion'),
            'id_aseguradora' => Yii::t('app', 'Aseguradora'),
            'estatus_poliza' => Yii::t('app', 'Estatus'),
            'nro_recibo' => Yii::t('app', 'Numero de Recibo'),
      			'tipo_vendedor'=> Yii::t('app', 'Seleccione en caso de ..'),
      			'id_vendedor'=> Yii::t('app', 'Vendedor(solo si aplica)'),
      			'id_automovil'=>Yii::t('app', 'Placa'),
      			'producto'=>Yii::t('app', 'Producto (necesario para mostrar las coberturas de la poliza)'),
            'deducible'=>Yii::t('app', 'Deducible'),
            'id_beneficiario'=>Yii::t('app', 'Beneficiario'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAseguradora()
    {
        return $this->hasOne(Aseguradoras::className(), ['id_aseguradora' => 'id_aseguradora']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdContratante()
    {
        return $this->hasOne(Cliente::className(), ['id_cliente' => 'id_contratante']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProducto()
    {
        return $this->hasOne(Productos::className(), ['id_producto' => 'id_producto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecibos()
    {
        return $this->hasMany(Recibo::className(), ['id_poliza' => 'id_poliza']);
    }
}
