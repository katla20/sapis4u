<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "marcas".
 *
 * @property string $id_marca
 * @property string $nombre
 * @property integer $ws_inhabilitado
 * @property string $fecha_registro
 * @property string $fecha_modificacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 *
 * @property Modelos[] $modelos
 */
class Marcas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'marcas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_marca'], 'required'],
            [['id_marca', 'nombre'], 'string'],
            [['ws_inhabilitado', 'id_user_registro', 'id_user_actualizacion'], 'integer'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_marca' => Yii::t('app', 'Id Marca'),
            'nombre' => Yii::t('app', 'Nombre'),
            'ws_inhabilitado' => Yii::t('app', 'Ws Inhabilitado'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Modificacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModelos()
    {
        return $this->hasMany(Modelos::className(), ['id_marca' => 'id_marca']);
    }
}
