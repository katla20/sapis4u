<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "estado".
 *
 * @property integer $id_estado
 * @property integer $id_pais
 * @property string $estatus
 * @property string $nombre
 * @property string $region
 * @property string $fecha_registro
 * @property string $fecha_modificacion
 * @property string $descripcion
 *
 * @property Ciudad[] $ciudads
 * @property Pais $idPais
 */
class Estado extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estado';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pais', 'estatus', 'nombre', 'region', ], 'required'],
            [['id_pais'], 'integer'],
            [['nombre', 'region', 'descripcion'], 'string'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_estado' => Yii::t('app', 'Id Estado'),
            'id_pais' => Yii::t('app', 'Id Pais'),
            'estatus' => Yii::t('app', 'Estatus'),
            'nombre' => Yii::t('app', 'Nombre'),
            'region' => Yii::t('app', 'Region'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Modificacion'),
            'descripcion' => Yii::t('app', 'Descripcion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCiudads()
    {
        return $this->hasMany(Ciudad::className(), ['id_estado' => 'id_estado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPais()
    {
        return $this->hasOne(Pais::className(), ['id_pais' => 'id_pais']);
    }
}
