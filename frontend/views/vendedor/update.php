<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\persona */

$this->title = Yii::t('app', 'Update {modelClass}: ', ['modelClass' => 'Persona']) . ' ' . $model->id_persona;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vendedor'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_persona, 'url' => ['view', 'id' => $model->id_persona]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="vendedor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelDireccion' => (empty($modelDireccion)) ? [new Direccion] : $modelDireccion,
        'modelCliente' => (empty($modelCliente)) ? [new Cliente] : $modelCliente,
        'modelCargaFamiliar' => (empty($modelCargaFamiliar)) ? [new CargaFamiliar] : $modelCargaFamiliar,

    ]) ?>

</div>
