<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use frontend\models\Pais;
use frontend\models\Estado;
use frontend\models\Ciudad;
use frontend\models\Zona;
use kartik\select2\Select2;
use kartik\depdrop\Depdrop; //http://demos.krajee.com/widget-details/depdrop
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;


//use kartik\widgets\ActiveForm;
//http://formvalidation.io/examples/

/* @var $this yii\web\View */
/* @var $model frontend\models\persona */
/* @var $form yii\widgets\ActiveForm */

//$form = ActiveForm::begin(['id' => 'dynamic-form','options' => ['enctype' => 'multipart/form-data']]);
//https://github.com/sintret/yii2-chat-adminlte/blob/master/views/index.php
?>


  <?php $form = ActiveForm::begin([
    'id' => 'vendedor-form',
    'enableAjaxValidation' => true,
    'enableClientScript' => true,
    'enableClientValidation' => true,
    ]); ?>

  <div class="vendedor-form">
  <div class="box box-default">
  <div class="box-header with-border">
    <h4 class="box-title"><i class="glyphicon glyphicon-user"></i> Datos Personales</h4>
      <div class="row">
            <div class="col-md-4"><?=$form->field($model, 'tipo')->inline()->radioList(array('V'=>'Natural','J'=>'Juridico'))->label('')?></div>
            <div class="col-md-4"><?=$form->field($model, 'identificacion')->widget(\yii\widgets\MaskedInput::className(), ['mask' => 'V-99999999-9'])?></div>
     </div>


    <div class="box-tools pull-right">
      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div><!-- /.box-tools -->
  </div><!-- /.box-header -->
  <div class="box-body">
	 <div class="container-items"><!-- widgetBody -->

        <div class="row">
					  <div class="col-sm-6">
					  <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
					  </div>
					  <div class="col-sm-6">
						  <?= $form->field($model, 'segundo_nombre')->textInput(['maxlength' => true]) ?>
					  </div>
        </div>
        <div class="row">
					  <div class="col-sm-6">
						  <?= $form->field($model, 'apellido')->textInput(['maxlength' => true]) ?>
					  </div>
					  <div class="col-sm-6">
						  <?= $form->field($model, 'segundo_apellido')->textInput(['maxlength' => true]) ?>
					  </div>
        </div>
          <div class="row">
					  <div class="col-sm-6">
						  <?=$form->field($model, 'fecha_nacimiento')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '99/99/9999']) ?>
					  </div>
					  <div class="col-sm-6">
						  <?= $form->field($model, 'sexo')->inline()->radioList(array('F'=>'Femenino','M'=>'Masculino','O'=>'Otro'))->label('Sexo'); ?>
					  </div>
          </div>
          <div class="row">
					  <div class="col-sm-6">
							<?= $form->field($model, 'correo')->textInput(['maxlength' => true]) ?>
					   </div>
          </div><!--container-items-->
          <div class="row">
					  <div class="col-sm-4">
							<?= $form->field($model, 'codigo')->textInput(['maxlength' => true]) ?>
					   </div>
             <div class="col-sm-4">
 							<?= $form->field($model, 'comision')->textInput(['maxlength' => true]) ?>
 					   </div>
          </div>

  </div><!-- /.box-body -->
</div><!-- /.box -->

    <?php DynamicFormWidget::begin([
       'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
       'widgetBody' => '.container-items', // required: css class selector
       'widgetItem' => '.item', // required: css class
       'limit' => 4, // the maximum times, an element can be added (default 999)
       'min' => 0, // 0 or 1 (default 1)
       'insertButton' => '.add-item', // css class
       'deleteButton' => '.remove-item', // css class
       'model' => $modelDireccion[0],
       'formId' => 'dynamic-form',
       'formFields' => [
           'nombre_direccion',
           'id_zona',
           'calle_avenida',
           'nro',
           'sector',
           'telefono',
           'telefono2',
           'telefono3'
       ],
     ]);?>

         <div class="box box-default">
         <div class="box-header with-border">
             <h4>
                 <i class="glyphicon glyphicon-envelope"></i> Direcciones
                 <button type="button" class="add-item btn btn-success btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i>Agregar</button>
             </h4>

         </div>
         <div class="box-body">
             <div class="container-items"><!-- widgetBody -->
             <?php foreach ($modelDireccion as $i => $modelDireccion): ?>
                 <div class="item panel panel-default"><!-- widgetItem -->
                     <div class="panel-heading">
                         <h5 class="panel-title pull-left">Direccion</h5>
                         <div class="pull-right">
                             <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                         </div>
                         <div class="clearfix"></div>
                     </div>
                     <div class="panel-body">
                         <?php
                             // necessary for update action.
                             if (! $modelDireccion->isNewRecord) {
                               //http://stackoverflow.com/questions/28925624/yii2kartik-depdrop-widget-default-value-on-update
                                 echo Html::activeHiddenInput($modelDireccion, "[{$i}]id");

								// echo CHtml::hiddenField('name' , 'value', array('id' => 'hiddenInput'));[$i]
                                 //ADDITIONAL PARAM ID YOU MAY USE TO SELECT A DEFAULT VALUE OF YOUR MODEL IN YOUR DEPDROP WHEN YOU WANT TO UPDATE:
                                  echo Html::hiddenInput('model_idEstado', $modelDireccion->id, ['id'=>'model_idEstado']);
								  // echo Html::hiddenInput('model_idEstado',"[$i]id", ['id'=>'model_idEstado']);
                             }
                         ?>
                      <div class="row">

						  <div class="col-sm-6">

                         <?=$form->field($modelDireccion,"[{$i}]nombre_direccion")->widget(Select2::classname(), [
                              'data' =>['Direccion Habitacion'=>'Direccion Habitacion','Direccion Trabajo'=>'Direccion Trabajo','Direccion Cobro'=>'Direccion Cobro'],
                              'language' => 'en',
                               'options' => ['placeholder' => 'Select a state ...'],
                              'pluginOptions' => [
                                  'allowClear' => true
                              ],
                            ]);
                          ?>
                        </div>
                      </div>
                        <div class="row">
                             <div class="col-sm-6">
                               <?php
								   echo $form->field($modelDireccion,"[{$i}]id_estado")->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map(Estado::find()->where(['id_pais' => '10'])->all(), 'id_estado', 'nombre'),
                                    'language' => 'en',
                                     'options' => ['placeholder' => 'Select a state ...'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                  ]);

                                ?>
                             </div>
							 <div class="col-sm-6">
								<?=$form->field($modelDireccion, "[{$i}]id_ciudad")->widget(DepDrop::classname(), [
										'data'=> [''=>'Seleccione '],
										'options'=>['placeholder'=>'Selecione ...'],
										'type' => DepDrop::TYPE_SELECT2,
										'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
										'pluginOptions'=>[
										 'depends'=>["direccion-$i-id_estado"],
										 'placeholder' => 'Seleccione ...',
										 'url'=>Url::to(['dependent-dropdown/child-account', 'db'=>Ciudad::classname(),'cmpo_dep'=>'id_estado','cmpo_mostrar'=>'nombre','id_cmpo'=>'id_ciudad']),
										 'loadingText' => 'Leyendo ...',
									   ]
									   ]);

								 ?>
							 </div>
						</div>
						<div class="row">

							 <div class="col-sm-6">

								<?=$form->field($modelDireccion, "[{$i}]id_zona")->widget(DepDrop::classname(), [
										'data'=> [''=>'Seleccione '],
										'options'=>['placeholder'=>'Selecione ...'],
										'type' => DepDrop::TYPE_SELECT2,
										'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
										'pluginOptions'=>[
										 'initialize'=>true,
										 'depends'=>["direccion-$i-id_ciudad"],
										 'placeholder' => 'Seleccione ...',
										 'url'=>Url::to(['dependent-dropdown/child-account', 'db'=>Zona::classname(),'cmpo_dep'=>'id_ciudad','cmpo_mostrar'=>'nombre','id_cmpo'=>'id_zona']),
										 'loadingText' => 'Leyendo ...',
										 'params'=>['model_id1'] ///SPECIFYING THE PARAM
									   ]
									   ]);
								 ?>
							 </div>


                          <div class="col-sm-6">
                                <?= $form->field($modelDireccion, "[{$i}]calle_avenida")->textInput(['maxlength' => true]) ?>
                          </div>
						</div>
                         <div class="row">
                             <div class="col-sm-6">
                                 <?= $form->field($modelDireccion, "[{$i}]nro")->textInput(['maxlength' => true]) ?>
                             </div>
                             <div class="col-sm-6">
                                 <?= $form->field($modelDireccion, "[{$i}]sector")->textInput(['maxlength' => true]) ?>
                             </div>
                         </div><!-- .row -->
                         <div class="row">
                             <div class="col-sm-4">
                                   <?= $form->field($modelDireccion, "[{$i}]telefono")->textInput(['maxlength' => true]) ?>
                             </div>
                             <div class="col-sm-4">
                                 <?= $form->field($modelDireccion, "[{$i}]telefono2")->textInput(['maxlength' => true]) ?>
                             </div>
                             <div class="col-sm-4">
                                 <?= $form->field($modelDireccion, "[{$i}]telefono3")->textInput(['maxlength' => true]) ?>

                             </div>
                         </div><!-- .row -->
                     </div>
                 </div>
               <?php endforeach; ?>
           </div><!--container-items-->
           </div><!--panel body-->
     </div><!-- .panel -->
     <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
     <?php DynamicFormWidget::end(); ?>
 <?php ActiveForm::end(); ?>

 </div>
</div>

<?php

$this->registerJs('
  // obtener la id del formulario y establecer el manejador de eventos
      $("form#vendedor-form").on("beforeSubmit", function(e) {
          var form = $(this);
          var params="";
          if(form.attr("action")=="/yii_t_final/vendedor/create"){
                params="?submit=true";
          }else{
              params="&submit=true";
          }
          $.post(
              form.attr("action")+params,form.serialize()
          )
          .done(function(result) {
              form.parent().html(result.message);
              $.pjax.reload({container:"#w0"});
              var container=$("#datosvendedor");
              if ( typeof container !== "undefined" && container) {//solo si esta definido
                $("#datosvendedor").html("<pre >Nombre Completo: "+result.nombre_completo+"; Numero de Identificacion "+result.identificacion+"</pre>");
                $("#poliza-id_vendedor_hidden").val(result.id_vendedor);
              }

          });
          return false;
      }).on("submit", function(e){
          e.preventDefault();
          e.stopImmediatePropagation();
          return false;
      });
  ');
?>

<?php
$script=<<< JS
//script para llenado de las direeciones

	$(".dynamicform_wrapper").on("beforeInsert", function(e, item) {
		console.log("beforeInsert");

		});

		$(".dynamicform_wrapper").on("afterInsert", function(e, item) {
		console.log("afterInsert");
  });

		$(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
		if (! confirm("Are you sure you want to delete this item?")) {
			return false;
		}
		return true;
	    });

		$(".dynamicform_wrapper").on("afterDelete", function(e) {
		console.log("Deleted item!");
		});

		  $(".dynamicform_wrapper").on("limitReached", function(e, item) {
		    alert("Limit reached");
	    });

JS;
$this->registerJs($script, \yii\web\View::POS_READY);
$this->registerCss(".identificacion{display:none;}");
?>
