<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\persona */

/*$this->title = Yii::t('app', 'Crear Cliente');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cliente'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="vendedor-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelDireccion' => (empty($modelDireccion)) ? [new Direccion] : $modelDireccion,
        'modelIntermediario' => (empty($modelIntermediario)) ? [new Intermediario] : $modelIntermediario,
    ]) ?>

</div>
