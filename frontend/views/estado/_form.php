<?php


/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json; //keyla bullon
use frontend\models\Pais;
use kartik\select2\Select2;//keyla bullon
use kartik\depdrop\DepDrop;//http://demos.krajee.com/widget-details/depdrop
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $model frontend\models\Estado */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="estado-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php

            $id_pais = ArrayHelper::map(Pais::find()->all(), 'id_pais', 'nombre');

            echo $form->field($model, 'id_pais')->widget(Select2::classname(), [
            'data' => $id_pais,
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione una Pais ...'],
            'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
	?>

    <?= $form->field($model, 'nombre')->textarea(['rows' => 6]) ?>
    
	<?php
    echo $form->field($model, 'estatus')->dropDownList([1 => 'Activo', 0 => 'Inactivo']);
	?>
    

    <?= $form->field($model, 'region')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
