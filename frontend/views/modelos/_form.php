<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json; //keyla bullon
use frontend\models\Marcas;
use kartik\select2\Select2;//keyla bullon
use kartik\depdrop\DepDrop;//http://demos.krajee.com/widget-details/depdrop
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model frontend\models\Modelos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modelos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_modelo')->textInput(['maxlength' => true]) ?>

    <?//= //$form->field($model, 'id_marca')->textInput(['maxlength' => true]) ?>
	<?php

            $id_marca = ArrayHelper::map(Marcas::find()->all(), 'id_marca', 'nombre');

            echo $form->field($model, 'id_marca')->widget(Select2::classname(), [
            'data' => $id_marca,
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione una Marca ...'],
            'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
	?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
