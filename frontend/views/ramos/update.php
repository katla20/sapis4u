<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Ramos */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Ramos',
]) . ' ' . $model->id_ramo;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ramos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_ramo, 'url' => ['view', 'id' => $model->id_ramo]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ramos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
