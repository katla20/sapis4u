<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use kartik\icons\Icon;
use yii\web\Response;
use kartik\checkbox\CheckboxX;
use kartik\money\MaskMoney;
use frontend\assets\LocateAsset;
LocateAsset::register($this);
/* @var $this yii\web\View */
/* @var $model frontend\models\Poliza */

/* SELECT aseguradoras.nombre AS nombre_aseguradora,
    aseguradoras.id_aseguradora,
    productos.nombre AS nombre_producto,
    productos.id_producto,
    coberturas.id_cobertura,
    coberturas.nombre AS nombre_cobertura
   FROM aseguradoras
     JOIN inter_aseg_producto USING (id_aseguradora)
     JOIN productos USING (id_producto)
     JOIN inter_prod_cob USING (id_producto)
     JOIN coberturas USING (id_cobertura)
  WHERE aseguradoras.id_aseguradora = 3 AND productos.id_producto = 1;
  -----------------------------------------------------------
  SELECT * FROM
    coberturas_view
WHERE id_aseguradora=3 AND id_producto=1
  */
?>


<?=$form->field($model, 'hi')->widget(CheckboxX::classname());?>
<?=$form->field($model, 'suma')->widget(MaskMoney::classname());?>

<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
-moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
<div class="box-header with-border">
     <h4 class="box-title"><?=Icon::show('dashboard',[],Icon::BSG)?> Coberturas</h4>
</div><!--box-header --->
<div class="box-body">
    <div class="container-items"><!-- widgetBody -->
          <div class="row"><!--row--->
          <div class="col-sm-10"><!--col-sm-12--->
                    <div class="table-responsive" id="contendor_coberturas">
                       <table class="table table-striped" id="tabla_coberturas" url_action="<?=Url::to(['poliza/buscar-coberturas'])?>">
                            <thead><tr><th>#</th><th>Cobertura</th><th>Suma Asegurada</th><th>Tasa(%)</th><th>Prima</th></tr></thead>
                            <tbody>
                              <tr style="text-align:center">
                              <th colspan="5">
                                <div class="callout callout-info lead">
                                  <h4>No ha Seleccionado Ningun producto!</h4>
                               </div>
                             </th>
                           </tr>
                         </tbody>
                       </table>
                  </div>
          </div><!--col-sm-12--->
          <div class="col-sm-2"><!--col-sm-2--->
          </br>
              <?php  echo $form->field($model, 'deducible',[
                                                        'addon' => [
                                                            'prepend' => [
                                                                'content' => 'Bs.',

                                                            ]
                                                        ]
                                                    ])->widget(MaskMoney::classname(), [
                                                                                        'pluginOptions' => [
                                                                                            'prefix' => '',
                                                                                            'suffix' => '',
                                                                                            'allowNegative' => false
                                                                                          ]
                                                                                       ]);?>

          </div><!--col-sm-2--->
        </div><!--row--->
    </div><!--container-items-->
</div><!--box-body-->
</div><!--box-->
<?php
$script=<<< JS

$("select#poliza-aseguradora").on("change", function(e) {
  if($("select#poliza-producto").val()==""){
    $("#tabla_coberturas").empty();
    $("#tabla_coberturas").html('<tr><th colspan="4" style="color:red;">Debe seleccionar un producto</th></tr>');
  }

});
$("select#poliza-producto").on("change", function(e) {
   var producto = $(this).val();
   var aseguradora=$('select#poliza-id_aseguradora').val();
   var url= $("#tabla_coberturas").attr("url_action");
   var out="";
     console.log(aseguradora+'--'+producto);
     if(producto!='...' || producto!='  ' || producto!='' ){
       $.post(url, { aseguradora: aseguradora, producto: producto })
       .done(function(result) {
         if(result==null){
                $("#tabla_coberturas").empty();
                 out+='<tr><th colspan="5"><div class="callout callout-info lead"><h4>No ha Seleccionado Ningun producto!</h4></div></th></tr>';
	        }else{
                $("#tabla_coberturas").empty();
	              var obj = eval(result);
                out+='<thead><tr><th>#</th><th>Cobertura</th><th>Suma Asegurada</th><th>Tasa(%)</th><th>Prima</th></tr></thead>';
	              $.each(obj, function(key, value) {
                  out+='<tbody><tr><td>';
                  out+='<input type="checkbox" id="cobBox'+key+'" name="cobBox['+key+']" class="checkX" data-cob="'+key+'"></td>';
                  out+='<td><label> '+value+' </label></td>';
                  out+='<td><div class="input-group"><span class="input-group-addon">Bs.</span>';
                  out+='<input type="text" name="suma['+key+']" class="form-control" id="suma_'+key+'" class="checkboxes" disabled ></div></td>';
                  out+='<td><div class="input-group"><span class="input-group-addon">%</span>';
                  out+='<input type="text" name="tasa['+key+']" class="form-control" id="tasa_'+key+'" maxlength="5"  class="checkboxes" disabled ></div></td>';
                  out+='<td><div class="input-group"><span class="input-group-addon">%</span>';
                  out+='<input type="text" name="prima['+key+']" class="form-control" id="prima_'+key+'" class="checkboxes" disabled ></div></td>';
                  out+='</tr></tbody>';
	              });

                $("#tabla_coberturas").html(out);
                $(".field-poliza-deducible").show();
                $("input[name*='cobBox'").checkboxX({threeState: false, size:'md'});
                $("input[id*='suma_'],input[id*='tasa_'],input[id*='prima_']").maskMoney({thousands:'', decimal:'.'});

	         }

        //$("#tabla_coberturas").html(result.message);

       });//fin post-done

     }//fin if

     // with plugin options


    return false;
});

var checkboxValues = "";
var suma=0;

$(".field-poliza-hi,.field-poliza-suma,.field-poliza-deducible").hide();

$('#tabla_coberturas').on('change', ".checkX", function(event) {
    // Do something on click on an existent or future .dynamicElement
    var cob = $(this).data("cob");
    //  event.preventDefault();
    if(this.checked){
       $("input[name*='["+cob+"']").removeAttr('disabled');
    }else{
      $("input[name*='["+cob+"']").val("");
      $("input[name*='["+cob+"']").attr('disabled','disabled');
    }
     //checkboxValues += this.id +",";
     //checkboxValues = checkboxValues.substring(0, checkboxValues.length-1);
     //console.log(checkboxValues);
});

$('#tabla_coberturas').on('keyup', "input[id*='tasa_']", function(event) {
    // Do something on click on an existent or future .dynamicElement
      event.preventDefault();
    var id_tasa=this.id;
    var id= id_tasa.substring(5);
    var  suma=$("input[id*='suma_"+id+"']").val();
    $("input[id*='prima_"+id+"']").val(suma*(this.value/100));
});

 //$('input[name="cobBox[]"]').serializeArray(); //array json los objetos del arreglo

JS;
$this->registerJs($script);
$this->registerCss(".btn span.glyphicon {
                    	opacity: 0;
                    }
                    .btn.active span.glyphicon {
                    	opacity: 1;
                    }

                    /* CSS REQUIRED */
                  .state-icon {
                      left: -5px;
                  }
                  .list-group-item-primary {
                      color: rgb(255, 255, 255);
                      background-color: rgb(66, 139, 202);
                  }

                  /* DEMO ONLY - REMOVES UNWANTED MARGIN */
                  .well .list-group {
                      margin-bottom: 0px;
                  }");
?>
