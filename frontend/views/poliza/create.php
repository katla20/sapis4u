<?php

use yii\helpers\Html;

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;
use kartik\icons\Icon;

Icon::map($this);

/* @var $this yii\web\View */
/* @var $model frontend\models\Poliza */


$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Polizas'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
$this->title="";


?>

<?php

/*echo Tabs::widget([
    'items' => [
        [
            'label' => 'Tab one',
            'content' => 'Mauris mauris ante, blandit et, ultrices a, suscipit eget...',
        ],
        [
            'label' => 'Tab two',
            'content' => 'Sed non urna. Phasellus eu ligula. Vestibulum sit amet purus...',
            'options' => ['tag' => 'div'],
            'headerOptions' => ['class' => 'my-class'],
        ],
        [
            'label' => 'Tab with custom id',
            'content' => 'Morbi tincidunt, dui sit amet facilisis feugiat...',
            'options' => ['id' => 'my-tab'],
        ],
        [
            'label' => 'Ajax tab',
            'url' => ['ajax/content'],
        ],
    ],
    'options' => ['tag' => 'div'],
    'itemOptions' => ['tag' => 'div'],
    'headerOptions' => ['class' => 'my-class'],
    'clientOptions' => ['collapsible' => false],
]);*/

?>
</br>
<div class="k-tabstrip-wrapper">
  <div class="k-widget k-tabstrip k-header multicolored-tabs" id="tabStripProject">

  <ul class="k-reset k-tabstrip-items">

    <li class="k-item k-state-default k-state-active" id="tabMainInfos" title="General information">
      <a class="k-link" href="#tabStripProject-1"><?php echo Icon::show('car', ['class' => 'fa-3x']);?></a>
      <span style="color:white;font-size:18px;font-weight:bold;"><?=$this->title = Yii::t('app', 'Crear Poliza Automovil Individual');?></span>
    </li>
    <!--<li class="k-item k-state-default" id="tabMembers" title="Members">
      <a class="k-link"><?php echo Icon::show('car', ['class' => 'fa-3x']);?></a>
    </li>
    <li class="k-item k-state-default" id="tabTasks" title="Tasks">
      <a class="k-link"><?php echo Icon::show('users', ['class' => 'fa-3x'], Icon::FA);?></a>
    </li>
    <li class="k-item k-state-default" id="tabBoards" title="Boards">
      <a class="k-link"><?php echo Icon::show('tachometer', ['class' => 'fa-3x'], Icon::FA);?></a>
    </li>
    <li class="k-item k-state-default" id="tabWorkedHours" title="Time entries">
      <a class="k-link"><?php echo Icon::show('info', ['class' => 'fa-3x']);?></a>
    </li>
    <li class="k-item k-state-default" id="tabExpenses" title="Expenses">
      <a class="k-link"><?php echo Icon::show('info', ['class' => 'fa-3x']);?></a>
    </li>
    <li class="k-item k-state-default" id="tabEstimates" title="Estimates">
      <a class="k-link"><?php echo Icon::show('info', ['class' => 'fa-3x']);?></a>
    </li>
    <li class="k-item k-state-default" id="tabInvoices" title="Invoices">
      <a class="k-link"><?php echo Icon::show('info', ['class' => 'fa-3x']);?></a>
    </li>
    <li class="k-item k-state-default" id="tabDashboard" title="Dashboards">
      <a class="k-link"><?php echo Icon::show('info', ['class' => 'fa-3x']);?></a>
    </li>
    <li class="k-item k-state-default" id="tabAttachments" title="Attachments">
      <a class="k-link"><?php echo Icon::show('info', ['class' => 'fa-3x']);?></a>
    </li>-->
  </ul>
  <!--<div class="k-content k-state-active" id="tabStripProject-1" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
  -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
  box-shadow: 0px 2px 8px -1px rgba(143,141,143,1); background-color:white;display:block;"><h1>holaaaaa</h1></div>
</div>-->
</div>

<!--  k-state-active  k-state-default-->
<!--http://www.cssmatic.com/es/box-shadow-->
<!-- normal -webkit-box-shadow: -1px 4px 15px 1px rgba(71,66,71,0.61);
-moz-box-shadow: -1px 4px 15px 1px rgba(71,66,71,0.61);
box-shadow: -1px 4px 15px 1px rgba(71,66,71,0.61);-->
<!--  con el evento onblur
-webkit-box-shadow: 0px 2px 24px -1px rgba(143,141,143,1);
-moz-box-shadow: 0px 2px 24px -1px rgba(143,141,143,1);
box-shadow: 0px 2px 24px -1px rgba(143,141,143,1);


-->


<div class="poliza-create">

   <?=$this->title=""?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>



</div>
