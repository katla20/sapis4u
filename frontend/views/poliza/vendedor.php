<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\Response;
use kartik\select2\Select2;
use kartik\depdrop\Depdrop; //http://demos.krajee.com/widget-details/depdrop
use wbraganca\dynamicform\DynamicFormWidget;
use yii\widgets\MaskedInput;
use kartik\icons\Icon;
use yii\widgets\Pjax;
use kartik\checkbox\CheckboxX;
use kartik\money\MaskMoney;
use frontend\models\Aseguradoras;
use frontend\models\InterAsegRamo;
use frontend\models\InterAsegProducto;
use frontend\models\Productos;
use frontend\models\Cliente;
use frontend\models\Persona;
use frontend\models\Intermediario;



/* @var $this yii\web\View */
/* @var $model frontend\models\Poliza */
?>

<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
  -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
  box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
<div class="box-header with-border">
       <h4 class="box-title"><?=Icon::show('car')?> Comisiones y Vendedor</h4>
</div><!--box-header --->
<div class="box-body"><!--box-body --->
    <div class="container-items"><!-- widgetBody -->
        <div class="row">
        		<div class="col-sm-3">
        		<?php  echo $form->field($model, 'comision_total',[
        												  'addon' => [
        													  'prepend' => [
        														  'content' => 'Bs.',

        													  ]
        												  ]
        											  ])->widget(MaskMoney::classname(), [
        																				  'pluginOptions' => [
        																					  'prefix' => '',
        																					  'suffix' => '',
        																					  'allowNegative' => false
        																					]
        																				 ]);?>

        		</div>
      	</div>
    	 <div class="row"><!--row-->
            <div class="col-sm-4"><!--col-sm-4-->
            <?=$form->field($model,'id_vendedor', [
                                                          'addon' => [
                                                              'prepend' => ['content'=>'<i class="glyphicon glyphicon-user"></i>'],
                                                              'append'=>['content'=>Html::a('Agregar', '#', [
                                                                                             'id' => 'activity-index-link',
                                                                                             'class' => 'btn btn-success',
                                                                                             'data-toggle' => 'modal',
                                                                                             'data-target' => '#modal',
                                                                                             'data-url' => Url::to(['vendedor/create'], true),
                                                                                             'data-pjax' => '0',
                                                                                         ]), 'asButton'=>true]
                                                          ]
                                   ])->widget(Select2::classname(), [
                                               'data' =>   ArrayHelper::map( Intermediario::find()
                                                                            ->joinwith('idPersona')
                                                                            ->where("intermediario.estatus = :status AND id_tipo_intermediario=2", [':status' => 1])
                                                                            ->select("(nombre||' '||segundo_nombre||' '||apellido|| ' ' ||segundo_apellido || ' ' || identificacion) AS nombre_completo ,id_intermediario")
                                                                            ->all(),'id_intermediario', 'nombre_completo'),
                                               'language' => 'en',
                                                'options' => ['placeholder' => 'Seleccione ...','id'=>'id_vendedor'],
                                                'pluginOptions' => [
                                                   'allowClear' => true
                                               ],
                               ]);
             ?>
            </div>
            <?=$form->field($model, 'id_vendedor_hidden')->hiddenInput()->label(false);?>
        		<div class="col-sm-2"><!--col-sm-2-->
        		 	 <?php  echo $form->field($model, 'comision_porcentaje',[
        												  'addon' => [
        													  'prepend' => [
        														  'content' => '%',

        													  ]
        												  ]
        											  ])->textInput(['readonly' => true]) ;?>
        		</div><!--col-sm-2-->
        		<div class="col-sm-2"><!--col-sm-2-->
        		 <?php  echo $form->field($model, 'comision_vendedor',[
        												  'addon' => [
        													  'prepend' => [
        														  'content' => 'Bs.',

        													  ]
        												  ]
        											  ])->textInput(['readonly' => true]);?>

        		</div><!--col-sm-2-->
       </div><!--row-->
       <div id="datosvendedor"></div></div>
   </div><!-- widgetBody -->
</div><!--box-body --->
</div><!--box--->
