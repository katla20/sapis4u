<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\PartesVehiculos */

$this->title = Yii::t('app', 'Create Partes Vehiculos');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Partes Vehiculos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partes-vehiculos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
