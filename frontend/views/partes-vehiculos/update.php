<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\PartesVehiculos */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Partes Vehiculos',
]) . ' ' . $model->id_parte;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Partes Vehiculos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_parte, 'url' => ['view', 'id' => $model->id_parte]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="partes-vehiculos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
