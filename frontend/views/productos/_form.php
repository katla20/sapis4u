<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\Ramos;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\models\Productos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productos-form">

  <?php $form = ActiveForm::begin([
        'id' => 'producto-form',
        'enableAjaxValidation' => true,
        'enableClientScript' => true,
        'enableClientValidation' => true,
   ]);

   ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?php
		$id_ramo = ArrayHelper::map(Ramos::find()->all(), 'id_ramo', 'nombre');
        //print_r($id_ramo);
		echo $form->field($model, 'id_ramo')->dropDownList($id_ramo,
		[
			'prompt'=>'Seleccione el Ramo',
			'.done(function( data ) {

									$( "#'.Html::getInputId($model, 'id_ramo').'" ).html( data );
								}
							);
						'

		]);

	?>

	<?php
    echo $form->field($model, 'estatus')->dropDownList([1 => 'Activo', 0 => 'Inactivo']);
	?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

$script=<<< JS
//script para llenado de las direeciones

$("form#producto-form").on("beforeSubmit", function(e) {
    var form = $(this);
    var params="";

  //  alert(form.attr("action"));

    if(form.attr("action")=="/yii_t/productos/create"){
            params="?submit=true";
    }else{
           params="&submit=true";
    }

    console.log(params);

    $.post(
        form.attr("action")+params,form.serialize()
    )
    .done(function(result) {
        form.parent().html(result.message);
        $.pjax.reload({container:"#w0"});
    });
    return false;
}).on("submit", function(e){
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});

JS;
$this->registerJs($script, \yii\web\View::POS_READY);
?>
