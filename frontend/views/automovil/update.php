<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Automovil */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Automovil',
]) . ' ' . $model->id_automovil;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Automovils'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_automovil, 'url' => ['view', 'id' => $model->id_automovil]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="automovil-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'documentos'=>$documentos,
    ]) ?>

</div>
