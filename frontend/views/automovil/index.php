<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\SearchAutomovil */
/* @var $dataProvider yii\data\ActiveDataProvider */
/*https://github.com/kartik-v/yii2-widget-select2/issues/86 */

$this->title = Yii::t('app', 'Automoviles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="automovil-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Automovil'), '#', [
            'id' => 'activity-index-link',
            'class' => 'btn btn-success',
            'data-toggle' => 'modal',
            'data-target' => '#modal',
            'data-url' => Url::to(['create']),
            'data-pjax' => '0',
        ]); ?>
    </p>
   <?php Pjax::begin(['id' => 'id_contenedor_auto', 'timeout' => false, 'enablePushState' => false]) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_automovil',
            'id_version',
            'placa',
            'fecha_registro',
            'fecha_actualizacion',
            // 'id_user_registro',
            // 'id_user_actualizacion',
            // 'color',
            // 'serial_motor:ntext',
            // 'serial_carroceria',
            // 'id_tipo_vehiculo',
            // 'id_uso',

            [
              'class' => 'yii\grid\ActionColumn',
              'template' => '{view} {update}',
              'buttons' => [
                  'update' => function ($url, $model, $key) {
                      return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                          'id' => 'activity-index-link',
                          'title' => Yii::t('app', 'Update'),
                          'data-toggle' => 'modal',
                          'data-target' => '#modal',
                          'data-url' => Url::to(['update', 'id' => $model->id_automovil]),
                          'data-pjax' => '0',
                      ]);
                  },
              ]
            ],
        ],
    ]); ?>
    <?php Pjax::end() ?>

</div>
<?php
$this->registerJs(
"$(document).on('click', '#activity-index-link', (function() {
    $.get(
        $(this).data('url'),
        function (data) {
            $('.modal-body').html(data);
            $('#modal').modal();
        }
    );
}));"
); ?>

<?php
Modal::begin([
    'options' => [
        'id' => 'modal',
        'tabindex' => false // important for Select2 to work properly
    ],
	'header' => '<h4 class="modal-title">Crear Automovil</h4>',
	//'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Cerrar</a>',
]);
echo "<div class='well'></div>";

Modal::end();
?>

<?php
//https://github.com/kartik-v/yii2-widgets/issues/202
//https://github.com/kartik-v/yii2-widget-select2/issues/19#issuecomment-81402573
$string = <<<EXP2

/*  $("#id_contenedor_auto").on("pjax:complete", function() {
  var el = $("#automovil-id_marca"),
      options = el.data('pluginOptions'); // select2 plugin settings saved
   jQuery.when(el.select2(window[options])).done(initSelect2Loading('automovil-id_marca'));
 });*/

EXP2;
$this->registerJs($string);

?>
