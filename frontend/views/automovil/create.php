<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Automovil */

$this->title = Yii::t('app', 'Create Automovil');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Automoviles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="automovil-create">

    <?= $this->render('_form', [
        'model' => $model,
        'documentos'=>$documentos,
    ]) ?>

</div>
