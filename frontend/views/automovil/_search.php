<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\search\SearchAutomovil */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="automovil-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_automovil') ?>

    <?= $form->field($model, 'id_version') ?>

    <?= $form->field($model, 'placa') ?>

    <?= $form->field($model, 'fecha_registro') ?>

    <?= $form->field($model, 'fecha_actualizacion') ?>

    <?php // echo $form->field($model, 'id_user_registro') ?>

    <?php // echo $form->field($model, 'id_user_actualizacion') ?>

    <?php // echo $form->field($model, 'color') ?>

    <?php // echo $form->field($model, 'serial_motor') ?>

    <?php // echo $form->field($model, 'serial_carroceria') ?>

    <?php // echo $form->field($model, 'id_tipo_vehiculo') ?>

    <?php // echo $form->field($model, 'id_uso') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
