<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use frontend\models\Marcas;
use frontend\models\Modelos;
use frontend\models\Version;
use frontend\models\Anio;
use frontend\models\Uso;
use frontend\models\Tipo_vehiculo;
use kartik\select2\Select2;
use kartik\depdrop\Depdrop; //http://demos.krajee.com/widget-details/depdrop
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use kartik\widgets\FileInput;
use yii\widgets\Pjax;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model frontend\models\Automovil */
/* @var $form yii\widgets\ActiveForm


*/
?>

<div class="automovil-form">

    <?php $form = ActiveForm::begin([
      'id' => 'automovil-form',
	    'options' => ["enctype" => "multipart/form-data"],
      'enableAjaxValidation' => true,
      'enableClientScript' => true,
      'enableClientValidation' => true,
      ]);
   ?>


	<div class="row">

		<div class="col-sm-6">
      <?=$form->field($model,"id_marca")->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Marcas::find()->all(), 'id_marca', 'nombre'),
                'language' => 'en',
                'options' => ['placeholder' => 'Select ...'],
                'pluginOptions' => [
                'allowClear' => true
                ],
            ]);
     ?>
		 </div>
		 <div class="col-sm-6">
            <?=$form->field($model, "id_modelo")->widget(DepDrop::classname(), [
          					'data'=> [''=>'Seleccione '],
          					'options'=>['placeholder'=>'Selecione ...'],
          					'type' => DepDrop::TYPE_SELECT2,
          					'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
          					'pluginOptions'=>[
          					'depends'=>["automovil-id_marca"],
          					'placeholder' => 'Seleccione ...',
          					'url'=>Url::to(['dependent-dropdown/child-account', 'db'=>Modelos::classname(),'cmpo_dep'=>'id_marca','cmpo_mostrar'=>'nombre','id_cmpo'=>'id_modelo']),
          					'loadingText' => 'Leyendo ...',
          					]
          				]);

			?>
		 </div>
    </div>

	<div class="row">

		<div class="col-sm-6">
             <?=$form->field($model, "id_anio")->widget(DepDrop::classname(), [
        					'data'=> [''=>'Seleccione '],
        					'options'=>['placeholder'=>'Selecione ...'],
        					'type' => DepDrop::TYPE_SELECT2,
        					'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
        					'pluginOptions'=>[
        					'depends'=>["automovil-id_modelo"],
        					'placeholder' => 'Seleccione ...',
        					'url'=>Url::to(['dependent-dropdown/anio','db'=>Modelos::classname()]),
        					'loadingText' => 'Leyendo ...',
        					]
        				]);

			?>
		 </div>
		 <div class="col-sm-6">
            <?=$form->field($model, "id_version")->widget(DepDrop::classname(), [
            					'data'=> [''=>'Seleccione '],
            					'options'=>['placeholder'=>'Selecione ...'],
            					'type' => DepDrop::TYPE_SELECT2,
            					'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
            					'pluginOptions'=>[
            					'depends'=>["automovil-id_anio"],
            					'placeholder' => 'Seleccione ...',
            					'url'=>Url::to(['dependent-dropdown/version','db'=>$model::classname()]),
            					'loadingText' => 'Leyendo ...',
            					]
            				]);

			?>

		 </div>
	</div>

	<div class="row">

		 <div class="col-sm-6">
			 <?= $form->field($model, 'placa')->textInput(['maxlength' => true]) ?>
		 </div>

		 <div class="col-sm-6">
			<?= $form->field($model, 'color')->textInput(['maxlength' => true]) ?>
		 </div>
	</div>

	<div class="row">

		 <div class="col-sm-6">
		     <?= $form->field($model, 'serial_motor')->textarea(['rows' => 1]) ?>
		 </div>

		 <div class="col-sm-6">
		    <?= $form->field($model, 'serial_carroceria')->textInput(['maxlength' => true]) ?>
		 </div>
    </div>

	<div class="row">

		<div class="col-sm-6">
             <?php
				      echo $form->field($model,"id_tipo_vehiculo")->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Tipo_vehiculo::find()->all(), 'id_tipo_vehiculo', 'nombre'),
                'language' => 'en',
                'options' => ['placeholder' => 'Seleccione ...'],
                'pluginOptions' => [
                'allowClear' => true
                ],
                 ]);

             ?>
		 </div>
		 <div class="col-sm-6">
             <?php
				echo $form->field($model,"id_uso")->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Uso::find()->all(), 'id_uso', 'nombre'),
                'language' => 'en',
                'options' => ['placeholder' => 'Seleccione ...'],
                'pluginOptions' => [
                'allowClear' => true
                ],
                 ]);

             ?>
		 </div>
    </div>
    <div class="row">
     <!--http://plugins.krajee.com/file-input#events-->
      <div class="col-sm-12">
       <?=$form->field($documentos, 'doc_files[]')->widget(FileInput::classname(),[
                                       'options'=>[
                                           'multiple'=>true,
                                           'accept'=>'*',
                                       ],
                                       'pluginOptions' => [
                                           'showUpload' => false,
                                           'dropZoneEnabled'=>false,
                                           'uploadUrl' => Url::to(['automovil/upload-file']),
                                           'allowedFileExtensions'=>['jpg','gif','png'],
                                           'uploadExtraData' => new JsExpression('function() {
                                             var obj = {};
                                             $("form#automovil-form").find("input").each(function() {
                                                 var id = $(this).attr("name"), val = $(this).val();
                                                 obj[id] = val;
                                             });
                                             return obj;
                                           }'),
                                       ]
                                    ])->label(false);
     ?>


      </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$string = <<<EXP2
//USAGE: $("#form").serializefiles();
  (function($) {
  $.fn.serializefiles = function() {
      var obj = $(this);
      /* ADD FILE TO PARAM AJAX */
      var formData = new FormData();
      $.each($(obj).find("#documentos-doc_files"), function(i, tag) {
          $.each($(tag)[0].files, function(i, file) {
              formData.append(tag.name, file);
          });
      });
      var params = $(obj).serializeArray();
      $.each(params, function (i, val) {
          formData.append(val.name, val.value);
      });
      return formData;
  };
  })(jQuery);

  $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    

EXP2;
$this->registerJs($string);

?>

<?php

//https://github.com/kartik-v/yii2-widget-select2/issues/41 no se puede tipear en el select
//https://github.com/select2/select2/issues/1436

$this->registerJs('
  // obtener la id del formulario y establecer el manejador de eventos

       $("button.kv-file-upload").hide();

      $("form#automovil-form").on("beforeSubmit", function(e) {
          var form = $(this);
          var params="";
          $("#documentos-doc_files").fileinput("upload");

          if(form.attr("action")=="/yii_t_final/automovil/create"){
                params="?submit=true";
          }else{
              params="&submit=true";
          }
          $.post(
              form.attr("action")+params,form.serialize()
          )
          .done(function(result) {
              form.parent().html(result.message);
              $.pjax.reload({container:"#w0"});

              var container=$("#datosautomovil");

              if ( typeof container !== "undefined" && container) {//solo si esta definido
                container.html("<pre >Placa: "+result.nombre_completo+"; Numero de Identificacion "+result.identificacion+"</pre>");
                $("#poliza-id_automovil_hidden").val(result.id_automovil);
              }

          });
          return false;
      }).on("submit", function(e){
          e.preventDefault();
          e.stopImmediatePropagation();
          return false;
      });

  ');
?>
