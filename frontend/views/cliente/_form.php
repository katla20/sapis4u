<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json; //keyla bullon
use frontend\models\Pais;
use frontend\models\Provincias;
use frontend\models\Departamentos;
use frontend\models\Localidades;
use kartik\select2\Select2;//keyla bullon
use kartik\depdrop\DepDrop;//http://demos.krajee.com/widget-details/depdrop
use wbraganca\dynamicform\DynamicFormWidget;


/* @var $this yii\web\View */
/* @var $model frontend\models\Cliente */
/* @var $form yii\widgets\ActiveForm */
?>

<!--<div class="cliente-form">

    <?php //$form = ActiveForm::begin(); ?>


    <?php //ActiveForm::end(); ?>

</div>
-->
<div class="customer-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
        </div>
		<div class="col-sm-6">
            <?= $form->field($model, 'nombre2')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'apellido')->textInput(['maxlength' => true]) ?>
        </div>
		<div class="col-sm-6">
            <?= $form->field($model, 'apellido2')->textInput(['maxlength' => true]) ?>
        </div>

	    <div class="col-sm-6">
	    <?= $form->field($model, 'cedula')->textInput() ?>
		 </div>
		 <div class="col-sm-6">
        <?= $form->field($model, 'rif')->textInput(['maxlength' => true]) ?>
		</div>
		 <div class="col-sm-6">
		<?= $form->field($model, 'fecha_nacimiento')->textInput() ?>
		</div>
		<div class="col-sm-6">
		<?php
		$provincia = ArrayHelper::map(Provincias::find()->all(), 'provincia_id', 'provincia');
		echo $form->field($model, 'provincia_id')->widget(Select2::classname(), [
			'data' => $provincia,
			'language' => 'en',
			'options' => ['placeholder' => 'Seleccione una provincia ...','id'=>'provincia_id'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
		?>
		</div>
		<div class="col-sm-6">
		<?php
		  /*$departamento = ArrayHelper::map(Departamentos::find()->all(), 'departamento_id', 'departamento');
		  //en Modelo se va a colocar el nombre de la clase del que deseamos traernos los datos
		  //en cmpo_dep es el nombre del campo de la dependencia , es el id que conecta una tabla con otra
		  // cmpo_mostrar el campo de la tabla que mostrara el listado

		  echo $form->field($model, 'departamento_id')->widget(DepDrop::classname(), [
			'data'=> [''=>'Seleccione departamentos'],
			'options' => ['id'=>'departamento_id'],
			 'type' => DepDrop::TYPE_SELECT2,
			 'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
			 'pluginOptions'=>[
				'depends'=>['provincia_id'],
				'placeholder' => 'Seleccione departamentos...',
				'url'=>Url::to(['dependent-dropdown/child-account', 'modelo'=>'Departamentos','cmpo_dep'=>'provincia_id','cmpo_mostrar'=>'departamento']),
				//'params'=>['input-type-1', 'input-type-2'],
				'loadingText' => 'Leyendo departamentos ...',
			]
			]);*/
			?>
            </div>

			<div class="col-sm-6">
			<?php
			/*echo $form->field($model, 'localidad_id')->widget(DepDrop::classname(), [
				'data'=> [''=>'Seleccione localidad'],
				'options' => ['id'=>'localidad_id'],
				 'type' => DepDrop::TYPE_SELECT2,
				 'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
				 'pluginOptions'=>[
					'depends'=>['departamento_id'],
					'url'=>Url::to(['dependent-dropdown/child-account', 'modelo'=>'Localidaddes','cmpo_dep'=>'departamento_id','cmpo_mostrar'=>'localidad']),
					//'params'=>['input-type-1', 'input-type-2'],
					'loadingText' => 'Leyendo localidades ...',
				]
			 ]);*/
			?>
			</div>
  </div>
     <?php DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
        'widgetBody' => '.container-items', // required: css class selector
        'widgetItem' => '.item', // required: css class
        'limit' => 4, // the maximum times, an element can be added (default 999)
        'min' => 0, // 0 or 1 (default 1)
        'insertButton' => '.add-item', // css class
        'deleteButton' => '.remove-item', // css class
        'model' => $modelDireccion[0],
        'formId' => 'dynamic-form',
        'formFields' => [
            'full_name',
            'address_line1',
            'address_line2',
            'city',
            'state',
            'postal_code',
        ],
    ]); ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <i class="glyphicon glyphicon-envelope"></i> Direcciones
                <button type="button" class="add-item btn btn-success btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i> Add</button>
            </h4>
        </div>
        <div class="panel-body">
            <div class="container-items"><!-- widgetBody -->
            <?php foreach ($modelDireccion as $i => $modelDireccion): ?>
                <div class="item panel panel-default"><!-- widgetItem -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">Direccion</h3>
                        <div class="pull-right">
                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
					           <div class="row">
                           <div class="col-sm-6">
						            <?php

            							$pais = ArrayHelper::map(Pais::find()->all(), 'id_pais', 'nombre');

            							echo $form->field($model, 'id_pais')->widget(Select2::classname(), [
            								'data' => $pais,
            								'language' => 'es',
            								'options' => ['placeholder' => 'Seleccione una Pais ...'],
            								'pluginOptions' => [
            									'allowClear' => true
            								],
            							]);
							            ?>
                    </div>

                    </div><!-- .row -->
                    <div class="panel-body">

                        <?php
                            // necessary for update action.
                            if (! $modelDireccion->isNewRecord) {
                                echo Html::activeHiddenInput($modelDireccion, "[{$i}]id");
                            }
                        ?>
                        <?= $form->field($modelDireccion, "[{$i}]calle_avenida")->textInput(['maxlength' => true]) ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <?= $form->field($modelDireccion, "[{$i}]nro")->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($modelDireccion, "[{$i}]sector")->textInput(['maxlength' => true]) ?>
                            </div>
                        </div><!-- .row -->
                        <div class="row">
                            <div class="col-sm-4">
                                <?= $form->field($modelDireccion, "[{$i}]telefono")->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-sm-4">
                                <?= $form->field($modelDireccion, "[{$i}]telefono2")->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-sm-4">
                                <?= $form->field($modelDireccion, "[{$i}]telefono3")->textInput(['maxlength' => true]) ?>
                            </div>
                        </div><!-- .row -->
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
        </div>
    </div><!-- .panel -->
    <?php DynamicFormWidget::end(); ?>

    <div class="form-group">
        <?= Html::submitButton($modelDireccion->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script=<<< JS
//script para llenado de las direeciones
	$(".dynamicform_wrapper").on("beforeInsert", function(e, item) {
		console.log("beforeInsert");
		});

		$(".dynamicform_wrapper").on("afterInsert", function(e, item) {
		console.log("afterInsert");
  });

		$(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
		if (! confirm("Are you sure you want to delete this item?")) {
			return false;
		}
		return true;
	    });

		$(".dynamicform_wrapper").on("afterDelete", function(e) {
		console.log("Deleted item!");
		});

		$(".dynamicform_wrapper").on("limitReached", function(e, item) {
		alert("Limit reached");
	    });


JS;
$this->registerJs($script);
$this->registerCss(".loading-cliente-id_pais { display: none; }");
?>
