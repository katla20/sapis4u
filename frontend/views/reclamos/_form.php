<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\TipoReclamos;
use kartik\file\FileInput;
use kartik\select2\Select2;
use kartik\depdrop\Depdrop; //http://demos.krajee.com/widget-details/depdrop
use frontend\models\Pais;
use frontend\models\Estado;

/* @var $this yii\web\View */
/* @var $model frontend\models\Reclamos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reclamos-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<div class="row">
                             <div class="col-sm-6">
                               <?php
								   /*echo $form->field($model,"poliza")->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map(Poliza::find()->where(['id_poliza' => '10'])->all(), 'id_poliza', 'nro_poliza'),
                                    'language' => 'en',
                                     'options' => ['placeholder' => 'Select a Poliza'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                  ]);*/

                                ?>
                             </div>
							 <div class="col-sm-6">
								<?/*=$form->field($model, "id_recibo")->widget(DepDrop::classname(), [
										'data'=> [''=>'Seleccione '],
										'options'=>['placeholder'=>'Selecione ...'],
										'type' => DepDrop::TYPE_SELECT2,
										'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
										'pluginOptions'=>[
										 'depends'=>["poliza"],
										 'placeholder' => 'Seleccione ...',
										 'url'=>Url::to(['dependent-dropdown/child-account', 'db'=>Recibo::classname(),'cmpo_dep'=>'id_poliza','cmpo_mostrar'=>'nro_recibo','id_cmpo'=>'id_poliza']),
										 'loadingText' => 'Leyendo ...',
									   ]
									   ]);

								 */?>
							 </div>
	</div>

    <?= $form->field($model, 'id_recibo')->textInput() ?>
	
	<?php
				echo $form->field($model,"id_tipo_reclamo")->widget(Select2::classname(), [
                'data' => ArrayHelper::map(TipoReclamos::find()->all(), 'id_tipo_reclamo', 'nombre'),
                'language' => 'en',
                'options' => ['placeholder' => 'Select ...'],
                'pluginOptions' => [
                'allowClear' => true
                ],
                 ]);

   ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'monto')->textInput() ?>

    <?php
    echo $form->field($model, 'estatus')->dropDownList([1 => 'Activo', 0 => 'Inactivo']);
	?> 

    <?= $form->field($model, 'fecha_ocurrencia')->textInput() ?>

    <?= $form->field($model, 'fecha_notificacion')->textInput() ?>
	
	<?php
	$opciones = \yii\helpers\ArrayHelper::map($tipoPartes, 'id_parte', 'nombre');
	echo $form->field($model, 'parte')->checkboxList($opciones, ['unselect'=>NULL]);
	?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
