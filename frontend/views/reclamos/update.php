<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Reclamos */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Reclamos',
]) . ' ' . $model->id_reclamo;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reclamos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_reclamo, 'url' => ['view', 'id' => $model->id_reclamo]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="reclamos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'tipoPartes' => $tipoPartes
    ]) ?>

</div>
