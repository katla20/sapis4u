<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\ReclamosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Reclamos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reclamos-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Reclamos'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_reclamo',
            'id_recibo',
            'id_tipo_reclamo',
            'descripcion:ntext',
            'monto',
            // 'fecha_registro',
            // 'fecha_actualizacion',
            // 'id_user_registro',
            // 'id_user_actualizacion',
            // 'estatus',
            // 'fecha_ocurrencia',
            // 'fecha_notificacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
