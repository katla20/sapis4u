<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Reclamos */

$this->title = $model->id_reclamo;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reclamos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reclamos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id_reclamo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id_reclamo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_reclamo',
            'id_recibo',
            'id_tipo_reclamo',
            'descripcion:ntext',
            'monto',
            'fecha_registro',
            'fecha_actualizacion',
            'id_user_registro',
            'id_user_actualizacion',
            'estatus',
            'fecha_ocurrencia',
            'fecha_notificacion',
        ],
    ]) ?>

</div>
