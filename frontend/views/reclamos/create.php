<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Reclamos */

$this->title = Yii::t('app', 'Create Reclamos');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reclamos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reclamos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'tipoPartes' => $tipoPartes
    ]) ?>

</div>
