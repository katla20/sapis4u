<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Aseguradoras */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="aseguradoras-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'direccion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

	<?php
    echo $form->field($model, 'estatus')->dropDownList([1 => 'Activo', 0 => 'Inactivo']);
	?>

    <?php
    	$opciones = \yii\helpers\ArrayHelper::map($tipoRamos, 'id_ramo', 'nombre');
    	echo $form->field($model, 'ramos')->checkboxList($opciones, ['unselect'=>NULL]);
	?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
