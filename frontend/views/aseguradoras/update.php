<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Aseguradoras */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Aseguradoras',
]) . ' ' . $model->id_aseguradora;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Aseguradoras'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_aseguradora, 'url' => ['view', 'id' => $model->id_aseguradora]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="aseguradoras-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        
        'tipoRamos' => $tipoRamos
    ]) ?>

</div>
