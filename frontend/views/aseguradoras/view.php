<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Aseguradoras */

$this->title = $model->id_aseguradora;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Aseguradoras'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="aseguradoras-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id_aseguradora], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id_aseguradora], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_aseguradora',
            'nombre',
            'direccion:ntext',
            'telefono',
			'estatus',
        ],
    ]) 
    
    ?>
    
    <h2>Ramos</h2>
    <?php 
    $Ram;
    foreach ($model->ramosList as $Ramos) {
        echo $Ramos['nombre'] . " ";
        //$Ram=$Ramos['nombre'];
      
     }
     
    ?>

</div>
