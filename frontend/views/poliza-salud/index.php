<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PolizaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Polizas');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php //echo Url::current();?>
<?php //echo '---'.Url::to();
/*@app: Your application root directory (either frontend or backend or console depending on where you access it from)
@vendor: Your vendor directory on your root app install directory
@runtime: Your application files runtime/cache storage folder
@web: Your application base url path
@webroot: Your application web root
@tests: Your console tests directory
@common: Alias for your common root folder on your root app install directory
@frontend: Alias for your frontend root folder on your root app install directory
@backend: Alias for your backend root folder on your root app install directory
@console: Alias for your console root folder on your root app install directory*/
?>
<?php echo Yii::getAlias('@web');?>


<div class="poliza-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Poliza'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_poliza',
            'numero_poliza',
            'id_tipo',
            'fecha_vigenciadesde',
            'fecha_vigenciahasta',
            // 'estatus',
            // 'id_titular',
            // 'fecha_registro',
            // 'fecha_actualizacion',
            // 'id_user_registro',
            // 'id_user_actualizacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
