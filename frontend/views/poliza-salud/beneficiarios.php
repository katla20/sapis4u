<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\Response;
use kartik\select2\Select2;
use kartik\depdrop\Depdrop; //http://demos.krajee.com/widget-details/depdrop
use wbraganca\dynamicform\DynamicFormWidget;
use yii\widgets\MaskedInput;
use kartik\icons\Icon;
use yii\widgets\Pjax;
use kartik\checkbox\CheckboxX;
use kartik\money\MaskMoney;
use frontend\models\Cliente;
use frontend\models\Persona;
?>

<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
  -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
  box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
<div class="box-header with-border">
       <h4 class="box-title"><?=Icon::show('car')?> Comisiones y Vendedor</h4>
</div><!--box-header --->
<div class="box-body"><!--box-body --->
    <div class="container-items"><!-- widgetBody -->
       <div class="row">
       <div class="col-sm-10">
             <div class="nonboxy-widget">
               <div class="widget-content">
                    <div class="widget-box">
                      <fieldset>
                         <legend><h4><?=Icon::show('group')?>Beneficiarios</h4></legend>
                         <div class="row">
                           <div class="col-sm-5"><!--col-sm-4-->
                             <?=$form->field($model,'id_beneficiario', [
                                                                           'addon' => [
                                                                               'prepend' => ['content'=>'<i class="glyphicon glyphicon-user"></i>'],
                                                                               'append'=>['content'=>Html::a('Agregar', '#', [
                                                                                                              'id' => 'activity-index-link',
                                                                                                              'class' => 'btn btn-success',
                                                                                                              'data-toggle' => 'modal',
                                                                                                              'data-target' => '#modal',
                                                                                                              'data-url' => Url::to(['persona/create'], true),
                                                                                                              'data-pjax' => '0',
                                                                                                          ]), 'asButton'=>true],
                                                                           ]
                                                    ])->widget(Select2::classname(), [
                                                                'data' =>ArrayHelper::map( Cliente::find()
                                                                                             ->joinwith('idPersona')
                                                                                             ->where("cliente.estatus = :status", [':status' => 1])
                                                                                             ->select("(nombre||' '||segundo_nombre||' '||apellido|| ' ' ||segundo_apellido || ' ' || identificacion) AS nombre_completo ,id_cliente")
                                                                                             ->all(),'id_cliente', 'nombre_completo'),
                                                                'language' => 'en',
                                                                 'options' => ['placeholder' => 'Seleccione ...','id'=>'id_beneficiario'],
                                                                 'pluginOptions' => [
                                                                    'allowClear' => true
                                                                ],
                                                ])->label(false);
                              ?>
                          </div>
                         <div class="col-sm-2"><?= Html::a( Icon::show('plus').' Agregar a la lista','#' , ['class'=>'btn btn-primary','id' => 'add-beneficiarios']) ?></div>
                         </div>
                        </div>
                          <table class="table table-condensed" id="table-beneficiarios">
                            <tr><th>Cedula</th><th>Nombres y Apellidos</th><th>Edad</th><th>Parentezco</th><th>Operacion</th></tr>
                            <tr><td>112211212</td><td>maria ramirez</td><td>20</td><td>Madre</td><td><i class="fa fa-trash"></i></td></tr>
                          </table>
                     </fieldset>
                    </div>
               </div>
           </div>
       </div>

   </div><!-- widgetBody -->
</div><!--box-body --->
</div><!--box--->
