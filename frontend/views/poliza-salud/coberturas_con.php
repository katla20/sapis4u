<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\Response;
use kartik\checkbox\CheckboxX;
use kartik\money\MaskMoney;

//file:///C:/xampp/htdocs/yii_t_final/vendor/almasaeed2010/adminlte/documentation/index.html

use frontend\assets\LocateAsset;
LocateAsset::register($this);


/* @var $this yii\web\View */
/* @var $model frontend\models\Poliza */

//http://awesome-bootstrap-checkbox.okendoken.com/demo/index.html
//http://montrezorro.github.io/bootstrap-checkbox/

/* SELECT aseguradoras.nombre AS nombre_aseguradora,
    aseguradoras.id_aseguradora,
    productos.nombre AS nombre_producto,
    productos.id_producto,
    coberturas.id_cobertura,
    coberturas.nombre AS nombre_cobertura
   FROM aseguradoras
     JOIN inter_aseg_producto USING (id_aseguradora)
     JOIN productos USING (id_producto)
     JOIN inter_prod_cob USING (id_producto)
     JOIN coberturas USING (id_cobertura)
  WHERE aseguradoras.id_aseguradora = 3 AND productos.id_producto = 1;
  -----------------------------------------------------------
  SELECT * FROM
    coberturas_view
WHERE id_aseguradora=3 AND id_producto=1
  */
?>
<?php
 //$opciones = \yii\helpers\ArrayHelper::map($tipoProductos, 'id_producto', 'nombre');
 //echo $form->field($model, 'productos')->checkboxList($opciones, ['unselect'=>NULL]);
?>
<div>
<?php echo $form->field($model, 'hi', [
    'template' => '{input}{error}{hint}',
    'labelOptions' => ['class' => 'cbx-label'],

])->widget(CheckboxX::classname(), ['autoLabel'=>false, 'pluginOptions'=>['threeState'=>false],]);?>
</div>
<div>
<?php  echo $form->field($model, 'suma',[
                                          'addon' => [
                                              'prepend' => [
                                                  'content' => 'Bs.',

                                              ]
                                          ]
                                      ])->widget(MaskMoney::classname(), [
                                                                          'pluginOptions' => [
                                                                              'prefix' => '',
                                                                              'suffix' => '',
                                                                              'allowNegative' => false
                                                                            ]
                                                                         ]);?>

</div>
<div class="poliza-automovil">
          <div class="table-responsive" id="contendor_coberturas">
             <table class="table table-striped" id="tabla_coberturas" url_action="<?=Url::to(['poliza/buscar-coberturas'])?>">
                  <tr><th>Item</th><th>Detalle de Coberturas</th><th>Suma A</th><th>Tasa(%)</th><th>Prima</th></tr><tr>
                  <tr style="text-align:center">
                    <th colspan="5">
                      <div class="callout callout-info lead">
                        <h4>No ha Seleccionado Ningun producto!</h4>
                     </div>
                   </th>
                 </tr>
             </table>
        </div>
</div>