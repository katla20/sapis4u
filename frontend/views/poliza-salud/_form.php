<?php
use yii\helpers\Html;
//use yii\bootstrap\ActiveForm;
//use yii\bootstrap\ActiveField;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use kartik\select2\Select2;
use kartik\depdrop\Depdrop; //http://demos.krajee.com/widget-details/depdrop
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use yii\bootstrap\Tabs;
use kartik\icons\Icon;
use kartik\tabs\TabsX;
use kartik\nav\NavX;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\web\Response;
use kartik\money\MaskMoney;
use frontend\models\Aseguradoras;
use frontend\models\InterAsegRamo;
use frontend\models\InterAsegProducto;
use frontend\models\Productos;
use frontend\models\Cliente;
use frontend\models\Persona;
use frontend\models\Intermediario;

/* @var $this yii\web\View */
/* @var $model frontend\models\Poliza */
/* @var $form yii\widgets\ActiveForm */

/*
self::BSG => ['prefix' => 'glyphicon glyphicon-', 'class' => '\\yii\\bootstrap\\BootstrapAsset'],
self::FA => ['prefix' => 'fa fa-', 'class' => 'FontAwesomeAsset'],
self::EL => ['prefix' => 'el-icon-', 'class' => 'ElusiveAsset'],
self::TYP => ['prefix' => 'typcn typcn-', 'class' => 'TypiconsAsset'],
self::WHHG => ['prefix' => 'icon-', 'class' => 'WhhgAsset'],
self::JUI => ['prefix' => 'ui-icon ui-icon-', 'class' => 'JuiAsset'],
self::UNI => ['prefix' => 'uni uni-', 'class' => 'UniAsset'],
self::SI => ['prefix' => 'socicon socicon-', 'class' => 'SociconAsset'],
self::OCT => ['prefix' => 'octicon octicon-', 'class' => 'OcticonsAsset'],
self::FI => ['prefix' => 'flag-icon flag-icon-', 'class' => 'FlagIconAsset'],
glyphicons glyphicons-handshake
*/
?>
<?php $form = ActiveForm::begin(['id' => 'poliza-form','enableAjaxValidation' =>false]);?>
<div class="poliza-form">

    <div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
    <div class="box-header with-border">
         <h4 class="box-title"><i class="glyphicon glyphicon-user"></i> Datos de la Poliza</h4>
    </div>
    <div class="box-body">
  	    <div class="container-items"><!-- widgetBody -->
          <div class="row">
             <div class="col-sm-5">
                  <?=$form->field($model,"id_aseguradora",[
                                                'addon' => [
                                                    'prepend' => [
                                                        'content' => '<i class="fa fa-university"></i>'

                                                    ]
                                                ]
                                            ])->widget(Select2::classname(), [
                       'data' =>   ArrayHelper::map(Aseguradoras::find()->joinwith('interAsegRamos')
                                         ->where('inter_aseg_ramo.id_ramo = :ramo and aseguradoras.estatus=:status')
                                         ->addParams([':ramo' => 2])
                                         ->addParams([':status' => 1])
                                         ->all(),'id_aseguradora', 'nombre'),
                       'language' => 'en',
                        'options' => ['placeholder' => 'Seleccione ...'],
                        'pluginOptions' => [
                           'allowClear' => true
                       ],
                     ]);
                   ?>
       </div>
       <div class="col-sm-5">
                   <?=$form->field($model, "producto",[
                                                 'addon' => [
                                                     'prepend' => [
                                                         'content' => '<i class="fa fa-cart-plus"></i>'

                                                     ]
                                                 ]
                                             ])->widget(DepDrop::classname(), [
                       										'data'=> [''=>'Seleccione '],
                       										'options'=>['placeholder'=>'Selecione ...'],
                       										'type' => DepDrop::TYPE_SELECT2,
                       										'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                       										'pluginOptions'=>[
                       										'depends'=>["poliza-id_aseguradora"],
                       										'placeholder' => 'Seleccione ...',
                       										  'url'=>Url::to(['dependent-dropdown/child-account-join', 'db'=>Productos::classname(),'join'=>'interAsegProductos','cmpo_dep'=>'inter_aseg_producto.id_aseguradora','cmpo_mostrar'=>'productos.nombre','id_cmpo'=>'productos.id_producto','cmpo_adic'=>'productos.id_ramo','valor_adic'=>2]),
                       									   ]
   									               ]);
   								?>
       </div>

     </div>

    <div class="row">
        <div class="col-sm-2">
         <?= $form->field($model, 'numero_poliza',[
                                       'addon' => [
                                           'prepend' => [
                                               'content' => '<i class="glyphicon glyphicon-list-alt"></i>'

                                           ]
                                       ]
                                   ])->textInput(['maxlength' => true]) ?>

        </div>

        <div class="col-sm-2">
          <?=$form->field($model, 'fecha_vigenciadesde',[
                                        'addon' => [
                                            'prepend' => [
                                                'content' => '<i class="glyphicon glyphicon-calendar"></i>'

                                            ]
                                        ]
                                    ])->widget(\yii\widgets\MaskedInput::className(),
                                                             [
                                                             'clientOptions' => ['alias' =>  'date']
                                                             ]) ?>

        </div>
        <div class="col-sm-2">
           <?=$form->field($model, 'fecha_vigenciahasta',[
                                         'addon' => [
                                             'prepend' => [
                                                 'content' => '<i class="glyphicon glyphicon-calendar"></i>'

                                             ]
                                         ]
                                     ])->widget(\yii\widgets\MaskedInput::className(),
                                                              [
                                                              'clientOptions' => ['alias' =>  'date']
                                                              ]) ?>
        </div>
    </div>

    <div class="row">
      <?php Pjax::begin();?>
        <div class="col-sm-5" id="w0">
          <div class="nonboxy-widget">
             <div class="widget-content">
                 <div class="widget-box">
                   <fieldset>
                      <legend><h4><i class="glyphicon glyphicon-user"></i> Contratante</h4></legend>
         <?php
         echo $form->field($model,'id_contratante', [
                                                       'addon' => [
                                                           'prepend' => ['content'=>'<i class="glyphicon glyphicon-user"></i>'],
                                                           'append'=>['content'=>Html::a('<i class="fa fa-user-plus"></i> Nuevo', '#', [
                                                                                          'id' => 'activity-index-link',
                                                                                          'class' => 'btn btn-success',
                                                                                          'data-toggle' => 'modal',
                                                                                          'data-target' => '#modal',
                                                                                          'data-url' => Url::to(['persona/create'], true),
                                                                                          'data-pjax' => '0',
                                                                                      ]), 'asButton'=>true],
                                                       ]
                                ])->widget(Select2::classname(), [
                                            'data' =>ArrayHelper::map( Cliente::find()
                                                                         ->joinwith('idPersona')
                                                                         ->where("cliente.estatus = :status", [':status' => 1])
                                                                         ->select("(nombre||' '||segundo_nombre||' '||apellido|| ' ' ||segundo_apellido || ' ' || identificacion) AS nombre_completo ,id_cliente")
                                                                         ->all(),'id_cliente', 'nombre_completo'),
                                            'language' => 'en',
                                             'options' => ['placeholder' => 'Seleccione el Contratante','id'=>'id_contratante'],
                                             'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                            ])->label(false);
          ?>
        </fieldset>
        </div></div></div></div>

        <?php Pjax::end(); ?>
        <div class="col-sm-5">
        </br></br></br>
          <p id="datoscontratante"></p>
          <?=$form->field($model, 'id_contratante_hidden')->hiddenInput()->label(false);?>
        </div>
        </div>
        <div class="row">
        <div class="col-sm-10">
              <div class="nonboxy-widget">
                <div class="widget-content">
                     <div class="widget-box">
                       <fieldset>
                          <legend><h4><?=Icon::show('group')?>Asegurados (puede incluir al Contratante con su carga familiar)</h4></legend>
                          <div class="row">
                          <div class="col-sm-5">
                            <?=$form->field($model,'asegurados', [
                                                          'addon' => [
                                                              'prepend' => ['content'=>'<i class="glyphicon glyphicon-user"></i>'],
                                                              'append'=>['content'=>Html::a('<i class="fa fa-user-plus"></i> Nuevo', '#', [
                                                                                             'id' => 'activity-index-link',
                                                                                             'class' => 'btn btn-success',
                                                                                             'data-toggle' => 'modal',
                                                                                             'data-target' => '#modal',
                                                                                             'data-url' => Url::to(['persona/create'], true),
                                                                                             'data-pjax' => '0',
                                                                                         ]), 'asButton'=>true],
                                                          ]
                                                   ])->widget(Select2::classname(), [
                                                               'data' =>ArrayHelper::map( Cliente::find()
                                                                                            ->joinwith('idPersona')
                                                                                            ->where("cliente.estatus = :status", [':status' => 1])
                                                                                            ->select("(nombre||' '||segundo_nombre||' '||apellido|| ' ' ||segundo_apellido || ' ' || identificacion) AS nombre_completo ,id_cliente")
                                                                                            ->all(),'id_cliente', 'nombre_completo'),
                                                               'language' => 'en',
                                                                'options' => ['placeholder' => 'Seleccione los Asegurados','id'=>'asegurados'],
                                                                'pluginOptions' => [
                                                                   'allowClear' => true
                                                               ],
                                               ])->label(false);
                             ?>
                          </div>
                          <div class="col-sm-2"><?= Html::a(Icon::show('plus').' Agregar a la lista','#', ['class'=>'btn btn-primary','id' => 'add-asegurados', 'data-url' => Url::to(['poliza-salud/buscar-asegurado'], true),]) ?></div>
                          </div>
                         </div>
                           <table class="table table-condensed" id="table-asegurados">
                             <tr><th>Cedula</th><th>Nombres y Apellidos</th><th>Edad</th><th>Parentezco</th><th>Operacion</th></tr>
                           </table>
                      </fieldset>
                     </div>
                </div>
            </div>
        </div>
        </div><!--CONTAINER-->
        </div><!--BOX BODY-->
  </div><!--BOX-->


  <div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
  -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
  box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
  <div class="box-header with-border">
       <h4 class="box-title"><?=Icon::show('newspaper-o')?>Datos del Recibo</h4>
  </div>
  <div class="box-body">
      <div class="container-items"><!-- widgetBody -->
        <div class="row">
            <div class="col-sm-2">
               <?=$form->field($model,"tipo_recibo",[
                                             'addon' => [
                                                 'prepend' => [
                                                     'content' => '<i class="fa fa-university"></i>'

                                                 ]
                                             ]
                                         ])->widget(Select2::classname(), [
                                                    'data' => ['Nuevo' => 'Nuevo', 'Renovacion' => 'Renovacion'] ,
                                                    'language' => 'en',
                                                     'options' => ['placeholder' => 'Seleccione ...'],
                                                     'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                  ]);
                ?>
              </div>
               <div class="col-sm-3">
                 <?= $form->field($model, 'nro_recibo',[
                                               'addon' => [
                                                   'prepend' => [
                                                       'content' => '<i class="glyphicon glyphicon-list-alt"></i>'
                                                   ]
                                               ]
                                           ])->textInput(['maxlength' => true]) ?>
               </div>
               <div class="col-sm-3">
                 <?= $form->field($model, 'nro_certificado',[
                                               'addon' => [
                                                   'prepend' => [
                                                       'content' => '<i class="glyphicon glyphicon-list-alt"></i>'
                                                   ]
                                               ]
                                           ])->textInput(['maxlength' => true]) ?>
               </div>

        </div>
        <div class="row">
            <div class="col-sm-10">
             <?php
                $items = [
                    [
                        'label'=>'Paso 1 - '.Icon::show('group').' Cargar los Beneficiarios',
                        'content'=>$this->render('beneficiarios', ['model' => $model, 'form' => $form]),
                        'headerOptions' => ['style'=>'font-weight:bold'],
                    ],
                    [
                        'label'=>'Paso 2 - '.Icon::show('tachometer').' Seleccionar Tipo de Carga de Coberturas',

                        'items'=>[
                               [
                                   'label'=>'<i class="glyphicon glyphicon-chevron-right"></i> Coberturas Detalladas',
                                   'encode'=>false,
                                   'content'=>$this->render('coberturas_det', ['model' => $model, 'form' => $form]),
                               ],
                               [
                                   'label'=>'<i class="glyphicon glyphicon-chevron-right"></i> Coberturas Consolidadas',
                                   'encode'=>false,
                                   'content'=>$this->render('coberturas_con', ['model' => $model, 'form' => $form]),
                               ]
                             ],
                    ],
                    [
                        'label'=>'Paso 3 - '.Icon::show('user').' Cargar las Comisiones y el Vendedor',
                        'encode'=>false,
                        'content'=>$this->render('vendedor', ['model' => $model, 'form' => $form]),
                        //'active'=>false
                    ],
                ];


                echo TabsX::widget([
                    'position' => TabsX::POS_ABOVE,
                    'align' => TabsX::ALIGN_LEFT,
                    'items' => $items,
                    'encodeLabels'=>false,
                    //'headerOptions' => ['class'=>'disabled'],
                ]);

                ?>
              </div>
          </div>

      </div><!--CONTAINER-->

  </div><!--BOX BODY-->
  </div><!--BOX-->

      <div class="form-group">
          <?= Html::submitButton('Registrar Poliza', ['class' => 'btn btn-primary']) ?>
          <?= Html::resetButton('Limpiar Campos', ['class' => 'btn btn-default']) ?>
      </div>
 </div>
    <?php ActiveForm::end();?>
<?php

$string = <<<EXP2

$('#id_contratante').change(function(){
  $('#poliza-id_contratante_hidden').val($(this).val());
  var out='<table class="table table-condensed" style="outline: solid #3c763d;"><tr><th>Nombre Completo</th><th>Identificacion</th></tr>';
      out+='<tr class="success"><td>'+$("#id_contratante option:selected").html()+'</td><td>hgghgh</td></tr></table>';
  $("#datoscontratante").html(out);

});//fin change

$('#id_automovil').change(function(){
  $('#poliza-id_automovil_hidden').val($(this).val());
  var out='<table class="table table-condensed" style="outline: solid #3c763d;"><tr><th>Placa</th><th>Marca/Modelo/Año</th></tr>';
      out+='<tr class="success"><td>'+$("#id_automovil option:selected").html()+'</td><td>hgghgh</td></tr></table>';
  $("#datosautomovil").html(out);

});//fin change

$('#id_vendedor').change(function(){

  var id=$(this).val();
     $.post("buscar-vendedor", { id: id })
     .done(function(result) {
       var datos=result[0];
       if(result[0]==null){
              $("#datosvendedor").empty();
        }else{
              $("#datosvendedor").empty();
              $('#poliza-id_vendedor_hidden').val(datos.id_intermediario);
              $('#poliza-comision_porcentaje').val(datos.comision);
              $('#poliza-comision_vendedor').val((datos.comision/100)*$('#poliza-comision_total').val());

              var out='<table class="table table-condensed" style="outline: solid #3c763d;"><tr><th>Identificacion</th><th>Nombre Completo</th></tr>';
                  out+='<tr class="success"><td>'+$("#id_vendedor option:selected").html()+'</td><td>hgghgh</td></tr></table>';
              $("#datosvendedor").html(out);
        }
     });//.donefunction(result)

});//fin change

$( "#poliza-comision_total-disp").keyup(function(event) {
  event.preventDefault();
  var valor=$(this).val();
  var porcentaje=($('#poliza-comision_porcentaje').val()/100);

  if($('#poliza-comision_porcentaje').val()==""){
      porcentaje=0;
  }

    $('#poliza-comision_vendedor').val((valor)*porcentaje);
});

$(document).on('click', '#add-asegurados', (function() {
  var out='';
  if($('#asegurados').val()==""){
     alert("debe seleccionar de la lista un asegurado");
  }else{

     $.get(
         $(this).data('url'),{id:$('#asegurados').val()},
         function (result) {

             out+='<tr><td>'+result[0].identificacion+'</td><td>'+result[0].nombre_completo+'</td><td>20</td><td>Madre</td><td><i class="fa fa-trash"></i></td></tr>';
             //console.log(result[0].identificacion+result[0].nombre_completo);
              $("#table-asegurados").append(out);
         });//FIN GET

  }

}));//FIN DOCUMENT CLICK



/*$('form').on('beforeSubmit', function(e) {
    var form = $(this);
    var formData = form.serialize();
    var formDataCob= $('input[name="cobBox[]"]').serializeArray(); //array json los objetos del arreglo
    $.ajax({
        url: form.attr("action"),
        type: form.attr("method"),
        data: formData,
        success: function (data) {
        console.log(data.message);
        },
        error: function () {
            alert("Something went wrong");
        }
    });
}).on('submit', function(e){
    e.preventDefault();
});*/

EXP2;


$this->registerJs($string, \yii\web\View::POS_READY);

$this->registerJs(
    "$(document).on('click', '#activity-index-link', (function() {
        $.get(
            $(this).data('url'),
            function (data) {
                $('.modal-body').html(data);
                $('#modal').modal();
            }
        );
    }));"
);
?>

<?php
Modal::begin([
      'options' => [
         'id' => 'modal',
         'tabindex' => false // important for Select2 to work properly
     ],
    //'header' => '<h4 class="modal-title">Crear Cliente</h4>',
    //'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Cerrar</a>',
]);

//echo "<div class='well'></div>";

Modal::end();
?>
