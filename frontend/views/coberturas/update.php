<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Coberturas */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Coberturas',
]) . ' ' . $model->id_cobertura;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coberturas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_cobertura, 'url' => ['view', 'id' => $model->id_cobertura]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="coberturas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'tipoProductos' => $tipoProductos
    ]) ?>

</div>
