<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Coberturas */

$this->title = Yii::t('app', 'Create Coberturas');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coberturas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coberturas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'tipoProductos' => $tipoProductos
    ]) ?>

</div>
