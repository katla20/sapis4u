<?php

/* @var $this \yii\web\View */
/* @var $content string */
use common\models\AccessMenu;

use yii\helpers\Url;//linea para el asistente de url keyla bullon


?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?php echo (isset(Yii::$app->user->identity->username))?Yii::$app->user->identity->username:'no identificado';?></p>
                <a href="#"><i class="fa fa-circle text-success"></i><?= Yii::t('app','Online') ?></a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        
        <ul class="sidebar-menu">       
          
        <?php	
        if(isset(Yii::$app->user->identity->username)){		
			$menu=AccessMenu::getAcceso(); 				
			/*$menuItems = [
				['label' => 'Home', 'url' => ['@webroot/site/index']],
			];*/
			if($menu!=1){
				
				foreach($menu as $row){
					$i=0;
					if(!empty($row['id_padre'])){
													 
					}
					 $hijo=AccessMenu::numHijos($row['id']); 
					if($hijo > 0){
						
						
							  
				?>
				 <li class="treeview">
					<a href="<?=Url::to($row['url'])?>">
						<i class="fa fa-share"></i> <span><?=$row['rotulo']?></span>
						<i class="fa fa-angle-left"></i>
					</a>
					<!--<ul class="treeview-menu">
						<li> 			
							<a href="#?>"><i class="fa fa-dashboard"></i> <i class="fa fa-angle-left pull-right"></i></a></a>
							
							<ul class="treeview-menu">-->
									
							  <? 
							   $m=AccessMenu::crearMenu($row['id']);
							  ?>  
							<!--</ul> 
						</li> 				
					</ul>-->
				 </li>               
			   <?php        
					}else{ 
								 
			   ?>
								 
				<li><a href="<?=Url::toRoute($row['url'])?>"><i class="fa fa-dashboard"></i> <span><?=$row['rotulo']?></a></span></a></li>
								 
			   <?php
				   }	
					
								
								
								

				}
			}
        }

       
        ?>
       </ul>
        

    </section>

</aside>
