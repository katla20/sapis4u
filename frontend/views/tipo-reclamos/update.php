<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\TipoReclamos */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Tipo Reclamos',
]) . ' ' . $model->id_tipo_reclamo;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipo Reclamos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_tipo_reclamo, 'url' => ['view', 'id' => $model->id_tipo_reclamo]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tipo-reclamos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
