<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\TipoReclamos */

$this->title = Yii::t('app', 'Create Tipo Reclamos');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipo Reclamos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-reclamos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
