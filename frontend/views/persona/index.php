<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\PersonaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Personas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="persona-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Persona'), '#', [
            'id' => 'activity-index-link',
            'class' => 'btn btn-success',
            'data-toggle' => 'modal',
            'data-target' => '#modal',
            'data-url' => Url::to(['create']),
            'data-pjax' => '0',
        ]); ?>
    </p>
   <?php Pjax::begin() ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nombre',
            'segundo_nombre',
            'apellido',
            'fecha_nacimiento',
            'identificacion',
            // 'identificacion_2',
            // 'sexo',
            // 'correo',
            // 'tipo',
            // 'id_persona',
            // 'segundo_apellido',

            [
              'class' => 'yii\grid\ActionColumn',
              'template' => '{view} {update}',
              'buttons' => [
                  'update' => function ($url, $model, $key) {
                      return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                          'id' => 'activity-index-link',
                          'title' => Yii::t('app', 'Update'),
                          'data-toggle' => 'modal',
                          'data-target' => '#modal',
                          'data-url' => Url::to(['update', 'id' => $model->id_persona]),
                          'data-pjax' => '0',
                      ]);
                  },
              ]
            ],
        ],
    ]); ?>

<?php Pjax::end() ?>
</div>



<?php
$this->registerJs(
"$(document).on('click', '#activity-index-link', (function() {
    $.get(
        $(this).data('url'),
        function (data) {
            $('.modal-body').html(data);
            $('#modal').modal();
        }
    );
}));"
); ?>

<?php
Modal::begin([
 'options' => [
        'id' => 'modal',
        'tabindex' => false // important for Select2 to work properly
  ],
'header' => '<h4 class="modal-title">Crear Cliente</h4>',
//'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Cerrar</a>',
]);
echo "<div class='well'></div>";

Modal::end();
?>
