<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\persona */

/*$this->title = Yii::t('app', 'Crear Cliente');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cliente'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="persona-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelDireccion' => (empty($modelDireccion)) ? [new Direccion] : $modelDireccion,
        'modelCliente' => (empty($modelCliente)) ? [new Cliente] : $modelCliente,
        'modelCargaFamiliar' => (empty($modelCargaFamiliar)) ? [new CargaFamiliar] : $modelCargaFamiliar,
    ]) ?>

</div>
