<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Version */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Version',
]) . ' ' . $model->id_version;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Versions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_version, 'url' => ['view', 'id' => $model->id_version]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="version-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
