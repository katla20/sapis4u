<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\Versionsearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Versions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="version-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Version'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_modelo2',
            'nombre',
            'tipo',
            'caja',
            'motor',
            // 'anio',
            // 'id_modelo',
            // 'id_version',
            // 'valor',
            // 'carga',
            // 'pasajeros',
            // 'peso',
            // 'lista',
            // 'ws_inhabilitado',
            // 'fecha_registro',
            // 'fecha_modificacion',
            // 'id_user_registro',
            // 'id_user_actualizacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
