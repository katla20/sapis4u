<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Version */

$this->title = $model->id_version;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Versions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="version-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id_version], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id_version], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_modelo2',
            'nombre',
            'tipo',
            'caja',
            'motor',
            'anio',
            'id_modelo',
            'id_version',
            'valor',
            'carga',
            'pasajeros',
            'peso',
            'lista',
            'ws_inhabilitado',
            'fecha_registro',
            'fecha_actualizacion',
            'id_user_registro',
            'id_user_actualizacion',
        ],
    ]) ?>

</div>
