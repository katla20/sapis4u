<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json; //keyla bullon
use frontend\models\Ciudad;
use kartik\select2\Select2;//keyla bullon
use kartik\depdrop\DepDrop;//http://demos.krajee.com/widget-details/depdrop
use wbraganca\dynamicform\DynamicFormWidget;


/* @var $this yii\web\View */
/* @var $model frontend\models\Zona */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zona-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php

            $id_ciudad = ArrayHelper::map(Ciudad::find()->all(), 'id_ciudad', 'nombre');

            echo $form->field($model, 'id_ciudad')->widget(Select2::classname(), [
            'data' => $id_ciudad,
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione una Ciudad...'],
            'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
	?>

    <?= $form->field($model, 'nombre')->textarea(['rows' => 6]) ?>
    
    <?php
    echo $form->field($model, 'estatus')->dropDownList([1 => 'Activo', 0 => 'Inactivo']);
	?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
