<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use frontend\models\Estado;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\CiudadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/*https://github.com/kartik-v/yii2-widgets/issues/202
https://github.com/kartik-v/yii2-grid/issues/243 
http://www.sitepoint.com/rendering-data-in-yii-2-with-gridview-and-listview/*/

$this->title = Yii::t('app', 'Ciudads');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ciudad-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Ciudad'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    
	<?php
	$modelEstado = new Estado();
	$gridColumns = [
		['class' => 'kartik\grid\SerialColumn'],
		[
			'class' => 'kartik\grid\EditableColumn',
			'attribute' => 'name',
			'pageSummary' => 'Page Total',
			'vAlign'=>'middle',
			'headerOptions'=>['class'=>'kv-sticky-column'],
			'contentOptions'=>['class'=>'kv-sticky-column'],
			'editableOptions'=>['header'=>'Name', 'size'=>'md']
		],
		[
			'attribute'=>'color',
			'value'=>function ($model, $key, $index, $widget) {
				return "<span class='badge' style='background-color: {$model->color}'> </span>  <code>" . 
					$model->color . '</code>';
			},
			'filterType'=>GridView::FILTER_COLOR,
			'vAlign'=>'middle',
			'format'=>'raw',
			'width'=>'150px',
			'noWrap'=>true
		],
		[
			'class'=>'kartik\grid\BooleanColumn',
			'attribute'=>'estatus', 
			'vAlign'=>'middle',
		],
		[
			'class' => 'kartik\grid\ActionColumn',
			'dropdown' => true,
			'vAlign'=>'middle',
			'urlCreator' => function($action, $model, $key, $index) { return '#'; },
			//'viewOptions'=>['title'=>$viewMsg, 'data-toggle'=>'tooltip'],
			//'updateOptions'=>['title'=>$updateMsg, 'data-toggle'=>'tooltip'],
			//'deleteOptions'=>['title'=>$deleteMsg, 'data-toggle'=>'tooltip'], 
		],
		['class' => 'kartik\grid\CheckboxColumn']
	];
	
	echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'columns' => $gridColumns,
		'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            
			[
				'attribute'=>'id_ciudad', 
				'value'=>'id_ciudad',
				'width'=>'100px',
			],	
            /*[
			 'attribute' => 'id_estado',
			 'value' => 'idEstado.nombre'
			],*/
			[
				'attribute'=>'id_estado', 
				'width'=>'310px',
				'value'=>'idEstado.nombre',
				'filterType'=>GridView::FILTER_SELECT2,
				'filter'=>ArrayHelper::map(Estado::find()->orderBy('nombre')->asArray()->all(), 'id_estado', 'nombre'), 
				'filterWidgetOptions'=>[
					'pluginOptions'=>['allowClear'=>true],
				],
				'filterInputOptions'=>['placeholder'=>'Estados','id' => 'estado-select2-id'],
				'group'=>true,  // enable grouping
			],
			[
			 'attribute' => 'estatus',
			 'value'=>'estatus',
			 'filterType'=>GridView::FILTER_SELECT2,
			 'filter' => array(""=>"","1"=>"Activo","0"=>"Inactivo"),
			],

            'nombre:ntext',
			/*[
				'attribute'=>'estatus', 
				'width'=>'100px',
				//'value'=>'estatus',
				'filterType'=>GridView::FILTER_SELECT2,
				'filter'=>[1 => 'Activo', 0 => 'Inactivo'], 
				'filterWidgetOptions'=>[
					'pluginOptions'=>['allowClear'=>true],
				],
				'filterInputOptions'=>['placeholder'=>'Estatus'],
				'group'=>true,  // enable grouping
			],*/
            //'fecha_registro',
            // 'fecha_modificacion',
            // 'ciudad_zona:ntext',
            // 'descripcion:ntext',
            // 'id_user_registro',
            // 'id_user_actualizacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
		'toolbar' => [
			[
				'content'=>
					Html::button('<i class="glyphicon glyphicon-plus"></i>', [
						'type'=>'button', 
						'title'=>Yii::t('app', 'Add Book'), 
						'class'=>'btn btn-success'
					]) . ' '.
					Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
						'class' => 'btn btn-default', 
						'title' => Yii::t('app', 'Reset Grid')
					]),
				
			],
			'{export}',
			'{toggleData}'
		],
		 
		'pjax' => false,
		'pjaxSettings' => [
			'options' => ['id' => 'ciudad-pjax-id'],// UNIQUE PJAX CONTAINER ID
		],
		'bordered' => false,
		'resizableColumns'=>true,
		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
		'striped' => true,
		'condensed' => false,
		'responsive' => true,
		'hover' => true,
		//'floatHeader' => true,
		//'floatHeaderOptions' => ['scrollingTop' => '50'],
		'showPageSummary' => false,
		'panel' => [
			'type' => GridView::TYPE_PRIMARY
		],
    ]);
	?>

</div>
<?php
//https://github.com/kartik-v/yii2-widgets/issues/202 resolver el problema de el pajax

$this->registerJs(
'$("#ciudad-pjax-id").on("pjax:complete", function() {
var $el = $("#estado-select2-id"), 
    options = $el.data("pluginOptions"); // select2 plugin settings saved
    jQuery.when($el.select2(window[options])).done(initSelect2Loading("estado-select2-id"));
});'
); ?>


