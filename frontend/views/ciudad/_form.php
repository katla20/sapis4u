<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json; //keyla bullon
use frontend\models\Estado;
use kartik\select2\Select2;//keyla bullon
use kartik\depdrop\DepDrop;//http://demos.krajee.com/widget-details/depdrop
use wbraganca\dynamicform\DynamicFormWidget;
/* @var $this yii\web\View */
/* @var $model frontend\models\Ciudad */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ciudad-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php

            $id_estado = ArrayHelper::map(Estado::find()->all(), 'id_estado', 'nombre');

            echo $form->field($model, 'id_estado')->widget(Select2::classname(), [
            'data' => $id_estado,
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione un Esatdo ...'],
            'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
	?>

    <?= $form->field($model, 'nombre')->textarea(['rows' => 6]) ?>
    
    <?php
    echo $form->field($model, 'estatus')->dropDownList([1 => 'Activo', 0 => 'Inactivo']);
	?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
