<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    //'bootstrap' => ['log'],
    'bootstrap' => ['debug'],
    'modules' => [
        'debug' => [
            'class' => 'yii\debug\Module',
        ],
    ],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
			'enableSession' => true,
            'authTimeout' =>5,
			//'authExpires' => 60, //sessione di 10 minuti
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
		'request'=>[
			'class' => 'common\components\Request',
			'web'=> '/frontend/web'
		],
		'urlManager' => [
				'enablePrettyUrl' => true,
				'showScriptName' => false,
		],
		'session' => [
			'name' => 'FRONTENDSESSID',
            'savePath' => __DIR__ . '/../runtime', // a temporary folder on backend
			'timeout' => 5,
			/*'cookieParams' => [
				//'httpOnly' => true,
				'path'     => '/backend/web',
			],*/
		],
    ],
    'params' => $params,
];
