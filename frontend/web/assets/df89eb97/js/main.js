
console.log('instanciando main.js');
//diseñado para sumar 1 año a la fecha introducida
sumaFecha = function(d, fecha)
{
 var Fecha = new Date();
 var sFecha = fecha || (Fecha.getDate() + "/" + (Fecha.getMonth() +1) + "/" + Fecha.getFullYear());
 var sep = sFecha.indexOf('/') != -1 ? '/' : '-';
 var aFecha = sFecha.split(sep);
 var fecha = aFecha[2]+'/'+aFecha[1]+'/'+aFecha[0];
 fecha= new Date(fecha);
 fecha.setDate(fecha.getDate()+parseInt(d));
 var anno=fecha.getFullYear();
 var mes= fecha.getMonth()+1;
 var dia= fecha.getDate();
 mes = (mes < 10) ? ("0" + mes) : mes;
 dia = (dia < 10) ? ("0" + dia) : dia;
 var fechaFinal = dia+sep+mes+sep+anno;
 return (fechaFinal);
 }

//fin de la funcion sumaFecha
/*otra sintaxis para PLUGINES
jQuery.fn.extend({
    xyz: function(){
        alert('xyz');
    }
});
*/

jQuery.fn.countElements = function() {
  /*esta funcion cuenta los elementos con el mismo nombre*/
  return $(this).length;
};

jQuery.fn.verificarIdentidad = function(id) {
  /*esta funcion determina si no esta repetido la identidad en la lista
    retorna ok cuando no existe en el arreglo de las identidades
  */
   //var ident = $.makeArray(this);//this representa el input alternativa para crear array de dom

  var ident=[];

  this.each(function() {
    ident.push($(this).val());
 });

  if($.inArray(id, ident) === -1){
    return 'ok';
  }
};

//$('a.external:first');
//$("span").eq( 0 ).( jQuery.inArray( "John", arr ) );

jQuery.fn.verificarCoberturas = function(cob_requeridas=[]) {//cob_requeridas=[2,5,7]
	//$('cobBox').verificarCoberturas();
  /*esta funcion determina si al menos una cobertura esta seleccionada
    retorna ok cuando se cumplan las condiciones establecidas
	exitira una validacion donde las coberturas que esten dentro de arreglo deben ser obligatorias y tener prima
  */
  var cob_selecc=[];//array de coberturas del producto seleccionadas
  var i=0;
	var j=0;
	var numCob=cob_requeridas.length;
    this.each(function(){//recorriendo y colocando en un array las seleccionadas
		    if($(this).is(':checked')){
				     cob_selecc.push($(this).data("cob"));
			     i++;
	        }
    });
	if(i > 0){
		if(numCob > 0){//verificar el array tiene elementos
		     $.each(cob_selecc,function(i,value){//recorriendo y verificando si existe en las coberturas obligatorias
      					if($.inArray(value, cob_requeridas) != -1){//verificado si las seleccionadas estan dentro de las obligatorias
                      j++;
      					}
          });

          if(j > 0){
            return 'ok';
          }else{
            return 'Debe Seleccionar al menos una cobertura Obligatoria';
          }
        }else{
			      return 'ok';
		    }
	}else{
	   return 'Debe Seleccionar al menos una cobertura';
	}
};


jQuery.fn.borraritem = function() {
  $(this).parent().parent().fadeTo(400, 0, function () {
        $(this).remove();
    });
    return false;
};

jQuery.fn.sumarPorcentaje = function() {
  var total=0;
   this.each(function() {
     var actual=Number($(this).formatearMonto());
     total+=actual;

  });
  return total;
};

jQuery.fn.formatearMonto = function() {
  var monto=$(this).val();
  for (var i = 0; i< monto.length; i++) {
        if(i!=(monto.length-3)){
          var caracter = monto.charAt(i);
          if( caracter == ".") {
                monto=monto.replace('.','');
          }

        }

   }

   for (var i = 0; i< monto.length; i++) {
         var caracter = monto.charAt(i);
         if( caracter == ",") {
               monto=monto.replace(',','.');
         }
    }
    
  return monto;
};
//borra registros de tablas donde este definido un anchor cuyo id comience con borrar
$(document).on('click', 'a[id*="borrar-"]', (function() {
  $(this).borraritem();
  if(this.id=='borrar-asegurados-t'){
     $("#table-asegurados").children("tbody").empty();
     $('#isTitular').removeAttr('checked');
     $('#m_carga_familiar').hide();
     $('#m_titular').show();

  }
}));//FIN DOCUMENT CLICK

/*levanta modales cuyo boton tenga el id activity-index-link debe estar la instancia del modal definida en el php
donde esta el boton creado
*/
$(document).on('click', '#activity-index-link', (function() {
    $.get(
        $(this).data('url'),
        function (data) {
            $('.modal-body').html(data);
            $('#modal').modal();
        }
    );
}));

//sumarle 1 año al fecha de poliza
$("input[name='Poliza[fecha_vigenciadesde]']").change(function(){
      $("input[name='Poliza[fecha_vigenciahasta]']").val(sumaFecha(365,$(this).val()));
});

$("#tab-cobertura,#tab-comision").children('a').on("click", function(e) {//DESABILITANDO EL EVENTO CLICK DEL ANCHOR DEL TAB DE RECIBO
	e.preventDefault();
  e.stopImmediatePropagation();
});

$("a[href='#tabs-pane-poliza'],a[href='#tabs-pane-recibo']").on("click", function(e) {//DESABILITANDO EL EVENTO CLICK DEL ANCHOR DEL TAB PRO
    e.preventDefault();
    e.stopImmediatePropagation();
});



//calculo de comisiones de las pestaña de polizas
$( "input[name='poliza-comision_total-disp']").keyup(function(event) {

  event.preventDefault();
  var porcentaje=0;
  var comision=0;
  var valor=parseFloat($(this).formatearMonto());

  if($('#poliza-comision_porcentaje').val()!=''){
      porcentaje=parseFloat($('#poliza-comision_porcentaje').val()/100);
  }

  $('#poliza-comision_vendedor').val(parseFloat(valor*porcentaje).toFixed(2));
});

//BOTONERA  PRINCIPAL DE POLIZA Y RECIBO
$("#siguiente-recibo").on("click", function(e) {
   $("a[id='breadcrumb-recibo']").removeClass('btn-default');
   $("a[id='breadcrumb-poliza']").removeClass('btn-primary');
   $("#breadcrumb-poliza").addClass('btn-default');
   $("#breadcrumb-recibo").addClass('btn-primary');

   $("#tabs-pane-poliza").addClass('ocultar');
   $("#tabs-pane-recibo").removeClass('ocultar');
});

$("#atras-poliza").on("click", function(e) {
    $("a[id='breadcrumb-recibo']").removeClass('btn-primary');
    $("a[id='breadcrumb-poliza']").removeClass('btn-default');
    $("#breadcrumb-poliza").addClass('btn-primary');
    $("#breadcrumb-recibo").addClass('btn-default');

    $("#tabs-pane-poliza").removeClass('ocultar');
    $("#tabs-pane-recibo").addClass('ocultar');

});

$(function(){

    $(document).on('click','.fc-day',function(){
        var date = $(this).attr('data-date');

		$.get('create',{'date':date},function(data){

			  $('#modal').modal('show')
			 .find('#modalContent')
			 .html(data);

			});
    });

    $('#modalButton').click(function(){
       $('#modal').modal('show')
         .find('#modalContent')
         .load($(this).attr('value'));
    });


});
