<?php
namespace common\models;

use yii;


class ConsultaPoliza {

    public static function getRenovaciones()
    {
		
        $connection = \Yii::$app->db;
        $sql = "select count(p.id_poliza) numero from poliza as p
                inner join cliente as c on (c.id_cliente=p.id_contratante)
                inner join persona as pe on (pe.id_persona=c.id_persona)
                inner join aseguradoras a on (a.id_aseguradora=p.id_aseguradora)
                
                where to_char(p.fecha_vigenciahasta,'mm-yyyy') = :fecha_2
                AND p.estatus=1";
        $command = $connection->createCommand($sql);
	    //$command->bindValue(":fecha_1", date('d-m-Y'));
        $command->bindValue(":fecha_2", date('m-Y',strtotime('+1 month')));
        $result = $command->queryOne();


        if ($result['numero'] != null){
            return $result['numero'];
        } else {
            return false;
        }
    }
    
    public static function getPerido_gracia()
    {
		
        $connection = \Yii::$app->db;
        $sql = "select count(p.id_poliza) numero from poliza as p
                inner join cliente as c on (c.id_cliente=p.id_contratante)
                inner join persona as pe on (pe.id_persona=c.id_persona)
                inner join aseguradoras a on (a.id_aseguradora=p.id_aseguradora)
                
                where p.fecha_vigenciahasta >= :fecha_1
                and p.fecha_vigenciahasta < :fecha_2";
        $command = $connection->createCommand($sql);
        $command->bindValue(":fecha_2", date('d-m-Y'));
        $command->bindValue(":fecha_1", date('d-m-Y',strtotime('-1 month')));
        $result = $command->queryOne();
        //echo $f1= date('d-m-Y');
        //echo $f2=date('d-m-Y',strtotime('-1 month'));
		
		

        if ($result['numero'] != null){
            return $result['numero'];
        } else {
            return false;
        }
    }
    
     public static function getVencidas()
    {
		
        $connection = \Yii::$app->db;
        $sql = "select count(p.id_poliza) numero from poliza as p
                inner join cliente as c on (c.id_cliente=p.id_contratante)
                inner join persona as pe on (pe.id_persona=c.id_persona)
                inner join aseguradoras a on (a.id_aseguradora=p.id_aseguradora)
                
                where p.fecha_vigenciahasta < :fecha_1 AND p.estatus=1 AND p.estatus_poliza!='Eliminada'";
        $command = $connection->createCommand($sql);
        $command->bindValue(":fecha_1", date('d-m-Y',strtotime('-1 month')));

        $result = $command->queryOne();


        if ($result['numero'] != null){
            return $result['numero'];
        } else {
            return false;
        }
    }
    
    

}

?>