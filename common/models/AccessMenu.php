<?php
namespace common\models;

use yii;

use yii\helpers\Url;//linea para el asistente de url keyla bullon


class AccessMenu {

    public static function getAcceso()
    {
		$res=array();
        $connection = \Yii::$app->db;
        $sql = 'SELECT o.id,o.rotulo,o.url,o.id_padre,o.icono
                FROM "user" u
                JOIN rol r ON u.rol_id = r.id
                JOIN rol_operacion ro ON r.id = ro.rol_id
                JOIN operacion o ON ro.operacion_id = o.id
                WHERE u.id =:id  and o.id_padre = 0 and o.etiqueta= 1
                AND menu = 1 AND o.estatus=1 order by o.orden';
        $command = $connection->createCommand($sql);
        //$command->bindValue(":user", $user);
        $user=(isset(Yii::$app->user->identity->id))? Yii::$app->user->identity->id:0;
		//(isset(Yii::$app->user->identity->id))? Yii::$app->user->identity->id:1;
        $command->bindValue(":id", $user);
        $result = $command->queryAll();
		//print_r($result); exit();

		$sqlsub1=' SELECT DISTINCT(o.rotulo),o.url,o.id_padre,o.icono
                FROM "user" u
                JOIN rol r ON u.rol_id = r.id
                JOIN rol_operacion ro ON r.id = ro.rol_id
                LEFT JOIN operacion o ON ro.operacion_id = o.id
                WHERE o.id_padre =1 and o.id_padre = 0 AND menu = 1 AND o.estatus=1 order by o.orden';

		$commandsub = $connection->createCommand($sqlsub1);

        if (count($result)!= 0){
			$res=$result;
            return $res;
        } else {
            return 1;
        }
    }
    
    
    public static function crearMenu($valpadre){
			   
        $res=array();
		$connection = \Yii::$app->db;
		$sql = 'SELECT o.id,o.rotulo,o.url,o.id_padre,o.icono
				FROM "user" u
				JOIN rol r ON u.rol_id = r.id
				JOIN rol_operacion ro ON r.id = ro.rol_id
				JOIN operacion o ON ro.operacion_id = o.id
				WHERE u.id =:id  and o.id_padre =:id_padre AND menu = 1 AND o.estatus=1 order by o.orden';
		$command = $connection->createCommand($sql);
			//$command->bindValue(":user", $user);
		$user=(isset(Yii::$app->user->identity->id))? Yii::$app->user->identity->id:1;
		$command->bindValue(":id", $user);
		$command->bindValue(":id_padre", $valpadre);
		$result = $command->queryAll();
			
		if (count($result)!= 0){
			$res=$result;
			foreach($result as $row2){
				$rotulo=$row2['rotulo'];
				//echo $url=Url::to($row2['url']);
				$url= Url::toRoute($row2['url']);
				$id_padre=$row2['id_padre'];
				$id=$row2['id'];
				$icono=$row2['icono'];
					
				$hijo=AccessMenu::numHijos($id);
					
				if($hijo > 0){
					
					echo '<ul class="treeview-menu">';
					 echo '<li> ';			
					   echo '<a href="#?>"><i class="'.$icono.'"></i>'.$rotulo.' <i class="fa fa-angle-left pull-right"></i></a></a>';	
						//echo '<ul class="treeview-menu">';
						
						   $menu2=AccessMenu::crearMenu($id);
							
						//echo '</ul>' ;
					   echo '</li>';				
					echo '</ul>';	
					
					
					
				}else{
					echo '<ul class="treeview-menu">';
					 echo '<li><a href="'.$url.'"><i class="'.$icono.'"></i> <span>'.$rotulo.'</a></span></a></li>';				
					echo '</ul>' ;				
				}
			}	
				
		
		}
                    
					
	                           
						
					
		         
	}							
			//	$i++;						
				//}
				
				//return $hijo2;
		
        
        public static function numHijos($valpadre){
			$res='';//array();
			$connection = \Yii::$app->db;
			$sql = 'SELECT count(o.id) hijos
					FROM "user" u
					JOIN rol r ON u.rol_id = r.id
					JOIN rol_operacion ro ON r.id = ro.rol_id
					JOIN operacion o ON ro.operacion_id = o.id
					WHERE u.id =:id  and o.id_padre =:id_padre AND menu = 1 AND o.estatus=1';
			$command = $connection->createCommand($sql);
			//$command->bindValue(":user", $user);
			$user=(isset(Yii::$app->user->identity->id))? Yii::$app->user->identity->id:1;
			$command->bindValue(":id", $user);
			$command->bindValue(":id_padre", $valpadre);
			$result = $command->queryAll();
			
			
				
				foreach($result as $row2){
					$res=$row2['hijos'];
				}
				return $res;
           
			
		}	
		

}
