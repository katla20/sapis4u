<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'aliases' => [
    '@foo' => '/path/to/foo',
    '@bar' => 'http://www.example.com',
    ],
	'language' => 'es',
    'components' => [
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'thousandSeparator' => '.',
            'decimalSeparator' => ',',
			       'dateFormat' => 'dd-MM-Y',
            'datetimeFormat' => 'dd-MM-YH:i:s',
            'timeFormat' => 'H:i:s',

        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
		'i18n' => [
           'translations' => [
               'app*' => [
                   'class' => 'yii\i18n\PhpMessageSource',
                   'basePath' => '@common/translations',
                   'sourceLanguage' => 'en_US',
                   'fileMap' => [
                    'app' => 'app.php',
                   ],
                ],
               'yii' => [
                   'class' => 'yii\i18n\PhpMessageSource',
                   'basePath' => '@common/translations',
                   'sourceLanguage' => 'en_US',
                   'fileMap' => [
                    'yii' => 'yii.php',
                   ],
               ],
           ],
       ],
  // echo Yii::getAlias('@foo');

	   /*'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],*/

	  /* 'view' => [
              'theme' => [
                  'pathMap' => [
                     '@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'
                  ],
              ],
            ],*/
    ],

	'modules' => [
       'gridview' =>  [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
        'debug' => [
            'class' => 'yii\debug\Module',
        ],


    ]
];
