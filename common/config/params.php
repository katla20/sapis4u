<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
	  'frontendUrl' => '/frontend/web/',
	  'backendUrl' => '/backend/web/',
    'icon-framework' => 'fa',
    'maskMoneyOptions' => [
            'prefix' => '',
            'suffix' => '',
            'affixesStay' => true,
            'thousands' => '.',
            'decimal' => ',',
            'precision' => 2,
            'allowZero' => false,
            'allowNegative' => false,
      ]

];
