<?php

namespace backend\assets;

use yii\web\AssetBundle;

class LocateAsset extends AssetBundle
{

  public $sourcePath = '@backend/dist';
  //public $baseUrl = '@webroot';
  //public $basePath = '@webroot';

  public $css = [
      'js/iCheck/all.css',
      'css/site.css',
      'css/main.css',
      'css/sprite/stylesheet_all.css',
      'js/bootstrap-toggle-master/css/bootstrap-toggle.css',
      'js/bootstrap-toggle-master/css/bootstrap-toggle.min.css',
      'js/bootstrap-toggle-master/css/bootstrap2-toggle.css',
      'js/bootstrap-toggle-master/css/bootstrap2-toggle.min.css',
  ];
  public $js = [
    'https://code.highcharts.com/highcharts.js',
    'http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.min.js',
    'https://code.highcharts.com/modules/exporting.js',
    'https://code.highcharts.com/highcharts-more.js',
    'js/Highcharts/js/highcharts.js',
  	'js/Highcharts/js/highcharts-more.js',
  	'js/Highcharts/js/exporting.js',
  	'js/jquery.validate.min.js',
    'js/main.js',
    'js/iCheck/icheck.min.js',
    'js/bootstrap-toggle-master/js/bootstrap-toggle.js',
    'js/bootstrap-toggle-master/js/bootstrap-toggle.min.js',
    'js/bootstrap-toggle-master/js/bootstrap2-toggle.js',
    'js/bootstrap-toggle-master/js/bootstrap2-toggle.min.js',
	'js/bootstrap-notify-master/bootstrap-notify.js',
	'js/bootstrap-notify-master/bootstrap-notify.min.js'

  ];
  public $depends = [
      'yii\web\YiiAsset',
      'yii\bootstrap\BootstrapAsset',


  ];
}
