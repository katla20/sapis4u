<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "facturacion_detalle".
 *
 * @property string $descripcion
 * @property string $monto
 * @property integer $id_facturacion
 *
 * @property Facturacion $idFacturacion
 */
class FacturacionDetalle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'facturacion_detalle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descripcion', 'id_facturacion'], 'required'],
            [['descripcion'], 'string'],
            [['monto'], 'number'],
            [['id_facturacion'], 'integer'],
            [['id_facturacion'], 'exist', 'skipOnError' => true, 'targetClass' => Facturacion::className(), 'targetAttribute' => ['id_facturacion' => 'id_facturacion']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'descripcion' => Yii::t('app', 'Descripcion'),
            'monto' => Yii::t('app', 'Monto'),
            'id_facturacion' => Yii::t('app', 'Id Facturacion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdFacturacion()
    {
        return $this->hasOne(Facturacion::className(), ['id_facturacion' => 'id_facturacion']);
    }
}
