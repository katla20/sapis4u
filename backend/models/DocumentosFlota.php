<?php

namespace backend\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;

/**
 * This is the model class for table "documentos".
 *
 * @property integer $id_documento
 * @property string $fecha_registro
 * @property integer $id_user_registro
 * @property integer $estatus
 * @property string $extension
 * @property string $nombre_archivo
 * @property string $categoria
 * @property integer $id_user_actualizacion
 * @property string $fecha_actualizacion
 * @property string $ruta
 * @property string $descripcion
 * @property integer $id_automovil
 * @property integer $id_persona
 *
 * @property Automovil $idAutomovil
 * @property Persona $idPersona
 */
class Documentos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $doc_files;

    public static function tableName()
    {
        return 'documentos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          [['doc_files'], 'file',
            'skipOnEmpty' =>  true,//importante para que traiga los datos
            'maxSize' => 1024*1024*1, //1 MB
            'tooBig' => 'El tamaño máximo permitido es 1MB', //Error
            'minSize' => 10, //10 Bytes
            'tooSmall' => 'El tamaño mínimo permitido son 10 BYTES', //Error
            'extensions' => 'xlsx',
            'wrongExtension' => 'El archivo {file} no contiene una extensión permitida {extensions}', //Error
            'maxFiles' => 4,
            'tooMany' => 'El máximo de archivos permitidos son {limit}', //Error
          ],
            [['doc_files'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_documento' => Yii::t('app', 'Id Documento'),
            'doc_files' => Yii::t('app', 'Seleccionar archivos'),
        ];
    }

    
    public static function find()
    {
        return new DocumentosQuery(get_called_class());
    }

    public function upload($tipo,$placa,$id)
      {
            $ruta=Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/uploads/';

                 if ($this->validate()) {
                      $i=1;
                     foreach ($this->doc_files as $file) {
                           if($file->saveAs( $ruta. $file->name.'_'.$i.'.' . $file->extension)){
							   return true;
						   }else return false;
						   
                     }    
                     
                 } else {
                     return false;
                 }

           

      }

}
