<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "forma_pago".
 *
 * @property integer $id_forma_pago
 * @property string $nombre_forma_pago
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property string $estatus
 */
class FormaPago extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'forma_pago';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre_forma_pago', 'id_user_registro'], 'required'],
            [['id_user_registro', 'id_user_actualizacion'], 'integer'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['estatus'], 'number'],
            [['nombre_forma_pago'], 'string', 'max' => 400]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_forma_pago' => Yii::t('app', 'Id Forma Pago'),
            'nombre_forma_pago' => Yii::t('app', 'Nombre Forma Pago'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'estatus' => Yii::t('app', 'Estatus'),
        ];
    }
}
