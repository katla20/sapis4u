<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "convenio".
 *
 * @property integer $id_convenio
 * @property integer $id_aseguradora
 * @property string $monto
 * @property string $fecha_desde
 * @property string $fecha_hasta
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property integer $estatus
 *
 * @property Aseguradoras $idAseguradora
 * @property ConvenioDetalle[] $convenioDetalles
 */
class Convenio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'convenio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_aseguradora', 'id_user_registro', 'id_user_actualizacion', 'estatus'], 'integer'],
            [['monto'], 'number'],
            [['fecha_desde', 'fecha_hasta', 'fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['id_aseguradora'], 'exist', 'skipOnError' => true, 'targetClass' => Aseguradoras::className(), 'targetAttribute' => ['id_aseguradora' => 'id_aseguradora']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_convenio' => Yii::t('app', 'Id Convenio'),
            'id_aseguradora' => Yii::t('app', 'Id Aseguradora'),
            'monto' => Yii::t('app', 'Monto'),
            'fecha_desde' => Yii::t('app', 'Fecha Desde'),
            'fecha_hasta' => Yii::t('app', 'Fecha Hasta'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'estatus' => Yii::t('app', 'Estatus'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAseguradora()
    {
        return $this->hasOne(Aseguradoras::className(), ['id_aseguradora' => 'id_aseguradora']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConvenioDetalles()
    {
        return $this->hasMany(ConvenioDetalle::className(), ['id_convenio' => 'id_convenio']);
    }
}
