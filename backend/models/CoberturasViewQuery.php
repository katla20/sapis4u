<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[CoberturasView]].
 *
 * @see CoberturasView
 */
class CoberturasViewQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return CoberturasView[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CoberturasView|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}