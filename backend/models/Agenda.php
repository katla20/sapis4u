<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "agenda".
 *
 * @property integer $id_agenda
 * @property string $titulo
 * @property string $descripcion
 * @property string $fecha_creacion
 * @property integer $id_relacion
 */
class Agenda extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agenda';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['titulo', 'descripcion'], 'required'],
            [['fecha_creacion'], 'safe'],
            [['id_relacion'], 'integer'],
            [['titulo'], 'string', 'max' => 100],
            [['descripcion'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_agenda' => Yii::t('app', 'Id Agenda'),
            'titulo' => Yii::t('app', 'Titulo'),
            'descripcion' => Yii::t('app', 'Descripcion'),
            'fecha_creacion' => Yii::t('app', 'Fecha Creacion'),
            'id_relacion' => Yii::t('app', 'Id Relacion'),
        ];
    }
    
    public static function getAgendas()
    {
		 $result=array();
        $connection = \Yii::$app->db;
        $fecha=date('d-m-Y'); 
         $fecha=date("d-m-Y", strtotime($fecha));
        $sql = "SELECT count(id_agenda) AS cantidad from agenda where fecha_creacion='$fecha'";
        $command = $connection->createCommand($sql);
		 //   $anio=date("Y"); 
       // $command->bindValue(":anio", $anio);
        $result = $command->queryOne();

        if (count($result)!= NULL){
			        return $result['cantidad'];
        }else return 0;
    }
    
    public static function BuscarAgenda()
    {
		 $result=array();
        $connection = \Yii::$app->db;
        $sql = "SELECT to_char(fecha_creacion,'yyyy/mm/dd') AS fecha_creacion,id_agenda,titulo,descripcion from agenda";
        $command = $connection->createCommand($sql);
		 //   $anio=date("Y"); 
       // $command->bindValue(":anio", $anio);
        $result = $command->queryAll();

        return $result;
    }
}
