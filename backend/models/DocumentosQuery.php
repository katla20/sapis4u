<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[Documentos]].
 *
 * @see Documentos
 */
class DocumentosQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Documentos[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Documentos|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}