<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "carga_familiar_view".
 *
 * @property string $nombre_completo
 * @property string $nombre
 * @property string $segundo_nombre
 * @property string $apellido
 * @property string $segundo_apellido
 * @property string $parentesco
 * @property string $identificacion
 * @property string $fecha_nacimiento
 * @property string $sexo
 * @property integer $id_titular
 * @property integer $id_beneficiario
 * @property integer $cliente_estatus
 * @property integer $carga_familiar_estatus
 */
class CargaFamiliarView extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'carga_familiar_view';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre_completo'], 'string'],
            [['fecha_nacimiento'], 'safe'],
            [['id_titular', 'id_beneficiario', 'cliente_estatus', 'carga_familiar_estatus'], 'integer'],
            [['nombre', 'segundo_nombre', 'apellido', 'segundo_apellido'], 'string', 'max' => 300],
            [['parentesco'], 'string', 'max' => 20],
            [['identificacion'], 'string', 'max' => 12],
            [['sexo'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nombre_completo' => Yii::t('app', 'Nombre Completo'),
            'nombre' => Yii::t('app', 'Nombre'),
            'segundo_nombre' => Yii::t('app', 'Segundo Nombre'),
            'apellido' => Yii::t('app', 'Apellido'),
            'segundo_apellido' => Yii::t('app', 'Segundo Apellido'),
            'parentesco' => Yii::t('app', 'Parentesco'),
            'identificacion' => Yii::t('app', 'Identificacion'),
            'fecha_nacimiento' => Yii::t('app', 'Fecha Nacimiento'),
            'sexo' => Yii::t('app', 'Sexo'),
            'id_titular' => Yii::t('app', 'Id Titular'),
            'id_beneficiario' => Yii::t('app', 'Id Beneficiario'),
            'cliente_estatus' => Yii::t('app', 'Cliente Estatus'),
            'carga_familiar_estatus' => Yii::t('app', 'Carga Familiar Estatus'),
        ];
    }
}
