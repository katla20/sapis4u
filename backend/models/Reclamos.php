<?php

namespace backend\models;
use backend\models\InterReclamoPartes;
use backend\models\PartesVehiculos;


use Yii;

/**
 * This is the model class for table "reclamos".
 *
 * @property integer $id_reclamo
 * @property integer $id_recibo
 * @property integer $id_tipo_reclamo
 * @property string $descripcion
 * @property string $monto
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property integer $estatus
 * @property string $fecha_ocurrencia
 * @property string $fecha_notificacion
 *
 * @property InterReclamoPartes[] $interReclamoPartes
 * @property Recibo $idRecibo
 */
class Reclamos extends \yii\db\ActiveRecord
{
    public  $parte,$poliza,$hi,$user,$aseguradora,$ramo,$contratante,$id_aseguradora,
            $id_ramo,$id_contratante,$id_producto,$producto,$recibo,$tipo_reclamo,$nombre_completo,$placa,$identidad;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reclamos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_recibo', 'id_tipo_reclamo', 'poliza','id_user_registro', 'id_user_actualizacion', 'estatus','causa','id_user_analista','id_automovil','id_beneficiario'], 'integer'],
            [['descripcion','numero_reclamo','lugar_ocurrencia'], 'string'],
            [['monto'], 'number'],
			['parte', 'safe'],
            [['fecha_registro', 'fecha_actualizacion', 'fecha_ocurrencia', 'fecha_notificacion','fecha_declaracion'], 'safe'],
			[['numero_reclamo', 'fecha_ocurrencia', 'fecha_notificacion','id_tipo_reclamo'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_reclamo' => Yii::t('app', 'Id'),
            'id_recibo' => Yii::t('app', 'Recibo'),
            'id_tipo_reclamo' => Yii::t('app', 'Tipo Reclamo'),
			'idTipoReclamos.nombre' => Yii::t('app', 'Tipo Reclamo'),
			'idRecibo.nro_recibo' => Yii::t('app', '# Recibo'),
            'descripcion' => Yii::t('app', ''),
            'monto' => Yii::t('app', 'Monto'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'estatus' => Yii::t('app', 'Estatus'),
            'fecha_ocurrencia' => Yii::t('app', 'Fecha Ocurrencia'),
            'fecha_notificacion' => Yii::t('app', 'Fecha Notificacion'),
			'parte' => Yii::t('app', ''),
			'numero_reclamo' => Yii::t('app', 'Numero-Reclamo'),
			'causa' => Yii::t('app', 'Causa'),
            'producto'=>Yii::t('app', 'Producto '),
            'id_producto'=>Yii::t('app', 'Producto '),
            'id_aseguradora' => Yii::t('app', 'Aseguradora'),
            'id_contratante' => Yii::t('app', 'Contratante'),
            'poliza' => Yii::t('app', 'Poliza'),
            'nombre_completo' => Yii::t('app', 'Contratante'),
			'id_user_analista' => Yii::t('app', 'Analista Caso'),
            'fecha_declaracion' => Yii::t('app', 'Fecha Declaracion'),
			'lugar_ocurrencia' => Yii::t('app', 'Lugar de Ocurrencia'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

	public function afterSave($insert, $changedAttributes){
		\Yii::$app->db->createCommand()->delete('inter_reclamo_partes', 'id_reclamo = '.(int) $this->id_reclamo)->execute();

    if(!empty($this->parte)){
    		foreach ($this->parte as $id) {
    			$ro = new InterReclamoPartes();
    			$ro->id_reclamo = $this->id_reclamo;
    			$ro->id_parte = $id;
    			$ro->save();
    		}
      }
	}

    public function getInterReclamoPartes()
    {
        return $this->hasMany(InterReclamoPartes::className(), ['id_reclamo' => 'id_reclamo']);
    }

	public function getPartesVehiculos()
	{
		return $this->hasMany(PartesVehiculos::className(), ['id_parte' => 'id_parte'])
			->viaTable('inter_reclamo_partes', ['id_reclamo' => 'id_reclamo']);
	}

	public function getPartesVehiculosList()
	{
		return $this->getPartesVehiculos()->asArray();
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRecibo()
    {
        return $this->hasOne(Recibo::className(), ['id_recibo' => 'id_recibo']);
    }

	public function getIdTipoReclamos()
    {
        return $this->hasOne(TipoReclamos::className(), ['id_tipo_reclamo' => 'id_tipo_reclamo']);
    }

    public function getTipoRamo($id)
    {
   	    $res=array();
        $connection = \Yii::$app->db;
        $sql = "select poliza.id_ramo 
		         from reclamos
                inner join recibo on (recibo.id_recibo=reclamos.id_recibo)
                inner join poliza on (poliza.id_poliza=recibo.id_poliza)
                where id_reclamo=:id";
        $command = $connection->createCommand($sql);
        $command->bindValue(":id", $id);
        $result = $command->queryAll();
		
		//print_r($result);exit();

        if (count($result)!= 0){

           return $result[0]['id_ramo'];

        } else{
            return 0;

        }

    }
}
