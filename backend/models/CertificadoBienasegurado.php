<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "certificado_bienasegurado".
 *
 * @property integer $id_certificado_bienasegurado
 * @property integer $id_certificado
 * @property string $c_biensegurado_indole
 * @property string $c_bienasegurado_riesgo
 * @property string $estatus
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property integer $id_direccion
 *
 * @property Certificado $idCertificado
 * @property Direccion $idDireccion
 */
class CertificadoBienasegurado extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'certificado_bienasegurado';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_certificado', 'id_user_registro', 'id_direccion'], 'required'],
            [['id_certificado', 'id_user_registro', 'id_user_actualizacion', 'id_direccion'], 'integer'],
            [['c_biensegurado_indole', 'c_bienasegurado_riesgo'], 'string'],
            [['estatus'], 'number'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['id_certificado'], 'exist', 'skipOnError' => true, 'targetClass' => Certificado::className(), 'targetAttribute' => ['id_certificado' => 'id_certificado']],
            [['id_direccion'], 'exist', 'skipOnError' => true, 'targetClass' => Direccion::className(), 'targetAttribute' => ['id_direccion' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_certificado_bienasegurado' => Yii::t('app', 'Id Certificado Bienasegurado'),
            'id_certificado' => Yii::t('app', 'Id Certificado'),
            'c_biensegurado_indole' => Yii::t('app', 'C Biensegurado Indole'),
            'c_bienasegurado_riesgo' => Yii::t('app', 'C Bienasegurado Riesgo'),
            'estatus' => Yii::t('app', 'Estatus'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'id_direccion' => Yii::t('app', 'Id Direccion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCertificado()
    {
        return $this->hasOne(Certificado::className(), ['id_certificado' => 'id_certificado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDireccion()
    {
        return $this->hasOne(Direccion::className(), ['id' => 'id_direccion']);
    }
}
