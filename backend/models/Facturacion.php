<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "facturacion".
 *
 * @property integer $id_facturacion
 * @property string $numero_factura
 * @property string $numero_control
 * @property string $monto_iva
 * @property string $monto_islr
 * @property string $monto_subtotal
 * @property string $monto_recibos
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property string $descripcion
 * @property integer $estatus_factura
 * @property integer $id_aseguradora
 * @property string $monto_factura
 * @property string $otro_monto
 * @property string $comentario
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property string $fecha_factura
 * @property string $fecha_recepcion
 *
 * @property Aseguradoras $idAseguradora
 * @property FacturacionDetalle[] $facturacionDetalles
 * @property InterReciboFactura[] $interReciboFacturas
 */
class Facturacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $recibos,$recibos_numero,$aseguradora;
    public static function tableName()
    {
        return 'facturacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['numero_factura', 'monto_recibos', 'id_user_registro', 'id_aseguradora', 'fecha_registro','recibos_numero'], 'required'],
            [['numero_factura', 'numero_control', 'monto_iva', 'monto_islr', 'monto_subtotal', 'monto_recibos', 'monto_factura', 'otro_monto'], 'number'],
            [['id_user_registro', 'id_user_actualizacion', 'estatus_factura', 'id_aseguradora'], 'integer'],
            [['descripcion', 'comentario'], 'string'],
            [['fecha_registro', 'fecha_actualizacion', 'fecha_factura', 'fecha_recepcion'], 'safe'],
            [['id_aseguradora'], 'exist', 'skipOnError' => true, 'targetClass' => Aseguradoras::className(), 'targetAttribute' => ['id_aseguradora' => 'id_aseguradora']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_facturacion' => Yii::t('app', 'Id Facturacion'),
            'numero_factura' => Yii::t('app', 'Numero Factura'),
            'numero_control' => Yii::t('app', 'Numero Control'),
            'monto_iva' => Yii::t('app', 'Monto Iva'),
            'monto_islr' => Yii::t('app', 'Monto Islr'),
            'monto_subtotal' => Yii::t('app', 'Monto Subtotal'),
            'monto_recibos' => Yii::t('app', 'Monto Recibos'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'descripcion' => Yii::t('app', 'Descripcion de Recibos'),
            'estatus_factura' => Yii::t('app', 'Estatus Factura'),
            'id_aseguradora' => Yii::t('app', 'Aseguradora'),
            'monto_factura' => Yii::t('app', 'Monto Factura'),
            'otro_monto' => Yii::t('app', 'Otro Monto'),
            'comentario' => Yii::t('app', 'Comentario'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'fecha_factura' => Yii::t('app', 'Fecha Factura'),
            'fecha_recepcion' => Yii::t('app', 'Fecha Recepcion'),
            'recibos_numero' => Yii::t('app', 'Numero de Recibos'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAseguradora()
    {
        return $this->hasOne(Aseguradoras::className(), ['id_aseguradora' => 'id_aseguradora']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturacionDetalles()
    {
        return $this->hasMany(FacturacionDetalle::className(), ['id_facturacion' => 'id_facturacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterReciboFacturas()
    {
        return $this->hasMany(InterReciboFactura::className(), ['id_facturacion' => 'id_facturacion']);
    }

    public function afterSave($insert, $changedAttributes)
    {
 		\Yii::$app->db->createCommand()->delete('inter_recibo_factura', 'id_facturacion = '.(int) $this->id_facturacion)->execute();

 		 if(!empty($this->recibos)){
 				foreach ($this->recibos as $id) {
 					$ro = new InterReciboFactura();
					  $re = Recibo::findOne($id);
								$ro->id_facturacion = $this->id_facturacion;
								$ro->id_recibo = $id;
								$ro->save();
					  $re->facturado=1;
					  $re->save();

 				}
 			}

 	   }

     public function getRecibos()
     {
     		return $this->hasMany(Recibo::className(), ['id_recibo' => 'id_recibo'])
     			->viaTable('inter_recibo_factura', ['id_facturacion' => 'id_facturacion']);
     }

     public function getRecibosList()
     {
     		return $this->getRecibos()->asArray();
     }
}
