<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pago".
 *
 * @property integer $id_pago
 * @property integer $id_forma_pago
 * @property integer $id_banco
 * @property integer $id_modalidad_pago
 * @property string $fecha_pago
 * @property string $monto
 * @property string $fecha_vencimiento
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property string $fecha_actualizacion
 * @property string $estatus
 * @property string $estatus_pago
 * @property string $fecha_registro
 * @property string $referencia
 * @property string $total_cuotas
 * @property string $cuota
 * @property string $dia_pago
 * @property string $observacion
 */
class Pago extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $contador,$id_poliza,$estatus_recibo,$monto_inicial;
    public static function tableName()
    {
        return 'pago';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_modalidad_pago'], 'required'],
            [['id_forma_pago', 'id_banco', 'id_modalidad_pago', 'id_user_registro', 'id_user_actualizacion'], 'integer'],
            [['fecha_pago', 'fecha_vencimiento', 'fecha_actualizacion', 'fecha_registro'], 'safe'],
            [['estatus', 'total_cuotas', 'cuota', 'dia_pago'], 'number'],
            [['monto','estatus_pago', 'referencia','observacion'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pago' => Yii::t('app', 'Id Pago'),
            'id_forma_pago' => Yii::t('app', 'Forma Pago'),
            'id_banco' => Yii::t('app', 'Banco'),
            'id_modalidad_pago' => Yii::t('app', 'Modalidad Pago'),
            'fecha_pago' => Yii::t('app', 'Fecha Pago'),
            'monto' => Yii::t('app', 'Monto de Fracciones/Cuotas'),
            'fecha_vencimiento' => Yii::t('app', 'Fecha vcto. pago de inicial'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'estatus' => Yii::t('app', 'Estatus'),
            'estatus_pago' => Yii::t('app', 'Estatus Pago'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'referencia' => Yii::t('app', 'Referencia'),
            'total_cuotas' => Yii::t('app', 'Total Cuotas'),
            'cuota' => Yii::t('app', 'Cuota'),
            'dia_pago' => Yii::t('app', 'Dia Pago'),
		    'observacion' => Yii::t('app', 'Observaciones'),
        ];
    }



    /**
     * @inheritdoc
     * @return PagoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PagoQuery(get_called_class());
    }

    public static function findMontoPagado($id){
           $connection = \Yii::$app->db;
        $sql2 = "select SUM (pago.monto) monto_pagado
                    FROM pago
                    INNER JOIN pago_recibo USING (id_pago)
                    INNER JOIN recibo USING (id_recibo)
                WHERE recibo.id_recibo=:id
                AND pago.estatus_pago='Pagado'
                AND pago.estatus=1 AND recibo.estatus=1";

      $command = $connection->createCommand($sql2);
      $command->bindValue(":id", $id);
      $result=$command->queryOne();
      return $result['monto_pagado'];
    }

}
