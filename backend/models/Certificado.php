<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "certificado".
 *
 * @property integer $id_certificado
 * @property integer $id_recibo
 * @property integer $id_titular
 * @property integer $estatus
 * @property integer $id_automovil
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property integer $id_producto
 * @property string $deducible
 * @property string $nro_certificado
 *
 * @property Automovil $idAutomovil
 * @property Cliente $idTitular
 * @property Recibo $idRecibo
 * @property CertificadoAsegurados[] $certificadoAsegurados
 * @property CertificadoBeneficiarios[] $certificadoBeneficiarios
 * @property CertificadoDetalle[] $certificadoDetalles
 */
class Certificado extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'certificado';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_recibo', 'id_titular', 'nro_certificado'], 'required'],
            [['id_recibo', 'id_titular', 'estatus', 'id_automovil', 'id_user_registro', 'id_user_actualizacion', 'id_producto'], 'integer'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['deducible'], 'number'],
            [['nro_certificado'], 'string', 'max' => 10],
            [['id_automovil'], 'exist', 'skipOnError' => true, 'targetClass' => Automovil::className(), 'targetAttribute' => ['id_automovil' => 'id_automovil']],
            [['id_titular'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['id_titular' => 'id_cliente']],
            [['id_recibo'], 'exist', 'skipOnError' => true, 'targetClass' => Recibo::className(), 'targetAttribute' => ['id_recibo' => 'id_recibo']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_certificado' => Yii::t('app', 'Id Certificado'),
            'id_recibo' => Yii::t('app', 'Id Recibo'),
            'id_titular' => Yii::t('app', 'Id Titular'),
            'estatus' => Yii::t('app', 'Estatus'),
            'id_automovil' => Yii::t('app', 'Id Automovil'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'id_producto' => Yii::t('app', 'Id Producto'),
            'deducible' => Yii::t('app', 'Deducible'),
            'nro_certificado' => Yii::t('app', 'Nro Certificado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAutomovil()
    {
        return $this->hasOne(Automovil::className(), ['id_automovil' => 'id_automovil']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTitular()
    {
        return $this->hasOne(Cliente::className(), ['id_cliente' => 'id_titular']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRecibo()
    {
        return $this->hasOne(Recibo::className(), ['id_recibo' => 'id_recibo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCertificadoAsegurados()
    {
        return $this->hasMany(CertificadoAsegurados::className(), ['id_certificado' => 'id_certificado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCertificadoBeneficiarios()
    {
        return $this->hasMany(CertificadoBeneficiarios::className(), ['id_certificado' => 'id_certificado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCertificadoDetalles()
    {
        return $this->hasMany(CertificadoDetalle::className(), ['id_certificado' => 'id_certificado']);
    }
}
