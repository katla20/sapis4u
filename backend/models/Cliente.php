<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "cliente".
 *
 * @property integer $id_persona
 * @property integer $estatus
 * @property integer $id_cliente
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 *
 * @property CargaFamiliar[] $cargaFamiliars
 * @property CargaFamiliar[] $cargaFamiliars0
 * @property CertificadoAsegurados[] $certificadoAsegurados
 * @property CertificadoBeneficiarios[] $certificadoBeneficiarios
 * @property Persona $idPersona
 * @property User $idUserRegistro
 * @property User $idUserActualizacion
 * @property DatosBancarios[] $datosBancarios
 * @property Poliza[] $polizas
 */
class Cliente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	public $nombre_completo;
    public static function tableName()
    {
        return 'cliente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_persona', 'fecha_registro'], 'required'],
            [['id_persona', 'estatus', 'id_user_registro', 'id_user_actualizacion'], 'integer'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['id_persona'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(), 'targetAttribute' => ['id_persona' => 'id_persona']],
            [['id_user_registro'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user_registro' => 'id']],
            [['id_user_actualizacion'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user_actualizacion' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_persona' => Yii::t('app', 'Id Persona'),
            'estatus' => Yii::t('app', 'Estatus'),
            'id_cliente' => Yii::t('app', 'Id Cliente'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCargaFamiliars()
    {
        return $this->hasMany(CargaFamiliar::className(), ['id_beneficiario' => 'id_cliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCargaFamiliars0()
    {
        return $this->hasMany(CargaFamiliar::className(), ['id_titular' => 'id_cliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCertificadoAsegurados()
    {
        return $this->hasMany(CertificadoAsegurados::className(), ['id_cliente' => 'id_cliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCertificadoBeneficiarios()
    {
        return $this->hasMany(CertificadoBeneficiarios::className(), ['id_cliente' => 'id_cliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPersona()
    {
        return $this->hasOne(Persona::className(), ['id_persona' => 'id_persona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUserRegistro()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user_registro']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUserActualizacion()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user_actualizacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDatosBancarios()
    {
        return $this->hasMany(DatosBancarios::className(), ['id_cliente' => 'id_cliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolizas()
    {
        return $this->hasMany(Poliza::className(), ['id_contratante' => 'id_cliente']);
    }
}
