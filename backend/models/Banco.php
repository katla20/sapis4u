<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "banco".
 *
 * @property integer $id_banco
 * @property string $nombre_banco
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property string $estatus
 *
 * @property DatosBancarios[] $datosBancarios
 */
class Banco extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banco';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre_banco', 'id_user_registro'], 'required'],
            [['id_user_registro', 'id_user_actualizacion'], 'integer'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['estatus'], 'number'],
            [['nombre_banco'], 'string', 'max' => 400]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_banco' => Yii::t('app', 'Id Banco'),
            'nombre_banco' => Yii::t('app', 'Nombre Banco'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'estatus' => Yii::t('app', 'Estatus'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDatosBancarios()
    {
        return $this->hasMany(DatosBancarios::className(), ['id_banco' => 'id_banco']);
    }
}
