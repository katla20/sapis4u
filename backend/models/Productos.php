<?php

namespace backend\models;
use backend\models\Aseguradoras;
use backend\models\InterAsegProductos;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property integer $id_producto
 * @property string $nombre
 * @property integer $id_ramo
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property integer $estatus
 *
 * @property InterAsegProducto[] $interAsegProductos
 * @property InterProdCob[] $interProdCobs
 * @property Poliza[] $polizas
 * @property Ramos $idRamo
 */
class Productos extends \yii\db\ActiveRecord
{
	public $aseguradoras;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_ramo', 'id_user_registro', 'id_user_actualizacion', 'estatus'], 'integer'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
			      ['aseguradoras', 'safe'],
            [['nombre'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_producto' => Yii::t('app', 'Id'),
            'nombre' => Yii::t('app', 'Producto'),
            'id_ramo' => Yii::t('app', 'Ramo'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'estatus' => Yii::t('app', 'Estatus'),
        ];
    }

	public function afterSave($insert, $changedAttributes){
		\Yii::$app->db->createCommand()->delete('inter_aseg_producto', 'id_producto = '.(int) $this->id_producto)->execute();

		foreach ($this->aseguradoras as $id) {
			$ro = new InterAsegProducto();
			$ro->id_producto = $this->id_producto;
			$ro->id_aseguradora= $id;
			$ro->save();
		}
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterAsegProductos()
    {
        return $this->hasMany(InterAsegProducto::className(), ['id_producto' => 'id_producto']);
    }

	public function getAseguradoras()
	{
		return $this->hasMany(Aseguradoras::className(), ['id_aseguradora' => 'id_aseguradora'])
			->viaTable('inter_aseg_producto', ['id_producto' => 'id_producto']);
	}

	public function getAseguradorasList()
	{
		return $this->getAseguradoras()->asArray();
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterProdCobs()
    {
        return $this->hasMany(InterProdCob::className(), ['id_producto' => 'id_producto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolizas()
    {
        return $this->hasMany(Poliza::className(), ['id_producto' => 'id_producto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRamo()
    {
        return $this->hasOne(Ramos::className(), ['id_ramo' => 'id_ramo']);
    }
}
