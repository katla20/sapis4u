<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Poliza;

/**
 * PolizaSearch represents the model behind the search form about `backend\models\Poliza`.
 */
class PolizaSearch extends Poliza
{
    /**
     * @inheritdoc
     */
    public $fecha_desde,$fecha_hasta;

    public function rules()
    {
        return [
            [['id_poliza', 'id_user_registro', 'id_user_actualizacion','id_aseguradora','id_ramo','id_producto','id_contratante'], 'integer'],
            [['numero_poliza', 'fecha_vigenciadesde', 'nombre_completo','id_producto','fecha_vigenciahasta', 'fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['id_tipo', 'estatus'], 'number'],
            [['estatus_poliza','placa','aseguradora','producto'], 'string', 'max' => 100]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_poliza' => Yii::t('app', 'Poliza'),
            'numero_poliza' => Yii::t('app', 'Numero Poliza'),
            'fecha_vigenciadesde' => Yii::t('app', 'Fecha'),
            'id_producto' => Yii::t('app', 'Producto'),
            'id_ramo' => Yii::t('app', 'Ramo'),
            'id_contratante' => Yii::t('app', 'Contratante'),
            'producto' => Yii::t('app', 'Producto'),
            'asegurardora' => Yii::t('app', 'Asegurardora')
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
   /* public function search($params,$ramo,$tipo)
    {


		 $query= Poliza::find()->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
                               ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
                               ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
                               ->innerJoin('ramos', 'ramos.id_ramo = poliza.id_ramo')
                              ->where("poliza.estatus = :status AND poliza.id_ramo = :ramo AND poliza.id_tipo= :tipo",[':status' => 1,':ramo'=> $ramo,':tipo' => $tipo])
                       ->select(["poliza.numero_poliza","poliza.id_poliza","(persona.nombre ||' '||persona.apellido) as nombre_completo",
                                 "persona.identificacion","aseguradoras.nombre AS aseguradora","aseguradoras.id_aseguradora",
                                 "poliza.fecha_vigenciadesde","poliza.fecha_vigenciahasta","poliza.estatus",
								"ramos.nombre AS ramo"," (CASE WHEN poliza.estatus_poliza ='Anulada' THEN poliza.estatus_poliza
								         WHEN poliza.estatus_poliza <> 'Anulada' and poliza.fecha_vigenciahasta < NOW() THEN 'Vencida'
 										 WHEN poliza.estatus_poliza <> 'Anulada' and poliza.fecha_vigenciahasta >= NOW() THEN 'Vigente'
										 ELSE poliza.estatus_poliza END) AS estatus_poliza"]);



        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
              'pageSize' => 12,
            ],
			       'sort' =>
       			   ['attributes' =>
       				   [
         				  'ramo',
         					'id_aseguradora',
         					'nombre_completo',
         					'numero_poliza',
         					'fecha_vigenciahasta',
							    'fecha_vigenciadesde',
							    'estatus_poliza',
							    'estatus',
       				   ]
       			   ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_poliza' => $this->id_poliza,
            'id_tipo' => $this->id_tipo,
            'fecha_vigenciadesde' => $this->fecha_vigenciadesde,
            'fecha_vigenciahasta' => $this->fecha_vigenciahasta,
            'poliza.estatus' => $this->estatus,
			'aseguradoras.id_aseguradora'=>$this->id_aseguradora,
            'fecha_registro' => $this->fecha_registro,
            'fecha_actualizacion' => $this->fecha_actualizacion,
            'id_user_registro' => $this->id_user_registro,
            'id_user_actualizacion' => $this->id_user_actualizacion,
            'ramos.id_ramo' => $this->id_ramo,
        ]);

        //$query->andFilterWhere(['like', 'estatus_poliza', $this->estatus_poliza]);

		if (isset($this->estatus_poliza)) {
			 $fecha_hasta2=date("d-m-Y");
			 //echo $this->estatus_poliza; exit();
			 if($this->estatus_poliza=='Vencida'){
				$query->andFilterWhere(['<', "poliza.fecha_vigenciahasta", $fecha_hasta2]);
			 }else if($this->estatus_poliza=='Vigente'){
				 $query->andFilterWhere(['>=', "poliza.fecha_vigenciahasta", $fecha_hasta2]);
			 }else{

				$query->andFilterWhere(['like', 'estatus_poliza', $this->estatus_poliza]);
			 }
		   }
		   
		$this->numero_poliza=strtoupper($this->numero_poliza);   
        $query->andFilterWhere(['like', '(upper(numero_poliza))', $this->numero_poliza]);
		$this->nombre_completo=strtoupper($this->nombre_completo);
		$query->andFilterWhere(['or',
            ['like', '(upper(persona.apellido))', $this->nombre_completo],
            ['like', '(upper(persona.nombre))', $this->nombre_completo],
			['like', '(upper(persona.segundo_nombre))', $this->nombre_completo],
			['like', '(upper(persona.segundo_apellido))', $this->nombre_completo]]);
		
		$query->andFilterWhere(['like', 'persona.identificacion', $this->identificacion]);

        return $dataProvider;
    }*/
    
    public function search($params,$ramo,$tipo)
    {

        if($ramo==2){
            $query= Poliza::find()->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
                               ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
                               ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora AND aseguradoras.id_pais=10')
                               ->innerJoin('ramos', 'ramos.id_ramo = poliza.id_ramo')
                              ->where("poliza.estatus = :status AND poliza.id_ramo = :ramo AND poliza.id_tipo= :tipo",[':status' => 1,':ramo'=> $ramo,':tipo' => $tipo])
                       ->select(["poliza.numero_poliza","poliza.id_poliza","(persona.nombre ||' '||persona.apellido) as nombre_completo",
                                 "persona.identificacion","aseguradoras.nombre AS aseguradora","aseguradoras.id_aseguradora",
                                 "poliza.fecha_vigenciadesde","poliza.fecha_vigenciahasta","poliza.estatus",
								"ramos.nombre AS ramo"," (CASE WHEN poliza.estatus_poliza ='Anulada' THEN poliza.estatus_poliza
								         WHEN poliza.estatus_poliza <> 'Anulada' and poliza.fecha_vigenciahasta < NOW() THEN 'Vencida'
 										 WHEN poliza.estatus_poliza <> 'Anulada' and poliza.fecha_vigenciahasta >= NOW() THEN 'Vigente'
										 ELSE poliza.estatus_poliza END) AS estatus_poliza"]);   
            
        }elseif($ramo==1){
		  $query= Poliza::find()->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
                               ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
                               ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
                               ->innerJoin('ramos', 'ramos.id_ramo = poliza.id_ramo')
                               ->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza')
                    		    ->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo')
                    		    ->innerJoin('automovil', 'automovil.id_automovil= certificado.id_automovil')
                              ->where("poliza.estatus = :status AND poliza.id_ramo = :ramo AND poliza.id_tipo= :tipo",[':status' => 1,':ramo'=> $ramo,':tipo' => $tipo])
                       ->select(["poliza.numero_poliza","poliza.id_poliza","(persona.nombre ||' '||persona.apellido) as nombre_completo",
                                 "persona.identificacion","aseguradoras.nombre AS aseguradora","aseguradoras.id_aseguradora",
                                 "poliza.fecha_vigenciadesde","poliza.fecha_vigenciahasta","poliza.estatus",
								"ramos.nombre AS ramo"," (CASE WHEN poliza.estatus_poliza ='Anulada' THEN poliza.estatus_poliza
								         WHEN poliza.estatus_poliza <> 'Anulada' and poliza.fecha_vigenciahasta < NOW() THEN 'Vencida'
 										 WHEN poliza.estatus_poliza <> 'Anulada' and poliza.fecha_vigenciahasta >= NOW() THEN 'Vigente'
										 ELSE poliza.estatus_poliza END) AS estatus_poliza,placa"]);
        }elseif($ramo==4){
		  $query= Poliza::find()->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
                               ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
                               ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
                               ->innerJoin('ramos', 'ramos.id_ramo = poliza.id_ramo')
                              ->where("poliza.estatus = :status AND poliza.id_ramo = :ramo AND poliza.id_tipo= :tipo",[':status' => 1,':ramo'=> $ramo,':tipo' => $tipo])
                       ->select(["poliza.numero_poliza","poliza.id_poliza","(persona.nombre ||' '||persona.apellido) as nombre_completo",
                                 "persona.identificacion","aseguradoras.nombre AS aseguradora","aseguradoras.id_aseguradora",
                                 "poliza.fecha_vigenciadesde","poliza.fecha_vigenciahasta","poliza.estatus",
								"ramos.nombre AS ramo"," (CASE WHEN poliza.estatus_poliza ='Anulada' THEN poliza.estatus_poliza
								         WHEN poliza.estatus_poliza <> 'Anulada' and poliza.fecha_vigenciahasta < NOW() THEN 'Vencida'
 										 WHEN poliza.estatus_poliza <> 'Anulada' and poliza.fecha_vigenciahasta >= NOW() THEN 'Vigente'
										 ELSE poliza.estatus_poliza END) AS estatus_poliza"]);  
        }elseif($ramo==3){
		  $query= Poliza::find()->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
                               ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
                               ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
                               ->innerJoin('ramos', 'ramos.id_ramo = poliza.id_ramo')
                              ->where("poliza.estatus = :status AND poliza.id_ramo = :ramo AND poliza.id_tipo= :tipo",[':status' => 1,':ramo'=> $ramo,':tipo' => $tipo])
                       ->select(["poliza.numero_poliza","poliza.id_poliza","(persona.nombre ||' '||persona.apellido) as nombre_completo",
                                 "persona.identificacion","aseguradoras.nombre AS aseguradora","aseguradoras.id_aseguradora",
                                 "poliza.fecha_vigenciadesde","poliza.fecha_vigenciahasta","poliza.estatus",
								"ramos.nombre AS ramo"," (CASE WHEN poliza.estatus_poliza ='Anulada' THEN poliza.estatus_poliza
								         WHEN poliza.estatus_poliza <> 'Anulada' and poliza.fecha_vigenciahasta < NOW() THEN 'Vencida'
 										 WHEN poliza.estatus_poliza <> 'Anulada' and poliza.fecha_vigenciahasta >= NOW() THEN 'Vigente'
										 ELSE poliza.estatus_poliza END) AS estatus_poliza"]);   
        }elseif($ramo==5){
            
            
                $query= Poliza::find()->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
                               ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
                               ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora AND aseguradoras.id_pais!=10 ')
                               ->innerJoin('ramos', 'ramos.id_ramo = poliza.id_ramo')
                              ->where("poliza.estatus = :status AND poliza.id_ramo = :ramo AND poliza.id_tipo= :tipo",[':status' => 1,':ramo'=>2,':tipo' => $tipo])
                       ->select(["poliza.numero_poliza","poliza.id_poliza","(persona.nombre ||' '||persona.apellido) as nombre_completo",
                                 "persona.identificacion","aseguradoras.nombre AS aseguradora","aseguradoras.id_aseguradora",
                                 "poliza.fecha_vigenciadesde","poliza.fecha_vigenciahasta","poliza.estatus",
								"ramos.nombre AS ramo"," (CASE WHEN poliza.estatus_poliza ='Anulada' THEN poliza.estatus_poliza
								         WHEN poliza.estatus_poliza <> 'Anulada' and poliza.fecha_vigenciahasta < NOW() THEN 'Vencida'
 										 WHEN poliza.estatus_poliza <> 'Anulada' and poliza.fecha_vigenciahasta >= NOW() THEN 'Vigente'
										 ELSE poliza.estatus_poliza END) AS estatus_poliza"]);   
            
            
            
            
        }                                



        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
              'pageSize' => 12,
            ],
			       'sort' =>
       			   ['attributes' =>
       				   [
         				  'ramo',
         					'id_aseguradora',
         					'nombre_completo',
         					'numero_poliza',
         					'fecha_vigenciahasta',
							    'fecha_vigenciadesde',
							    'estatus_poliza',
							    'estatus',
                                'placa',
                                'producto',
                                'aseguradora'
       				   ]
       			   ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'poliza.id_poliza' => $this->id_poliza,
            'id_tipo' => $this->id_tipo,
            'fecha_vigenciadesde' => $this->fecha_vigenciadesde,
            'fecha_vigenciahasta' => $this->fecha_vigenciahasta,
            'poliza.estatus' => $this->estatus,
			'aseguradoras.id_aseguradora'=>$this->id_aseguradora,
            'fecha_registro' => $this->fecha_registro,
            'fecha_actualizacion' => $this->fecha_actualizacion,
            'id_user_registro' => $this->id_user_registro,
            'id_user_actualizacion' => $this->id_user_actualizacion,
            'ramos.id_ramo' => $this->id_ramo,
        ]);

        //$query->andFilterWhere(['like', 'estatus_poliza', $this->estatus_poliza]);

		if (isset($this->estatus_poliza)) {
			 $fecha_hasta2=date("d-m-Y");
			 //echo $this->estatus_poliza; exit();
			 if($this->estatus_poliza=='Vencida'){
				$query->andFilterWhere(['<', "poliza.fecha_vigenciahasta", $fecha_hasta2]);
			 }else if($this->estatus_poliza=='Vigente'){
				 $query->andFilterWhere(['>=', "poliza.fecha_vigenciahasta", $fecha_hasta2]);
			 }else{

				$query->andFilterWhere(['like', 'estatus_poliza', $this->estatus_poliza]);
			 }
		   }
		   
		$this->numero_poliza=strtoupper($this->numero_poliza);   
        $query->andFilterWhere(['like', '(upper(numero_poliza))', $this->numero_poliza]);
		$this->nombre_completo=strtoupper($this->nombre_completo);
		$query->andFilterWhere(['or',
            ['like', '(upper(persona.apellido))', $this->nombre_completo],
            ['like', '(upper(persona.nombre))', $this->nombre_completo],
			['like', '(upper(persona.segundo_nombre))', $this->nombre_completo],
			['like', '(upper(persona.segundo_apellido))', $this->nombre_completo]]);
		
		$query->andFilterWhere(['like', 'persona.identificacion', $this->identificacion]);
        $this->placa=strtoupper($this->placa); 
        $query->andFilterWhere(['like', '(upper(placa))', $this->placa]);

        return $dataProvider;
    }

    public function search2($params)
    {

        if(!$params){

		 $query= Poliza::find()->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
                               ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
                               ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
                               ->innerJoin('ramos', 'ramos.id_ramo = poliza.id_ramo')
                              ->where("poliza.estatus = :status",[':status' => NULL])
                              ->select("poliza.numero_poliza,poliza.id_poliza,(persona.nombre||' '||persona.apellido) as nombre_completo,
                                 persona.identificacion,aseguradoras.nombre AS aseguradora,aseguradoras.id_aseguradora,poliza.estatus_poliza,
                                 poliza.fecha_vigenciadesde,poliza.fecha_vigenciahasta,poliza.estatus,
                                 ramos.nombre AS ramo");
    }else{

		 $query= Poliza::find()->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
                               ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
                               ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
                               ->innerJoin('ramos', 'ramos.id_ramo = poliza.id_ramo')
                               ->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza')
                               ->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo')
                               ->innerJoin('productos', 'productos.id_producto = certificado.id_producto')
                              ->where("poliza.estatus = :status",[':status' => 1])
                       ->select("poliza.numero_poliza,poliza.id_poliza,(persona.nombre||' '||persona.apellido) as nombre_completo,
                                 persona.identificacion,aseguradoras.nombre AS aseguradora,aseguradoras.id_aseguradora,poliza.estatus_poliza,
                                 poliza.fecha_vigenciadesde,poliza.fecha_vigenciahasta,poliza.estatus,productos.nombre AS producto,
                                 ramos.nombre AS ramo");
    }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
              'pageSize' => 12,
            ],
			'sort' =>
       			   ['attributes' =>
       				   [
         				    'ramo',
         					'id_aseguradora',
         					'nombre_completo',
         					'numero_poliza',
         					'fecha_vigenciahasta',
							'fecha_vigenciadesde',
							'estatus_poliza',
							'estatus',
       				   ]
       			   ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (isset($this->fecha_vigenciadesde) && !empty($this->fecha_vigenciadesde)) {
           list($this->fecha_desde, $this->fecha_hasta) = explode(' - ', $this->fecha_vigenciadesde);
            
        }

        $query->andFilterWhere([
            'id_poliza' => $this->id_poliza,
            'ramos.id_ramo' => $this->id_ramo,
            'poliza.id_contratante' => $this->id_contratante,
			'aseguradoras.id_aseguradora'=>$this->id_aseguradora,
            'productos.id_producto'=>$this->id_producto,
        ]);

        $query->andFilterWhere(['>=', 'recibo.fecha_vigenciahasta', $this->fecha_desde]);
        $query->andFilterWhere(['<=', 'recibo.fecha_vigenciahasta', $this->fecha_hasta]);

        $this->numero_poliza=strtoupper($this->numero_poliza);   
        $query->andFilterWhere(['like', '(upper(numero_poliza))', $this->numero_poliza]);
        $query->andFilterWhere(['like', 'numero_poliza', $this->numero_poliza]);
		$query->andFilterWhere(['like', 'persona.identificacion', $this->identificacion]);



        return $dataProvider;
    }
	public function search3($params,$id)
    {



		 $query= Poliza::find()->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza  and recibo.fecha_vigenciadesde >=poliza.fecha_vigenciadesde
		                                              and  recibo.fecha_vigenciahasta <=poliza.fecha_vigenciahasta')
                               ->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo')
							   ->innerJoin('certificado_detalle', 'certificado_detalle.id_certificado = certificado.id_certificado')

                              ->where("poliza.estatus = :status AND poliza.id_poliza= :id",[':status' => 1,':id' => $id])
                       ->select("recibo.id_recibo,recibo.estatus_recibo,recibo.nro_recibo,recibo.fecha_vigenciahasta,recibo.fecha_vigenciadesde,tipo_recibo,sum(certificado_detalle.prima) AS prima")
					   ->groupBy('recibo.id_recibo,recibo.nro_recibo,recibo.fecha_vigenciahasta,recibo.fecha_vigenciadesde,tipo_recibo');


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
              'pageSize' => 3,
            ],
			'sort' =>
       			   ['attributes' =>
       				   [
         				    'nro_recibo',
         					'recibo.fecha_vigenciahasta',
         					'recibo.fecha_vigenciadesde',
         					'tipo_recibo',
							'prima',
							'id_recibo',
							'producto',
                            'aseguradora'
       				   ]
       			   ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (isset($this->fecha_vigenciadesde) && !empty($this->fecha_vigenciadesde)) {
           list($this->fecha_desde, $this->fecha_hasta) = explode(' - ', $this->fecha_vigenciadesde);}

        $query->andFilterWhere([
            'id_poliza' => $this->id_poliza,
            'ramos.id_ramo' => $this->id_ramo,
            'poliza.id_contratante' => $this->id_contratante,
            'productos.id_producto'=>$this->id_producto,
        ]);

        $query->andFilterWhere(['>=', 'recibo.fecha_vigenciadesde', $this->fecha_desde]);
        $query->andFilterWhere(['<=', 'recibo.fecha_vigenciadesde', $this->fecha_hasta]);

        $query->andFilterWhere(['like', 'estatus_poliza', $this->estatus_poliza]);
        $this->numero_poliza=strtoupper($this->numero_poliza);   
        $query->andFilterWhere(['like', '(upper(numero_poliza))', $this->numero_poliza]);
		$query->andFilterWhere(['like', 'persona.identificacion', $this->identificacion]);



        return $dataProvider;
    }
    
   public function search4($params,$id)
    {



		 $query= Poliza::find()->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza  and recibo.fecha_vigenciadesde >=poliza.fecha_vigenciadesde
		                                              and  recibo.fecha_vigenciahasta <=poliza.fecha_vigenciahasta')
                               ->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo')
							   ->innerJoin('certificado_detalle', 'certificado_detalle.id_certificado = certificado.id_certificado')

                              ->where("poliza.estatus = :status AND poliza.id_poliza= :id",[':status' => 1,':id' => $id])
                       ->select("recibo.id_recibo,recibo.estatus_recibo,recibo.nro_recibo,recibo.fecha_vigenciahasta,recibo.fecha_vigenciadesde,tipo_recibo,sum(certificado_detalle.prima) AS prima")
					   ->groupBy('recibo.id_recibo,recibo.nro_recibo,recibo.fecha_vigenciahasta,recibo.fecha_vigenciadesde,tipo_recibo');


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
              'pageSize' => 3,
            ],
			'sort' =>
       			   ['attributes' =>
       				   [
         				    'nro_recibo',
         					'recibo.fecha_vigenciahasta',
         					'recibo.fecha_vigenciadesde',
         					'tipo_recibo',
							'prima',
							'id_recibo',
							'producto',
                            'aseguradora'
       				   ]
       			   ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (isset($this->fecha_vigenciadesde) && !empty($this->fecha_vigenciadesde)) {
           list($this->fecha_desde, $this->fecha_hasta) = explode(' - ', $this->fecha_vigenciadesde);}

        $query->andFilterWhere([
            'id_poliza' => $this->id_poliza,
            'ramos.id_ramo' => $this->id_ramo,
            'poliza.id_contratante' => $this->id_contratante,
			'aseguradoras.id_aseguradora'=>$this->id_aseguradora,
            'productos.id_producto'=>$this->id_producto,
        ]);

        $query->andFilterWhere(['>=', 'recibo.fecha_vigenciadesde', $this->fecha_desde]);
        $query->andFilterWhere(['<=', 'recibo.fecha_vigenciadesde', $this->fecha_hasta]);

        $query->andFilterWhere(['like', 'estatus_poliza', $this->estatus_poliza]);
        $this->numero_poliza=strtoupper($this->numero_poliza);   
        $query->andFilterWhere(['like', '(upper(numero_poliza))', $this->numero_poliza]);
		$query->andFilterWhere(['like', 'persona.identificacion', $this->identificacion]);



        return $dataProvider;
    }
    
    
}
