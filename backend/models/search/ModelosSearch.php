<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Modelos;

/**
 * ModelosSearch represents the model behind the search form about `backend\models\Modelos`.
 */
class ModelosSearch extends Modelos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_modelo', 'id_marca', 'nombre', 'fecha_registro', 'fecha_actualizacion', 'id_user_actualizacion'], 'safe'],
            [['ws_inhabilitado', 'id_user_registro'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Modelos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		$query->joinWith(['idMarca']);

        $query->andFilterWhere([
            'modelos.ws_inhabilitado' => $this->ws_inhabilitado,
            'modelos.fecha_registro' => $this->fecha_registro,
            'modelos.fecha_actualizacion' => $this->fecha_actualizacion,
            'modelos.id_user_registro' => $this->id_user_registro,
            'modelos.id_user_actualizacion' => $this->id_user_actualizacion,
        ]);

        $query->andFilterWhere(['like', 'modelos.id_modelo', $this->id_modelo])
            ->andFilterWhere(['like', 'modelos.id_marca', $this->id_marca])
            ->andFilterWhere(['like', 'modelos.nombre', $this->nombre]);

        return $dataProvider;
    }
}
