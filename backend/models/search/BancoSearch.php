<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Banco;

/**
 * BancoSearch represents the model behind the search form about `backend\models\Banco`.
 */
class BancoSearch extends Banco
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_banco', 'id_user_registro', 'id_user_actualizacion'], 'integer'],
            [['nombre_banco', 'fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['estatus'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Banco::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_banco' => $this->id_banco,
            'id_user_registro' => $this->id_user_registro,
            'id_user_actualizacion' => $this->id_user_actualizacion,
            'fecha_registro' => $this->fecha_registro,
            'fecha_actualizacion' => $this->fecha_actualizacion,
            'estatus' => $this->estatus,
        ]);

        $query->andFilterWhere(['like', 'nombre_banco', $this->nombre_banco]);

        return $dataProvider;
    }
}
