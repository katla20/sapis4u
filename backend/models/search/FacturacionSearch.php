<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Facturacion;
use backend\models\Poliza;

/**
 * FacturacionSearch represents the model behind the search form about `backend\models\Facturacion`.
 */
class FacturacionSearch extends Facturacion
{
    /**
     * @inheritdoc
     */
    public $aseguradora;
    public function rules()
    {
        return [
            [['id_facturacion', 'id_user_registro', 'id_user_actualizacion', 'estatus_factura', 'id_aseguradora'], 'integer'],
            [['numero_factura', 'numero_control', 'monto_iva', 'monto_islr', 'monto_subtotal', 'monto_recibos','monto_factura'], 'number'],
            [['fecha_registro', 'fecha_actualizacion', 'descripcion','fecha_factura'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {

		    $query= Facturacion::find()->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora=facturacion.id_aseguradora')
                                   ->select(["id_facturacion","aseguradoras.nombre AS aseguradora","estatus_factura",
                                             "numero_factura","numero_control","descripcion","fecha_factura","monto_factura",
                                             "aseguradoras.id_aseguradora"]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
              'pageSize' => 12,
            ],
			     'sort' =>
       			   ['attributes' =>
       				   [
         					 'id_aseguradora',
         					 'numero_factura',
							 'monto_factura',
							 'estatus_factura',
							 'fecha_factura',
							 'descripcion',
       				   ]
       			   ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_facturacion' => $this->id_facturacion,
            'numero_control' => $this->numero_control,
            'monto_iva' => $this->monto_iva,
            'monto_islr' => $this->monto_islr,
            'monto_subtotal' => $this->monto_subtotal,
			'monto_factura' => $this->monto_factura,
            'monto_recibos' => $this->monto_recibos,
            'id_user_registro' => $this->id_user_registro,
            'id_user_actualizacion' => $this->id_user_actualizacion,
            'fecha_registro' => $this->fecha_registro,
            'fecha_factura' => $this->fecha_factura,
            'fecha_actualizacion' => $this->fecha_actualizacion,
            'estatus_factura' => $this->estatus_factura,
            'aseguradoras.id_aseguradora' => $this->id_aseguradora,
        ]);

        $query->andFilterWhere(['like', 'descripcion', $this->descripcion]);
        $query->andFilterWhere(['like', 'numero_factura', $this->numero_factura]);
        return $dataProvider;
    }


}
