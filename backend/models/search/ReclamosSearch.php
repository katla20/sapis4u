<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Reclamos;

/**
 * ReclamosSearch represents the model behind the search form about `backend\models\Reclamos`.
 */
class ReclamosSearch extends Reclamos
{
    /**
     * @inheritdoc
     */
      public $fecha_desde,$fecha_hasta; 
     
    public function rules()
    {
        return [
            [['id_reclamo', 'id_recibo', 'id_tipo_reclamo', 'id_user_registro', 'id_user_actualizacion', 'estatus','causa'], 'integer'],
            [['descripcion', 'poliza','fecha_registro','id_contratante','id_producto','id_ramo','id_aseguradora','fecha_actualizacion', 'fecha_ocurrencia', 'fecha_notificacion','numero_reclamo'], 'safe'],
            [['monto'], 'number'],
        ];
    }
    
     public function attributeLabels()
    {
        return [
            'poliza' => Yii::t('app', 'Poliza'),
            'numero_poliza' => Yii::t('app', 'Numero Poliza'),
            'fecha_vigenciadesde' => Yii::t('app', 'Fecha'),
            'id_producto' => Yii::t('app', 'Producto'),
            'id_ramo' => Yii::t('app', 'Ramo'),
            'id_contratante' => Yii::t('app', 'Contratante'),
            'id_aseguradora' => Yii::t('app', 'Aseguradora'),
            'id_tipo_reclamos' => Yii::t('app', 'Tipo Reclamos'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Reclamos::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
              'pageSize' => 12,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if (isset($this->fecha_ocurrencia) && !empty($this->fecha_ocurrencia)) {
           list($this->fecha_desde, $this->fecha_hasta) = explode(' - ', $this->fecha_ocurrencia);}

        $query->andFilterWhere([
            'id_reclamo' => $this->id_reclamo,
            'id_recibo' => $this->id_recibo,
            'id_tipo_reclamo' => $this->id_tipo_reclamo,
            'monto' => $this->monto,
            'fecha_registro' => $this->fecha_registro,
            'fecha_actualizacion' => $this->fecha_actualizacion,
            'id_user_registro' => $this->id_user_registro,
            'id_user_actualizacion' => $this->id_user_actualizacion,
            'estatus' => $this->estatus,
            'fecha_ocurrencia' => $this->fecha_ocurrencia,
            'fecha_notificacion' => $this->fecha_notificacion,
			'causa' => $this->causa,
        ]);
        
        $query->andFilterWhere(['>=', 'fecha_ocurrencia', $this->fecha_desde]);
        $query->andFilterWhere(['<=', 'fecha_ocurrencia', $this->fecha_hasta]);


        $query->andFilterWhere(['like', 'descripcion', $this->descripcion]);
		$query->andFilterWhere(['like', 'numero_reclamo', $this->numero_reclamo]);

        return $dataProvider;
    }
	
    public function search2($params)
    {
        
         $user=(isset(Yii::$app->user->identity->id))? Yii::$app->user->identity->id:0;
         $query = Reclamos::find()
        ->innerJoin('reclamo_seguimiento', 'reclamo_seguimiento.id_reclamo = reclamos.id_reclamo')
        ->where("user_destino=".$user." AND estatus_mensaje=0");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
              'pageSize' => 12,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_reclamo' => $this->id_reclamo,
            'id_recibo' => $this->id_recibo,
            'id_tipo_reclamo' => $this->id_tipo_reclamo,
            'monto' => $this->monto,
            'fecha_registro' => $this->fecha_registro,
            'fecha_actualizacion' => $this->fecha_actualizacion,
            'id_user_registro' => $this->id_user_registro,
            'id_user_actualizacion' => $this->id_user_actualizacion,
            'estatus' => $this->estatus,
            'fecha_ocurrencia' => $this->fecha_ocurrencia,
            'fecha_notificacion' => $this->fecha_notificacion,
			'causa' => $this->causa,
        ]);

        $query->andFilterWhere(['like', 'descripcion', $this->descripcion]);
		$query->andFilterWhere(['like', 'numero_reclamo', $this->numero_reclamo]);

        return $dataProvider;
    }
    
    public function search3($params)
    {
        
         $user=(isset(Yii::$app->user->identity->id))? Yii::$app->user->identity->id:0;
         $query = Reclamos::find()
                                ->innerJoin('tipo_reclamos', 'tipo_reclamos.id_tipo_reclamo= reclamos.id_tipo_reclamo')
                                ->innerJoin('recibo', 'recibo.id_recibo= reclamos.id_recibo')
                                ->innerJoin('poliza', 'poliza.id_poliza= recibo.id_poliza')
                                ->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
                                ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
                                ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
                                ->innerJoin('ramos', 'ramos.id_ramo = poliza.id_ramo')
                                ->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo')
                               ->innerJoin('productos', 'productos.id_producto = certificado.id_producto')
                                 ->where("poliza.estatus = :status",[':status' => 1])
                       ->select("reclamos.numero_reclamo,poliza.id_poliza,(persona.nombre ||' '||segundo_nombre||' '||apellido|| ' ' ||segundo_apellido) as nombre_completo,
                                 persona.identificacion,aseguradoras.nombre AS aseguradora,aseguradoras.id_aseguradora,tipo_reclamos.nombre AS tipo_reclamo,
                                 poliza.estatus,productos.nombre AS producto,reclamos.descripcion,reclamos.monto,recibo.nro_recibo AS recibo,
                                 ramos.nombre AS ramo");
        
        //->where("user_destino=".$user." AND estatus_mensaje=0");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
              'pageSize' => 12,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_reclamo' => $this->id_reclamo,
            'id_recibo' => $this->id_recibo,
            'reclamos.id_tipo_reclamo' => $this->id_tipo_reclamo,
            'monto' => $this->monto,
            'fecha_registro' => $this->fecha_registro,
            'fecha_actualizacion' => $this->fecha_actualizacion,
            'id_user_registro' => $this->id_user_registro,
            'id_user_actualizacion' => $this->id_user_actualizacion,
            'estatus' => $this->estatus,
            'fecha_ocurrencia' => $this->fecha_ocurrencia,
            'fecha_notificacion' => $this->fecha_notificacion,
			'causa' => $this->causa,
            'ramos.id_ramo' => $this->id_ramo,
            'poliza.id_contratante' => $this->id_contratante,
            'poliza.id_poliza' => $this->poliza,
			'aseguradoras.id_aseguradora'=>$this->id_aseguradora,
            'productos.id_producto'=>$this->id_producto,
        ]);

        $query->andFilterWhere(['like', 'descripcion', $this->descripcion]);
		$query->andFilterWhere(['like', 'numero_reclamo', $this->numero_reclamo]);

        return $dataProvider;
    }
    
    public function search4($params,$estatus)
    {
        
         $user=(isset(Yii::$app->user->identity->id))? Yii::$app->user->identity->id:0;
         $query = Reclamos::find()
                                ->innerJoin('tipo_reclamos', 'tipo_reclamos.id_tipo_reclamo= reclamos.id_tipo_reclamo')
                                ->innerJoin('recibo', 'recibo.id_recibo= reclamos.id_recibo')
                                ->innerJoin('poliza', 'poliza.id_poliza= recibo.id_poliza')
                                ->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
                                ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
                                ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
                                ->innerJoin('ramos', 'ramos.id_ramo = poliza.id_ramo')
                                ->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo')
                               ->innerJoin('productos', 'productos.id_producto = certificado.id_producto')
                                 ->where("poliza.estatus = :status AND reclamos.estatus_reclamo= :statusr",[':status' => 1,':statusr' => $estatus])
                                ->select("reclamos.numero_reclamo,poliza.id_poliza,(persona.nombre ||' '||segundo_nombre||' '||apellido|| ' ' ||segundo_apellido) as nombre_completo,
                                 persona.identificacion,aseguradoras.nombre AS aseguradora,aseguradoras.id_aseguradora,tipo_reclamos.nombre AS tipo_reclamo,
                                 poliza.estatus,productos.nombre AS producto,reclamos.descripcion,reclamos.monto,recibo.nro_recibo AS recibo,
                                 ramos.nombre AS ramo,reclamos.id_reclamo,recibo.id_recibo,tipo_reclamos.id_tipo_reclamo");
        
        //->where("user_destino=".$user." AND estatus_mensaje=0");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
              'pageSize' => 12,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_reclamo' => $this->id_reclamo,
            'id_recibo' => $this->id_recibo,
            'reclamos.id_tipo_reclamo' => $this->id_tipo_reclamo,
            'monto' => $this->monto,
            'fecha_registro' => $this->fecha_registro,
            'fecha_actualizacion' => $this->fecha_actualizacion,
            'id_user_registro' => $this->id_user_registro,
            'id_user_actualizacion' => $this->id_user_actualizacion,
            'estatus' => $this->estatus,
            'fecha_ocurrencia' => $this->fecha_ocurrencia,
            'fecha_notificacion' => $this->fecha_notificacion,
			'causa' => $this->causa,
            'ramos.id_ramo' => $this->id_ramo,
            'poliza.id_contratante' => $this->id_contratante,
            'poliza.id_poliza' => $this->poliza,
			'aseguradoras.id_aseguradora'=>$this->id_aseguradora,
            'productos.id_producto'=>$this->id_producto,
        ]);

        $query->andFilterWhere(['like', 'descripcion', $this->descripcion]);
		$query->andFilterWhere(['like', 'numero_reclamo', $this->numero_reclamo]);

        return $dataProvider;
    }
}
