<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\LiquidacionComisiones;

/**
 * LiquidacionComisionesSearch represents the model behind the search form about `backend\models\LiquidacionComisiones`.
 */
class LiquidacionComisionesSearch extends LiquidacionComisiones
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_liquidacion_comisiones', 'id_intermediario', 'id_user_registro', 'id_user_actualizacion'], 'integer'],
            [['comentario','recibos', 'fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['monto'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LiquidacionComisiones::find()->innerJoin('intermediario', 'intermediario.id_intermediario = liquidacion_comisiones.id_intermediario')
    		    ->leftJoin('persona', 'persona.id_persona = intermediario.id_persona')
    	        ->where("liquidacion_comisiones.estatus=1")
            ->select("liquidacion_comisiones.*,(nombre||' '||apellido) AS nombre_completo");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_liquidacion_comisiones' => $this->id_liquidacion_comisiones,
            'intermediario.id_intermediario' => $this->id_intermediario,
            'liquidacion_comisiones.fecha_registro' => $this->fecha_registro,
            'liquidacion_comisiones.fecha_actualizacion' => $this->fecha_actualizacion,
            'liquidacion_comisiones.id_user_registro' => $this->id_user_registro,
            'liquidacion_comisiones.id_user_actualizacion' => $this->id_user_actualizacion,
            'monto' => $this->monto,
        ]);

        $query->andFilterWhere(['like', 'comentario', $this->comentario]);

        return $dataProvider;
    }
}
