<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Estado;

/**
 * EstadoSearch represents the model behind the search form about `backend\models\Estado`.
 */
class EstadoSearch extends Estado
{
    /**
     * @inheritdoc
     */
	 
	public $id_pais; 
    public function rules()
    {
        return [
            [['id_estado', 'estatus', 'id_pais', 'id_user_registro', 'id_user_actualizacion'], 'integer'],
            [['nombre', 'region', 'fecha_registro', 'fecha_actualizacion', 'descripcion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Estado::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		$query->joinWith(['idPais']);
		
		/*$dataProvider->sort->attributes['id_pais'] = [
        // The tables are the ones our relation are configured to
        // in my case they are prefixed with "tbl_"
        'asc' => ['pais.nombre' => SORT_ASC],
        'desc' => ['pais.nombre' => SORT_DESC],
         ];*/

        $query->andFilterWhere([
            'id_estado' => $this->id_estado,
            'pais.id_pais' => $this->id_pais,
            'fecha_registro' => $this->fecha_registro,
            'fecha_actualizacion' => $this->fecha_actualizacion,
            'id_user_registro' => $this->id_user_registro,
            'id_user_actualizacion' => $this->id_user_actualizacion,
			'estado.estatus' => $this->estatus,
            
        ]);

        $query->andFilterWhere(['like', 'estado.nombre', $this->nombre])
			->andFilterWhere(['like', 'pais.nombre', $this->id_pais])
            ->andFilterWhere(['like', 'region', $this->region])
            ->andFilterWhere(['like', 'estado.descripcion', $this->descripcion]);

        return $dataProvider;
    }
}
