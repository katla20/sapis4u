<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Recibo;

class PagoSearch extends Recibo
{
    /**
     * @inheritdoc
     */

    public $identificacion,$aseguradora,$numero_poliza,$nombre_completo;
    public function rules()
    {
        return [
          [['id_poliza','id_recibo','identificacion','aseguradora','fecha_vigenciahasta','numero_poliza','nombre_completo','nro_recibo'], 'default'],
          [['id_user_registro', 'id_user_actualizacion'], 'integer'],




        ];
    }

  //  ['country', 'validateCountry', 'skipOnEmpty' => false, 'skipOnError' => false],

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //$query = Pago::find();

        $query= Recibo::find()->innerJoin('poliza', 'poliza.id_poliza=recibo.id_poliza')
                               ->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
                               ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
                               ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
                               ->innerJoin('ramos', 'ramos.id_ramo = poliza.id_ramo')
                              ->where("poliza.estatus = :status AND recibo.fecha_vigenciahasta > CURRENT_DATE AND recibo.estatus_recibo='Pendiente'",
                                [':status' => 1])
                       ->select("poliza.numero_poliza,poliza.id_poliza,recibo.nro_recibo,recibo.id_recibo,(persona.nombre ||' '||segundo_nombre||' '||apellido|| ' ' ||segundo_apellido) as nombre_completo,
                                 persona.identificacion,aseguradoras.nombre AS aseguradora,aseguradoras.id_aseguradora,
                                 (poliza.fecha_vigenciadesde||'  -  '||poliza.fecha_vigenciahasta) AS fecha_vigenciahasta,
                                 ramos.nombre AS ramo");/*->asArray()->all();*/

         $dataProvider = new ActiveDataProvider([
              'query' => $query,
       			  'pagination' => [
       				'pageSize' => 12,
       			  ],
       			  'sort' =>
       			   ['attributes' =>
       				   [
         				  'ramo',
         					'aseguradora',
         					'nombre_completo',
         					'numero_poliza',
                  'nro_recibo',
         					'fecha_vigenciahasta'
       				   ]
       			   ],

          ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([


           'id_poliza' => $this->id_poliza,
           'id_recibo' => $this->id_recibo,
           'fecha_vigenciahasta' => $this->fecha_vigenciahasta,
           'ramo' => $this->ramo,

        ]);

        $query->andFilterWhere(['like', 'numero_poliza', $this->numero_poliza]);
        $query->andFilterWhere(['like', 'nombre_completo', $this->nombre_completo]);
        $query->andFilterWhere(['like', 'nro_recibo', $this->nro_recibo]);
        $query->andFilterWhere(['like', 'aseguradoras.nombre', $this->aseguradora]);
        $query->andFilterWhere(['like', 'persona.identificacion', $this->identificacion]);


        return $dataProvider;
    }
}
