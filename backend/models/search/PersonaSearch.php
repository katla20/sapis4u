<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Persona;

/**
 * PersonaSearch represents the model behind the search form about `backend\models\persona`.
 */
class PersonaSearch extends persona
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'segundo_nombre', 'apellido', 'fecha_nacimiento', 'identificacion', 'identificacion_2', 'sexo', 'correo', 'tipo', 'segundo_apellido'], 'safe'],
            [['id_persona'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$tipo)
    {
        $query = persona::find();
  
		  if($tipo==1){//cliente
		  $query->innerJoin('cliente', 'cliente.id_persona = persona.id_persona'); 
		  }elseif($tipo==2){
			
		   $query->innerJoin('intermediario', 'intermediario.id_persona = persona.id_persona');
		  }
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 12,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'fecha_nacimiento' => $this->fecha_nacimiento,
            'id_persona' => $this->id_persona,
        ]);
        $this->nombre=strtoupper($this->nombre);  
		$this->segundo_nombre=strtoupper($this->segundo_nombre);  
		$this->apellido=strtoupper($this->apellido);  
		$this->segundo_apellido=strtoupper($this->segundo_apellido);  
        $query->andFilterWhere(['like','(upper(nombre))', $this->nombre])
            ->andFilterWhere(['like', '(upper(segundo_nombre))', $this->segundo_nombre])
            ->andFilterWhere(['like', '(upper(apellido))', $this->apellido])
            ->andFilterWhere(['like', 'identificacion', $this->identificacion])
            ->andFilterWhere(['like', 'identificacion_2', $this->identificacion_2])
            ->andFilterWhere(['like', 'sexo', $this->sexo])
            ->andFilterWhere(['like', 'correo', $this->correo])
            ->andFilterWhere(['like', 'tipo', $this->tipo])
            ->andFilterWhere(['like', '(upper(segundo_apellido))', $this->segundo_apellido]);

        return $dataProvider;
    }
}
