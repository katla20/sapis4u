<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Automovil;

/**
 * automovilSearch represents the model behind the search form about `backend\models\automovil`.
 */
class AutomovilSearch extends automovil
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_version', 'id_user_registro', 'id_user_actualizacion', 'id_tipo_vehiculo', 'id_uso','id_modelo','id_marca'], 'integer'],
            [['placa','marca','version','modelo','fecha_registro', 'fecha_actualizacion', 'color', 'serial_motor', 'serial_carroceria'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Automovil::find()
          ->leftJoin('version', 'version.id_version = automovil.id_version')
          ->leftJoin('modelos', 'modelos.id_modelo = automovil.id_modelo')
          ->leftJoin('marcas', 'marcas.id_marca = modelos.id_marca')
          ->select("automovil.*,marcas.nombre AS marca,modelos.nombre AS modelo,marcas.id_marca,version.nombre AS version");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_automovil' => $this->id_automovil,
            'marcas.id_marca' => $this->id_marca,
            'modelos.id_modelo' => $this->id_modelo,
        ]);

		$this->placa=strtoupper($this->placa);
        $query->andFilterWhere(['like', '(upper(placa))', $this->placa])
              ->andFilterWhere(['like', 'color', $this->color])
              ->andFilterWhere(['like', 'serial_motor', $this->serial_motor])
              ->andFilterWhere(['like', 'version.nombre', $this->version]);

        return $dataProvider;
    }
}
