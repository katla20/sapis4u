<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Recibo;

/**
 * ReciboSearch represents the model behind the search form about `\backend\models\Recibo`.
 */
class ReciboSearch extends Recibo
{
    /**
     * @inheritdoc
     */
	 public $fecha_desde,$fecha_hasta;

    public function rules()
    {
        return [
            [['id_recibo', 'nro_recibo', 'id_poliza', 'estatus', 'id_user_registro', 'id_user_actualizacion', 'id_subcontratante', 'id_vendedor', 'id_productor'], 'integer'],
            [['fecha_vigenciadesde', 'fecha_vigenciahasta', 'fecha_registro', 'fecha_actualizacion','numero_poliza','comision_total'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Recibo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
              'pageSize' => 12,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_recibo' => $this->id_recibo,
            'nro_recibo' => $this->nro_recibo,
            'id_poliza' => $this->id_poliza,
            'estatus' => $this->estatus,
            'fecha_registro' => $this->fecha_registro,
            'fecha_actualizacion' => $this->fecha_actualizacion,
            'id_user_registro' => $this->id_user_registro,
            'id_user_actualizacion' => $this->id_user_actualizacion,
            'id_subcontratante' => $this->id_subcontratante,
            'id_vendedor' => $this->id_vendedor,
            'id_productor' => $this->id_productor,
        ]);

        $query->andFilterWhere(['like', 'estatus_recibo', $this->estatus_recibo]);

        return $dataProvider;
    }

	public function search2($params,$id)
    {
         $query = Recibo::find()->innerJoin('poliza', 'poliza.id_poliza = recibo.id_poliza')
                              ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
                              ->where("poliza.estatus = :status AND recibo.facturado = :fac AND aseguradoras.id_aseguradora= :id",
                                        [':status' => 1,':fac'=> 0,':id'=>$id])
                              ->select("numero_poliza AS numero_poliza ,
                                recibo.nro_recibo,recibo.comision_total,recibo.id_recibo,aseguradoras.nombre AS aseguradora,
								recibo.fecha_vigenciadesde");




        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
              'pageSize' => 12,
            ],
			'sort' =>
       			   ['attributes' =>
       				   [
         				    'numero_poliza',
         					'nro_recibo',
         					'comision_total',
         					'numero_poliza',
							'fecha_vigenciadesde',
       				   ]
       			   ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

		if (isset($this->fecha_vigenciadesde) && !empty($this->fecha_vigenciadesde)) {
           list($this->fecha_desde, $this->fecha_hasta) = explode(' - ', $this->fecha_vigenciadesde);}

        $query->andFilterWhere([
            'id_recibo' => $this->id_recibo,
            'nro_recibo' => $this->nro_recibo,
            'recibo.id_poliza' => $this->id_poliza,
            'estatus' => $this->estatus,
			'numero_poliza' => $this->numero_poliza,
            'fecha_registro' => $this->fecha_registro,
            'fecha_actualizacion' => $this->fecha_actualizacion,
            'id_user_registro' => $this->id_user_registro,
            'id_user_actualizacion' => $this->id_user_actualizacion,
            'id_subcontratante' => $this->id_subcontratante,
            'id_vendedor' => $this->id_vendedor,
            'id_productor' => $this->id_productor,
        ]);

		$query->andFilterWhere(['>=', 'recibo.fecha_vigenciadesde', $this->fecha_desde]);
        $query->andFilterWhere(['<=', 'recibo.fecha_vigenciadesde', $this->fecha_hasta]);

        $query->andFilterWhere(['like', 'estatus_recibo', $this->estatus_recibo]);
         //print_r($dataProvider);exit();
        return $dataProvider;
    }
	
	public function search3($params,$id)
    {
         $query = Recibo::find()->innerJoin('poliza', 'poliza.id_poliza = recibo.id_poliza')
                              ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
                              ->where("poliza.estatus = :status AND recibo.facturado = :fac AND recibo.id_vendedor= :id
							  AND recibo.liquidado = :liq",
                                        [':status' => 1,':fac'=> 1,':id'=>$id,':liq'=>0])
                              ->select("numero_poliza AS numero_poliza ,
                                recibo.nro_recibo,recibo.comision_total,recibo.id_recibo,aseguradoras.nombre AS aseguradora,
								recibo.fecha_vigenciadesde"); 
								
								

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
              'pageSize' => 12,
            ],
			'sort' =>
       			   ['attributes' =>
       				   [
         				    'numero_poliza',
         					'nro_recibo',
         					'comision_total',
         					'numero_poliza',
							'fecha_vigenciadesde',
       				   ]
       			   ],
        ]);

        
        $this->load($params);

       
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

		
		if (isset($this->fecha_vigenciadesde) && !empty($this->fecha_vigenciadesde)) {
           list($this->fecha_desde, $this->fecha_hasta) = explode(' - ', $this->fecha_vigenciadesde);}

          
        $query->andFilterWhere([
            'id_recibo' => $this->id_recibo,
            'nro_recibo' => $this->nro_recibo,
            'recibo.id_poliza' => $this->id_poliza,
            'estatus' => $this->estatus,
			'numero_poliza' => $this->numero_poliza,
            'fecha_registro' => $this->fecha_registro,
            'fecha_actualizacion' => $this->fecha_actualizacion,
            'id_user_registro' => $this->id_user_registro,
            'id_user_actualizacion' => $this->id_user_actualizacion,
            'id_subcontratante' => $this->id_subcontratante,
            'id_vendedor' => $this->id_vendedor,
            'id_productor' => $this->id_productor,
        ]);

		
		$query->andFilterWhere(['>=', 'recibo.fecha_vigenciadesde', $this->fecha_desde]);
        $query->andFilterWhere(['<=', 'recibo.fecha_vigenciadesde', $this->fecha_hasta]);

        $query->andFilterWhere(['like', 'estatus_recibo', $this->estatus_recibo]);
         //print_r($dataProvider);exit();
        return $dataProvider;
    }
}
