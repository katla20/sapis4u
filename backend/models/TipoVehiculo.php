<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tipo_vehiculo".
 *
 * @property integer $id_tipo_vehiculo
 * @property string $nombre
 * @property string $descripcion
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property integer $estatus
 *
 * @property Automovil[] $automovils
 */
class TipoVehiculo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_vehiculo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['descripcion'], 'string'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['id_user_registro', 'id_user_actualizacion', 'estatus'], 'integer'],
            [['nombre'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_tipo_vehiculo' => 'Id Tipo Vehiculo',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'fecha_registro' => 'Fecha Registro',
            'fecha_actualizacion' => 'Fecha Actualizacion',
            'id_user_registro' => 'Id User Registro',
            'id_user_actualizacion' => 'Id User Actualizacion',
            'estatus' => 'Estatus',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutomovils()
    {
        return $this->hasMany(Automovil::className(), ['id_tipo_vehiculo' => 'id_tipo_vehiculo']);
    }
}
