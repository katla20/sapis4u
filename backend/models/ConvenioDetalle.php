<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "convenio_detalle".
 *
 * @property integer $id_convenio_detalle
 * @property integer $id_convenio
 * @property integer $id_ramo
 * @property integer $id_producto
 * @property integer $cantidad
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 *
 * @property Convenio $idConvenio
 * @property Productos $idProducto
 * @property Ramos $idRamo
 */
class ConvenioDetalle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'convenio_detalle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_convenio', 'id_ramo', 'id_producto', 'cantidad', 'id_user_registro', 'id_user_actualizacion'], 'integer'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['id_convenio'], 'exist', 'skipOnError' => true, 'targetClass' => Convenio::className(), 'targetAttribute' => ['id_convenio' => 'id_convenio']],
            [['id_producto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['id_producto' => 'id_producto']],
            [['id_ramo'], 'exist', 'skipOnError' => true, 'targetClass' => Ramos::className(), 'targetAttribute' => ['id_ramo' => 'id_ramo']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_convenio_detalle' => Yii::t('app', 'Id Convenio Detalle'),
            'id_convenio' => Yii::t('app', 'Id Convenio'),
            'id_ramo' => Yii::t('app', 'Id Ramo'),
            'id_producto' => Yii::t('app', 'Id Producto'),
            'cantidad' => Yii::t('app', 'Cantidad'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdConvenio()
    {
        return $this->hasOne(Convenio::className(), ['id_convenio' => 'id_convenio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProducto()
    {
        return $this->hasOne(Productos::className(), ['id_producto' => 'id_producto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRamo()
    {
        return $this->hasOne(Ramos::className(), ['id_ramo' => 'id_ramo']);
    }
}
