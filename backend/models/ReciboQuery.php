<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[Recibo]].
 *
 * @see Recibo
 */
class ReciboQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Recibo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Recibo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}