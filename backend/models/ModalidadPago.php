<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "modalidad_pago".
 *
 * @property integer $id_modalidad_pago
 * @property string $nombre_modalidad_pago
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property string $estatus
 */
class ModalidadPago extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'modalidad_pago';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre_modalidad_pago', 'id_user_registro'], 'required'],
            [['id_user_registro', 'id_user_actualizacion'], 'integer'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['estatus'], 'number'],
            [['nombre_modalidad_pago'], 'string', 'max' => 400]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_modalidad_pago' => Yii::t('app', 'Id Modalidad Pago'),
            'nombre_modalidad_pago' => Yii::t('app', 'Nombre Modalidad Pago'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'estatus' => Yii::t('app', 'Estatus'),
        ];
    }
}
