<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "excel".
 *
 * @property string $certificado
 * @property string $tipo
 * @property string $cedula
 * @property string $primer_nombre
 * @property string $primer_apellido
 * @property string $fecha_nacimiento
 * @property string $estado
 * @property string $ciudad
 * @property string $direccion
 * @property string $producto
 * @property string $placa
 * @property string $marca
 * @property string $modelo
 * @property string $color
 * @property string $version
 * @property string $carroceria
 * @property string $deducible
 * @property integer $anio
 * @property string $sexo
 * @property string $suma_asegurada
 * @property string $prima
 * @property integer $id_user
 * @property string $tipo_au
 * @property string $uso_au
 */
class Excel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'excel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha_nacimiento'], 'safe'],
            [['deducible', 'suma_asegurada', 'prima'], 'number'],
            [['anio', 'id_user'], 'integer'],
            [['certificado', 'cedula', 'placa'], 'string', 'max' => 10],
            [['tipo'], 'string', 'max' => 3],
            [['primer_nombre', 'primer_apellido', 'estado', 'ciudad'], 'string', 'max' => 50],
            [['direccion', 'producto'], 'string', 'max' => 100],
            [['marca', 'modelo', 'color', 'version', 'carroceria'], 'string', 'max' => 20],
            [['sexo'], 'string', 'max' => 1],
            [['tipo_au', 'uso_au'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'certificado' => Yii::t('app', 'Certificado'),
            'tipo' => Yii::t('app', 'Tipo'),
            'cedula' => Yii::t('app', 'Cedula'),
            'primer_nombre' => Yii::t('app', 'Primer Nombre'),
            'primer_apellido' => Yii::t('app', 'Primer Apellido'),
            'fecha_nacimiento' => Yii::t('app', 'Fecha Nacimiento'),
            'estado' => Yii::t('app', 'Estado'),
            'ciudad' => Yii::t('app', 'Ciudad'),
            'direccion' => Yii::t('app', 'Direccion'),
            'producto' => Yii::t('app', 'Producto'),
            'placa' => Yii::t('app', 'Placa'),
            'marca' => Yii::t('app', 'Marca'),
            'modelo' => Yii::t('app', 'Modelo'),
            'color' => Yii::t('app', 'Color'),
            'version' => Yii::t('app', 'Version'),
            'carroceria' => Yii::t('app', 'Carroceria'),
            'deducible' => Yii::t('app', 'Deducible'),
            'anio' => Yii::t('app', 'Anio'),
            'sexo' => Yii::t('app', 'Sexo'),
            'suma_asegurada' => Yii::t('app', 'Suma Asegurada'),
            'prima' => Yii::t('app', 'Prima'),
            'id_user' => Yii::t('app', 'Id User'),
            'tipo_au' => Yii::t('app', 'Tipo Au'),
            'uso_au' => Yii::t('app', 'Uso Au'),
        ];
    }
}
