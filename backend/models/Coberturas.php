<?php

namespace backend\models;

use Yii;
use backend\models\InterProdCob;
use backend\models\Productos;
use backend\models\Poliza;

/**
 * This is the model class for table "coberturas".
 *
 * @property integer $id_cobertura
 * @property string $nombre
 *
 * @property InterProdCob[] $interProdCobs
 */
class Coberturas extends \yii\db\ActiveRecord
{
    public $productos;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coberturas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 50],
            ['productos', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */

  public function afterSave($insert, $changedAttributes){
		\Yii::$app->db->createCommand()->delete('inter_prod_cob', 'id_cobertura = '.(int) $this->id_cobertura)->execute();
      if(!empty($this->productos)) {
        		foreach ($this->productos as $id) {
        			$ro = new InterProdCob();
        			$ro->id_cobertura = $this->id_cobertura;
        			$ro->id_producto = $id;
        			$ro->save();
        		}
        }
	}
    public function attributeLabels()
    {
        return [
            'id_cobertura' => Yii::t('app', 'Id Cobertura'),
            'nombre' => Yii::t('app', 'Nombre'),
			'estatus' => Yii::t('app', 'Estatus'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterProdCobs()
    {
        return $this->hasMany(InterProdCob::className(), ['id_cobertura' => 'id_cobertura']);
    }

    public function getProductos()
	{
		return $this->hasMany(Productos::className(), ['id_producto' => 'id_producto'])
			->viaTable('inter_prod_cob', ['id_cobertura' => 'id_cobertura']);
	}

	public function getProductosList()
	{
		return $this->getProductos()->asArray();
	}

  public static function getCoberturas($id)//recibiendo datos del recibo
    {

        return Poliza::find()
        ->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza')
        ->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo')
        ->innerJoin('certificado_detalle', 'certificado_detalle.id_certificado = certificado.id_certificado')
        ->innerJoin('coberturas', 'coberturas.id_cobertura=certificado_detalle.id_cobertura')
        ->where("poliza.id_poliza=".$id."")
        ->select("certificado_detalle.*,coberturas.*,recibo.id_recibo,certificado.id_producto,
                  (CASE WHEN certificado_detalle.id_cobertura=1
                  THEN 0
                  ELSE 1 END) AS tipo")
        ->all();
    }



}
