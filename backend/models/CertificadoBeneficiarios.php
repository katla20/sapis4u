<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "certificado_beneficiarios".
 *
 * @property integer $id_cliente
 * @property integer $id_certificado
 * @property string $beneficiario_porcentaje
 *
 * @property Certificado $idCertificado
 * @property Cliente $idCliente
 */
class CertificadoBeneficiarios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'certificado_beneficiarios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_cliente', 'id_certificado', 'beneficiario_porcentaje'], 'required'],
            [['id_cliente', 'id_certificado'], 'integer'],
            [['beneficiario_porcentaje'], 'number'],
            [['id_certificado'], 'exist', 'skipOnError' => true, 'targetClass' => Certificado::className(), 'targetAttribute' => ['id_certificado' => 'id_certificado']],
            [['id_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['id_cliente' => 'id_cliente']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_cliente' => Yii::t('app', 'Id Cliente'),
            'id_certificado' => Yii::t('app', 'Id Certificado'),
            'beneficiario_porcentaje' => Yii::t('app', 'Beneficiario Porcentaje'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCertificado()
    {
        return $this->hasOne(Certificado::className(), ['id_certificado' => 'id_certificado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCliente()
    {
        return $this->hasOne(Cliente::className(), ['id_cliente' => 'id_cliente']);
    }
}
