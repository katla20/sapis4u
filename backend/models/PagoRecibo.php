<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pago_recibo".
 *
 * @property integer $id_pago
 * @property integer $id_recibo
 * @property string $estatus
 */
class PagoRecibo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pago_recibo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pago', 'id_recibo'], 'required'],
            [['id_pago', 'id_recibo'], 'integer'],
            [['estatus'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pago' => 'Id Pago',
            'id_recibo' => 'Id Recibo',
            'estatus' => 'Estatus',
        ];
    }
}
