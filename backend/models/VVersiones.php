<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "v_versiones".
 *
 * @property string $modelos
 * @property string $id_modelo2
 * @property string $version
 * @property string $tipo
 * @property string $caja
 * @property string $motor
 * @property integer $anio
 * @property string $id_modelo
 * @property integer $id_version
 * @property double $valor
 * @property integer $carga
 * @property integer $pasajeros
 * @property integer $peso
 * @property integer $lista
 * @property integer $ws_inhabilitado
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 */
class VVersiones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_versiones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['anio', 'id_version', 'carga', 'pasajeros', 'peso', 'lista', 'ws_inhabilitado', 'id_user_registro', 'id_user_actualizacion'], 'integer'],
            [['valor'], 'number'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['modelos', 'tipo', 'caja'], 'string', 'max' => 50],
            [['id_modelo2', 'id_modelo'], 'string', 'max' => 15],
            [['version'], 'string', 'max' => 150],
            [['motor'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'modelos' => Yii::t('app', 'Modelos'),
            'id_modelo2' => Yii::t('app', 'Id Modelo2'),
            'version' => Yii::t('app', 'Version'),
            'tipo' => Yii::t('app', 'Tipo'),
            'caja' => Yii::t('app', 'Caja'),
            'motor' => Yii::t('app', 'Motor'),
            'anio' => Yii::t('app', 'Anio'),
            'id_modelo' => Yii::t('app', 'Id Modelo'),
            'id_version' => Yii::t('app', 'Id Version'),
            'valor' => Yii::t('app', 'Valor'),
            'carga' => Yii::t('app', 'Carga'),
            'pasajeros' => Yii::t('app', 'Pasajeros'),
            'peso' => Yii::t('app', 'Peso'),
            'lista' => Yii::t('app', 'Lista'),
            'ws_inhabilitado' => Yii::t('app', 'Ws Inhabilitado'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
        ];
    }
}
