<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "recibo".
 *
 * @property integer $id_recibo
 * @property integer $id_poliza
 * @property string $fecha_vigenciadesde
 * @property string $fecha_vigenciahasta
 * @property integer $estatus
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property integer $id_subcontratante
 * @property string $estatus_recibo
 * @property integer $id_vendedor
 * @property integer $id_productor
 * @property string $tipo_recibo
 * @property string $comision_total
 * @property string $comision_vendedor
 * @property string $nro_recibo
 *
 * @property Certificado[] $certificados
 * @property Poliza $idPoliza
 * @property Reclamos[] $reclamos
 */
class PolizaAnulacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'poliza';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha_anulacion', 'motivo_anulacion'], 'required'],
            [['fecha_anulacion'], 'safe'],
            [['motivo_anulacion'],'string'],
            [['estatus_poliza'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fecha_anulacion' => Yii::t('app', 'Fecha Anulacion'),
            'motivo_anulacion' => Yii::t('app', 'Motivo Anulacion'),
            'estatus_poliza' => Yii::t('app', 'Estatus'),
        ];
    }
	
	public static function CantReclamos($id)
      {
          $connection = \Yii::$app->db;
        $sql = 'select count(DISTINCT id_reclamo) AS numero from reclamos 
				inner join recibo on (recibo.id_recibo=reclamos.id_recibo)
				inner join poliza on (poliza.id_poliza=recibo.id_poliza 
				and recibo.fecha_vigenciadesde >= poliza.fecha_vigenciadesde 
				and recibo.fecha_vigenciahasta <=poliza.fecha_vigenciahasta  )
                where (poliza.id_poliza= :id OR recibo.id_recibo= :id) ';
        $command = $connection->createCommand($sql);
        $command->bindValue(":id", $id);
        
        $result = $command->queryOne();


        if ($result['numero'] != null){
            return $result['numero'];
        } else {
            return false;
        }
       
      }



}
