<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "inter_recibo_factura".
 *
 * @property integer $id_recibo
 * @property integer $id_facturacion
 *
 * @property Facturacion $idFacturacion
 * @property Recibo $idRecibo
 */
class InterReciboFactura extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inter_recibo_factura';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_recibo', 'id_facturacion'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_recibo' => 'Id Recibo',
            'id_facturacion' => 'Id Facturacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdFacturacion()
    {
        return $this->hasOne(Facturacion::className(), ['id_facturacion' => 'id_facturacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRecibo()
    {
        return $this->hasOne(Recibo::className(), ['id_recibo' => 'id_recibo']);
    }
}
