<?php
namespace backend\models;

use yii;

use yii\helpers\Url;//linea para el asistente de url keyla bullon


class Graficas {

    public static function getTotales()
    {
		  $result=array();
        $connection = \Yii::$app->db;
        $sql = "SELECT aseguradora,sum(comision) AS comision, sum(COALESCE(reclamo,0)) AS reclamo
				 FROM(
				  SELECT
				  a.nombre AS aseguradora,
				  prima AS comision,
				  (SELECT sum(monto) from  reclamos where id_recibo=r.id_recibo) AS reclamo
				  FROM aseguradoras a
				  INNER JOIN poliza p ON (p.id_aseguradora=a.id_aseguradora)
				  INNER JOIN recibo r ON (r.id_poliza=p.id_poliza)
				  INNER JOIN certificado AS c ON (c.id_recibo=r.id_recibo)
				  INNER JOIN certificado_detalle AS cd ON (cd.id_certificado=c.id_certificado)
				  WHERE r.estatus_recibo NOT IN  ('Anulado','Eliminado') AND p.estatus_poliza <> 'Anulada'
				  AND to_char(r.fecha_vigenciadesde,'YYYY')=:anio
				  ) AS total
				GROUP BY aseguradora";
        $command = $connection->createCommand($sql);
	    $anio=date("Y");
        $command->bindValue(":anio", $anio);
        $result = $command->queryAll();

        if (count($result)!= 0){
			        return $result;
        }else return 0;
    }




}
