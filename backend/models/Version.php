<?php

namespace backend\models;

use Yii;


/**
 * This is the model class for table "version".
 *
 * @property string $nombre
 * @property string $tipo
 * @property string $caja
 * @property string $motor
 * @property integer $anio
 * @property string $id_modelo
 * @property integer $id_version
 * @property double $valor
 * @property integer $carga
 * @property integer $pasajeros
 * @property integer $peso
 * @property integer $lista
 * @property integer $ws_inhabilitado
 * @property string $fecha_registro
 * @property string $fecha_modificacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 */
class Version extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'version';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['anio', 'carga', 'pasajeros', 'peso', 'lista', 'ws_inhabilitado', 'id_user_registro', 'id_user_actualizacion'], 'integer'],
            [['valor'], 'number'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['nombre'], 'string', 'max' => 150],
            [['tipo', 'caja'], 'string', 'max' => 50],
            [['motor'], 'string', 'max' => 20],
            [['id_modelo'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
		        'id_version' => Yii::t('app', 'ID'),
		        'id_modelo' => Yii::t('app', 'Modelo'),
			      'id_modelo2' => Yii::t('app', 'Modelo'),
            'nombre' => Yii::t('app', 'Version'),
			      'anio' => Yii::t('app', 'Año'),
            'tipo' => Yii::t('app', 'Tipo'),
            'caja' => Yii::t('app', 'Caja'),
            'motor' => Yii::t('app', 'Motor'),
            'valor' => Yii::t('app', 'Valor'),
            'carga' => Yii::t('app', 'Carga'),
            'pasajeros' => Yii::t('app', 'Pasajeros'),
            'peso' => Yii::t('app', 'Peso'),
            'lista' => Yii::t('app', 'Lista'),
            'ws_inhabilitado' => Yii::t('app', 'Estatus'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
        ];
    }

	/**
     * @return \yii\db\ActiveQuery
     */
    public function getIdModelos()
    {
        return $this->hasOne(Modelos::className(), ['id_modelo' => 'id_modelo2']);
    }

	 public static function datos_version($id)
    {
        return Version::find()
            ->joinWith(['idModelos'])
            ->where("id_version =".$id )
            ->one();
    }
}
