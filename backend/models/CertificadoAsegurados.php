<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "certificado_asegurados".
 *
 * @property integer $id_cliente
 * @property integer $id_certificado
 *
 * @property Certificado $idCertificado
 * @property Cliente $idCliente
 */
class CertificadoAsegurados extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'certificado_asegurados';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_cliente', 'id_certificado'], 'required'],
            [['id_cliente', 'id_certificado'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_cliente' => Yii::t('app', 'Id Cliente'),
            'id_certificado' => Yii::t('app', 'Id Certificado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCertificado()
    {
        return $this->hasOne(Certificado::className(), ['id_certificado' => 'id_certificado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCliente()
    {
        return $this->hasOne(Cliente::className(), ['id_cliente' => 'id_cliente']);
    }

    /**
     * @inheritdoc
     * @return CertificadoAseguradosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CertificadoAseguradosQuery(get_called_class());
    }
}
