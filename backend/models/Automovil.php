<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "automovil".
 *
 * @property integer $id_automovil
 * @property integer $id_version
 * @property string $placa
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property string $color
 * @property string $serial_motor
 * @property string $serial_carroceria
 * @property integer $id_tipo_vehiculo
 * @property integer $id_uso
 * @property string $filename
 * @property string $id_modelo
 * @property string $estatus
 *
 * @property Modelos $idModelo
 * @property TipoVehiculo $idTipoVehiculo
 * @property Uso $idUso
 * @property Version $idVersion
 * @property Certificado[] $certificados
 * @property Documentos[] $documentos
 */
class Automovil extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
     public $id_marca,$id_anio,$marca,$modelo,$version,$anio,$tipo_vehiculo,$uso;

    public static function tableName()
    {
        return 'automovil';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_version', 'placa', 'fecha_registro', 'id_user_registro', 'id_modelo'], 'required'],
            [['id_version', 'id_user_registro', 'id_user_actualizacion', 'id_tipo_vehiculo', 'id_uso'], 'integer'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['serial_motor'], 'string'],
            [['estatus'], 'number'],
            [['placa'], 'string', 'max' => 10],
            [['color'], 'string', 'max' => 20],
            [['serial_carroceria'], 'string', 'max' => 30],
            [['filename'], 'string', 'max' => 100],
            [['id_modelo'], 'string', 'max' => 15],
            [['id_modelo'], 'exist', 'skipOnError' => true, 'targetClass' => Modelos::className(), 'targetAttribute' => ['id_modelo' => 'id_modelo']],
            [['id_tipo_vehiculo'], 'exist', 'skipOnError' => true, 'targetClass' => TipoVehiculo::className(), 'targetAttribute' => ['id_tipo_vehiculo' => 'id_tipo_vehiculo']],
            [['id_uso'], 'exist', 'skipOnError' => true, 'targetClass' => Uso::className(), 'targetAttribute' => ['id_uso' => 'id_uso']],
            [['id_version'], 'exist', 'skipOnError' => true, 'targetClass' => Version::className(), 'targetAttribute' => ['id_version' => 'id_version']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
          'id_automovil' => Yii::t('app', 'Automovil'),
          'id_version' => Yii::t('app', 'Version'),
          'placa' => Yii::t('app', 'Placa'),
          'fecha_registro' => Yii::t('app', 'Fecha Registro'),
          'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
          'id_user_registro' => Yii::t('app', 'Id User Registro'),
          'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
          'color' => Yii::t('app', 'Color'),
          'serial_motor' => Yii::t('app', 'Serial del Motor'),
          'serial_carroceria' => Yii::t('app', 'Serial de la Carroceria'),
          'id_tipo_vehiculo' => Yii::t('app', 'Tipo Vehiculo'),
          'id_uso' => Yii::t('app', 'Uso'),
          'id_marca' => Yii::t('app', 'Marca'),
          'id_modelo' => Yii::t('app', 'Modelo'),
          'id_anio' => Yii::t('app', 'Año'),
          'marca' => Yii::t('app', 'Marca'),
          'modelo' => Yii::t('app', 'Modelo'),
          'version' => Yii::t('app', 'Version'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdModelo()
    {
        return $this->hasOne(Modelos::className(), ['id_modelo' => 'id_modelo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTipoVehiculo()
    {
        return $this->hasOne(TipoVehiculo::className(), ['id_tipo_vehiculo' => 'id_tipo_vehiculo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUso()
    {
        return $this->hasOne(Uso::className(), ['id_uso' => 'id_uso']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdVersion()
    {
        return $this->hasOne(Version::className(), ['id_version' => 'id_version']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCertificados()
    {
        return $this->hasMany(Certificado::className(), ['id_automovil' => 'id_automovil']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentos()
    {
        return $this->hasMany(Documentos::className(), ['id_automovil' => 'id_automovil']);
    }

    public static function datos_vehiculo($id)
     {
         return Automovil::find()
         ->leftJoin('uso', 'uso.id_uso = automovil.id_uso')
         ->leftJoin('tipo_vehiculo', 'tipo_vehiculo.id_tipo_vehiculo = automovil.id_tipo_vehiculo')
         ->leftJoin('version', 'version.id_version = automovil.id_version')
         ->leftJoin('modelos', 'modelos.id_modelo = automovil.id_modelo')
         ->leftJoin('marcas', 'marcas.id_marca = modelos.id_marca')
         ->where("automovil.id_automovil = :automovil",[':automovil' => $id])
         ->select("automovil.*,marcas.nombre AS marca,modelos.nombre AS modelo,marcas.id_marca,version.nombre AS version,
                   version.anio,tipo_vehiculo.nombre AS tipo_vehiculo,uso.nombre AS uso")->one();
     }
     public static function datos_vehiculo_update($id)
      {
         /*SELECT "automovil".*, "marcas"."nombre" AS "marca", "modelos"."nombre" AS "modelo", "marcas"."id_marca",
         "version"."nombre" AS "version", "version"."id_version", "version"."anio" AS "id_anio",
         "tipo_vehiculo"."nombre" AS "tipo_vehiculo", "uso"."nombre" AS "uso" FROM "automovil"
         LEFT JOIN "uso" ON uso.id_uso = automovil.id_uso
         LEFT JOIN "tipo_vehiculo" ON tipo_vehiculo.id_tipo_vehiculo = automovil.id_tipo_vehiculo
         LEFT JOIN "version" ON version.id_version = automovil.id_version
         LEFT JOIN "modelos" ON modelos.id_modelo = automovil.id_modelo
         LEFT JOIN "marcas" ON marcas.id_marca = modelos.id_marca
         WHERE automovil.id_automovil = '79'*/

          return Automovil::find()
          ->leftJoin('uso', 'uso.id_uso = automovil.id_uso')
          ->leftJoin('tipo_vehiculo', 'tipo_vehiculo.id_tipo_vehiculo = automovil.id_tipo_vehiculo')
          ->leftJoin('version', 'version.id_version = automovil.id_version')
          ->leftJoin('modelos', 'modelos.id_modelo = automovil.id_modelo')
          ->leftJoin('marcas', 'marcas.id_marca = modelos.id_marca')
          ->where("automovil.id_automovil = :automovil",[':automovil' => $id])
          ->select("automovil.*,marcas.nombre AS marca,modelos.nombre AS modelo,marcas.id_marca,version.nombre AS version,version.id_version,
                    version.anio AS id_anio,tipo_vehiculo.nombre AS tipo_vehiculo,uso.nombre AS uso")->one();
      }
     public static function datosVehiculoArray($id)
     {
         return Automovil::find()
         ->leftJoin('uso', 'uso.id_uso = automovil.id_uso')
         ->leftJoin('tipo_vehiculo', 'tipo_vehiculo.id_tipo_vehiculo = automovil.id_tipo_vehiculo')
         ->leftJoin('version', 'version.id_version = automovil.id_version')
         ->leftJoin('modelos', 'modelos.id_modelo = automovil.id_modelo')
         ->leftJoin('marcas', 'marcas.id_marca = modelos.id_marca')
         ->where("automovil.id_automovil = :automovil",[':automovil' => $id])
         ->select(["automovil.*","marcas.nombre AS marca","modelos.nombre AS modelo","marcas.id_marca","version.nombre AS version",
                   "version.anio",
                  "(CASE WHEN tipo_vehiculo.nombre IS NOT NULL THEN tipo_vehiculo.nombre ELSE 'N/A' END) AS tipo_vehiculo",
                  "(CASE WHEN uso.nombre IS NOT NULL THEN uso.nombre ELSE 'N/A' END) AS uso"])->asArray()->one();
     }
     
     public static function datos_documentos($id)
     {
         return Automovil::find()
         ->innerJoin('documentos', 'documentos.identificacion = automovil.placa')
         ->where("automovil.id_automovil = :automovil",[':automovil' => $id])
         ->select(["documentos.*"])->asArray()->all();
     }
     
     
     
}
