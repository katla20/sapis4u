<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "intermediario".
 *
 * @property integer $id_intermediario
 * @property integer $id_persona
 * @property string $codigo
 * @property string $comision
 * @property integer $id_tipo_intermediario
 * @property integer $estatus
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 *
 * @property Persona $idPersona
 * @property TipoIntermediario $idTipoIntermediario
 * @property User $idUserRegistro
 * @property User $idUserActualizacion
 */
class Intermediario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $nombre_completo;
    public static function tableName()
    {
        return 'intermediario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_persona', 'id_user_registro', 'fecha_registro'], 'required'],
            [['id_persona', 'id_tipo_intermediario', 'estatus', 'id_user_registro', 'id_user_actualizacion'], 'integer'],
            [['comision'], 'number'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['codigo'], 'string', 'max' => 15],
            [['id_persona'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(), 'targetAttribute' => ['id_persona' => 'id_persona']],
            [['id_tipo_intermediario'], 'exist', 'skipOnError' => true, 'targetClass' => TipoIntermediario::className(), 'targetAttribute' => ['id_tipo_intermediario' => 'id_tipo_intermediario']],
            [['id_user_registro'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user_registro' => 'id']],
            [['id_user_actualizacion'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user_actualizacion' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_intermediario' => Yii::t('app', 'Id Intermediario'),
            'id_persona' => Yii::t('app', 'Id Persona'),
            'codigo' => Yii::t('app', 'Codigo'),
            'comision' => Yii::t('app', 'Comision'),
            'id_tipo_intermediario' => Yii::t('app', 'Tipo Intermediario'),
            'estatus' => Yii::t('app', 'Estatus'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPersona()
    {
        return $this->hasOne(Persona::className(), ['id_persona' => 'id_persona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTipoIntermediario()
    {
        return $this->hasOne(TipoIntermediario::className(), ['id_tipo_intermediario' => 'id_tipo_intermediario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUserRegistro()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user_registro']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUserActualizacion()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user_actualizacion']);
    }
}
