<?php

namespace backend\models;

use common\models\User;
use yii\base\Model;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $rol_id
 * @property string $nombre
 * @property string $nombre2
 * @property string $apellido
 * @property string $apellido2
 * @property integer $cedula
 *
 * @property Cliente[] $clientes
 * @property Cliente[] $clientes0
 * @property DatosBancarios[] $datosBancarios
 * @property DatosBancarios[] $datosBancarios0
 * @property Direccion[] $direccions
 * @property Direccion[] $direccions0
 * @property Intermediario[] $intermediarios
 * @property Intermediario[] $intermediarios0
 */
class User2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	public $newPassword; 
	 
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at','rol_id','nombre','apellido','cedula'], 'required'],
            [['status', 'created_at', 'updated_at', 'rol_id', 'cedula'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['nombre', 'nombre2', 'apellido', 'apellido2'], 'string', 'max' => 50],
            [['cedula'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['username'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'rol_id' => Yii::t('app', 'Rol ID'),
            'nombre' => Yii::t('app', 'Nombre'),
            'nombre2' => Yii::t('app', ''),
            'apellido' => Yii::t('app', 'Apellido'),
            'apellido2' => Yii::t('app', ''),
            'cedula' => Yii::t('app', 'Cedula'),
        ];
    }
    
     /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password_hash);
            $user->rol_id = $this->rol_id;
            $user->nombre = $this->nombre;
            $user->nombre2 = $this->nombre2;
            $user->apellido = $this->apellido;
            $user->apellido2 = $this->apellido2;
            $user->cedula = $this->cedula;
            $user->fecha_registro=date('Y-m-d H:i:s');
            $user->id_user_registro =Yii::$app->user->identity->id;
            $user->generateAuthKey();
		
            if ($user->save()) {
                return $user;
            }
      
    }
	
	

	public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
           
 
            if ($this->newPassword){
                  $this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($this->newPassword);

            return true;
			}
           return false;
        }
     } 
    
	
	

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientes()
    {
        return $this->hasMany(Cliente::className(), ['id_user_registro' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientes0()
    {
        return $this->hasMany(Cliente::className(), ['id_user_actualizacion' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDatosBancarios()
    {
        return $this->hasMany(DatosBancarios::className(), ['id_user_registro' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDatosBancarios0()
    {
        return $this->hasMany(DatosBancarios::className(), ['id_user_actualizacion' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDireccions()
    {
        return $this->hasMany(Direccion::className(), ['id_user_registro' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDireccions0()
    {
        return $this->hasMany(Direccion::className(), ['id_user_actualizacion' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIntermediarios()
    {
        return $this->hasMany(Intermediario::className(), ['id_user_registro' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIntermediarios0()
    {
        return $this->hasMany(Intermediario::className(), ['id_user_actualizacion' => 'id']);
    }
}
