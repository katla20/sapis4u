<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "anexo".
 *
 * @property integer $id_anexo
 * @property integer $id_poliza
 * @property string $titulo
 * @property string $descripcion
 * @property integer $id_recibo
 *
 * @property Recibo $idRecibo
 */
class Anexo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
     
     public $id_poliza,$recibo;
    public static function tableName()
    {
        return 'anexo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_poliza', 'id_recibo'], 'integer'],
            [['titulo', 'descripcion'], 'required'],
            [['titulo'], 'string', 'max' => 100],
            [['descripcion'], 'string', 'max' => 250],
            [['id_recibo'], 'exist', 'skipOnError' => true, 'targetClass' => Recibo::className(), 'targetAttribute' => ['id_recibo' => 'id_recibo']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_anexo' => Yii::t('app', 'Id Anexo'),
            'id_poliza' => Yii::t('app', 'Id Poliza'),
            'titulo' => Yii::t('app', 'Titulo'),
            'descripcion' => Yii::t('app', 'Descripcion'),
            'id_recibo' => Yii::t('app', 'Id Recibo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRecibo()
    {
        return $this->hasOne(Recibo::className(), ['id_recibo' => 'id_recibo']);
    }
}
