<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "carga_familiar".
 *
 * @property integer $id_beneficiario
 * @property integer $id_titular
 * @property integer $id_carga_familiar
 * @property string $parentesco
 * @property string $fecha_registro
 * @property integer $id_user_registro
 * @property string $fecha_actualizacion
 * @property string $id_user_actualizacion
 * @property integer $estatus
 *
 * @property Cliente $idBeneficiario
 * @property Cliente $idTitular
 */
class CargaFamiliar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $nombre,$segundo_nombre,$apellido,$segundo_apellido,$identificacion,$fecha_nacimiento,$sexo; 
    public static function tableName()
    {
        return 'carga_familiar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           [['id_titular', 'id_beneficiario', 'parentesco', 'fecha_registro', 'id_user_registro',
               'nombre','apellido','identificacion','fecha_nacimiento','sexo'], 'required'],
            [['id_beneficiario', 'id_titular', 'id_user_registro', 'estatus'], 'integer'],
            [['fecha_registro', 'fecha_actualizacion', 'id_user_actualizacion'], 'safe'],
            [['parentesco'], 'string', 'max' => 20],
            [['id_beneficiario'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['id_beneficiario' => 'id_cliente']],
            [['id_titular'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['id_titular' => 'id_cliente']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_beneficiario' => Yii::t('app', 'Id Beneficiario'),
            'id_titular' => Yii::t('app', 'Id Titular'),
            'id_carga_familiar' => Yii::t('app', 'Id Carga Familiar'),
            'parentesco' => Yii::t('app', 'Parentesco'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'estatus' => Yii::t('app', 'Estatus')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdBeneficiario()
    {
        return $this->hasOne(Cliente::className(), ['id_cliente' => 'id_beneficiario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTitular()
    {
        return $this->hasOne(Cliente::className(), ['id_cliente' => 'id_titular']);
    }
	
	public function getCargaFamiliarForm(){

      $query = (new \yii\db\Query())
              ->select(['nombre','segundo_nombre','apellido','segundo_apellido','parentesco','identificacion','fecha_nacimiento','sexo'])
              ->from('carga_familiar')
              ->where('carga_familiar.id_titular=:id', array(':id'=>$modelCliente->id_cliente))
			  ->innerJoin('cliente', 'cliente.id_cliente = carga_familiar.id_beneficiario')
              ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
              ->createCommand();
        return $query->queryAll();
         //echo $query->sql;exit();

    }
	
	
	
}
