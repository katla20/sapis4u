<?php
namespace backend\models;

use yii;

use yii\helpers\Url;//linea para el asistente de url keyla bullon
use backend\models\Reclamos;




class ConsultaMensaje {

    public static function getMensajes()
    {
		$res=array();
        $connection = \Yii::$app->db;
        $sql = "select count(mensaje) AS mensaje from reclamo_seguimiento where user_destino=:id and estatus_mensaje=0";
        $command = $connection->createCommand($sql);
        $user=(isset(Yii::$app->user->identity->id))? Yii::$app->user->identity->id:0;
        $command->bindValue(":id", $user);
        $result = $command->queryAll();

        if (count($result)!= 0){

           foreach($result as $row){
              
              return $row['mensaje'];
                
          }
        }
    }
    
     public static function getLeer($id)
    {
		$res=array();
        $connection = \Yii::$app->db;
        $sql = "UPDATE reclamo_seguimiento set estatus_mensaje=1 where user_destino=:id AND estatus_mensaje=0 
        AND id_reclamo=".$id;
        $command = $connection->createCommand($sql);
        $user=(isset(Yii::$app->user->identity->id))? Yii::$app->user->identity->id:0;
        $command->bindValue(":id", $user);
        $result = $command->queryAll();
    }


}
