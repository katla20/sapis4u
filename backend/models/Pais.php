<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pais".
 *
 * @property integer $id_pais
 * @property string $estatus
 * @property string $nombre
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property string $descripcion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 *
 * @property Aseguradoras[] $aseguradoras
 * @property Estado[] $estados
 */
class Pais extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pais';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['estatus', 'nombre'], 'required'],
            [['nombre', 'descripcion'], 'string'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['id_user_registro', 'id_user_actualizacion'], 'integer'],
            [['estatus'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pais' => Yii::t('app', 'Id Pais'),
            'estatus' => Yii::t('app', 'Estatus'),
            'nombre' => Yii::t('app', 'Nombre'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'descripcion' => Yii::t('app', 'Descripcion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAseguradoras()
    {
        return $this->hasMany(Aseguradoras::className(), ['id_pais' => 'id_pais']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstados()
    {
        return $this->hasMany(Estado::className(), ['id_pais' => 'id_pais']);
    }
}
