<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "operacion".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $rotulo
 * @property string $url
 * @property integer $id_padre
 */
class Operacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'operacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre','etiqueta','menu'], 'required'],
            [['url'], 'string'],
            [['id_padre','estatus','menu'], 'integer'],
			[['etiqueta'], 'integer'],
			[['orden'], 'integer'],
            [['nombre'], 'string', 'max' => 64],
			[['icono'], 'string', 'max' => 40],
            [['rotulo'], 'string', 'max' => 25]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'rotulo' => 'Rotulo',
            'url' => 'Url',
            'id_padre' => 'Padre',
			'idPadre.nombre' => 'Padre',
			'etiqueta' => 'Etiqueta',
			'orden' => 'Orden',
			'icono' => 'Icono',
			'estatus' => 'Estatus',
            'menu' => 'Menu',
        ];
    }
	
	public function getIdPadre()
    {
        return $this->hasOne(Operacion::className(), ['id' => 'id_padre']);
    }
}
