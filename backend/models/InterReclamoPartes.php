<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "inter_reclamo_partes".
 *
 * @property integer $id_reclamo
 * @property integer $id_parte
 *
 * @property PartesVehiculos $idParte
 * @property Reclamos $idReclamo
 */
class InterReclamoPartes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inter_reclamo_partes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_reclamo', 'id_parte'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_reclamo' => Yii::t('app', 'Id Reclamo'),
            'id_parte' => Yii::t('app', 'Id Parte'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdParte()
    {
        return $this->hasOne(PartesVehiculos::className(), ['id_parte' => 'id_parte']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdReclamo()
    {
        return $this->hasOne(Reclamos::className(), ['id_reclamo' => 'id_reclamo']);
    }
}
