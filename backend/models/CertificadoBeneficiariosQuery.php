<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[CertificadoBeneficiarios]].
 *
 * @see CertificadoBeneficiarios
 */
class CertificadoBeneficiariosQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return CertificadoBeneficiarios[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CertificadoBeneficiarios|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}