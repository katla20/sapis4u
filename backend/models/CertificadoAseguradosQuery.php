<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[CertificadoAsegurados]].
 *
 * @see CertificadoAsegurados
 */
class CertificadoAseguradosQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return CertificadoAsegurados[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CertificadoAsegurados|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}