<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "persona".
 *
 * @property string $nombre
 * @property string $segundo_nombre
 * @property string $apellido
 * @property string $fecha_nacimiento
 * @property string $identificacion
 * @property string $identificacion_2
 * @property string $identidad_digitorif

 * @property string $sexo
 * @property string $correo
 * @property string $tipo
 * @property integer $id_persona
 * @property string $segundo_apellido
 * @property integer $estatus
 * @property integer $nit
 * @property string $actividad_economica
 * @property string $numero_registro
 * @property string $representante
 *
 * @property Cliente[] $clientes
 * @property Direccion[] $direccions
 * @property Documentos[] $documentos
 * @property Intermediario[] $intermediarios
 */
class Persona extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	var $codigo,$comision,$nombre_direccion,$parentesco,$preidentificacion,$postidentificacion,$telefono,$telefono2,$telefono3,$action;
    public static function tableName()
    {
        return 'persona';
    }

    /**
     * @inheritdoc
     */
	  const SCENARIO_CLIENTE = 'cliente';
      const SCENARIO_VENDEDOR = 'vendedor';
	  const SCENARIO_FAMILIA = 'familia';

    public function scenarios()
    {
        return [
            self::SCENARIO_CLIENTE => ['nombre', 'segundo_nombre', 'apellido', 'segundo_apellido','sexo','fecha_nacimiento','correo',
			      'identificacion','identificacion_2','nit','tipo','preidentificacion','postidentificacion','actividad_economica','identidad_digitorif',
			      'numero_registro','fecha_registroempresa','representante','action'],
            self::SCENARIO_VENDEDOR =>['nombre', 'segundo_nombre', 'apellido', 'segundo_apellido','sexo','tipo','fecha_nacimiento','correo','identificacion','identificacion_2','identidad_digitorif','codigo','comision','preidentificacion','postidentificacion'],
			      self::SCENARIO_FAMILIA => ['nombre', 'segundo_nombre', 'apellido', 'segundo_apellido','sexo','fecha_nacimiento','identificacion','identificacion_2','parentesco','postidentificacion'],
        ];
    }
    public function rules()
    {
        return [

			[['nombre','fecha_nacimiento','correo','identificacion','comision','tipo'], 'required', 'on' => self::SCENARIO_VENDEDOR],
			[['identificacion'], 'validateIntermediario', 'on' => self::SCENARIO_VENDEDOR],

			[['nombre','correo','identificacion','fecha_nacimiento','tipo','action'], 'required', 'on' => self::SCENARIO_CLIENTE],
			[['identificacion'], 'validateCliente', 'on' =>self::SCENARIO_CLIENTE],

			/*[['apellido','sexo','fecha_nacimiento'], 'required', 'when' => function ($model) {
                    return ($model->tipo == 'V');
            }],*/

			//[['apellido','sexo','fecha_nacimiento',
			//'numero_registro','fecha_registroempresa','representante','actividad_economica'], 'validateSegunTipo', 'on' => self::SCENARIO_CLIENTE],

      [['nombre','apellido','sexo','fecha_nacimiento','identificacion','parentesco'], 'required', 'on' => self::SCENARIO_FAMILIA],
      [['fecha_nacimiento','parentesco','postidentificacion'], 'safe'],
      [['estatus', 'nit','identificacion','postidentificacion'], 'integer'],
      [['nombre', 'segundo_nombre', 'apellido', 'segundo_apellido'], 'string', 'max' => 300],
      [['identificacion' ], 'string', 'max' => 10],
      [['identificacion'], 'string', 'min' => 6],
      [['sexo', 'tipo','preidentificacion'], 'string', 'max' => 1],
      [['correo'], 'string', 'max' => 150],
			[['correo'], 'email'],
      [['filename'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nombre' => Yii::t('app', 'Nombre'),
            'segundo_nombre' => Yii::t('app', 'Segundo Nombre'),
            'apellido' => Yii::t('app', 'Apellido'),
            'fecha_nacimiento' => Yii::t('app', 'Fecha Nacimiento'),
            'identificacion' => Yii::t('app', 'Identificación'),
            'identificacion_2' => Yii::t('app', 'Identificaciòn 2'),
            'sexo' => Yii::t('app', 'Sexo'),
            'correo' => Yii::t('app', 'Correo'),
            'tipo' => Yii::t('app', 'Tipo'),
            'id_persona' => Yii::t('app', 'Id Persona'),
            'segundo_apellido' => Yii::t('app', 'Segundo Apellido'),
            'estatus' => Yii::t('app', 'Estatus'),
            'nit' => Yii::t('app', 'Nit'),
            'actividad_economica' => Yii::t('app', 'Actividad E.'),
            'numero_registro' => Yii::t('app', 'Nro Registro'),
            'representante' => Yii::t('app', 'Representante'),
			      'codigo' => Yii::t('app', 'Codigo Vendedor'),
            'comision' => Yii::t('app', 'Porcentaje Comisión'),

        ];
    }

	public function validateIntermediario($attribute, $params)
    {
		$rif="";

		if($this->postidentificacion!=""){

			$rif=" OR persona.identificacion_2='".$this->identificacion."-".$this->postidentificacion."'";

		}

		 $datos=Persona::find()->innerJoin('intermediario', 'intermediario.id_persona = persona.id_persona')
								   ->where("persona.identificacion='".$this->identificacion."'".$rif)
								   ->one();

        if (count($datos)>0) {
            $this->addError('identificacion', 'Intermediario ya existe.');
        }
    }

	public function validateCliente($attribute, $params)
  {
		$rif="";
        //SI ES NECESARIO VALIDAR POR TIPO
      $action=$this->action;
      //$this->addError('identificacion', "$action");

     if($action=='insert'){
          if($this->postidentificacion!=""){
                  $rif=" OR persona.identificacion_2='".$this->identificacion."-".$this->postidentificacion."'";
          }
           $datos=Persona::find()->innerJoin('cliente', 'cliente.id_persona = persona.id_persona')
                       ->where("persona.identificacion='".$this->identificacion."'".$rif)
                       ->one();
          if (count($datos)>0) {
                $this->addError('identificacion', 'El cliente ya existe.');
          }
     }


    }

	public function validateSegunTipo($attribute, $params)
    {

  		if($this->tipo=="V" && ($this->apellido=="" || $this->sexo=="")){//natural
  			$this->addError($attribute,'no debe estar vacio');

  		}else if($this->tipo=="J" && ($this->actividad_economica=="" || $this->numero_registro=="" || $this->representante=="")){//juridica
  			$this->addError($attribute,'no debe estar vacio');
  		}

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientes()
    {
        return $this->hasMany(Cliente::className(), ['id_persona' => 'id_persona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDireccions()
    {
        return $this->hasMany(Direccion::className(), ['id_persona' => 'id_persona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentos()
    {
        return $this->hasMany(Documentos::className(), ['id_persona' => 'id_persona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIntermediarios()
    {
        return $this->hasMany(Intermediario::className(), ['id_persona' => 'id_persona']);
    }

	public static function datosDireccion($id)
    {
          return Persona::find()
          ->innerJoin('direccion', 'direccion.id_persona = persona.id_persona')
          ->leftJoin('ciudad', 'ciudad.id_ciudad = direccion.id_ciudad')
		     ->leftJoin('estado', 'estado.id_estado = ciudad.id_estado')
		     ->leftJoin('zona', 'zona.id_zona = direccion.id_zona')
  		  ->where("persona.id_persona=".$id)
          ->select("direccion.*,estado.nombre AS estado,ciudad.nombre AS ciudad,zona.nombre AS zona")->asArray()->all();
    }

    public static function datosPoliza($id)
    {
          return Persona::find()
          ->innerJoin('cliente AS cl', 'cl.id_persona = persona.id_persona')
          ->leftJoin('certificado AS ce', 'ce.id_titular = cl.id_cliente')
		  ->leftJoin('recibo AS re', 're.id_recibo = ce.id_recibo')
		  ->leftJoin('poliza AS po', 'po.id_poliza = re.id_poliza AND re.fecha_vigenciadesde >= po.fecha_vigenciadesde AND re.fecha_vigenciahasta <= po.fecha_vigenciahasta')
  		  ->where("persona.id_persona=".$id)
          ->select("po.id_poliza,po.numero_poliza,
                    CASE WHEN po.estatus_poliza ='Anulada' THEN po.estatus_poliza
					WHEN po.estatus_poliza <> 'Anulada' and po.fecha_vigenciahasta < NOW() THEN 'Vencida'
 					WHEN po.estatus_poliza <> 'Anulada' and po.fecha_vigenciahasta >= NOW() THEN 'Vigente'
					ELSE po.estatus_poliza END AS estatus_poliza,po.id_tipo,po.id_ramo")
          ->groupBy('po.id_poliza,po.numero_poliza,estatus_poliza,po.id_tipo,po.id_ramo')->asArray()->all();
    }

	public static function getCumpleanio()
    {
		$res=array();
        $connection = \Yii::$app->db;
        $sql = "select count(*) AS numero FROM persona
                 INNER JOIN cliente ON persona.id_persona = cliente.id_persona
                 LEFT JOIN direccion ON direccion.id_persona = persona.id_persona
                 WHERE cliente.estatus = 1 AND to_char(fecha_nacimiento,'dd/mm')= to_char(now(),'dd/mm')";
        $command = $connection->createCommand($sql);
        //$user=(isset(Yii::$app->user->identity->id))? Yii::$app->user->identity->id:0;
        //$command->bindValue(":id", $user);
        $result = $command->queryAll();

        if (count($result)!= 0){

           foreach($result as $row){

              return $row['numero'];

          }
        }
    }
    
     public static function documentos_cliente($id)
     {
         return Persona::find()
         ->innerJoin('documentos', 'documentos.identificacion = persona.identificacion')
         ->where("persona.id_persona = :persona",[':persona' => $id])
         ->select(["documentos.*"])->asArray()->all();
     }
    
}
