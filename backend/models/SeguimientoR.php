<?php
namespace backend\models;

use yii;

use yii\helpers\Url;//linea para el asistente de url keyla bullon
use backend\models\Reclamos;
use kartik\icons\Icon;




class SeguimientoR {

    public static function getMostrar($id)
    {
		$res=array();
        $connection = \Yii::$app->db;
        $sql = "select DISTINCT (to_char(fecha_registro,'mm-yyyy')) AS anio_mes
                FROM reclamo_seguimiento
                where id_reclamo=:id
                order by anio_mes DESC";
        $command = $connection->createCommand($sql);
        $command->bindValue(":id", $id);
        $result = $command->queryAll();

        if (count($result)!= 0){

           foreach($result as $row){
               echo '<li class="time-label">';
                    echo'<span class="bg-green">';
                        echo  $row['anio_mes'];
                    echo'</span>';
               echo '</li>';
               SeguimientoR::comentarios($id,$row['anio_mes']);
          }
        }
    }


  public static function comentarios($id,$anio_mes){

        $res=array();
		$connection = \Yii::$app->db;
		$sql = "SELECT
                rs.comentario,
                rs.monto,
                to_char(rs.fecha_registro, 'dd-mm-yyyy') fecha,
                to_char(rs.fecha_registro, 'HH12:MI:SS') hora ,
                rs.estatus_reclamo,
                (u.nombre||' '||u.apellido) AS nombre,mensaje
                FROM reclamo_seguimiento rs
                INNER JOIN ".'"user"'." AS u on (u.id=rs.id_user_registro)
                WHERE id_reclamo=:id AND to_char(rs.fecha_registro,'mm-yyyy')=:anio_mes
				order by fecha DESC,hora DESC";
		$command = $connection->createCommand($sql);
			//$command->bindValue(":user", $user);
		$user=(isset(Yii::$app->user->identity->id))? Yii::$app->user->identity->id:1;
		$command->bindValue(":id", $id);
		$command->bindValue(":anio_mes", $anio_mes);
		$result = $command->queryAll();
		
		//print_r($result);exit();

		if (count($result)!= 0){
			foreach($result as $row){
				
				if($row['mensaje']==0){

					   echo '<li>';
					   echo '<i class="fa fa-comments bg-yellow"></i>';

					   echo '<div class="timeline-item">';

					   echo '<span class="time"><i class="fa fa-clock-o"></i>'.$row['hora'].'</span>';
					   echo '<span class="time">'.$row['fecha'].'</span>';

					   echo '<h3 class="timeline-header"><i class="fa fa-user bg-aqua"></i>'.$row['nombre'].'</h3>';

					   echo '<div class="timeline-body">';
					   echo '<div class="">'.$row['comentario'].'</div>';
					   echo '<div class=""><strong>MONTO: '.$row['monto'].' Bs.   </strong><div class="kv-attribute"><kbd>ESTATUS RECLAMO:'.$row['estatus_reclamo'].'</kbd></div>';
					  // echo '<div class="kv-attribute"><kbd>ESTATUS RECLAMO:'.$row['estatus_reclamo'].'</kbd></div>';
					   echo '</div>
					   </div>
					   </li>';
				}else{
					
					echo '<li>';
					 echo '<i class="fa fa-envelope bg-blue"></i>';

					 echo '<div class="timeline-item">';

					   echo '<span class="time"><i class="fa fa-clock-o"></i>'.$row['hora'].'</span>';
					   echo '<span class="time">'.$row['fecha'].'</span>';

					   echo '<h3 class="timeline-header"><i class="fa fa-user bg-aqua"></i>  '.$row['nombre'].'</h3>';

					   echo '<div class="timeline-body">
						  '.$row['comentario'].'
						</div>';
					   echo '</div>
					</li>';
					
				}

			}
		}
	}


	public static function getDatos($id)
    {
		$res=array();
        $connection = \Yii::$app->db;
        $sql = "select
				p.numero_poliza,
				(pe.nombre ||' '||apellido) as nombre_completo,
				pe.identificacion,
				re.numero_reclamo,
				ra.nombre AS ramo,
				a.nombre AS aeguradora,
				cer.id_automovil,
				re.monto,
				re.estatus_reclamo,
				re.numero_reclamo,
				tp.nombre AS tipo,
				to_char(re.fecha_declaracion,'dd/mm/yyyy') AS fecha_declaracion
				from poliza as p
				inner join recibo as r on (r.id_poliza=p.id_poliza)
				inner join cliente as c on (c.id_cliente=p.id_contratante)
				inner join persona as pe on (pe.id_persona=c.id_persona)
				inner join aseguradoras a on (a.id_aseguradora=p.id_aseguradora)
				inner join reclamos as re on (re.id_recibo=r.id_recibo)
				inner join certificado as cer on (cer.id_recibo=r.id_recibo)
				inner join automovil as aut on (aut.id_automovil=re.id_automovil)
				inner join ramos as ra on (ra.id_ramo=p.id_ramo)
				INNER JOIN tipo_reclamos as tp on (tp.id_tipo_reclamo=re.id_tipo_reclamo)
				WHERE re.id_reclamo=:id";
        $command = $connection->createCommand($sql);
        $command->bindValue(":id", $id);
        $result = $command->queryAll();
		
		
		$sql2 = "select ph.nombre from reclamos AS r
					INNER JOIN inter_reclamo_partes as irp ON (r.id_reclamo=irp.id_reclamo)
					INNER JOIN partes_vehiculos as ph ON (ph.id_parte=irp.id_parte)
				WHERE r.id_reclamo=:id";
        $command2 = $connection->createCommand($sql2);
        $command2->bindValue(":id", $id);
        $result2 = $command2->queryAll();
		
		if(count($result2)!= 0){
			
			$partes='';
			foreach ($result2 as $par) {
			
			   $partes.= $par['nombre'].", ";
				//$Ram=$Ramos['nombre'];
			  
			 }
			 $partes=substr($partes, 0, -2);
			
		}

        if (count($result)!= 0){

            foreach($result as $row){
			   echo "<div class='bg-aqua top-modulo'>
                       <span class='icon-modulo'>".Icon::show('car', ['class' => 'fa-3x'])."</span>
                       <span style='color:white;font-size:18px;font-weight:bold;'>".Yii::t('app', 'SEGUIMIENTO DEL RECLAMO')."</span>
                     </div>";
			   echo '<div class="panel panel-info">
			         <div class="panel-heading panel-info" style="font-weight:bold;">DATOS DEL RECLAMO</div>
				     <div class="panel-body">';			
               echo '<div class = "table-responsive">
						<table class = "table">';

					echo'<thead>';

						echo'<tr >';
							echo '<td><strong>ASEGURADORA:</strong> '.$row['aeguradora'].'</td>';
							echo '<td><strong>RAMO:</strong>  '.$row['ramo'].'</td>';
							echo '<td><strong>PÒLIZA:</strong> '.$row['numero_poliza'].'</td>';
							echo '<td><strong>CONTRATANTE:</strong> '.$row['nombre_completo'].'</td>';
						echo'</tr>';
						echo'<tr >';
							echo '<td><strong>RECLAMO:</strong> '.$row['numero_reclamo'].'</td>';
							echo '<td><strong>TIPO:</strong> '.$row['tipo'].'</td>';
							echo '<td><strong>ESTATUS:</strong>  '.$row['estatus_reclamo'].'</td>';
							echo '<td><strong>MONTO:</strong> '.$row['monto'].'</td>';
							
						echo '</tr>';
						if(trim($row['ramo'])==trim('Automovil')){
							
				            SeguimientoR::Automovil($row['id_automovil']);
						  
						    echo'<tr>';
							echo '<td><strong>FECHA DECLARACIÒN:</strong>'.$row['fecha_declaracion'].'</td>';
							echo '<td colspan="3"><strong>PARTES AFECTADAS:</strong>'.$partes.'</td>';
							
						echo'</tr>';

			            }else{
						    echo'<tr >';
							echo '<td colspan="4"><strong>FECHA DECLARACIÒN:</strong> '.$row['fecha_declaracion'].'</td>';
						    echo'</tr>';
						}
						
				    echo' </thead>';
                   echo '</table>
					 </div>
					</div>
				</div>	 ';

			}
        }
    }
	
	public static function getDatos2($id)
    {
		$res=array();
        $connection = \Yii::$app->db;
        $sql = "select
				p.numero_poliza,
				(pe.nombre ||' '||pe.apellido) as nombre_completo,
				pe.identificacion,
				re.numero_reclamo,
				ra.nombre AS ramo,
				a.nombre AS aeguradora,
				cer.id_automovil,
				re.monto,
				re.estatus_reclamo,
				re.numero_reclamo,
				tp.nombre AS tipo,
				to_char(re.fecha_declaracion,'dd/mm/yyyy') AS fecha_declaracion
				from poliza as p
				inner join recibo as r on (r.id_poliza=p.id_poliza)
				inner join cliente as c on (c.id_cliente=p.id_contratante)
				inner join persona as pe on (pe.id_persona=c.id_persona)
				inner join aseguradoras a on (a.id_aseguradora=p.id_aseguradora)
				inner join reclamos as re on (re.id_recibo=r.id_recibo)
				inner join certificado as cer on (cer.id_recibo=r.id_recibo)
				inner join persona as per on (per.id_persona=re.id_beneficiario)
				inner join ramos as ra on (ra.id_ramo=p.id_ramo)
				INNER JOIN tipo_reclamos as tp on (tp.id_tipo_reclamo=re.id_tipo_reclamo)
				WHERE re.id_reclamo=:id";
        $command = $connection->createCommand($sql);
        $command->bindValue(":id", $id);
        $result = $command->queryAll();
		
		
		$sql2 = "select ph.nombre from reclamos AS r
					INNER JOIN inter_reclamo_partes as irp ON (r.id_reclamo=irp.id_reclamo)
					INNER JOIN partes_vehiculos as ph ON (ph.id_parte=irp.id_parte)
				WHERE r.id_reclamo=:id";
        $command2 = $connection->createCommand($sql2);
        $command2->bindValue(":id", $id);
        $result2 = $command2->queryAll();
		
		if(count($result2)!= 0){
			
			$partes='';
			foreach ($result2 as $par) {
			
			   $partes.= $par['nombre'].", ";
				//$Ram=$Ramos['nombre'];
			  
			 }
			 $partes=substr($partes, 0, -2);
			
		}

        if (count($result)!= 0){

            foreach($result as $row){
			   echo "<div class='bg-aqua top-modulo'>
                       <span class='icon-modulo'>".Icon::show('car', ['class' => 'fa-3x'])."</span>
                       <span style='color:white;font-size:18px;font-weight:bold;'>".Yii::t('app', 'SEGUIMIENTO DEL RECLAMO')."</span>
                     </div>";
			   echo '<div class="panel panel-info">
			         <div class="panel-heading panel-info" style="font-weight:bold;">DATOS DEL RECLAMO</div>
				     <div class="panel-body">';			
               echo '<div class = "table-responsive">
						<table class = "table">';

					echo'<thead>';

						echo'<tr >';
							echo '<td><strong>ASEGURADORA:</strong> '.$row['aeguradora'].'</td>';
							echo '<td><strong>RAMO:</strong>  '.$row['ramo'].'</td>';
							echo '<td><strong>PÒLIZA:</strong> '.$row['numero_poliza'].'</td>';
							echo '<td><strong>CONTRATANTE:</strong> '.$row['nombre_completo'].'</td>';
						echo'</tr>';
						echo'<tr >';
							echo '<td><strong>RECLAMO:</strong> '.$row['numero_reclamo'].'</td>';
							echo '<td><strong>TIPO:</strong> '.$row['tipo'].'</td>';
							echo '<td><strong>ESTATUS:</strong>  '.$row['estatus_reclamo'].'</td>';
							echo '<td><strong>MONTO:</strong> '.$row['monto'].'</td>';
							
						echo '</tr>';
						if(trim($row['ramo'])==trim('Automovil')){
							
				            SeguimientoR::Automovil($row['id_automovil']);
						  
						    echo'<tr>';
							echo '<td><strong>FECHA DECLARACIÒN:</strong>'.$row['fecha_declaracion'].'</td>';
							echo '<td colspan="3"><strong>PARTES AFECTADAS:</strong>'.$partes.'</td>';
							
						echo'</tr>';

			            }else{
						    echo'<tr >';
							echo '<td colspan="4"><strong>FECHA DECLARACIÒN:</strong> '.$row['fecha_declaracion'].'</td>';
						    echo'</tr>';
						}
						
				    echo' </thead>';
                   echo '</table>
					 </div>
					</div>
				</div>	 ';

			}
        }
    }


	public static function Automovil($id)
    {
		$res=array();
        $connection = \Yii::$app->db;
        $sql = "SELECT
				m.nombre AS marca,
				md.nombre AS modelo,
				v.nombre AS ver,
				a.placa
				FROM automovil a
				LEFT JOIN ".'"version"'." v on (v.id_version=a.id_version)
				LEFT JOIN ".'"modelos"'." md on (md.id_modelo=a.id_modelo)
				LEFT JOIN marcas m on (m.id_marca=md.id_marca)
				WHERE a.id_automovil=:id";
        $command = $connection->createCommand($sql);
        $command->bindValue(":id", $id);
        $result = $command->queryAll();

        if (count($result)!= 0){

            foreach($result as $row){

			    echo'<tr >';
					echo '<td><strong>MARCA:</strong>  '.$row['marca'].'</td>';
					echo '<td><strong>MODELO:</strong>  '.$row['modelo'].'</td>';
					echo '<td><strong>VERSIÒN:</strong>  '.$row['ver'].'</td>';
					echo '<td><strong>PLACA:  </strong>'.$row['placa'].'</td>';
				echo'</tr>';
			}
        }
    }




}
