<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "recibo".
 *
 * @property integer $id_recibo
 * @property integer $id_poliza
 * @property string $fecha_vigenciadesde
 * @property string $fecha_vigenciahasta
 * @property integer $estatus
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property integer $id_subcontratante
 * @property string $estatus_recibo
 * @property integer $id_vendedor
 * @property integer $id_productor
 * @property string $tipo_recibo
 * @property string $comision_total
 * @property string $comision_vendedor
 * @property string $nro_recibo
 *
 * @property Certificado[] $certificados
 * @property Poliza $idPoliza
 * @property Reclamos[] $reclamos
 */
class ReciboAnulacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'recibo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha_anulacion', 'motivo_anulacion'], 'required'],
            [['fecha_anulacion'], 'safe'],
            [['motivo_anulacion'],'string'],
            [['estatus_recibo'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fecha_anulacion' => Yii::t('app', 'Fecha Anulacion'),
            'motivo_anulacion' => Yii::t('app', 'Motivo Anulacion'),
            'estatus_recibo' => Yii::t('app', 'Estatus'),
        ];
    }


}
