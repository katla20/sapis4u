<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "poliza".
 *
 * @property integer $id_poliza
 * @property string $numero_poliza
 * @property string $fecha_vigenciadesde
 * @property string $fecha_vigenciahasta
 * @property integer $estatus
 * @property integer $id_tipo
 * @property integer $id_contratante
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property integer $id_aseguradora
 * @property string $estatus_poliza
 * @property integer $id_ramo
 *
 * @property Aseguradoras $idAseguradora
 * @property Cliente $idContratante
 * @property Recibo[] $recibos
 */
class Poliza extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public $tipo_recibo,$nro_recibo,$producto,$id_automovil,$id_vendedor,$id_beneficiario,$tipo,$id_subcontratante,$id_subcontratante_hidden,
           $id_contratante_hidden,$id_vendedor_hidden,$id_automovil_hidden,$suma,$hi,$nro_certificado,$estatus_recibo,$id_pais,
           $comision_total,$comision_vendedor,$comision_porcentaje,$tipo_vendedor,$asegurados,$beneficiarios,$deducible,$ramo,
           $nombre_completo,$identificacion,$aseguradora,$nombre_cli,$apellido_cli,$nombre_ven,$apellido_ven,$prima,$isTitular,$bienes_asegurados,
		       $marca,$modelo,$version,$anio,$placa,$codigo,$id_recibo,$id_asegurado,$id_asegurado_hidden,$id_producto,$rcbo_fecha_vigenciadesde,$rcbo_fecha_vigenciahasta,
           $indole,$riesgo;
    public static function tableName()
    {
        return 'poliza';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_aseguradora','comision_total','numero_poliza','nro_recibo','tipo_recibo','id_contratante','fecha_vigenciadesde','fecha_vigenciahasta','producto','nro_certificado'], 'required'],
            [['id_contratante_hidden'], 'required','message'=>'El contratante no puede estar vacío'],
            [['id_asegurado_hidden'], 'required','message'=>'El Asegurado no puede estar vacío'],
            [['id_automovil_hidden'], 'required','message'=>'El Automovil no puede estar vacío'],
            [['estatus', 'id_tipo', 'id_contratante', 'id_user_registro', 'id_user_actualizacion', 'id_aseguradora'], 'integer'],
            [['fecha_vigenciadesde', 'fecha_vigenciahasta','rcbo_fecha_vigenciadesde', 'rcbo_fecha_vigenciahasta','aseguradora','fecha_registro', 'fecha_actualizacion','indole','riesgo'], 'safe'],
            [['estatus_poliza'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_poliza' => Yii::t('app', 'Poliza'),
            'numero_poliza' => Yii::t('app', 'Numero Poliza'),
            'fecha_vigenciadesde' => Yii::t('app', 'Fecha Vigencia Desde'),
            'fecha_vigenciahasta' => Yii::t('app', 'Fecha Vigencia Hasta'),
            'estatus' => Yii::t('app', 'Estatus'),
            'id_tipo' => Yii::t('app', 'Tipo'),
            'id_contratante' => Yii::t('app', 'Tomador'),
            'id_subcontratante' => Yii::t('app', 'Subcontratante'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Usuario Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Usuario Actualizacion'),
            'id_aseguradora' => Yii::t('app', 'Aseguradora'),
            'id_pais' => Yii::t('app', 'Pais'),
            'estatus_poliza' => Yii::t('app', 'Estatus'),
            'nro_recibo' => Yii::t('app', 'Numero de Recibo'),
      		  'tipo_vendedor'=> Yii::t('app', 'Seleccione en caso de ..'),
      	  	'id_vendedor'=> Yii::t('app', 'Vendedor(solo si aplica)'),
      	  	'id_automovil'=>Yii::t('app', 'Placa'),
      	  	'producto'=>Yii::t('app', 'Producto (necesario para mostrar las coberturas de la poliza)'),
            'deducible'=>Yii::t('app', 'Deducible'),
            'id_beneficiario'=>Yii::t('app', 'Beneficiario'),
            'tipo_recibo'=>Yii::t('app', 'Tipo Poliza'),
            'aseguradora'=>Yii::t('app', 'Aseguradora'),
            'identificacion'=>Yii::t('app', 'Identificación'),
            'nombre_completo'=>Yii::t('app', 'Tomador'),
            'estatus_poliza'=>Yii::t('app', 'Estatus Poliza'),
            'id_asegurado'=> Yii::t('app', 'Asegurado(a)'),
            'rcbo_fecha_vigenciadesde' => Yii::t('app', 'Fecha Vigencia Desde'),
            'rcbo_fecha_vigenciahasta' => Yii::t('app', 'Fecha Vigencia Hasta'),
            'isTitular' => 'Es el Tomador Titular Asegurado?',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAseguradora()
    {
        return $this->hasOne(Aseguradoras::className(), ['id_aseguradora' => 'id_aseguradora']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdContratante()
    {
        return $this->hasOne(Cliente::className(), ['id_cliente' => 'id_contratante']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProducto()
    {
        return $this->hasOne(Productos::className(), ['id_producto' => 'id_producto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecibos()
    {
        return $this->hasMany(Recibo::className(), ['id_poliza' => 'id_poliza']);
    }

	public static function datos_poliza($id)
    {
        return Poliza::find()
        ->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
        ->innerJoin('persona as cli', 'cli.id_persona = cliente.id_persona')
		->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza AND poliza.fecha_vigenciadesde >= recibo.fecha_vigenciadesde AND poliza.fecha_vigenciahasta <= recibo.fecha_vigenciahasta')
		->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo')
		->innerJoin('automovil', 'automovil.id_automovil= certificado.id_automovil')
		->leftJoin('version', 'version.id_version = automovil.id_version')
        ->leftJoin('modelos', 'modelos.id_modelo = automovil.id_modelo')
        ->leftJoin('marcas', 'marcas.id_marca = modelos.id_marca')
		->leftJoin('intermediario', 'intermediario.id_intermediario = recibo.id_vendedor')
		->leftJoin('persona as ven', 'ven.id_persona = intermediario.id_persona')
		->innerJoin('productos', 'productos.id_producto = certificado.id_producto')
        ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
		->where("poliza.id_poliza=".$id)
        ->select(["poliza.*","(cli.nombre ||' '||COALESCE(cli.segundo_nombre,'')) AS nombre_cli","(cli.apellido||' '||COALESCE(cli.segundo_apellido,'')) AS apellido_cli",
         "(ven.nombre||' '||COALESCE(ven.segundo_nombre,'')) AS nombre_ven","(ven.apellido||' '||COALESCE(ven.segundo_apellido,''))  AS apellido_ven",
				 "cli.identificacion,aseguradoras.nombre AS aseguradora,(poliza.fecha_vigenciadesde||'  -  '||poliza.fecha_vigenciahasta) AS fecha_vigenciahasta,placa,nro_recibo,
				 marcas.nombre AS marca,modelos.nombre AS modelo,marcas.id_marca,version.nombre AS version, version.anio,
				 aseguradoras.nombre AS aseguradora, productos.nombre AS producto,comision_total,comision_vendedor,codigo,
				 tipo_recibo,recibo.id_recibo"])->orderBy('poliza.fecha_vigenciadesde ASC')
        ->one();
    }

	public static function datos_polizaSalud($id)
    {
            return Poliza::find()
            ->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
            ->innerJoin('persona as cli', 'cli.id_persona = cliente.id_persona')
		    ->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza')
		    ->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo AND poliza.fecha_vigenciadesde >= recibo.fecha_vigenciadesde')
		    ->leftJoin('intermediario', 'intermediario.id_intermediario = recibo.id_vendedor')
		    ->leftJoin('persona as ven', 'ven.id_persona = intermediario.id_persona')
		    ->innerJoin('productos', 'productos.id_producto = certificado.id_producto')
            ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
		    ->where("poliza.id_poliza=".$id)
            ->select(["poliza.*","(cli.nombre ||' '||COALESCE(cli.segundo_nombre,' ')) AS nombre_cli","(cli.apellido||' '||COALESCE(cli.segundo_apellido,' ')) AS apellido_cli",
         "(ven.nombre||' '||COALESCE(ven.segundo_nombre,' ')) AS nombre_ven","(ven.apellido||' '||COALESCE(ven.segundo_apellido,' '))  AS apellido_ven",
         "cli.identificacion","aseguradoras.nombre AS aseguradora","poliza.fecha_vigenciadesde", "poliza.fecha_vigenciahasta","nro_recibo",
				 "aseguradoras.nombre AS aseguradora", "productos.nombre AS producto","comision_total","comision_vendedor","codigo","tipo_recibo","recibo.id_recibo"])
         ->one();
         
         //AND poliza.fecha_vigenciahasta <= recibo.fecha_vigenciahasta ESTABA EN LA LINEA 165
    }
	public static function coberturas($id)
    {
        return Poliza::find()
		     ->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza')
		     ->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo')
        ->innerJoin('certificado_detalle', 'certificado.id_certificado = certificado_detalle.id_certificado')
		    ->innerJoin('coberturas', 'coberturas.id_cobertura= certificado_detalle.id_cobertura')
		    ->where("recibo.id_recibo=".$id)
        ->select("certificado_detalle.*, coberturas.nombre AS coberturas");
    }
    public static function recibos($id)
    {
       /* return Poliza::find()
		     ->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza')
		    ->where("recibo.id_poliza=".$id)
        ->select("recibo.*")->orderBy('fecha_registro')
    ->all();*/

		$res=array();
        $connection = \Yii::$app->db;
        $sql = "SELECT recibo.*,to_char(recibo.fecha_vigenciadesde,'dd-mm-yyyy') AS desde FROM poliza
		INNER JOIN recibo ON recibo.id_poliza = poliza.id_poliza
		AND recibo.fecha_vigenciadesde >= poliza.fecha_vigenciadesde
		AND recibo.fecha_vigenciahasta <= poliza.fecha_vigenciahasta
		WHERE recibo.id_poliza=:id ORDER BY fecha_registro DESC";
        $command = $connection->createCommand($sql);
        $command->bindValue(":id", $id);
        $result = $command->queryAll();

        if (count($result)!= 0){
			$res=$result;

            return $res;
		}
    }

    public static function certificados($id)
    {
        return Poliza::find()
		    ->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza')
		    ->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo')
		    ->innerJoin('automovil', 'automovil.id_automovil= certificado.id_automovil')
		    ->leftJoin('version', 'version.id_version = automovil.id_version')
        ->leftJoin('modelos', 'modelos.id_modelo = automovil.id_modelo')
		    ->innerJoin('cliente', 'cliente.id_cliente = certificado.id_titular')
        ->innerJoin('persona as cli', 'cli.id_persona = cliente.id_persona')
        ->leftJoin('marcas', 'marcas.id_marca = modelos.id_marca')
		    ->where("recibo.id_recibo=".$id)
        ->select(["marcas.nombre AS marca","modelos.nombre AS modelo","marcas.id_marca","version.nombre AS version", "version.anio",
		        "(cli.nombre||' '||COALESCE(cli.segundo_nombre,' '))  AS nombre_cli","(cli.apellido||' '||COALESCE(cli.segundo_apellido,' '))  AS apellido_cli",
            "cli.identificacion","(SELECT SUM(prima) FROM certificado_detalle WHERE id_certificado=certificado.id_certificado) AS prima",
            "certificado.nro_certificado","automovil.placa"]);
    }

	public static function certificadosS($id)
    {
        return Poliza::find()
		    ->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza')
		    ->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo')
        ->innerJoin('certificado_asegurados', 'certificado_asegurados.id_certificado = certificado.id_certificado')
		    ->innerJoin('cliente', 'cliente.id_cliente = certificado_asegurados.id_cliente')
        ->innerJoin('persona as cli', 'cli.id_persona = cliente.id_persona')
		    ->where("recibo.id_recibo=".$id)
        ->select(["(cli.nombre||' '||COALESCE(cli.segundo_nombre,' ')) AS nombre_cli","(cli.apellido||' '||COALESCE(cli.segundo_apellido,' '))  AS apellido_cli",
        "cli.identificacion","(SELECT SUM(prima) FROM certificado_detalle WHERE id_certificado=certificado.id_certificado) AS prima",
        "certificado.nro_certificado","(CASE WHEN certificado_asegurados.maternidad=0 THEN 'NO' ELSE 'SI' END) AS maternidad"]);
    }


		public static function asegurados($id)
    {
        return Poliza::find()
		     ->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza')
		     ->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo')
         ->innerJoin('certificado_asegurados', 'certificado.id_certificado = certificado_asegurados.id_certificado')
			   ->innerJoin('cliente', 'cliente.id_cliente = certificado_asegurados.id_cliente')
          ->innerJoin('persona as cli', 'cli.id_persona = cliente.id_persona')
		     ->where("recibo.id_recibo=".$id)
        ->select(["certificado_asegurados.*","(cli.nombre ||' '||COALESCE(cli.segundo_nombre,'')) as nombre_cli",
                  "(cli.apellido||' '||COALESCE(cli.segundo_apellido,'')) as apellido_cli","cli.identificacion"]);
    }

    public static function arrayAsegurados($id)
    {
        return Poliza::find()
		       ->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza')
		       ->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo')
           ->innerJoin('certificado_asegurados', 'certificado.id_certificado = certificado_asegurados.id_certificado')
			     ->innerJoin('cliente', 'cliente.id_cliente = certificado_asegurados.id_cliente')
           ->leftJoin('carga_familiar', 'carga_familiar.id_beneficiario= cliente.id_cliente')
           ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
		       ->where("recibo.id_recibo=".$id)
           ->select(["certificado_asegurados.*",
		        "(nombre ||' '||COALESCE(segundo_nombre,'')) as nombres","(apellido|| ' ' ||COALESCE(segundo_apellido,'')) as apellidos",
            "identificacion","cliente.id_cliente AS id_beneficiario",
            "substring(age(now(),persona.fecha_nacimiento)::text from 1 for 2)::int AS anios",
            "(CASE WHEN carga_familiar.id_beneficiario IS NOT NULL THEN carga_familiar.parentesco ELSE 'n/a' END) AS parentesco"])->asArray()->all();
    }

    public static function arrayBeneficiarios($id)
      {
          return Poliza::find()
  		    ->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza')
  		     ->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo')
            ->innerJoin('certificado_beneficiarios', 'certificado.id_certificado = certificado_beneficiarios.id_certificado')
  			->innerJoin('cliente', 'cliente.id_cliente = certificado_beneficiarios.id_cliente')
            ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
  		      ->where("recibo.id_recibo=".$id)
            ->select(["certificado_beneficiarios.*",
  		               "(nombre ||' '||COALESCE(segundo_nombre,'')) as nombres","(apellido|| ' ' ||COALESCE(segundo_apellido,'')) as apellidos",
                     "identificacion","cliente.id_cliente"])->asArray()->all();
      }
      
      public static function arrayBienesasegurados($id)
      {
          
           
              return Poliza::find()
  		     ->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza')
  		     ->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo')
             ->leftJoin('certificado_bienasegurado', 'certificado_bienasegurado.id_certificado = certificado.id_certificado')
  			 ->innerJoin('direccion', 'direccion.id = certificado_bienasegurado.id_direccion')
  			 ->innerJoin('ciudad', 'ciudad.id_ciudad = direccion.id_ciudad')
  			 ->innerJoin('estado', 'estado.id_estado = ciudad.id_estado')
  			 ->leftJoin('zona', 'zona.id_zona = direccion.id_zona')
  		     ->where("recibo.id_recibo=".$id)
             ->select(["direccion.*","certificado_bienasegurado.id_direccion",
             "ciudad.nombre AS ciudad","estado.nombre AS estado","COALESCE(zona.nombre,'') as zona"])->asArray()->all();
             
             
      }
      
 
	public static function beneficiarios($id)
    {
        return Poliza::find()
		     ->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza')
		     ->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo')
             ->innerJoin('certificado_beneficiarios', 'certificado.id_certificado = certificado_beneficiarios.id_certificado')
			 ->innerJoin('cliente', 'cliente.id_cliente = certificado_beneficiarios.id_cliente')
             ->innerJoin('persona as cli', 'cli.id_persona = cliente.id_persona')
		     ->where("recibo.id_recibo=".$id)
        ->select(["certificado_beneficiarios.*",
		        "(cli.nombre||' '||COALESCE(cli.segundo_nombre,'')) AS nombre_cli",
            "(cli.apellido||' '||COALESCE(cli.segundo_apellido,'')) AS apellido_cli","cli.identificacion"]);
    }

    public static function datosRenovacionAuto($id)
      {
          return Poliza::find()
          ->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
          ->innerJoin('persona as cli', 'cli.id_persona = cliente.id_persona')
  		   ->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza')
  		   ->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo AND poliza.fecha_vigenciadesde >= recibo.fecha_vigenciadesde AND poliza.fecha_vigenciahasta <= recibo.fecha_vigenciahasta')
  		   ->innerJoin('automovil', 'automovil.id_automovil= certificado.id_automovil')
  		   ->leftJoin('intermediario', 'intermediario.id_intermediario = recibo.id_vendedor')
  		   ->leftJoin('persona as ven', 'ven.id_persona = intermediario.id_persona')
  		   ->innerJoin('productos', 'productos.id_producto = certificado.id_producto')
          ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
          ->innerJoin('certificado_detalle', 'certificado_detalle.id_certificado = certificado.id_certificado')
  		  ->where("poliza.id_poliza=".$id)
          ->select("poliza.id_poliza,poliza.id_poliza,
                    CAST(poliza.fecha_vigenciadesde AS DATE) + CAST('1 year' AS INTERVAL) AS fecha_vigenciadesde,
                    CAST(poliza.fecha_vigenciahasta AS DATE) + CAST('1 year' AS INTERVAL) AS fecha_vigenciahasta,
                    poliza.estatus,poliza.id_tipo,poliza.id_contratante,poliza.estatus_poliza,poliza.id_ramo,poliza.numero_poliza,
                    poliza.id_aseguradora,
                    ('Renovacion') AS tipo_recibo,
                    CAST(recibo.fecha_vigenciadesde AS DATE) + CAST('1 year' AS INTERVAL) AS rcbo_fecha_vigenciadesde,
                    CAST(recibo.fecha_vigenciahasta AS DATE) + CAST('1 year' AS INTERVAL) AS rcbo_fecha_vigenciahasta,
                    recibo.id_recibo,id_vendedor,certificado.deducible,id_vendedor AS id_vendedor_hidden,
                    certificado.id_producto,certificado.id_titular AS id_asegurado,certificado.nro_certificado,
                    automovil.id_automovil,intermediario.comision AS comision_porcentaje,
                    (CASE WHEN certificado_detalle.id_cobertura=5
                       THEN 0 ELSE 1 END) AS tipo,poliza.id_contratante AS id_contratante_hidden,
                       certificado.id_titular AS id_asegurado_hidden,automovil.id_automovil AS id_automovil_hidden,
                       recibo.id_subcontratante,recibo.id_vendedor")
          ->one();
      }

      public static function datosRenovacionSalud($id)
      {
            return Poliza::find()
            ->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
            ->innerJoin('persona as cli', 'cli.id_persona = cliente.id_persona')
    		    ->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza')
    		    ->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo AND poliza.fecha_vigenciadesde >= recibo.fecha_vigenciadesde AND poliza.fecha_vigenciahasta <= recibo.fecha_vigenciahasta')
    		    ->leftJoin('intermediario', 'intermediario.id_intermediario = recibo.id_vendedor')
    		    ->leftJoin('persona as ven', 'ven.id_persona = intermediario.id_persona')
    		    ->innerJoin('productos', 'productos.id_producto = certificado.id_producto')
            ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
            ->innerJoin('certificado_detalle', 'certificado_detalle.id_certificado = certificado.id_certificado')
    		->where("poliza.id_poliza=".$id)
            ->select("poliza.id_poliza,
                     CAST(poliza.fecha_vigenciadesde AS DATE) + CAST('1 year' AS INTERVAL) AS fecha_vigenciadesde,
                     CAST(poliza.fecha_vigenciahasta AS DATE) + CAST('1 year' AS INTERVAL) AS fecha_vigenciahasta,
                     poliza.estatus,poliza.id_tipo,poliza.id_contratante,poliza.estatus_poliza,poliza.id_ramo,poliza.numero_poliza,
                     poliza.id_aseguradora,('Renovacion') AS tipo_recibo,
                      recibo.fecha_vigenciadesde AS rcbo_fecha_vigenciadesde,
                      recibo.fecha_vigenciahasta AS rcbo_fecha_vigenciahasta,
                      recibo.id_recibo,id_vendedor,certificado.deducible,id_vendedor AS id_vendedor_hidden,
                      certificado.id_producto,certificado.nro_certificado,intermediario.comision AS comision_porcentaje,
                      (CASE WHEN certificado_detalle.id_cobertura=5
                         THEN 0 ELSE 1 END) AS tipo,poliza.id_contratante AS id_contratante_hidden,recibo.id_vendedor")->one();
        }


	  public static function datosRenovacionAutoF($id)
      {
          return Poliza::find()
          ->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
          ->innerJoin('persona as cli', 'cli.id_persona = cliente.id_persona')
  		    ->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza')
  		    ->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo AND poliza.fecha_vigenciadesde >= recibo.fecha_vigenciadesde AND poliza.fecha_vigenciahasta <= recibo.fecha_vigenciahasta')
  		    ->innerJoin('automovil', 'automovil.id_automovil= certificado.id_automovil')
  		    ->leftJoin('intermediario', 'intermediario.id_intermediario = recibo.id_vendedor')
  		    ->leftJoin('persona as ven', 'ven.id_persona = intermediario.id_persona')
  		    ->innerJoin('productos', 'productos.id_producto = certificado.id_producto')
          ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
          ->innerJoin('certificado_detalle', 'certificado_detalle.id_certificado = certificado.id_certificado')
  		  ->where("poliza.id_poliza=".$id)
          ->select("poliza.id_poliza,poliza.fecha_vigenciadesde,poliza.fecha_vigenciahasta,
					  poliza.id_tipo,poliza.id_contratante,poliza.fecha_registro,poliza.fecha_actualizacion,
					  poliza.id_user_registro,poliza.id_user_actualizacion,poliza.id_aseguradora,
					  poliza.estatus_poliza,poliza.id_ramo,poliza.numero_poliza,
                    recibo.fecha_vigenciadesde AS rcbo_fecha_vigenciadesde,
                    recibo.fecha_vigenciahasta AS rcbo_fecha_vigenciahasta,
                    recibo.id_recibo,id_vendedor,certificado.deducible,id_vendedor AS id_vendedor_hidden,
                    certificado.id_producto,certificado.id_titular AS id_asegurado,certificado.nro_certificado,
                    automovil.id_automovil,intermediario.comision AS comision_porcentaje,
                    (CASE WHEN certificado_detalle.id_cobertura=5
                       THEN 0 ELSE 1 END) AS tipo,poliza.id_contratante AS id_contratante_hidden,
                       certificado.id_titular AS id_asegurado_hidden,automovil.id_automovil AS id_automovil_hidden,
                       recibo.id_subcontratante,recibo.id_vendedor")->one();
      }

	  public static function datosRenovacionColectivo($id)
      {
          return Poliza::find()
          ->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
          ->innerJoin('persona as cli', 'cli.id_persona = cliente.id_persona')
  		  ->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza')
  		  ->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo AND poliza.fecha_vigenciadesde >= recibo.fecha_vigenciadesde AND poliza.fecha_vigenciahasta <= recibo.fecha_vigenciahasta')
  		  ->leftJoin('intermediario', 'intermediario.id_intermediario = recibo.id_vendedor')
  		  ->leftJoin('persona as ven', 'ven.id_persona = intermediario.id_persona')
  		  ->innerJoin('productos', 'productos.id_producto = certificado.id_producto')
          ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
          ->innerJoin('certificado_detalle', 'certificado_detalle.id_certificado = certificado.id_certificado')
  		  ->where("poliza.id_poliza=".$id)
          ->select("poliza.id_poliza,poliza.fecha_vigenciadesde,poliza.fecha_vigenciahasta,
					  poliza.id_tipo,poliza.id_contratante,poliza.fecha_registro,poliza.fecha_actualizacion,
					  poliza.id_user_registro,poliza.id_user_actualizacion,poliza.id_aseguradora,
					  poliza.estatus_poliza,poliza.id_ramo,poliza.numero_poliza,
                    recibo.fecha_vigenciadesde AS rcbo_fecha_vigenciadesde,
                    recibo.fecha_vigenciahasta AS rcbo_fecha_vigenciahasta,
                    recibo.id_recibo,id_vendedor,certificado.deducible,id_vendedor AS id_vendedor_hidden,
                    certificado.id_producto,certificado.id_titular AS id_asegurado,certificado.nro_certificado,
					intermediario.comision AS comision_porcentaje,
                    (CASE WHEN certificado_detalle.id_cobertura=5
                       THEN 0 ELSE 1 END) AS tipo,poliza.id_contratante AS id_contratante_hidden,
                       certificado.id_titular AS id_asegurado_hidden,
                       recibo.id_subcontratante,recibo.id_vendedor")->one();
      }
      
      public static function datosRenovacionPatrimonial($id)
      {
         
          return  $bien=Poliza::find()
            ->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
            ->innerJoin('persona as cli', 'cli.id_persona = cliente.id_persona')
		    ->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza')
		    ->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo AND poliza.fecha_vigenciadesde >= recibo.fecha_vigenciadesde AND poliza.fecha_vigenciahasta <= recibo.fecha_vigenciahasta')
		    ->leftJoin('intermediario', 'intermediario.id_intermediario = recibo.id_vendedor')
		    ->leftJoin('persona as ven', 'ven.id_persona = intermediario.id_persona')
		    ->innerJoin('productos', 'productos.id_producto = certificado.id_producto')
            ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
            ->innerJoin('certificado_detalle', 'certificado_detalle.id_certificado = certificado.id_certificado')
            ->innerJoin('certificado_asegurados', 'certificado_asegurados.id_certificado = certificado.id_certificado')
            ->leftJoin('certificado_bienasegurado', 'certificado_bienasegurado.id_certificado = certificado.id_certificado')
    		->where("poliza.id_poliza=".$id)
            ->select("poliza.id_poliza,
                    CAST(poliza.fecha_vigenciadesde AS DATE) + CAST('1 year' AS INTERVAL) AS fecha_vigenciadesde,
                    CAST(poliza.fecha_vigenciahasta AS DATE) + CAST('1 year' AS INTERVAL) AS fecha_vigenciahasta,
                    poliza.estatus,poliza.id_tipo,poliza.id_contratante,poliza.estatus_poliza,poliza.id_ramo,poliza.numero_poliza,
                    poliza.id_aseguradora,('Renovacion') AS tipo_recibo,
                    recibo.fecha_vigenciadesde AS rcbo_fecha_vigenciadesde,
                    recibo.fecha_vigenciahasta AS rcbo_fecha_vigenciahasta,
                    recibo.id_recibo,id_vendedor,certificado.deducible,id_vendedor AS id_vendedor_hidden,
                      certificado.id_producto,certificado.nro_certificado,intermediario.comision AS comision_porcentaje,
                      certificado_asegurados.id_cliente AS id_asegurado, certificado_asegurados.id_cliente AS id_asegurado_hidden,
                      certificado_bienasegurado.c_bienasegurado_indole AS indole,certificado_bienasegurado.c_bienasegurado_riesgo AS riesgo,
                      (CASE WHEN certificado_detalle.id_cobertura=5
                         THEN 0 ELSE 1 END) AS tipo,poliza.id_contratante AS id_contratante_hidden,recibo.id_vendedor")->one();
                         
                      
        }

}
