<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "excel_colectivo".
 *
 * @property string $certificado
 * @property string $tipo
 * @property string $cedula
 * @property string $primer_nombre
 * @property string $primer_apellido
 * @property string $fecha_nacimiento
 * @property string $estado
 * @property string $ciudad
 * @property string $direccion
 * @property string $deducible
 * @property string $cedula_t
 * @property string $parentesco
 * @property integer $id
 * @property string $producto
 * @property string $sexo
 * @property string $porcentaje_beneficiario
 * @property string $suma_asegurada
 * @property string $prima
 * @property integer $id_user
 * @property string $maternidad
 */
class ExcelColectivo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'excel_colectivo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha_nacimiento'], 'safe'],
            [['deducible', 'porcentaje_beneficiario', 'suma_asegurada', 'prima'], 'number'],
            [['id_user'], 'integer'],
            [['certificado', 'cedula', 'cedula_t'], 'string', 'max' => 10],
            [['tipo'], 'string', 'max' => 3],
            [['primer_nombre', 'primer_apellido', 'estado', 'ciudad'], 'string', 'max' => 50],
            [['direccion', 'producto'], 'string', 'max' => 100],
            [['parentesco'], 'string', 'max' => 20],
            [['sexo'], 'string', 'max' => 1],
            [['maternidad'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'certificado' => Yii::t('app', 'Certificado'),
            'tipo' => Yii::t('app', 'Tipo'),
            'cedula' => Yii::t('app', 'Cedula'),
            'primer_nombre' => Yii::t('app', 'Primer Nombre'),
            'primer_apellido' => Yii::t('app', 'Primer Apellido'),
            'fecha_nacimiento' => Yii::t('app', 'Fecha Nacimiento'),
            'estado' => Yii::t('app', 'Estado'),
            'ciudad' => Yii::t('app', 'Ciudad'),
            'direccion' => Yii::t('app', 'Direccion'),
            'deducible' => Yii::t('app', 'Deducible'),
            'cedula_t' => Yii::t('app', 'Cedula T'),
            'parentesco' => Yii::t('app', 'Parentesco'),
            'id' => Yii::t('app', 'ID'),
            'producto' => Yii::t('app', 'Producto'),
            'sexo' => Yii::t('app', 'Sexo'),
            'porcentaje_beneficiario' => Yii::t('app', 'Porcentaje Beneficiario'),
            'suma_asegurada' => Yii::t('app', 'Suma Asegurada'),
            'prima' => Yii::t('app', 'Prima'),
            'id_user' => Yii::t('app', 'Id User'),
            'maternidad' => Yii::t('app', 'Maternidad'),
        ];
    }
}
