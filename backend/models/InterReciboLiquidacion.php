<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "inter_recibo_liquidacion".
 *
 * @property integer $id_liquidacion_comisiones
 * @property integer $id_recibo
 *
 * @property LiquidacionComisiones $idLiquidacionComisiones
 * @property Recibo $idRecibo
 */
class InterReciboLiquidacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inter_recibo_liquidacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_liquidacion_comisiones', 'id_recibo'], 'integer'],
            [['id_liquidacion_comisiones'], 'exist', 'skipOnError' => true, 'targetClass' => LiquidacionComisiones::className(), 'targetAttribute' => ['id_liquidacion_comisiones' => 'id_liquidacion_comisiones']],
            [['id_recibo'], 'exist', 'skipOnError' => true, 'targetClass' => Recibo::className(), 'targetAttribute' => ['id_recibo' => 'id_recibo']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_liquidacion_comisiones' => Yii::t('app', 'Id Liquidacion Comisiones'),
            'id_recibo' => Yii::t('app', 'Id Recibo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLiquidacionComisiones()
    {
        return $this->hasOne(LiquidacionComisiones::className(), ['id_liquidacion_comisiones' => 'id_liquidacion_comisiones']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRecibo()
    {
        return $this->hasOne(Recibo::className(), ['id_recibo' => 'id_recibo']);
    }
}
