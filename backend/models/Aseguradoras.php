<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "aseguradoras".
 *
 * @property integer $id_aseguradora
 * @property string $nombre
 * @property string $direccion
 * @property string $telefono
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property integer $estatus
 * @property string $rif
 * @property integer $id_pais
 *
 * @property Pais $idPais
 * @property Convenio[] $convenios
 * @property Facturacion[] $facturacions
 * @property InterAsegProducto[] $interAsegProductos
 * @property InterAsegRamo[] $interAsegRamos
 * @property Poliza[] $polizas
 */
class Aseguradoras extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

      public $ramos;
    public static function tableName()
    {
        return 'aseguradoras';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['direccion'], 'string'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['id_user_registro', 'id_user_actualizacion', 'estatus', 'id_pais'], 'integer'],
            [['nombre'], 'string', 'max' => 200],
            [['telefono'], 'string', 'max' => 20],
            [['rif'], 'string', 'max' => 12],
            [['id_pais'], 'exist', 'skipOnError' => true, 'targetClass' => Pais::className(), 'targetAttribute' => ['id_pais' => 'id_pais']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_aseguradora' => Yii::t('app', 'Id Aseguradora'),
            'nombre' => Yii::t('app', 'Nombre'),
            'direccion' => Yii::t('app', 'Direccion'),
            'telefono' => Yii::t('app', 'Telefono'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'estatus' => Yii::t('app', 'Estatus'),
            'rif' => Yii::t('app', 'Rif'),
            'id_pais' => Yii::t('app', 'Id Pais'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPais()
    {
        return $this->hasOne(Pais::className(), ['id_pais' => 'id_pais']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConvenios()
    {
        return $this->hasMany(Convenio::className(), ['id_aseguradora' => 'id_aseguradora']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturacions()
    {
        return $this->hasMany(Facturacion::className(), ['id_aseguradora' => 'id_aseguradora']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterAsegProductos()
    {
        return $this->hasMany(InterAsegProducto::className(), ['id_aseguradora' => 'id_aseguradora']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterAsegRamos()
    {
        return $this->hasMany(InterAsegRamo::className(), ['id_aseguradora' => 'id_aseguradora']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolizas()
    {
        return $this->hasMany(Poliza::className(), ['id_aseguradora' => 'id_aseguradora']);
    }

    public function afterSave($insert, $changedAttributes){
 		\Yii::$app->db->createCommand()->delete('inter_aseg_ramo', 'id_aseguradora = '.(int) $this->id_aseguradora)->execute();

 		 if(!empty($this->ramos)){
 				foreach ($this->ramos as $id) {
 					$ro = new InterAsegRamo();
 					$ro->id_aseguradora = $this->id_aseguradora;
 					$ro->id_ramo = $id;
 					$ro->save();
 				}
 			}

 	}

  public function getRamos()
  {
    return $this->hasMany(Ramos::className(), ['id_ramo' => 'id_ramo'])
      ->viaTable('inter_aseg_ramo', ['id_aseguradora' => 'id_aseguradora']);
  }

  public function getRamosList()
  {
    return $this->getRamos()->asArray();
  }

}
