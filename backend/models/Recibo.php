<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "recibo".
 *
 * @property integer $id_recibo
 * @property integer $id_poliza
 * @property string $fecha_vigenciadesde
 * @property string $fecha_vigenciahasta
 * @property integer $estatus
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property integer $id_subcontratante
 * @property string $estatus_recibo
 * @property integer $id_vendedor
 * @property integer $id_productor
 * @property string $tipo_recibo
 * @property string $comision_total
 * @property string $comision_vendedor
 * @property string $nro_recibo
 *
 * @property Certificado[] $certificados
 * @property Poliza $idPoliza
 * @property Reclamos[] $reclamos
 */
class Recibo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public $ramo,$aseguradora,$numero_poliza,$nombre_completo;

    public static function tableName()
    {
        return 'recibo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_poliza', 'fecha_vigenciadesde', 'fecha_vigenciahasta', 'id_productor', 'tipo_recibo'], 'required'],
            [['id_poliza', 'estatus', 'id_user_registro', 'id_user_actualizacion', 'id_subcontratante', 'id_vendedor', 'id_productor','liquidado'], 'integer'],
            [['fecha_vigenciadesde', 'fecha_vigenciahasta','fecha_exclusion', 'fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['comision_total', 'comision_vendedor','devolucion_total'], 'number'],
            [['ramo','aseguradora','numero_poliza','nombre_completo'],'string'],
            [['estatus_recibo'], 'string', 'max' => 100],
            [['tipo_recibo'], 'string', 'max' => 50],
            [['nro_recibo'], 'string', 'max' => 25]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_recibo' => Yii::t('app', 'Id Recibo'),
            'id_poliza' => Yii::t('app', 'Id Poliza'),
            'fecha_vigenciadesde' => Yii::t('app', 'Fecha Vigenciadesde'),
            'fecha_vigenciahasta' => Yii::t('app', 'Fecha Vigenciahasta'),
            'estatus' => Yii::t('app', 'Estatus'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'id_subcontratante' => Yii::t('app', 'Id Subcontratante'),
            'estatus_recibo' => Yii::t('app', 'Estatus Recibo'),
            'id_vendedor' => Yii::t('app', 'Id Vendedor'),
            'id_productor' => Yii::t('app', 'Id Productor'),
            'tipo_recibo' => Yii::t('app', 'Tipo Recibo'),
            'comision_total' => Yii::t('app', 'Comision Total'),
            'comision_vendedor' => Yii::t('app', 'Comision Vendedor'),
            'nro_recibo' => Yii::t('app', 'Nro Recibo'),
			'fecha_exclusion' => Yii::t('app', 'Fecha Exclusion'),
			'devolucion_total' => Yii::t('app', 'Total devolucion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCertificados()
    {
        return $this->hasMany(Certificado::className(), ['id_recibo' => 'id_recibo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPoliza()
    {
        return $this->hasOne(Poliza::className(), ['id_poliza' => 'id_poliza']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReclamos()
    {
        return $this->hasMany(Reclamos::className(), ['id_recibo' => 'id_recibo']);
    }

    public static function findReciboPrima($id)
     {
       $connection = \Yii::$app->db;
       $sql = "select SUM(cd.prima) AS prima_total
                   FROM recibo
                   INNER JOIN certificado USING (id_recibo)
                   INNER JOIN certificado_detalle AS cd USING (id_certificado)
                WHERE recibo.id_recibo=:id
                AND recibo.estatus=1
                AND certificado.estatus=1
                AND cd.estatus=1";

       $command = $connection->createCommand($sql);
       $command->bindValue(":id", $id);
       $result=$command->queryOne();
       return $result['prima_total'];
     }
}
