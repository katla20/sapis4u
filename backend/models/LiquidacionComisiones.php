<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "liquidacion_comisiones".
 *
 * @property integer $id_liquidacion_comisiones
 * @property integer $id_intermediario
 * @property string $comentario
 * @property string $fecha_registro
 * @property string $fecha_actualizacion
 * @property integer $id_user_registro
 * @property integer $id_user_actualizacion
 * @property integer $estatus
 * @property string $monto
 *
 * @property InterReciboLiquidacion[] $interReciboLiquidacions
 * @property Intermediario $idIntermidiario
 */
class LiquidacionComisiones extends \yii\db\ActiveRecord
{
	 public $recibos2,$recibos_numero,$vendedor,$tipo,$nombre_completo,$identificacion; 
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'liquidacion_comisiones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_intermediario', 'comentario', 'monto'], 'required'],
            [['id_intermediario', 'id_user_registro', 'id_user_actualizacion','estatus'], 'integer'],
            [['comentario','recibos'], 'string'],
            [['fecha_registro', 'fecha_actualizacion'], 'safe'],
            [['monto'], 'number'],
            [['id_intermediario'], 'exist', 'skipOnError' => true, 'targetClass' => Intermediario::className(), 'targetAttribute' => ['id_intermediario' => 'id_intermediario']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_liquidacion_comisiones' => Yii::t('app', 'Id Liquidacion Comisiones'),
            'id_intermediario' => Yii::t('app', 'Intermediario'),
            'comentario' => Yii::t('app', 'Descripcion'),
            'fecha_registro' => Yii::t('app', 'Fecha Liquidacion'),
            'fecha_actualizacion' => Yii::t('app', 'Fecha Actualizacion'),
            'id_user_registro' => Yii::t('app', 'Id User Registro'),
            'id_user_actualizacion' => Yii::t('app', 'Id User Actualizacion'),
            'monto' => Yii::t('app', 'Monto'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterReciboLiquidacions()
    {
        return $this->hasMany(InterReciboLiquidacion::className(), ['id_liquidacion_comisiones' => 'id_liquidacion_comisiones']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIntermidiario()
    {
        return $this->hasOne(Intermediario::className(), ['id_intermediario' => 'id_intermediario']);
    }
	
	public function afterSave($insert, $changedAttributes)
    {
		if(empty($this->recibos2)){
			\Yii::$app->db->createCommand()->delete('inter_recibo_liquidacion', 'id_liquidacion_comisiones = '.(int) $this->id_liquidacion_comisiones)->execute();
		}

 		if($this->tipo==1){
			 if(!empty($this->recibos2)){
					foreach ($this->recibos2 as $id) {
						$ro = new InterReciboLiquidacion();
						  $re = Recibo::findOne($id);
									$ro->id_liquidacion_comisiones = $this->id_liquidacion_comisiones;
									$ro->id_recibo = $id;
									$ro->save();
						  $re->liquidado=1;
						  $re->save();

					}
				}
		}else{
			
			if(!empty($this->recibos2)){
					foreach ($this->recibos2 as $id) {
						  $re = Recibo::findOne($id);
						  $re->liquidado=0;
						  $re->save();

					}
				}
		}	

 	}

     public function getRecibos()
     	{
     		return $this->hasMany(Recibo::className(), ['id_recibo' => 'id_recibo'])
     			->viaTable('inter_recibo_liquidacion', ['id_liquidacion_comisiones' => 'id_liquidacion_comisiones']);
     	}

     	public function getRecibosList()
     	{
     		return $this->getRecibos()->asArray();
     	}
		
	public static function datosLiquidacion($id)
      {
            return LiquidacionComisiones::find()
            ->innerJoin('intermediario', 'intermediario.id_intermediario = liquidacion_comisiones.id_intermediario')
    		    ->leftJoin('persona', 'persona.id_persona = intermediario.id_persona')
    		    ->where("id_liquidacion_comisiones=".$id)
            ->select("liquidacion_comisiones.*,(nombre||' '||apellido) AS nombre_completo, identificacion")->one();
        }
	public static function datosRecibos($id)
      {
            return LiquidacionComisiones::find()
            ->innerJoin('inter_recibo_liquidacion', 'inter_recibo_liquidacion.id_liquidacion_comisiones = liquidacion_comisiones.id_liquidacion_comisiones')
    		->innerJoin('recibo', 'recibo.id_recibo = inter_recibo_liquidacion.id_recibo')
			->innerJoin('poliza', 'poliza.id_poliza = recibo.id_poliza')
			->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
			->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
            ->innerJoin('persona as cli', 'cli.id_persona = cliente.id_persona')
    		    ->where("liquidacion_comisiones.id_liquidacion_comisiones=".$id)
            ->select("comision_vendedor,nro_recibo,aseguradoras.nombre as aseguradora,poliza.numero_poliza,(cli.nombre||' '||cli.apellido) AS nombre_completo")->orderBy('aseguradoras.nombre');
        }	
		
}
