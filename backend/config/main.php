<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

//http://stackoverflow.com/questions/28118691/yii2-htaccess-how-to-hide-frontend-web-and-backend-web-completely

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    //'bootstrap' => ['log'],
    'bootstrap' => ['debug'],
    'modules' => [
        'debug' => [
            'class' => 'yii\debug\Module',
        ],
        'gii' =>[
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['*'],
        ],
    ],
	'as beforeRequest' => [
    'class' => 'yii\filters\AccessControl',
    'rules' => [
        [
            'allow' => true,
            'actions' => ['login'],
        ],
        [
            'allow' => true,
            'roles' => ['@'],
        ],
    ],
    'denyCallback' => function () {
        return Yii::$app->response->redirect(['site/login']);
    },
    ],
    'components' => [
      'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
			'enableSession' => true,
            'authTimeout' =>3600,
			'loginUrl' => ['site/login'],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
		'request'=>[
			'class' => 'common\components\Request',
			'web'=> '/backend/web',
			'adminUrl' => '/admin'
		],
		'urlManager' => [
				'enablePrettyUrl' => true,
				'showScriptName' => false,
		],
		'session' => [
			'name' => 'BACKENDSESSID',
            'savePath' => __DIR__ . '/../runtime', // a temporary folder on backend
			/*'cookieParams' => [
				//'httpOnly' => true,
				'path'     => '/backend/web',
			],*/
		],
    ],
    'params' => $params,
];
