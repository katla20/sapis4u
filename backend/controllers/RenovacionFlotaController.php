<?php

namespace backend\controllers;

use Yii;
use backend\models\Poliza;
use backend\models\search\PolizaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\base\Model;
use yii\web\Response;
use yii\widgets\Pjax;
use backend\models\Cliente;
use backend\models\Intermediario;
use backend\models\Persona;
use backend\models\Coberturas;
use backend\models\CoberturasView;
use backend\models\Aseguradoras;
use backend\models\InterAsegRamo;
use backend\models\InterAsegProducto;
use backend\models\Productos;
use backend\models\Recibo;
use backend\models\Certificado;
use backend\models\CertificadoDetalle;
use backend\models\InterProdCob;
use backend\models\Automovil;
use backend\models\Version;
use backend\models\Modelo;
use backend\models\Marca;
use yii\data\ActiveDataProvider;
use backend\models\Documentos;
use backend\models\Excel;
use arogachev\excel\import\basic\Importer;



//https://www.youtube.com/watch?v=4NivIbhq3N8
//http://stackoverflow.com/questions/30677927/yii2-how-to-validate-a-form-with-ajax-and-then-submit-the-data-using-ajax-again


/**
 * PolizaController implements the CRUD actions for Poliza model.
 */
class RenovacionFlotaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Poliza models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PolizaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,1);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
             'model' => $this->datos($id),
        ]);
    }

    /**
     * Creates a new Poliza model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     
public function actionImportExcel($recibo,$id_aseguradora)
{

    $usuario=Yii::$app->user->identity->id;
    $fecha_registro=date('Y-m-d').'-'.date('H');
    $inputFile=Yii::$app->basePath.'/uploads/excel/'.$usuario.'-'.$fecha_registro.'.xlsx';

    try{
        $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
        $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFile);

    } catch (Exception $e) {
        die('Error');
    }

    $sheet = $objPHPExcel->getSheet(0);
    $highestRow = $sheet->getHighestRow();
    $highestColumn = $sheet->getHighestColumn();

    for($row=1; $row <= $highestRow; $row++)
    {
        $rowData = $sheet->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);

        if($row==1)
        {
            continue;
        }
         
         
        
         
       /*  if($row==3)
        {
             print_r($rowData); exit();
        }*/
         
        
          $excel= new Excel();
          $excel->certificado=$rowData[0][0];
          $excel->tipo=$rowData[0][1];
          $excel->cedula=$rowData[0][2];
          $excel->primer_nombre=$rowData[0][3];
          $excel->primer_apellido=$rowData[0][4];
          $excel->fecha_nacimiento=$rowData[0][5];
          $excel->estado=$rowData[0][6];
          $excel->ciudad=$rowData[0][7];
          $excel->direccion=$rowData[0][8];
          $excel->producto=$rowData[0][9];
          $excel->placa=$rowData[0][10];
          $excel->marca=$rowData[0][11];
          $excel->modelo=$rowData[0][12];
          $excel->color=$rowData[0][13];
          $excel->anio=$rowData[0][14];
          $excel->version=$rowData[0][15];
          $excel->tipo_au=$rowData[0][16];
		  $excel->uso_au=$rowData[0][17];
          $excel->carroceria=$rowData[0][18];
          $excel->suma_asegurada=$rowData[0][19];
          $excel->prima=$rowData[0][20];
          $excel->deducible=$rowData[0][21];
		  $excel->id_user=$usuario;
          if($rowData[0][2]!= ""){
            $excel->save();
            if($bandera=$excel->save(false)){
                 echo $rowData[0][1]."llegue";
                }else{
                    
                    echo "fallo";
                }
         
         
         
            }
         $placa=$rowData[0][9];
        //print_r($excel->getErrors());
    }

 
        $result=array();
        $connection = \Yii::$app->db;
        echo $sql = "SELECT carga_masiva('excel','$recibo','$usuario','$id_aseguradora')"; //exit;
        $command = $connection->createCommand($sql);

        $result = $command->queryAll();

        if ($result){
			return true;
			//
        }else return false;//die('fallo');
    


    
}
     
    public function actionUpdate($id)
    {

        

        $post=Yii::$app->request->post();

		$model= Poliza::datosRenovacionAutoF($id);
        $documentos= new Documentos();

        $recibo = new Recibo();
        $certificado= new Certificado();
        $certificadoDetalle= new CertificadoDetalle();
        $poliza = Poliza::findOne($id);

        //var_dump($model); exit();


         // ajax validation
        /* if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
               Yii::$app->response->format = Response::FORMAT_JSON;
                 return ActiveForm::validate($model);
          }*/

        $post=Yii::$app->request->post();
		
		if ($model->load(Yii::$app->request->post())){//validar post

            $model->id_contratante=$model->id_contratante_hidden;
            $model->id_tipo=2;
            $model->fecha_actualizacion=date('Y-m-d H:i:s');
            $model->id_user_actualizacion=isset(Yii::$app->user->identity->id)?Yii::$app->user->identity->id:1;
			      $model->id_ramo=1;
			
			$transaction = \Yii::$app->db->beginTransaction();
               try {

					  if($bandera=$model->save(false)){//validar que guarde la poliza

					   //exit(var_dump($model));
						$recibo->nro_recibo=$model->nro_recibo;
						$recibo->id_poliza=$model->id_poliza;
						$recibo->tipo_recibo=$model->tipo_recibo;
						$recibo->fecha_registro=$model->fecha_registro;
						$recibo->id_user_registro=$model->id_user_registro;
						$recibo->fecha_vigenciadesde=$post['Recibo']['fecha_vigenciadesde'];
						$recibo->fecha_vigenciahasta=$post['Recibo']['fecha_vigenciahasta'];
						$recibo->id_subcontratante=$model->id_subcontratante;
						$recibo->id_vendedor=$post['Poliza']['id_vendedor'];
						$recibo->id_productor=2;//OJO LLENARLO CON EL ID DEL USUARIO DEL ASESOR
							  $recibo->comision_total=$post['Poliza']['comision_total'];
							  $recibo->comision_vendedor=$post['Poliza']['comision_vendedor'];

						if($bandera=$recibo->save(false)){//validar que guarde el recibo

						  //extrayendo los datos del excel

						$bandera=$this->actionImportExcel($recibo->id_recibo,$model->id_aseguradora);
                        
						if($bandera){
								
							$transaction->commit();
							
							die('okay');
						}else{
							die('fallo');
						}
						

						}else{

						  var_dump($recibo->getErrors());

						}


					  }
					} catch (Exception $e) {
							  $transaction->rollBack();
						   }  


        } else {
             // var_dump($recibo->getErrors());
            return $this->render('update', [
                'model' => $model,
                'documentos' => $documentos,
                'recibo' => $recibo,
            ]);
        }

    }
    
    public function actionInclusion($id)
    {

        

        $post=Yii::$app->request->post();

		$model= Poliza::datosRenovacionAutoF($id);
        $documentos= new Documentos();

        $recibo = new Recibo();
        $certificado= new Certificado();
        $certificadoDetalle= new CertificadoDetalle();
        $poliza = Poliza::findOne($id);

        //var_dump($model); exit();


         // ajax validation
        /* if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
               Yii::$app->response->format = Response::FORMAT_JSON;
                 return ActiveForm::validate($model);
          }*/

        $post=Yii::$app->request->post();

        if ($model->load(Yii::$app->request->post())){//validar post

          //if($bandera=$model->save(false)){//validar que guarde la poliza
            $transaction = \Yii::$app->db->beginTransaction();
               try {
           //exit(var_dump($model));
            $recibo->nro_recibo=$model->nro_recibo;
            $recibo->id_poliza=$model->id_poliza;
            $recibo->tipo_recibo='Inclusion';
            $recibo->fecha_registro=$model->fecha_registro;
            $recibo->id_user_registro=$model->id_user_registro;
            $recibo->fecha_vigenciadesde=$post['Recibo']['fecha_vigenciadesde'];
            $recibo->fecha_vigenciahasta=$post['Recibo']['fecha_vigenciahasta'];
             $recibo->id_subcontratante=$model->id_subcontratante;
            $recibo->id_vendedor=$post['Poliza']['id_vendedor'];
            $recibo->id_productor=2;//OJO LLENARLO CON EL ID DEL USUARIO DEL ASESOR
			      $recibo->comision_total=$post['Poliza']['comision_total'];
			      $recibo->comision_vendedor=$post['Poliza']['comision_vendedor'];

            if($bandera=$recibo->save(false)){//validar que guarde el recibo

              //extrayendo los datos del excel
				$bandera=$this->actionImportExcel($recibo->id_recibo,$model->id_aseguradora);
                        
						if($bandera){
								
							$transaction->commit();
							
							die('okay');
						}else{
							die('fallo');
						}
						

						}else{

						  var_dump($recibo->getErrors());

						}
						
					} catch (Exception $e) {
							  $transaction->rollBack();
						   }  


        } else {
             // var_dump($recibo->getErrors());
            return $this->render('inclusion', [
                'model' => $model,
                'documentos' => $documentos,
                'recibo' => $recibo,
            ]);
        }

    }
    
    public function actionExclusion($id)
    {

        

        $post=Yii::$app->request->post();

		$model= Poliza::datosRenovacionAutoF($id);
        $documentos= new Documentos();

        $recibo = new Recibo();
        $certificado= new Certificado();
        $certificadoDetalle= new CertificadoDetalle();
        $poliza = Poliza::findOne($id);

        //var_dump($model); exit();


         // ajax validation
        /* if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
               Yii::$app->response->format = Response::FORMAT_JSON;
                 return ActiveForm::validate($model);
          }*/

        $post=Yii::$app->request->post();
		//echo $post['Recibo']['devolucion_total'];
		//var_dump($post); exit();

        if ($model->load(Yii::$app->request->post())){//validar post

          //if($bandera=$model->save(false)){//validar que guarde la poliza
         $transaction = \Yii::$app->db->beginTransaction();
               try {
           //exit(var_dump($model));
            $recibo->nro_recibo=$model->nro_recibo;
            $recibo->id_poliza=$model->id_poliza;
            $recibo->tipo_recibo='Exclusion';
            $recibo->fecha_registro=$model->fecha_registro;
            $recibo->id_user_registro=$model->id_user_registro;
            $recibo->fecha_exclusion=$post['Recibo']['fecha_exclusion'];
			$recibo->fecha_vigenciadesde=$post['Recibo']['fecha_exclusion'];
            $recibo->fecha_vigenciahasta=$post['Recibo']['fecha_exclusion'];
            $recibo->id_subcontratante=$model->id_subcontratante;
            $recibo->id_vendedor=$post['Poliza']['id_vendedor'];
            $recibo->id_productor=2;//OJO LLENARLO CON EL ID DEL USUARIO DEL ASESOR
			$recibo->comision_total=0;
			$recibo->comision_vendedor=$post['Poliza']['comision_vendedor'];
			$recibo->devolucion_total=$post['Recibo']['devolucion_total'];

             if($bandera=$recibo->save(false)){//validar que guarde el recibo

            $bandera=$this->actionImportExcel($recibo->id_recibo,$model->id_aseguradora);
                        
						if($bandera){
								
							$transaction->commit();
							
							die('okay');
						}else{
							die('fallo');
						}
						

						}else{

						  var_dump($recibo->getErrors());

						}
						
					} catch (Exception $e) {
							  $transaction->rollBack();
						   }  


        } else {
             // var_dump($recibo->getErrors());
            return $this->render('exclusion', [
                'model' => $model,
                'documentos' => $documentos,
                'recibo' => $recibo,
            ]);
        }

    }

    public function actionBuscarCoberturas()
    {



		   $filtroaux="AND coberturas.id_cobertura <> 5";
		   if(Yii::$app->request->post('tipo')==0){
		   $filtroaux="AND coberturas.id_cobertura = 5";
		  }


				$QUERY="SELECT
						id_cobertura,
						cobertura,
						sum(prima) As prima,
						sum(suma_asegurada) AS suma_asegurada,
						sum(checado) AS checado
						FROM (
							Select DISTINCT coberturas.id_cobertura,coberturas.nombre AS cobertura, 0 as prima,0 AS suma_asegurada,0 AS checado
              from recibo
							INNER JOIN certificado ON certificado.id_recibo = recibo.id_recibo
							INNER JOIN productos ON productos.id_producto = certificado.id_producto
							INNER JOIN inter_prod_cob ON inter_prod_cob.id_producto= certificado.id_producto
							INNER JOIN coberturas ON coberturas.id_cobertura = inter_prod_cob.id_cobertura
							WHERE recibo.id_recibo IN (:id) $filtroaux

							UNION

							Select DISTINCT coberturas.id_cobertura,coberturas.nombre AS cobertura, certificado_detalle.prima,certificado_detalle.suma_asegurada,
							(CASE WHEN (certificado_detalle.prima IS NOT NULL AND certificado_detalle.suma_asegurada IS NOT NULL)
						              THEN 1 ELSE 0
						        END) AS checado
							FROM recibo
							INNER JOIN certificado ON certificado.id_recibo = recibo.id_recibo
							INNER JOIN certificado_detalle ON certificado_detalle.id_certificado = certificado.id_certificado
							INNER JOIN coberturas ON coberturas.id_cobertura = certificado_detalle.id_cobertura
							WHERE recibo.id_recibo IN (:id) $filtroaux
							) AS cob GROUP BY  1,2;";


				$connection = \Yii::$app->db;
				$command = $connection->createCommand($QUERY);

				$command->bindValue(":id", Yii::$app->request->post('id_recibo'));
				Yii::$app->response->format = Response::FORMAT_JSON;
				return $result = $command->queryAll();

    }

    public function actionBuscarCliente()
    {

          Yii::$app->response->format = Response::FORMAT_JSON;
            return Cliente::find()->joinWith(['idPersona'])
           ->select(["*","CONCAT(nombre, ' ', apellido) AS nombre_completo"])
           ->where("cliente.estatus = :status AND cliente.id_cliente = :id_cliente",
           [':status' => 1,':id_cliente' => Yii::$app->request->post('id')])->asArray()->all();


    }

    public function actionBuscarAuto()
    {
        if(!empty(Yii::$app->request->post('id'))){
          Yii::$app->response->format = Response::FORMAT_JSON;
            return Automovil::datos_vehiculo(Yii::$app->request->post('id'));
        }

    }

    public function actionBuscarCliente2()
    {
         if(!empty(Yii::$app->request->post('id'))){
         Yii::$app->response->format = Response::FORMAT_JSON;
                            return Cliente::find()
                                       ->joinwith('idPersona')
                                        ->where("cliente.estatus = :status AND Cliente.id_cliente = :id_cliente",
                                        [':status' => 1,':id_cliente' => Yii::$app->request->post('id')])
                                        ->select("(nombre||' '||segundo_nombre||' '||apellido|| ' ' ||segundo_apellido || ' ' || identificacion) AS nombre_completo ,id_cliente")
                                        ->asArray()->all();
        }
    }

    public function actionBuscarVendedor()
    {
          if(!empty(Yii::$app->request->post('id'))){
            Yii::$app->response->format = Response::FORMAT_JSON;
                    return Intermediario::find()
                                 ->joinwith('idPersona')
                                 ->where("intermediario.estatus = :status AND intermediario.id_intermediario = :intermediario AND intermediario.id_tipo_intermediario=2",
                                 [':status' => 1,':intermediario' => Yii::$app->request->post('id')])
                                 //->select("(nombre||' '||segundo_nombre||' '||apellido|| ' ' ||segundo_apellido) AS nombre_completo,id_intermediario,codigo,comision")
                                 ->select(['*',"CONCAT(nombre, ' ', apellido) AS nombre_completo"])->asArray()->all();
          }
    }


    protected function findModel($id)
    {
        if (($model = Poliza::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	protected function datos($id)
    {
        if (($model = Poliza::datos_poliza($id)) !== null) {
            return $model;
        } else {

            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
