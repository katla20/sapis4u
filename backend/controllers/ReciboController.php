<?php

namespace backend\controllers;

use Yii;
use backend\models\Poliza;
use backend\models\search\PolizaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\base\Model;
use yii\web\Response;
use yii\widgets\Pjax;
use backend\models\Cliente;
use backend\models\Intermediario;
use backend\models\Persona;
use backend\models\Coberturas;
use backend\models\CoberturasView;
use backend\models\Aseguradoras;
use backend\models\InterAsegRamo;
use backend\models\InterAsegProducto;
use backend\models\Productos;
use backend\models\Recibo;
use backend\models\Certificado;
use backend\models\CertificadoDetalle;
use backend\models\InterProdCob;
use backend\models\Automovil;
use backend\models\Version;
use backend\models\Modelo;
use backend\models\Marca;
use yii\data\ActiveDataProvider;



//https://www.youtube.com/watch?v=4NivIbhq3N8
//http://stackoverflow.com/questions/30677927/yii2-how-to-validate-a-form-with-ajax-and-then-submit-the-data-using-ajax-again


/**
 * PolizaController implements the CRUD actions for Poliza model.
 */
class ReciboController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Poliza models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PolizaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,1);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
             'model' => $this->datos($id),
        ]);
    }

    /**
     * Creates a new Poliza model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpdate($id)
    {



        $post=Yii::$app->request->post();

		$model= Poliza::datosRenovacionAuto($id);

        $recibo = new Recibo();
        $certificado= new Certificado();
        $certificadoDetalle= new CertificadoDetalle();
        $poliza = Poliza::findOne($id);

        //var_dump($post);


         // ajax validation
        /* if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
               Yii::$app->response->format = Response::FORMAT_JSON;
                 return ActiveForm::validate($model);
          }*/

        if ($model->load(Yii::$app->request->post())){//validar post



             $poliza->fecha_vigenciadesde=$model->fecha_vigenciadesde;
             $poliza->fecha_vigenciahasta=$model->fecha_vigenciadesde;
             $poliza->id_contratante=$model->id_contratante_hidden;
             $poliza->id_user_actualizacion=Yii::$app->user->identity->id;
             $poliza->fecha_actualizacion=date('Y-m-d H:i:s');

           if($bandera=$poliza->save(false)){//validar que actualice la poliza

               $recibo->nro_recibo=$model->nro_recibo;
               $recibo->id_poliza=$model->id_poliza;
               $recibo->tipo_recibo=$model->tipo_recibo;
               $recibo->fecha_registro=date('Y-m-d H:i:s');
               $recibo->id_user_registro=Yii::$app->user->identity->id;
               $recibo->fecha_vigenciadesde=$model->fecha_vigenciadesde;
               $recibo->fecha_vigenciahasta=$model->fecha_vigenciahasta;
               $recibo->id_vendedor=$post['Poliza']['id_vendedor'];
               $recibo->id_productor=2;//OJO LLENARLO CON EL ID DEL USUARIO DEL ASESOR MASTER
   		       $recibo->comision_total=$post['Poliza']['comision_total'];
   		       $recibo->comision_vendedor=$post['Poliza']['comision_vendedor'];

             if($bandera=$recibo->save(false)){//validar que guarde el recibo

                   //validar certificados
                   $certificado->id_recibo=$recibo->id_recibo;
                   $certificado->nro_certificado=$model->nro_certificado;
                   $certificado->id_automovil=$post['Poliza']['id_automovil_hidden'];
                   $certificado->fecha_registro=$model->fecha_registro;
                   $certificado->id_user_registro=$model->id_user_registro;
                   $certificado->id_producto=$model->producto;
                   $certificado->id_titular=$post['Poliza']['id_asegurado_hidden'];
 				   $certificado->deducible=$post['Poliza']['deducible'];

                   if($bandera=$certificado->save(false)){

           					  $suma=Yii::$app->request->post('suma');
           					  $prima=Yii::$app->request->post('prima');

               					  foreach ($suma as $clave => $valor){

             										$certificadoDetalle= new CertificadoDetalle();
             										$certificadoDetalle->id_certificado=$certificado->id_certificado;
             										$certificadoDetalle->suma_asegurada=str_replace(",", ".",str_replace(".", "",$suma[$clave]));
             										$certificadoDetalle->prima=str_replace(",", ".",str_replace(".", "",$prima[$clave]));
             										$certificadoDetalle->comision=0;
             										$certificadoDetalle->fecha_registro=$model->fecha_registro;
             										$certificadoDetalle->id_user_registro=$model->id_user_registro;
             										$certificadoDetalle->id_cobertura=$clave;

                 							if($bandera=$certificadoDetalle->save(false)){
                 								var_dump($certificadoDetalle->getErrors());
                 							}
               					  }


           					  if($certificadoDetalle){
           					       return $this->redirect(['view', 'id' => $model->id_poliza]);
           					  }else{
           						       var_dump($recibo->getErrors());
           					  }

                   }else{
                     var_dump($certificado->getErrors());
                   }

             }else{
               var_dump($recibo->getErrors());
             }

           }else{
             var_dump($poliza->getErrors());
           }

        } else {
             // var_dump($recibo->getErrors());
            return $this->render('update', [
                'model' => $model,
            ]);
        }

    }

    public function actionBuscarCoberturas()
    {



		   $filtroaux="AND coberturas.id_cobertura <> 5";
		   if(Yii::$app->request->post('tipo')==0){
		   $filtroaux="AND coberturas.id_cobertura = 5";
		  }


				$QUERY="SELECT
						id_cobertura,
						cobertura,
						sum(prima) As prima,
						sum(suma_asegurada) AS suma_asegurada,
						sum(checado) AS checado
						FROM (
							Select DISTINCT coberturas.id_cobertura,coberturas.nombre AS cobertura, 0 as prima,0 AS suma_asegurada,0 AS checado
              from recibo
							INNER JOIN certificado ON certificado.id_recibo = recibo.id_recibo
							INNER JOIN productos ON productos.id_producto = certificado.id_producto
							INNER JOIN inter_prod_cob ON inter_prod_cob.id_producto= certificado.id_producto
							INNER JOIN coberturas ON coberturas.id_cobertura = inter_prod_cob.id_cobertura
							WHERE recibo.id_recibo IN (:id) $filtroaux

							UNION

							Select DISTINCT coberturas.id_cobertura,coberturas.nombre AS cobertura, certificado_detalle.prima,certificado_detalle.suma_asegurada,
							(CASE WHEN (certificado_detalle.prima IS NOT NULL AND certificado_detalle.suma_asegurada IS NOT NULL)
						              THEN 1 ELSE 0
						        END) AS checado
							FROM recibo
							INNER JOIN certificado ON certificado.id_recibo = recibo.id_recibo
							INNER JOIN certificado_detalle ON certificado_detalle.id_certificado = certificado.id_certificado
							INNER JOIN coberturas ON coberturas.id_cobertura = certificado_detalle.id_cobertura
							WHERE recibo.id_recibo IN (:id) $filtroaux
							) AS cob GROUP BY  1,2;";


				$connection = \Yii::$app->db;
				$command = $connection->createCommand($QUERY);

				$command->bindValue(":id", Yii::$app->request->post('id_recibo'));
				Yii::$app->response->format = Response::FORMAT_JSON;
				return $result = $command->queryAll();

    }

    public function actionBuscarCliente()
    {

          Yii::$app->response->format = Response::FORMAT_JSON;
            return Cliente::find()->joinWith(['idPersona'])
           ->select(["*","CONCAT(nombre, ' ', apellido) AS nombre_completo"])
           ->where("cliente.estatus = :status AND cliente.id_cliente = :id_cliente",
           [':status' => 1,':id_cliente' => Yii::$app->request->post('id')])->asArray()->all();


    }

    public function actionBuscarAuto()
    {
        if(!empty(Yii::$app->request->post('id'))){
          Yii::$app->response->format = Response::FORMAT_JSON;
            return Automovil::datos_vehiculo(Yii::$app->request->post('id'));
        }

    }

    public function actionBuscarCliente2()
    {
         if(!empty(Yii::$app->request->post('id'))){
         Yii::$app->response->format = Response::FORMAT_JSON;
                            return Cliente::find()
                                       ->joinwith('idPersona')
                                        ->where("cliente.estatus = :status AND Cliente.id_cliente = :id_cliente",
                                        [':status' => 1,':id_cliente' => Yii::$app->request->post('id')])
                                        ->select("(nombre||' '||segundo_nombre||' '||apellido|| ' ' ||segundo_apellido || ' ' || identificacion) AS nombre_completo ,id_cliente")
                                        ->asArray()->all();
        }
    }

    public function actionBuscarVendedor()
    {
          if(!empty(Yii::$app->request->post('id'))){
            Yii::$app->response->format = Response::FORMAT_JSON;
                    return Intermediario::find()
                                 ->joinwith('idPersona')
                                 ->where("intermediario.estatus = :status AND intermediario.id_intermediario = :intermediario AND intermediario.id_tipo_intermediario=2",
                                 [':status' => 1,':intermediario' => Yii::$app->request->post('id')])
                                 //->select("(nombre||' '||segundo_nombre||' '||apellido|| ' ' ||segundo_apellido) AS nombre_completo,id_intermediario,codigo,comision")
                                 ->select(['*',"CONCAT(nombre, ' ', apellido) AS nombre_completo"])->asArray()->all();
          }
    }


    protected function findModel($id)
    {
        if (($model = Poliza::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	protected function datos($id)
    {
        if (($model = Poliza::datos_poliza($id)) !== null) {
            return $model;
        } else {

            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
