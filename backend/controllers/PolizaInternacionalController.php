<?php

namespace backend\controllers;

use Yii;
use backend\models\Poliza;
use backend\models\PolizaAnulacion;
use backend\models\search\PolizaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\base\Model;
use yii\web\Response;
use yii\widgets\Pjax;
use backend\models\Cliente;
use backend\models\Intermediario;
use backend\models\Persona;
use backend\models\Coberturas;
use backend\models\CoberturasView;
use backend\models\Aseguradoras;
use backend\models\InterAsegRamo;
use backend\models\InterAsegProducto;
use backend\models\Productos;
use backend\models\Recibo;
use backend\models\ReciboAnulacion;
use backend\models\Certificado;
use backend\models\CertificadoDetalle;
use backend\models\InterProdCob;
use backend\models\CertificadoAsegurados;
use backend\models\CertificadoBeneficiarios;
use backend\models\CargaFamiliarView;


/**
 * PolizaController implements the CRUD actions for Poliza model.
 */
class PolizaInternacionalController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Poliza models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PolizaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,5,1);//ramo=salud internacional, tipo=individual

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Poliza model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
             'model' => $this->datos($id),
        ]);
    }

	public function actionViewModal($id)
    {

        return $this->renderAjax('view-modal', [
             'model' => $this->datos($id),
        ]);
    }

    public function actionAnulacion($id)
    {
        $model = $this->findModelAnulacion($id);

    //print_r($model);

    if ($model->load(Yii::$app->request->post())) {
      $model->fecha_vigenciahasta=$model->fecha_anulacion;
      if($model->estatus_poliza=='Eliminar'){
        $model->estatus=0;
      }
      if($model->save()){
                return $this->redirect(['index']);
      }else{
        var_dump($model->getErrors());
      }


        }else{
            return $this->render('anulacion', [
                        'model' => $model
            ]);
        }

       /* if ($model->load(Yii::$app->request->post())) {

          if($model->save()){
                return $this->redirect(['index', 'id' => $model->id_poliza]);
          }else{
                 var_dump($model->getErrors());
          }

        } else {
            return $this->renderAjax('anulacion', [
                'model' => $model,
            ]);
        }*/
    }
    public function actionAnulacionRecibo($id)
    {
        $model = $this->findModelRecibo($id);

    //print_r($model); //exit;

    if ($model->load(Yii::$app->request->post())) {
      $model->fecha_vigenciahasta=$model->fecha_anulacion;
      $model->estatus_recibo='Anulado';

      if($model->save()){
                return $this->redirect(['index']);
      }else{
        var_dump($model->getErrors());
      }


        }else{
            return $this->render('anulacion-recibo', [
                        'model' => $model
            ]);
        }

    }

    /**
     * Creates a new Poliza model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
public function actionCreate()
{
        $model = new Poliza();
        $recibo = new Recibo();
        $certificado= new Certificado();
        $certificadoDetalle= new CertificadoDetalle();

        $post=Yii::$app->request->post();


         // ajax validation
        /* if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
               Yii::$app->response->format = Response::FORMAT_JSON;
                 return ActiveForm::validate($model);
          }*/



        if ($model->load(Yii::$app->request->post())){//validar post

				$model->id_contratante=$model->id_contratante_hidden;
				$model->id_tipo=1;
				$model->fecha_registro=date('Y-m-d H:i:s');
				$model->id_user_registro=isset(Yii::$app->user->identity->id)?Yii::$app->user->identity->id:1;
				$model->id_ramo=2;

          if($bandera=$model->save(false)){//validar que guarde la poliza

			   //exit(var_dump($model));
				$recibo->nro_recibo=$model->nro_recibo;
				$recibo->id_poliza=$model->id_poliza;
				$recibo->tipo_recibo=$model->tipo_recibo;
				$recibo->fecha_registro=$model->fecha_registro;
				$recibo->id_user_registro=$model->id_user_registro;
				$recibo->fecha_vigenciadesde=$model->fecha_vigenciadesde;
				$recibo->fecha_vigenciahasta=$model->fecha_vigenciahasta;
				$recibo->id_vendedor=$post['Poliza']['id_vendedor'];
				$recibo->id_productor=2;//OJO LLENARLO CON EL ID DEL USUARIO DEL ASESOR
				$recibo->comision_total=$post['Poliza']['comision_total'];
				$recibo->comision_vendedor=$post['Poliza']['comision_vendedor'];

            if($bandera=$recibo->save(false)){//validar que guarde el recibo

                  //validar certificados
                  $certificado->id_recibo=$recibo->id_recibo;
                  $certificado->nro_certificado=$model->nro_certificado;
                  $certificado->fecha_registro=$model->fecha_registro;
                  $certificado->id_user_registro=$model->id_user_registro;
                  $certificado->id_producto=$model->producto;
                  $certificado->id_titular=$post['Poliza']['id_contratante_hidden'];
				  $certificado->deducible=$post['Poliza']['deducible'];


                  if($bandera=$certificado->save(false)){


                      $suma=Yii::$app->request->post('suma');
                      $tasa=Yii::$app->request->post('tasa');
                      $prima=Yii::$app->request->post('prima');
                      $cobBox=Yii::$app->request->post('cobBox');


                         //Yii::$app->session->setFlash('success','You have updated your profile.');

                           //REGISTRANDO ASEGURADOS AL CERTIFICADO

                           if(count($_POST["asegurado"])){
                              
                                foreach ($_POST["asegurado"] as $clave => $valor){
                                  $certificadoAsegurados= new certificadoAsegurados();
                                  $certificadoAsegurados->id_certificado=$certificado->id_certificado;
                                  $certificadoAsegurados->id_cliente=$clave;
    
                                  if(!$bandera=$certificadoAsegurados->save(false)){
                    					         var_dump($certificadoAsegurados->getErrors());exit();
                    			    }
    
                               }  
   
                          }

                           //REGISTRANDO BENEFICIARIOS AL CERTIFICADO
                           
                           

                           if(isset($_POST["beneficiario"])){
                               
                                  $porcentaje=Yii::$app->request->post('porcentaje');
                               
                                  foreach ($_POST["beneficiario"] as $clave => $valor){
                                    $certificadoBeneficiarios= new certificadoBeneficiarios();
                                    $certificadoBeneficiarios->id_certificado=$certificado->id_certificado;
                                    $certificadoBeneficiarios->id_cliente=$clave;
                                    $certificadoBeneficiarios->beneficiario_porcentaje=$porcentaje[$clave];
                                    if(!$bandera=$certificadoBeneficiarios->save(false)){
                      					    var_dump($certificadoBeneficiarios->getErrors());exit();
                      				}
    
                                 }  
                               
                           }


              					  foreach ($cobBox as $clave => $valor){

                    						  $certificadoDetalle= new CertificadoDetalle();
                    							$certificadoDetalle->id_certificado=$certificado->id_certificado;
                                                $certificadoDetalle->suma_asegurada=str_replace(",", ".",str_replace(".", "",$suma[$clave]));
                    							$certificadoDetalle->prima=str_replace(",", ".",str_replace(".", "",$prima[$clave]));
                    							$certificadoDetalle->comision=0;
                    							$certificadoDetalle->fecha_registro=$model->fecha_registro;
                    							$certificadoDetalle->id_user_registro=$model->id_user_registro;
                    							$certificadoDetalle->id_cobertura=$clave;
              							//  echo 'tasa'.$tasa[$clave];
                							if(!$certificadoDetalle->save(false)){
                								var_dump($certificadoDetalle->getErrors());
                							}
              					  }


          					  if($bandera){
          					       return $this->redirect(['view', 'id' => $model->id_poliza]);
          					  }else{
          						       var_dump($recibo->getErrors());
          					  }

                  }else{
                    var_dump($recibo->getErrors());
                  }

            }else{

              var_dump($recibo->getErrors());

            }
          }

        } else {
             // var_dump($recibo->getErrors());
            return $this->render('create', [
                'model' => $model,
            ]);
        }
}

      public function actionBuscarCoberturas()
      {

        $filtroaux="AND id_cobertura <> 5";
        if(Yii::$app->request->post('tipo')==0){
        $filtroaux="AND id_cobertura IN (5,12)";
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
         return CoberturasView::find()->where("cobertura_estatus = :status AND producto_estatus =:status AND aseguradora_estatus =:status
                                                AND id_aseguradora = :aseguradora AND id_producto = :producto $filtroaux",
                                                [':status' => 1,
                                                 ':aseguradora' => Yii::$app->request->post('aseguradora'),
                                                 ':producto' => Yii::$app->request->post('producto')])
                                       ->select(['id_cobertura',"nombre_cobertura AS cobertura","suma_asegurada_ AS suma_asegurada","prima_ AS prima"])->asArray()->all();



      }

      public function actionBuscarCliente()
      {

          Yii::$app->response->format = Response::FORMAT_JSON;
            return Cliente::find()->joinWith(['idPersona'])
           ->select(["*","CONCAT(nombre, ' ', apellido) AS nombre_completo"])
           ->where("cliente.estatus = :status AND cliente.id_cliente = :id_cliente",
           [':status' => 1,':id_cliente' => Yii::$app->request->post('id')])->asArray()->all();
           //->groupBy('seller_id')
           //->orderBy(['cnt' => 'DESC'])

      }


  public function actionBuscarAsegurados_cargafamiliar(){//INABILITADO
  //PARA CARGA FAMILIAR
	  Yii::$app->response->format = Response::FORMAT_JSON;
        return CargaFamiliarView::find()->select(["substring(age(now(),fecha_nacimiento)::text from 1 for 2)::int AS anios","*"])
          ->where("cliente_estatus = :status AND carga_familiar_estatus = :status AND id_beneficiario= :id_beneficiario",
          [':status' => 1,
		      ':id_beneficiario' => Yii::$app->request->get('id')])
		  ->asArray()->all();

      /*<?=$form->field($model, "asegurados",[
																		   'addon' => [
																			   'prepend' => ['content'=>'<i class="glyphicon glyphicon-user"></i>'],
																			   'append'=>['content'=>Html::a('<i class="fa fa-user-plus"></i> Nuevo', '#', [
                                                                                                 'id' => 'activity-index-link',
                                                                                                 'class' => 'btn btn-success',
                                                                                                 'data-toggle' => 'modal',
                                                                                                 'data-target' => '#modal',
                                                                                                 'data-url' => Url::to(['persona/create'], true),
                                                                                                 'data-pjax' => '0',
                                                                                ]), 'asButton'=>true],
																		   ]
																	   ])->widget(DepDrop::classname(), [
																						'data'=> [''=>'Seleccione '],
																						'options'=>['placeholder'=>'Selecione ...'],
																						'type' => DepDrop::TYPE_SELECT2,
																						'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
																						'pluginOptions'=>[
																						'depends'=>["id_contratante"],
																						'placeholder' => 'Seleccione ...',
																						'url'=>Url::to(['dependent-dropdown/child-account-asegurados','db'=>CargaFamiliar::classname(),'cmpo_dep'=>'id_titular','cmpo_mostrar'=>'nombre_completo','id_cmpo'=>'id_beneficiario']),
																					   ]
																			   ])->label(false);

															?>*/

  }

  public function actionBuscarAsegurados(){

      if(!empty(Yii::$app->request->get('id'))){

          $titular=(Yii::$app->request->get('id_titular')!=null)?Yii::$app->request->get('id_titular'):0;
          $caseParentesco="('Titular') AS parentesco";
          $isFamiliar="";

        if(!empty($titular)){//validando que es carga familiar en incluyendole el filtro de titular
            $isFamiliar="AND carga_familiar.id_titular=".$titular;
            $caseParentesco="(CASE WHEN carga_familiar.id_beneficiario IS NOT NULL THEN carga_familiar.parentesco ELSE 'Cliente' END) AS parentesco";
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
         return Cliente::find()
                     ->leftJoin('carga_familiar', 'carga_familiar.id_beneficiario= cliente.id_cliente')
                     ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
                     ->where("cliente.estatus = 1 AND cliente.id_cliente =".Yii::$app->request->get('id')." ".$isFamiliar)
                     ->select(["(nombre ||' '||COALESCE(segundo_nombre,'')) as nombres","(apellido|| ' ' ||COALESCE(segundo_apellido,'')) as apellidos",
                       "CONCAT(nombre, ' ', apellido) AS nombre_completo",
                      "identificacion","cliente.id_cliente",
                      "substring(age(now(),fecha_nacimiento)::text from 1 for 2)::int AS anios",$caseParentesco
                      ])->asArray()->all();
                        /* return Cliente::find()
                                    ->joinwith('idPersona')
                                     ->where("cliente.estatus = :status AND Cliente.id_cliente = :id_cliente",
                                     [':status' => 1,':id_cliente' => Yii::$app->request->get('id')])
                                     ->select(["CONCAT(nombre, ' ', apellido) AS nombre_completo,
                                                substring(age(now(),fecha_nacimiento)::text from 1 for 2)::int AS anios"
                                                ,"*"])->asArray()->all();*/
     }
  }





  public function actionBuscarBeneficiarios(){

      Yii::$app->response->format = Response::FORMAT_JSON;
        return Cliente::find()->joinWith(['idPersona'])
       ->select(["*","CONCAT(nombre, ' ', apellido) AS nombre_completo"])
       ->where("cliente.estatus = :status AND cliente.id_cliente = :id_cliente",
       [':status' => 1,':id_cliente' => Yii::$app->request->get('id')])->asArray()->all();
  }

      public function actionBuscarVendedor()
      {
            Yii::$app->response->format = Response::FORMAT_JSON;
                    return Intermediario::find()
                                 ->joinwith('idPersona')
                                 ->where("intermediario.estatus = :status AND intermediario.id_intermediario = :intermediario AND intermediario.id_tipo_intermediario=2",
                                 [':status' => 1,':intermediario' => Yii::$app->request->post('id')])
                                 ->select(['*',"CONCAT(nombre, ' ', apellido) AS nombre_completo"])->asArray()->all();
     }

    /**
     * Updates an existing Poliza model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
//http://stackoverflow.com/questions/28925624/yii2kartik-depdrop-widget-default-value-on-update
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_poliza]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Poliza model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Poliza model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Poliza the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Poliza::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelAnulacion($id)
    {
        if (($model = PolizaAnulacion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	protected function findModelRecibo($id)
    {
        if (($model = ReciboAnulacion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	protected function datos($id)
    {
        if (($model = Poliza::datos_polizaSalud($id)) !== null) {
            return $model;
        } else {

            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
