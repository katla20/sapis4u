<?php

namespace backend\controllers;

use Yii;
use backend\models\Facturacion;
use backend\models\DetalleFactura;
use backend\models\FacturacionDetalle;

use backend\models\search\FacturacionSearch;
use backend\models\search\ReciboSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Poliza;
use backend\models\Recibo;
use yii\data\ActiveDataProvider;
use common\models\MultipleModel as Model;
use yii\helpers\Json;
use yii\web\Response;
use yii\widgets\Pjax;

/**
 * FacturacionController implements the CRUD actions for Facturacion model.
 */
class FacturacionController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

	public function actionRecibos($id)
    {

		$searchModel = new ReciboSearch();
		//print_r(Yii::$app->request->queryParams);exit();
        $dataProvider = $searchModel->search2(Yii::$app->request->queryParams,$_GET['id']);

        return $this->renderAjax('recibos', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Facturacion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FacturacionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Facturacion model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Facturacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Facturacion();
		    $modelDetalle = [new FacturacionDetalle];

         $modelDetalle = Model::createMultiple(FacturacionDetalle::classname());
         Model::loadMultiple($modelDetalle, Yii::$app->request->post());

        if ($model->load(Yii::$app->request->post())) {

            $recibos=explode(',',$_POST["Facturacion"]['recibos']);
            $model->recibos=$recibos;
            $model->id_user_registro =Yii::$app->user->identity->id;
            $model->fecha_registro=date('Y-m-d H:i:s');
            if($flag=$model->save()){
              if(count($modelDetalle)>0){

                foreach ($modelDetalle as $modelDet) {
     							   $modelDetalle = new FacturacionDetalle;
     							   $modelDetalle->id_facturacion = $model->id_facturacion;
     							   $modelDetalle->descripcion=$modelDet->descripcion;
     							   $modelDetalle->monto=$modelDet->monto;
     							   $flag = $modelDetalle->save();
     					  }

     					  if ($flag = $modelDetalle->save(false)) {

     						}else{
     							var_dump($modelDetalle->getErrors());
     				    }

              }else{
                return $this->redirect(['view', 'id' => $model->id_facturacion]);
              }


      			}else{
      				var_dump($model->getErrors());
      			}

        } else {
            return $this->render('create', [
                'model' => $model,
				'modelDetalle' => (empty($modelDetalle)) ? [new FacturacionDetalle] : $modelDetalle,
            ]);
        }
    }

    /**
     * Updates an existing Facturacion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$modelDetalle = new DetalleFactura($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_facturacion]);
        } else {
            return $this->render('update', [
                'model' => $model,
				'modelDetalle' => $modelDetalle,
            ]);
        }
    }

    /**
     * Deletes an existing Facturacion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionCargandoRecibos()
    {
        $pk = Yii::$app->request->post('keylist');

        $recibos=Recibo::find()->where("id_recibo IN ($pk)")->select("nro_recibo,comision_vendedor")->asArray()->all();
        $cadena="";
        $recibo="";
        $comision=0;
        foreach ($recibos as $key => $value) {
            $re[]=$value['nro_recibo'];
            $comision+=$value['comision_vendedor'];
        }
         $recibo=implode(',',$re);

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $arrayName = array('recibos' => $recibo,'comision'=>$comision);

    }


    /**
     * Finds the Facturacion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Facturacion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Facturacion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
