<?php

namespace backend\controllers;

use Yii;
use backend\models\Reclamos;
use backend\models\search\ReclamosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\PartesVehiculos;
use backend\models\ReclamoSeguimiento;

/**
 * ReclamosController implements the CRUD actions for Reclamos model.
 */
class ReclamosController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Reclamos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReclamosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionReporte()
    {
        $searchModel = new ReclamosSearch();
        $dataProvider = $searchModel->search3(Yii::$app->request->queryParams);

        return $this->render('reporte', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Reclamos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	
	public function actionMensajeSegui()
    {
        $searchModel = new ReclamosSearch();
        $dataProvider = $searchModel->search2(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionReclamosEstatus($estatus)
    {
        $searchModel = new ReclamosSearch();
        $dataProvider = $searchModel->search4(Yii::$app->request->queryParams,$estatus);
        
        //print_r($dataProvider); exit();

        return $this->render('reclamos-estatus', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
	public function actionSeguimiento($id)
    {
        //return $this->render('reclamo-seguimiento');
		$model = $this->findModel($id);
		$modelSeguimiento = new ReclamoSeguimiento();
		$tipoPartes = PartesVehiculos::find()->all();

		$model->parte = \yii\helpers\ArrayHelper::getColumn(
                     $model->getInterReclamoPartes()->asArray()->all(),
			         'id_parte'
		);
		
		//print_r(Yii::$app->request->post());
		
	

		if ($model->load(Yii::$app->request->post())) {

			if (!isset($_POST['Reclamos']['parte'])) {
				     $model->parte= [];
			}

				//print_r(Yii::$app->request->post()); 
				//echo $modelSeguimiento->comentario = $modelSeguimiento->comentario.'asfasfasfasfsaf';
				 $model->monto = $_POST['ReclamoSeguimiento']['monto']+$model->monto; //exit();
				 $model->estatus_reclamo = $_POST['Reclamos']['estatus_reclamo'];
				 
				 //echo $model->monto;
				 //echo $model->estatus_reclamo;//exit();
				 
			if ($model->save()) {

				$modelSeguimiento->id_reclamo = $model->id_reclamo;
				$modelSeguimiento->fecha_registro=date('Y-m-d H:i:s');
				$modelSeguimiento->id_user_registro =Yii::$app->user->identity->id;
				$modelSeguimiento->estatus_reclamo = $_POST['Reclamos']['estatus_reclamo']; 
                $modelSeguimiento->comentario = $_POST['ReclamoSeguimiento']['comentario'];
                $modelSeguimiento->monto = $_POST['ReclamoSeguimiento']['monto'];
				
				

				if ($modelSeguimiento->save()) {

					return $this->render('reclamo-seguimiento', [
					'model' => $model,
					//'modelSeguimiento' => $modelSeguimiento,
					'tipoPartes' => $tipoPartes
					]);
				}else
					{
						var_dump($modelSeguimiento->getErrors());
					}
				
				
			}else
			{
                var_dump($model->getErrors());
            }
			
			
			
		} else {
			return $this->render('reclamo-seguimiento', [
				'model' => $model,
			//	'modelSeguimiento' => $modelSeguimiento,
				'tipoPartes' => $tipoPartes
			]);
		}
    }
	
	public function actionMensaje($id)
    {
        //return $this->render('reclamo-seguimiento');
		$model = $this->findModel($id);
		$modelMensaje = new ReclamoSeguimiento();
		
		//print_r(Yii::$app->request->post());
		//echo $model->id_reclamo;
		

		if ($modelMensaje->load(Yii::$app->request->post())) {

			

				$modelMensaje->id_reclamo = $model->id_reclamo;
				$modelMensaje->fecha_registro=date('Y-m-d H:i:s');
				$modelMensaje->id_user_registro =Yii::$app->user->identity->id;
                $modelMensaje->comentario = $_POST['ReclamoSeguimiento']['comentario'];
				$modelMensaje->user_destino = $_POST['ReclamoSeguimiento']['user_destino'];
				$modelMensaje->mensaje = 1;
				
				///print_r($modelMensaje);exit();

				if ($modelMensaje->save()) {

					$searchModel = new ReclamosSearch();
					$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

					return $this->render('index', [
						'searchModel' => $searchModel,
						'dataProvider' => $dataProvider,
					]);
				}
		} else {
			return $this->renderAjax('mensaje', [
				'model' => $model,
				'modelMensaje' => $modelMensaje
			]);
		}
    }

    /**
     * Creates a new Reclamos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Reclamos();
		$tipoPartes = PartesVehiculos::find()->all();


        if ($model->load(Yii::$app->request->post())) {
			$model->fecha_registro=date('Y-m-d H:i:s');
			$model->id_user_registro =Yii::$app->user->identity->id;
			$id_auto=$this->ObtenerAuto($_POST['placa']);

			if($id_auto){
				$model->id_automovil=$id_auto;	
			}
			 
			if($model->save(false)){
				return $this->redirect(['seguimiento', 'id' => $model->id_reclamo]);
			}else
			{
                var_dump($recibo->getErrors());
            }
            
        } else {
            return $this->render('create', [
                'model' => $model,
				'tipoPartes' => $tipoPartes
            ]);
        }
    }

    public function actionSalud($id=null)
    {
		
		if($id){
			
			 $model = $this->findModel($id);
			 $tipoPartes = PartesVehiculos::find()->all();
			 //print_r($tipoPartes);exit('saliendo');

			$model->parte = \yii\helpers\ArrayHelper::getColumn(
				$model->getInterReclamoPartes()->asArray()->all(),
				'id_parte'
			);
			
			$model->poliza=$model->idRecibo->id_poliza;

			if ($model->load(Yii::$app->request->post())) {
				if (!isset($_POST['Reclamos']['parte'])) {
						 $model->parte= [];
				}
				
				$model->fecha_actualizacion=date('Y-m-d H:i:s');
				$model->id_user_actualizacion =Yii::$app->user->identity->id;
				
				if ($model->save()) {
					return $this->redirect(['seguimiento', 'id' => $model->id_reclamo]);
				}
			} else {
				return $this->render('salud', [
					'model' => $model,
					'tipoPartes' => $tipoPartes
				]);
			}		
	
		}else{
			
		    $model = new Reclamos();
		    $tipoPartes = PartesVehiculos::find()->all();

			if ($model->load(Yii::$app->request->post()) && $model->save()) {
		  
				$model->fecha_registro=date('Y-m-d H:i:s');
				$model->id_user_registro =Yii::$app->user->identity->id;
				$ident=$this->ObtenerIdentidad($_POST['identidad']);
				if($ident){
					$model->id_beneficiario=$ident;	
				}else return false;
				if($model->save(false)){
					return $this->redirect(['seguimiento', 'id' => $model->id_reclamo]);
				}else
				{
					var_dump($recibo->getErrors());
				}
			} else {
				return $this->render('salud', [
					'model' => $model,
					'tipoPartes' => $tipoPartes
				]);
			}	
				
				
			}
 
    }

    /**
     * Updates an existing Reclamos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		    $tipoPartes = PartesVehiculos::find()->all();

    		$model->parte = \yii\helpers\ArrayHelper::getColumn(
    			$model->getInterReclamoPartes()->asArray()->all(),
    			'id_parte'
    		);
                $model->poliza=$model->idRecibo->id_poliza;

    		if ($model->load(Yii::$app->request->post())) {
    			if (!isset($_POST['Reclamos']['parte'])) {
    				      $model->parte= [];
    			}
				$model->fecha_actualizacion=date('Y-m-d H:i:s');
			    $model->id_user_actualizacion =Yii::$app->user->identity->id;
				$id_auto=$this->ObtenerAuto($_POST['placa']);

				if($id_auto){
					$model->id_automovil=$id_auto;	
				}
    			if ($model->save()) {
    				return $this->redirect(['seguimiento', 'id' => $model->id_reclamo]);
    			}
    		} else {
    			return $this->render('update', [
    				'model' => $model,
    				'tipoPartes' => $tipoPartes
    			]);
		}

       /* if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_reclamo]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }*/
    }

    /**
     * Deletes an existing Reclamos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Reclamos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Reclamos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Reclamos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	protected function ObtenerAuto($placa)
    {
        $res=array();
        $connection = \Yii::$app->db;
        $sql = "select id_automovil from automovil where placa= :placa";
        $command = $connection->createCommand($sql);
        $command->bindValue(":placa", $placa);
        $result = $command->queryAll();
		
		//print_r($result);exit();

        if (count($result)!= 0){

           return $result[0]['id_automovil'];

        } else{
            return NULL;

        }
    }
	
	protected function ObtenerIdentidad($ident)
    {
        $res=array();
        $connection = \Yii::$app->db;
        $sql = "select id_persona from persona where identificacion= :ident";
        $command = $connection->createCommand($sql);
        $command->bindValue(":ident", $ident);
        $result = $command->queryAll();
		
		//print_r($result);exit();

        if (count($result)!= 0){

           return $result[0]['id_persona'];

        } else{
            return NULL;

        }
    }
}
