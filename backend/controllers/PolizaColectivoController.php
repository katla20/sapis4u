<?php

namespace backend\controllers;

use Yii;
use backend\models\Poliza;
use backend\models\PolizaAnulacion;
use backend\models\search\PolizaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\base\Model;
use yii\web\Response;
use yii\widgets\Pjax;
use backend\models\Cliente;
use backend\models\Intermediario;
use backend\models\Persona;
use backend\models\Coberturas;
use backend\models\CoberturasView;
use backend\models\Aseguradoras;
use backend\models\InterAsegRamo;
use backend\models\InterAsegProducto;
use backend\models\Productos;
use backend\models\Recibo;
use backend\models\ReciboAnulacion;
use backend\models\Certificado;
use backend\models\CertificadoDetalle;
use backend\models\InterProdCob;
use backend\models\CertificadoAsegurados;
use backend\models\CertificadoBeneficiarios;
use arogachev\excel\import\basic\Importer;
use backend\models\Documentos;
use backend\models\ExcelColectivo;


/**
 * PolizaController implements the CRUD actions for Poliza model.
 */
class PolizaColectivoController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    public function actionAnulacion($id)
    {
        $model = $this->findModelAnulacion($id);

		//print_r($model);

		if ($model->load(Yii::$app->request->post())) {
			$model->fecha_vigenciahasta=$model->fecha_anulacion;
			if($model->estatus_poliza=='Eliminar'){
				$model->estatus=0;
			}
			if($model->save()){
                return $this->redirect(['index']);
			}else{
				var_dump($model->getErrors());
			}


        }else{
            return $this->render('anulacion', [
                        'model' => $model
            ]);
        }

       /* if ($model->load(Yii::$app->request->post())) {

          if($model->save()){
                return $this->redirect(['index', 'id' => $model->id_poliza]);
          }else{
                 var_dump($model->getErrors());
          }

        } else {
            return $this->renderAjax('anulacion', [
                'model' => $model,
            ]);
        }*/
    }
    public function actionAnulacionRecibo($id)
    {
        $model = $this->findModelRecibo($id);

		//print_r($model); //exit;

		if ($model->load(Yii::$app->request->post())) {
			$model->fecha_vigenciahasta=$model->fecha_anulacion;
			$model->estatus_recibo='Anulado';

			if($model->save()){
                return $this->redirect(['index']);
			}else{
				var_dump($model->getErrors());
			}


        }else{
            return $this->render('anulacion-recibo', [
                        'model' => $model
            ]);
        }

    }


    /**
     * Lists all Poliza models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PolizaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,2,2);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Poliza model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
             'model' => $this->datos($id),
        ]);
    }

    /**
     * Creates a new Poliza model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */


public function actionImportExcel($recibo,$id_aseguradora)
{

    $usuario=Yii::$app->user->identity->id;
    $fecha_registro=date('Y-m-d').'-'.date('H');
    $inputFile=Yii::$app->basePath.'/uploads/excel/'.$usuario.'-'.$fecha_registro.'.xlsx';

    try{
        $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
        $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFile);

    } catch (Exception $e) {
        die('Error');
    }

    $sheet = $objPHPExcel->getSheet(0);
    $highestRow = $sheet->getHighestRow();
    $highestColumn = $sheet->getHighestColumn();

    for($row=1; $row <= $highestRow; $row++)
    {
        $rowData = $sheet->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);

        if($row==1)
        {
            continue;
        }
         
         
        //print_r($rowData); exit();
         
       /*  if($row==3)
        {
             print_r($rowData); exit();
        }*/
         
        
          $excel= new ExcelColectivo();
          $excel->certificado=$rowData[0][0];
          $excel->tipo=$rowData[0][1];
          $excel->cedula_t=$rowData[0][2];
		  $excel->cedula=$rowData[0][3];
          $excel->primer_nombre=$rowData[0][4];
          $excel->primer_apellido=$rowData[0][5];
          $excel->fecha_nacimiento=$rowData[0][6];
		  $excel->sexo=$rowData[0][7];
		  $excel->parentesco=$rowData[0][8];
          $excel->estado=$rowData[0][9];
          $excel->ciudad=$rowData[0][10];
          $excel->direccion=$rowData[0][11];
          $excel->producto=$rowData[0][12];
          $excel->suma_asegurada=$rowData[0][13];
          $excel->prima=$rowData[0][14];
          $excel->deducible=$rowData[0][15];
		  $excel->porcentaje_beneficiario=$rowData[0][16];
		  $excel->maternidad=$rowData[0][17];
		  $excel->id_user=$usuario;
          if($rowData[0][2]!= ""){
            $excel->save();
            if($bandera=$excel->save(false)){
                 echo $rowData[0][1]."llegue";
                }else{
                    
                   print_r($excel->getErrors());
                }
         
         
         
            }
         
        //print_r($excel->getErrors());
    }

 
        $result=array();
        $connection = \Yii::$app->db;
        echo $sql = "SELECT carga_masiva_colectivo('$usuario','$recibo','$id_aseguradora')"; //exit;
        $command = $connection->createCommand($sql);

        $result = $command->queryAll();

        if ($result){
			return true;
			//
        }else return false;//die('fallo');
    


    
}
	 
public function actionCreate()
    {
        $model = new Poliza();
        $recibo = new Recibo();
        $documentos= new Documentos();
         // ajax validation
        /* if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
               Yii::$app->response->format = Response::FORMAT_JSON;
                 return ActiveForm::validate($model);
          }*/

          $post=Yii::$app->request->post();
		  
		 // print_r($post);exit;

        if ($model->load(Yii::$app->request->post())){//validar post

            $model->id_contratante=$model->id_contratante_hidden;
            $model->id_tipo=2;
            $model->fecha_registro=date('Y-m-d H:i:s');
            $model->id_user_registro=isset(Yii::$app->user->identity->id)?Yii::$app->user->identity->id:1;
			$model->id_ramo=2;
			$model->fecha_vigenciadesde=date("Y-m-d", strtotime($model->fecha_vigenciadesde));
            $model->fecha_vigenciahasta=date("Y-m-d", strtotime($model->fecha_vigenciahasta));
			
			$transaction = \Yii::$app->db->beginTransaction();
               try {

					  if($bandera=$model->save(false)){//validar que guarde la poliza

					   //exit(var_dump($model));
						$recibo->nro_recibo=$model->nro_recibo;
						$recibo->id_poliza=$model->id_poliza;
						$recibo->tipo_recibo=$model->tipo_recibo;
						$recibo->fecha_registro=$model->fecha_registro;
						$recibo->id_user_registro=$model->id_user_registro;
						$recibo->fecha_vigenciadesde=date("Y-m-d", strtotime($post['Recibo']['fecha_vigenciadesde']));
                        $recibo->fecha_vigenciahasta=date("Y-m-d", strtotime($post['Recibo']['fecha_vigenciahasta']));
						$recibo->id_subcontratante=$post['Poliza']['id_subcontratante'];
						$recibo->id_vendedor=$post['Poliza']['id_vendedor'];
						$recibo->id_productor=2;//OJO LLENARLO CON EL ID DEL USUARIO DEL ASESOR
					    $recibo->comision_total=$post['Poliza']['comision_total'];
					    $recibo->comision_vendedor=$post['Poliza']['comision_vendedor'];

						if($bandera=$recibo->save(false)){//validar que guarde el recibo

						  //extrayendo los datos del excel

						$bandera=$this->actionImportExcel($recibo->id_recibo,$model->id_aseguradora);
                        
						if($bandera){
								
							$transaction->commit();
							
							die('okay');
						}else{
							die('fallo');
						}
						

						}else{

						  var_dump($recibo->getErrors());

						}


					  }
					} catch (Exception $e) {
							  $transaction->rollBack();
						  }  


        } else {
             // var_dump($recibo->getErrors());
            return $this->render('create', [
                'model' => $model,'documentos' => $documentos,'recibo' => $recibo,


            ]);
        }

}

      public function actionBuscarCoberturas()
      {
        Yii::$app->response->format = Response::FORMAT_JSON;
         return ArrayHelper::map(CoberturasView::find()
                                      ->where("cobertura_estatus = :status AND producto_estatus =:status AND aseguradora_estatus =:status
                                                AND id_aseguradora = :aseguradora AND id_producto = :producto ",
                                                [':status' => 1,
                                                 ':aseguradora' => Yii::$app->request->post('aseguradora'),
                                                 ':producto' => Yii::$app->request->post('producto')])
                                      ->all(),'id_cobertura', 'nombre_cobertura');


      }

      public function actionBuscarCliente()
      {

          Yii::$app->response->format = Response::FORMAT_JSON;
            return Cliente::find()->joinWith(['idPersona'])
           ->select(["*","CONCAT(nombre, ' ', apellido) AS nombre_completo"])
           ->where("cliente.estatus = :status AND cliente.id_cliente = :id_cliente",
           [':status' => 1,':id_cliente' => Yii::$app->request->post('id')])->asArray()->all();
           //->groupBy('seller_id')
           //->orderBy(['cnt' => 'DESC'])

      }


  public function actionBuscarAsegurados(){
        /*SELECT CONCAT(nombre, ' ', apellido) AS nombre_completo, identificacion FROM cliente
       LEFT JOIN persona ON cliente.id_persona= persona.id_persona WHERE cliente.estatus
       = 1 AND cliente.id_cliente = '2'*/

      Yii::$app->response->format = Response::FORMAT_JSON;
        return Cliente::find()->joinWith(['idPersona'])
       ->select(["*","CONCAT(nombre, ' ', apellido) AS nombre_completo"])
       ->where("cliente.estatus = :status AND cliente.id_cliente = :id_cliente",
       [':status' => 1,':id_cliente' => Yii::$app->request->get('id')])->asArray()->all();
       //->groupBy('seller_id')
       //->orderBy(['cnt' => 'DESC'])

  }
  public function actionBuscarBeneficiarios(){
        /*SELECT CONCAT(nombre, ' ', apellido) AS nombre_completo, identificacion FROM cliente
       LEFT JOIN persona ON cliente.id_persona= persona.id_persona WHERE cliente.estatus
       = 1 AND cliente.id_cliente = '2'*/

      Yii::$app->response->format = Response::FORMAT_JSON;
        return Cliente::find()->joinWith(['idPersona'])
       ->select(["*","CONCAT(nombre, ' ', apellido) AS nombre_completo"])
       ->where("cliente.estatus = :status AND cliente.id_cliente = :id_cliente",
       [':status' => 1,':id_cliente' => Yii::$app->request->get('id')])->asArray()->all();
       //->groupBy('seller_id')
       //->orderBy(['cnt' => 'DESC'])

  }

      public function actionBuscarVendedor()
      {
            Yii::$app->response->format = Response::FORMAT_JSON;
                    return Intermediario::find()
                                 ->joinwith('idPersona')
                                 ->where("intermediario.estatus = :status AND intermediario.id_intermediario = :intermediario AND intermediario.id_tipo_intermediario=2",
                                 [':status' => 1,':intermediario' => Yii::$app->request->post('id')])
                                 //->select("(nombre||' '||segundo_nombre||' '||apellido|| ' ' ||segundo_apellido) AS nombre_completo,id_intermediario,codigo,comision")
                                 ->select(['*'])
                                 ->all();
     }

    /**
     * Updates an existing Poliza model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
//http://stackoverflow.com/questions/28925624/yii2kartik-depdrop-widget-default-value-on-update
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_poliza]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Poliza model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Poliza model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Poliza the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Poliza::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    protected function findModelAnulacion($id)
    {
        if (($model = PolizaAnulacion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	protected function findModelRecibo($id)
    {
        if (($model = ReciboAnulacion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    } 
	
	protected function datos($id)
    {
        if (($model = Poliza::datos_polizaSalud($id)) !== null) {
            return $model;
        } else {

            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
