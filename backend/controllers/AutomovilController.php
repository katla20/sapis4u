<?php

namespace backend\controllers;

use Yii;
use backend\models\Automovil;
use backend\models\Documentos;
use backend\models\search\AutomovilSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\widgets\MaskedInput;
use yii\bootstrap\Tabs;
use yii\web\Response;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\ActiveField;
use yii\helpers\ArrayHelper;
use kartik\icons\Icon;
use kartik\tabs\TabsX;
use kartik\nav\NavX;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Json;



/**
 * AutomovilController implements the CRUD actions for Automovil model.
 */
class AutomovilController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Automovil models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AutomovilSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Automovil model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
       
        return $this->render('view', [
            'model' => $this->findModel($id),
            'datosDocumentos'=>$this->consultarDocumentos($id),
            'ruta'=>Yii::$app->basePath
        ]);
    }
    
    public function consultarDocumentos($id)
    {
       return Automovil::datos_documentos($id);
    }
    

    /**
     * Creates a new Automovil model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($submit = false)
    {
        $model = new Automovil();
        $documentos= new Documentos();
        // ajax validation
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
             return ActiveForm::validate($model);
        }
     
        if($model->load(Yii::$app->request->post())){
          
              $model->fecha_registro=date('Y-m-d H:i:s');
              $model->id_user_registro=Yii::$app->user->identity->id;
        
                if ($model->save()) {//registrando el vehiculo
                
                   $datosAuto=Automovil::datosVehiculoArray($model->id_automovil);
                    Yii::$app->response->format = Response::FORMAT_JSON;
                     return [
                        'message' => 'Exito',
                        'id_automovil'=>$model->id_automovil,
                        'placa'=>$datosAuto['placa'],
                        'marca'=>$datosAuto['marca'],
                        'modelo'=>$datosAuto['modelo'],
                        'version'=>$datosAuto['version'],
                        'anio'=>$datosAuto['anio'],
                        'uso'=>$datosAuto['uso'],
                        'tipo_vehiculo'=>$datosAuto['tipo_vehiculo']
                    
                  ]; 

                }else{
				    var_dump($model->getErrors());
			    }
     
          }else{

            return $this->renderAjax('create', [
                'model' => $model,
                'documentos'=>$documentos,
            ]);
        }
    }


    public function actionUploadFile(){


      $documentos= new Documentos();
      $model= new Automovil();
      $ruta=Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/uploads/automovil/';

      $doc_files= UploadedFile::getInstances($documentos, 'doc_files');
      $documentos->doc_files=null;
      
     
      if($model->load(Yii::$app->request->post())){


          foreach ($doc_files as $file) {


                 $archivo=$ruta. $model->placa.'_'.$file->name;
                 $documentos->extension=$file->extension;
                 $documentos->nombre_archivo=$model->placa.'_'.$file->name;
                 $documentos->ruta=$model->placa.'_'.$file->name;
                 $documentos->categoria='auto';
                 $documentos->identificacion=$model->placa;

                   if(!$documentos->save(false)){
                         echo json_encode(['error'=>$files]);
                   }else{
                     if(!$file->saveAs($archivo)){
                          echo json_encode(['error'=>$files]);
                    }else{

                        echo  json_encode(['successful'=>$file]);

                    }
                 }
           }


      }
      
      if (empty($doc_files)) {
            echo json_encode(['successful'=>'Listo']);
      }

     }
     
     private function downloadFile($file)
     {
         
       //Ruta absoluta del archivo
       $path = Yii::$app->basePath.'/uploads/automovil/'.$file;
       
        //Obtener información del archivo
        $file_info = pathinfo($path);
 
           //Procedemos a descargar el archivo
           // Definir headers
           $size = filesize($path);
           header("Content-Type: application/force-download");
           header("Content-Disposition: attachment; filename=$file");
           header("Content-Transfer-Encoding: binary");
           header("Content-Length: " . $size);
           // Descargar archivo
           readfile($path);
           //Correcto
           return true;

      //Ha ocurrido un error al descargar el archivo
      return false;
     }
 
     public function actionDownload()
     {
      if (Yii::$app->request->get("file"))
      {
       //Si el archivo no se ha podido descargar
       //if (!$this->downloadFile(Html::encode($_GET["file"])))
       if (!$this->downloadFile($_GET["file"]))
       {
        //Mensaje flash para mostrar el error
        Yii::$app->session->setFlash("errordownload");
       }
      }
      
      return $this->render("download");
     }

    /**
     * Updates an existing Automovil model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

     //http://stackoverflow.com/questions/29292377/yii2-kartik-file-input-update/34666757
    public function actionUpdate($id)
    {
        echo $id;
        $model = $this->findModel($id);
        $documentos= new Documentos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id_automovil]);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
                'documentos'=>$documentos,
            ]);
        }
    }

    /**
     * Deletes an existing Automovil model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Automovil model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Automovil the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
    
        if (($model = Automovil::datos_vehiculo_update($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
}
