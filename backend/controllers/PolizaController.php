<?php

namespace backend\controllers;

use Yii;
use backend\models\Poliza;
use backend\models\PolizaAnulacion;
use backend\models\search\PolizaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\base\Model;
use yii\web\Response;
use yii\widgets\Pjax;
use backend\models\Cliente;
use backend\models\Intermediario;
use backend\models\Persona;
use backend\models\Coberturas;
use backend\models\CoberturasView;
use backend\models\Aseguradoras;
use backend\models\InterAsegRamo;
use backend\models\InterAsegProducto;
use backend\models\Productos;
use backend\models\Recibo;
use backend\models\ReciboAnulacion;
use backend\models\Certificado;
use backend\models\CertificadoDetalle;
use backend\models\InterProdCob;
use backend\models\Automovil;
use backend\models\Version;
use backend\models\Modelo;
use backend\models\Marca;
use yii\data\ActiveDataProvider;



//https://www.youtube.com/watch?v=4NivIbhq3N8
//http://stackoverflow.com/questions/30677927/yii2-how-to-validate-a-form-with-ajax-and-then-submit-the-data-using-ajax-again


/**
 * PolizaController implements the CRUD actions for Poliza model.
 */
class PolizaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Poliza models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PolizaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,1,1);//ramo=automovil, tipo=individual

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionReporte()
    {
        $searchModel = new PolizaSearch();


        $dataProvider = $searchModel->search2(Yii::$app->request->queryParams);
        //$dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('reporte', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionAnulacion($id)
    {
        $model = $this->findModelAnulacion($id);

		//print_r($model);

		if ($model->load(Yii::$app->request->post())) {
			    $model->fecha_vigenciahasta=$model->fecha_anulacion;
			if($model->estatus_poliza=='Eliminar'){
				$model->estatus=0;
			}
			if($model->save()){
                return $this->redirect(['index']);
			}else{
				var_dump($model->getErrors());
			}


        }else{
            return $this->render('anulacion', [
                        'model' => $model
            ]);
        }

    }
    public function actionAnulacionRecibo($id)
    {
        $model = $this->findModelRecibo($id);

		if ($model->load(Yii::$app->request->post())) {

			$model->fecha_vigenciahasta=$model->fecha_anulacion;
			$model->estatus_recibo='Anulado';

			if($model->save()){
                return $this->redirect(['index']);
			}else{
				var_dump($model->getErrors());
			}


        }else{
            return $this->render('anulacion-recibo', [
                        'model' => $model
            ]);
        }

    }
    public function actionRenovacion()
       {
           //print_r(Yii::$app->request->queryParams);exit();

          $query= Poliza::find()->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
                                 ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
                                ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
                                 ->innerJoin('ramos', 'ramos.id_ramo = poliza.id_ramo')
                                 ->where("poliza.estatus = :status  AND to_char(poliza.fecha_vigenciahasta,'mm-yyyy') = :fecha2",
                                  [':status' => 1,':fecha2'=> date('m-Y',strtotime('+1 month'))])
                                ->select("numero_poliza AS numero_poliza ,id_poliza,
                                  (persona.nombre ||' '||persona.apellido) as nombre_completo,
                                  persona.identificacion,aseguradoras.nombre AS aseguradora,(fecha_vigenciadesde||'  -  '||fecha_vigenciahasta) AS fecha_vigenciahasta,
                      ramos.nombre AS ramo");

          $dataProvider = new ActiveDataProvider([
               'query' => $query,
      			   'pagination' => [
      						'pageSize' => 12,
      				]
           ]);



           //$searchModel = new PolizaSearch();
           $searchModel=$dataProvider; //->search(Yii::$app->request->queryParams);

           return $this->render('renovacion', [
               'searchModel' => $searchModel,
               'dataProvider' => $dataProvider,
           ]);
       }

       public function actionPeriodo()
          {
              //print_r(Yii::$app->request->queryParams);exit();

             $query= Poliza::find()->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
      				                     ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
                                   ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
  						                     ->innerJoin('ramos', 'ramos.id_ramo = poliza.id_ramo')
                                  ->where("poliza.estatus = :status AND poliza.fecha_vigenciahasta >= :fecha1 AND poliza.fecha_vigenciahasta < :fecha2",
                                     [':status' => 1,':fecha1'=> date('d-m-Y',strtotime('-1 month')),
                                     ':fecha2'=> date('d-m-Y')])
                            ->select("numero_poliza AS numero_poliza ,id_poliza,
                              (persona.nombre ||' '||persona.apellido) as nombre_completo,
                             persona.identificacion,aseguradoras.nombre AS aseguradora,(fecha_vigenciadesde||'  -  '||fecha_vigenciahasta) AS fecha_vigenciahasta,
  						   ramos.nombre AS ramo");


             $dataProvider = new ActiveDataProvider([
                  'query' => $query,
				  'pagination' => [
						'pageSize' => 12,
				  ]
              ]);



              //$searchModel = new PolizaSearch();
              $searchModel=$dataProvider; //->search(Yii::$app->request->queryParams);

              return $this->render('periodo', [
                  'searchModel' => $searchModel,
                  'dataProvider' => $dataProvider,
              ]);
          }

          public function actionVencidas()
             {
                 //print_r(Yii::$app->request->queryParams);exit();

                $query= Poliza::find()->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
         				                      ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
                                      ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
     						                      ->innerJoin('ramos', 'ramos.id_ramo = poliza.id_ramo')
                                     ->where("poliza.estatus = :status AND poliza.fecha_vigenciahasta < :fecha1 ",
                                        [':status' => 1,':fecha1'=> date('d-m-Y',strtotime('-1 month'))])
                               ->select("numero_poliza AS numero_poliza ,id_poliza,
                                (persona.nombre ||' '||persona.apellido) as nombre_completo,
                                persona.identificacion,aseguradoras.nombre AS aseguradora,(fecha_vigenciadesde||'  -  '||fecha_vigenciahasta) AS fecha_vigenciahasta,
     						   ramos.nombre AS ramo");

                $dataProvider = new ActiveDataProvider([
                     'query' => $query,
					 'pagination' => [
						'pageSize' => 12,
					 ]
                 ]);



                 //$searchModel = new PolizaSearch();
                 $searchModel=$dataProvider; //->search(Yii::$app->request->queryParams);

                 return $this->render('vencidas', [
                     'searchModel' => $searchModel,
                     'dataProvider' => $dataProvider,
                 ]);
             }

    /**
     * Displays a single Poliza model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        
       //print_r($this->datos($id));exit();
        return $this->render('view', [
             'model' => $this->datos($id),
        ]);
    }

	public function actionViewModal($id)
    {

        return $this->renderAjax('view-modal', [
             'model' => $this->datos($id),
        ]);
    }

    /**
     * Creates a new Poliza model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Poliza();
        $recibo = new Recibo();
        $certificado= new Certificado();
        $certificadoDetalle= new CertificadoDetalle();
        $post=Yii::$app->request->post();

     
        if ($model->load(Yii::$app->request->post())){//validar post
        
         
           $transaction = Yii::$app->db->beginTransaction();

            $model->id_contratante=$model->id_contratante_hidden;
            $model->id_tipo=1;
            $model->fecha_registro=date('Y-m-d H:i:s');
            $model->id_user_registro=Yii::$app->user->identity->id;
			$model->id_ramo=1;
			//$model->fecha_vigenciadesde=date("Y-m-d", strtotime($model->fecha_vigenciadesde));
            //$model->fecha_vigenciahasta=date("Y-m-d", strtotime($model->fecha_vigenciahasta));

          if($bandera=$model->save(false)){//validar que guarde la poliza

          // exit(var_dump($model));
            $recibo->nro_recibo=$model->nro_recibo;
            $recibo->id_poliza=$model->id_poliza;
            $recibo->tipo_recibo=$model->tipo_recibo;
            $recibo->fecha_registro=$model->fecha_registro;
            $recibo->id_user_registro=$model->id_user_registro;
			
            //$recibo->fecha_vigenciadesde=date("Y-m-d", strtotime($model->fecha_vigenciadesde));
            //$recibo->fecha_vigenciahasta=date("Y-m-d", strtotime($model->fecha_vigenciahasta));
            $recibo->fecha_vigenciadesde=$model->fecha_vigenciadesde;
            $recibo->fecha_vigenciahasta=$model->fecha_vigenciahasta;
            $recibo->id_vendedor=$post['Poliza']['id_vendedor'];
            $recibo->id_productor=2;//OJO LLENARLO CON EL ID DEL USUARIO DEL ASESOR MASTER
	        $recibo->comision_total=$post['Poliza']['comision_total'];
	        $recibo->comision_vendedor=$post['Poliza']['comision_vendedor'];

            if($bandera=$recibo->save(false)){//validar que guarde el recibo

                  //validar certificados
                  $certificado->id_recibo=$recibo->id_recibo;
                  $certificado->nro_certificado=$model->nro_certificado;
                  $certificado->id_automovil=$post['Poliza']['id_automovil_hidden'];
                  $certificado->fecha_registro=$model->fecha_registro;
                  $certificado->id_user_registro=$model->id_user_registro;
                  $certificado->id_producto=$model->producto;
                  $certificado->id_titular=$post['Poliza']['id_asegurado_hidden'];
				  $certificado->deducible=$post['Poliza']['deducible'];


                  if($bandera=$certificado->save(false)){


          					  $suma=Yii::$app->request->post('suma');
          					  $tasa=Yii::$app->request->post('tasa');
          					  $prima=Yii::$app->request->post('prima');
                              $cobBox=Yii::$app->request->post('cobBox');

                     foreach ($cobBox as $clave => $valor){

                             $certificadoDetalle= new CertificadoDetalle();
                             $certificadoDetalle->id_certificado=$certificado->id_certificado;
                             $certificadoDetalle->suma_asegurada=str_replace(",", ".",str_replace(".", "",$suma[$clave]));
                             $certificadoDetalle->prima=str_replace(",", ".",str_replace(".", "",$prima[$clave]));
                             $certificadoDetalle->comision=0;
                             $certificadoDetalle->fecha_registro=$model->fecha_registro;
                             $certificadoDetalle->id_user_registro=$model->id_user_registro;
                             $certificadoDetalle->id_cobertura=$clave;
                       //  echo 'tasa'.$tasa[$clave];
                         if(!$bandera=$certificadoDetalle->save(false)){
                           var_dump($certificadoDetalle->getErrors());
                            $transaction->rollBack();
                         }
                     }

          					  if($bandera){
          					       $transaction->commit();
          					       return $this->redirect(['view', 'id' => $model->id_poliza]);
          					  }else{
          						       var_dump($recibo->getErrors());
          						        $transaction->rollBack();
          					  }

                  }else{
                    var_dump($certificado->getErrors());
                     $transaction->rollBack();
                  }


            }else{
             
              var_dump($recibo->getErrors());
               $transaction->rollBack();

            }


          }else{
             
             var_dump($model->getErrors());
             $transaction->rollBack();
          }
        }else{
             // var_dump($recibo->getErrors());
            return $this->render('create', [
                'model' => $model,
            ]);
        }

}

      public function actionBuscarCoberturas()
      {

        $filtroaux="AND id_cobertura <> 5";
        if(Yii::$app->request->post('tipo')==0){
        $filtroaux="AND id_cobertura IN (5)";
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
         return CoberturasView::find()->where("cobertura_estatus = :status AND producto_estatus =:status AND aseguradora_estatus =:status
                                                AND id_aseguradora = :aseguradora AND id_producto = :producto $filtroaux",
                                                [':status' => 1,
                                                 ':aseguradora' => Yii::$app->request->post('aseguradora'),
                                                 ':producto' => Yii::$app->request->post('producto')])
                                       ->select(['id_cobertura',"nombre_cobertura AS cobertura","suma_asegurada_ AS suma_asegurada","prima_ AS prima"])
                                       ->asArray()->all();



      }

      public function actionBuscarCliente()
      {

          Yii::$app->response->format = Response::FORMAT_JSON;
            return Cliente::find()->joinWith(['idPersona'])
           ->select(["*","CONCAT(nombre, ' ', apellido) AS nombre_completo"])
           ->where("cliente.estatus = :status AND cliente.id_cliente = :id_cliente",
           [':status' => 1,':id_cliente' => Yii::$app->request->post('id')])->asArray()->all();


      }

      public function actionBuscarAuto()
      {
        if(!empty(Yii::$app->request->post('id'))){
          Yii::$app->response->format = Response::FORMAT_JSON;
            return Automovil::datosVehiculoArray(Yii::$app->request->post('id'));
            //return Automovil::datos_vehiculo(Yii::$app->request->post('id'));
        }

      }

     public function actionBuscarCliente2()
     {
         if(!empty(Yii::$app->request->post('id'))){
         Yii::$app->response->format = Response::FORMAT_JSON;
                            return Cliente::find()
                                       ->joinwith('idPersona')
                                        ->where("cliente.estatus = :status AND Cliente.id_cliente = :id_cliente",
                                        [':status' => 1,':id_cliente' => Yii::$app->request->post('id')])
                                        ->select("(nombre||' '||apellido||' '|| identificacion) AS nombre_completo ,id_cliente")
                                        ->asArray()->all();
        }
      }

      public function actionBuscarVendedor()
      {
          if(!empty(Yii::$app->request->post('id'))){
            Yii::$app->response->format = Response::FORMAT_JSON;
                    return Intermediario::find()
                                 ->joinwith('idPersona')
                                 ->where("intermediario.estatus = :status AND intermediario.id_intermediario = :intermediario AND intermediario.id_tipo_intermediario=2",
                                 [':status' => 1,':intermediario' => Yii::$app->request->post('id')])
                                 //->select("(nombre||' '||segundo_nombre||' '||apellido|| ' ' ||segundo_apellido) AS nombre_completo,id_intermediario,codigo,comision")
                                 ->select(['*',"CONCAT(nombre, ' ', apellido) AS nombre_completo"])->asArray()->all();
          }
     }

    /**
     * Updates an existing Poliza model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

          if($model->save()){
                return $this->redirect(['view', 'id' => $model->id_poliza]);
          }else{
                 var_dump($model->getErrors());
          }

        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Poliza model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Poliza model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Poliza the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Poliza::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    protected function findModelAnulacion($id)
    {
        if (($model = PolizaAnulacion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	protected function findModelRecibo($id)
    {
        if (($model = ReciboAnulacion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	protected function datos($id)
    {
        if (($model = Poliza::datos_poliza($id)) !== null) {
            return $model;
        } else {

            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
