<?php

namespace backend\controllers;

use Yii;
use backend\models\Cliente;
use backend\models\Persona;
use backend\models\Direccion;
use backend\models\CargaFamiliar;
use backend\models\CargaFamiliarView;
use backend\models\search\PersonaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
//use yii\base\Model;
use yii\web\Response;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\ActiveField;
use yii\helpers\ArrayHelper;
use common\models\MultipleModel as Model;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;
use yii\helpers\Json;
use backend\models\Documentos;
use backend\models\Estado;

/**
 * PersonaController implements the CRUD actions for persona model.
 */
class PersonaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all persona models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PersonaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,1);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

	public function actionCumpleanio()
    {
                 //print_r(Yii::$app->request->queryParams);exit();

                $query= Persona::find()->innerJoin('cliente', 'persona.id_persona = cliente.id_persona')
                                       ->leftJoin('direccion', 'direccion.id_persona = persona.id_persona')
                                     ->where("cliente.estatus = :status AND to_char(fecha_nacimiento,'dd/mm')= to_char(now(),'dd/mm')",
                                        [':status' => 1])
                               ->select("nombre,apellido,
                                        identificacion,telefono,telefono2,telefono3");

                $dataProvider = new ActiveDataProvider([
                     'query' => $query,
					 'pagination' => [
						'pageSize' => 12,
					 ],
					 'sort' =>
					   ['attributes' =>
						   [
								'nombre',
								'apellido',
								'identificacion',
								'telefono',
								'telefono2',
								'telefono3',
						   ]
					   ],
                 ]);



                 //$searchModel = new PolizaSearch();
                 $searchModel=$dataProvider; //->search(Yii::$app->request->queryParams);

                 return $this->render('cumpleanio', [
                     'searchModel' => $searchModel,
                     'dataProvider' => $dataProvider,
                 ]);
    }

    /**
     * Displays a single persona model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
             'datosDocumentos'=>$this->consultarDocumentos($id),
             'ruta'=>Yii::$app->basePath
        ]);
    }

    public function consultarDocumentos($id)
    {
       return Persona::documentos_cliente($id);
    }


	public function actionCargaFamiliar()
    {

		   // $model= new Persona();
			  $model = new Persona(['scenario' => 'familia']);
		    $modelCliente = new Cliente();
        $modelCargaFamiliar= new CargaFamiliar();
	      $asegurado_titular=$_GET['depend'];
        $tipo_persona=$_GET['tipo_persona'];

	    if ($model->load(Yii::$app->request->post())) {


			     //var_dump($model);exit();

				  //$model->fecha_nacimiento=date('Y-m-d',strtotime($model->fecha_nacimiento));//formateo de fecha antes de guardar
				  $model->tipo="V";

				   if ($flag = $model->save(false)) {

				            //guardando como cliente

          					$modelCliente->id_persona=$model->id_persona;
          					$modelCliente->fecha_registro=date('Y-m-d H:i:s');
                    $modelCliente->id_user_registro =Yii::$app->user->identity->id;

					    if ($flag = $modelCliente->save(false)) {

						    //guardando carga familiar
							$modelCargaFamiliar->id_titular=$asegurado_titular;
							$modelCargaFamiliar->id_beneficiario=$modelCliente->id_cliente;
							$modelCargaFamiliar->parentesco=$_POST['Persona']['parentesco'];
							$modelCargaFamiliar->fecha_registro=date('Y-m-d H:i:s');
                            $modelCargaFamiliar->id_user_registro =Yii::$app->user->identity->id;

							 if ($flag = $modelCargaFamiliar->save(false)) {

							  	 Yii::$app->response->format = Response::FORMAT_JSON;
								  return [
									  'message' => 'Exito',
									  'id_cliente'=>$modelCliente->id_cliente,
									  'nombre_completo'=>$model->nombre.' '.$model->apellido,
									  'identificacion'=>$model->identificacion,
									  'parentesco'=>$modelCargaFamiliar->parentesco,
									  'anios'=>$modelCliente->fecha_registro-$model->fecha_nacimiento,
                                      'tipo_persona'=>$tipo_persona

									];

						     }else{
							       var_dump($modelCargaFamiliar->getErrors());
						     }

					    }else{

							var_dump($cliente->getErrors());
						}

				   }else{

					  var_dump($model->getErrors());
				   }

		}else{

			return $this->renderAjax('create', [
        				'model' => $model,
        				'depend'=>$asegurado_titular,
                        'tipo_persona'=>$tipo_persona

			]);

		}

    }

   private function downloadFile($file)
     {

       //Ruta absoluta del archivo
       $path = Yii::$app->basePath.'/uploads/cliente/'.$file;

        //Obtener información del archivo
        $file_info = pathinfo($path);

           //Procedemos a descargar el archivo
           // Definir headers
           $size = filesize($path);
           header("Content-Type: application/force-download");
           header("Content-Disposition: attachment; filename=$file");
           header("Content-Transfer-Encoding: binary");
           header("Content-Length: " . $size);
           // Descargar archivo
           readfile($path);
           //Correcto
           return true;

      //Ha ocurrido un error al descargar el archivo
      return false;
     }

     public function actionDownload()
     {
      if (Yii::$app->request->get("file"))
      {
       //Si el archivo no se ha podido descargar
       //if (!$this->downloadFile(Html::encode($_GET["file"])))
       if (!$this->downloadFile($_GET["file"]))
       {
        //Mensaje flash para mostrar el error
        Yii::$app->session->setFlash("errordownload");
       }
      }

      return $this->render("download");
     }

public function actionBien($submit = false)
{


        $modelDireccion = new Direccion();
        $persona=$this->getPersona($_GET['depend']);

        if (Yii::$app->request->isAjax && $modelDireccion->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ArrayHelper::merge(
                ActiveForm::validate($modelDireccion)
            );
        }

        if ($modelDireccion->load(Yii::$app->request->post())) {


          if($modelDireccion->validate()){

              $modelDireccion->id_persona = $persona->id_persona;
              $modelDireccion->fecha_registro=date('Y-m-d H:i:s');
              $modelDireccion->id_user_registro =Yii::$app->user->identity->id;

               if (! ($flag = $modelDireccion->save(false))) {
                       var_dump($modelDireccion->getErrors());
               }else{


                 $Ciudad = $modelDireccion->idCiudad;//objeto de ciudad
                 $Estado = Estado::findOne($Ciudad->id_estado);//objeto estado
                 $Zona = $modelDireccion->idZona;//objeto zona

                 Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                  'message' => 'Exito',
                  'id_direccion'=>$modelDireccion->id,
                  'ciudad'=>$Ciudad->nombre,
                  'estado'=>$Estado->nombre,
                  'zona'=>$Zona->nombre,
                  'direccion'=>$modelDireccion->direccion,
                  'telefono'=>$modelDireccion->telefono,

                ];
               }



          }

      }else{

        return $this->renderAjax('bienasegurado', [
                  'modelDireccion' => $modelDireccion,
                  'depend'=>$_GET['depend'],
        ]);

     }

}


    public function actionCargaFamiliarUpdate()
    {

  		    //$model= new Persona();
  		    $modelCliente = new Cliente();
          $modelCargaFamiliar= new CargaFamiliar();
  	    	$asegurado_titular=$_GET['depend'];
			    $tipo_persona=$_GET['tipo_persona'];
		    	$persona=$this->getPersona($_GET['id']);
		    	$model = $this->findModel($persona->id_persona);



  	    if ($model->load(Yii::$app->request->post())) {

  						    //guardando carga familiar
  							$modelCargaFamiliar->id_titular=$asegurado_titular;
  							$modelCargaFamiliar->id_beneficiario=$_GET['id'];
  							$modelCargaFamiliar->parentesco=$_POST['Persona']['parentesco'];
  							$modelCargaFamiliar->fecha_registro=date('Y-m-d H:i:s');
                $modelCargaFamiliar->id_user_registro =Yii::$app->user->identity->id;

  							 if ($flag = $modelCargaFamiliar->save(false)) {

  							  	 Yii::$app->response->format = Response::FORMAT_JSON;
  								  return [
  									  'message' => 'Exito',
  									  'id_cliente'=>$_GET['id'],
  									  'nombre_completo'=>$model->nombre.' '.$model->apellido,
  									  'identificacion'=>$model->identificacion,
  									  'parentesco'=>$modelCargaFamiliar->parentesco,
  									  'anios'=>$modelCargaFamiliar->fecha_registro-$model->fecha_nacimiento,
                                      'tipo_persona'=>$tipo_persona

  									];

  						     }else{
  							       var_dump($modelCargaFamiliar->getErrors());
  						     }




  		}else{

  			return $this->renderAjax('create', [
          				'model' => $model,
          				'depend'=>$asegurado_titular,
                        'tipo_persona'=>$tipo_persona

  			]);

  		}

  }

    /**
     * Creates a new persona model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($submit = false)
    {

		$model = new Persona(['scenario' => 'cliente']);
		$documentos= new Documentos();

        $modelCliente = new Cliente();
        $modelDireccion = [new Direccion];
        $tipo_persona=$_GET['tipo_persona'];
        $tipo_poliza=$_GET['tipo_poliza'];

        $modelDireccion = Model::createMultiple(Direccion::classname());
        Model::loadMultiple($modelDireccion, Yii::$app->request->post());

        $modelCargaFamiliar = Model::createMultiple(CargaFamiliar::classname());
        Model::loadMultiple($modelCargaFamiliar, Yii::$app->request->post());


         // ajax validation
         if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
             Yii::$app->response->format = Response::FORMAT_JSON;
             return ArrayHelper::merge(
                 ActiveForm::validateMultiple($modelDireccion),
                 ActiveForm::validateMultiple($modelCargaFamiliar),
                 ActiveForm::validate($model)
             );
         }

        if ($model->load(Yii::$app->request->post())) {


           // validate all models
           $valid = $model->validate();
           $valid = Model::validateMultiple($modelDireccion) && $valid;
           //$valid = Model::validateMultiple($modelCargaFamiliar) && $valid2 ;

           if ($valid) {
               $transaction = \Yii::$app->db->beginTransaction();
               try {

                      //$model->fecha_nacimiento=date('Y-m-d',strtotime($model->fecha_nacimiento));
                      $model->fecha_nacimiento=$model->fecha_nacimiento;
                      //REGISTRANDO AL TITULAR POR EL MODELO PERSONA
                   if ($flag = $model->save(false)) {
                          //SETEAR LOS VALORES EN EL MODELO DE CLIENTE ANTES DE GUARDAR
                           $modelCliente->id_persona = $model->id_persona;
                           $modelCliente->fecha_registro=date('Y-m-d H:i:s');
                           $modelCliente->id_user_registro =Yii::$app->user->identity->id;

                             if (! ($flag = $modelCliente->save(false))) {////REGISTRANDO AL TITULAR POR EL MODELO CLIENTE
                                 $transaction->rollBack();
                                 //break;
                             }

                            //CARGANDOLE LAS DIRECCIONES A LA PERSONA
                            foreach ($modelDireccion as $modelDireccion) {
                                       $modelDireccion->id_persona = $model->id_persona;
                                if (! ($flag = $modelDireccion->save(false))) {
                                        $transaction->rollBack();
                                        //break;
                                }
                            }

                     }    //REGISTRANDO AL TITULAR POR EL MODELO PERSONA


                        foreach ($modelCargaFamiliar as $cargaFamiliar) { //RECORRIENDO LOS BENEFICIARIOS

                                  $modelMult = new Persona;
                                  //SETEANDO LOS VALORES EN EL MODELO DE PERSONA
                                  $modelMult->identificacion = $cargaFamiliar->identificacion;
                                  $modelMult->nombre = $cargaFamiliar->nombre;
                                  $modelMult->segundo_nombre = $cargaFamiliar->segundo_nombre;
                                  $modelMult->apellido = $cargaFamiliar->apellido;
                                  $modelMult->segundo_apellido = $cargaFamiliar->segundo_apellido;
                                  $modelMult->sexo = $cargaFamiliar->sexo;
                                  $modelMult->fecha_nacimiento=date('Y-m-d',strtotime($cargaFamiliar->fecha_nacimiento));//formateo de fecha antes de guardar
                                  $modelMult->tipo="V";

                               if (! ($flag = $modelMult->save(false))) {////REGISTRANDO A LOS BENEFICIARIOS PÓR EL MODULO DE PERSONA
                                       $transaction->rollBack();
                                       //break;
                               }else{

                                          $modelClienteMult = new Cliente;
                                          $modelClienteMult->id_persona = $modelMult->id_persona;
                                          $modelClienteMult->fecha_registro=date('Y-m-d H:i:s');
                                          $modelClienteMult->id_user_registro =Yii::$app->user->identity->id;

                                            if (! ($flag = $modelClienteMult->save(false))) {////REGISTRANDO AL TITULAR POR EL MODELO CLIENTE
                                                     $transaction->rollBack();
                                                //break;
                                            }else{
                                                   //SETANDO LOS VALORES DE LOS BENEFICIARIOS EN EL MODELO CARGA FAMILIAR
                                                   $modelCargaFamiliar= new CargaFamiliar;
                                                   $modelCargaFamiliar->id_titular = $modelCliente->id_cliente;  //CARGANDO EL TITULAR
                                                   $modelCargaFamiliar->id_beneficiario = $modelClienteMult->id_cliente;//CARGANDO LOS BENEFICIARIOS
                                                   $modelCargaFamiliar->parentesco=$cargaFamiliar->parentesco;
                                                   $modelCargaFamiliar->fecha_registro=date('Y-m-d H:i:s');
                                                   $modelCargaFamiliar->id_user_registro =Yii::$app->user->identity->id;

                                                  if (! ($flag = $modelCargaFamiliar->save(false))) {
                                                      $transaction->rollBack();
                                                    //break;
                                                  }

                                            }//FIN ELSE

                                  }//FIN ELSE

                      } //RECORRIENDO LOS BENEFICIARIOS

                   if ($flag){
                       $transaction->commit();
                         //$model->refresh();
                          Yii::$app->response->format = Response::FORMAT_JSON;
                          return [
                              'message' => 'Exito',
                              'id_cliente'=>$modelCliente->id_cliente,
                              'nombre_completo'=>$model->nombre.' '.$model->apellido,
                              'identificacion'=>$model->identificacion,
                              'identificacion2'=>(!empty($model->identificacion2))?$model->identificacion2:'N/A',
                              'anios'=>$modelCliente->fecha_registro-$model->fecha_nacimiento,
                              'tipo_persona'=>$tipo_persona,
                              'tipo_poliza'=>$tipo_poliza,
                              'parentesco'=>'N/A',
                            ];

                       //return $this->redirect(['view', 'id' => $model->id_persona]);
                   }else{

                     Yii::$app->response->format = Response::FORMAT_JSON;
                          return ArrayHelper::merge(
                             ActiveForm::validateMultiple($modelDireccion),
                             ActiveForm::validateMultiple($modelCargaFamiliar),
                             ActiveForm::validate($model)
                         );


                   }

               } catch (Exception $e) {
                  $transaction->rollBack();
               }

           } //if ($valid)

     }else{

         return $this->renderAjax('create', [
             'model' => $model,
             'modelDireccion' => (empty($modelDireccion)) ? [new Direccion] : $modelDireccion,
             'modelCliente' => (empty($modelCliente)) ? [new Cliente] : $modelCliente,
             'modelCargaFamiliar' => (empty($modelCargaFamiliar)) ? [new CargaFamiliar] : $modelCargaFamiliar,
             'tipo_persona'=>$tipo_persona,
             'tipo_poliza'=>$tipo_poliza,
             'documentos'=>$documentos,


         ]);
     } //if ($model->load(Yii::$app->request->post()))

    }


    public function actionUploadFile(){

      //print_r($_POST);

      $documentos= new Documentos();
      $model = new Persona(['scenario' => 'cliente']);
      $ruta=Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/uploads/cliente/';

      $doc_files= UploadedFile::getInstances($documentos, 'doc_files');
      $documentos->doc_files=null;
      $aleatorio=rand(4,4);

      if (empty($doc_files)) {
            echo json_encode(['successful'=>'Listo']);
            //return true;
      }

      if($model->load(Yii::$app->request->post())){

          if(empty($model->identificacion)){
            echo json_encode(['error'=>'Debe colocar el numero de cedula o rif para subir el documento']);
          }else{

            foreach ($doc_files as $file) {

                   $archivo=$ruta.$model->identificacion.'_'.$file->name;
                   $documentos->extension=$file->extension;
                   $documentos->nombre_archivo=$model->identificacion.'_'.$file->name;
                   $documentos->ruta=$model->identificacion.'_'.$file->name;
                   $documentos->categoria='persona';
                   $documentos->identificacion=$model->identificacion;

                     if(!$documentos->save(false)){
                           echo json_encode(['error'=>$files]);
                     }else{
                       if(!$file->saveAs($archivo)){
                            echo json_encode(['error'=>$files]);
                      }else{
                           echo json_encode(['successful'=>$file]);

                      }//fin else $file->saveAs($archivo)
                   }//fin else $documentos->save(false)
             }//fin foreach

          }//fin else

      }//if($model->load(Yii::$app->request->post()))

     }

    /**
     * Updates an existing persona model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function getCargaFamiliars($id)
    {
      return CargaFamiliarView::find()->where(['id_titular' => $id])->all();
    }

    public function getCliente($id)
    {

          return Cliente::find()->joinWith(['idPersona'])
                 ->select(["id_cliente"])
                 ->where("cliente.estatus = :status AND cliente.id_persona = :id_persona",[':status' => 1,':id_persona' => $id])->one();

    }

    public function getPersona($id)
    {

          return Cliente::find()->joinWith(['idPersona'])
                 ->select(["persona.id_persona"])
                 ->where("cliente.estatus = :status AND cliente.id_cliente = :id_cliente",[':status' => 1,':id_cliente' => $id])->one();

    }

    public function actionUpdate($id)
    {

	       $model = $this->findModel($id);
		   $modelDireccion = $model->direccions;
		   $modelCliente=$this->getCliente($id);
		   $modelCargaFamiliar=$this->getCargaFamiliars($modelCliente->id_cliente);

           $documentos= new Documentos();

        if ($model->load(Yii::$app->request->post())) {

            $oldIDs = ArrayHelper::map($modelDireccion, 'id', 'id');
            $modelDireccion = Model::createMultiple(Direccion::classname(), $modelDireccion);
            Model::loadMultiple($modelDireccion, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelDireccion, 'id', 'id')));


            $oldIDs_CF = ArrayHelper::map($modelCargaFamiliar, 'id_carga_familiar', 'id_carga_familiar');
            $modelCargaFamiliar = Model::createMultiple(CargaFamiliar::classname());
            Model::loadMultiple($modelCargaFamiliar, Yii::$app->request->post());

            $deletedID_CF = array_diff($oldIDs_CF, array_filter(ArrayHelper::map($modelCargaFamiliar, 'id_carga_familiar', 'id_carga_familiar')));

            // ajax validation
            /*if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelDireccion),
                    //ActiveForm::validateMultiple($modelCargaFamiliar),
                    ActiveForm::validate($model)
                );


            }*/

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelDireccion) && $valid;

            if ($valid) {

                $transaction = \Yii::$app->db->beginTransaction();
                try {

					   //$model->fecha_nacimiento=date('d-m-Y',strtotime($model->fecha_nacimiento));
					   $model->fecha_nacimiento=$model->fecha_nacimiento;
					   
                    if ($flag = $model->save(false)) {

                        if (! empty($deletedIDs) && ! empty($deletedIDs) ) {
                            Direccion::deleteAll(['id' => $deletedIDs]);
                            //CargaFamiliar::deleteAll(['id_carga_familiar' => $deletedID_CF]);
                        }

                        foreach ($modelDireccion as $modelDireccion) {
                            $modelDireccion->id_persona = $model->id_persona;
                            if (!($flag = $modelDireccion->save(false))) {

                                $transaction->rollBack();
                                var_dump($modelDireccion->getErrors());
                                //break;
                            }
                        }

                    }else{
                         var_dump($model->getErrors());
                    }
                    if ($flag) {
                        $transaction->commit();
                               Yii::$app->response->format = Response::FORMAT_JSON;
                               return [
                                   'message' => 'Exito',
                                   'id_cliente'=>$modelCliente->id_cliente,
                                   'nombre_completo'=>$model->nombre.' '.$model->apellido,
                                   'identificacion'=>$model->identificacion,
                                   'identificacion2'=>(!empty($model->identificacion2))?$model->identificacion2:'N/A',
                                   'anios'=>$modelCliente->fecha_registro-$model->fecha_nacimiento,
                                   'tipo_persona'=>'cliente',
                                   'parentesco'=>'N/A',
                                 ];


                        //return $this->redirect(['view', 'id' => $model->id_persona]);
                    }else{
                       var_dump($model->getErrors());
                    }

                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->renderAjax('update',
                [
                    'model' => $model,
                    'modelDireccion' => (empty($modelDireccion)) ? [new Direccion] : $modelDireccion,
                    'modelCliente' => (empty($modelCliente)) ? [new Cliente] : $modelCliente,
                    'modelCargaFamiliar' => (empty($modelCargaFamiliar)) ? [new CargaFamiliar] : $modelCargaFamiliar,
                    'documentos'=>$documentos,
                ]);
    }


    /**
     * Deletes an existing persona model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**/

    public function actionCargarContrante(){

      return ArrayHelper::map( Cliente::find()
                                   ->joinwith('idPersona')
                                   ->where("cliente.estatus = :status", [':status' => 1])
                                   ->select("(nombre||' '||segundo_nombre||' '||apellido|| ' ' ||segundo_apellido || ' ' || identificacion) AS nombre_completo ,id_cliente")
                                   ->all(),'id_cliente', 'nombre_completo');
    }

    /**
     * Finds the persona model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return persona the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {


        if (($model = Persona::findOne($id)) !== null) {
             $model->scenario = 'cliente';
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
