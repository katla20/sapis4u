<?php

namespace backend\controllers;

use Yii;


use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\base\Model;
use yii\web\Response;
use yii\widgets\Pjax;
use backend\models\Aseguradoras;
use backend\models\Cliente;
use backend\models\Persona;
use backend\models\Recibo;
use backend\models\FormaPago;
use backend\models\Banco;
use backend\models\Pago;
use backend\models\PagoRecibo;
use backend\models\search\PagoSearch;
use backend\models\Poliza;
use yii\data\ActiveDataProvider;
use kartik\money\MaskMoney;
use kartik\widgets\DatePicker;
use yii\widgets\MaskedInput;
/**
 * PolizaController implements the CRUD actions for Poliza model.
 */
class PagoController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pago models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PagoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        /*$query= Recibo::find()->innerJoin('poliza', 'poliza.id_poliza=recibo.id_poliza')
                               ->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
                               ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
                               ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
                               ->innerJoin('ramos', 'ramos.id_ramo = poliza.id_ramo')
                              ->where("poliza.estatus = :status AND recibo.fecha_vigenciahasta > CURRENT_DATE AND recibo.estatus_recibo='Pendiente'",
                                [':status' => 1])
                       ->select("numero_poliza,poliza.id_poliza,nro_recibo,id_recibo,(persona.nombre ||' '||segundo_nombre||' '||apellido|| ' ' ||segundo_apellido) as nombre_completo,
                                 persona.identificacion,aseguradoras.nombre AS aseguradora,
                                 (poliza.fecha_vigenciadesde||'  -  '||poliza.fecha_vigenciahasta) AS fecha_vigenciahasta,
                                 ramos.nombre AS ramo");

        $dataProvider = new ActiveDataProvider([
             'query' => $query,
      			 'pagination' => [
      				'pageSize' => 12,
      			  ],
      			  'sort' =>
      			   ['attributes' =>
      				   [
        				  'ramo',
        					'aseguradora',
        					'nombre_completo',
        					'numero_poliza',
        					'fecha_vigenciahasta'
      				   ]
      			   ],

         ]);

        $searchModel=$dataProvider;*/

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionDocumentacion()
    {
        return $this->render('documentacion');
    }

  public function actionCreate()
    {
        $model = new Pago();
        $id_recibo=$_GET['id_recibo'];
      	$prima=Recibo::findReciboPrima($id_recibo);
      	
      	 

    if ($model->load(Yii::$app->request->post())){//validar post
    
           $modalidad=$_POST['id_modalidad_pago'];
           $model->id_modalidad_pago=$modalidad;
           $model->fecha_registro=date('Y-m-d H:i:s');
           $model->id_user_registro=Yii::$app->user->identity->id;
         

            switch ($modalidad) {

               case 1://CONTADO
    
                  $model->monto=str_replace(",",".",str_replace(".","",$model->monto));
                  $model->estatus_pago='Pagado';
                  $model->fecha_pago=date('Y-m-d');
                  $model->fecha_actualizacion=date('Y-m-d H:i:s');
                  $model->id_user_actualizacion=Yii::$app->user->identity->id;
       
                  if(!$model->save(false)){
                    var_dump($model->getErrors());
                  }else{

                    $pago_recibo= new PagoRecibo();
                    $pago_recibo->id_pago=$model->id_pago;
                    $pago_recibo->id_recibo=$id_recibo;
                    

                    if(!$pago_recibo->save(false)){

                        var_dump($pago_recibo->getErrors());
                    }else{
                      //actualizar el recibo a pagado
                        $recibo = Recibo::find()->where(['id_recibo' => $pago_recibo->id_recibo])->one();
                        $recibo->estatus_recibo='Pagado';
                        
                        $recibo->fecha_actualizacion=date('Y-m-d H:i:s');
                        $recibo->id_user_actualizacion=Yii::$app->user->identity->id;

                        if(!$recibo->save(false)){
                          var_dump($recibo->getErrors());
                        }else{
                          //return $this->redirect(['view', 'id' => $model->id_pago]);
						                 return $this->redirect(['index']);
                        }
                    }//FIN ELSE

                  }//FIN ELSE

                break;
              case 2://CREDITO
                # code...
              //  echo 'CREDITOOO';

                if($_POST['pago-contador'] > 0){//si existen pagos entrar

                    //  $transaction = \Yii::$app->db->beginTransaction();

                    //  try {

                          $mnto=Yii::$app->request->post('_mnto');
                          $fchp=Yii::$app->request->post('_fchv');
                          
                      

                             foreach ($fchp as $clave => $valor){

                               echo "<pre>$clave-- $valor</pre>";

                                $model = new Pago();
  
                                $model->monto=str_replace(",", ".",$mnto[$clave]);
                                $model->fecha_vencimiento=$fchp[$clave];
                                $model->total_cuotas=$_POST['pago-contador'];
                                $model->cuota=$clave+1;
                                $model->id_modalidad_pago=$modalidad;
                                $model->fecha_registro=date('Y-m-d H:i:s');
                                $model->id_user_registro=Yii::$app->user->identity->id;
        
                                 if (!$model->save(true)) {
                                        //$transaction->rollBack();
                                         var_dump($model->getErrors());
                                      
                                 }else{
                                     
           
                                     $pago_recibo= new PagoRecibo();
                                     
                                     $pago_recibo->id_pago=$model->id_pago;
                                     $pago_recibo->id_recibo=$id_recibo;
                                   

                                     if (!$pago_recibo->save(true)) {
                                              //$transaction->rollBack();
                                               var_dump($pago_recibo->getErrors());
                                      }else{
                                          
                                          return $this->redirect(['index']);
                                          
                                      }
                                }

                            }//FIN foreach ($fchp as $clave => $valor)


                               //return $this->redirect(['view', 'id' => $pago_recibo->id_recibo]);
                               


                        /*} catch (Exception $e) {
                           $transaction->rollBack();
                        }*/
                    }//fin if
                break;
              case 3://FINANCIADO
                # code...
                if($_POST['pago-contador'] > 0){//si existen pagos entrar

                    //  $transaction = \Yii::$app->db->beginTransaction();

                    //  try {

                          $mnto=Yii::$app->request->post('_mnto');
                          $fchp=Yii::$app->request->post('_fchv');
                          
                         

                             foreach ($fchp as $clave => $valor){

                                  $model = new Pago();
                               
                             
                                  $model->monto=str_replace(",", ".",$mnto[$clave]);
                                  $model->fecha_vencimiento=$fchp[$clave];
                                  $model->total_cuotas=$_POST['pago-contador'];
                                  $model->cuota=$clave;
                                  $model->id_modalidad_pago=$modalidad;
                                  $model->fecha_registro=date('Y-m-d H:i:s');
                                  $model->id_user_registro=Yii::$app->user->identity->id;

                                if (!$model->save()) {
                                         var_dump($model->getErrors());
                                }else{

                                      $pago_recibo= new PagoRecibo();
                                      $pago_recibo->id_pago=$model->id_pago;
                                      $pago_recibo->id_recibo=$id_recibo;

                                      if ($pago_recibo->save()) {
                                        
                                           echo 'guardo pago'.$clave;
                                           //return $this->redirect(['index']);
                                      }else{
                                          var_dump($pago_recibo->getErrors());
                                      }
                                }
                            }//FIN foreach ($fchp as $clave => $valor)

                             
                        /*} catch (Exception $e) {
                           $transaction->rollBack();
                        }*/
                    }//fin if
                break;
            }

    }else{

    return $this->render('create', [
        'model' => $model,
		    'prima'=>$prima,
    ]);
  }


}

public function actionBuscarCliente()
{

          Yii::$app->response->format = Response::FORMAT_JSON;
            return Cliente::find()->joinWith(['idPersona'])
           ->select(["*","CONCAT(nombre, ' ', apellido) AS nombre_completo"])
           ->where("cliente.estatus = :status AND cliente.id_cliente = :id_cliente",
           [':status' => 1,':id_cliente' => Yii::$app->request->post('id')])->asArray()->all();
           //->groupBy('seller_id')
           //->orderBy(['cnt' => 'DESC'])

}

public static function getDatosPago($recibo,$modalidad="")
{

        return Poliza::find()->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza')
                              ->innerJoin('pago_recibo', 'pago_recibo.id_recibo = recibo.id_recibo')
                              ->innerJoin('pago', 'pago.id_pago = pago_recibo.id_pago')
                              ->where("recibo.id_recibo=:recibo AND poliza.estatus = :status AND recibo.estatus = :status AND pago_recibo.estatus = :status",
                               [':status' => 1,':recibo'=>$recibo])
                            ->select("recibo.id_recibo,recibo.estatus_recibo,pago.id_modalidad_pago")->asArray()->all();

}

public function actionAplicarPago()
{


	    $connection = \Yii::$app->db;

		//calcular el monto de la prima del recibo para  calcular validar cuando se pase del monto en los pagos

		$model = Pago::find()->where(['id_pago' => $_GET['id_pago']])->one();

		$pago_recibo = PagoRecibo::find()->where(['id_pago' => $_GET['id_pago']])->one();

		$res=array();

            $sql = "select SUM(cd.prima) AS prima_total , SUM (pago.monto) AS monto_pagos
                        FROM pago
                        INNER JOIN pago_recibo USING (id_pago)
                        INNER JOIN recibo USING (id_recibo)
						            INNER JOIN certificado USING (id_recibo)
						             INNER JOIN certificado_detalle AS cd USING (id_certificado)
                    WHERE recibo.id_recibo=:id AND pago.estatus_pago='Pagado'";

            $command = $connection->createCommand($sql);
            $command->bindValue(":id", $pago_recibo->id_recibo);
            $result = $command->queryOne();


		$model->fecha_actualizacion=date('Y-m-d H:i:s');
    $model->id_user_actualizacion=Yii::$app->user->identity->id;
    //$model->monto=str_replace(",",".",str_replace(".","",$_GET['monto']));
		$model->estatus_pago="Pagado";
		$model->fecha_pago=$_GET['fecha_pago'];
		$model->observacion=$_GET['observacion'];

    if($model->save(false)){


			/*if($model){ //si es la ultima cuota actualizar el recibo y retornar bandera para que refresque el grid

					$recibo = Recibo::find()->where(['id_recibo' => $pago_recibo->id_recibo])->one();
					$recibo->estatus_recibo='Pagado';
					$recibo->fecha_actualizacion=date('Y-m-d H:i:s');
					$recibo->id_user_actualizacion=Yii::$app->user->identity->id;
				   // $recibo->save(false);

			}*/

			$res=array();

            $sql = "select *
                        FROM pago
                    WHERE pago.id_pago=:id";

            $command = $connection->createCommand($sql);
            $command->bindValue(":id", $model->id_pago);
            $result = $command->queryAll();

            $out="";

            if (count($result)!= 0){

                foreach($result as $row){

                       $_mnto=MaskMoney::widget([
           							 'name' => '_mnto['.$row['id_pago'].']',
           							 'id'=>'_mnto-'.$row['id_pago'],
                         'disabled' => true,
                         'value' => $row['monto'],
           						]);

                        $_fchp=MaskedInput::widget([
                                 'name' => '_fchp['.$row['id_pago'].']',
                                 'id'=>'_fchp-'.$row['id_pago'],
                                 'value' => $row['fecha_pago'],
                                 'clientOptions' => ['alias' =>  'dd/mm/yyyy'],
                                 'options'=>['size' =>  '10','disabled' => true]
                        ]);

                      $_obs='<input type="text" id="_obs" name="_obs['.$row['id_pago'].']" cuota="'.$row['cuota'].'"  value="'.$row['observacion'].'" size="70" disabled="disabled">';

            					$tdOp=($row['estatus_pago']=='Pendiente')?'<a id="btn-'.$row['id_pago'].'" class="boton-aplicar" href="#"><i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i></a>':'<i class="icon-ok fa-lg" style="color:#4CAF50;" aria-hidden="true"></i>';

                                                        $out.='<td>'.$row['cuota'].'</td>';
                                                        $out.='<td>'.$row['fecha_vencimiento'].'</td>';
                                                        $out.='<td>'.$row['monto'].'</td>';
                                                        $out.='<td>'.$_fchp.'</td>';
            											                      $out.='<td>'.$_obs.'</td>';
                                                        $out.='<td>'.$tdOp.'</td>';
                }

                echo $out;
            }
		}else{
      var_dump($model->getErrors());
    }

}



public static function getDatos($id)
{

        $res=array();
            $connection = \Yii::$app->db;
            $sql = "select
            p.numero_poliza,
            (pe.nombre ||' '||apellido) as nombre_completo,
            pe.identificacion,
            ra.nombre AS ramo,
            a.nombre AS aseguradora,
	        SUM(dc.prima) AS prima
            from poliza as p
            inner join recibo as r on (r.id_poliza=p.id_poliza)
            inner join cliente as c on (c.id_cliente=p.id_contratante)
            inner join persona as pe on (pe.id_persona=c.id_persona)
            inner join aseguradoras a on (a.id_aseguradora=p.id_aseguradora)
            inner join certificado as cer on (cer.id_recibo=r.id_recibo)
	        inner join certificado_detalle as dc on (dc.id_certificado=cer.id_certificado)
            inner join ramos as ra on (ra.id_ramo=p.id_ramo)
            WHERE r.id_recibo=:id
            GROUP BY 1,2,3,4,5";

            $command = $connection->createCommand($sql);
            $command->bindValue(":id", $id);
            $result = $command->queryAll();
            $out="";
            if (count($result)!= 0){

                foreach($result as $row){

                  $out.='<table class="table"><thead class="thead-default"><tr>';
                  $out.='<th>Aseguradora</th><th>Ramo</th><th>Poliza</th><th>Contratante</th><th>Prima Recibo</th></tr></thead>';
                  $out.='<tbody><tr><th scope="row">'.$row['aseguradora'].'</th><td>'.$row['ramo'].'</td><td>'.$row['numero_poliza'].'</td><td>'.$row['nombre_completo'].'</td><td>'.$row['prima'].'</td></tr></tbody></table>';

                }

                echo $out;
            }
}

public static function getPagos($id)
{
            $connection = \Yii::$app->db;
            $sql = "select *
                        FROM pago
                        INNER JOIN pago_recibo USING (id_pago)
                        INNER JOIN recibo USING (id_recibo)
                        INNER JOIN poliza USING (id_poliza)
                    WHERE recibo.id_recibo=:id";

            $command = $connection->createCommand($sql);
            $command->bindValue(":id", $id);
            $result = $command->queryAll();

            $out="";
            if (count($result)!= 0){

              $out.='<table class="table" border="1">
                        <theader>
                        <tr>
                           <th>Cuota</th><th>Fecha Vencimiento</th><th>Monto</th><th>Fecha de Pago</th><th>Observacion</th><th>Estatus</th>
                        </tr>
                        </theader>
                    <tbody>';
                foreach($result as $row){

          					if($row['estatus_pago']=='Pagado'){

                          $disabled=true;
                          $hiddenPago="";
                          $tdOp='<i class="icon-ok fa-lg" aria-hidden="true" style="color:#4CAF50;" title="Pago Aplicado" rel="tooltip"></i>';
                          $_obs='<input type="text" id="_obs" name="_obs['.$row['id_pago'].']" cuota="'.$row['cuota'].'" value="'.$row['observacion'].'" size="70" disabled="disabled">';

          					}else{

                           $tdOp='<a id="btn-'.$row['id_pago'].'" class="boton-aplicar" href="#"><i class="fa fa-pencil-square-o fa-lg" aria-hidden="true" title="Aplicar Pago" rel="tooltip"></i></a>';
                           $disabled=false;
                           $hiddenPago='<input type="hidden" id="_pgo" name="_pgo['.$row['id_pago'].']" cuota="'.$row['cuota'].'" value="'.$row['id_pago'].'" data-url="'.Url::to(['pago/aplicar-pago'], true).'">';
                           $_obs='<input type="text" id="_obs" name="_obs['.$row['id_pago'].']" cuota="'.$row['cuota'].'" value="'.$row['observacion'].'" size="70" >';
                    }



                      $_mnto=MaskMoney::widget([
          							'name' => '_mnto['.$row['id_pago'].']',
          							'id'=>'_mnto-'.$row['id_pago'],
                        'disabled' => $disabled,
                        'value' => $row['monto'],
          						]);

                       $_fchp=MaskedInput::widget([
                                'name' => '_fchp['.$row['id_pago'].']',
                                'id'=>'_fchp-'.$row['id_pago'],
                                'value' => $row['fecha_pago'],
                                'clientOptions' => ['alias' =>  'dd/mm/yyyy'],
                                'options'=>['size' =>  '10','disabled' => $disabled]
                       ]);


                        $out.='<tr id="tr-'.$row['id_pago'].'"><td>'.$row['cuota'].$hiddenPago.'</td>';
                        $out.='<td>'.$row['fecha_vencimiento'].'</td>';
                        $out.='<td>'.$row['monto'].'</td>';
                        $out.='<td>'.$_fchp.'</td>';
                        $out.='<td>'.$_obs.'</td>';
                        $out.='<td>'.$tdOp.'</td></tr>';


                  }

             $out.='</tbody></table>';

                echo $out;
            }
}



    /**
     * Updates an existing Pago model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_poliza]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Poliza model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Poliza model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Poliza the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pago::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
