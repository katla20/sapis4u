<?php

namespace backend\controllers;

use Yii;
use backend\models\Poliza;
use backend\models\search\PolizaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\base\Model;
use yii\web\Response;
use yii\widgets\Pjax;
use backend\models\Cliente;
use backend\models\Intermediario;
use backend\models\Persona;
use backend\models\Coberturas;
use backend\models\CoberturasView;
use backend\models\Aseguradoras;
use backend\models\InterAsegRamo;
use backend\models\InterAsegProducto;
use backend\models\Productos;
use backend\models\Recibo;
use backend\models\Certificado;
use backend\models\CertificadoDetalle;
use backend\models\InterProdCob;
use backend\models\CertificadoAsegurados;
use backend\models\CertificadoBeneficiarios;
use backend\models\CargaFamiliarView;


/**
 * PolizaController implements the CRUD actions for Poliza model.
 */
class RenovacionInternacionalController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Poliza models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PolizaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,2,1);//ramo=salud, tipo=individual

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Poliza model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
             'model' => $this->datos($id),
        ]);
    }

    /**
     * Creates a new Poliza model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     public function actionUpdate($id)
     {

        $model= Poliza::datosRenovacionSalud($id);

        $recibo = new Recibo();
        $certificado= new Certificado();
        $certificadoDetalle= new CertificadoDetalle();
        $poliza = Poliza::findOne($id);
        $post=Yii::$app->request->post();

         if($model)
         {

            if ($model->load(Yii::$app->request->post())){//validar post
            
             $transaction = Yii::$app->db->beginTransaction();
    
        				$model->id_contratante=$model->id_contratante_hidden;
        				$model->id_tipo=1;
        				$model->fecha_registro=date('Y-m-d H:i:s');
        				$model->id_user_registro=isset(Yii::$app->user->identity->id)?Yii::$app->user->identity->id:1;
        				$model->id_ramo=2;
    
                  if($bandera=$model->save(false)){//validar que guarde la poliza
    
        			   //exit(var_dump($model));
        				$recibo->nro_recibo=$model->nro_recibo;
        				$recibo->id_poliza=$model->id_poliza;
        				$recibo->tipo_recibo=$model->tipo_recibo;
        				$recibo->fecha_registro=$model->fecha_registro;
        				$recibo->id_user_registro=$model->id_user_registro;
        				$recibo->fecha_vigenciadesde=$model->fecha_vigenciadesde;
        				$recibo->fecha_vigenciahasta=$model->fecha_vigenciahasta;
        				$recibo->id_vendedor=$post['Poliza']['id_vendedor'];
        				$recibo->id_productor=2;//OJO LLENARLO CON EL ID DEL USUARIO DEL ASESOR
        				$recibo->comision_total=$post['Poliza']['comision_total'];
        				$recibo->comision_vendedor=$post['Poliza']['comision_vendedor'];
    
                    if($bandera=$recibo->save(false)){//validar que guarde el recibo
    
                          //validar certificados
                          $certificado->id_recibo=$recibo->id_recibo;
                          $certificado->nro_certificado=$model->nro_certificado;
                          $certificado->fecha_registro=date('Y-m-d H:i:s');
                          $certificado->id_user_registro=Yii::$app->user->identity->id;
                          $certificado->id_producto=$model->id_producto;
                          $certificado->id_titular=$post['Poliza']['id_contratante_hidden'];
        				  $certificado->deducible=$post['Poliza']['deducible'];
    
    
                          if($bandera=$certificado->save(false)){
    
    
                  					  $suma=Yii::$app->request->post('suma');
                  					  $tasa=Yii::$app->request->post('tasa');
                  					  $prima=Yii::$app->request->post('prima');
                                      $cobBox=Yii::$app->request->post('cobBox');
    
    
                                 //Yii::$app->session->setFlash('success','You have updated your profile.');
    
                                   //REGISTRANDO ASEGURADOS AL CERTIFICADO
                                   foreach ($_POST["asegurado"] as $clave => $valor){
                                      $certificadoAsegurados= new certificadoAsegurados();
                                      $certificadoAsegurados->id_certificado=$certificado->id_certificado;
                                      $certificadoAsegurados->id_cliente=$clave;
                                      if(!$bandera=$certificadoAsegurados->save(false)){
                        					var_dump($certificadoAsegurados->getErrors());
                        					$transaction->rollBack();
                        			  }
    
                                   }
    
                                     //var_dump($_POST);exit();
    
                                      $porcentaje=Yii::$app->request->post('porcentaje');
                                      
                                      if(isset($_POST["beneficiario"])){
                                          
                                           //REGISTRANDO BENEFICIARIOS AL CERTIFICADO
                                            foreach ($_POST["beneficiario"] as $clave => $valor){
                                            $certificadoBeneficiarios= new certificadoBeneficiarios();
                                            $certificadoBeneficiarios->id_certificado=$certificado->id_certificado;
                                            $certificadoBeneficiarios->id_cliente=$clave;
                                            $certificadoBeneficiarios->beneficiario_porcentaje=$porcentaje[$clave];
        
                                            if(!$bandera=$certificadoBeneficiarios->save(false)){
                              					     var_dump($certificadoBeneficiarios->getErrors());
                              					     $transaction->rollBack();
                              				      }
    
                                         }
               
                                      }
    
                                   foreach ($suma as $clave => $valor){
    
                                         $certificadoDetalle= new CertificadoDetalle();
                                         $certificadoDetalle->id_certificado=$certificado->id_certificado;
                                         $certificadoDetalle->suma_asegurada=str_replace(",", ".",str_replace(".", "",$suma[$clave]));
                                         $certificadoDetalle->prima=str_replace(",", ".",str_replace(".", "",$prima[$clave]));
                                         $certificadoDetalle->comision=0;
                                         $certificadoDetalle->fecha_registro=$model->fecha_registro;
                                         $certificadoDetalle->id_user_registro=$model->id_user_registro;
                                         $certificadoDetalle->id_cobertura=$clave;
    
                                       if(!$bandera=$certificadoDetalle->save(false)){
                                         var_dump($certificadoDetalle->getErrors());
                                         $transaction->rollBack();
                                       }
                                   }
    
              					  if($bandera){
              					       $transaction->commit();
              					       return $this->redirect(['poliza-internacional/view', 'id' => $model->id_poliza]);
              					  }else{
              						       var_dump($recibo->getErrors());
              						       $transaction->rollBack();
              					  }
    
                      }else{
                        var_dump($recibo->getErrors());
                        $transaction->rollBack();
                      }
    
                }else{
    
                  var_dump($recibo->getErrors());
                  $transaction->rollBack();
    
                }
              }
    
            } else {
                 // var_dump($recibo->getErrors());
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
            
      }else{
          
         echo 'Esta Poliza presenta falla en sus datos recomendamos que vuelva a generarla';
      } 
}

public function actionBuscarCoberturas()
{

   $filtroaux="AND coberturas.id_cobertura <> 5";
   if(Yii::$app->request->post('tipo')==0){
   $filtroaux="AND coberturas.id_cobertura = 5";
   }


    $QUERY="SELECT
        id_cobertura,
        cobertura,
        sum(prima) As prima,
        sum(suma_asegurada) AS suma_asegurada,
        sum(checado) AS checado
        FROM (
          Select DISTINCT coberturas.id_cobertura,coberturas.nombre AS cobertura, 0 as prima,0 AS suma_asegurada,0 AS checado
          from recibo
          INNER JOIN certificado ON certificado.id_recibo = recibo.id_recibo
          INNER JOIN productos ON productos.id_producto = certificado.id_producto
          INNER JOIN inter_prod_cob ON inter_prod_cob.id_producto= certificado.id_producto
          INNER JOIN coberturas ON coberturas.id_cobertura = inter_prod_cob.id_cobertura
          WHERE recibo.id_recibo IN (:id) $filtroaux

          UNION

          Select DISTINCT coberturas.id_cobertura,coberturas.nombre AS cobertura, certificado_detalle.prima,certificado_detalle.suma_asegurada,
          (CASE WHEN (certificado_detalle.prima IS NOT NULL AND certificado_detalle.suma_asegurada IS NOT NULL)
                      THEN 1 ELSE 0
                END) AS checado
          FROM recibo
          INNER JOIN certificado ON certificado.id_recibo = recibo.id_recibo
          INNER JOIN certificado_detalle ON certificado_detalle.id_certificado = certificado.id_certificado
          INNER JOIN coberturas ON coberturas.id_cobertura = certificado_detalle.id_cobertura
          WHERE recibo.id_recibo IN (:id) $filtroaux
          ) AS cob GROUP BY  1,2;";


    $connection = \Yii::$app->db;
    $command = $connection->createCommand($QUERY);

    $command->bindValue(":id", Yii::$app->request->post('id_recibo'));
    Yii::$app->response->format = Response::FORMAT_JSON;
    return $result = $command->queryAll();

}

      public function actionBuscarCliente()
      {

          Yii::$app->response->format = Response::FORMAT_JSON;
            return Cliente::find()->joinWith(['idPersona'])
           ->select(["*","CONCAT(nombre, ' ', apellido) AS nombre_completo"])
           ->where("cliente.estatus = :status AND cliente.id_cliente = :id_cliente",
           [':status' => 1,':id_cliente' => Yii::$app->request->post('id')])->asArray()->all();
           //->groupBy('seller_id')
           //->orderBy(['cnt' => 'DESC'])

      }

      public function actionAsegurados(){

          if(!empty(Yii::$app->request->get('id'))){
          Yii::$app->response->format = Response::FORMAT_JSON;
                  return Poliza::arrayAsegurados(Yii::$app->request->get('id'));
         }
      }

      public function actionBeneficiarios(){

          if(!empty(Yii::$app->request->get('id'))){
          Yii::$app->response->format = Response::FORMAT_JSON;
                  return Poliza::arrayBeneficiarios(Yii::$app->request->get('id'));
         }
      }


  public function actionBuscarAsegurados(){

	  Yii::$app->response->format = Response::FORMAT_JSON;

        return CargaFamiliarView::find()->select(["substring(age(now(),fecha_nacimiento)::text from 1 for 2)::int AS anios","*"])
          ->where("cliente_estatus = :status AND carga_familiar_estatus = :status AND id_beneficiario= :id_beneficiario",
          [':status' => 1,
		      ':id_beneficiario' => Yii::$app->request->get('id')])
		  ->asArray()->all();

  }
  public function actionBuscarBeneficiarios(){

      Yii::$app->response->format = Response::FORMAT_JSON;
        return Cliente::find()->joinWith(['idPersona'])
       ->select(["*","CONCAT(nombre, ' ', apellido) AS nombre_completo"])
       ->where("cliente.estatus = :status AND cliente.id_cliente = :id_cliente",
       [':status' => 1,':id_cliente' => Yii::$app->request->get('id')])->asArray()->all();
       //->groupBy('seller_id')
       //->orderBy(['cnt' => 'DESC'])

  }

      public function actionBuscarVendedor()
      {
            Yii::$app->response->format = Response::FORMAT_JSON;
                    return Intermediario::find()
                                 ->joinwith('idPersona')
                                 ->where("intermediario.estatus = :status AND intermediario.id_intermediario = :intermediario AND intermediario.id_tipo_intermediario=2",
                                 [':status' => 1,':intermediario' => Yii::$app->request->post('id')])
                                 ->select(['*',"CONCAT(nombre, ' ', apellido) AS nombre_completo"])->asArray()->all();
     }


    /**
     * Deletes an existing Poliza model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Poliza model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Poliza the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Poliza::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	protected function datos($id)
    {
        if (($model = Poliza::datos_polizaSalud($id)) !== null) {
            return $model;
        } else {

            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
