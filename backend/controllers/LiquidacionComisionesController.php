<?php

namespace backend\controllers;

use Yii;
use backend\models\LiquidacionComisiones;
use backend\models\search\LiquidacionComisionesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\search\ReciboSearch;
use backend\models\Poliza;
use backend\models\Recibo;
use yii\helpers\Json;
use yii\base\Model;
use yii\web\Response;

/**
 * LiquidacionComisionesController implements the CRUD actions for LiquidacionComisiones model.
 */
class LiquidacionComisionesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LiquidacionComisiones models.
     * @return mixed
     */
	 
	public function actionRecibos($id)
    {

		$searchModel = new ReciboSearch();
		//print_r(Yii::$app->request->queryParams);exit();
        $dataProvider = $searchModel->search4(Yii::$app->request->queryParams,$_GET['id']);

        return $this->renderAjax('recibos', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
       /*  $searchModel = new FacturacionSearch();

		 //print_r($searchModel); exit('llegue');
        $query= Poliza::find()->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza')
                              ->innerJoin('aseguradoras', 'aseguradoras.id_aseguradora = poliza.id_aseguradora')
                              ->where("poliza.estatus = :status AND recibo.facturado = :fac AND aseguradoras.id_aseguradora= :id",
                                        [':status' => 1,':fac'=> 0,':id'=>$id])
                              ->select("numero_poliza AS numero_poliza ,
                                recibo.nro_recibo,recibo.comision_total,recibo.id_recibo,aseguradoras.nombre AS aseguradora,
								recibo.fecha_vigenciadesde");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
			'pageSize' => 12,
			]
        ]);



        //$searchModel = new PolizaSearch();
        //$searchModel=$dataProvider; //->search(Yii::$app->request->queryParams);

        return $this->renderAjax('recibos', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
         ]);*/
    } 
	 
    public function actionIndex()
    {
        $searchModel = new LiquidacionComisionesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LiquidacionComisiones model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->datos($id),
        ]);
    }

    /**
     * Creates a new LiquidacionComisiones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LiquidacionComisiones();
		
		if ($model->load(Yii::$app->request->post())) {
			
			//print_r(Yii::$app->request->post()); exit();

          //  $prueba='42-41-40';
            $recibos2=explode(',',$_POST["LiquidacionComisiones"]['recibos']);
            $model->recibos2=$recibos2;
            $model->id_user_registro =Yii::$app->user->identity->id;
            $model->fecha_registro=date('Y-m-d H:i:s');
			$model->tipo=1;
            if($flag=$model->save()){
		
					return $this->redirect(['view', 'id' => $model->id_liquidacion_comisiones]);    	
      			}else{
      				var_dump($model->getErrors());
      			}

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }



    /**
     * Updates an existing LiquidacionComisiones model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		if ($model->load(Yii::$app->request->post())) {
			
			//print_r(Yii::$app->request->post()); exit();

          //  $prueba='42-41-40';
            $recibos2=explode(',',$_POST["LiquidacionComisiones"]['recibos']);
            $model->recibos2=$recibos2;
            $model->id_user_actualizacion =Yii::$app->user->identity->id;
            $model->fecha_actualizacion=date('Y-m-d H:i:s');
			$model->estatus=0;
			$model->tipo=2;
            if($flag=$model->save()){
		
					return $this->redirect(['index']);    	
      			}else{
      				var_dump($model->getErrors());
      			}

        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }    

        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_liquidacion_comisiones]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }*/
    }

    /**
     * Deletes an existing LiquidacionComisiones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
	public function actionCargandoRecibos()
    {
        $pk = Yii::$app->request->post('keylist');

        $recibos=Recibo::find()->where("id_recibo IN ($pk)")->select("nro_recibo,comision_vendedor")->asArray()->all();
        $cadena="";
      //  var_dump($recibos);
	    $comision=0;
		$recibo='';
        foreach ($recibos as $key => $value) {
            $re[]=$value['nro_recibo'];
			$comision+=$value['comision_vendedor'];
        }
         $recibo=implode(',',$re);
        Yii::$app->response->format = Response::FORMAT_JSON;
		return array('recibo'=>$recibo,'comision'=>$comision); 

    }

    /**
     * Finds the LiquidacionComisiones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LiquidacionComisiones the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LiquidacionComisiones::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	protected function datos($id)
    {
        if (($model = LiquidacionComisiones::datosLiquidacion($id)) !== null) {
            return $model;
        } else {

            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
