<?php

/* @var $this \yii\web\View */
/* @var $content string */
use common\models\AccessMenu;

use yii\helpers\Url;//linea para el asistente de url keyla bullon
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\icons\Icon;
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <?= Html::a("<img src='$directoryAssetlocal/img/logo.png' class='img-circle' class='user-image' width='40px' height='40px'>", Yii::$app->homeUrl, ['class' => 'logo']) ?><span style="color:white;">&nbsp; &nbsp;JUAN VICENTE AGUILAR</span>
        </div>
        <ul class="sidebar-menu" id="sidebar">

        <?php
        if(isset(Yii::$app->user->identity->username)){
		      	$menu=AccessMenu::getAcceso();
			/*$menuItems = [
				['label' => 'Home', 'url' => ['@webroot/site/index']],
			];*/
			if($menu!=1){

				foreach($menu as $row){
					$i=0;
					if(!empty($row['id_padre'])){

					}
					 $hijo=AccessMenu::numHijos($row['id']);
					if($hijo > 0){



				?>
				 <li class="treeview">
					<a href="<?=Url::to($row['url'])?>">
						<i class="fa fa-sort-desc"></i> <span><?=$row['rotulo']?></span>
						<!--<i class="fa fa-angle-left"></i>-->
					</a>
					<!--<ul class="treeview-menu">
						<li>
							<a href="#?>"><i class="fa fa-dashboard"></i> <i class="fa fa-angle-left pull-right"></i></a></a>

							<ul class="treeview-menu">-->

							  <?
							   $m=AccessMenu::crearMenu($row['id']);
							  ?>
							<!--</ul>
						</li>
					</ul>-->
				 </li>
			   <?php
					}else{

			   ?>

				<li><a href="<?=Url::toRoute($row['url'])?>"><i class="fa fa-dashboard"></i> <span><?=$row['rotulo']?></a></span></a></li>

			   <?php
				   }





				}
			}
        }

        ?>
       </ul>
        <?/*= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => $menuItems,/*[
                    ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']],
                    ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Same tools',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'fa fa-circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'fa fa-circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],

            ]
        ) */?>

    </section>
</aside>
<?php

$this->registerJs(

"$('#sidebar').affix({
  offset: {
    top: 100
  }
});"
);


$string=".affix-top,.affix{
 position: static;
}

@media (min-width: 979px) {
  #sidebar.affix-top {
    position: static;
  	margin-top:30px;
  	width:228px;
  }

  #sidebar.affix {
    position: fixed;
    top:70px;
    width:228px;
  }
};";

$this->registerCss($string);

?>
