<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
/* bloquear la barra de debug keyla bullon*/
/*if (class_exists('yii\debug\Module')) {
    $this->off(\yii\web\View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);
}*/

if (Yii::$app->controller->action->id === 'login') {
/**
 * Do not use this code in your template. Remove it.
 * Instead, use the code  $this->layout = '//main-login'; in your controller.
 */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {

    if (class_exists('backend\assets\LocateAsset')) {
        backend\assets\LocateAsset::register($this);
    } else {
        app\assets\LocateAsset::register($this);
    }

    dmstr\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');

    $directoryAssetlocal = Yii::$app->assetManager->getPublishedUrl('@backend/dist');

    $directoryUploads = Yii::$app->assetManager->getPublishedUrl('@backend/uploads');

    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <link rel="shortcut icon" type="image/x-icon" href="<?=$directoryAssetlocal?>/img/favicon.ico" />
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="skin-blue sidebar-mini " data-spy="scroll">
    <?php $this->beginBody() ?>
    <div class="wrapper">

        <?= $this->render(
            'header.php',
            ['directoryAsset' => $directoryAsset,
            'directoryAssetlocal'=>$directoryAssetlocal]
        ) ?>

        <?= $this->render(
            'left.php',
            ['directoryAsset' => $directoryAsset,
            'directoryAssetlocal'=>$directoryAssetlocal]
        )
        ?>
        <br/><br/>
        <?= $this->render(
            'content.php',
            ['content' => $content,
            'directoryAsset' => $directoryAsset,
            'directoryAssetlocal'=>$directoryAssetlocal]
        ) ?>

    </div>

    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>