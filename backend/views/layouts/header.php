<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\icons\Icon;
use common\models\ConsultaPoliza;
use backend\models\ConsultaMensaje;
use backend\models\ConsultaReclamos;
use backend\models\Persona;
use backend\models\Agenda;

$renov=ConsultaPoliza::getRenovaciones();
$periodo_g=ConsultaPoliza::getPerido_gracia();
$vencidas=ConsultaPoliza::getVencidas();
$mensajes=ConsultaMensaje::getMensajes();

$reclaIns=ConsultaReclamos::getReclamos('PENDIENTE POR INSPECCION');
$reclaRe=ConsultaReclamos::getReclamos('POR RECAUDOS');
$reclaOr=ConsultaReclamos::getReclamos('POR ORDEN');

$agendar=Agenda::getAgendas();

$cumpleanio=Persona::getCumpleanio();

/* @var $this \yii\web\View */
/* @var $content string */
?>
<!--Html::a(Icon::show('home', ['class' => 'fa-2x'])-->
<header class="main-header">
    <section class="sidebar navbar-fixed-top">
     
      <?= Html::a("<img src='$directoryAssetlocal/img/logotech4u.png' width='110px' height='40px'>", Yii::$app->homeUrl, ['class' => 'logo']) ?>
    </section>

    <nav class="navbar navbar-fixed-top navbar-inverse" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
			    <li class="notifications-menu">
                  <a href="<?=Url::toRoute('persona/cumpleanio')?>" >
                      <i class="fa fa-birthday-cake" aria-hidden="true"></i>
					  <span class="label label-danger"><?=$cumpleanio?></span>
                  </a>
          </li>
			    <li class="notifications-menu">
                  <a href="<?=Url::toRoute('agenda/index')?>" >
                      <i class="glyphicon glyphicon-calendar"></i>
                      <span class="label label-danger"><?=$agendar?></span>
                  </a>
          </li>
			    <li class="dropdown notifications-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" >
                      <i class="fa fa-life-ring" aria-hidden="true"></i>
					               <span class="label label-danger"><?=($reclaIns+$reclaRe+$reclaOr)?></span>
                  </a>
				  <ul class="dropdown-menu">
					  <li class="header">Tiene <?=$reclaIns+$reclaRe+$reclaOr?> Reclamos a Revisar</li>
                      <li>
                          <!-- inner menu: contains the actual data -->
                          <ul class="menu">
							  <li>
                                  <a href="<?=Url::toRoute('reclamos/reclamos-estatus?estatus=PENDIENTE POR INSPECCION')?>">
                                      <i class="fa fa-users text-red"></i> <?=$reclaIns?> Reclamos por Inspeccion
                                  </a>
                              </li>
							  <li>
                                  <a href="<?=Url::toRoute('reclamos/reclamos-estatus?estatus=POR RECAUDOS')?>">
                                      <i class="fa fa-users text-red"></i> <?=$reclaRe?> Por Recaudos
                                  </a>
                              </li>
							  <li>
                                  <a href="<?=Url::toRoute('reclamos/reclamos-estatus?estatus=POR ORDEN')?>">
                                      <i class="fa fa-users text-red"></i> <?=$reclaOr?> Por Orden
                                  </a>
                              </li>
                          </ul>
                      </li>
                      <!--<li class="footer"><a href="#">View all</a></li>-->
                  </ul>
                </li>
              <li class="dropdown notifications-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-bell-o fa-1x"></i>
                      <span class="label label-danger"><?=($renov+$periodo_g+$vencidas+$reclaIns+$reclaRe+$reclaOr)?></span>
                  </a>
                  <ul class="dropdown-menu">
                      <li class="header">Tiene <?=$renov+$periodo_g+$vencidas?> Polizas a Revisar</li>
                      <li>
                          <!-- inner menu: contains the actual data -->
                          <ul class="menu">
                              <li>
                                  <a href="<?=Url::toRoute('poliza/renovacion')?>">
                                      <i class="fa fa-users text-aqua"></i> <?=$renov?> Polizas por Renovar
                                  </a>
                              </li>
                              <li>
                                  <a href="<?=Url::toRoute('poliza/vencidas')?>">
                                      <i class="fa fa-warning text-red"></i> <?=$vencidas?> Polizas Vencidas
                                  </a>
                              </li>
                              <li>
                                  <a href="<?=Url::toRoute('poliza/periodo')?>">
                                      <i class="fa fa-users text-red"></i> <?=$periodo_g?> Polizas en Periodo de gracia
                                  </a>
                              </li>
							  

                             <!-- <li>
                                  <a href="#">
                                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                                  </a>
                              </li>
                              <li>
                                  <a href="#">
                                      <i class="fa fa-user text-red"></i> You changed your username
                                  </a>
                              </li>-->
                          </ul>
                      </li>
                      <!--<li class="footer"><a href="#">View all</a></li>-->
                  </ul>
              </li>
              <li class="menu">
                  <a href="<?=Url::toRoute('reclamos/mensaje-segui')?>" >
                      <i class="glyphicon glyphicon-envelope"></i>
                      <span class="label label-danger"><?=$mensajes?></span>
                  </a>
              </li>
                <!-- Tasks: style can be found in dropdown.less -->
                <li class="dropdown tasks-menu" style="display:none;">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-flag-o"></i>
                        <span class="label label-danger">9</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 9 tasks</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                <li><!-- Task item -->
                                    <a href="#">
                                        <h3>
                                            Design some buttons
                                            <small class="pull-right">20%</small>
                                        </h3>
                                        <div class="progress xs">
                                            <div class="progress-bar progress-bar-aqua" style="width: 20%"
                                                 role="progressbar" aria-valuenow="20" aria-valuemin="0"
                                                 aria-valuemax="100">
                                                <span class="sr-only">20% Complete</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- end task item -->
                                <li><!-- Task item -->
                                    <a href="#">
                                        <h3>
                                            Create a nice theme
                                            <small class="pull-right">40%</small>
                                        </h3>
                                        <div class="progress xs">
                                            <div class="progress-bar progress-bar-green" style="width: 40%"
                                                 role="progressbar" aria-valuenow="20" aria-valuemin="0"
                                                 aria-valuemax="100">
                                                <span class="sr-only">40% Complete</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- end task item -->
                                <li><!-- Task item -->
                                    <a href="#">
                                        <h3>
                                            Some task I need to do
                                            <small class="pull-right">60%</small>
                                        </h3>
                                        <div class="progress xs">
                                            <div class="progress-bar progress-bar-red" style="width: 60%"
                                                 role="progressbar" aria-valuenow="20" aria-valuemin="0"
                                                 aria-valuemax="100">
                                                <span class="sr-only">60% Complete</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- end task item -->
                                <li><!-- Task item -->
                                    <a href="#">
                                        <h3>
                                            Make beautiful transitions
                                            <small class="pull-right">80%</small>
                                        </h3>
                                        <div class="progress xs">
                                            <div class="progress-bar progress-bar-yellow" style="width: 80%"
                                                 role="progressbar" aria-valuenow="20" aria-valuemin="0"
                                                 aria-valuemax="100">
                                                <span class="sr-only">80% Complete</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- end task item -->
                            </ul>
                        </li>
                        <li class="footer">
                            <a href="#">View all tasks</a>
                        </li>
                    </ul>
                </li>
                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?=$directoryAssetlocal?>/img/logo.png" class="img-circle" class="user-image" alt="foto usuario" width="22px" height="22px">
                          <span class="hidden-xs"><?php echo Yii::$app->user->identity->username;?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <p>
                                 TECHNOSOLUTIONS4YOU
                                <small></small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body"></li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <!--<a href="#" class="btn btn-default btn-flat">Profile</a>-->
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Salir',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <!--<li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>-->
            </ul>
        </div>
    </nav>
</header>
<?php

$string = <<<EXP2
EXP2;

$this->registerJs($string, \yii\web\View::POS_READY);
?>
