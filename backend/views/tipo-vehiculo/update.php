<?php

use yii\helpers\Html;
use kartik\icons\Icon;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\TipoVehiculo */

$this->title = '';
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
          <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
          <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Tipos de Vehiculos')?></div></a>
          <a href="#" class="btn btn-default"><div>Actualizar tipos de Vehiculos</div></a>
</div>
</br></br>
<div class="tipo-vehiculo-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
