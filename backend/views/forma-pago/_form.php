<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\FormaPago */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="forma-pago-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre_forma_pago')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_user_registro')->textInput() ?>

    <?= $form->field($model, 'id_user_actualizacion')->textInput() ?>

    <?= $form->field($model, 'fecha_registro')->textInput() ?>

    <?= $form->field($model, 'fecha_actualizacion')->textInput() ?>

    <?= $form->field($model, 'estatus')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
