<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\FormaPago */

$this->title = Yii::t('app', 'Create Forma Pago');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Forma Pagos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="forma-pago-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
