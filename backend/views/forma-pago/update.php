<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\FormaPago */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Forma Pago',
]) . ' ' . $model->id_forma_pago;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Forma Pagos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_forma_pago, 'url' => ['view', 'id' => $model->id_forma_pago]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="forma-pago-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
