<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\icons\Icon;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\MarcasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title ='';
?>
<div class="marcas-index">
	<?php
	$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
      'attribute'=>'id_marca',
      'value'=>'id_marca',
      'width'=>'100px',
    ],
    'nombre:ntext',
    [
     'class' => 'yii\grid\ActionColumn',
     'template' => '{view} {update}',
           'buttons' => [
             'view' => function ($url,$model, $key) {
                       return Html::a('<i class="fa fa-binoculars fa-lg" aria-hidden="true"></i>', ['view','id' => $model->id_marca], [
                           'id' => 'activity-index-link',
                           'title' => Yii::t('app', 'Ver Marca'),'rel'=>'tooltip',
                           'data-pjax' => '0',
                       ]);
              },
              'update' => function ($url, $model, $key) {
                  return Html::a('<i class="fa fa-pencil fa-lg" aria-hidden="true"></i>', ['update', 'id' => $model->id_marca], [
                      'id' => 'activity-index-link',
                      'title' => Yii::t('app', 'Actualizar Marca'),'rel'=>'tooltip',
                      'data-pjax' => '0',
                  ]);
              },
           ]
    ],
	];

	echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
		'columns' => $gridColumns,
	  'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
		'toolbar' => [
			[
				'content'=>
					Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],[
						'title'=>Yii::t('app', 'Registrar Marca'),'rel'=>'tooltip',
						'class'=>'btn btn-success'
					]) . ' '.
					Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
						'class' => 'btn btn-default',
						'title' => Yii::t('app', 'Reset Grid'),'rel'=>'tooltip',
					]),
			],
			'{export}',
			'{toggleData}'
		],
    'exportConfig' => [
      GridView::EXCEL => [],
      GridView::TEXT => [],
      GridView::PDF => [],
    ],
		'pjax' => false,
		'pjaxSettings' => [
			'options' => ['id' => 'marca-pjax-id'],// UNIQUE PJAX CONTAINER ID
		],
		'bordered' => false,
		'resizableColumns'=>true,
		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
		'striped' => true,
		'condensed' => false,
		'responsive' => true,
		'hover' => true,
		'showPageSummary' => false,
		'panel' => [
			'heading'=>'<h2 class="panel-title" style="font-weight:bold;">'.Icon::show('car', ['class' => 'fa-2x']).'&nbsp; &nbsp;MARCAS</h2>',
			'type' => GridView::TYPE_INFO
		],
    ]);
	?>
</div>
<?php
$stringJS = <<<EXP2
$("[rel=tooltip]").tooltip({ placement: 'top'});
EXP2;
$this->registerJs($stringJS, \yii\web\View::POS_READY);

$stringCSS='
 .fa{margin:0px 4px 0px 0px;}
  .fa-trash{
      color:  #d9534f;
  }
  .fa-plus{
      color:  #5cb85c;
  }
  .fa-retweet{
      color:  #5bc0de;
  }
  .fa-binoculars{
      color:  #39CCCC;
  }';
 $this->registerCss($stringCSS);
 ?>
