<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;//keyla bullon

/* @var $this yii\web\View */
/* @var $model frontend\models\Marcas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="marcas-form">

    <?php $form = ActiveForm::begin(); ?>

	<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
    <div class="box-header with-border">
         <h4 class="box-title"><!--<i class="glyphicon glyphicon-user"></i>--> Registro de Marca</h4>
    </div>

		<div class="box-body">
			<div class="container-items"><!-- widgetBody -->
				<div class="row">
				    <div class="col-sm-5">
						<?= $form->field($model, 'id_marca')->textInput(['maxlength' => true]) ?>
					</div>
					<div class="col-sm-3">
						<?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
					</div>

				</div>
				<div class="row">
					<div class="col-sm-2">
						<?php
						//echo $form->field($model, 'estatus')->dropDownList([1 => 'Activo', 0 => 'Inactivo']);
						echo $form->field($model, 'ws_inhabilitado')->widget(Select2::classname(), [
								'data' => array("0"=>"Activo","1"=>"Inactivo"),
								'language' => 'es',
								//'options' => ['placeholder' => 'Seleccione un Esatdo ...'],
								'pluginOptions' => [
								'allowClear' => true
								],
						]);
						?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2">
						<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
					</div>
				</div>

			</div>
		</div>
    </div>



    <?php ActiveForm::end(); ?>

</div>
