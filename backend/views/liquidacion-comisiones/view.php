<?php

use yii\helpers\Html;
//use yii\widgets\DetailView;
use kartik\detail\DetailView;
use backend\models\LiquidacionComisiones;
use yii\helpers\ArrayHelper;
use kartik\icons\Icon;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\LiquidacionComisiones */

$this->title ='';
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
            <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
            <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Listado de liquidaciones')?></div></a>
            <a href="#" class="btn btn-default"><div>Datos Liquidacion</div></a>
</div>
</br></br>
<div class="liquidacion-comisiones-view">

    <?php 
	$modelRecibos=LiquidacionComisiones::datosRecibos($model->id_liquidacion_comisiones)->asArray()->all();
	
	
		$attributes = [
    /*[
        'group'=>true,
        'label'=>'Version ' .$model->id_version,
        'rowOptions'=>['class'=>'info']
    ],*/
	[
        'columns' => [
			[
                'attribute'=>'identificacion', 
				'label'=>'Identificacion',
                'format'=>'raw', 
                //'value'=> $model->idModelos->nombre,
                'valueColOptions'=>['style'=>'width:30%'], 
                'displayOnly'=>true
            ],  
		    [
                'attribute'=>'nombre_completo', 
				'label'=>'Nombre',
                'format'=>'raw', 
                //'value'=>'<kbd>'.$model->id_version.'</kbd>',
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ],  
            
        ],
	],
    [	
		'columns' => [
			[
                'attribute'=>'comentario', 
				'label'=>'Comentario',
                'format'=>'raw', 
                //'value'=>'<kbd>'.$model->id_version.'</kbd>',
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ], 
			[
                'attribute'=>'monto', 
				'label'=>'monto',
                'format'=>['decimal', 2], 
                //'value'=>'<kbd>'.$model->id_version.'</kbd>',
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ], 
            
        ],
    ],
	];
	
	$attributes[]= 
				[
							'group'=>true,
							'label'=>'Detalle',
							'rowOptions'=>['class'=>'success']
				];
			

	$total=0;
	$attributes[]= 
             [
			'columns' => [
				[
					//'attribute'=>'numero_poliza', 
					'label'=>'Cliente',
					'format'=>'raw', 
					'value'=>'',
					'valueColOptions'=>['style'=>'display:none;'],
					'labelColOptions'=>['style'=>'width:30%'],
					'displayOnly'=>true
				], 
				[
					//'attribute'=>'numero_poliza', 
					'label'=>'Nro Poliza',
					'format'=>'raw', 
					'value'=>'',
					'valueColOptions'=>['style'=>'display:none;'],
					'labelColOptions'=>['style'=>'width:30%'],
					'displayOnly'=>true
				], 
				[
					//'attribute'=>'numero_poliza', 
					'label'=>'Nro Recibo',
					'format'=>'raw', 
					'value'=>'',
					'valueColOptions'=>['style'=>'display:none;'],
					'labelColOptions'=>['style'=>'width:30%'],
					'displayOnly'=>true
				], 
				[
					//'attribute'=>'fecha_vigenciahasta', 
					'label'=>'Comision',
					'format'=>'raw', 
					'value'=>'',
					'valueColOptions'=>['style'=>'display:none;'],
					'labelColOptions'=>['style'=>'width:20%;text-align: right;'],
					'displayOnly'=>true
				],
			],	
		];
	$aseguradora='';
	foreach ($modelRecibos as $re) {
      
	  $total+=$re['comision_vendedor'];
	  
	  $attributes[]= 
             [
			 
			'columns' => [
				[
					//'attribute'=>'numero_poliza', 
					'label'=>'Nro recibo',
					'format'=>'raw', 
					'value'=>$re['nombre_completo'],
					'valueColOptions'=>['style'=>'width:30%'],
					'labelColOptions'=>['style'=>'display:none;'],
					'displayOnly'=>true
				], 
				[
					//'attribute'=>'numero_poliza', 
					'label'=>'Nro recibo',
					'format'=>'raw', 
					'value'=>$re['numero_poliza'],
					'valueColOptions'=>['style'=>'width:30%'],
					'labelColOptions'=>['style'=>'display:none;'],
					'displayOnly'=>true
				], 
				[
					//'attribute'=>'numero_poliza', 
					'label'=>'Nro recibo',
					'format'=>'raw', 
					'value'=>$re['nro_recibo'],
					'valueColOptions'=>['style'=>'width:30%'],
					'labelColOptions'=>['style'=>'display:none;'],
					'displayOnly'=>true
				], 
				[
					//'attribute'=>'fecha_vigenciahasta', 
					'label'=>'Suma Asegurada',
					'format'=>['decimal', 2], 
					'value'=> $re['comision_vendedor'],
					'valueColOptions'=>['style'=>'width:20%;text-align: right;'], 
					'labelColOptions'=>['style'=>'display:none;'],
					'displayOnly'=>true
				], 
	 	
			],
		];
		
		if($re['aseguradora']!=$aseguradora && $aseguradora!=''){
			
			$attributes[]= 
             [
			'columns' => [
			
				[
					//'attribute'=>'fecha_vigenciahasta', 
					'label'=>'Total'.' '.$aseguradora,
					'format'=>['decimal', 2], 
					'value'=>  $total,
					'valueColOptions'=>['style'=>'width:20%;text-align: right;'],
                    'labelColOptions'=>['style'=>'width:50%;text-align: right;'],					
					'displayOnly'=>true
				], 	
			],
		]; 
		
		
			
		}
		
		 $aseguradora=$re['aseguradora'];
      
       }
	   
	   $attributes[]= 
             [
			'columns' => [
			
				[
					//'attribute'=>'fecha_vigenciahasta', 
					'label'=>'Total'.' '.$aseguradora,
					'format'=>['decimal', 2], 
					'value'=>  $total,
					'valueColOptions'=>['style'=>'width:20%;text-align: right;'],
                    'labelColOptions'=>['style'=>'width:50%;text-align: right;'],					
					'displayOnly'=>true
				], 	
			],
		]; 
	   
	  
	
	echo DetailView::widget([
		'model' => $model,
		'attributes' => $attributes,
		'mode' => 'view',
		'enableEditMode'=> false,
		//'bordered' => 'bordered',
		//'striped' => $striped,
		//'condensed' => $condensed,
		'responsive' => 'responsive',
		//'hover' => $hover,
		//'hAlign'=>$hAlign,
		//'vAlign'=>$vAlign,
		//'fadeDelay'=>$fadeDelay,
		'deleteOptions'=>[ // your ajax delete parameters
			'params' => ['id' => 1000, 'kvdelete'=>true],
		],
		'panel'=>[
        'heading'=>'DETALLE LIQUIDACION ['.$model->id_liquidacion_comisiones.']',
        'type'=>DetailView::TYPE_INFO,
		
        ],
		'container' => ['id'=>'kv-demo'],
		//'formOptions' => ['action' => Url::current(['update' => 'kv-demo'])] // your action to delete
	]);
	
	
	$string="

         .etica{
			background-color:#B2B9BF;
			color:#FFFFFF;
			font-size: 13px;
			font-weight: bold;
			border-left: 0px solid #EEF6FF; 
		 }";
      $this->registerCss($string);
	 
	 
	?>

</div>
