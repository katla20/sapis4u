<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use backend\models\Intermediario;
use backend\models\Persona;
use kartik\select2\Select2;//keyla bullon
use kartik\depdrop\DepDrop;//http://demos.krajee.com/widget-details/depdrop
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\icons\Icon;
use yii\web\Response;
use kartik\money\MaskMoney;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\widgets\Pjax;
use kartik\widgets\DatePicker;



/* @var $this yii\web\View */
/* @var $model backend\models\LiquidacionComisiones */
/* @var $form yii\widgets\ActiveForm */
?>
<?php 

$model->isNewRecord ? $model->tipo=1 : $model->tipo=2;

;?>
<div class="liquidacion-comisiones-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
    <div class="box-header">
         <h4 class="box-title">Datos Basicos Para liquidar Comisiones</h4>
    </div>

		<div class="box-body">
			<div class="container-items"><!-- widgetBody -->

		<div class="row">
				<div class="col-sm-3">
					<?php

						$id_intermediario = ArrayHelper::map(Intermediario::find()->joinwith('idPersona')
                                                                                    ->where("intermediario.estatus = :status", [':status' => 1])
                                                                                    ->select("(nombre||' '||apellido||' '|| identificacion) AS nombre_completo ,id_intermediario")
                                                                                    ->all(),'id_intermediario', 'nombre_completo');

						echo $form->field($model, 'id_intermediario',[
                                          'addon' => [
                                              'prepend' => [
                                                  'content' => '<i class="fa fa-university"></i>'

                                              ]
                                          ]
                                      ])->widget(Select2::classname(), [
							'data' => $id_intermediario,
							'language' => 'es',
							'options' => ['placeholder' => 'Seleccione un Vendedora ...'],
							'pluginOptions' => [
							'allowClear' => true
							],
						]);
					?>
                     <?=$form->field($model, 'recibos')->hiddenInput()->label(false);?>
					  <?=$form->field($model, 'tipo')->hiddenInput()->label(false);?>
				</div>
				<div class="col-sm-6"><div id="datosaseguradora"></div></div>	
	    </div>
		<div class="row">
          <div class="col-sm-6">
          
            <?= $form->field($model, 'recibos_numero',[
                        'addon' => [
                            'prepend' => ['content'=>'<i class="glyphicon glyphicon-user"></i>'],
                            'append'=>['content'=>Html::a('<i class="fa fa-user-plus"></i> Recibos', '#', [
                                                           'id' => 'm_recibos',
                                                           'class' => 'btn btn-success',
                                                           //'data-toggle' => 'modal',
                                                          // 'data-target' => '#modal',
                                                           'data-url' => Url::to(['recibos']),
                                                           'data-pjax' => '0',
                                                       ]), 'asButton'=>true],
                        ]
                  ])->textInput() ?>
          </div>
		  <div class="col-sm-2">
          <?=$form->field($model, 'monto',[
        												  'addon' => [
        													  'prepend' => [
        														  'content' => 'Bs.',

        													  ]
        												  ]
        											  ])->widget(MaskMoney::classname(), [
        																				  'pluginOptions' => [
        																					  'prefix' => '',
        																					  'suffix' => '',
        																					  'allowNegative' => false
        																					]
        			]);?>
					</div>
			</div>
		<div class="row">
			<div class="col-sm-8">
			 <?= $form->field($model, 'comentario',[
							'addon' => [
								'prepend' => [
									'content' => '<i class="fa fa-pencil"></i>'

								]
							]
						])->textarea(['rows' => 2]) ?>
			</div>

		  </div>
		  
		    <div class="form-group">
				<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			</div>
		</div>
		
    </div>
</div>	

  

    <?php ActiveForm::end(); ?>

</div>

<?php
$string = <<<EXP2
$(document).on('click', '#m_recibos', (function(e) {
	
	var id_intermediario=$('#liquidacioncomisiones-id_intermediario').val();
	
	if(id_intermediario!=""){
		
		$.get($(this).data('url'),{'tipo':'liquidacion-comisiones','id':id_intermediario},
          function (data) {
            $('.modal-body').html(data);
            $('#modal').modal({
				backdrop: true,
				keyboard: true
			});
          }
		);
		$('#modal').on('hidden.bs.modal', function (e) {
				$(this).find('.modal-body').html('');
			});
			
	}else{
	   $('#modal').hide();
	   alert("Debe Seleccionar un Vendedor ");
	}

}));

$('#modal .modal-dialog').css({
     width: '40%',
     heigth: '50%'});
	 
	 
 
 if($('#liquidacioncomisiones-tipo').val()==2){
	 
	 $('#liquidacioncomisiones-recibos_numero,#liquidacioncomisiones-monto-disp').prop('readonly', true);
	 $('#m_recibos,#liquidacioncomisiones-id_intermediario').prop('disabled', true);
	 $('#liquidacioncomisiones-comentario').val('');
	 $('label[for="liquidacioncomisiones-comentario"').text('Motivo Anulacion');
	 
 }
 
 
EXP2;

$this->registerJs($string, \yii\web\View::POS_READY);

?>

<?php
Modal::begin([
 'options' => [
        'id' => 'modal',
        'tabindex' => false, // important for Select2 to work properly
  ],
]);
Modal::end();
?>

