﻿<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\Poliza;
use kartik\icons\Icon;
use kartik\daterange\DateRangePicker;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PolizaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//http://www.w3schools.com/colors/colors_picker.asp colores
$this->title = "";
?>
<?php Pjax::begin(['id' => 'recibos-pjax-id','timeout' => false,'enablePushState' => false]) ?>
<div class="recibos-modal-index">
    <?php
  	$gridColumns = [
  	//['class' => 'kartik\grid\SerialColumn'],
		[
			 'class'=>'kartik\grid\CheckboxColumn',
       'checkboxOptions' => function ($model, $key, $index, $column) {
          return ['value' => $model->numero_poliza];
       },
		],
    [
          'class' => 'kartik\grid\DataColumn',
          'attribute'=>'numero_poliza',
          'value'=>'numero_poliza',
          'width'=>'100px',
    ],
	 /* [
            'class' => 'kartik\grid\DataColumn',
            'attribute'=>'aseguradora',
            'value'=>'aseguradora',
            'width'=>'300px',
     ],*/
	  [
          'class' => 'kartik\grid\DataColumn',
          'attribute'=>'nro_recibo',
          'value'=>'nro_recibo',
          'width'=>'100px',
    ],
		[
            'attribute' => 'fecha_vigenciadesde',
            'value' => 'fecha_vigenciadesde',
            'format'=>'raw',
            'options' => ['style' => 'width: 25%;'],
            'filter' => DateRangePicker::widget([
                'model' => $searchModel,
                'attribute' => 'fecha_vigenciadesde',
                'useWithAddon'=>false,
                'convertFormat'=>true,
                'pluginOptions'=>[
                    'locale'=>['format'=>'Y-m-d']
                ],
            ])
        ],

        'comision_total',

       /*  [
           'class' => 'kartik\grid\ActionColumn',
           'template' => '',
           'buttons' => [
               'update' => function ($url, $model, $key) {
                   return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                       'id' => 'activity-index-link',
                       'title' => Yii::t('app', 'Actualizar Cliente'),
                       'data-toggle' => 'modal',
                       'data-target' => '#modal',
                       'data-url' => Url::to(['update', 'id' => $model->id_persona]),
                       'data-pjax' => '0',
                   ]);
               },
           ]
         ],*/
  	];
 ?>


 <?php

  	echo GridView::widget([
	   'id'=>'recibos-gridview-id',
       'dataProvider' => $dataProvider,
       'filterModel' => $searchModel,
  		'columns' => $gridColumns,
  		'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
        'rowOptions' => function ($model, $key, $index, $grid) {
           return ['id' =>$key,'data-recibo'=>$model->id_recibo,'class' => GridView::TYPE_INFO];
        },//'rbo-'+$key
  		'toolbar' => [
  			/*[
  				'content'=>
  					Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['recibos'], [
  						'class' => 'btn btn-default',
  						'title' => Yii::t('app', 'Actualizar Grid')
  					]),

  			],*/
  			'{export}',
  			'{toggleData}'
  		],

  	/*'pjax' => true,
		'pjaxSettings'=>[
           'options'=>[
            'enablePushState'=>false,
            'id'=>'recibos-pjax-id',// UNIQUE PJAX CONTAINER ID
            'formSelector'=>'ddd',
          ],
      ],*/
  		'bordered' => false,
  		'resizableColumns'=>true,
  		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
  		'striped' => true,
  		'condensed' => false,
  		'responsive' => true,
  		'hover' => true,
  		'showPageSummary' => false,
    		'panel' => [
          'heading'=>'<h2 class="panel-title">&nbsp; &nbsp;Recibos</h2>',
    			'type' => GridView::TYPE_INFO
    		],
      ]);
 ?>

<?php Pjax::end() ?>
</div>
<div class="form-group" style="float:left"><!--INICIO BOTONES DEL PANEL-->
<?= Html::Button(Icon::show('more').'Incluir Recibos para liquidar', ['class' => 'btn btn-primary','id' =>'cargar-recibos']) ?>
</div><!--FIN BOTONES DEL PANEL-->


<?php
$string = <<<EXP2

/*$("#cargar-recibos").on("click", function(e) {
   var pk=$('#recibos-gridview-id').yiiGridView('getSelectedRows');
   var recibos = new Array();
     //llegan don el identificador de la columna y con el atributo data-recibo que se seteo en el tr del la columna me traigo el valor del recibo
     $.each(pk,function( k, v ) {
        recibos[k] = $('#'+v).data('recibo');
    });

    $.post(
        "cargando-recibos",
        {
            pk : recibos
        },
        function () {
          //  $.pjax.reload({container:'#recibos-gridview-id'});
          //  window.close();
        }
    );
});
*/
//$("#recibos-gridview-id input[type=checkbox]").click(function(){
$("#cargar-recibos").on("click", function() {
        var keys = $("#recibos-gridview-id").yiiGridView('getSelectedRows');
        var recibos="";
        $.each(keys,function( k, v ) {
           recibos += v+',';
        });
        recibos=recibos.slice(0,-1);
        /*$.ajax({
            type:"POST",
            url: "cargando-recibos", // your controller action
            dataType: "json",
            data: {keylist: keys},
            success: $("#liquidacioncomisiones-recibos_numero").val(recibos);
        });*/

        if(recibos!=''){
           $("#liquidacioncomisiones-recibos").val(recibos);
           $.post("cargando-recibos", { keylist: recibos })
           .done(function(result) {
			   
			 //  console.log(result.comision);
		   var comi=parseFloat(result.comision).toFixed(2);
  
		   $("#liquidacioncomisiones-recibos_numero").val(result.recibo);
			  $("#liquidacioncomisiones-monto").val(comi);
			   $("#liquidacioncomisiones-monto-disp").val(comi);
			  
           });//.donefunction(result)
         $('#modal').modal('hide');
        }else{
          alert('Debe seleccionar al menos un recibo');
        }

});
//data-col-seq

EXP2;
$this->registerJs($string, \yii\web\View::POS_READY);
$string='';
$this->registerCss($string);
?>
