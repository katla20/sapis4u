<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\Intermediario;
use backend\models\Persona;
use kartik\icons\Icon;
use yii\helpers\Url;
Icon::map($this, Icon::WHHG); // Maps the Elusive icon font framework
Icon::map($this, Icon::FA); // Maps the Elusive icon font framework

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\FacturacionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title ='';//' Yii::t('app', 'Facturacions');
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
           <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
</div>
</br></br>
<div class="facturacion-index">

<?php
	$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
          'attribute'=>'id_intermediario',
          'width'=>'300px',
          'value'=>'nombre_completo',
          'filterType'=>GridView::FILTER_SELECT2,
          'filter'=>ArrayHelper::map(Intermediario::find()->joinwith('idPersona')
                                                                                    ->where("intermediario.estatus = :status", [':status' => 1])
                                                                                    ->select("(nombre||' '||apellido||' '|| identificacion) AS nombre_completo ,id_intermediario")
                                                                                    ->all(),'id_intermediario', 'nombre_completo'),
          'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
            ],
      'filterInputOptions'=>['placeholder'=>'Aseguradoras','id' => 'aseguradoras-select2-id'],
    ],
    
    ['class' => 'kartik\grid\DataColumn',
     'attribute'=>'fecha_registro',
     'width'=>'200px',
     'value'=>function ($model) {
               if($model->fecha_registro!=""){
                 return date("d-m-Y", strtotime($model->fecha_registro));
               }else{
                 return "";
               }

      },
     'filterType'=>GridView::FILTER_DATE,
     'filterWidgetOptions' => [
        'pluginOptions'=>[
        'format' => 'dd-mm-yyyy',
        'autoWidget' => true,
        'autoclose' => true,
        //'todayBtn' => true,
        ],

      ],
    ],
    [
      'attribute'=>'monto',
      'value'=>'monto',
      'width'=>'200px',
    ],
    [
      'attribute'=>'comentario',
      'value'=>'comentario',
      'width'=>'500px',
    ],


    [
     'class' => 'yii\grid\ActionColumn',
     'contentOptions' => ['style' => 'width:200px;'],
     'header'=>'GESTION LIQUIDACION DE COMISIONES',
     'template' => '{view} {update}',
      'buttons' => [
              'view' => function ($url,$model, $key) {
                return Html::a('<i class="fa fa-binoculars fa-lg" aria-hidden="true"></i>', ['liquidacion-comisiones/view','id' => $model->id_liquidacion_comisiones], [
                    'id' => 'activity-index-link-view',
                    'title' => Yii::t('app', 'Ver Liquidacion'),'rel'=>'tooltip',
                    'data-pjax' => '0',
                ]);
              },
             'update' => function ($url,$model, $key) {
                       return Html::a('<i class="fa fa-trash fa-lg" aria-hidden="true"></i>', ['liquidacion-comisiones/update','id' => $model->id_liquidacion_comisiones], [
                           'id' => 'activity-index-link-update',
                           'title' => Yii::t('app', 'Anular Liquidacion'),'rel'=>'tooltip',
                           'data-pjax' => '0',
                       ]);
                      },
           ]
    ],
	];

	echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		    'columns' => $gridColumns,
		    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    		'toolbar' => [
    			[
    				'content'=>
    					Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],[
    						//'type'=>'button',
    						'title'=>Yii::t('app', 'Liquidar Comisiones'),
    						'class'=>'btn btn-success'
    					]) . ' '.
    					Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
    						'class' => 'btn btn-default',
    						'title' => Yii::t('app', 'Reset Grid')
    					]),

    			],
    			'{export}',
    			'{toggleData}'
    		],
        'exportConfig' => [
          GridView::EXCEL => [],
          GridView::TEXT => [],
          GridView::PDF => [],
        ],

		'pjax' => false,
		'pjaxSettings' => [
			'options' => ['id' => 'facturacion-pjax-id'],// UNIQUE PJAX CONTAINER ID
		],
		'bordered' => false,
		'resizableColumns'=>true,
		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
		'striped' => true,
		'condensed' => false,
		'responsive' => true,
		'hover' => true,
		'showPageSummary' => false,
		'panel' => [
      'heading'=>'<h2 class="panel-title" style="font-weight:bold;">'.Icon::show('cashregister', ['class'=>'icon'], Icon::WHHG).'&nbsp; &nbsp;LIQUIDACION DE COMISIONES</h2>',
      'type' => GridView::TYPE_INFO
		],
    ]);
	?>

</div>
<?php

$string = <<<EXP2
$("[rel=tooltip]").tooltip({ placement: 'top'});

EXP2;
$this->registerJs($string, \yii\web\View::POS_READY);

?>

<?php

$string='
@brand-success: #5cb85c;
@brand-info:    #5bc0de;
@brand-warning: #f0ad4e;
@brand-danger:  #d9534f;

.icon-cashregister{
       font-size: 150%;

}
.fa{margin:0px 4px 0px 0px;}
 .fa-bars{
     color:  #d9534f;
 }
 .fa-card{
     color:  #5cb85c;
 }
 .fa-money{
     color:  #5bc0de;
 }

 ';
$this->registerCss($string);
?>

