<?php

use yii\helpers\Html;

use yii\bootstrap\ActiveForm;
use kartik\icons\Icon;
use yii\helpers\Url;
Icon::map($this, Icon::WHHG); // Maps the Elusive icon font framework
Icon::map($this, Icon::FA); // Maps the Elusive icon font framework


/* @var $this yii\web\View */
/* @var $model backend\models\LiquidacionComisiones */

$this->title = '';//Yii::t('app', 'Create Liquidacion Comisiones');
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
            <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
            <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Listado de Liquidaciones')?></div></a>
            <a href="#" class="btn btn-default"><div>Liquidacion de Comisiones</div></a>
  </div>
  </br></br>
  <div class="liquidacion-comisiones-create">
      <div class="bg-aqua top-modulo">
          <span class="icon-modulo"><?=Icon::show('cashregister', ['class'=>'icon'], Icon::WHHG)?></span>
          <span style="color:white;font-size:18px;font-weight:bold;"><?=Yii::t('app', 'Liquidacion de Comisiones');?></span>
      </div

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>


</div>

<?php $string=".top-modulo{padding:2px 2px 2px 8px;}
          .icon-modulo{margin:0px 4px 0px 0px}";
      $this->registerCss($string);
?>
<?php

$string='
.icon-cashregister{
       font-size: 150%;

}
.fa{margin:0px 4px 0px 0px;}
 .fa-bars{
     color:  #d9534f;
 }
 .fa-card{
     color:  #5cb85c;
 }
 .fa-money{
     color:  #5bc0de;
 }

';
$this->registerCss($string);
?>
