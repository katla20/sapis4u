<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\LiquidacionComisionesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="liquidacion-comisiones-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_liquidacion_comisiones') ?>

    <?= $form->field($model, 'id_intermidiario') ?>

    <?= $form->field($model, 'comentario') ?>

    <?= $form->field($model, 'fecha_registro') ?>

    <?= $form->field($model, 'fecha_actualizacion') ?>

    <?php // echo $form->field($model, 'id_user_registro') ?>

    <?php // echo $form->field($model, 'id_user_actualizacion') ?>

    <?php // echo $form->field($model, 'monto') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
