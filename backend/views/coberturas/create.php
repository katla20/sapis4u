<?php

use yii\helpers\Html;
use kartik\icons\Icon;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model frontend\models\Coberturas */

$this->title = '';
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
          <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
          <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Coberturas')?></div></a>
          <a href="#" class="btn btn-default"><div>Crear Cobertura</div></a>
</div>
</br></br>
<div class="coberturas-create">
    <?= $this->render('_form', [
        'model' => $model,
        'tipoProductos' => $tipoProductos
    ]) ?>

</div>
