<?php

use yii\helpers\Html;
//use yii\widgets\DetailView;
use kartik\detail\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\models\Coberturas */

$this->title ='';// $model->id_cobertura;
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
            <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
            <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Coberturas')?></div></a>
            <a href="#" class="btn btn-default"><div>Datos del la cobertura</div></a>
</div>
</br></br>
<div class="coberturas-view">
	<?php
	$pro='';
    foreach ($model->productosList as $Productos) {

	   $pro.= $Productos['nombre'].", ";
        //$Ram=$Ramos['nombre'];

     }
	 $pro=substr($pro, 0, -2);

	$attributes = [
    /*[
        'group'=>true,
        'label'=>'Aseguradora',
        'rowOptions'=>['class'=>'info']
    ],*/
    [
        'columns' => [
		    [
                'attribute'=>'id_cobertura',
				'label'=>'Código',
                'format'=>'raw',
                'value'=>'<kbd>'.$model->id_cobertura.'</kbd>',
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ],
            [
                'attribute'=>'nombre',
                'label'=>'Cobertura',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],



        ],
    ],
	[
        'columns' => [
            [
                'attribute'=>'estatus',
                'label'=>'Estatus',
                'format'=>'raw',
                'type'=>DetailView::INPUT_SWITCH,
                'widgetOptions' => [
                    'pluginOptions' => [
                        'onText' => 'Activo',
                        'offText' => 'Inactivo',
                    ]
                ],
                'value'=>($model->estatus==1)? '<span class="label label-success">Activo</span>' : '<span class="label label-danger">Inactivo</span>',
                //'valueColOptions'=>['style'=>'width:10%']
            ],

        ],
    ],
	[
        'columns' => [

            [
                'attribute'=>'Productos',
				'value'=>$pro,
                //'valueColOptions'=>['style'=>'width:70%'],
            ],

        ],
    ],
	];
//print_r($model);
	// View file rendering the widget

	echo DetailView::widget([
		'model' => $model,
		'attributes' => $attributes,
		'mode' => 'view',
		'enableEditMode'=> false,
		//'bordered' => 'bordered',
		//'striped' => $striped,
		//'condensed' => $condensed,
		'responsive' => 'responsive',
		//'hover' => $hover,
		//'hAlign'=>$hAlign,
		//'vAlign'=>$vAlign,
		//'fadeDelay'=>$fadeDelay,
		'deleteOptions'=>[ // your ajax delete parameters
			'params' => ['id' => 1000, 'kvdelete'=>true],
		],
		'panel'=>[
        'heading'=>'Cobertura',
        'type'=>DetailView::TYPE_INFO,

        ],
		'container' => ['id'=>'kv-demo'],
		//'formOptions' => ['action' => Url::current(['update' => 'kv-demo'])] // your action to delete
	]);


    ?>

</div>
