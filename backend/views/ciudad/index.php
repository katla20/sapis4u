<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use backend\models\Estado;
use yii\helpers\ArrayHelper;
use kartik\icons\Icon;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\CiudadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/*https://github.com/kartik-v/yii2-widgets/issues/202
https://github.com/kartik-v/yii2-grid/issues/243
http://www.sitepoint.com/rendering-data-in-yii-2-with-gridview-and-listview/*/

$this->title ='';
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
<a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
</div>
</br></br>
<div class="ciudad-index">

	<?php
	$modelEstado = new Estado();
	$gridColumns = [
		  ['class' => 'yii\grid\SerialColumn'],
      [
        'attribute'=>'id_ciudad',
        'value'=>'id_ciudad',
        'width'=>'100px',
      ],
      [
        'attribute'=>'id_estado',
        'width'=>'310px',
        'value'=>'idEstado.nombre',
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(Estado::find()->orderBy('nombre')->asArray()->all(), 'id_estado', 'nombre'),
        'filterWidgetOptions'=>[
          'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>'Estados','id' => 'estado-select2-id'],
        //'group'=>true,  // enable grouping
      ],
      'nombre:ntext',
      [
        'class'=>'kartik\grid\BooleanColumn',
        'filterType'=>GridView::FILTER_SELECT2,
        'attribute'=>'estatus',
        'vAlign'=>'middle',
        'width'=>'100px',
      ],
      [
       'class' => 'yii\grid\ActionColumn',
       'template' => '{view} {update}',
             'buttons' => [
               'view' => function ($url,$model, $key) {
                         return Html::a('<i class="fa fa-binoculars fa-lg" aria-hidden="true"></i>', ['view','id' => $model->id_ciudad], [
                             'id' => 'activity-index-link',
                             'title' => Yii::t('app', 'Ver Ciudad'),'rel'=>'tooltip',
                             'data-pjax' => '0',
                         ]);
                },
                'update' => function ($url, $model, $key) {
                    return Html::a('<i class="fa fa-pencil fa-lg" aria-hidden="true"></i>', ['update', 'id' => $model->id_ciudad], [
                        'id' => 'activity-index-link',
                        'title' => Yii::t('app', 'Actualizar Ciudad'),'rel'=>'tooltip',
                        'data-pjax' => '0',
                    ]);
                },
             ]

      ],

	];

	echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'columns' => $gridColumns,
		'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
		'toolbar' => [
			[
				'content'=>
					Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],[
						//'type'=>'button',
						'title'=>Yii::t('app', 'Registrar Ciudad'),
						'class'=>'btn btn-success'
					]) . ' '.
					Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
						'class' => 'btn btn-default',
						'title' => Yii::t('app', 'Reset Grid')
					]),

			],
			'{export}',
			'{toggleData}'
		],

		'pjax' => false,
		'pjaxSettings' => [
			'options' => ['id' => 'ciudad-pjax-id'],// UNIQUE PJAX CONTAINER ID
		],
		'bordered' => false,
		'resizableColumns'=>true,
		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
		'striped' => true,
		'condensed' => false,
		'responsive' => true,
		'hover' => true,
		'showPageSummary' => false,
		'panel' => [
		    'heading'=>'<h2 class="panel-title" style="font-weight:bold;">'.Icon::show('car', ['class' => 'fa-2x']).'&nbsp; &nbsp;CIUDADES</h2>',
			  'type' => GridView::TYPE_INFO
		],
    ]);
	?>

</div>
<?php
//https://github.com/kartik-v/yii2-widgets/issues/202 resolver el problema de el pajax

$this->registerJs(
'$("#ciudad-pjax-id").on("pjax:complete", function() {
var $el = $("#estado-select2-id"),
    options = $el.data("pluginOptions"); // select2 plugin settings saved
    jQuery.when($el.select2(window[options])).done(initSelect2Loading("estado-select2-id"));
});'
); ?>

<?php
$stringJS = <<<EXP2
$("[rel=tooltip]").tooltip({ placement: 'top'});
EXP2;
$this->registerJs($stringJS, \yii\web\View::POS_READY);

$stringCSS='
 .fa{margin:0px 4px 0px 0px;}
  .fa-trash{
      color:  #d9534f;
  }
  .fa-plus{
      color:  #5cb85c;
  }
  .fa-retweet{
      color:  #5bc0de;
  }
  .fa-binoculars{
      color:  #39CCCC;
  }';
 $this->registerCss($stringCSS);
 ?>
