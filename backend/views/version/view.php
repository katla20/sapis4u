<?php

use yii\helpers\Html;
//use yii\widgets\DetailView;
use kartik\detail\DetailView;
use backend\models\Modelos;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\models\Version */

$this->title ='';// $model->id_version;
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
            <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
            <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Versiones')?></div></a>
            <a href="#" class="btn btn-default"><div>Datos del la version</div></a>
</div>
</br></br>
<div class="version-view">
    <?
	$attributes = [

	[
        'columns' => [
		    [
                'attribute'=>'id_version',
				'label'=>'Código',
                'format'=>'raw',
                'value'=>'<kbd>'.$model->id_version.'</kbd>',
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ],
            [
                'attribute'=>'id_modelo2',
				'label'=>'Modelo',
                'format'=>'raw',
                'value'=> $model->idModelos->nombre,
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ],
        ],
    ],
    [
        'columns' => [
		    [
                'attribute'=>'nombre',
                'label'=>'Version #',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],

			[
                'attribute'=>'tipo',
                'label'=>'Tipo',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],

        ],
    ],
	[
		'columns' => [
		    [
                'attribute'=>'caja',
                'label'=>'Caja',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],

            [
                'attribute'=>'motor',
                'label'=>'Motor',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],

			],
	],
	[
		'columns' => [
		    [
                'attribute'=>'anio',
                'label'=>'Año',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],
            [
                'attribute'=>'carga',
                'label'=>'carga',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],

			],
	],
    [
		  'columns' => [
			[
                'attribute'=>'pasajeros',
                'label'=>'Pasajeros',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],

            [
                'attribute'=>'ws_inhabilitado',
                'label'=>'Estatus',
                'format'=>'raw',
                'type'=>DetailView::INPUT_SWITCH,
                'widgetOptions' => [
                    'pluginOptions' => [
                        'onText' => 'Activo',
                        'offText' => 'Inactivo',
                    ]
                ],
                'value'=>($model->ws_inhabilitado==1)? '<span class="label label-success">Activo</span>' : '<span class="label label-danger">Inactivo</span>',
                //'valueColOptions'=>['style'=>'width:10%']
            ],

        ],
    ],
	];
//print_r($model);
	// View file rendering the widget
		echo DetailView::widget([
		'model' => $model,
		'attributes' => $attributes,
		'mode' => 'view',
		'enableEditMode'=> false,
		//'bordered' => 'bordered',
		//'striped' => $striped,
		//'condensed' => $condensed,
		'responsive' => 'responsive',
		//'hover' => $hover,
		//'hAlign'=>$hAlign,
		//'vAlign'=>$vAlign,
		//'fadeDelay'=>$fadeDelay,
		'deleteOptions'=>[ // your ajax delete parameters
			'params' => ['id' => 1000, 'kvdelete'=>true],
		],
		'panel'=>[
        'heading'=>'Version',
        'type'=>DetailView::TYPE_INFO,

        ],
		'container' => ['id'=>'kv-demo'],
		//'formOptions' => ['action' => Url::current(['update' => 'kv-demo'])] // your action to delete
	]);

	?>

</div>
