<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\icons\Icon;
/* @var $this yii\web\View */
/* @var $model frontend\models\Version */
$this->title ='';
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
          <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
          <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Versiones de vehiculos')?></div></a>
          <a href="#" class="btn btn-default"><div>Actualizar Version</div></a>
</div>
</br></br>
<div class="version-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
