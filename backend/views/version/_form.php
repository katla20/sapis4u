<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\Modelos;
use kartik\select2\Select2;//keyla bullon
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model frontend\models\Version */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="version-form">

    <?php $form = ActiveForm::begin(); ?>

	<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
    <div class="box-header with-border">
         <h4 class="box-title"><!--<i class="glyphicon glyphicon-user"></i>--> Registro de Version</h4>
    </div>

		<div class="box-body">
			<div class="container-items"><!-- widgetBody -->
				<div class="row">
				    <div class="col-sm-3">
						<?php

								$id_modelo = ArrayHelper::map(Modelos::find()->all(), 'id_modelo', 'nombre');

								echo $form->field($model, 'id_modelo2')->widget(Select2::classname(), [
								'data' => $id_modelo,
								'language' => 'es',
								'options' => ['placeholder' => 'Seleccione una Modelo ...'],
								'pluginOptions' => [
								'allowClear' => true
							],
						]);
						?>
					</div>
					<div class="col-sm-3">
						<?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
					</div>

					<div class="col-sm-3">
						<?= $form->field($model, 'tipo')->textInput(['maxlength' => true]) ?>
					</div>



				</div>
				<div class="row">
				    <div class="col-sm-3">

						<?= $form->field($model, 'caja')->textInput(['maxlength' => true]) ?>
					</div>
					<div class="col-sm-3">
						 <?= $form->field($model, 'motor')->textInput(['maxlength' => true]) ?>
					</div>
					<div class="col-sm-3">
						<?= $form->field($model, 'anio')->textInput() ?>
					</div>



				</div>
				<div class="row">
					<div class="col-sm-3">
						<?= $form->field($model, 'valor')->textInput() ?>
					</div>
					<div class="col-sm-3">
						 <?= $form->field($model, 'carga')->textInput() ?>
					</div>
					<div class="col-sm-3">
						<?= $form->field($model, 'pasajeros')->textInput() ?>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-3">
						<?= $form->field($model, 'peso')->textInput() ?>
					</div>
					<div class="col-sm-3">
						  <?= $form->field($model, 'lista')->textInput() ?>
					</div>
					<div class="col-sm-2">
						<?php
						//echo $form->field($model, 'estatus')->dropDownList([1 => 'Activo', 0 => 'Inactivo']);
						echo $form->field($model, 'ws_inhabilitado')->widget(Select2::classname(), [
								'data' => array("0"=>"Activo","1"=>"Inactivo"),
								'language' => 'es',
								//'options' => ['placeholder' => 'Seleccione un Esatdo ...'],
								'pluginOptions' => [
								'allowClear' => true
								],
						]);
						?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2">
						<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
					</div>

				</div>


			</div>
		</div>
    </div>



    <?php ActiveForm::end(); ?>

</div>
