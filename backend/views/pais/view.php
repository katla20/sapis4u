<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model frontend\models\Pais */

$this->title ='';
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
            <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
            <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Pais')?></div></a>
            <a href="#" class="btn btn-default"><div>Datos del Pais</div></a>
</div>
</br></br>

<div class="pais-view">
	<?php
	$attributes = [
    [
        'columns' => [
		    [
                'attribute'=>'id_pais',
				        'label'=>'País',
                'format'=>'raw',
                'value'=>'<kbd>'.$model->id_pais.'</kbd>',
                'valueColOptions'=>['style'=>'width:15%'],
                'displayOnly'=>true
            ],
            [
                'attribute'=>'nombre',
                'label'=>'Nombre',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:45%']
            ],
        ],
    ],
	[
        'columns' => [
            [
                'attribute'=>'estatus',
                'label'=>'Estatus',
                'format'=>'raw',
                'type'=>DetailView::INPUT_SWITCH,
                'widgetOptions' => [
                    'pluginOptions' => [
                        'onText' => 'Activo',
                        'offText' => 'Inactivo',
                    ]
                ],
                'value'=>($model->estatus==1)? '<span class="label label-success">Activo</span>' : '<span class="label label-danger">Inactivo</span>',

            ],
            [
                'attribute'=>'descripcion',
                'label'=>'Descripcion',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:45%']
            ],

        ],
    ],
	];


	echo DetailView::widget([
		'model' => $model,
		'attributes' => $attributes,
		'mode' => 'view',
		'enableEditMode'=> false,
		//'bordered' => 'bordered',
		//'striped' => $striped,
		//'condensed' => $condensed,
		'responsive' => 'responsive',
		//'hover' => $hover,
		//'hAlign'=>$hAlign,
		//'vAlign'=>$vAlign,
		//'fadeDelay'=>$fadeDelay,
		'deleteOptions'=>[ // your ajax delete parameters
			'params' => ['id' => 1000, 'kvdelete'=>true],
		],
		'panel'=>[
        'heading'=>'PAIS',
        'type'=>DetailView::TYPE_INFO,

        ],
		'container' => ['id'=>'kv-demo'],
	]);

    ?>

</div>