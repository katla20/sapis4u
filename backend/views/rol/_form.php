<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Rol */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rol-form">
<?php $form = ActiveForm::begin(); ?>
	<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
		<div class="box-header with-border">
			 <h4 class="box-title"><!--<i class="glyphicon glyphicon-user"></i>--> Registrar Rol</h4>
		</div>
		<div class="box-body">
			<div class="container-items"><!-- widgetBody -->
				<div class="row" >
					<div class="col-sm-5">
					    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
					</div>
				</div>
			</div>
		</div>
	</div>



	<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
		<div class="box-header with-border">
			 <h4 class="box-title"><!--<i class="glyphicon glyphicon-user"></i>--> Modulos</h4>
		</div>
		<div class="box-body">
			<div class="container-items"><!-- widgetBody -->
				<div class="row" >

							<div class="col-sm-6 three-columns">
							<P>
								<?php
									$opciones = \yii\helpers\ArrayHelper::map($tipoOperaciones, 'id', 'nombre');
									echo $form->field($model, 'operaciones')->checkboxList($opciones, ['unselect'=>NULL])->label(false);
								?>
							</P>
							</div>
				</div>
			</div>
		</div>
	</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Editar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

$this->registerCss("
    p {
        font-size: 15px;
        font-size: 1.5rem;
        line-height: 1.333333334;
        margin-bottom: 20px;
        margin-bottom: 2rem;
        text-align: justify;
    }

    .three-columns {

        -webkit-column-count: 4; /* Chrome, Safari, Opera */
        -moz-column-count: 4; /* Firefox */
        column-count: 4;

        -webkit-column-gap: 30px; /* Chrome, Safari, Opera */
        -moz-column-gap: 30px; /* Firefox */
        column-gap: 30px;

        -webkit-column-gap: 2rem; /* Chrome, Safari, Opera */
        -moz-column-gap: 2rem; /* Firefox */
        column-gap: 2rem;

    }
"

);

?>
