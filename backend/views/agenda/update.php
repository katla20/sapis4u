<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Agenda */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Agenda',
]) . $model->id_agenda;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agendas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_agenda, 'url' => ['view', 'id' => $model->id_agenda]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="agenda-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
