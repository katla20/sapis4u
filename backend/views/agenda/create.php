<?php

use yii\helpers\Html;
use kartik\icons\Icon;
use yii\helpers\Url;



/* @var $this yii\web\View */
/* @var $model backend\models\Agenda */

$this->title = '';
?>
<div class="bg-aqua">
      <?php echo Icon::show('calendar', ['class' => 'fa-3x','style'=>'padding:2px auto;']);?>
	  <span style="color:white;font-size:18px;font-weight:bold;">
	  <?=Yii::t('app', 'Crear Evento');?></span>
</div>
<div class="agenda-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
