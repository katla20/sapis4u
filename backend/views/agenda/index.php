<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;
use kartik\icons\Icon;
use yii\helpers\Url;

Icon::map($this);



/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\AgendaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "";
//$this->params['breadcrumbs'][] = $this->title;
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
          <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
          <a href="#" class="btn btn-default"><div>Agendar</div></a>
</div>
</br></br>
<div class="bg-aqua">
      <?php echo Icon::show('calendar', ['class' => 'fa-3x','style'=>'padding:2px auto;']);?>
	  <span style="color:white;font-size:18px;font-weight:bold;">
	  <?=Yii::t('app', 'AGENDAR');?></span>
</div>
<div class="agenda-index">

	<?php
    Modal::begin([
          'id' =>'modal',
          'size'=>'modal-md'
    ]);
     echo "<div id='modalContent'></div>";
    Modal::end();
	

?>

	  
<?php 
//$html='<div class="material-button material-button-toggle" >';
    $html='<span class="fa fa-plus" aria-hidden="true"></span>';
	//$html.='</div>'; 
	  
$JSClickEvent = <<<EOF

function(calEvent, jsEvent, view) {
                alert('Titulo: '+ calEvent.title +' Descripcion'+ calEvent.description);
				$(this).css('border-color', 'red');

}      
EOF;

?>	

<?= \yii2fullcalendar\yii2fullcalendar::widget(array(
	        'options'=>[
			    'id'=>'calendar',
			],
			'clientOptions' => [
				'eventClick'=> new \yii\web\JsExpression($JSClickEvent),
				
			],
            'events'=> $events,
			
			
    
      ));
?>  
	   
	  

