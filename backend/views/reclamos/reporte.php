<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\TipoReclamos;
use backend\models\Recibo;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use kartik\icons\Icon;
use yii\bootstrap\Collapse;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\ReclamosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = "";
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
<a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
</div>
</br></br>
<div class="reclamos-index">
     <?php  echo Collapse::widget([
        
            'items' =>[
            
                [
                    'label' => 'Filtros de Busqueda',
                    'content' => $this->render('_search', ['model' => $searchModel]),
                ],
            ]
        
        ]);
    
    
    ?>
	<?php
	$modelTreclamo = new TipoReclamos();
	$modelRecibo = new Recibo();
	$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
				'attribute'=>'numero_reclamo',
				'value'=>'numero_reclamo',
				'width'=>'100px',
		],
    [
      'attribute'=>'recibo',
      'width'=>'100px',
      'value'=>'recibo',
    ],
    [
      'attribute'=>'tipo_reclamo',
      'width'=>'200px',
      'value'=>'tipo_reclamo',
    ],

    'descripcion:ntext',
    'monto',
    
    [
  		   'attribute'=>'nombre_completo',
  		   'width'=>'350px',
  		   'value'=>'nombre_completo'
        ],
        [
  		   'attribute'=>'ramo',
  		   'width'=>'100px',
  		   'value'=>'ramo'
        ],
        [
  		   'attribute'=>'aseguradora',
  		   'width'=>'150px',
  		   'value'=>'aseguradora'
        ],

    [
     'class' => 'yii\grid\ActionColumn',
     'template' => ''
          

    ],
	];

	echo GridView::widget([
        'dataProvider' => $dataProvider,
		'columns' => $gridColumns,
		'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
		'toolbar' => [
			[
				'content'=>
					Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
						'class' => 'btn btn-default',
						'title' => Yii::t('app', 'Reset Grid')
					]),

			],
			'{export}',
			'{toggleData}'
		],
    'exportConfig' => [
      GridView::EXCEL => [],
      GridView::TEXT => [],
      GridView::PDF => [],
    ],

		'pjax' => false,
		'pjaxSettings' => [
			'options' => ['id' => 'pajax-reclamos'],// UNIQUE PJAX CONTAINER ID
		],
		'bordered' => false,
		'resizableColumns'=>true,
		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
		'striped' => true,
		'condensed' => false,
		'responsive' => true,
		'hover' => true,
		'showPageSummary' => false,
    'panel' => [
        'heading'=>'<h2 class="panel-title" style="font-weight:bold;">'.Icon::show('car', ['class' => 'fa-2x']).'&nbsp; &nbsp;REPORTE DE RECLAMOS</h2>',
        'type' => GridView::TYPE_INFO
    ],
    ]);
	?>

</div>
