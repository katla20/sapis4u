<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\TipoReclamos;
use backend\models\Recibo;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\ReclamosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = "";
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
<a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
</div>
</br></br>
<div class="reclamos-index">
	<?php
	$modelTreclamo = new TipoReclamos();
	$modelRecibo = new Recibo();
	$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
				'attribute'=>'numero_reclamo',
				'value'=>'numero_reclamo',
				'width'=>'100px',
		],
    [
      'attribute'=>'id_recibo',
      'width'=>'310px',
      'value'=>'idRecibo.nro_recibo',
      'filterType'=>GridView::FILTER_SELECT2,
      'filter'=>ArrayHelper::map(Recibo::find()->orderBy('nro_recibo')->asArray()->all(), 'id_recibo', 'nro_recibo'),
      'filterWidgetOptions'=>[
        'pluginOptions'=>['allowClear'=>true],
      ],
      'filterInputOptions'=>['placeholder'=>'Recibos','id' => 'recibo-select2-id'],
      //'group'=>true,  // enable grouping
    ],
    [
      'attribute'=>'id_tipo_reclamo',
      'width'=>'310px',
      'value'=>'idTipoReclamos.nombre',
      'filterType'=>GridView::FILTER_SELECT2,
      'filter'=>ArrayHelper::map(TipoReclamos::find()->orderBy('nombre')->asArray()->all(), 'id_tipo_reclamo', 'nombre'),
      'filterWidgetOptions'=>[
        'pluginOptions'=>['allowClear'=>true],
    ],
      'filterInputOptions'=>['placeholder'=>'Tipo de reclamos','id' => 'estado-select2-id'],
      //'group'=>true,  // enable grouping
    ],
    'descripcion:ntext',
          //'monto',
    [
     'class'=>'kartik\grid\BooleanColumn',
     'attribute' => 'estatus',
     'filterType'=>GridView::FILTER_SELECT2,
     'vAlign'=>'middle',
     'width'=>'200px',
    ],
    [
     'class' => 'yii\grid\ActionColumn',
     'template' => '{update} {seguimiento} {mensaje}',
           'buttons' => [
             'update' => function ($url,$model, $key) {
                 return Html::a('<i class="fa fa-pencil fa-lg" aria-hidden="true"></i>', [($model->getTipoRamo($model->id_reclamo)==1)? 'update' : 'salud','id' => $model->id_reclamo], [
                     'id' => 'activity-index-link',
                     'title' => Yii::t('app', 'Actualizar Reclamo'),'rel'=>'tooltip',
                     'data-pjax' => '0',
                 ]);
             },
         'seguimiento' => function ($url,$model, $key) {
                 return Html::a('<i class="fa fa-plus fa-lg" aria-hidden="true"></i>', ['seguimiento','id' => $model->id_reclamo], [
                     'id' => 'activity-index-link',
                     'title' => Yii::t('app', 'Seguimiento de Reclamo'),'rel'=>'tooltip',
                     'data-pjax' => '0',
                 ]);
             },
       'mensaje' => function ($url,$model, $key) {
                 return Html::a('<span class="glyphicon glyphicon-envelope"></span>', '#', [
                    'id' => 'activity-index-link',
                    'title' => Yii::t('app', 'Crear Mensaje'),'rel'=>'tooltip',
                    'data-toggle' => 'modal',
                    'data-target' => '#modal',
                    'data-url' => Url::to(['mensaje', 'id' => $model->id_reclamo]),
                    'data-pjax' => '0',
                ]);
             }
           ]

    ],
	];

	echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
		'columns' => $gridColumns,
		'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
		'toolbar' => [
			[
				'content'=>
					Html::a('<span class="glyphicon glyphicon-plus"></span> Auto.', ['create'],[
						//'type'=>'button',
						'title'=>Yii::t('app', 'Reclamo Automovil'),'rel'=>'tooltip',
						'class'=>'btn btn-success'
					]) . ' '.
    	            Html::a('<span class="glyphicon glyphicon-plus"></span> Salud', ['salud'],[
						//'type'=>'button',
						'title'=>Yii::t('app', 'Reclamo Salud'),'rel'=>'tooltip',
						'class'=>'btn btn-success'
					]) . ' '.
					Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
						'class' => 'btn btn-default',
						'title' => Yii::t('app', 'Actualizar Grid'),'rel'=>'tooltip',
					]),

			],
			'{export}',
			'{toggleData}'
		],
    'exportConfig' => [
      GridView::EXCEL => [],
      GridView::TEXT => [],
      GridView::PDF => [],
    ],

		'pjax' => false,
		'pjaxSettings' => [
			'options' => ['id' => 'pajax-reclamos'],// UNIQUE PJAX CONTAINER ID
		],
		'bordered' => false,
		'resizableColumns'=>true,
		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
		'striped' => true,
		'condensed' => false,
		'responsive' => true,
		'hover' => true,
		'showPageSummary' => false,
        'panel' => [
        'heading'=>'<h2 class="panel-title" style="font-weight:bold;">'.Icon::show('bullhorn', ['class' => 'fa-2x']).'&nbsp; &nbsp;RECLAMOS</h2>',
        'type' => GridView::TYPE_INFO
    ],
    ]);
	?>

</div>


<?php
$this->registerJs(
"
//alert($(this).width());
$(document).on('click', '#activity-index-link', (function() {
    $.get(
        $(this).data('url'),
        function (data) {
            $('.modal-body').html(data);
            $('#modal').modal({
				backdrop: true,
				keyboard: true
			}).css({
				 width: '100%',
				 heigth: 'auto'

			});

        }
    );
}));
$('#modal .modal-dialog').css({
				 width: '40%',
				 heigth: '40%'});


"
); ?>

<?php
$string = <<<EXP2
$("[rel=tooltip]").tooltip({ placement: 'top'});
EXP2;
$this->registerJs($string, \yii\web\View::POS_READY);
$string='
.fa{margin:0px 4px 0px 0px;}
 .fa-trash{
     color:  #d9534f;
 }
 .fa-plus{
     color:  #5cb85c;
 }
 .fa-retweet{
     color:  #5bc0de;
 }
 .glyphicon-envelope{
     color:  #39CCCC;
 }';
$this->registerCss($string);
?>

<?php
Modal::begin([
 'options' => [
        'id' => 'modal',
        'tabindex' => false, // important for Select2 to work properly
  ],
'header' => '<h4 class="modal-title">Crear Mensaje</h4>',
//'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Cerrar</a>',
]);

Modal::end();
?>
