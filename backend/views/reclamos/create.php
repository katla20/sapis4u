<?php

use yii\helpers\Html;
use kartik\icons\Icon;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model frontend\models\Reclamos */
$this->title = '';//Yii::t('app', 'Create Reclamos');
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
          <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
          <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Reclamos')?></div></a>
          <a href="#" class="btn btn-default"><div>Crear Reclamo Automovil</div></a>
</div>
</br></br>
  <div class="reclamos-create">
      <div class="bg-aqua top-modulo">
          <span class="icon-modulo"><?=Icon::show('car', ['class' => 'fa-3x'])?></span>
          <span style="color:white;font-size:18px;font-weight:bold;"><?=Yii::t('app', 'Crear Reclamo Automovil');?></span>
      </div>
	  <?= $this->render('_form', [
        'model' => $model,
		'tipoPartes' => $tipoPartes
    ]) ?>
  </div>
<?php $string=".top-modulo{padding:2px 2px 2px 8px;}
          .icon-modulo{margin:0px 4px 0px 0px}";
      $this->registerCss($string);
?>
