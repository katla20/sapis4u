<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\ReclamoSeguimiento;
use backend\models\Modelos;
use backend\models\User2;
use kartik\icons\Icon;
use kartik\form\ActiveField;
use yii\helpers\Json;
use kartik\select2\Select2;
//use kartik\depdrop\Depdrop;
use kartik\widgets\DepDrop;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;
use yii\web\Response;
use kartik\money\MaskMoney;
use kartik\form\ActiveForm;
use backend\models\TipoReclamos;
use backend\models\SeguimientoR;
use kartik\detail\DetailView;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\PolizaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//http://www.w3schools.com/colors/colors_picker.asp colores

$this->title = "Registrar Seguimiento";
$this->params['breadcrumbs'][] = $this->title;


//print_r($model);
?>
<div class="mensaje">
<!-- Content Wrapper. Contains page content
<div class="content-wrapper">-->

	<div class="mensaje-form">

		<?php $form = ActiveForm::begin(); ?>

		<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
		-moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
		box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">

			<div class="box-body">
				<div class="container-items"><!-- widgetBody -->

					<div class="row">

						<div class="col-sm-5">
						<?= $form->field($modelMensaje, 'comentario',[
                                                  'addon' => [
                                                      'prepend' => [
                                                          'content' => '<i class="glyphicon glyphicon-list-alt"></i>'

                                                      ]
                                                  ]
                                              ])->textarea(['rows' => 2]) ?>
						</div>
						
					</div>
                    <div class="row">

						<div class="col-sm-5">
							<?php

							  echo $form->field($modelMensaje,"user_destino",[
                                                  'addon' => [
                                                      'prepend' => [
                                                          'content' => '<i class="glyphicon glyphicon-user"></i>'

                                                      ]
                                                  ]
                                              ])->widget(Select2::classname(), [
                                         'data' => ArrayHelper::map(User2::find()->where("status = :status ", [':status' => 10])
                                                                                 ->select("(cedula||'--'||nombre||'--'||apellido) AS nombre ,id")
                                                                                 ->all(), 'id', 'nombre'),
                                        'language' => 'en',
                                        'options' => ['placeholder' => 'Select a Usuario destino'],
                                        'pluginOptions' => [
                                        'allowClear' => true
                                        ],
                                ]);
                                
                        
                            ?>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
						<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Crear Mensaje'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php ActiveForm::end(); ?>
	</div>
<?php
$script=<<< JS
 $(".three-columns").hide();
JS;
$this->registerJs($script);

?>
