<?php

use yii\helpers\Html;
use kartik\icons\Icon;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\models\Reclamos */
$this->title = '';
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
          <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
          <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Reclamos')?></div></a>
          <a href="#" class="btn btn-default"><div><?=Yii::t('app', 'Update')?> Reclamo Automovil</div></a>
</div>
</br></br>
<div class="reclamos-update">
    <?= $this->render('_form', [
        'model' => $model,
		    'tipoPartes' => $tipoPartes
    ]) ?>
</div>
