<?php

/**
 * @author www.intercambiosvirtuales.org
 * @copyright 2016
 */

<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\TipoReclamos;
use backend\models\Recibo;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\ReclamosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Reclamos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reclamos-index">
	<?php
	
	$modelTreclamo = new TipoReclamos();
	$modelRecibo = new Recibo();
	$gridColumns = [
		['class' => 'kartik\grid\SerialColumn'],
		[
			'class' => 'kartik\grid\EditableColumn',
			'attribute' => 'name',
			'pageSummary' => 'Page Total',
			'vAlign'=>'middle',
			'headerOptions'=>['class'=>'kv-sticky-column'],
			'contentOptions'=>['class'=>'kv-sticky-column'],
			'editableOptions'=>['header'=>'Name', 'size'=>'md']
		],
		[
			'attribute'=>'color',
			'value'=>function ($model, $key, $index, $widget) {
				return "<span class='badge' style='background-color: {$model->color}'> </span>  <code>" . 
					$model->color . '</code>';
			},
			'filterType'=>GridView::FILTER_COLOR,
			'vAlign'=>'middle',
			'format'=>'raw',
			'width'=>'150px',
			'noWrap'=>true
		],
		[
			'class'=>'kartik\grid\BooleanColumn',
			'attribute'=>'estatus', 
			'vAlign'=>'middle',
		],
		[
			'class' => 'kartik\grid\ActionColumn',
		
			'dropdown' => true,
			'vAlign'=>'middle',
			'urlCreator' => function($action, $model, $key, $index) { return '#'; },
			//'viewOptions'=>['title'=>$viewMsg, 'data-toggle'=>'tooltip'],
			//'updateOptions'=>['title'=>$updateMsg, 'data-toggle'=>'tooltip'],
			//'deleteOptions'=>['title'=>$deleteMsg, 'data-toggle'=>'tooltip'], 
		],
		['class' => 'kartik\grid\CheckboxColumn']
	];
	
	echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'columns' => $gridColumns,
		'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            
			[
				'attribute'=>'numero_reclamo', 
				'value'=>'numero_reclamo',
				'width'=>'100px',
			],	
            

            
			[
				'attribute'=>'id_recibo', 
				'width'=>'310px',
				'value'=>'idRecibo.nro_recibo',
				'filterType'=>GridView::FILTER_SELECT2,
				'filter'=>ArrayHelper::map(Recibo::find()->orderBy('nro_recibo')->asArray()->all(), 'id_recibo', 'nro_recibo'), 
				'filterWidgetOptions'=>[
					'pluginOptions'=>['allowClear'=>true],
				],
				'filterInputOptions'=>['placeholder'=>'Recibos','id' => 'recibo-select2-id'],
				//'group'=>true,  // enable grouping
			],
			[
				'attribute'=>'id_tipo_reclamo', 
				'width'=>'310px',
				'value'=>'idTipoReclamos.nombre',
				'filterType'=>GridView::FILTER_SELECT2,
				'filter'=>ArrayHelper::map(TipoReclamos::find()->orderBy('nombre')->asArray()->all(), 'id_tipo_reclamo', 'nombre'), 
				'filterWidgetOptions'=>[
					'pluginOptions'=>['allowClear'=>true],
				],
				'filterInputOptions'=>['placeholder'=>'Tipo de reclamos','id' => 'estado-select2-id'],
				//'group'=>true,  // enable grouping
			],
			'descripcion:ntext',
            //'monto',
			[
			 'attribute' => 'estatus',
			 'value'=>'estatus',
			 'filterType'=>GridView::FILTER_SELECT2,
			 'filter' => array(""=>"","1"=>"Activo","0"=>"Inactivo"),
			],

            [
			 'class' => 'yii\grid\ActionColumn',
			 'template' => '{view} {update} {seguimiento} {mensaje}',
             'buttons' => [
               'update' => function ($url,$model, $key) {
                   return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [($model->getTipoRamo($model->id_reclamo)==1)? 'update' : 'salud','id' => $model->id_reclamo], [
                       'id' => 'activity-index-link',
                       'title' => Yii::t('app', 'Seguimiento de Reclamo'),
                       'data-pjax' => '0',
                   ]);
               },
			    'seguimiento' => function ($url,$model, $key) {
                   return Html::a('<span class="glyphicon glyphicon-plus"></span>', ['seguimiento','id' => $model->id_reclamo], [
                       'id' => 'activity-index-link',
                       'title' => Yii::t('app', 'Seguimiento de Reclamo'),
                       'data-pjax' => '0',
                   ]);
               },
			   'mensaje' => function ($url,$model, $key) {
                   return Html::a('<span class="glyphicon glyphicon-envelope"></span>', '#', [
                      'id' => 'activity-index-link',
                      'title' => Yii::t('app', 'Crear Mensaje'),
                      'data-toggle' => 'modal',
                      'data-target' => '#modal',
                      'data-url' => Url::to(['mensaje', 'id' => $model->id_reclamo]),
                      'data-pjax' => '0',
                  ]);
               }
             ]
			
			],
        ],
		'toolbar' => [
			[
				'content'=>
					Html::a('Auto.', ['create'],[
						//'type'=>'button', 
						'title'=>Yii::t('app', 'Reclamo Automovil'), 
						'class'=>'btn btn-success'
					]) . ' '.
    	            Html::a('Salud', ['salud'],[
						//'type'=>'button', 
						'title'=>Yii::t('app', 'Reclamo Salud'), 
						'class'=>'btn btn-success'
					]) . ' '.
					Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
						'class' => 'btn btn-default', 
						'title' => Yii::t('app', 'Reset Grid')
					]),
				
			],
			'{export}',
			'{toggleData}'
		],
		 
		'pjax' => false,
		'pjaxSettings' => [
			'options' => ['id' => 'ciudad-pjax-id'],// UNIQUE PJAX CONTAINER ID
		],
		'bordered' => false,
		'resizableColumns'=>true,
		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
		'striped' => true,
		'condensed' => false,
		'responsive' => true,
		'hover' => true,
		//'floatHeader' => true,
		//'floatHeaderOptions' => ['scrollingTop' => '50'],
		'showPageSummary' => false,
		'panel' => [
			'type' => GridView::TYPE_PRIMARY
		],
    ]);
	?>

</div>


<?php
$this->registerJs(
"
//alert($(this).width());
$(document).on('click', '#activity-index-link', (function() {
    $.get(
        $(this).data('url'),
        function (data) {
            $('.modal-body').html(data);
            $('#modal').modal({
				backdrop: true,
				keyboard: true
			}).css({
				 width: '100%',
				 heigth: 'auto'
				
			});

			
			
        }
    );
}));
$('#modal .modal-dialog').css({
				 width: '40%',
				 heigth: '40%'});


"
); ?>

<?php
Modal::begin([
 'options' => [
        'id' => 'modal',
        'tabindex' => false, // important for Select2 to work properly
  ],
'header' => '<h4 class="modal-title">Crear Mensaje</h4>',
//'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Cerrar</a>',
]);
echo "<div class='well'></div>";

Modal::end();
?>



?>