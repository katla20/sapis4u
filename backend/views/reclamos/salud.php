<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\TipoReclamos;
use kartik\file\FileInput;
use kartik\select2\Select2;
//use kartik\depdrop\Depdrop; //http://demos.krajee.com/widget-details/depdrop
use kartik\widgets\DepDrop;
use backend\models\Poliza;
use backend\models\Recibo;
use yii\helpers\Url;
use kartik\icons\Icon;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use backend\models\Cliente;
use backend\models\Persona;
use kartik\checkbox\CheckboxX;
use backend\models\User2;

/* @var $this yii\web\View */
/* @var $model frontend\models\Reclamos */
/* @var $form yii\widgets\ActiveForm */
$this->title = '';

if($model->isNewRecord ){
	
}else{
	
$model->fecha_ocurrencia=date("d-m-Y", strtotime($model->fecha_ocurrencia));
$model->fecha_notificacion=date("d-m-Y", strtotime($model->fecha_notificacion));
$model->fecha_declaracion=date("d-m-Y", strtotime($model->fecha_declaracion));
}
?>

<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
          <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
          <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Reclamos')?></div></a>
          <a href="#" class="btn btn-default"><div>Crear Reclamo Salud</div></a>
</div>
</br></br>
<div class="reclamos-salud">
    <div class="bg-aqua top-modulo">
          <span class="icon-modulo"><i class="fa fa-heartbeat fa-3x" aria-hidden="true"></i></span>
          <span style="color:white;font-size:18px;font-weight:bold;"><?=Yii::t('app', 'Crear Reclamo Salud');?></span>
    </div>
    <?php $form = ActiveForm::begin(); ?>
	  <?=$form->field($model, 'hi')->widget(CheckboxX::classname());?>
	<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
    <div class="box-header">
         <h4 class="box-title">DATOS DEL RECLAMO</h4>
    </div>

		<div class="box-body">
			<div class="container-items"><!-- widgetBody -->
				<div class="row">
				
                   <div class="col-sm-3">
                    <?php
					
							 echo $form->field($model,"poliza")->widget(Select2::classname(), [
                             'data' => ArrayHelper::map(Poliza::find()->innerJoin('recibo', 'poliza.id_poliza=recibo.id_poliza AND poliza.fecha_vigenciadesde >= recibo.fecha_vigenciadesde AND poliza.fecha_vigenciahasta <= recibo.fecha_vigenciahasta')
							                                           ->innerJoin('certificado', 'recibo.id_recibo=certificado.id_recibo')
																	   ->innerJoin('certificado_asegurados', 'certificado.id_certificado=certificado_asegurados.id_certificado')
							                                          ->innerJoin('cliente', 'certificado_asegurados.id_cliente=cliente.id_cliente')
													                  ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
                                                                      ->where("poliza.estatus = :status AND poliza.id_ramo= 2", [':status' => 1])
                                                                      ->select("(numero_poliza||'--'||identificacion) AS numero_poliza ,poliza.id_poliza")
                                                                      ->all(), 'id_poliza', 'numero_poliza'),
																	 
																	 
                            'language' => 'en',
                            'options' => ['placeholder' => 'Select a Poliza'],
                            'pluginOptions' => [
                            'allowClear' => true
                            ],
                    ]);
					
					echo Html::hiddenInput('identidad', $model->identidad, ['id'=>'identidad']);
                ?>
                </div>
				    <div class="col-sm-3">

						<?php
                 if (!$model->isNewRecord) {
                      echo Html::hiddenInput('recibo', $model->id_recibo, ['id'=>'recibo']);
                 }

                 echo $form->field($model, "id_recibo")->widget(DepDrop::classname(), [
                							//'data'=> [''=>'Seleccione'],
                							'options'=>['placeholder'=>'Selecione ...'],
                							'type' => DepDrop::TYPE_SELECT2,
                							'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                							'pluginOptions'=>[
                							'depends'=>["reclamos-poliza"],
                                             'initialize'=>true,
                							'placeholder' => 'Seleccione ...',
                							'url'=>Url::to(['dependent-dropdown/child-recibo', 'db'=>Recibo::classname(),'cmpo_dep'=>'id_poliza','cmpo_mostrar'=>'nro_recibo','id_cmpo'=>'id_recibo']),
                							'loadingText' => 'Leyendo ...',
                              'params'=>['recibo'] ///SPECIFYING THE PARAM
                							]
							]);

						?>
					</div>
					<div class="col-sm-3">
							<?php

							  echo $form->field($model,"id_user_analista")->widget(Select2::classname(), [
                                         'data' => ArrayHelper::map(User2::find()->where("status = :status ", [':status' => 10])
                                                                                 ->select("(cedula||'--'||nombre||'--'||apellido) AS nombre ,id")
                                                                                 ->all(), 'id', 'nombre'),
                                        'language' => 'en',
                                        'options' => ['placeholder' => 'Select a Usuario destino'],
                                        'pluginOptions' => [
                                        'allowClear' => true
                                        ],
                                ]);
                                
                        
                            ?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
						<?=$form->field($model, 'fecha_ocurrencia',[
													'addon' => [
														'prepend' => [
															'content' => '<i class="glyphicon glyphicon-calendar"></i>'

														]
													]
												])->widget(\yii\widgets\MaskedInput::className(),
													[
														'clientOptions' => ['alias' =>  'date']
													])
						?>
					</div>
					<div class="col-sm-3">
						<?=$form->field($model, 'fecha_notificacion',[
													'addon' => [
														'prepend' => [
															'content' => '<i class="glyphicon glyphicon-calendar"></i>'

														]
													]
												])->widget(\yii\widgets\MaskedInput::className(),
													[
														'clientOptions' => ['alias' =>  'date']
													])
						?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
						<?=$form->field($model, 'fecha_declaracion',[
													'addon' => [
														'prepend' => [
															'content' => '<i class="glyphicon glyphicon-calendar"></i>'

														]
													]
												])->widget(\yii\widgets\MaskedInput::className(),
													[
														'clientOptions' => ['alias' =>  'date']
													])
						?>
					</div>
					<div class="col-sm-3">
						<?= $form->field($model, 'numero_reclamo')->textInput() ?>
					</div>
				
				</div>
				<div class="row">
				    
					<div class="col-sm-3">
						<?php
						echo $form->field($model,"id_tipo_reclamo")->widget(Select2::classname(), [
						'data' => ArrayHelper::map(TipoReclamos::find()->where("id_ramo = :id ", [':id' => 2])->all(), 'id_tipo_reclamo', 'nombre'),
						'language' => 'en',
						'options' => ['placeholder' => 'Select ...'],
						'pluginOptions' => [
						'allowClear' => true
						],
						 ]);

						?>
					</div>
					<div class="col-sm-3">

						<?php
						echo $form->field($model, 'causa')->widget(Select2::classname(), [
								'data' => array("1"=>"Robo","2"=>"Inundacion"),
								'language' => 'es',
								//'options' => ['placeholder' => 'Seleccione un Esatdo ...'],
								'pluginOptions' => [
								'allowClear' => true
								],
						]);	
						?>
					</div>
					
				</div>
				<div class="row">
					<div class="col-sm-6">
						<?= $form->field($model, 'descripcion')->textarea(['rows' => 3])->label('Observacion') ?>
					</div>
				</div>



			</div>
		</div>
    </div>

	<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1); display: none;">
		<div class="box-header">
			 <h4 class="box-title"><!--<i class="glyphicon glyphicon-user"></i>--> Partes del Vheiculo</h4>
		</div>
		<div class="box-body">
			<div class="container-items"><!-- widgetBody -->
				<div class="row" >

							<div class="col-sm-6 three-columns">
							<P>
								<?php
								$opciones = \yii\helpers\ArrayHelper::map($tipoPartes, 'id_parte', 'nombre');
								echo $form->field($model, 'parte')->checkboxList($opciones, ['unselect'=>NULL])->label(false);
								?>
							</P>
							</div>
				</div>
			</div>
		</div>
	</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script=<<< JS
 //alert('hola');
 $(".field-reclamos-hi").hide();
 // $('input[type="checkbox"]').checkboxX({threeState: false, size:'md'});
 
 $(function(){

    
	$( "#reclamos-poliza" ).change(function() {
		var identidad=$("#reclamos-poliza option:selected").text();
		var res = identidad.split("--");
		res = res.slice(1,2);
		$("#identidad").val(res);
      // alert($("#identidad").val());
	   //alert(res);
    });

 });	
 
JS;
$this->registerJs($script);
?>
<?php $string=".top-modulo{padding:2px 2px 2px 8px;}
               .icon-modulo{margin:0px 4px 0px 0px}
	p {
        font-size: 15px;
        font-size: 1.5rem;
        line-height: 1.333333334;
        margin-bottom: 20px;
        margin-bottom: 2rem;
        text-align: justify;
    }

    .three-columns {

        -webkit-column-count: 3; /* Chrome, Safari, Opera */
        -moz-column-count: 3; /* Firefox */
        column-count: 3;

        -webkit-column-gap: 20px; /* Chrome, Safari, Opera */
        -moz-column-gap: 20px; /* Firefox */
        column-gap: 20px;

        -webkit-column-gap: 2rem; /* Chrome, Safari, Opera */
        -moz-column-gap: 2rem; /* Firefox */
        column-gap: 2rem;

    }";
  $this->registerCss($string);
?>

