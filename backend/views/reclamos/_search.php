<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use kartik\widgets\ActiveForm;
use backend\models\Aseguradoras;
use backend\models\Ramos;
use backend\models\Productos;
use backend\models\Cliente;
use backend\models\Persona;
use backend\models\Poliza;
use backend\models\TipoReclamos;
use kartik\select2\Select2;
use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model frontend\models\search\ReclamosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reclamos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['reporte'],
        'method' => 'get',
    ]); ?>
     <div class="box-body">
			<div class="container-items"><!-- widgetBody -->
				<div class="row">
                    <div class="col-sm-3">
                        <?php
        
        							  echo $form->field($model,"poliza")->widget(Select2::classname(), [
                                     'data' => ArrayHelper::map(Poliza::find()->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
        													                                         ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
                                                                             ->where("poliza.estatus = :status AND poliza.id_ramo= 1", [':status' => 1])
                                                                             ->select("(numero_poliza||'--'||identificacion) AS numero_poliza ,id_poliza")
                                                                             ->all(), 'id_poliza', 'numero_poliza'),
                                    'language' => 'en',
                                    'options' => ['placeholder' => 'Select a Poliza'],
                                    'pluginOptions' => [
                                    'allowClear' => true
                                    ],
                            ]);
                        ?>
                    </div>
					<div class="col-sm-3">
						<?= $form->field($model, 'numero_reclamo') ?>
					</div>
                    <div class="col-sm-3">
						<?php
						echo $form->field($model,"id_tipo_reclamo")->widget(Select2::classname(), [
						'data' => ArrayHelper::map(TipoReclamos::find()->all(), 'id_tipo_reclamo', 'nombre'),
						'language' => 'en',
						'options' => ['placeholder' => 'Select ...'],
						'pluginOptions' => [
						'allowClear' => true
						],
						 ]);

						?>
					</div>
                    
				</div>
                <div class="row">
                    <div class="col-sm-3">
						 
						 <?=$form->field($model,"id_aseguradora",[
                                                          'addon' => [
                                                              'prepend' => [
                                                                  'content' => '<i class="fa fa-university"></i>'

                                                              ]
                                                          ]
                                                      ])->widget(Select2::classname(), [
                                 'data' =>   ArrayHelper::map(Aseguradoras::find()->all(),'id_aseguradora', 'nombre'),
                                 'language' => 'en',
                                  'options' => ['placeholder' => 'Seleccione ...'],
                                  'pluginOptions' => [
                                     'allowClear' => true
                                 ],
                               ]);
                             ?>
					</div>
					<div class="col-sm-3">
						<?=$form->field($model,"id_ramo",[
                                                          'addon' => [
                                                              'prepend' => [
                                                                  'content' => '<i class="fa fa-university"></i>'

                                                              ]
                                                          ]
                                                      ])->widget(Select2::classname(), [
                                 'data' =>   ArrayHelper::map(Ramos::find()->all(),'id_ramo', 'nombre'),
                                 'language' => 'en',
                                  'options' => ['placeholder' => 'Seleccione ...'],
                                  'pluginOptions' => [
                                     'allowClear' => true
                                 ],
                               ]);
                             ?>
					</div>
					
					<div class="col-sm-3">
						 
						  <?=$form->field($model,"id_producto",[
                                                          'addon' => [
                                                              'prepend' => [
                                                                  'content' => '<i class="fa fa-university"></i>'

                                                              ]
                                                          ]
                                                      ])->widget(Select2::classname(), [
                                 'data' =>   ArrayHelper::map(Productos::find()->all(),'id_producto', 'nombre'),
                                 'language' => 'en',
                                  'options' => ['placeholder' => 'Seleccione ...'],
                                  'pluginOptions' => [
                                     'allowClear' => true
                                 ],
                               ]);
                             ?>
					</div>
                    
				</div>

				<div class="row">
					<div class="col-sm-3">
					    <?=$form->field($model,'id_contratante', [
                                                          'addon' => [
                                                              'prepend' => [
                                                                  'content' => '<i class="fa fa-university"></i>'

                                                              ]
                                                          ]
                                                      ])->widget(Select2::classname(), [
                                                       'data' =>ArrayHelper::map( Cliente::find()
                                                                                    ->joinwith('idPersona')
                                                                                    ->where("cliente.estatus = :status", [':status' => 1])
                                                                                    ->select("(nombre||' '||segundo_nombre||' '||apellido|| ' ' ||segundo_apellido || ' ' || identificacion) AS nombre_completo ,id_cliente")
                                                                                    ->all(),'id_cliente', 'nombre_completo'),
                                                       'language' => 'en',
                                                        'options' => ['placeholder' => 'Seleccione ...','id'=>'id_contratante'],
                                                        'pluginOptions' => [
                                                           'allowClear' => true
                                                       ],
                                       ]);
                     ?>	
					</div> 
					<div class="col-sm-3">
					    <?//= $form->field($model, 'id_tipo') ?>
    
                          <?// DateRangePicker with ActiveForm and model. Check the `required` model validation for 
                    	// the attribute. This also features configuration of Bootstrap input group addon.
                    		echo $form->field($model, 'fecha_ocurrencia', [
                    			'addon'=>['prepend'=>['content'=>'<i class="glyphicon glyphicon-calendar"></i>']],
                    			'options'=>['class'=>'drp-container form-group']
                    		])->widget(DateRangePicker::classname(), [
                    			'useWithAddon'=>true,
                    			'pluginOptions'=>[
                    				'locale'=>[
                    					'format'=>'DD-MM-Y',
                    					'separator'=>' - ',
                    				]
                    			]
                    		]);
                    	?>
					</div> 
				</div>	
			
			</div>
		</div>
    <?//= $form->field($model, 'id_reclamo') ?>

    <?//= $form->field($model, 'id_recibo') ?>

    <?//= $form->field($model, 'id_tipo_reclamo') ?>

    <?//= $form->field($model, 'descripcion') ?>

    <?//= $form->field($model, 'monto') ?>
	
	<?//= $form->field($model, 'idRecibo.nro_recibo') ?>

    <?php // echo $form->field($model, 'fecha_registro') ?>

    <?php // echo $form->field($model, 'fecha_actualizacion') ?>

    <?php // echo $form->field($model, 'id_user_registro') ?>

    <?php // echo $form->field($model, 'id_user_actualizacion') ?>

    <?php // echo $form->field($model, 'estatus') ?>

    <?php // echo $form->field($model, 'fecha_ocurrencia') ?>

    <?php // echo $form->field($model, 'fecha_notificacion') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
