<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\ReclamoSeguimiento;
use backend\models\Modelos;
use kartik\icons\Icon;
use kartik\form\ActiveField;
use yii\helpers\Json;
use kartik\select2\Select2;
use kartik\depdrop\Depdrop;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;
use yii\web\Response;
use kartik\money\MaskMoney;
use kartik\form\ActiveForm;
use backend\models\TipoReclamos;
use backend\models\Poliza;
use backend\models\Recibo;
use backend\models\SeguimientoR;
use kartik\detail\DetailView;
use backend\models\ConsultaMensaje;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\PolizaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//http://www.w3schools.com/colors/colors_picker.asp colores

$this->title = "";
$modelSeguimiento = new ReclamoSeguimiento();
ConsultaMensaje::getLeer($model->id_reclamo);
//print_r($model);
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
          <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
          <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Reclamos')?></div></a>
          <a href="#" class="btn btn-default"><div>Registrar Seguimiento</div></a>
</div>
</br></br>
<div class="reclamos-seguimiento">
<!-- Content Wrapper. Contains page content
<div class="content-wrapper">-->

	<div class="reclamos-seguimiento-form">

		<?php $form = ActiveForm::begin(); ?>

		<?php 
			if($model->id_automovil){
				$menu=SeguimientoR::getDatos($model->id_reclamo);
			}else if($model->id_beneficiario){
				$menu=SeguimientoR::getDatos2($model->id_reclamo);
			} 
		?>
		<?php //echo $model->id_automovil; ?>


		<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
		-moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">

			<div class="box-body">
				<div class="container-items"><!-- widgetBody -->

					<div class="row">

						<div class="col-sm-5">
						   <?= $form->field($modelSeguimiento, 'comentario',[
                                                  'addon' => [
                                                      'prepend' => [
                                                          'content' => '<i class="glyphicon glyphicon-list-alt"></i>'

                                                      ]
                                                  ]
                                              ])->textarea(['rows' => 2]) ?>

						</div>

						<div class="col-sm-3">
						    <?=$form->field($modelSeguimiento, 'monto',[
        												  'addon' => [
        													  'prepend' => [
        														  'content' => 'Bs.',

        													  ]
        												  ]
        											  ])->widget(MaskMoney::classname(), [
        																				  'pluginOptions' => [
        																					  'prefix' => '',
        																					  'suffix' => '',
        																					  'allowNegative' => false
        																					]
        											   ]);?>
						</div>
							<div class="col-sm-3">
							<?php
							echo $form->field($model, 'estatus_reclamo',[
                                                  'addon' => [
                                                      'prepend' => [
                                                          'content' => '<i class="glyphicon glyphicon-list-alt"></i>'

                                                      ]
                                                  ]
                                              ])->widget(Select2::classname(), [
												'data' => array("PENDIENTE POR INSPECCION"=>"PENDIENTE POR INSPECCION","POR RECAUDOS"=>"POR RECAUDOS",
                                                                "POR ORDEN"=>"POR ORDEN","PAGADO"=>"PAGADO","RECHAZADO"=>"RECHAZADO"),
												'language' => 'es',
												//'options' => ['placeholder' => 'Seleccione un Esatdo ...'],
												'pluginOptions' => [
												'allowClear' => true
												],
							    ]);
							?>
							</div>
					</div>
					<div class="col-sm-6 three-columns" style="display: none">
							<P>
								<?php
								$opciones = \yii\helpers\ArrayHelper::map($tipoPartes, 'id_parte', 'nombre');
								echo $form->field($model, 'parte')->checkboxList($opciones, ['unselect'=>NULL])->label(false);
								?>
							</P>
						    <div class="col-sm-6">
								<?= $form->field($model, 'descripcion')->textarea(['rows' => 3]) ?>
							</div>

							<div class="col-sm-3">
							<?php
							echo $form->field($model,"id_tipo_reclamo")->widget(Select2::classname(), [
							'data' => ArrayHelper::map(TipoReclamos::find()->all(), 'id_tipo_reclamo', 'nombre'),
							'language' => 'en',
							'options' => ['placeholder' => 'Select ...'],
							'pluginOptions' => [
							'allowClear' => true
							],
							 ]);

							?>
							</div>
								<div class="col-sm-3">
								<?=$form->field($model, 'fecha_ocurrencia',[
															'addon' => [
																'prepend' => [
																	'content' => '<i class="glyphicon glyphicon-calendar"></i>'

																]
															]
														])->widget(\yii\widgets\MaskedInput::className(),
															[
																'clientOptions' => ['alias' =>  'date']
															])
								?>
							</div>
							<div class="col-sm-3">
								<?=$form->field($model, 'fecha_notificacion',[
															'addon' => [
																'prepend' => [
																	'content' => '<i class="glyphicon glyphicon-calendar"></i>'

																]
															]
														])->widget(\yii\widgets\MaskedInput::className(),
															[
																'clientOptions' => ['alias' =>  'date']
															])
								?>
							</div>
							<div class="col-sm-3">
								<?php
									echo $form->field($model,"poliza")->widget(Select2::classname(), [
									'data' => ArrayHelper::map(Poliza::find()->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
																			 ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
																			 ->where("poliza.estatus = :status", [':status' => 1])
																			 ->select("(numero_poliza||'--'||identificacion) AS numero_poliza ,id_poliza")
																			 ->all(), 'id_poliza', 'numero_poliza'),
									'language' => 'en',
									'options' => ['placeholder' => 'Select a Poliza'],
									'pluginOptions' => [
									'allowClear' => true
									],
									]);
								 ?>
							</div>
							<div class="col-sm-3">
								<?=$form->field($model, "id_recibo")->widget(DepDrop::classname(), [
									'data'=> [''=>'Seleccione '],
									'options'=>['placeholder'=>'Selecione ...'],
									'type' => DepDrop::TYPE_SELECT2,
									'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
									'pluginOptions'=>[
									'depends'=>["reclamos-poliza"],
									'placeholder' => 'Seleccione ...',
									'url'=>Url::to(['dependent-dropdown/child-account', 'db'=>Recibo::classname(),'cmpo_dep'=>'id_poliza','cmpo_mostrar'=>'nro_recibo','id_cmpo'=>'id_recibo']),
									'loadingText' => 'Leyendo ...',
									]
									]);

								?>
								<?= $form->field($model, 'numero_reclamo',[
                                                  'addon' => [
                                                      'prepend' => [
                                                          'content' => '<i class="glyphicon glyphicon-list-alt"></i>'

                                                      ]
                                                  ]
                                              ])->textInput() ?>
							</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
						 <?//= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Crear Seguimiento'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>
					</div>
				</div>
			</div>
		</div>





		<?php ActiveForm::end(); ?>

	</div>




<!-- Content Header (Page header) -->
<section class="content-header">
<!--<h1>
# Reclamo <?=$model->numero_reclamo;?>
</h1>-->
<!--<ol class="breadcrumb">
<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
<li><a href="#">UI</a></li>
<li class="active">Timeline</li>
</ol>-->
</section>

<!-- Main content -->
<section class="content">

<!-- row -->
<div class="row">
<div class="col-md-12">
  <!-- The time line -->
  <ul class="timeline">
    <!-- timeline time label -->
<?php $menu=SeguimientoR::getMostrar($model->id_reclamo); ?>

    <!-- END timeline item -->
    <li>
      <i class="fa fa-clock-o bg-gray"></i>
    </li>
  </ul>
</div>
<!-- /.col -->
</div>
<!-- /.row -->

</section>


</div>
<?php
$script=<<< JS
 $(".three-columns").hide();
 $(document).ready(function(){
      $('body').append('<div id="toTop" class="btn btn-info"><span class="glyphicon glyphicon-chevron-up"></span> Subir</div>');
    	$(window).scroll(function () {
			if ($(this).scrollTop() != 0) {
				$('#toTop').fadeIn();
			} else {
				$('#toTop').fadeOut();
			}
		});
    $('#toTop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
});
JS;
$this->registerJs($script);
?>
<?php $string=".top-modulo{padding:2px 2px 2px 8px;}
          .icon-modulo{margin:0px 4px 0px 0px}
          #toTop{
            	position: fixed;
            	bottom:60px;
            	right: 120px;
            	cursor: pointer;
            	display: none;
            }
          ";
      $this->registerCss($string);
?>
