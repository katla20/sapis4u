<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\TipoReclamos;
use kartik\file\FileInput;
use kartik\select2\Select2;
//use kartik\depdrop\Depdrop; //http://demos.krajee.com/widget-details/depdrop
use kartik\widgets\DepDrop;
use backend\models\Poliza;
use backend\models\Recibo;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use backend\models\Cliente;
use backend\models\Persona;
use kartik\checkbox\CheckboxX;
use backend\models\User2;

/* @var $this yii\web\View */
/* @var $model frontend\models\Reclamos */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord ){
	
}else{
$model->fecha_ocurrencia=date("d-m-Y", strtotime($model->fecha_ocurrencia));
$model->fecha_notificacion=date("d-m-Y", strtotime($model->fecha_notificacion));
$model->fecha_declaracion=date("d-m-Y", strtotime($model->fecha_declaracion));
}
?>

<div class="reclamos-form">

    <?php $form = ActiveForm::begin(); ?>

	<?=$form->field($model, 'hi')->widget(CheckboxX::classname());?>
	<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
    <div class="box-header">
         <h4 class="box-title">DATOS DEL RECLAMO</h4>
    </div>

		<div class="box-body">
			<div class="container-items"><!-- widgetBody -->
				<div class="row">
					<div class="col-sm-3">
						<?php

									  echo $form->field($model,"poliza")->widget(Select2::classname(), [
									 'data' => ArrayHelper::map(Poliza::find()->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
																			  ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
																			  ->innerJoin('recibo', 'recibo.id_poliza = poliza.id_poliza AND poliza.fecha_vigenciadesde >= recibo.fecha_vigenciadesde AND poliza.fecha_vigenciahasta <= recibo.fecha_vigenciahasta')
																			  ->innerJoin('certificado', 'certificado.id_recibo = recibo.id_recibo')
																			  ->innerJoin('automovil', 'automovil.id_automovil= certificado.id_automovil')
																			  ->where("poliza.estatus = :status AND poliza.id_ramo= 1", [':status' => 1])
																			  ->select("(numero_poliza||'--'||identificacion||'--'||placa) AS numero_poliza ,poliza.id_poliza")
																			  ->all(), 'id_poliza', 'numero_poliza'),
									'language' => 'en',
									'options' => ['placeholder' => 'Select a Poliza'],
									'pluginOptions' => [
									'allowClear' => true
									],
							]);
							
							 echo Html::hiddenInput('placa', $model->placa, ['id'=>'placa']);
						?>
						
						
					</div>
				    <div class="col-sm-3">

						<?php
						 if (!$model->isNewRecord) {
							  echo Html::hiddenInput('recibo', $model->id_recibo, ['id'=>'recibo']);
						 }

						 echo $form->field($model, "id_recibo")->widget(DepDrop::classname(), [
													//'data'=> [''=>'Seleccione'],
													'options'=>['placeholder'=>'Selecione ...'],
													'type' => DepDrop::TYPE_SELECT2,
													'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
													'pluginOptions'=>[
													'depends'=>["reclamos-poliza"],
													'initialize'=>true,
													'placeholder' => 'Seleccione ...',
													'url'=>Url::to(['dependent-dropdown/child-recibo', 'db'=>Recibo::classname(),'cmpo_dep'=>'id_poliza','cmpo_mostrar'=>'nro_recibo','id_cmpo'=>'id_recibo']),
													'loadingText' => 'Leyendo ...',
													 'params'=>['recibo'] ///SPECIFYING THE PARAM
													]
									]);

								?>
					</div>
					<div class="col-sm-3">
							<?php

							  echo $form->field($model,"id_user_analista")->widget(Select2::classname(), [
                                         'data' => ArrayHelper::map(User2::find()->where("status = :status ", [':status' => 10])
                                                                                 ->select("(cedula||'--'||nombre||'--'||apellido) AS nombre ,id")
                                                                                 ->all(), 'id', 'nombre'),
                                        'language' => 'en',
                                        'options' => ['placeholder' => 'Select a Usuario destino'],
                                        'pluginOptions' => [
                                        'allowClear' => true
                                        ],
                                ]);
                                
                        
                            ?>
					</div>
				</div>
				<div class="row">
				   <div class="col-sm-3">
							<?= $form->field($model, 'numero_reclamo',[
													  'addon' => [
														  'prepend' => [
															  'content' => '<i class="glyphicon glyphicon-list-alt"></i>'

														  ]
													  ]
												  ])->textInput(['maxlength' => true]) ?>
							
						</div>
					<div class="col-sm-3">
						<?php
						echo $form->field($model,"id_tipo_reclamo")->widget(Select2::classname(), [
						'data' => ArrayHelper::map(TipoReclamos::find()
						->where("id_ramo = :id ", [':id' => 1])
						->all(), 'id_tipo_reclamo', 'nombre'),
						'language' => 'en',
						'options' => ['placeholder' => 'Select ...'],
						'pluginOptions' => [
						'allowClear' => true
						],
						 ]);

						?>
					</div>
					<div class="col-sm-3">
						<?php
						echo $form->field($model, 'causa')->widget(Select2::classname(), [
								'data' => array("1"=>"Robo","2"=>"Inundacion"),
								'language' => 'es',
								//'options' => ['placeholder' => 'Seleccione un Esatdo ...'],
								'pluginOptions' => [
								'allowClear' => true
								],
						]);	
						?>
					</div>
					
				</div>
				<div class="row">
				     <div class="col-sm-3">
						<?=$form->field($model, 'fecha_ocurrencia',[
													'addon' => [
														'prepend' => [
															'content' => '<i class="glyphicon glyphicon-calendar"></i>'

														]
													]
												])->widget(\yii\widgets\MaskedInput::className(),
													[
														'clientOptions' => ['alias' =>  'date']
													])
						?>
					</div>
					<div class="col-sm-3">
							<?=$form->field($model, 'fecha_notificacion',[
														'addon' => [
															'prepend' => [
																'content' => '<i class="glyphicon glyphicon-calendar"></i>'

															]
														]
													])->widget(\yii\widgets\MaskedInput::className(),
														[
															'clientOptions' => ['alias' =>  'date']
														])
							?>
						</div>
					<div class="col-sm-3">
						<?=$form->field($model, 'fecha_declaracion',[
													'addon' => [
														'prepend' => [
															'content' => '<i class="glyphicon glyphicon-calendar"></i>'

														]
													]
												])->widget(\yii\widgets\MaskedInput::className(),
													[
														'clientOptions' => ['alias' =>  'date']
													])
						?>
					</div>
				</div>
				
				<div class="row">
					
			    </div>
				<div class="row">
					<div class="col-sm-6">
						<?= $form->field($model, 'lugar_ocurrencia',[
                                                  'addon' => [
                                                      'prepend' => [
                                                          'content' => '<i class="glyphicon glyphicon-list-alt"></i>'

                                                      ]
                                                  ]
                                              ])->textInput(['maxlength' => true]) ?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<?= $form->field($model, 'descripcion',[
                                                  'addon' => [
                                                      'prepend' => [
                                                          'content' => '<i class="glyphicon glyphicon-list-alt"></i>'

                                                      ]
                                                  ]
                                              ])->textarea(['rows' => 3])->label('Relato de los hechos') ?>
					</div>
				</div>
			</div>
		</div>
    </div>

	<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
		<div class="box-header">
			 <h4 class="box-title">PARTES AFECTADAS DEL VEHICULO</h4>
		</div>
		<div class="box-body">
			<div class="container-items"><!-- widgetBody -->
				<div class="row" >

							<div class="col-sm-6 three-columns">
							<P>
								<?php
								$opciones = \yii\helpers\ArrayHelper::map($tipoPartes, 'id_parte', 'nombre');
								echo $form->field($model, 'parte')->checkboxList($opciones, ['unselect'=>NULL])->label(false);
								?>
							</P>
							</div>
				</div>
			</div>
		</div>
	</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script=<<< JS
 //alert('hola');
 $(".field-reclamos-hi").hide();
 // $('input[type="checkbox"]').checkboxX({threeState: false, size:'md'});


$(function(){

    
	$( "#reclamos-poliza" ).change(function() {
		var placa=$("#reclamos-poliza option:selected").text();
		var res = placa.split("--");
		res = res.slice(2,3);
		$("#placa").val(res);
       //alert($("#placa").val());
    });

});	


 
JS;
$this->registerJs($script);

$this->registerCss("
    p {
        font-size: 15px;
        font-size: 1.5rem;
        line-height: 1.333333334;
        margin-bottom: 20px;
        margin-bottom: 2rem;
        text-align: justify;
    }

    .three-columns {

        -webkit-column-count: 3; /* Chrome, Safari, Opera */
        -moz-column-count: 3; /* Firefox */
        column-count: 3;

        -webkit-column-gap: 20px; /* Chrome, Safari, Opera */
        -moz-column-gap: 20px; /* Firefox */
        column-gap: 20px;

        -webkit-column-gap: 2rem; /* Chrome, Safari, Opera */
        -moz-column-gap: 2rem; /* Firefox */
        column-gap: 2rem;

    }
"

);

?>
