<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use backend\models\Ciudad;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\ZonaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title ='';
?>
<div class="zona-index">
	<?php
	$modelCiudad = new Ciudad();
	$gridColumns = [
      ['class' => 'yii\grid\SerialColumn'],
      [
        'attribute'=>'id_zona',
        'value'=>'id_zona',
        'width'=>'100px',
      ],
      [
      'attribute'=>'id_ciudad',
      'width'=>'310px',
      'value'=>'idCiudad.nombre',
      'filterType'=>GridView::FILTER_SELECT2,
      'filter'=>ArrayHelper::map(Ciudad::find()->orderBy('nombre')->asArray()->all(), 'id_ciudad', 'nombre'),
      'filterWidgetOptions'=>[
        'pluginOptions'=>['allowClear'=>true],
      ],
      'filterInputOptions'=>['placeholder'=>'Ciudad','id' => 'ciudad-select2-id'],
      //'group'=>true,  // enable grouping
    ],
    'nombre:ntext',
    [
      'class'=>'kartik\grid\BooleanColumn',
      'filterType'=>GridView::FILTER_SELECT2,
      'attribute'=>'estatus',
      'vAlign'=>'middle',
      'width'=>'100px',
    ],
    [
     'class' => 'yii\grid\ActionColumn',
     'template' => '{view} {update}',
           'buttons' => [
             'view' => function ($url,$model, $key) {
                       return Html::a('<i class="fa fa-binoculars fa-lg" aria-hidden="true"></i>', ['view','id' => $model->id_zona], [
                           'id' => 'activity-index-link',
                           'title' => Yii::t('app', 'Ver Zona'),'rel'=>'tooltip',
                           'data-pjax' => '0',
                       ]);
              },
              'update' => function ($url, $model, $key) {
                  return Html::a('<i class="fa fa-pencil fa-lg" aria-hidden="true"></i>', ['update', 'id' => $model->id_zona], [
                      'id' => 'activity-index-link',
                      'title' => Yii::t('app', 'Actualizar Zona'),'rel'=>'tooltip',
                      'data-pjax' => '0',
                  ]);
              },
      ]
    ],
	];

	echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
		'columns' => $gridColumns,
		'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
		'toolbar' => [
			[
				'content'=>
					Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], [
						//'type'=>'button',
						'title'=>Yii::t('app', 'Registrar Ciudad'),'rel'=>'tooltip',
						'class'=>'btn btn-success'
					]) . ' '.
					Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
						'class' => 'btn btn-default',
						'title' => Yii::t('app', 'Actualizar Grid'),'rel'=>'tooltip',
					]),

			],
			'{export}',
			'{toggleData}'
		],
    'exportConfig' => [
      GridView::EXCEL => [],
      GridView::TEXT => [],
      GridView::PDF => [],
    ],

		'pjax' => false,
		'pjaxSettings' => [
			'options' => ['id' => 'zona-pjax-id'],// UNIQUE PJAX CONTAINER ID
		],
		'bordered' => false,
		'resizableColumns'=>true,
		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
		'striped' => true,
		'condensed' => false,
		'responsive' => true,
		'hover' => true,
		'showPageSummary' => false,
		'panel' => [
			'heading'=>'<h2 class="panel-title" style="font-weight:bold;">'.Icon::show('car', ['class' => 'fa-2x']).'&nbsp; &nbsp;ZONAS DEL LA CIUDAD</h2>',
			'type' => GridView::TYPE_INFO
		],
    ]);
	?>
</div>
<?php
$stringJS = <<<EXP2
$("[rel=tooltip]").tooltip({ placement: 'top'});
EXP2;
$this->registerJs($stringJS, \yii\web\View::POS_READY);

$stringCSS='
 .fa{margin:0px 4px 0px 0px;}
  .fa-trash{
      color:  #d9534f;
  }
  .fa-plus{
      color:  #5cb85c;
  }
  .fa-retweet{
      color:  #5bc0de;
  }
  .fa-binoculars{
      color:  #39CCCC;
  }';
 $this->registerCss($stringCSS);
 ?>
