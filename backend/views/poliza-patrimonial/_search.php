<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PolizaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="poliza-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_poliza') ?>

    <?= $form->field($model, 'numero_poliza') ?>

    <?= $form->field($model, 'id_tipo') ?>

    <?= $form->field($model, 'fecha_vigenciadesde') ?>

    <?= $form->field($model, 'fecha_vigenciahasta') ?>



    <?php=$form->field($model, 'id_contratante') ?>
      <?php // echo $form->field($model, 'estatus') ?>

    <?php // echo $form->field($model, 'fecha_registro') ?>

    <?php // echo $form->field($model, 'fecha_actualizacion') ?>

    <?php // echo $form->field($model, 'id_user_registro') ?>

    <?php // echo $form->field($model, 'id_user_actualizacion') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
