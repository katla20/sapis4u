<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\Poliza;
use backend\models\PolizaAnulacion;
use backend\models\search\PolizaSearch;
use backend\models\Aseguradoras;
use kartik\icons\Icon;
use kartik\select2\Select2;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;


        $searchModel = new PolizaSearch();
        $dataProvider = $searchModel->search3(Yii::$app->request->queryParams,$poliza);//ramo=automovil, tipo=individual
		//print_r($dataProvider); exit;

?>
<div class="poliza-recibos">

    <?php
  	$gridColumns = [
  	//	['class' => 'yii\grid\SerialColumn'],
    		[
    			'attribute'=>'nro_recibo',
    			'value'=>'nro_recibo',
    			'width'=>'180px',
            ],
			['class' => 'kartik\grid\DataColumn',
			   'attribute'=>'fecha_vigenciadesde',
			   'width'=>'200px',
				'value'=>function ($model) {
					 return date($model->fecha_vigenciadesde);
				},
			   'filterType'=>GridView::FILTER_DATE,
			   'filterWidgetOptions' => [
				  'pluginOptions'=>[
				  'format' => 'dd-mm-yyyy',
				  'autoWidget' => true,
				  'autoclose' => true,
				  //'todayBtn' => true,
				  ],

				],
		   ],
		  ['class' => 'kartik\grid\DataColumn',
		   'attribute'=>'fecha_vigenciahasta',
		   'width'=>'200px',
		   'value'=>function ($model) {
					 return date($model->fecha_vigenciahasta);
			},
		   'filterType'=>GridView::FILTER_DATE,
		   'filterWidgetOptions' => [
			  'pluginOptions'=>[
			  'format' => 'dd-mm-yyyy',
			  'autoWidget' => true,
			  'autoclose' => true,
			  //'todayBtn' => true,
			  ],

			],
		  ],
	  [
  		   'attribute'=>'tipo_recibo',
  		   'width'=>'50px',
  		   'value'=>'tipo_recibo'
      ],

	  [
  		   'attribute'=>'prima',
  		   'width'=>'50px',
  		   'value'=>'prima'
      ],
	  [ 
  		'attribute'=>'estatus_recibo',
  		'vAlign'=>'middle',
  		'width'=>'50px',
  		'value'=>function ($model, $key, $index, $widget) {
    					if($model->estatus_recibo=='Pendiente'){
  							return "<span class='label label-success'>".$model->estatus_recibo .'</span>';

    					}else if($model->estatus_recibo=='Anulado'){
    					   return "<span class='label label-danger'>".$model->estatus_recibo .'</span>';

    					}else if($model->estatus_recibo=='Cobrado'){
    					   return "<span class='label label-warning'>".$model->estatus_recibo .'</span>';
    					}
    			},
    	'format'=>'raw',
    	'noWrap'=>false,
  		],

      [
       'class' => 'yii\grid\ActionColumn',
	   'contentOptions' => ['style' => 'width:150px;'],
       'header'=>'GESTION DE RECIBOS',
       'template' => '{anulacion}',
             'buttons' => [
               /*'view' => function ($url,$model, $key) {
                 return Html::a('<i class="fa fa-binoculars fa-lg" aria-hidden="true"></i>', ['view','id' => $model->id_recibo], [
                     'id' => 'activity-index-link-view',
                     'title' => Yii::t('app', 'Ver Recibo'),'rel'=>'tooltip',
                     'data-pjax' => '0',
                 ]);
               },*/

               /*'anulacion' => function ($url,$model, $key) {
                         return Html::a('<i class="fa fa-trash" aria-hidden="true"></i>', '#', [
                            'id' => 'activity-index-link',
                            'title' => Yii::t('app','Anulacion de Recibos'),
                            'data-toggle' => 'modal',
                            'data-target' => '#modal',
                            'data-url' => Url::to(['anulacion', 'id' => $model->id_poliza]),
                            'data-pjax' => '0',
                        ]);
               }*/
			   'anulacion' => function ($url,$model, $key) {
                         return Html::a('<i class="fa fa-trash fa-lg" aria-hidden="true"></i>', ['anulacion-recibo','id' => $model->id_recibo], [
                             'id' => 'activity-index-link-anular',
                             'title' => Yii::t('app','Anulacion de Recibo'),'rel'=>'tooltip',
                             'data-pjax' => '0',
                         ]);
                 },
        ]
      ],

  	];

  	echo GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
  		'columns' => $gridColumns,
		//'showPageSummary' => true,
  		//'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
        'responsive'=>true,
        'hover'=>true
  		/*'pjax' => false,
  		'pjaxSettings' => [
  			'options' => ['id' => 'poliza-pjax-id'],// UNIQUE PJAX CONTAINER ID
  		],
  		'bordered' => false,
  		'resizableColumns'=>true,
  		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
  		'striped' => true,
  		'condensed' => false,
  		'responsive' => true,
  		'hover' => true,
    	'panel' => [
          'heading'=>'<h2 class="panel-title" style="font-weight:bold;"> &nbsp; &nbsp;RECIBOS </h2>',
    			'type' => GridView::TYPE_INFO
    		],*/
      ]);
  	?>

</div>
<?php
$string='.nav-bar_opcion {
  position: fixed;
  top: 200;
  left: 0;
  right: 10;
  z-index: 9999;
  width: 50%;
  height: 50px;
}';
$this->registerCss($string);
?>
<?php
$string = <<<EXP2
$("[rel=tooltip]").tooltip({ placement: 'top'});
$(document).on('click', '#activity-index-link-anular-recibo', (function() {
    $.get(
        $(this).data('url'),
        function (data) {
            $('.modal-body').html(data);
            $('#modal').modal();
        }
    );
}));
$('#modal .modal-dialog').css({
     width: '50%',
     heigth: 'auto'});
EXP2;
$this->registerJs($string, \yii\web\View::POS_READY);
?>
<?php
Modal::begin([
 'options' => [
        'id' => 'modal',
        'tabindex' => false, // important for Select2 to work properly
  ],
'header' => '<h4 class="modal-title"></h4>',
]);
Modal::end();
?>
