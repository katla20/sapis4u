<?php

use yii\helpers\Html;

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;
use kartik\icons\Icon;
use yii\helpers\Url;

Icon::map($this);

/* @var $this yii\web\View */
/* @var $model backend\models\Poliza */
$this->title="";
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
          <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
          <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Polizas Patrimoniales')?></div></a>
          <a href="#" class="btn btn-default"><div>Crear Poliza</div></a>
</div>
</br></br>
<div class="poliza-salud-create">
  <div class="bg-aqua">
      <?php echo Icon::show('home', ['class' => 'fa-3x']);?>
      <span style="color:white;font-size:18px;font-weight:bold;"><?=Yii::t('app', 'Crear Poliza Patrimonial');?></span>
 </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
