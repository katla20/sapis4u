<?php
use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use backend\models\Poliza;
use kartik\icons\Icon;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Collapse;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PolizaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//http://www.w3schools.com/colors/colors_picker.asp colores

$this->title = "";
	$model = new Poliza();
?>
 <div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
            <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
</div>
</br></br>

<div class="poliza-reporte">


    <?php  echo Collapse::widget([
        
            'items' =>[
            
                [
                    'label' => 'Filtros de Busqueda',
                    'content' => $this->render('_search', ['model' => $searchModel]),
                ],
            ]
        
        ]);
    
    
    ?>
    <?php  //echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php
  
  	$gridColumns = [
  		['class' => 'yii\grid\SerialColumn'],
		[
			'attribute'=>'numero_poliza',
			'value'=>'numero_poliza',
			'width'=>'100px',
        ],
        [
  		   'attribute'=>'nombre_completo',
  		   'width'=>'350px',
  		   'value'=>'nombre_completo'
        ],
        [
  		   'attribute'=>'ramo',
  		   'width'=>'100px',
  		   'value'=>'ramo'
        ],
        [
  		   'attribute'=>'aseguradora',
  		   'width'=>'150px',
  		   'value'=>'aseguradora'
        ],
		[ //'class' => 'kartik\grid\DataColumn',
			'attribute'=>'estatus_poliza',
			'filterType'=>GridView::FILTER_SELECT2,
			'filter'=>ArrayHelper::map(Poliza::find()->orderBy('estatus_poliza')->asArray()->all(), 'estatus_poliza', 'estatus_poliza'),
			'filterWidgetOptions'=>[
			  'pluginOptions'=>['allowClear'=>true],
	        ],
				'vAlign'=>'middle',
				'width'=>'150px',
				'value'=>function ($model, $key, $reporte, $widget) {
  					if($model->estatus_poliza=='Activa'){
    				       return "<span class='label label-success'>".$model->estatus_poliza .'</span>';
  					}else if($model->estatus_poliza=='Vencida'){
  					   return "<span class='label label-danger'>".$model->estatus_poliza .'</span>';
  					}else if($model->estatus_poliza=='Pendiente'){
  					   return "<span class='label label-warning'>".$model->estatus_poliza .'</span>';
  					}
  			},
  			 'format'=>'raw',
  			 'noWrap'=>false,
		],
        [
  		   'attribute'=>'producto',
  		   'width'=>'250px',
  		   'value'=>'producto'
        ],
        ['class' => 'kartik\grid\DataColumn',
		   'attribute'=>'fecha_vigenciadesde',
		   'width'=>'250px',
		   'value'=>'fecha_vigenciadesde',
	      // 'format'=>['date','php:d-m-Y'],
       ],
      ['class' => 'kartik\grid\DataColumn',
       'attribute'=>'fecha_vigenciahasta',
       'width'=>'250px',

       'value'=>'fecha_vigenciahasta',
      // 'format'=>['date','php:d-m-Y'],       
     
      ],
	  
	 /* [
		'class'=>'kartik\grid\BooleanColumn',
		'filterType'=>GridView::FILTER_SELECT2,
		'attribute'=>'estatus',
		'vAlign'=>'middle',
		'width'=>'150px',
	   ],*/
  		[
              'class' => 'yii\grid\ActionColumn',
              'template' => '',
              
        ],
  	];

  	echo GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
  		'columns' => $gridColumns,
		//'showPageSummary' => true,
        
  		'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
  		'toolbar' => [
  			[
  				'content'=>
  					Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['reporte'], [
  						'class' => 'btn btn-default',
  						'title' => Yii::t('app', 'Actualizar Grid')
  					]),

  			],
  			'{export}',
  			'{toggleData}'
  		],
  		'exportConfig' => [
  			GridView::EXCEL => [],
  			GridView::TEXT => [],
  			GridView::PDF => [],
           ],


  		'pjax' => false,
  		'pjaxSettings' => [
  			'options' => ['id' => 'poliza-pjax-id'],// UNIQUE PJAX CONTAINER ID
  		],
  		'bordered' => false,
  		'resizableColumns'=>true,
  		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
  		'striped' => true,
  		'condensed' => false,
  		'responsive' => true,
  		'hover' => true,
    	'panel' => [
          'heading'=>'<h2 class="panel-title" style="font-weight:bold;">'.Icon::show('car', ['class' => 'fa-2x']).'&nbsp; &nbsp;REPORTE DE POLIZAS</h2>',
    			'type' => GridView::TYPE_INFO
    		],
      ]);
  	?>

</div>
<?php
$string='';
$this->registerCss($string);
?>
