<?php
use yii\helpers\Html;
//use yii\bootstrap\ActiveForm;
//use yii\bootstrap\ActiveField;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use kartik\select2\Select2;
//use kartik\depdrop\Depdrop; //http://demos.krajee.com/widget-details/depdrop
use kartik\widgets\DepDrop;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use yii\bootstrap\Tabs;
use kartik\icons\Icon;
use kartik\tabs\TabsX;
use kartik\nav\NavX;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\web\Response;
use kartik\money\MaskMoney;
use backend\models\Aseguradoras;
use backend\models\InterAsegRamo;
use backend\models\InterAsegProducto;
use backend\models\Productos;
use backend\models\Cliente;
use backend\models\Persona;
use backend\models\Intermediario;
use backend\models\Automovil;
use backend\models\Version;
use backend\models\Modelo;
use backend\models\Marca;
use yii\bootstrap\Nav;
use backend\models\CargaFamiliar;
use backend\models\CargaFamiliarView;

backend\assets\LocateAsset::register($this); //importante para incluir el main.js donde estan los plugines

//$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');

/* @var $this yii\web\View */
/* @var $model backend\models\Poliza */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['id' => 'poliza-form','enableAjaxValidation' =>false]);?>
<?//=$form->errorSummary($model);?>
<div class="poliza-form"><!--INICIO DEL MAQUETADO-->
           <div class="tab-pane active" id="tabs-pane-poliza"><!--FORMULARIO DE POLIZA-->
               <div class="box box-solid box-default" style="-webkit-box-shadow: 0px 3px 3px -2px rgba(143,141,143,1);
			            -moz-box-shadow: 0px 3px 3px -2px rgba(143,141,143,1);
               box-shadow: 0px 3px 3px -2px rgba(143,141,143,1);">
               <div class="box-header">
                    <h4 class="box-title"><i class="glyphicon glyphicon-user"></i> Datos de la Poliza</h4>
               </div>
               <div class="box-body">
                  <div class="container-items"><!-- widgetBody -->
                    <div class="row"><!--INICIO ROW-->
                       <div class="col-sm-5">
                            <?=$form->field($model,"id_aseguradora",[
                                                          'addon' => [
                                                              'prepend' => [
                                                                  'content' => '<i class="fa fa-university"></i>'

                                                              ]
                                                          ]
                                                      ])->widget(Select2::classname(), [
                                 'data' =>   ArrayHelper::map(Aseguradoras::find()->joinwith('interAsegRamos')
                                                   ->where('inter_aseg_ramo.id_ramo = :ramo and aseguradoras.estatus=:status')
                                                   ->addParams([':ramo' => 1])
                                                   ->addParams([':status' => 1])
                                                   ->all(),'id_aseguradora', 'nombre'),
                                 'language' => 'en',
                                  'options' => ['placeholder' => 'Seleccione ...'],
                                  'pluginOptions' => [
                                     'allowClear' => true
                                 ],
                               ]);
                             ?>
                      </div>
                     <div class="col-sm-5">
                                 <?=$form->field($model, "producto",[
                                                               'addon' => [
                                                                   'prepend' => [
                                                                       'content' => '<i class="fa fa-cart-plus"></i>'

                                                                   ]
                                                               ]
                                                           ])->widget(DepDrop::classname(), [
                                                        'data'=> [''=>'Seleccione '],
                                                        'options'=>['placeholder'=>'Selecione ...'],
                                                        'type' => DepDrop::TYPE_SELECT2,
                                                        'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                                                        'pluginOptions'=>[
                                                        'depends'=>["poliza-id_aseguradora"],
                                                        'placeholder' => 'Seleccione ...',
                                                           'url'=>Url::to(['dependent-dropdown/child-account-join', 'db'=>Productos::classname(),'join'=>'interAsegProductos','cmpo_dep'=>'inter_aseg_producto.id_aseguradora','cmpo_mostrar'=>'productos.nombre','id_cmpo'=>'productos.id_producto','cmpo_adic'=>'productos.id_ramo','valor_adic'=>1]),
                                                         ]
                                                 ]);
                                ?>
                     </div>
               </div><!--FIN ROW-->
               <div class="row"><!--INICIO ROW-->
                  <div class="col-sm-2">

                    <?
                    $model->tipo_recibo = 'Nuevo';
                    echo $form->field($model,"tipo_recibo",[
                                                  'addon' => [
                                                      'prepend' => [
                                                          'content' => '<i class="fa fa-university"></i>'

                                                      ]
                                                  ]
                                              ])->widget(Select2::classname(), [
                                                         'data' => ['Nuevo' => 'Nuevo', 'Renovacion' => 'Renovacion'] ,
                                                         'language' => 'en',
                                                          'options' => ['placeholder' => 'Seleccione ...'],
                                                          'pluginOptions' => [
                                                             'allowClear' => true
                                                         ],
                                                       ]);
                     ?>
                   </div>
                   <div class="col-sm-2">
                    <?= $form->field($model, 'numero_poliza',[
                                                  'addon' => [
                                                      'prepend' => [
                                                          'content' => '<i class="glyphicon glyphicon-list-alt"></i>'

                                                      ]
                                                  ]
                                              ])->textInput(['maxlength' => true]) ?>

                   </div>
                   <div class="col-sm-2">
                     <?=$form->field($model, 'fecha_vigenciadesde',[
                                                   'addon' => [
                                                       'prepend' => [
                                                           'content' => '<i class="glyphicon glyphicon-calendar"></i>'

                                                       ]
                                                   ]
                                               ])->widget(\yii\widgets\MaskedInput::className(),
                                                                        [
                                                                        'clientOptions' => ['alias' =>  'date']
                                                                        ]) ?>

                   </div>
                   <div class="col-sm-2">
                      <?=$form->field($model, 'fecha_vigenciahasta',[
                                                    'addon' => [
                                                        'prepend' => [
                                                            'content' => '<i class="glyphicon glyphicon-calendar"></i>'

                                                        ]
                                                    ]
                                                ])->widget(\yii\widgets\MaskedInput::className(),
                                                                         [
                                                                         'clientOptions' => ['alias' =>  'date']
                                                                         ]) ?>
                   </div>
               </div><!--FIN ROW-->
               <div class="row"><!--INICIO ROW-->
                  <?php Pjax::begin(['id' => 'pjax-persona']);?>
                   <div class="col-sm-5">
                    <?=$form->field($model,'id_contratante', [
                                                                  'addon' => [
                                                                      'prepend' => ['content'=>'<i class="glyphicon glyphicon-user"></i>'],
                                                                      'append'=>['content'=>Html::a('<i class="fa fa-user-plus"></i> Nuevo', '#', [
                                                                                                     'id' => 'm_contratante',
                                                                                                     'class' => 'btn btn-success',
                                                                                                     'data-toggle' => 'modal',
                                                                                                     'data-target' => '#modal',
                                                                                                     'data-url' => Url::to(['persona/create'], true),
                                                                                                     'data-pjax' => '0',
                                                                                                 ]), 'asButton'=>true],
                                                                  ]
                                           ])->widget(Select2::classname(), [
                                                       'data' =>ArrayHelper::map( Cliente::find()
                                                                                    ->joinwith('idPersona')
                                                                                    ->where("cliente.estatus = :status", [':status' => 1])
                                                                                    ->select("(nombre||' '||apellido||' '|| identificacion) AS nombre_completo ,id_cliente")
                                                                                    ->all(),'id_cliente', 'nombre_completo'),
                                                       'language' => 'en',
                                                        'options' => ['placeholder' => 'Seleccione ...','id'=>'id_contratante'],
                                                        'pluginOptions' => [
                                                           'allowClear' => true
                                                       ],
                                       ]);
                     ?>
                   </div>
                   <?php Pjax::end(); ?>
                     <?=$form->field($model, 'id_contratante_hidden')->hiddenInput()->label(false);?>
                     <div class="col-sm-5" id="datoscontratante"></div>
                </div><!--FIN ROW-->
                <div class="row">
                  <?php Pjax::begin(['id' => 'pjax-asegurados']);?>
                     <div class="col-sm-5">
                            <?=$form->field($model, "id_asegurado",[
                                 'addon' => [
                                   'prepend' => ['content'=>'<i class="glyphicon glyphicon-user"></i>'],
                                   'append'=>['content'=>Html::a('<i class="fa fa-user-plus"></i> Nuevo', '#', [
                                                                                           'id' => 'm_asegurado',
                                                                                           'class' => 'btn btn-success',
                                                                                           'data-toggle' => 'modal',
                                                                                           'data-target' => '#modal',
                                                                                           'data-url' => Url::to(['persona/create'], true),
                                                                                           'data-pjax' => '0',
                                                                          ]), 'asButton'=>true],
                                 ]
                       ])->widget(Select2::classname(), [
                                   'data' =>ArrayHelper::map( Cliente::find()
                                                                ->joinwith('idPersona')
                                                                ->where("cliente.estatus = :status", [':status' => 1])
                                                                ->select("(nombre||' '||apellido||' '|| identificacion) AS nombre_completo ,id_cliente")
                                                                ->all(),'id_cliente', 'nombre_completo'),
                                   'language' => 'en',
                                    'options' => ['placeholder' => 'Seleccione ...','id'=>'id_asegurado'],
                                    'pluginOptions' => [
                                       'allowClear' => true
                                   ],
                        ]);

                ?>
                      </div>
                   	<?php Pjax::end(); ?>
                    <?=$form->field($model, 'id_asegurado_hidden')->hiddenInput()->label(false);?>
                    <div class="col-sm-5" id="datosasegurado"></div>
                </div>
				    <div class="form-group"><!--INICIO BOTONES DEL PANEL-->
					 <?= Html::Button('RECIBO '.Icon::show('arrow-circle-right'), ['class' => 'btn btn-primary','id' =>'siguiente-recibo']) ?>
				   </div><!--FIN BOTONES DEL PANEL-->
        </div><!--fin  widgetBody -->
      </div><!--FIN BOX BODY-->
      </div><!--FIN BOX-PANEL 1-->
           </div><!--FIN-PANEL 1-->
           <div class="tab-pane ocultar" id="tabs-pane-recibo"><!--INICIO DEL TAB-PANEL 2-->
             <div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
             -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
             box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
             <div class="box-header">
                  <h4 class="box-title"><?=Icon::show('newspaper-o')?>Datos del Recibo</h4>
                  <?php /*echo NavX::widget([
                    'options' => ['class' => 'nav nav-pills '],
                    'items' => [
                        ['label' => Icon::show('plus').' Crear Recibo Consolidado', 'url' => '#'],
                        ['label' => Icon::show('plus').' Crear Recibo Detallado', 'url' => '#'],
                    ],
                    'encodeLabels' => false
                  ]); */?>
             </div>
             <div class="box-body">
                 <div class="container-items"><!-- widgetBody -->
                   <div class="row">
                          <div class="col-sm-3">
                            <?= $form->field($model, 'nro_recibo',[
                                                          'addon' => [
                                                              'prepend' => [
                                                                  'content' => '<i class="glyphicon glyphicon-list-alt"></i>'
                                                              ]
                                                          ]
                                                      ])->textInput(['maxlength' => true]) ?>
                          </div>
                          <div class="col-sm-3">
                            <?= $form->field($model, 'nro_certificado',[
                                                          'addon' => [
                                                              'prepend' => [
                                                                  'content' => '<i class="glyphicon glyphicon-list-alt"></i>'
                                                              ]
                                                          ]
                                                      ])->textInput(['maxlength' => true,'value'=>0]) ?>
                          </div>

                   </div>
                   <div class="row">
                       <div class="col-sm-12">
                        <?php
                           $items = [
                               [
                                   'label'=>'Paso 1 - '.Icon::show('car').' Cargar el Automovil <i class="glyphicon glyphicon-chevron-right"></i>',
                                   'content'=>$this->render('automovil', ['model' => $model, 'form' => $form]),
                                   'headerOptions' => ['style'=>'font-weight:bold','id' => 'tab-auto'],

                               ],
                               [
                                   'label'=>' Paso 2 Coberturas <i class="glyphicon glyphicon-chevron-right"></i>',
                                   'encode'=>false,
                                   'content'=>$this->render('coberturas_det', ['model' => $model, 'form' => $form]),
                                   'headerOptions' => ['class'=>'disabled','id' => 'tab-cobertura'],
                               ],
                               [
                                   'label'=>'Paso 3 - '.Icon::show('user').' Cargar las Comisiones y el Vendedor <i class="glyphicon glyphicon-chevron-right"></i>',
                                   'encode'=>false,
                                   'content'=>$this->render('vendedor', ['model' => $model, 'form' => $form]),
                                   'headerOptions' => ['class'=>'disabled','id' => 'tab-comision'],
                               ],
                           ];


                           echo TabsX::widget([
                               'position' => TabsX::POS_ABOVE,
                               'align' => TabsX::ALIGN_LEFT,
                               'items' => $items,
                               'encodeLabels'=>false,
							                 'options' => ['class' =>'nav nav-pills'], // set this to nav-tab to get tab-styled navigation

                           ]);

                           ?>
                         </div>
                     </div>
					 <div class="form-group" style="float:right"><!--INICIO BOTONES DEL PANEL-->
					   <?= Html::submitButton(Icon::show('save').'[Registrar Poliza]', ['class' => 'btn btn-success']) ?>
				     <?= Html::resetButton(Icon::show('eraser').'Limpiar Campos', ['class' => 'btn btn-default']) ?>
				     </div><!--FIN BOTONES DEL PANEL-->
					 <div class="form-group" style="float:left"><!--INICIO BOTONES DEL PANEL-->
					 <?= Html::Button(Icon::show('arrow-circle-left').' POLIZA', ['class' => 'btn btn-primary','id' =>'atras-poliza']) ?>
				     </div><!--FIN BOTONES DEL PANEL-->

          </div><!--CONTAINER-->

             </div><!--BOX BODY-->

             </div><!--FIN BOX-PANEL 1-->
			 <!--BOTONERA DEL MAQUETADO--><!--<div class="form-group"></div>--><!--FIN DEL MAQUETADO-->
             </div><!--FIN DEL TAB-PANEL 2-->
		   </div><!--FIN DEL MAQUETADO-->
  <?php ActiveForm::end();?>

<?php

$string = <<<EXP2

$('#poliza-form').on('change', "select[name='Poliza[id_contratante]']", function(event) {
  var out="";
  var id =$("#id_contratante").val();
  $('#poliza-id_contratante_hidden').val(id);
  $("#datoscontratante").empty();

     $.post("buscar-cliente", { id: id })
     .done(function(result) {
       var datos=result[0];
        if(datos!=null){
              $("#datoscontratante").empty();
                      out+='<div class="datagrid"><table class="table table-condensed"><thead><tr><th>Nombre Completo</th><th>Identificacion</th></tr></thead>';
                      out+='<tbody><tr><td>'+datos.nombre_completo+'</td><td>'+datos.identificacion+'</td></tr></tbody></table></div>';
         }
    $("#datoscontratante").html(out);
  });//.donefunction(result)
});//fin change


$('#poliza-form').on('change', "select[name='Poliza[id_asegurado]']", function(event) {
  var out="";
  var id =$("#id_asegurado").val();
  $('#poliza-id_asegurado_hidden').val(id);
  $("#datosasegurado").empty();

     $.post("buscar-cliente", { id: id })
     .done(function(result) {
       var datos=result[0];
        if(datos!=null){
              $("#datosasegurado").empty();
                      out+='<div class="datagrid"><table class="table table-condensed"><thead><tr><th>Nombre Completo</th><th>Identificacion</th></tr></thead>';
                      out+='<tbody><tr><td>'+datos.nombre_completo+'</td><td>'+datos.identificacion+'</td></tr></tbody></table></div>';
         }
    $("#datosasegurado").html(out);
  });//.donefunction(result)

});//fin change

$('#poliza-form').on('change', "select[name='Poliza[id_automovil]']", function(event) {

  var out="";
  var id =$("#id_automovil").val();
  $('#poliza-id_automovil_hidden').val(id);
  $("#datosautomovil").empty();

  $.post("buscar-auto", { id: id })
  .done(function(result) {
     if(result.placa!=null){
          out+='<div class="datagrid"><table class="table table-condensed"><thead><tr><th>Placa</th><th>Modelo</th><th>Version</th><th>Año</th><th>Uso</th><th>Tipo Vehiculo</th></tr></thead>';
          out+='<tbody><tr><td>'+result.placa+'</td><td>'+result.modelo+'</td><td>'+result.version+'</td><td>'+result.anio+'</td><td>'+result.uso+'</td><td>'+result.tipo_vehiculo+'</td></tr></tbody></table></div>';
      }
     $("#datosautomovil").html(out);
  });//.donefunction(result)

});//fin change

$('#poliza-form').on('change', "select[name='Poliza[id_vendedor]']", function(event) {
  var id=$(this).val();
  var out="";
     $.post("buscar-vendedor", { id: id })
     .done(function(result) {
       var datos=result[0];
        if(result[0]==null){
              $("#datosvendedor").empty();
        }else{
              $("#datosvendedor").empty();
              $('#poliza-id_vendedor_hidden').val(datos.id_intermediario);
              $('#poliza-comision_porcentaje').val(datos.comision);
              $('#poliza-comision_vendedor').val((datos.comision/100)*$('#poliza-comision_total').val());
                  out+='<div class="datagrid"><table class="table table-condensed"><thead><tr><th>Cedula</th><th>Nombre Completo</th><th>Codigo</th></tr></thead>';
                  out+='<tbody><tr><td>'+datos.identificacion+'</td><td>'+datos.nombre_completo+'</td><td>'+datos.codigo+'</td></tr></tbody></table></div>';
              $("#datosvendedor").html(out);
        }
     });//.donefunction(result)

});//fin change


$("#siguiente-auto").on("click", function(e) {
    //$("#tab-cobertura,").children('a').prop('disabled', true);//PARA DOCUMENTACION
     $("#tab-cobertura").removeClass('disabled');
     $("#tab-cobertura").addClass('active');
     $("#tab-auto").addClass('disabled');
     $("#w0-tab1").addClass("in active");
     $("#w0-tab0").removeClass('in active');

});

$("#atras-cobertura").on("click", function(e) {

	   $("#tab-cobertura").removeClass('disabled');
	   $("#tab-cobertura").addClass('active');
     $("#w0-tab1").removeClass('in active');
     $("#w0-tab0").addClass("in active");

});

$("#siguiente-cobertura").on("click", function(e) {
    //$("#tab-comision").removeClass('disabled');
    $("#tab-comision").addClass('active');
    $("#tab-cobertura").addClass('disabled');
    $("#w0-tab2").addClass("in active");
    $("#w0-tab1").removeClass('in active');

});

$("#atras-comision").on("click", function(e) {

    $("#tab-comision").addClass('disabled');
    $("#tab-cobertura").removeClass('disabled');
    $("#w0-tab1").addClass("in active");
    $("#w0-tab2").removeClass('in active');

});

$(document).on('click', '#m_contratante', (function(e) {
	$.get($(this).data('url'),{'depend':'','tipo_persona':'Contratante','tipo_poliza':'auto'},
        function (data) {
            $('.modal-body').html(data);
            $('#modal').modal({
				backdrop: true,
				keyboard: true
			}).css({
				 width: '100%',
				 heigth: 'auto'

			});
			//$("#modal").modal("show").find('.modal-content').load($(this).attr('href'));
        }
    );
	$('#modal').on('hidden.bs.modal', function (e) {
		$(this).find('.modal-body').html('');
	});
}));

$(document).on('click', '#m_asegurado', (function(e) {
	$.get($(this).data('url'),{'depend':'','tipo_persona':'Asegurado','tipo_poliza':'auto'},
        function (data) {
            $('.modal-body').html(data);
            $('#modal').modal({
				backdrop: true,
				keyboard: true
			}).css({
				 width: '100%',
				 heigth: 'auto'

			});
			//$("#modal").modal("show").find('.modal-content').load($(this).attr('href'));
        }
		
    );
	$('#modal').on('hidden.bs.modal', function (e) {
		$(this).find('.modal-body').html('');
	});
}));

$(document).on('click', '#m_vehiculo', (function(e) {
	$.get($(this).data('url'),
        function (data) {
            $('.modal-body').html(data);
            $('#modal').modal({
				backdrop: true,
				keyboard: true
			  }).css({
				 width: '100%',
				 heigth: 'auto'

			});
			//$("#modal").modal("show").find('.modal-content').load($(this).attr('href'));
        }
    );
	$('#modal').on('hidden.bs.modal', function (e) {
		$(this).find('.modal-body').html('');
	});
}));

$( "#poliza-form").submit(function( event ) {//validando campos jquery
  $("#mensaje").empty();
  //var coberturas = [1,3];
  var coberturas = [];
  var mensajes=[];

  if ($("input[name*='cobBox']").verificarCoberturas(coberturas)!= "ok" ) {
     mensajes[0]=$("input[name*='cobBox']").verificarCoberturas(coberturas);
     //event.preventDefault();
  }

  if(mensajes.lenght==0){//si mensaje esta vacio todo ha sido validado
     return;
  }else{

           $.each(mensajes, function(key, value) {
              	$.notify({
              	icon: 'glyphicon glyphicon-star',
              	message: value,
              	type:'warning'
              	/*,template:''*/
                });
          });//fin each

  }

//http://fiddle.jshell.net/H3CK2/7/light/

});

EXP2;

$this->registerJs($string, \yii\web\View::POS_READY);
?>

<?php
Modal::begin([
      'options' => [
         'id' => 'modal',
         'tabindex' => false // important for Select2 to work properly
     ],
]);
Modal::end();

$this->registerCss("");
?>
