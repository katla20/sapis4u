<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\Automovil;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use kartik\select2\Select2;
use kartik\depdrop\Depdrop; //http://demos.krajee.com/widget-details/depdrop
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use yii\bootstrap\Tabs;
use kartik\icons\Icon;
use kartik\tabs\TabsX;
use kartik\nav\NavX;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;


/* @var $this yii\web\View */
/* @var $model backend\models\Poliza */
$time=0;
?>
  <div class="box box-solid box-default">
  <div class="box-header">
       <h4 class="box-title"><?=Icon::show('car')?> Datos de Vehiculo</h4>
  </div><!--box-header --->
  <div class="box-body"><!--box-body --->
      <div class="container-items"><!-- widgetBody -->
        <div class="row"><!--row-->
          
            <div class="col-sm-4"><!--col-sm-4-->
            </br>
			 <?php Pjax::begin(['id' => 'pajax-automovil']);?>
             <?php
              //http://demos.krajee.com/widget-details/active-field
                echo $form->field($model,'id_automovil', [
                                                           'addon' => [
                                                               'prepend' => ['content'=>Icon::show('car')],
                                                               'append'=>['content'=>Html::a('<i class="fa fa-plus"></i> Nuevo', '#', [
                                                                                              'id' => 'm_vehiculo',
                                                                                              'class' => 'btn btn-success',
                                                                                              'data-toggle' => 'modal',
                                                                                              'data-target' => '#modal',
                                                                                              'data-url' => Url::to(['automovil/create'], true),
                                                                                              'data-pjax' => '0',
                                                                                          ]), 'asButton'=>true],
                                                           ]
                                    ])->widget(Select2::classname(), [
                                                'data' =>   ArrayHelper::map( Automovil::find()->all(),'id_automovil', 'placa'),
                                                'language' => 'en',
                                                'options' => ['placeholder' => 'Seleccione ...','id'=>'id_automovil'],
                                                 'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                ]);
              ?>
			<?php Pjax::end();?>
            </div><!--col-sm-4-->
            <?=$form->field($model, 'id_automovil_hidden')->hiddenInput()->label(false);?>
            </br>
         </div><!--row-->
         <div class="row"><div class="col-sm-10" id="datosautomovil"></div></div><!--row-->
      <div><?= Html::Button('Siguiente '.Icon::show('arrow-circle-right'), ['class' => 'btn btn-info','id' =>'siguiente-auto']) ?></div>
    </div><!-- widgetBody -->
  </div><!--box-body --->
</div><!--box--->
