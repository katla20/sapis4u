<?php

use yii\helpers\Html;
use kartik\daterange\DateRangePicker;
use kartik\widgets\ActiveForm;
use backend\models\Aseguradoras;
use backend\models\Ramos;
use backend\models\Productos;
use backend\models\Cliente;
use backend\models\Persona;
use kartik\select2\Select2;
use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model backend\models\PolizaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="poliza-search">

    <?php $form = ActiveForm::begin([
        'action' => ['reporte'],
        'method' => 'get',
    ]); ?>
<div class="box-body">
			<div class="container-items"><!-- widgetBody -->
				<div class="row">
					<div class="col-sm-3">
						<?= $form->field($model, 'numero_poliza') ?>
					</div>
                    <div class="col-sm-3">
						<?= $form->field($model, 'placa') ?>
					</div>
					<div class="col-sm-3">
						 
						 <?=$form->field($model,"id_aseguradora",[
                                                          'addon' => [
                                                              'prepend' => [
                                                                  'content' => '<i class="fa fa-university"></i>'

                                                              ]
                                                          ]
                                                      ])->widget(Select2::classname(), [
                                 'data' =>   ArrayHelper::map(Aseguradoras::find()->all(),'id_aseguradora', 'nombre'),
                                 'language' => 'en',
                                  'options' => ['placeholder' => 'Seleccione ...'],
                                  'pluginOptions' => [
                                     'allowClear' => true
                                 ],
                               ]);
                             ?>
					</div>
                    
                    
				</div>
                <div class="row">
					<div class="col-sm-3">
						<?=$form->field($model,"id_ramo",[
                                                          'addon' => [
                                                              'prepend' => [
                                                                  'content' => '<i class="fa fa-university"></i>'

                                                              ]
                                                          ]
                                                      ])->widget(Select2::classname(), [
                                 'data' =>   ArrayHelper::map(Ramos::find()->all(),'id_ramo', 'nombre'),
                                 'language' => 'en',
                                  'options' => ['placeholder' => 'Seleccione ...'],
                                  'pluginOptions' => [
                                     'allowClear' => true
                                 ],
                               ]);
                             ?>
					</div>
					
					<div class="col-sm-3">
						 
						  <?=$form->field($model,"id_producto",[
                                                          'addon' => [
                                                              'prepend' => [
                                                                  'content' => '<i class="fa fa-university"></i>'

                                                              ]
                                                          ]
                                                      ])->widget(Select2::classname(), [
                                 'data' =>   ArrayHelper::map(Productos::find()->all(),'id_producto', 'nombre'),
                                 'language' => 'en',
                                  'options' => ['placeholder' => 'Seleccione ...'],
                                  'pluginOptions' => [
                                     'allowClear' => true
                                 ],
                               ]);
                             ?>
					</div>
                    
				</div>

				<div class="row">
					<div class="col-sm-3">
					    <?=$form->field($model,'id_contratante', [
                                                          'addon' => [
                                                              'prepend' => [
                                                                  'content' => '<i class="fa fa-university"></i>'

                                                              ]
                                                          ]
                                                      ])->widget(Select2::classname(), [
                                                       'data' =>ArrayHelper::map( Cliente::find()
                                                                                    ->joinwith('idPersona')
                                                                                    ->where("cliente.estatus = :status", [':status' => 1])
                                                                                    ->select("(nombre||' '||segundo_nombre||' '||apellido|| ' ' ||segundo_apellido || ' ' || identificacion) AS nombre_completo ,id_cliente")
                                                                                    ->all(),'id_cliente', 'nombre_completo'),
                                                       'language' => 'en',
                                                        'options' => ['placeholder' => 'Seleccione ...','id'=>'id_contratante'],
                                                        'pluginOptions' => [
                                                           'allowClear' => true
                                                       ],
                                       ]);
                     ?>	
					</div> 
					<div class="col-sm-3">
					    <?//= $form->field($model, 'id_tipo') ?>
    
                          <?// DateRangePicker with ActiveForm and model. Check the `required` model validation for 
                    	// the attribute. This also features configuration of Bootstrap input group addon.
                    		echo $form->field($model, 'fecha_vigenciadesde', [
                    			'addon'=>['prepend'=>['content'=>'<i class="glyphicon glyphicon-calendar"></i>']],
                    			'options'=>['class'=>'drp-container form-group']
                    		])->widget(DateRangePicker::classname(), [
                    			'useWithAddon'=>true,
                    			'pluginOptions'=>[
                    				'locale'=>[
                    					'format'=>'DD-MM-Y',
                    					'separator'=>' - ',
                    				]
                    			]
                    		]);
                    	?>
					</div> 
				</div>	
			
			</div>
		</div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

$addon = <<<HTML

<span class="input-group-addon">
    <i class="glyphicon glyphicon-calendar"></i>
</span>
HTML;
?>