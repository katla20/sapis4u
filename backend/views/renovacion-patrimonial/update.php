<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;
use kartik\icons\Icon;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Poliza */

$this->title ="";
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
          <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
          <a href="<?=Url::to(['poliza-patrimonial/index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Polizas')?></div></a>
          <a href="#" class="btn btn-default"><div>Renovacion de poliza patrimonial</div></a>
</div>
</br></br>
<div class="poliza-salud-update">
    <div class="bg-aqua top-modulo">
      <?php echo Icon::show('home', ['class' => 'fa-3x']);?>
      <span style="color:white;font-size:18px;font-weight:bold;"><?=Yii::t('app', 'Renovacion de poliza patrimonial');?></span>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
<?php $string=".top-modulo{padding:2px 2px 2px 8px;}
          .icon-modulo{margin:0px 4px 0px 0px}";
      $this->registerCss($string);
?>
