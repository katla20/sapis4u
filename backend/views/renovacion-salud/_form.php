<?php
use yii\helpers\Html;
//use yii\bootstrap\ActiveForm;
//use yii\bootstrap\ActiveField;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use kartik\select2\Select2;
use kartik\widgets\DepDrop;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use yii\bootstrap\Tabs;
use kartik\icons\Icon;
use kartik\tabs\TabsX;
use kartik\nav\NavX;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\web\Response;
use kartik\money\MaskMoney;
use backend\models\Aseguradoras;
use backend\models\InterAsegRamo;
use backend\models\InterAsegProducto;
use backend\models\Productos;
use backend\models\Cliente;
use backend\models\Persona;
use backend\models\Intermediario;
use backend\models\CargaFamiliar;
use backend\models\CargaFamiliarView;

backend\assets\LocateAsset::register($this); //importante para incluir el main.js donde estan los plugines

/* @var $this yii\web\View */
/* @var $model backend\models\Poliza */
/* @var $form yii\widgets\ActiveForm */

?>
<?php $form = ActiveForm::begin(['id' => 'poliza-form','enableAjaxValidation' =>false]);?>
<div class="poliza-form">
        <div class="tab-pane active" id="tabs-pane-poliza"><!--FORMULARIO DE POLIZA-->
          <div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
          -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
          box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
          <div class="box-header">
               <h4 class="box-title"><i class="glyphicon glyphicon-user"></i> Datos de la Poliza</h4>
          </div>
          <div class="box-body">
        	    <div class="container-items"><!-- widgetBody -->
                <div class="row">
                   <div class="col-sm-5">
                        <?=$form->field($model,"id_aseguradora",[
                                                      'addon' => [
                                                          'prepend' => [
                                                              'content' => '<i class="fa fa-university"></i>'

                                                          ]
                                                      ]
                                                  ])->widget(Select2::classname(), [
                             'data' =>   ArrayHelper::map(Aseguradoras::find()->joinwith('interAsegRamos')
                                               ->where('inter_aseg_ramo.id_ramo = :ramo and aseguradoras.estatus=:status')
                                               ->addParams([':ramo' => 2])
                                               ->addParams([':status' => 1])
                                               ->all(),'id_aseguradora', 'nombre'),
                             'language' => 'en',
                              'options' => ['placeholder' => 'Seleccione ...'],
                              'disabled' => 'disabled',
                              'pluginOptions' => [
                                 'allowClear' => true
                             ],
                           ]);
                         ?>
             </div>
            <div class="col-sm-5">
               <?php if (!$model->isNewRecord) {
                    echo Html::hiddenInput('id_producto', $model->id_producto, ['id'=>'id_producto']);
                }?>
                         <?=$form->field($model, "producto",[
                                                       'addon' => [
                                                           'prepend' => [
                                                               'content' => '<i class="fa fa-cart-plus"></i>'

                                                           ]
                                                       ]
                                                   ])->widget(DepDrop::classname(), [
                             										'data'=> [''=>'Seleccione '],
                             										'options'=>['placeholder'=>'Selecione ...'],
                             										'type' => DepDrop::TYPE_SELECT2,
                             										'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                             										'pluginOptions'=>[
                             										'depends'=>["poliza-id_aseguradora"],
                             										'placeholder' => 'Seleccione ...',
                             										'url'=>Url::to(['dependent-dropdown/child-account-join', 'db'=>Productos::classname(),'join'=>'interAsegProductos','cmpo_dep'=>'inter_aseg_producto.id_aseguradora','cmpo_mostrar'=>'productos.nombre','id_cmpo'=>'productos.id_producto','cmpo_adic'=>'productos.id_ramo','valor_adic'=>2]),
                                                'params'=>['recibo'], ///SPECIFYING THE PARAM
                                                'initialize'=>true,
                             									  ]
         									               ]);
         								?>
             </div>
         </div>
        <div class="row">
           <div class="col-sm-2">
             <?php $model->tipo_recibo = 'Renovacion';
              echo $form->field($model,"tipo_recibo",[
                                           'addon' => [
                                               'prepend' => [
                                                   'content' => '<i class="fa fa-university"></i>'

                                               ]
                                           ]
                                       ])->widget(Select2::classname(), [
                                                  'data' => ['Nuevo' => 'Nuevo', 'Renovacion' => 'Renovacion'] ,
                                                  'language' => 'en',
                                                   'options' => ['placeholder' => 'Seleccione ...'],
                                                    'disabled' => 'disabled',
                                                   'pluginOptions' => [
                                                      'allowClear' => true
                                                  ],
                                                ]);
              ?>
            </div>
            <div class="col-sm-2">
             <?= $form->field($model, 'numero_poliza',[
                                           'addon' => [
                                               'prepend' => [
                                                   'content' => '<i class="glyphicon glyphicon-list-alt"></i>'

                                               ]
                                           ]
                                       ])->textInput(['maxlength' => true,'disabled' => 'disabled']) ?>

            </div>

            <div class="col-sm-2">
              <?=$form->field($model, 'fecha_vigenciadesde',[
                                            'addon' => [
                                                'prepend' => [
                                                    'content' => '<i class="glyphicon glyphicon-calendar"></i>'

                                                ]
                                            ]
                                        ])->widget(\yii\widgets\MaskedInput::className(),
                                                                 [
                                                                 'clientOptions' => ['alias' =>  'date']
                                                                 ]) ?>

            </div>
            <div class="col-sm-2">
               <?=$form->field($model, 'fecha_vigenciahasta',[
                                             'addon' => [
                                                 'prepend' => [
                                                     'content' => '<i class="glyphicon glyphicon-calendar"></i>'

                                                 ]
                                             ]
                                         ])->widget(\yii\widgets\MaskedInput::className(),
                                                                  [
                                                                  'clientOptions' => ['alias' =>  'date']
                                                                  ]) ?>
            </div>
           </div>
        <div class="row">
            <div class="col-sm-5">
              <div class="nonboxy-widget">
                 <div class="widget-content">
                    <?php Pjax::begin(['id' => 'pjax-persona']);?>
                     <div class="widget-box">
                       <fieldset>
                          <legend><h4><i class="glyphicon glyphicon-user"></i> Contratante</h4></legend>
                          <?php
                                echo $form->field($model,'id_contratante', [
                                                           'addon' => [
                                                               'prepend' => ['content'=>'<i class="glyphicon glyphicon-user"></i>'],
                                                               'append'=>['content'=>Html::a('<i class="fa fa-user-plus"></i> Nuevo', '#', [
                                                                                              'id' => 'activity-index-link',
                                                                                              'class' => 'btn btn-success',
                                                                                              'data-toggle' => 'modal',
                                                                                              'data-target' => '#modal',
                                                                                              'data-url' => Url::to(['persona/create'], true),
                                                                                              'data-pjax' => '0',
                                                                                          ]), 'asButton'=>true],
                                                           ]
                                    ])->widget(Select2::classname(), [
                                                'data' =>ArrayHelper::map( Cliente::find()
                                                                             ->joinwith('idPersona')
                                                                             ->where("cliente.estatus = :status", [':status' => 1])
                                                                             ->select("(nombre||' '||apellido||' '|| identificacion) AS nombre_completo ,id_cliente")
                                                                             ->all(),'id_cliente', 'nombre_completo'),
                                                'language' => 'en',
                                                 'options' => ['placeholder' => 'Seleccione el Contratante','id'=>'id_contratante'],
                                                 'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                ])->label(false);
              ?>
            </fieldset>
            </div>
              <?php Pjax::end(); ?>
             </div></div></div>
            <div class="col-sm-5">
            </br></br></br>
              <p id="datoscontratante"></p>
              <?=$form->field($model, 'id_contratante_hidden')->hiddenInput()->label(false);?>
            </div>
            </div>
            <div class="row">
            <div class="col-sm-10">
                  <div class="nonboxy-widget">
                    <div class="widget-content">
                         <div class="widget-box">
                           <fieldset>
                              <legend><h4><?=Icon::show('group')?>Asegurados (puede incluir al Contratante con su carga familiar)</h4></legend>
                              <div class="row">
                                <?php Pjax::begin(['id' => 'pjax-asegurados']);?>
								                   <div class="col-sm-5">
											                    <?=$form->field($model, "asegurados",[
																		   'addon' => [
																			   'prepend' => ['content'=>'<i class="glyphicon glyphicon-user"></i>'],
																			   'append'=>['content'=>Html::a('<i class="fa fa-user-plus"></i> Nuevo', '#', [
                                                                                                 'id' => 'activity-index-link',
                                                                                                 'class' => 'btn btn-success',
                                                                                                 'data-toggle' => 'modal',
                                                                                                 'data-target' => '#modal',
                                                                                                 'data-url' => Url::to(['persona/create'], true),
                                                                                                 'data-pjax' => '0',
                                                                                ]), 'asButton'=>true],
																		   ]
																	   ])->widget(DepDrop::classname(), [
																						'data'=> [''=>'Seleccione '],
																						'options'=>['placeholder'=>'Selecione ...'],
																						'type' => DepDrop::TYPE_SELECT2,
																						'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
																						'pluginOptions'=>[
																						'depends'=>["id_contratante"],
																						'placeholder' => 'Seleccione ...',
																						'url'=>Url::to(['dependent-dropdown/child-account-asegurados','db'=>CargaFamiliar::classname(),'cmpo_dep'=>'id_titular','cmpo_mostrar'=>'nombre_completo','id_cmpo'=>'id_beneficiario']),
																					   ]
																			   ])->label(false);

															?>
                                    </div>

                              </div>
							   <div class="col-sm-2"><?= Html::a(Icon::show('plus').' Agregar a la lista ','#', ['class'=>'btn btn-primary','id' => 'add-asegurados', 'data-url' => Url::to(['poliza-salud/buscar-asegurados'], true),]) ?></div>
							   <?php Pjax::end(); ?>
                             </div>
                             <div class="datagrid">
                               <table class="table table-condensed" id="table-asegurados">
                                 <thead>
                                 <tr><th>Cedula</th><th>Nombres y Apellidos</th><th>Edad</th><th>Parentezco</th><th>Operacion</th></tr>
                               </thead>
                               <tbody></tbody>
                               </table>
                             </div>
                          </fieldset>
                         </div>
                    </div>
                </div>

            </div>
            </div><!--CONTAINER-->
            <div class="form-group"><!--INICIO BOTONES DEL PANEL-->
               <?= Html::Button('RECIBO '.Icon::show('arrow-circle-right'), ['class' => 'btn btn-primary','id' =>'siguiente-recibo']) ?>
            </div><!--FIN BOTONES DEL PANEL-->
            </div><!--BOX BODY-->
        </div><!--BOX-->
    </div><!--FIN FORMULARIO DE POLIZA-->
    <div class="tab-pane ocultar" id="tabs-pane-recibo"><!--INICIO FORMULARIO DE RECIBO-->
      <div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
      -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
      box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
      <div class="box-header with-border">
           <h4 class="box-title"><?=Icon::show('newspaper-o')?>Datos del Recibo</h4>
      </div>
        <div class="box-body">
            <div class="container-items"><!-- widgetBody -->
              <div class="row">
                     <div class="col-sm-3">
                       <?=Html::hiddenInput('id_recibo', $model->id_recibo, ['id'=>'id_recibo']);?>
                       <?= $form->field($model, 'nro_recibo',[
                                                     'addon' => [
                                                         'prepend' => [
                                                             'content' => '<i class="glyphicon glyphicon-list-alt"></i>'
                                                         ]
                                                     ]
                                                 ])->textInput(['maxlength' => true]) ?>
                     </div>
                     <div class="col-sm-3">
                       <?= $form->field($model, 'nro_certificado',[
                                                     'addon' => [
                                                         'prepend' => [
                                                             'content' => '<i class="glyphicon glyphicon-list-alt"></i>'
                                                         ]
                                                     ]
                                                 ])->textInput(['maxlength' => true]) ?>
                     </div>
              </div>
             <div class="row">
                <div class="col-sm-12">
                 <?php
                    $items = [
                        [
                            'label'=>'Paso 1 - '.Icon::show('group').' Cargar los Beneficiarios',
                            'content'=>$this->render('beneficiarios', ['model' => $model, 'form' => $form]),
                            'headerOptions' => ['style'=>'font-weight:bold','id' => 'tab-benef'],
                        ],
                        [
                            'label'=>' Paso 2 Coberturas <i class="glyphicon glyphicon-chevron-right"></i>',
                            'encode'=>false,
                            'content'=>$this->render('coberturas_det', ['model' => $model, 'form' => $form]),
                            'headerOptions' => ['class'=>'disabled','id' => 'tab-cobertura'],
                        ],
                        [
                            'label'=>'Paso 3 - '.Icon::show('user').' Cargar las Comisiones y el Vendedor',
                            'encode'=>false,
                            'content'=>$this->render('vendedor', ['model' => $model, 'form' => $form]),
                            'headerOptions' => ['class'=>'disabled','id' => 'tab-comision'],
                        ],
                    ];

                    echo TabsX::widget([
                        'position' => TabsX::POS_ABOVE,
                        'align' => TabsX::ALIGN_LEFT,
                        'items' => $items,
                        'encodeLabels'=>false,
                        'options' => ['class' =>'nav nav-pills'], // set this to nav-tab to get tab-styled navigation
                    ]);

                    ?>
                  </div>
              </div>
              <div class="form-group" style="float:left"><!--INICIO BOTONES DEL PANEL-->
   					   <?= Html::Button(Icon::show('arrow-circle-left').' POLIZA', ['class' => 'btn btn-primary','id' =>'atras-poliza']) ?>
   				    </div><!--FIN BOTONES DEL PANEL-->
              <div class="form-group" style="float:right"><!--INICIO BOTONES DEL PANEL-->
   					   <?= Html::submitButton(Icon::show('save').'[Registrar Poliza]', ['class' => 'btn btn-success']) ?>
   				     <?= Html::resetButton(Icon::show('eraser').'Limpiar Campos', ['class' => 'btn btn-default']) ?>
   				     </div><!--FIN BOTONES DEL PANEL-->
            </div><!--CONTAINER-->
          </div><!--BOX BODY-->
        </div><!--BOX-->
</div><!--FIN FORMULARIO DE RECIBO-->
    <?php ActiveForm::end();?>
<?php

$string = <<<EXP2

$("#poliza-producto").on('depdrop.afterChange', function(event, id, value) {
   $(this).attr('disabled','disabled');
});

$('#id_contratante').change(function(){
  var out="";
  var id =$("#id_contratante").val();
  $('#poliza-id_contratante_hidden').val(id);
  $("#datoscontratante").empty();

    $.post("buscar-cliente", { id: id })
     .done(function(result) {
       var datos=result[0];
        if(datos!=null){
              $("#datoscontratante").empty();
                  var out='<div class="datagrid"><table class="table table-condensed"><thead><tr><th>Nombre Completo</th><th>Identificacion</th></tr></thead>';
                      out+='<tbody><tr><td>'+datos.nombre_completo+'</td><td>'+datos.identificacion+'</td></tr></tbody></table></div>';
         }
    $("#datoscontratante").html(out);
  });//.donefunction(result)

});//fin change

$('#id_vendedor').change(function(){
  var id=$(this).val();
     $.post("buscar-vendedor", { id: id })
     .done(function(result) {
       var datos=result[0];

        if(result[0]==null){
              $("#datosvendedor").empty();
        }else{
              $("#datosvendedor").empty();
              $('#poliza-id_vendedor_hidden').val(datos.id_intermediario);
              $('#poliza-comision_porcentaje').val(datos.comision);
              $('#poliza-comision_vendedor').val((datos.comision/100)*$('#poliza-comision_total').val());

              var out='<div class="datagrid"><table class="table table-condensed"><thead><tr><th>Cedula</th><th>Nombre Completo</th><th>Codigo</th></tr></thead>';
                  out+='<tbody><tr><td>'+datos.identificacion+'</td><td>'+datos.nombre_completo+'</td><td>'+datos.codigo+'</td></tr></tbody></table></div>';
              $("#datosvendedor").html(out);
        }
     });//.donefunction(result)

});//fin change

$(document).on('click', '#add-asegurados', (function() {
  var out='';
  if($('#poliza-asegurados').val()==""){
     alert("debe seleccionar de la lista un asegurado");
  }else{
      if($("input[name*='asegurado']").verificarIdentidad($('#poliza-asegurados').val())=='ok'){
         $.get(
             $(this).data('url'),{id:$('#poliza-asegurados').val()},
             function (result) {
                  out+='<tr id="'+result[0].identificacion+'"><td><input type="hidden" name="asegurado['+result[0].id_beneficiario+']" id="asegurado" value="'+result[0].id_beneficiario+'">'+result[0].identificacion+'</td><td>'+result[0].nombre_completo+'</td><td>'+result[0].anios+'</td><td>'+result[0].parentesco+'</td><td><a id="borrar-asegurados"><i class="fa fa-trash fa-lg"></i></a></td></tr>';
                  $("#table-asegurados").children("tbody").append(out);
             });//FIN GET

       }else{
         alert("El asegurado ya se agrego a la lista");
       }
  }

}));//FIN DOCUMENT CLICK


$.get(
    'asegurados',{id:$('#id_recibo').val()},
     function (result) {
       var out="";
          $.each(result, function(key, value) {
                out+='<tr id="'+value.identificacion+'"><td><input type="hidden" name="asegurado['+value.id_beneficiario+']" id="asegurado" value="'+value.id_beneficiario+'">'+value.identificacion+'</td><td>'+value.nombres+' '+value.apellidos+'</td><td>'+value.anios+'</td><td>'+value.parentesco+'</td><td><a id="borrar-asegurados"><i class="fa fa-trash fa-lg"></i></a></td></tr>';
          });//FIN GET
         $("#table-asegurados").children("tbody").append(out);
});//FIN GET

$.get(
    'beneficiarios',{id:$('#id_recibo').val()},
     function (result) {
       var out="";
          $.each(result, function(key, value) {
                 out+='<tr id="'+value.identificacion+'"><td><input type="hidden" name="beneficiario['+value.id_cliente+']" id="beneficiario" value="'+value.id_cliente+'">'+value.identificacion+'</td><td>'+value.nombres+' '+value.apellidos+'</td><td><input type="text" name="porcentaje['+value.id_cliente+']" id="porcentaje" value="'+value.beneficiario_porcentaje+'" size="5"></td><td><a id="borrar-beneficiarios"><i class="fa fa-trash fa-lg"></i></a></td></tr>';
          });//FIN GET
          $("#table-beneficiarios").children("tbody").append(out);
});//FIN GET

$(document).on('click', '#add-beneficiarios', (function() {
  var out='';
  if($('#beneficiarios').val()==""){
     alert("debe seleccionar de la lista un beneficiario");
  }else{

     if($("input[name*='porcentaje']").sumarPorcentaje()==100){
      alert("porcentaje de los beneficiarios completados");
     }

    if($("input[name*='beneficiario']").verificarIdentidad($('#beneficiarios').val())=='ok'){
             $.get(
                 $(this).data('url'),{id:$('#beneficiarios').val()},
                 function (result) {
                   out+='<tr id="'+result[0].identificacion+'"><td><input type="hidden" name="beneficiario['+result[0].id_cliente+']" id="beneficiario" value="'+result[0].id_cliente+'">'+result[0].identificacion+'</td><td>'+result[0].nombre_completo+'</td><td><input type="text" name="porcentaje['+result[0].id_cliente+']" id="porcentaje" size="5"></td><td><a id="borrar-beneficiarios"><i class="fa fa-trash fa-lg"></i></a></td></tr>';
                      $("#table-beneficiarios").children("tbody").append(out);
                 });//FIN GET

      }else{
             alert("El beneficiario ya se agrego a la lista");
      }


  }

}));//FIN DOCUMENT CLICK

$('#table-beneficiarios').on('keyup', "input[id*='porcentaje']", function(event) {
    // Do something on click on an existent or future .dynamicElement
      var SumaPorc=$("input[name*='porcentaje']").sumarPorcentaje();
      //console.log(SumaPorc);
      if(SumaPorc > 100){
         alert('La Suma de los Porcentajes no debe superar el 100%');
         $(this).val("");
      }else if (SumaPorc==100) {
        alert('Porcentaje completado al 100%');
      }

});

//$("input[name*='porcentaje']").maskMoney({thousands:'.', decimal:','});

$("#siguiente-benef").on("click", function(e) {
    //$("#tab-cobertura,").children('a').prop('disabled', true);//PARA DOCUMENTACION
     $("#tab-cobertura").removeClass('disabled');
     $("#tab-cobertura").addClass('active');
    $("#tab-benef").addClass('disabled');
     $("#w0-tab1").addClass("in active");
     $("#w0-tab0").removeClass('in active');

});

$("#atras-cobertura").on("click", function(e) {

	   $("#tab-cobertura").removeClass('disabled');
	   $("#tab-cobertura").addClass('active');
     $("#w0-tab1").removeClass('in active');
     $("#w0-tab0").addClass("in active");

});

$("#siguiente-cobertura").on("click", function(e) {
    //$("#tab-comision").removeClass('disabled');
    $("#tab-comision").addClass('active');
    $("#tab-cobertura").addClass('disabled');
    $("#w0-tab2").addClass("in active");
    $("#w0-tab1").removeClass('in active');

});

$("#atras-comision").on("click", function(e) {

    $("#tab-comision").addClass('disabled');
    $("#tab-cobertura").removeClass('disabled');
    $("#w0-tab1").addClass("in active");
    $("#w0-tab2").removeClass('in active');

});

EXP2;

$this->registerJs($string, \yii\web\View::POS_READY);

?>

<?php
Modal::begin([
      'options' => [
         'id' => 'modal',
         'tabindex' => false // important for Select2 to work properly
     ],
]);
Modal::end();

$this->registerCss("");
?>
