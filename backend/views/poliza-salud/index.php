<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\helpers\Url;
use kartik\icons\Icon;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\Poliza;
use backend\models\PolizaAnulacion;
use kartik\select2\Select2;
use backend\models\Aseguradoras;

Icon::map($this, Icon::WHHG); // Maps the Elusive icon font framework
Icon::map($this, Icon::FA); // Maps the Elusive icon font framework

$this->title = "";
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
  <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
</div>
</br></br>
<div class="poliza-salud-index">

  <?php $gridColumns = [
    	  ['class' => 'yii\grid\SerialColumn'],
  		  [
  			'attribute'=>'numero_poliza',
  			'value'=>'numero_poliza',
  			'width'=>'150px',
          ],
		      [
          'attribute'=>'id_aseguradora',
          'width'=>'250px',
          'value'=>'aseguradora',
          'filterType'=>GridView::FILTER_SELECT2,
          'filter'=>ArrayHelper::map(Aseguradoras::find()->orderBy('nombre')->asArray()->all(), 'id_aseguradora', 'nombre'),
          'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
          ],
          'filterInputOptions'=>['placeholder'=>'Aseguradoras','id' => 'aseguradoras-select2-id'],
          //'group'=>true,  // enable grouping
        ],
        [ //'class' => 'kartik\grid\DataColumn',
  			'attribute'=>'estatus_poliza',
  			'filterType'=>GridView::FILTER_SELECT2,
               'filter'=>ArrayHelper::map(Poliza::find()->where("poliza.estatus = :status AND poliza.id_ramo = :ramo AND poliza.id_tipo= :tipo",[':status' => 1,':ramo'=> 1,':tipo' => 1])->select(["(CASE WHEN poliza.estatus_poliza ='Anulada' THEN poliza.estatus_poliza
  								         WHEN poliza.estatus_poliza <> 'Anulada' and poliza.fecha_vigenciahasta < NOW() THEN 'Vencida'
   										 WHEN poliza.estatus_poliza <> 'Anulada' and poliza.fecha_vigenciahasta >= NOW() THEN 'Vigente'
  										 ELSE poliza.estatus_poliza END) AS estatus_poliza"])->orderBy('estatus_poliza')->asArray()->all(), 'estatus_poliza', 'estatus_poliza'),
  			//'filter'=>ArrayHelper::map(Poliza::find()->orderBy('estatus_poliza')->asArray()->all(), 'estatus_poliza', 'estatus_poliza'),
  			'filterWidgetOptions'=>[
  			  'pluginOptions'=>['allowClear'=>true],
  	        ],
			'filterInputOptions'=>['placeholder'=>'Estatus','id' => 'estatus-select2-id'],
  				'vAlign'=>'middle',
  				'width'=>'150px',
  			'value'=>function ($model, $key, $index, $widget) {
    					if($model->estatus_poliza=='Vigente'){
  							return "<span class='label label-success'>".$model->estatus_poliza .'</span>';

    					}else if($model->estatus_poliza=='Anulada'){
    					   return "<span class='label label-danger'>".$model->estatus_poliza .'</span>';

    					}else if($model->estatus_poliza=='Vencida'){
    					   return "<span class='label label-warning'>".$model->estatus_poliza .'</span>';
    					}
    			},
    			 'format'=>'raw',
    			 'noWrap'=>false,
  		],
		  ['class' => 'kartik\grid\DataColumn',
			   'attribute'=>'fecha_vigenciadesde',
			   'width'=>'200px',
			    /*'value'=>function ($model) {
					 return date("d-m-Y", strtotime($model->fecha_vigenciadesde));
				},*/
				'value'=>'fecha_vigenciadesde',
			   'filterType'=>GridView::FILTER_DATE,
			   'filterWidgetOptions' => [
				  'pluginOptions'=>[
				  'format' => 'dd-mm-yyyy',
				  'autoWidget' => true,
				  'autoclose' => true,
				  //'todayBtn' => true,
				  ],

				],
		],
	    ['class' => 'kartik\grid\DataColumn',
		   'attribute'=>'fecha_vigenciahasta',
		   'width'=>'200px',
		   /*'value'=>function ($model) {
					 return date("d-m-Y", strtotime($model->fecha_vigenciahasta));
			},*/
			'value'=>'fecha_vigenciahasta',
		   'filterType'=>GridView::FILTER_DATE,
		   'filterWidgetOptions' => [
			  'pluginOptions'=>[
			  'format' => 'dd-mm-yyyy',
			  'autoWidget' => true,
			  'autoclose' => true,
			  //'todayBtn' => true,
			  ],

			],
		],
        [
  		   'attribute'=>'nombre_completo',
  		   'width'=>'350px',
  		   'value'=>'nombre_completo'
        ],
  		[
			'class'=>'kartik\grid\ExpandRowColumn',
			'width'=>'50px',
			'value'=>function ($model, $key, $index, $column) {
						return GridView::ROW_COLLAPSED;
			},
			'detail'=>function ($model, $key, $index, $column) {
					return Yii::$app->controller->renderPartial('recibos', ['poliza'=>$model->id_poliza]);
			},

			'headerOptions'=>['class'=>'kartik-sheet-style'],
			'expandOneOnly'=>true
        ],

      [
       'class' => 'yii\grid\ActionColumn',
	     'contentOptions' => ['style' => 'width:150px;'],
       'header'=>'GESTION DE RECIBOS',
       'template' => '{view} {update} {renovacion} {recibos} {anulacion}',


            'buttons' => [
                'view' => function ($url,$model, $key) {
                  return Html::a('<i class="fa fa-binoculars fa-lg" aria-hidden="true"></i>', ['poliza-salud/view','id' => $model->id_poliza], [
                      'id' => 'activity-index-link',
                      'title' => Yii::t('app', 'Ver poliza'),'rel'=>'tooltip',
                      'data-pjax' => '0',
                  ]);
                 },

			     'update' => function ($url,$model, $key) {
                   return Html::a('<i class="fa fa-pencil fa-lg" aria-hidden="true"></i>', ['poliza-salud/update','id' => $model->id_poliza], [
                       'id' => 'activity-index-link',
                       'title' => Yii::t('app', 'Actualizar Poliza'),'rel'=>'tooltip',
                       'data-pjax' => '0',

                   ]);
                  },
               'renovacion' => function ($url,$model, $key) {
                     if($model->estatus_poliza=='Vencida'){
                       return Html::a('<i class="fa fa-retweet fa-lg" aria-hidden="true"></i>', ['renovacion-salud/update','id' => $model->id_poliza], [
                           'id' => 'activity-index-link',
                           'title' => Yii::t('app', 'Renovar Poliza'),'rel'=>'tooltip',
                           'data-pjax' => '0',
                       ]);
                     }else{
                               return '';
                     }
                  },
                'recibos' => function ($url,$model, $key) {
                         return Html::a('<i class="fa fa-plus fa-lg" aria-hidden="true"></i>', ['recibo-salud/update','id' => $model->id_poliza], [
                             'id' => 'activity-index-link',
                             'title' => Yii::t('app', 'Crear Recibo Adicional'),'rel'=>'tooltip',
                             'data-pjax' => '0',
                         ]);
                 },
                 'anulacion' => function ($url,$model, $key) {
                           return Html::a('<i class="fa fa-trash fa-lg" aria-hidden="true"></i>', ['anulacion','id' => $model->id_poliza], [
                               //'id' => 'activity-index-link',
                               'title' => Yii::t('app','Anulacion de Poliza'),'rel'=>'tooltip',
                               'data-pjax' => '0',
                              'data-confirm'=>'Tiene '.PolizaAnulacion::CantReclamos($model->id_poliza).' Reclamos Asociada a la poliza',
                           ]);
                   },
               /*'anulacion' => function ($url,$model, $key) {
                         return Html::a('<i class="fa fa-trash fa-lg" aria-hidden="true"></i>', '#', [
                            'id' => 'activity-index-link',
                            'title' => Yii::t('app','Anular Poliza'),'rel'=>'tooltip',
                            'data-toggle' => 'modal',
                            'data-target' => '#modal',
                            'data-url' => Url::to(['anulacion', 'id' => $model->id_poliza]),
                            'data-pjax' => '0',
                        ]);
               }*/
             ]
        ],
                    ];

  	echo GridView::widget([
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
  		    'columns' => $gridColumns,
  	    	'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false

  		'toolbar' => [
  			[
  				'content'=>
  					Html::a('<i class="glyphicon glyphicon-plus"></i> Registrar', ['create'],[
  						//'type'=>'button',
  						'title'=>Yii::t('app', 'Registrar Poliza Salud'),
  						'class'=>'btn btn-success',
  					]) . ' '.
  					Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
  						'class' => 'btn btn-default',
  						'title' => Yii::t('app', 'Actualizar Grid')
  					]),
  			],
  			'{export}',
  			'{toggleData}'
  		],

  		'pjax' => false,
  		'pjaxSettings' => [
  			'options' => ['id' => 'salud-pjax-id'],// UNIQUE PJAX CONTAINER ID
  		],
  		'bordered' => false,
  		'resizableColumns'=>true,
  		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
  		'striped' => true,
  		'condensed' => false,
  		'responsive' => true,
  		'hover' => true,
  		'showPageSummary' => false,
    		'panel' => [
          'heading'=>'<h2 class="panel-title" style="font-weight:bold;"><i class="fa fa-user-md fa-2x" aria-hidden="true"></i>&nbsp; &nbsp;POLIZAS DE SALUD INDIVIDUAL</h2>',
    			'type' => GridView::TYPE_INFO
    		],
      ]);
  	?>

</div>
<?php
$string = <<<EXP2
$("[rel=tooltip]").tooltip({ placement: 'top'});
EXP2;
$this->registerJs($string, \yii\web\View::POS_READY);
?>
<?php
$string='
@brand-success: #5cb85c;
@brand-info:    #5bc0de;
@brand-warning: #f0ad4e;
@brand-danger:  #d9534f;

.fa{margin:0px 4px 0px 0px;}
 .fa-trash{
     color:  #d9534f;
 }
.fa-plus{
     color:  #5cb85c;
 }
.fa-retweet{
     color:  #5bc0de;
 }
.fa-binoculars{
     color:  #39CCCC;
}';
$this->registerCss($string);
?>
