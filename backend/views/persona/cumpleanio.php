<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\Persona;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PolizaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//http://www.w3schools.com/colors/colors_picker.asp colores

$this->title = "";

?>

<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
            <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
</div>
</br></br>
<div class="poliza-index">
    <?php
 
  	$gridColumns = [
  		['class' => 'kartik\grid\SerialColumn'],
   
       'nombre',
       'apellido',
       'identificacion',
       'telefono',
       'telefono2',
       'telefono3',
  	];


  	echo GridView::widget([
          'dataProvider' => $dataProvider,
          //'filterModel' => $searchModel,
  		'columns' => $gridColumns,
  		'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
  		'toolbar' => [
  			[
  				'content'=>
  					Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['vencidas'], [
  						'class' => 'btn btn-default',
  						'title' => Yii::t('app', 'Actualizar Grid')
  					]),

  			],
  			'{export}',
  			'{toggleData}'
  		],

  		'pjax' => false,
  		'pjaxSettings' => [
  			'options' => ['id' => 'vencidas-pjax-id'],// UNIQUE PJAX CONTAINER ID
  		],
  		'bordered' => false,
  		'resizableColumns'=>true,
  		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
  		'striped' => true,
  		'condensed' => false,
  		'responsive' => true,
  		'hover' => true,
  		'showPageSummary' => false,
    		'panel' => [
          'heading'=>'<h2 class="panel-title"><i class="fa fa-birthday-cake" aria-hidden="true"></i>&nbsp; &nbsp;CUMPLEA&NtildeOS DEL DIA</h2>',
    			'type' => GridView::TYPE_INFO
    		],
      ]);
  	?>
</div>

<?php

$string='.crumbs2 {
    display: block;
    margin-left: 27px;
    padding: 0;
    padding-top: 10px;
}
.crumbs2 li { display: inline; }
.crumbs2 li a,
.crumbs2 li a:link,
.crumbs2 li a:visited {
    color: #666;
    display: block;
    float: left;
    font-size: 12px;
    padding: 7px 16px 7px 19px;
    position: relative;
    text-decoration: none;
    border: 1px solid #d9d9d9;
    border-right-width: 0px;
}
.crumbs2 li a  {
    background-image: -webkit-gradient(linear,left bottom,left top,color-stop(0.45, rgb(241,241,241)),color-stop(0.73, rgb(245,245,245)));
    background-image: -moz-linear-gradient( center bottom, rgb(241,241,241) 45%, rgb(245,245,245) 73%);

    /* For Internet Explorer 5.5 - 7 */
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f5f5f5);

    /* For Internet Explorer 8 */
    -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f5f5f5)";
}

.crumbs2 li.first a {
    border-top-left-radius: 5px;
    -moz-border-radius-topleft: 5px;
    -webkit-border-top-left-radius: 5px;
}
.crumbs2 li.last a {
    border-right-width: 1px;
    border-bottom-right-radius: 5px;
    -moz-border-radius-bottomright: 5px;
    -webkit-border-bottom-right-radius: 5px;
}

.crumbs2 li a:hover {
    border-top-color: #c4c4c4;
    border-bottom-color: #c4c4c4;

    background-image: -webkit-gradient(linear,left bottom,left top,color-stop(0.45, rgb(241,241,241)),color-stop(0.73, rgb(248,248,248)));
    background-image: -moz-linear-gradient( center bottom, rgb(241,241,241) 45%, rgb(248,248,248) 73%);

    /* For Internet Explorer 5.5 - 7 */
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f8f8f8, endColorstr=#f1f1f1);

    /* For Internet Explorer 8 */
    -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#f8f8f8, endColorstr=#f1f1f1)";

    color: #333;

    -moz-box-shadow: 0px 2px 2px #e8e8e8;
    -webkit-box-shadow: 0px 2px 2px #e8e8e8;
    box-shadow: 0px 2px 2px #e8e8e8;
}
';
$this->registerCss($string);
?>
