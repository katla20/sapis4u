<?php

use yii\helpers\Html;
//use yii\widgets\DetailView;
use kartik\detail\DetailView;
use yii\helpers\Url;
use backend\models\Persona;
use kartik\icons\Icon;
use yii\bootstrap\Modal;


/* @var $this yii\web\View */
/* @var $model frontend\models\persona */

$this->title = '';//$model->id_persona;


$directoryUploads = '/uploads/cliente/';
$directoryUploadsAll = $ruta.'/uploads/cliente/';


?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
            <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
            <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Cliente')?></div></a>
            <a href="#" class="btn btn-default"><div>Detalles Cliente</div></a>
</div>
</br></br>
<div class="cliente-view">
	<?php
    $direc='';
    foreach ($model->direccions as $direccions) {

	    $sector= $direccions['sector'];
        //$Ram=$Ramos['nombre'];

     }
	 $modelDirecciones=Persona::datosDireccion($model->id_persona);
     $modelPoliza=Persona::datosPoliza($model->id_persona);
	// $ope=substr($ope, 0, -2);

	$attributes = [
    /*[
        'group'=>true,
        'label'=>'Aseguradora',
        'rowOptions'=>['class'=>'info']
    ],*/
    [
        'columns' => [
		    [
                'attribute'=>'id_persona',
				        'label'=>'Código',
                'format'=>'raw',
                'value'=>'<kbd>'.$model->id_persona.'</kbd>',
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ],
            [
                'attribute'=>'identificacion',
                'label'=>'Cédula',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],



        ],
    ],
	[
        'columns' => [

			[
                'attribute'=>'nombre',
                'label'=>'Nombre',
				        'value'=>''.$model->nombre.' '.$model->segundo_nombre.'',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],

			[
                'attribute'=>'apellido',
                'label'=>'Apellido',
				        'value'=>''.$model->apellido.' '.$model->segundo_apellido.'',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],

        ],
    ],
	[
        'columns' => [
            [
                'attribute'=>'estatus',
                'label'=>'Estatus',
                'format'=>'raw',
                'type'=>DetailView::INPUT_SWITCH,
                'widgetOptions' => [
                    'pluginOptions' => [
                        'onText' => 'Activo',
                        'offText' => 'Inactivo',
                    ]
                ],
                'value'=>($model->estatus==1)? '<span class="label label-success">Activo</span>' : '<span class="label label-danger">Inactivo</span>',
                //'valueColOptions'=>['style'=>'width:10%']
            ],

        ],
    ],
	];


	//print_r($modelDirecciones);exit;

	 if(!empty($modelDirecciones)){

		  $attributes[]=
				[
							'group'=>true,
							'label'=>'Direcciones',
							'rowOptions'=>['class'=>'success']
				];
	 }

	foreach ($modelDirecciones as $re) {



			$attributes[]=
				 [
				'columns' => [
					[
						//'attribute'=>'numero_poliza',
						'label'=>'Numeros Telefonico:',
						'format'=>'raw',
						'value'=>$re['telefono'].'  -  '.$re['telefono2'].'  -  '.$re['telefono3'],
						'valueColOptions'=>['style'=>'width:10%'],
						'labelColOptions'=>['style'=>'width:10%'],
						'displayOnly'=>true
					],


					[
						//'attribute'=>'numero_poliza',
						'label'=>'Tipo',
						'format'=>'raw',
						'value'=>$re['nombre_direccion'],
						'valueColOptions'=>['style'=>'width:10%'],
						'labelColOptions'=>['style'=>'width:10%'],
						'displayOnly'=>true
					],

					[
						//'attribute'=>'fecha_vigenciahasta',
						'label'=>'Código Postal',
						'format'=>'raw',
						'value'=> $re['codigo_postal'],
						'valueColOptions'=>['style'=>'width:10%'],
						'labelColOptions'=>['style'=>'width:10%'],
						'displayOnly'=>true
					],
				],
			   ];
			$attributes[]=
				 [
					'columns' => [
					[
						//'attribute'=>'numero_poliza',
						'label'=>'Estado',
						'format'=>'raw',
						'value'=>$re['estado'],
						'valueColOptions'=>['style'=>'width:10%'],
						'labelColOptions'=>['style'=>'width:10%'],
						'displayOnly'=>true
					],


					[
						//'attribute'=>'numero_poliza',
						'label'=>'Ciudad',
						'format'=>'raw',
						'value'=>$re['ciudad'],
						'valueColOptions'=>['style'=>'width:10%'],
						'labelColOptions'=>['style'=>'width:10%'],
						'displayOnly'=>true
					],

					[
						//'attribute'=>'fecha_vigenciahasta',
						'label'=>'Zona',
						'format'=>'raw',
						'value'=> $re['zona'],
						'valueColOptions'=>['style'=>'width:10%'],
						'labelColOptions'=>['style'=>'width:10%'],
						'displayOnly'=>true
					],
				],
			];
			$attributes[]=
				 [
				'columns' => [
					[
						//'attribute'=>'numero_poliza',
						'label'=>'Calle /Avenida',
						'format'=>'raw',
						'value'=>$re['calle_avenida'],
						'valueColOptions'=>['style'=>'width:10%'],
						'labelColOptions'=>['style'=>'width:10%'],
						'displayOnly'=>true
					],


					[
						//'attribute'=>'numero_poliza',
						'label'=>'Sector',
						'format'=>'raw',
						'value'=>$re['sector'],
						'valueColOptions'=>['style'=>'width:10%'],
						'labelColOptions'=>['style'=>'width:10%'],
						'displayOnly'=>true
					],

					[
						//'attribute'=>'fecha_vigenciahasta',
						'label'=>'Nro',
						'format'=>'raw',
						'value'=> $re['nro'],
						'valueColOptions'=>['style'=>'width:10%'],
						'labelColOptions'=>['style'=>'width:10%'],
						'displayOnly'=>true
					],
				],

			];

     }

  //   print_r($modelPoliza); exit;
     if(!empty($modelPoliza)){
      $url="";
      $estatus="";
		  $attributes[]=
				[
							'group'=>true,
							'label'=>'Polizas',
							'rowOptions'=>['class'=>'success']
				];

	    }
     foreach ($modelPoliza as $po) {

        if($po['estatus_poliza']=='Vigente'){
		        $estatus ="<span class='label label-success'>".$po['estatus_poliza'] .'</span>';

      		}else if($po['estatus_poliza']=='Anulada'){
      			   $estatus ="<span class='label label-danger'>".$po['estatus_poliza'] .'</span>';
    		}else if($po['estatus_poliza']=='Vencida'){
      		     $estatus = "<span class='label label-warning'>".$po['estatus_poliza'] .'</span>';
      	}

        if($po['id_tipo']==1 && $po['id_ramo']==1){
    				$url=	Url::to(['poliza/view-modal','id' => $po['id_poliza']]);
    			}else if($po['id_tipo']==1 && $po['id_ramo']==2){
    				$url=	Url::to(['poliza-salud/view-modal','id' => $po['id_poliza']]);
    		}


      $attributes[]=
				 [
				'columns' => [
					[
						//'attribute'=>'numero_poliza',
						'label'=>Html::a('#  '.$po['numero_poliza'], '#', [
									'id' => 'activity-index-link',
									'title'=>Yii::t('app', 'Registrar Cliente'),'rel'=>'tooltip',
									'data-toggle' => 'modal',
									'data-target' => '#modal',
									'data-url' => $url,
									'data-pjax' => '0',
									]).'  '.$estatus,
						'format'=>'raw',
						'value'=> '',
						'valueColOptions'=>['style'=>'display:none'],
						'labelColOptions'=>['style'=>'width:10%'],
						'displayOnly'=>true
					],

				],
			   ];
     }
     
 if(count($datosDocumentos)){
 	
		 	$options = [
		    'title' => 'Documento',
		    'target' => '_blank',
		    'alt' => 'enlase a Documento',
		    ];
            $attributes[]=
				[
							'group'=>true,
							'label'=>'DOCUMENTOS',
							'rowOptions'=>['class'=>'success']
				];

	  
	    
	foreach ($datosDocumentos as $documento) {
 
         $attributes []= 
         [
    
            'columns' => [
                [
    				'attribute'=>'tipo_vehiculo',
                    'label'=>'Descripcion Documento',
                    'value'=>$documento['ruta'],
                    'displayOnly'=>true,
                    'valueColOptions'=>['style'=>'width:40%']
                ],
                [
    				'label'=>'',
    				'format'=>'raw', 
    				'value'=>"<a href='".Url::toRoute(["persona/download", "file" => $documento['ruta']])."' 'target'= '_blank' >Descargar archivo</a>",
                    'format' => ['raw'],
    				'valueColOptions'=>['style'=>'width:50%'],
    				'labelColOptions'=>['style'=>'display:none;'],
    				'displayOnly'=>true
    			], 
    
            ],
        ];
    
 	}
 	
}
     
     
//print_r($model);
	// View file rendering the widget

	echo DetailView::widget([
		'model' => $model,
		'attributes' => $attributes,
		'mode' => 'view',
		'enableEditMode'=> false,
		//'bordered' => 'bordered',
		//'striped' => $striped,
		//'condensed' => $condensed,
		'responsive' => 'responsive',
		//'hover' => $hover,
		//'hAlign'=>$hAlign,
		//'vAlign'=>$vAlign,
		//'fadeDelay'=>$fadeDelay,
		'deleteOptions'=>[ // your ajax delete parameters
			'params' => ['id' => 1000, 'kvdelete'=>true],
		],
		'panel'=>[
        'heading'=>'<h2 class="panel-title" style="font-weight:bold;">'.Icon::show('user', ['class' => 'fa-2x']).'&nbsp; &nbsp;CLIENTES</h2>',
        'type'=>DetailView::TYPE_INFO,

        ],
		'container' => ['id'=>'kv-demo'],
		//'formOptions' => ['action' => Url::current(['update' => 'kv-demo'])] // your action to delete
	]);





    ?>


</div>



<?php

$string = <<<EXP2
$(document).on('click', '#activity-index-link', (function(e) {

		$.get($(this).data('url'),{'tipo':'liquidacion-comisiones','id':id_intermediario},
          function (data) {
            $('.modal-body').html(data);
            $('#modal').modal({
				backdrop: true,
				keyboard: true
			});
          }
		);
		$('#modal').on('hidden.bs.modal', function (e) {
				$(this).find('.modal-body').html('');
			});



}));

$('#modal .modal-dialog').css({
     width: '70%',
     heigth: '50%'});

EXP2;

$this->registerJs($string, \yii\web\View::POS_READY);


Modal::begin([
 'options' => [
        'id' => 'modal',
        'tabindex' => false, // important for Select2 to work properly
  ],
]);
Modal::end();




?>
