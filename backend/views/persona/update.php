<?php

use yii\helpers\Html;
use kartik\icons\Icon;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\models\persona */

$this->title = "";
?>
<div class="bg-aqua top-modulo">
          <span class="icon-modulo"><?=Icon::show('user', ['class' => 'fa-3x'])?></span>
          <span style="color:white;font-size:18px;font-weight:bold;"><?=Yii::t('app', 'Actualizar Cliente');?></span>
</div>
<br/>
<div class="persona-update">

    <?= $this->render('_form', [
        'model' => $model,
        'modelDireccion' => (empty($modelDireccion)) ? [new Direccion] : $modelDireccion,
        'modelCliente' => (empty($modelCliente)) ? [new Cliente] : $modelCliente,
        'modelCargaFamiliar' => (empty($modelCargaFamiliar)) ? [new CargaFamiliar] : $modelCargaFamiliar,
        'documentos'=>$documentos,

    ]) ?>

</div>
