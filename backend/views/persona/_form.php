<?php

use yii\helpers\Html;
//use yii\bootstrap\ActiveForm;
//use yii\bootstrap\ActiveField;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use frontend\models\Pais;
use frontend\models\Estado;
use frontend\models\Ciudad;
use frontend\models\Zona;
use kartik\select2\Select2;
//use kartik\depdrop\Depdrop; //http://demos.krajee.com/widget-details/depdrop
use kartik\widgets\DepDrop;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;
use yii\web\JsExpression;

?>

<div class="persona-form">
  <?php $form = ActiveForm::begin([
    'id' => 'dynamic-form',
    'options' => ["enctype" => "multipart/form-data"],
    'enableAjaxValidation' => true,
    'enableClientScript' => true,
    'enableClientValidation' => true,
    ]); ?>
  <div class="box box-default">
  <div class="box-header">
    <h4 class="box-title"><i class="glyphicon glyphicon-user"></i> Datos Personales</h4>
      <div class="row">
        <div class="col-md-4">

        <?php  $list = ['V'=>'Natural','J'=>'Juridico'];
          /* Display an inline checkbox list */
		      $model->tipo='V';
          echo $form->field($model, "tipo")->radioList($list, ['inline'=>true]);
         ?>
         <?php
         $action=($model->isNewRecord) ? 'insert' :'update';
         echo $form->field($model, 'action')->hiddenInput(['value'=>$action])->label(false);?>
        </div>
		<div class="col-md-6">
			<div class="input-group">

				<?//=$form->field($model, 'preidentificacion')->dropDownList(['J' => 'J', 'V' => 'V','E' => 'E', 'G' => 'G']);?>
				<?=$form->field($model, 'identificacion',[
											'addon' => [
											            'prepend' => [
															  'content'=>'<select id="persona-preidentificacion" name="Persona[preidentificacion]">
															                      <option value="V">V</option>
																				  <option value="E">E</option>
																				  <option value="J">J</option>
																				  <option value="G">G</option>
																		  </select>'
														],
												        'append' => ['content' => '--'],
														'groupOptions' => ['class'=>'input-group-md'],
														'contentAfter' => '<input type="text" id="persona-postidentificacion" name="Persona[postidentificacion]" class="form-control" placeholder="ult rif" maxlength="1">'
											],
											'inputOptions' => [
                                              'placeholder' => 'Cedula o Rif'
											]
                                          ])->textInput(['maxlength' => true])?>
			</div>
         </div>
     </div>
    <div class="box-tools pull-right">
      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div><!-- /.box-tools -->
  </div><!-- /.box-header -->
  <div class="box-body">
	 <div class="container-items"><!-- widgetBody -->

     <div class="row">
			  <div class="col-sm-6" >
			  <?= $form->field($model, 'nombre',[
								  'addon' => [
									  'prepend' => [
										  'content' => '<i class="fa fa-pencil"></i>'

									  ]
								  ]
							  ])->textInput(['maxlength' => true]) ?>
			  </div>
			  <div class="col-sm-6" id="snombre">
				  <?= $form->field($model, 'segundo_nombre',[
									'addon' => [
										'prepend' => [
											'content' => '<i class="fa fa-pencil"></i>'

										]
									]
								])->textInput(['maxlength' => true]) ?>
			  </div>
        </div>
		<div class="row ocultar" id="row-juridico">
			<div class="col-sm-3">
					<?= $form->field($model, 'nit',[
												  'addon' => [
													  'prepend' => [
														  'content' => '<i class="fa fa-pencil"></i>'

													  ]
												  ]
											  ])->textInput(['maxlength' => true,'class' => 'ocultar']) ?>
			</div>
			<div class="col-sm-4">
						<?= $form->field($model, 'numero_registro',[
													  'addon' => [
														  'prepend' => [
															  'content' => '<i class="fa fa-pencil"></i>'

														  ]
													  ]
												  ])->textInput(['maxlength' => true,'class' => 'ocultar']) ?>

			</div>
			<div class="col-sm-10">
						<?= $form->field($model, 'actividad_economica',[
													  'addon' => [
														  'prepend' => [
															  'content' => '<i class="fa fa-pencil"></i>'

														  ]
													  ]
												  ])->textInput(['maxlength' => true,'class' => 'ocultar']) ?>

			</div>
			<div class="col-sm-10">
						<?= $form->field($model, 'representante',[
													  'addon' => [
														  'prepend' => [
															  'content' => '<i class="fa fa-pencil"></i>'

														  ]
													  ]
												  ])->textInput(['maxlength' => true]) ?>

			</div>
     </div>
        <div class="row" id="apellidos" >
					  <div class="col-sm-6">
						  <?= $form->field($model, 'apellido',[
                                            'addon' => [
                                                'prepend' => [
                                                    'content' => '<i class="fa fa-pencil"></i>'

                                                ]
                                            ]
                                        ])->textInput(['maxlength' => true]) ?>
					  </div>
					  <div class="col-sm-6">
						  <?= $form->field($model, 'segundo_apellido',[
                                            'addon' => [
                                                'prepend' => [
                                                    'content' => '<i class="fa fa-pencil"></i>'

                                                ]
                                            ]
                                        ])->textInput(['maxlength' => true]) ?>
					  </div>
        </div>
          <div class="row" id="otros">
			<div class="col-sm-6">
              <?php
               $list = ['F' => 'Femenino', 'M' => 'Masculino'];
               /* Display an inline checkbox list */
               echo $form->field($model, "sexo")->radioList($list, ['inline'=>true]);
               ?>
					  </div>
          </div>
          <div class="row">
				  <div class="col-sm-4">
					   <?=$form->field($model, 'fecha_nacimiento',[
													 'addon' => [
														 'prepend' => [
															 'content' => '<i class="glyphicon glyphicon-calendar"></i>'

														 ]
													 ]
												 ])->widget(\yii\widgets\MaskedInput::className(),
																		  [
																		  'clientOptions' => ['alias' =>  'date']
																		  ])?>
					</div>
					  <div class="col-sm-8">
							<?= $form->field($model, 'correo',[
                                            'addon' => [
                                                'prepend' => [
                                                    'content' => '<i class="glyphicon glyphicon-envelope"></i>'

                                                ]
                                            ]
                                        ])->textInput(['maxlength' => true]) ?>
					   </div>
          </div><!--container-items-->
          <div class="row">
           <!--http://plugins.krajee.com/file-input#events-->
                <div class="col-sm-12">
                   <?=$form->field($documentos, 'doc_files[]')->widget(FileInput::classname(),[
                                                   'options'=>[
                                                       'multiple'=>true,
                                                       'accept'=>'*',
                                                   ],
                                                   'pluginOptions' => [
                                                      'uploadAsync'=>false,
                                                       'showUpload' => false,
                                                       'dropZoneEnabled'=>false,
                                                       'uploadUrl' => Url::to(['persona/upload-file']),
                                                        'allowedFileExtensions'=>['pdf','jpg'],
                                                       'uploadExtraData' => new JsExpression('function() {
                                                         var obj = {};
                                                         $("form#dynamic-form").find("input").each(function() {
                                                             var id = $(this).attr("name"), val = $(this).val();
                                                             obj[id] = val;
                                                         });
                                                         return obj;
                                                       }'),
                                                       /*'initialPreview'=>[
                                                                   "http://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/FullMoon2010.jpg/631px-FullMoon2010.jpg",
                                                                   "http://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Earth_Eastern_Hemisphere.jpg/600px-Earth_Eastern_Hemisphere.jpg"
                                                        ],
                                                         'initialPreviewAsData'=>true,
                                                         'initialCaption'=>"The Moon and the Earth",
                                                         'initialPreviewConfig' => [
                                                                   ['caption' => 'Moon.jpg', 'size' => '873727'],
                                                                   ['caption' => 'Earth.jpg', 'size' => '1287883'],
                                                               ],
                                                       'overwriteInitial'=>false,*/

                                                   ]
                                                ])->label(false);
                 ?>
              </div>
          </div>
          <div class="row">
            <div class="col-sm-12" style="color:red;">Debe presionar el boton <i class="glyphicon glyphicon-upload text-info"></i> por cada imagen que este en la bandeja de imagenes,una vez que la imagen indique <i class="glyphicon glyphicon-ok-sign text-success"></i> ya esta preparada para guardar</div></br>
          </div>
  </div><!-- /.box-body -->
</div><!-- /.box -->

    <?php DynamicFormWidget::begin([
       'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
       'widgetBody' => '.container-items', // required: css class selector
       'widgetItem' => '.item', // required: css class
       'limit' => 4, // the maximum times, an element can be added (default 999)
       'min' => 0, // 0 or 1 (default 1)
       'insertButton' => '.add-item', // css class
       'deleteButton' => '.remove-item', // css class
       'model' => $modelDireccion[0],
       'formId' => 'dynamic-form',
       'formFields' => [
           'nombre_direccion',
           'id_zona',
           'calle_avenida',
           'nro',
           'sector',
           'direccion',
           'telefono',
           'telefono2',
           'telefono3'
       ],
     ]);?>

         <div class="box box-default">
         <div class="box-header">
             <h4>
                 <i class="glyphicon glyphicon-envelope"></i> Direcciones
                 <button type="button" class="add-item btn btn-success btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i>Agregar</button>
             </h4>

         </div>
         <div class="box-body">
             <div class="container-items"><!-- widgetBody -->
             <?php foreach ($modelDireccion as $i => $modelDireccion): ?>
                 <div class="item panel panel-default"><!-- widgetItem -->
                     <div class="panel-heading">
                         <h5 class="panel-title pull-left">Direccion</h5>
                         <div class="pull-right">
                             <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                         </div>
                         <div class="clearfix"></div>
                     </div>
                     <div class="panel-body">
                         <?php
                             // necessary for update action.
                             if (! $modelDireccion->isNewRecord) {
                               //http://stackoverflow.com/questions/28925624/yii2kartik-depdrop-widget-default-value-on-update
                                 echo Html::activeHiddenInput($modelDireccion, "[{$i}]id");

								// echo CHtml::hiddenField('name' , 'value', array('id' => 'hiddenInput'));[$i]
                                 //ADDITIONAL PARAM ID YOU MAY USE TO SELECT A DEFAULT VALUE OF YOUR MODEL IN YOUR DEPDROP WHEN YOU WANT TO UPDATE:
                                  echo Html::hiddenInput('model_idEstado', $modelDireccion->id, ['id'=>'model_idEstado']);
								  // echo Html::hiddenInput('model_idEstado',"[$i]id", ['id'=>'model_idEstado']);
                             }
                         ?>
                      <div class="row">

						<div class="col-sm-6">
                         <? $modelDireccion->nombre_direccion='Direccion Habitacion';?>
                         <?=$form->field($modelDireccion,"[{$i}]nombre_direccion")->widget(Select2::classname(), [
                              'data' =>['Direccion Habitacion'=>'Habitacion','Direccion Trabajo'=>'Trabajo','Direccion Cobro'=>'Cobro'],
                              'language' => 'en',
                               'options' => ['placeholder' => 'Select ...'],
                              'pluginOptions' => [
                                  'allowClear' => true
                              ],
                            ]);
                          ?>
                        </div>
						<div class="col-sm-6">
                               <?php
								   echo $form->field($modelDireccion,"[{$i}]id_estado",[
                                                 'addon' => [
                                                     'prepend' => [
                                                         'content' => '<i class="fa fa-map-marker"></i>'

                                                     ]
                                                 ]
                                             ])->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map(Estado::find()->where(['id_pais' => '10'])->all(), 'id_estado', 'nombre'),
                                    'language' => 'en',
                                     'options' => ['placeholder' => 'Select a state ...'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                  ]);

                                ?>
                             </div>
                      </div>
                        <div class="row">
							 <div class="col-sm-6">
								<?=$form->field($modelDireccion, "[{$i}]id_ciudad",[
                                              'addon' => [
                                                  'prepend' => [
                                                      'content' => '<i class="fa fa-map-marker"></i>'

                                                  ]
                                              ]
                                          ])->widget(DepDrop::classname(), [
										'data'=> [''=>'Seleccione '],
										'options'=>['placeholder'=>'Selecione ...'],
										'type' => DepDrop::TYPE_SELECT2,
										'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
										'pluginOptions'=>[
										 'depends'=>["direccion-$i-id_estado"],
										 'placeholder' => 'Seleccione ...',
										 'url'=>Url::to(['dependent-dropdown/child-account', 'db'=>Ciudad::classname(),'cmpo_dep'=>'id_estado','cmpo_mostrar'=>'nombre','id_cmpo'=>'id_ciudad']),
										 'loadingText' => 'Leyendo ...',
									   ]
									   ]);

								 ?>
							 </div>
							 <div class="col-sm-6">

								<?=$form->field($modelDireccion, "[{$i}]id_zona",[
                                              'addon' => [
                                                  'prepend' => [
                                                      'content' => '<i class="fa fa-map-marker"></i>'

                                                  ]
                                              ]
                                        ])->widget(DepDrop::classname(), [
										'data'=> [''=>'Seleccione '],
										'options'=>['placeholder'=>'Selecione ...'],
										'type' => DepDrop::TYPE_SELECT2,
										'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
										'pluginOptions'=>[
										 'initialize'=>true,
										 'depends'=>["direccion-$i-id_ciudad"],
										 'placeholder' => 'Seleccione ...',
										 'url'=>Url::to(['dependent-dropdown/child-account', 'db'=>Zona::classname(),'cmpo_dep'=>'id_ciudad','cmpo_mostrar'=>'nombre','id_cmpo'=>'id_zona']),
										 'loadingText' => 'Leyendo ...',
										 'params'=>['model_id1'] ///SPECIFYING THE PARAM
									   ]
									   ]);
								 ?>
							 </div>
						</div>
						<div class="row">
							 <div class="col-sm-6">
                                 <?= $form->field($modelDireccion, "[{$i}]sector",[
                                                               'addon' => [
                                                                   'prepend' => [
                                                                       'content' => '<i class="fa fa-map-marker"></i>'

                                                                   ]
                                                               ]
                                                           ])->textInput(['maxlength' => true]) ?>
                             </div>
							  <div class="col-sm-6">
									<?= $form->field($modelDireccion, "[{$i}]calle_avenida",[
																  'addon' => [
																	  'prepend' => [
																		  'content' => '<i class="fa fa-map-marker"></i>'

																	  ]
																  ]
															  ])->textInput(['maxlength' => true]) ?>
							  </div>
						</div>
           <div class="row">
						    <div class="col-sm-6">
                                 <?= $form->field($modelDireccion, "[{$i}]edif_bloque",[
                                                               'addon' => [
                                                                   'prepend' => [
                                                                       'content' => '<i class="fa fa-map-marker"></i>'

                                                                   ]
                                                               ]
                                                           ])->textInput(['maxlength' => true]) ?>
                </div>
                 <div class="col-sm-6">
                     <?= $form->field($modelDireccion, "[{$i}]nro",[
                                                   'addon' => [
                                                       'prepend' => [
                                                           'content' => '<i class="fa fa-map-marker"></i>'

                                                       ]
                                                   ]
                                               ])->textInput(['maxlength' => true]) ?>
                 </div>
            </div><!-- .row -->
             <div class="row">
                 <div class="col-sm-12">
                     <?= $form->field($modelDireccion, "[{$i}]direccion",[
                                                   'addon' => [
                                                       'prepend' => [
                                                           'content' => '<i class="fa fa-map-marker"></i>'

                                                       ]
                                                   ]
                                               ])->textArea(['rows' => '3']) ?>
                 </div>
             </div><!-- .row -->
             <div class="row">
                 <div class="col-sm-4">
                       <?= $form->field($modelDireccion, "[{$i}]telefono",[
                                                     'addon' => [
                                                         'prepend' => [
                                                             'content' => '<i class="fa fa-phone"></i>'

                                                         ]
                                                     ]
                                                 ])->textInput(['maxlength' => true]) ?>
                 </div>
                 <div class="col-sm-4">
                     <?= $form->field($modelDireccion, "[{$i}]telefono2",[
                                                   'addon' => [
                                                       'prepend' => [
                                                           'content' => '<i class="fa fa-phone"></i>'

                                                       ]
                                                   ]
                                               ])->textInput(['maxlength' => true]) ?>
                 </div>
                 <div class="col-sm-4">
                     <?= $form->field($modelDireccion, "[{$i}]telefono3",[
                                                   'addon' => [
                                                       'prepend' => [
                                                           'content' => '<i class="fa fa-phone"></i>'

                                                       ]
                                                   ]
                                               ])->widget(\yii\widgets\MaskedInput::classname(), [
                                                        'mask' => '999-999-9999',
                                                     	  ])?>

                 </div>
             </div><!-- .row -->
                     </div>
                 </div>
               <?php endforeach; ?>
           </div><!--container-items-->
           </div><!--panel body-->
     </div><!-- .panel -->
     <?php DynamicFormWidget::end(); ?>

	  <?php DynamicFormWidget::begin([
    	   'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
    	   'widgetBody' => '.container-cargaFamiliar', // required: css class selector
    	   'widgetItem' => '.item-carga', // required: css class
    	   'limit' => 10, // the maximum times, an element can be added (default 999)
    	   'min' => 0, // 0 or 1 (default 1)
    	   'insertButton' => '.add-carga', // css class
    	   'deleteButton' => '.remove-carga', // css class
    	   'model' => $modelCargaFamiliar[0],
    	   'formId' => 'dynamic-form',
    	   'formFields' => [
    		   'parentesco',
    		   'nombre',
           'segundo_nombre',
           'apellido',
    		   'segundo_apellido',
    		   'identidad',
    		   'fecha_nacimiento',
    		   'sexo'
    	   ],
	    ]);?>

     <div class="box box-default carga-familiar">
     <div class="box-header">
         <h4>
             <i class="glyphicon glyphicon-user"></i> Carga Familiar
             <button type="button" class="add-carga btn btn-success btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i>Agregar</button>
         </h4>
     </div>
     <div class="box-body">
         <div class="container-cargaFamiliar"><!-- widgetBody -->
         <?php foreach ($modelCargaFamiliar as $i => $modelCargaFamiliar): ?>
             <div class="item-carga panel panel-default"><!-- widgetItem -->
                 <div class="panel-heading">
                     <h5 class="panel-title pull-left">Datos Personales</h5>
                     <div class="pull-right">
                         <button type="button" class="remove-carga btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                     </div>
                     <div class="clearfix"></div>
                 </div>
                 <div class="panel-body">
                   <div class="row">
                      <div class="col-sm-6">

                     <?=$form->field($modelCargaFamiliar,"[{$i}]parentesco")->widget(Select2::classname(), [
                          'data' =>['Padre'=>'Padre','Madre'=>'Madre','Hijo(a)'=>'Hijo(a)'],
                          'language' => 'en',
                           'options' => ['placeholder' => 'Select a state ...'],
                          'pluginOptions' => [
                              'allowClear' => true
                          ],
                        ]);
                      ?>
                      </div>
                      <div class="col-sm-6">
                          <?= $form->field($modelCargaFamiliar, "[{$i}]identificacion")->textInput(['maxlength' => true]) ?>
                     </div>
                  </div>

                  <div class="row">

                      <div class="col-sm-6">
                            <?= $form->field($modelCargaFamiliar, "[{$i}]nombre",[
                                                          'addon' => [
                                                              'prepend' => [
                                                                  'content' => '<i class="fa fa-pencil"></i>'

                                                              ]
                                                          ]
                                                      ])->textInput(['maxlength' => true]) ?>
                      </div>
                      <div class="col-sm-6">
                          <?= $form->field($modelCargaFamiliar, "[{$i}]segundo_nombre",[
                                                        'addon' => [
                                                            'prepend' => [
                                                                'content' => '<i class="fa fa-pencil"></i>'

                                                            ]
                                                        ]
                                                    ])->textInput(['maxlength' => true]) ?>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                          <?= $form->field($modelCargaFamiliar, "[{$i}]apellido",[
                                                        'addon' => [
                                                            'prepend' => [
                                                                'content' => '<i class="fa fa-pencil"></i>'

                                                            ]
                                                        ]
                                                    ])->textInput(['maxlength' => true]) ?>
                      </div>
                      <div class="col-sm-6">
                          <?= $form->field($modelCargaFamiliar, "[{$i}]segundo_apellido",[
                                                        'addon' => [
                                                            'prepend' => [
                                                                'content' => '<i class="fa fa-pencil"></i>'

                                                            ]
                                                        ]
                                                    ])->textInput(['maxlength' => true]) ?>
                      </div>
                  </div><!-- .row -->
                  <div class="row">
        					  <div class="col-sm-6">
							     <?php

                                    echo $form->field($modelCargaFamiliar, "[{$i}]fecha_nacimiento")->widget(DatePicker::classname(), [
                                    'options' => ['placeholder' => 'Please enter employment start date'],
                                    'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'dd/mm/yyyy'
                                    ]
                                    ]);
								 ?>


							</div>
        			<div class="col-sm-6">
                      <?php //$model->tipo = 0;
                       $list = ['F' => 'Femenino', 'M' => 'Masculino'];
                       /* Display an inline checkbox list */
                       echo $form->field($modelCargaFamiliar, "[{$i}]sexo")->radioList($list, ['inline'=>true]);
                       ?>
        			</div>
                  </div>

                 </div>
             </div>
           <?php endforeach; ?>
       </div><!--container-items-->
       </div><!--panel body-->
 </div><!-- .panel -->
 <div class="form-group">

        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
 </div>
 <?php Pjax::begin(['id' => 'pjax-asegurados']);?><div></div><?php Pjax::end(); ?>
 <?php Pjax::begin(['id' => 'pajax-vendedor']);?><div></div><?php Pjax::end(); ?>
 <?php DynamicFormWidget::end(); ?>
 <?php ActiveForm::end(); ?>

</div>

<?php

$string = <<<EXP2

// obtener la id del formulario y establecer el manejador de eventos
  //$('#row-juridico').removeClass("ocultar");

$(document).on("change", "input:radio[name='Persona[tipo]']", (function(e) {
	//<label class="control-label" for="persona-nombre">Nombre</label>
   if($("input:radio[name='Persona[tipo]']:checked").val()=='J'){
	    $("label[for='persona-nombre']").text('Nombre de la Empresa');
		$("label[for='persona-fecha_nacimiento']").text('Fecha registro de la empresa');
	    $('#row-juridico').removeClass("ocultar");
		$('#snombre,#apellidos,#otros').addClass("ocultar");
   }else{
	    $("label[for='persona-nombre']").text('Nombre');
		$("label[for='persona-fecha_nacimiento']").text('Fecha de Nacimiento');
	   	$('#row-juridico').addClass("ocultar");
		$('#snombre,#apellidos,#otros').removeClass("ocultar");
   }

}));

$("form#dynamic-form").on("beforeSubmit", function(e) {
    var form = $(this);
    var params="";
    var out='';
      $("#documentos-doc_files").fileinput("upload");

    if(form.attr("action")=="/sapis4u/admin/persona/create"){
          params="?submit=true";

    }else{
        params="&submit=true";
    }

    $.post(
        form.attr("action")+params,form.serialize()
    )
    .done(function(result) {

		  if(result.message=='Exito'){

    		$('#modal').modal('hide');
    		
    		console.log(result.tipo_persona);
    		console.log(result.tipo_poliza);

          if(result.tipo_persona=='Contratante'){
          
             $.pjax.reload({container:"#pjax-persona",async:false});
            
          
             if(result.tipo_poliza=='salud'){
                 
                 $.pjax.reload({container:"#pjax-asegurados",async:false}); 
                 $('#m_cliente,#m_carga_familiar,.or').hide();
             }else if(result.tipo_poliza=='auto'){
                 $.pjax.reload({container:"#pjax-asegurados",async:false}); 
             }else{
                 $.pjax.reload({container:"#pjax-asegurado",async:false});
             }
        
              

              $("#datoscontratante").empty();
                  out+='<div class="datagrid"><table class="table table-condensed"><thead><tr><th>Nombre Completo</th><th>Identificacion</th></tr></thead>';
                  out+='<tbody><tr><td>'+result.nombre_completo+'</td><td>'+result.identificacion+'</td></tr></tbody></table></div>';
              $("#datoscontratante").html(out);
              $("#poliza-id_contratante_hidden").val(result.id_cliente);

          }else if (result.tipo_persona=='Otro') {

               	$.pjax.reload({container:"#pjax-asegurados",async:false});//uncaught exception: no pjax container for #pjax-asegurados
  
               out+='<tr id="'+result.identificacion+'"><td><input type="hidden" name="asegurado['+result.id_cliente+']" id="asegurado" value="'+result.id_cliente+'">'+result.identificacion+'</td><td>'+result.nombre_completo+'</td><td>'+result.anios+'</td><td>Cliente</td><td><a id="borrar-asegurados"><i class="fa fa-trash fa-lg"></i></a></td></tr>';
               $("#table-asegurados").children("tbody").append(out);

               if($("input[name*='asegurado']").countElements()==1){
                 $('#m_cliente,#m_carga_familiar,.or').show();
                 $('#m_titular').hide();
               }

          }else if (result.tipo_persona=='Titular') {
          
               $.pjax.reload({container:"#pjax-persona",async:false});
          
               if(result.tipo_poliza=='salud'){
                 
                 $.pjax.reload({container:"#pjax-asegurados",async:false}); 
                 $('#m_cliente,#m_carga_familiar,.or').hide();
               }else{
                  $.pjax.reload({container:"#pjax-asegurado",async:false});
               }

                out+='<tr id="'+result.identificacion+'"><td><input type="hidden" name="asegurado['+result.id_cliente+']" id="asegurado" value="'+result.id_cliente+'">'+result.identificacion+'</td><td>'+result.nombre_completo+'</td><td>'+result.anios+'</td><td>'+result.tipo_persona+'</td><td><a id="borrar-asegurados-t"><i class="fa fa-trash fa-lg"></i></a></td></tr>';
                $("#table-asegurados").children("tbody").append(out);

                if($("input[name*='asegurado']").countElements()==1){
                  $('#m_cliente,#m_carga_familiar,.or').show();
                  $('#m_titular').hide();
                }

          }else if(result.tipo_persona=='Asegurado'){

            $.pjax.reload({container:"#pjax-persona",async:false});
     
            if(result.tipo_poliza=='salud'){
                 
                 $.pjax.reload({container:"#pjax-asegurados",async:false}); 
                 $('#m_cliente,#m_carga_familiar,.or').hide();
            }else if(result.tipo_poliza=='auto'){
                 $.pjax.reload({container:"#pjax-asegurados",async:false}); 
            }else{
                 $.pjax.reload({container:"#pjax-asegurado",async:false});
            }
            

              $("#datosasegurado").empty();
                      out+='<div class="datagrid"><table class="table table-condensed"><thead><tr><th>Nombre Completo</th><th>Identificacion</th></tr></thead>';
                      out+='<tbody><tr><td>'+result.nombre_completo+'</td><td>'+result.identificacion+'</td></tr></tbody></table></div>';
              $("#datosasegurado").html(out);
              $("#poliza-id_asegurado_hidden").val(result.id_cliente);

          }else if(result.tipo_persona=='cliente'){
               	$.pjax.reload({container:"#pjax-persona"});
          }else{
                $.pjax.reload({container:"#pjax-vendedor",async:false});
          }

			}//fin exito

        });
        return false;
  }).on("submit", function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        return false;
  });




EXP2;

$this->registerJs($string, \yii\web\View::POS_READY);

?>

<?php
$script=<<< JS

$(".dynamicform_wrapper").on('afterInsert', function(e, item){
    var datePickers = $(this).find('[data-krajee-kvdatepicker]');
	   datePickers.each(function(index, el){
  	   $(this).parent().removeData().kvDatepicker('remove');
  	   $(this).parent().kvDatepicker(eval($(this).attr('data-krajee-kvdatepicker')));
	   });
});


$(".dynamicform_wrapper").on("beforeInsert", function(e, item) {
		console.log("beforeInsert");
});


$(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
		if (! confirm("Are you sure you want to delete this item?")) {
			return false;
    }
		return true;
});

$(".dynamicform_wrapper").on("afterDelete", function(e) {
		console.log("Deleted item!");
});

$(".dynamicform_wrapper").on("limitReached", function(e, item) {
		    alert("Limit reached");
});
$('select.select2').select2('destroy');
$('.select-select2').select2('destroy');
$('#selectbox').select2('destroy');


 /*if($("input[name='Persona[tipo]']").val()){

 }*/

JS;
$this->registerJs($script, \yii\web\View::POS_READY);
//https://github.com/wbraganca/yii2-dynamicform/issues/41
$string="
#last{
    overflow:auto;
    display:block;
}
.inlineInput{
    float:left;
}
 .ocultar,.carga-familiar{
	  display:none;
 }
";
$this->registerCss($string);
?>

<?php
$string = <<<EXP2

  (function($) {
  $.fn.serializefiles = function() {
      var obj = $(this);
      /* ADD FILE TO PARAM AJAX */
      var formData = new FormData();
      $.each($(obj).find("#documentos-doc_files"), function(i, tag) {
          $.each($(tag)[0].files, function(i, file) {
              formData.append(tag.name, file);
          });
      });
      var params = $(obj).serializeArray();
      $.each(params, function (i, val) {
          formData.append(val.name, val.value);
      });
      return formData;
  };
  })(jQuery);

  $.fn.modal.Constructor.prototype.enforceFocus = function() {};


EXP2;
$this->registerJs($string);

?>
