<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use kartik\select2\Select2;
use kartik\widgets\DepDrop;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;
use kartik\widgets\DatePicker;

?>

<div class="form-carga-familiar">
  <?php $form = ActiveForm::begin([
    'id' => 'form-carga-familiar',
    //'enableAjaxValidation' => true,
    'enableClientScript' => true,
    'enableClientValidation' => true,
    ]); ?>

	<div class="box box-default">
	 <div class="box-body">

						   <div class="row">
							  <div class="col-sm-6">

							 <?=$form->field($model,"parentesco")->widget(Select2::classname(), [
								  'data' =>['Padre'=>'Padre','Madre'=>'Madre','Hijo(a)'=>'Hijo(a)','Conyuge'=>'Conyuge'],
								  'language' => 'en',
								   'options' => ['placeholder' => 'Seleccione...'],
								   'pluginOptions' => [
									  'allowClear' => true
								  ],
								]);
							  ?>
							  </div>
							  <div class="col-sm-6">
								  <?= $form->field($model, "identificacion")->textInput(['maxlength' => true]) ?>
							 </div>
						  </div>

						  <div class="row">

							  <div class="col-sm-6">
									<?= $form->field($model, "nombre",[
																  'addon' => [
																	  'prepend' => [
																		  'content' => '<i class="fa fa-pencil"></i>'

																	  ]
																  ]
															  ])->textInput(['maxlength' => true]) ?>
							  </div>
							  <div class="col-sm-6">
								  <?= $form->field($model, "segundo_nombre",[
																'addon' => [
																	'prepend' => [
																		'content' => '<i class="fa fa-pencil"></i>'

																	]
																]
															])->textInput(['maxlength' => true]) ?>
							  </div>
							</div>
							<div class="row">
							  <div class="col-sm-6">
								  <?= $form->field($model, "apellido",[
																'addon' => [
																	'prepend' => [
																		'content' => '<i class="fa fa-pencil"></i>'

																	]
																]
															])->textInput(['maxlength' => true]) ?>
							  </div>
							  <div class="col-sm-6">
								  <?= $form->field($model, "segundo_apellido",[
																'addon' => [
																	'prepend' => [
																		'content' => '<i class="fa fa-pencil"></i>'

																	]
																]
															])->textInput(['maxlength' => true]) ?>
							  </div>
						  </div><!-- .row -->
						  <div class="row">
									  <div class="col-sm-6">
										 <?php

											echo $form->field($model,"fecha_nacimiento")->widget(DatePicker::classname(), [
											'options' => ['placeholder' => 'Please enter employment start date'],
											'pluginOptions' => [
											'autoclose'=>true,
											'format' => 'dd/mm/yyyy'
											]
											]);
										 ?>


									</div>
							<div class="col-sm-6">
							  <?php //$model->tipo = 0;
							   $list = ['F' => 'Femenino', 'M' => 'Masculino'];
							   /* Display an inline checkbox list */
							   echo $form->field($model, "sexo")->radioList($list, ['inline'=>true]);
							   ?>
							</div>
						  </div>

	   <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
       </div>
       </div><!--panel body-->
  </div><!-- .panel -->
 <?php ActiveForm::end();?>
</div>

<?php

$string = <<<EXP2

    $("form#form-carga-familiar").on("beforeSubmit", function(e) {
        var form = $(this);
        var params="";
		    var out='';

        if(form.attr("action")=="/yii_t_final/admin/persona/create"){
              params="?submit=true";

        }else{
            params="&submit=true";
        }
        $.post(
            form.attr("action")+params,form.serialize()
        )
        .done(function(result) {

      			if(result.message=='Exito'){
      			  	$('#modal').modal('hide');
                  $.pjax.reload({container:"#pjax-asegurados",async:false});//uncaught exception: no pjax container for #pjax-asegurados
                  $.pjax.reload({container:"#pjax-beneficiario",async:false});//uncaught exception: no pjax container for #pjax-beneficiario
                  
      		   //	$("#table-asegurados").empty();
      				 out+='<tr id="'+result.identificacion+'"><td><input type="hidden" name="asegurado['+result.id_cliente+']" id="asegurado" value="'+result.id_cliente+'">'+result.identificacion+'</td><td>'+result.nombre_completo+'</td><td>'+result.anios+'</td><td>'+result.parentesco+'</td><td><a id="borrar-asegurados"><i class="fa fa-trash fa-lg"></i></a></td></tr>';
      			   $("#table-asegurados").children("tbody").append(out);

				    $('#m_carga_familiar').show();
                    $('#m_titular,#m_cliente,.or').hide();


      			}

        });
        return false;
    }).on("submit", function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        return false;
    });


EXP2;

$this->registerJs($string, \yii\web\View::POS_READY);
?>
<?php
$string="";
$this->registerCss($string);
?>
