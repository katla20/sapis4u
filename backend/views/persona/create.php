<?php

use yii\helpers\Html;
use kartik\icons\Icon;
use yii\helpers\Url;

?>

<div class="persona-create">
<?php
     if(!empty($depend)){?>


			<div class="bg-aqua top-modulo">
			  <span class="icon-modulo"><?=Icon::show('user', ['class' => 'fa-3x'])?></span>
			  <span style="color:white;font-size:18px;font-weight:bold;"><?=Yii::t('app', 'Agregar Carga Familiar');?></span>
			</div>
			<br/>

<?php  echo $this->render('_cargaFamiliar', [
				'model' =>$model,
				'depend'=>$depend,
          ]);

    }else{ ?>
	<div class="bg-aqua top-modulo">
			  <span class="icon-modulo"><?=Icon::show('user', ['class' => 'fa-3x'])?></span>
			  <span style="color:white;font-size:18px;font-weight:bold;"><?=Yii::t('app', 'Crear Cliente');?></span>
			</div>
			<br/>

<?php			echo $this->render('_form', [
        				'model' => $model,
        				'modelDireccion' => (empty($modelDireccion)) ? [new Direccion] : $modelDireccion,
        				'modelCliente' => (empty($modelCliente)) ? [new Cliente] : $modelCliente,
        				'modelCargaFamiliar' => (empty($modelCargaFamiliar)) ? [new CargaFamiliar] : $modelCargaFamiliar,
                'documentos'=>$documentos,
             ]);

	}
?>
</div>
