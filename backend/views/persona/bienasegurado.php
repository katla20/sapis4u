<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use frontend\models\Pais;
use frontend\models\Estado;
use frontend\models\Ciudad;
use frontend\models\Zona;
use kartik\select2\Select2;
use kartik\widgets\DepDrop;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;
use kartik\widgets\DatePicker;
use kartik\icons\Icon;
use yii\web\JsExpression;

?>
<div class="bienasegurado">
<div class="bg-aqua top-modulo">
    <span class="icon-modulo"><?=Icon::show('home', ['class' => 'fa-3x'])?></span>
    <span style="color:white;font-size:18px;font-weight:bold;"><?=Yii::t('app', 'Agregar Bien Asegurado');?></span>
</div>
<br/>
<div class="form-bienasegurado">
  <?php $form = ActiveForm::begin([
    'id' => 'form-bienasegurado',
    //'enableAjaxValidation' => true,
    'enableClientScript' => true,
    'enableClientValidation' => true,
    ]); ?>

	<div class="box box-default">
	 <div class="box-body">
          <div class="row">
              <div class="col-sm-6">

                     <?=$form->field($modelDireccion,"nombre_direccion")->widget(Select2::classname(), [
                          'data' =>['Direccion Habitacion'=>'Habitacion','Direccion Trabajo'=>'Trabajo'],
                          'language' => 'en',
                           'options' => ['placeholder' => 'Seleccione tipo de direccion'],
                          'pluginOptions' => [
                              'allowClear' => true
                          ],
                        ]);
                      ?>
              </div>
              <div class="col-sm-6">
                           <?php
               echo $form->field($modelDireccion,"id_estado",[
                                             'addon' => [
                                                 'prepend' => [
                                                     'content' => '<i class="fa fa-map-marker"></i>'

                                                 ]
                                             ]
                                         ])->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(Estado::find()->where(['id_pais' => '10'])->all(), 'id_estado', 'nombre'),
                                'language' => 'en',
                                 'options' => ['placeholder' => 'Select a state ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                              ]);

                            ?>
              </div>
          </div>
          <div class="row">
           <div class="col-sm-6">
            <?=$form->field($modelDireccion, "id_ciudad",[
                                          'addon' => [
                                              'prepend' => [
                                                  'content' => '<i class="fa fa-map-marker"></i>'

                                              ]
                                          ]
                                      ])->widget(DepDrop::classname(), [
                'data'=> [''=>'Seleccione '],
                'options'=>['placeholder'=>'Selecione ...'],
                'type' => DepDrop::TYPE_SELECT2,
                'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                'pluginOptions'=>[
                 'depends'=>["direccion-id_estado"],
                 'placeholder' => 'Seleccione ...',
                 'url'=>Url::to(['dependent-dropdown/child-account', 'db'=>Ciudad::classname(),'cmpo_dep'=>'id_estado','cmpo_mostrar'=>'nombre','id_cmpo'=>'id_ciudad']),
                 'loadingText' => 'Leyendo ...',
                 ]
                 ]);

             ?>
           </div>
           <div class="col-sm-6">

            <?=$form->field($modelDireccion, "id_zona",[
                                          'addon' => [
                                              'prepend' => [
                                                  'content' => '<i class="fa fa-map-marker"></i>'

                                              ]
                                          ]
                                    ])->widget(DepDrop::classname(), [
                'data'=> [''=>'Seleccione '],
                'options'=>['placeholder'=>'Selecione ...'],
                'type' => DepDrop::TYPE_SELECT2,
                'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                'pluginOptions'=>[
                 'initialize'=>true,
                 'depends'=>["direccion-id_ciudad"],
                 'placeholder' => 'Seleccione ...',
                 'url'=>Url::to(['dependent-dropdown/child-account', 'db'=>Zona::classname(),'cmpo_dep'=>'id_ciudad','cmpo_mostrar'=>'nombre','id_cmpo'=>'id_zona']),
                 'loadingText' => 'Leyendo ...',
                 'params'=>['model_id1'] ///SPECIFYING THE PARAM
                 ]
                 ]);
             ?>
           </div>
        </div>
        <div class="row">
           <div class="col-sm-6">
                             <?= $form->field($modelDireccion, "sector",[
                                                           'addon' => [
                                                               'prepend' => [
                                                                   'content' => '<i class="fa fa-map-marker"></i>'

                                                               ]
                                                           ]
                                                       ])->textInput(['maxlength' => true]) ?>
                         </div>
            <div class="col-sm-6">
              <?= $form->field($modelDireccion, "calle_avenida",[
                              'addon' => [
                                'prepend' => [
                                  'content' => '<i class="fa fa-map-marker"></i>'

                                ]
                              ]
                            ])->textInput(['maxlength' => true]) ?>
            </div>
        </div>
       <div class="row">
            <div class="col-sm-6">
                             <?= $form->field($modelDireccion, "edif_bloque",[
                                                           'addon' => [
                                                               'prepend' => [
                                                                   'content' => '<i class="fa fa-map-marker"></i>'

                                                               ]
                                                           ]
                                                       ])->textInput(['maxlength' => true]) ?>
            </div>
             <div class="col-sm-6">
                 <?= $form->field($modelDireccion, "nro",[
                                               'addon' => [
                                                   'prepend' => [
                                                       'content' => '<i class="fa fa-map-marker"></i>'

                                                   ]
                                               ]
                                           ])->textInput(['maxlength' => true]) ?>
             </div>
        </div><!-- .row -->
         <div class="row">
             <div class="col-sm-12">
                 <?= $form->field($modelDireccion, "direccion",[
                                               'addon' => [
                                                   'prepend' => [
                                                       'content' => '<i class="fa fa-map-marker"></i>'

                                                   ]
                                               ]
                                           ])->textArea(['rows' => '3']) ?>
             </div>
         </div><!-- .row -->
         <div class="row">
           <div class="col-sm-4">
               <?= $form->field($modelDireccion, "telefono",[
                                             'addon' => [
                                                 'prepend' => [
                                                     'content' => '<i class="fa fa-phone"></i>'

                                                 ]
                                             ]
                                         ])->widget(\yii\widgets\MaskedInput::classname(), [
                                                  'mask' => '999-999-9999',
                                                  ])?>

              </div>
               <div class="col-sm-4">
                   <?= $form->field($modelDireccion, "telefono2",[
                                                 'addon' => [
                                                     'prepend' => [
                                                         'content' => '<i class="fa fa-phone"></i>'

                                                     ]
                                                 ]
                                             ])->widget(\yii\widgets\MaskedInput::classname(), [
                                                      'mask' => '999-999-9999',
                                                      ])?>

               </div>
               <div class="col-sm-4">
                   <?= $form->field($modelDireccion, "telefono3",[
                                                 'addon' => [
                                                     'prepend' => [
                                                         'content' => '<i class="fa fa-phone"></i>'

                                                     ]
                                                 ]
                                             ])->widget(\yii\widgets\MaskedInput::classname(), [
                                                      'mask' => '999-999-9999',
                                                      ])?>

               </div>
         </div><!-- .row -->

	      <div class="form-group">
        <?= Html::submitButton($modelDireccion->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $modelDireccion->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
       </div>
       </div><!--panel body-->
  </div><!-- .panel -->
 <?php ActiveForm::end();?>
</div>
</div>

<?php

$string = <<<EXP2

    $("form#form-bienasegurado").on("beforeSubmit", function(e) {
        var form = $(this);
        var params="";
		    var out='';
        if(form.attr("action")=="/sapis4/admin/persona/bien"){
              params="?submit=true";

        }else{
            params="&submit=true";
        }
        $.post(
            form.attr("action")+params,form.serialize()
        )
        .done(function(result) {

      			if(result.message=='Exito'){
      			  	$('#modal').modal('hide');
      				 out+='<tr id="'+result.id_direccion+'"><td><input type="hidden" name="bienasegurado['+result.id_direccion+']" id="bienasegurado" value="'+result.id_direccion+'">'+result.estado+'</td><td>'+result.ciudad+'</td><td>'+result.zona+'</td><td>'+result.direccion+'</td><td>'+result.telefono+'</td><td><a id="borrar-bienasegurados"><i class="fa fa-trash fa-lg"></i></a></td></tr>';
      			   $("#table-bienesasegurados").children("tbody").append(out);

      			}

        });
        return false;
    }).on("submit", function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        return false;
    });


EXP2;

$this->registerJs($string, \yii\web\View::POS_READY);
?>
<?php
$string="";
$this->registerCss($string);
?>
