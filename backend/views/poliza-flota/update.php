<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Poliza */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Poliza',
]) . ' ' . $model->id_poliza;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Polizas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_poliza, 'url' => ['view', 'id' => $model->id_poliza]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="poliza-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,'documentos' => $documentos,'recibo' => $recibo
    ]) ?>

</div>
