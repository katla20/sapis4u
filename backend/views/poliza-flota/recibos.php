<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\Poliza;
use backend\models\search\PolizaSearch;
use backend\models\Aseguradoras;
use kartik\icons\Icon;
use kartik\select2\Select2;


        $searchModel = new PolizaSearch();
        $dataProvider = $searchModel->search3(Yii::$app->request->queryParams,$poliza);//ramo=automovil, tipo=individual
		//print_r($dataProvider); exit;
		
?>		
<div class="poliza-recibos">

    <?php
  	$gridColumns = [
  	//	['class' => 'yii\grid\SerialColumn'],
    		[
    			'attribute'=>'nro_recibo',
    			'value'=>'nro_recibo',
    			'width'=>'180px',
            ],
			['class' => 'kartik\grid\DataColumn',
			   'attribute'=>'fecha_vigenciadesde',
			   'width'=>'200px',
				'value'=>function ($model) {
					 return date("d-m-Y", strtotime($model->fecha_vigenciadesde));
				},
			   'filterType'=>GridView::FILTER_DATE,
			   'filterWidgetOptions' => [
				  'pluginOptions'=>[
				  'format' => 'dd-mm-yyyy',
				  'autoWidget' => true,
				  'autoclose' => true,
				  //'todayBtn' => true,
				  ],

				],
		   ],
		  ['class' => 'kartik\grid\DataColumn',
		   'attribute'=>'fecha_vigenciahasta',
		   'width'=>'200px',
		   'value'=>function ($model) {
					 return date("d-m-Y", strtotime($model->fecha_vigenciahasta));
			},
		   'filterType'=>GridView::FILTER_DATE,
		   'filterWidgetOptions' => [
			  'pluginOptions'=>[
			  'format' => 'dd-mm-yyyy',
			  'autoWidget' => true,
			  'autoclose' => true,
			  //'todayBtn' => true,
			  ],

			],
		  ],
	  [
  		   'attribute'=>'tipo_recibo',
  		   'width'=>'50px',
  		   'value'=>'tipo_recibo'
      ],
	  
	  [
  		   'attribute'=>'prima',
  		   'width'=>'50px',
  		   'value'=>'prima'
      ],
	  [ 
  		'attribute'=>'estatus_recibo',
  		'vAlign'=>'middle',
  		'width'=>'50px',
  		'value'=>function ($model, $key, $index, $widget) {
    					if($model->estatus_recibo=='Pendiente'){
  							return "<span class='label label-success'>".$model->estatus_recibo .'</span>';

    					}else if($model->estatus_recibo=='Anulado'){
    					   return "<span class='label label-danger'>".$model->estatus_recibo .'</span>';

    					}else if($model->estatus_recibo=='Cobrado'){
    					   return "<span class='label label-warning'>".$model->estatus_recibo .'</span>';
    					}
    			},
    	'format'=>'raw',
    	'noWrap'=>false,
  	  ],

      [
       'class' => 'yii\grid\ActionColumn',
	   'contentOptions' => ['style' => 'width:150px;'],
       'header'=>'GESTION DE RECIBOS',
       'template' => '{anulacion}',
             'buttons' => [
			   
               'anulacion' => function ($url,$model, $key) {
                         return Html::a('<i class="fa fa-trash fa-lg" aria-hidden="true"></i>', ['anulacion-recibo','id' => $model->id_recibo], [
                             'id' => 'activity-index-link-anular',
                             'title' => Yii::t('app','Anulacion de Recibo'),'rel'=>'tooltip',
                             'data-pjax' => '0',
                         ]);
                 },
             ]
      ],

  	];

  	echo GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
  		'columns' => $gridColumns,
		//'showPageSummary' => true,
  		//'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
        'responsive'=>true,
        'hover'=>true
  		/*'pjax' => false,
  		'pjaxSettings' => [
  			'options' => ['id' => 'poliza-pjax-id'],// UNIQUE PJAX CONTAINER ID
  		],
  		'bordered' => false,
  		'resizableColumns'=>true,
  		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
  		'striped' => true,
  		'condensed' => false,
  		'responsive' => true,
  		'hover' => true,
    	'panel' => [
          'heading'=>'<h2 class="panel-title" style="font-weight:bold;"> &nbsp; &nbsp;RECIBOS </h2>',
    			'type' => GridView::TYPE_INFO
    		],*/
      ]);
  	?>

</div>
<?php
$string='.nav-bar_opcion {
  position: fixed;
  top: 200;
  left: 0;
  right: 10;
  z-index: 9999;
  width: 50%;
  height: 50px;
}';
$this->registerCss($string);
?>

