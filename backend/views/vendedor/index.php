<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\PersonaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider*/

$this->title ="";
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
      <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
</div>
</br></br>
<div class="vendedor-index">

  <?  $gridColumns = [
      ['class' => 'yii\grid\SerialColumn'],
      'identificacion',
      'nombre',
      'apellido',
      'fecha_nacimiento',
      [
        'class'=>'kartik\grid\BooleanColumn',
        'attribute'=>'estatus',
        'vAlign'=>'middle',
      ],
      [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{view} {update}',
        'buttons' => [
            'update' => function ($url, $model, $key) {
                return Html::a('<i class="fa fa-pencil fa-lg" aria-hidden="true"></i>', '#', [
                    'id' => 'activity-index-link',
                    'title' => Yii::t('app', 'Actualizar Vendedor'),'rel'=>'tooltip',
                    'data-toggle' => 'modal',
                    'data-target' => '#modal',
                    'data-url' => Url::to(['update', 'id' => $model->id_persona]),
                    'data-pjax' => '0',
                ]);
            },
			'view' => function ($url,$model, $key) {
                  return Html::a('<i class="fa fa-binoculars fa-lg" aria-hidden="true"></i>', ['view','id' => $model->id_persona], [
                      'id' => 'activity-index-link',
                      'title' => Yii::t('app', 'Ver Vendedor'),'rel'=>'tooltip',
                      'data-pjax' => '0',
                  ]);
            },
        ]
      ],
      //['class' => 'kartik\grid\CheckboxColumn']
    ];

    echo GridView::widget([
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
          'columns' => $gridColumns,
          'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
          'toolbar' => [
            [
              'content'=>
                    Html::a('<i class="glyphicon glyphicon-plus"></i> Registrar', '#', [
                  'id' => 'activity-index-link',
                  'title'=>Yii::t('app', 'Registrar Vendedor'),'rel'=>'tooltip',
                  'class' => 'btn btn-success',
                  'data-toggle' => 'modal',
                  'data-target' => '#modal',
                  'data-url' => Url::to(['create']),
                  'data-pjax' => '0',
                  ]). ' '.
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
                  'class' => 'btn btn-default',
                  'title' => Yii::t('app', 'Actualizar Grid'),'rel'=>'tooltip',
                ]),

            ],
            '{export}',
            '{toggleData}',
            'exportConfig' => [
              GridView::EXCEL => [],
              GridView::TEXT => [],
              GridView::PDF => [],
                 ],
           ],
          'pjax' => true,
          'pjaxSettings' => [
            'options' => ['id' => 'pjax-vendedor'],// UNIQUE PJAX CONTAINER ID
          ],
          'bordered' => false,
          'resizableColumns'=>true,
          'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
          'striped' => true,
          'condensed' => false,
          'responsive' => true,
          'hover' => true,
          'showPageSummary' => false,
            'panel' => [
              'heading'=>'<h2 class="panel-title">'.Icon::show('user', ['class' => 'fa-2x']).'&nbsp; &nbsp;VENDEDORES</h2>',
              'type' => GridView::TYPE_INFO
            ],
          ]);
    ?>
</div><?php
$string = <<<EXP2
$("[rel=tooltip]").tooltip({ placement: 'top'});
EXP2;
$this->registerJs($string, \yii\web\View::POS_READY);
?>

<?php
$this->registerJs(
"$(document).on('click', '#activity-index-link', (function() {
    $.get(
        $(this).data('url'),
        function (data) {
            $('.modal-body').html(data);
            $('#modal').modal();
        }
    );
}));"
); ?>

<?php
Modal::begin([
 'options' => [
        'id' => 'modal',
        'tabindex' => false // important for Select2 to work properly
  ],
]);
Modal::end();
?>
