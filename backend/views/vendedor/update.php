<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $model frontend\models\persona */

$this->title ='';
?>
<div class="bg-aqua top-modulo">
          <span class="icon-modulo"><?=Icon::show('user', ['class' => 'fa-3x'])?></span>
          <span style="color:white;font-size:18px;font-weight:bold;"><?=Yii::t('app', 'Actualizar Vendedor');?></span>
</div><br/>
<div class="vendedor-update">

    <?= $this->render('_form', [
         'model' => $model,
         'modelDireccion' => (empty($modelDireccion)) ? [new Direccion] : $modelDireccion,
         'modelIntermediario' => (empty($modelIntermediario)) ? [new Intermediario] : $modelIntermediario,

    ]) ?>

</div>
