<?php

use yii\helpers\Html;
use kartik\icons\Icon;
use yii\helpers\Url;

?>
<div class="bg-aqua top-modulo">
          <span class="icon-modulo"><?=Icon::show('user', ['class' => 'fa-3x'])?></span>
          <span style="color:white;font-size:18px;font-weight:bold;"><?=Yii::t('app', 'Crear Vendedor');?></span>
</div><br/>
<div class="vendedor-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelDireccion' => (empty($modelDireccion)) ? [new Direccion] : $modelDireccion,
        'modelIntermediario' => (empty($modelIntermediario)) ? [new Intermediario] : $modelIntermediario,
    ]) ?>

</div>
