<?php

use yii\helpers\Html;
//use yii\bootstrap\ActiveForm;
//use yii\bootstrap\ActiveField;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use frontend\models\Pais;
use frontend\models\Estado;
use frontend\models\Ciudad;
use frontend\models\Zona;
use kartik\select2\Select2;
use kartik\widgets\DepDrop;
//use kartik\depdrop\Depdrop; //http://demos.krajee.com/widget-details/depdrop
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;
use kartik\widgets\DatePicker;

?>

<div class="persona-form">
  <?php $form = ActiveForm::begin([
    'id' => 'dynamic-form',
    'enableAjaxValidation' => true,
    'enableClientScript' => true,
    'enableClientValidation' => true,
    ]); ?>
  <div class="box box-default">
  <div class="box-header">
    <h4 class="box-title"><i class="glyphicon glyphicon-user"></i> Datos Personales</h4>
      <div class="row">
        <div class="col-md-4">

        <?php  $list = ['V'=>'Natural','J'=>'Juridico'];
          /* Display an inline checkbox list */
		  $model->tipo='V';
          echo $form->field($model, "tipo")->radioList($list, ['inline'=>true]);
         ?>
        </div>
		<div class="col-md-6">
			<div class="input-group">

				<?//=$form->field($model, 'preidentificacion')->dropDownList(['J' => 'J', 'V' => 'V','E' => 'E', 'G' => 'G']);?>
				<?=$form->field($model, 'identificacion',[
											'addon' => [
											            'prepend' => [
															  'content'=>'<select id="persona-preidentificacion" name="Persona[preidentificacion]">
															                      <option value="V">V</option>
																				  <option value="E">E</option>
																				  <option value="J">J</option>
																				  <option value="G">G</option>
																		  </select>'
														],
												        'append' => ['content' => '--'],
														'groupOptions' => ['class'=>'input-group-md'],
														'contentAfter' => '<input type="text" id="persona-postidentificacion" name="Persona[postidentificacion]" class="form-control" placeholder="ult rif" maxlength="1">'
											],
											'inputOptions' => [
                                              'placeholder' => 'Cedula o Rif'
											]
                                          ])->textInput(['maxlength' => true])?>
			</div>
         </div>
     </div>
    <div class="box-tools pull-right">
      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div><!-- /.box-tools -->
  </div><!-- /.box-header -->
  <div class="box-body">
	 <div class="container-items"><!-- widgetBody -->

     <div class="row">
			  <div class="col-sm-6" >
			  <?= $form->field($model, 'nombre',[
								  'addon' => [
									  'prepend' => [
										  'content' => '<i class="fa fa-pencil"></i>'

									  ]
								  ]
							  ])->textInput(['maxlength' => true]) ?>
			  </div>
			  <div class="col-sm-6" id="snombre">
				  <?= $form->field($model, 'segundo_nombre',[
									'addon' => [
										'prepend' => [
											'content' => '<i class="fa fa-pencil"></i>'

										]
									]
								])->textInput(['maxlength' => true]) ?>
			  </div>
        </div>
		<div class="row ocultar" id="row-juridico">
			<div class="col-sm-3">
					<?= $form->field($model, 'nit',[
												  'addon' => [
													  'prepend' => [
														  'content' => '<i class="fa fa-pencil"></i>'

													  ]
												  ]
											  ])->textInput(['maxlength' => true,'class' => 'ocultar']) ?>
			</div>
			<div class="col-sm-4">
						<?= $form->field($model, 'numero_registro',[
													  'addon' => [
														  'prepend' => [
															  'content' => '<i class="fa fa-pencil"></i>'

														  ]
													  ]
												  ])->textInput(['maxlength' => true,'class' => 'ocultar']) ?>

			</div>
			<div class="col-sm-10">
						<?= $form->field($model, 'actividad_economica',[
													  'addon' => [
														  'prepend' => [
															  'content' => '<i class="fa fa-pencil"></i>'

														  ]
													  ]
												  ])->textInput(['maxlength' => true,'class' => 'ocultar']) ?>

			</div>
			<div class="col-sm-10">
						<?= $form->field($model, 'representante',[
													  'addon' => [
														  'prepend' => [
															  'content' => '<i class="fa fa-pencil"></i>'

														  ]
													  ]
												  ])->textInput(['maxlength' => true]) ?>

			</div>
     </div>
        <div class="row" id="apellidos" >
					  <div class="col-sm-6">
						  <?= $form->field($model, 'apellido',[
                                            'addon' => [
                                                'prepend' => [
                                                    'content' => '<i class="fa fa-pencil"></i>'

                                                ]
                                            ]
                                        ])->textInput(['maxlength' => true]) ?>
					  </div>
					  <div class="col-sm-6">
						  <?= $form->field($model, 'segundo_apellido',[
                                            'addon' => [
                                                'prepend' => [
                                                    'content' => '<i class="fa fa-pencil"></i>'

                                                ]
                                            ]
                                        ])->textInput(['maxlength' => true]) ?>
					  </div>
        </div>
          <div class="row" id="otros">
			<div class="col-sm-6">
              <?php
               $list = ['F' => 'Femenino', 'M' => 'Masculino'];
               /* Display an inline checkbox list */
               echo $form->field($model, "sexo")->radioList($list, ['inline'=>true]);
               ?>
					  </div>
          </div>
          <div class="row">
				  <div class="col-sm-4">
					   <?=$form->field($model, 'fecha_nacimiento',[
													 'addon' => [
														 'prepend' => [
															 'content' => '<i class="glyphicon glyphicon-calendar"></i>'

														 ]
													 ]
												 ])->widget(\yii\widgets\MaskedInput::className(),
																		  [
																		  'clientOptions' => ['alias' =>  'date']
																		  ])?>
					</div>
					  <div class="col-sm-8">
							<?= $form->field($model, 'correo',[
                                            'addon' => [
                                                'prepend' => [
                                                    'content' => '<i class="glyphicon glyphicon-envelope"></i>'

                                                ]
                                            ]
                                        ])->textInput(['maxlength' => true]) ?>
					   </div>
          </div><!--container-items-->
		  <div class="row">
			  <div class="col-sm-4">
					<?= $form->field($model, 'codigo')->textInput(['maxlength' => true]) ?>
			   </div>
				<div class="col-sm-4">
					<?= $form->field($model, 'comision')->textInput(['maxlength' => true]) ?>
			   </div>
          </div>

  </div><!-- /.box-body -->
</div><!-- /.box -->

    <?php DynamicFormWidget::begin([
       'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
       'widgetBody' => '.container-items', // required: css class selector
       'widgetItem' => '.item', // required: css class
       'limit' => 4, // the maximum times, an element can be added (default 999)
       'min' => 0, // 0 or 1 (default 1)
       'insertButton' => '.add-item', // css class
       'deleteButton' => '.remove-item', // css class
       'model' => $modelDireccion[0],
       'formId' => 'dynamic-form',
       'formFields' => [
           'nombre_direccion',
           'id_zona',
           'calle_avenida',
           'nro',
           'sector',
           'direccion',
           'telefono',
           'telefono2',
           'telefono3'
       ],
     ]);?>

         <div class="box box-default">
         <div class="box-header">
             <h4>
                 <i class="glyphicon glyphicon-envelope"></i> Direcciones
                 <button type="button" class="add-item btn btn-success btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i>Agregar</button>
             </h4>

         </div>
         <div class="box-body">
             <div class="container-items"><!-- widgetBody -->
             <?php foreach ($modelDireccion as $i => $modelDireccion): ?>
                 <div class="item panel panel-default"><!-- widgetItem -->
                     <div class="panel-heading">
                         <h5 class="panel-title pull-left">Direccion</h5>
                         <div class="pull-right">
                             <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                         </div>
                         <div class="clearfix"></div>
                     </div>
                     <div class="panel-body">
                         <?php
                             // necessary for update action.
                             if (! $modelDireccion->isNewRecord) {

                                 echo Html::activeHiddenInput($modelDireccion, "[{$i}]id");

								// echo CHtml::hiddenField('name' , 'value', array('id' => 'hiddenInput'));[$i]
                                 //ADDITIONAL PARAM ID YOU MAY USE TO SELECT A DEFAULT VALUE OF YOUR MODEL IN YOUR DEPDROP WHEN YOU WANT TO UPDATE:
                                  echo Html::hiddenInput('model_idEstado', $modelDireccion->id, ['id'=>'model_idEstado']);
								  // echo Html::hiddenInput('model_idEstado',"[$i]id", ['id'=>'model_idEstado']);
                             }
                         ?>
                      <div class="row">

						  <div class="col-sm-6">

                         <?=$form->field($modelDireccion,"[{$i}]nombre_direccion")->widget(Select2::classname(), [
                              'data' =>['Direccion Habitacion'=>'Habitacion','Direccion Trabajo'=>'Trabajo','Direccion Cobro'=>'Cobro'],
                              'language' => 'en',
                               'options' => ['placeholder' => 'Select ...'],
                              'pluginOptions' => [
                                  'allowClear' => true
                              ],
                            ]);
                          ?>
                        </div>
                      </div>
                        <div class="row">
                             <div class="col-sm-6">
                               <?php
								   echo $form->field($modelDireccion,"[{$i}]id_estado",[
                                                 'addon' => [
                                                     'prepend' => [
                                                         'content' => '<i class="fa fa-map-marker"></i>'

                                                     ]
                                                 ]
                                             ])->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map(Estado::find()->where(['id_pais' => '10'])->all(), 'id_estado', 'nombre'),
                                    'language' => 'en',
                                     'options' => ['placeholder' => 'Select a state ...'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                  ]);

                                ?>
                             </div>
							 <div class="col-sm-6">
								<?=$form->field($modelDireccion, "[{$i}]id_ciudad",[
                                              'addon' => [
                                                  'prepend' => [
                                                      'content' => '<i class="fa fa-map-marker"></i>'

                                                  ]
                                              ]
                                          ])->widget(DepDrop::classname(), [
										'data'=> [''=>'Seleccione '],
										'options'=>['placeholder'=>'Selecione ...'],
										'type' => DepDrop::TYPE_SELECT2,
										'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
										'pluginOptions'=>[
										 'depends'=>["direccion-$i-id_estado"],
										 'placeholder' => 'Seleccione ...',
										 'url'=>Url::to(['dependent-dropdown/child-account', 'db'=>Ciudad::classname(),'cmpo_dep'=>'id_estado','cmpo_mostrar'=>'nombre','id_cmpo'=>'id_ciudad']),
										 'loadingText' => 'Leyendo ...',
									   ]
									   ]);

								 ?>
							 </div>
						</div>
						<div class="row">

							 <div class="col-sm-6">

								<?=$form->field($modelDireccion, "[{$i}]id_zona",[
                                              'addon' => [
                                                  'prepend' => [
                                                      'content' => '<i class="fa fa-map-marker"></i>'

                                                  ]
                                              ]
                                        ])->widget(DepDrop::classname(), [
										'data'=> [''=>'Seleccione '],
										'options'=>['placeholder'=>'Selecione ...'],
										'type' => DepDrop::TYPE_SELECT2,
										'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
										'pluginOptions'=>[
										 'initialize'=>true,
										 'depends'=>["direccion-$i-id_ciudad"],
										 'placeholder' => 'Seleccione ...',
										 'url'=>Url::to(['dependent-dropdown/child-account', 'db'=>Zona::classname(),'cmpo_dep'=>'id_ciudad','cmpo_mostrar'=>'nombre','id_cmpo'=>'id_zona']),
										 'loadingText' => 'Leyendo ...',
										 'params'=>['model_id1'] ///SPECIFYING THE PARAM
									   ]
									   ]);
								 ?>
							 </div>


                          <div class="col-sm-6">
                                <?= $form->field($modelDireccion, "[{$i}]calle_avenida",[
                                                              'addon' => [
                                                                  'prepend' => [
                                                                      'content' => '<i class="fa fa-map-marker"></i>'

                                                                  ]
                                                              ]
                                                          ])->textInput(['maxlength' => true]) ?>
                          </div>
						</div>
                         <div class="row">
                             <div class="col-sm-6">
                                 <?= $form->field($modelDireccion, "[{$i}]nro",[
                                                               'addon' => [
                                                                   'prepend' => [
                                                                       'content' => '<i class="fa fa-map-marker"></i>'

                                                                   ]
                                                               ]
                                                           ])->textInput(['maxlength' => true]) ?>
                             </div>
                             <div class="col-sm-6">
                                 <?= $form->field($modelDireccion, "[{$i}]sector",[
                                                               'addon' => [
                                                                   'prepend' => [
                                                                       'content' => '<i class="fa fa-map-marker"></i>'

                                                                   ]
                                                               ]
                                                           ])->textInput(['maxlength' => true]) ?>
                             </div>
                         </div><!-- .row -->
                         <div class="row">
                             <div class="col-sm-12">
                                 <?= $form->field($modelDireccion, "[{$i}]direccion",[
                                                               'addon' => [
                                                                   'prepend' => [
                                                                       'content' => '<i class="fa fa-map-marker"></i>'

                                                                   ]
                                                               ]
                                                           ])->textArea(['rows' => '3']) ?>
                             </div>
                         </div><!-- .row -->
                         <div class="row">
                             <div class="col-sm-4">
                                   <?= $form->field($modelDireccion, "[{$i}]telefono",[
                                                                 'addon' => [
                                                                     'prepend' => [
                                                                         'content' => '<i class="fa fa-phone"></i>'

                                                                     ]
                                                                 ]
                                                             ])->textInput(['maxlength' => true]) ?>
                             </div>
                             <div class="col-sm-4">
                                 <?= $form->field($modelDireccion, "[{$i}]telefono2",[
                                                               'addon' => [
                                                                   'prepend' => [
                                                                       'content' => '<i class="fa fa-phone"></i>'

                                                                   ]
                                                               ]
                                                           ])->textInput(['maxlength' => true]) ?>
                             </div>
                             <div class="col-sm-4">
                                 <?= $form->field($modelDireccion, "[{$i}]telefono3",[
                                                               'addon' => [
                                                                   'prepend' => [
                                                                       'content' => '<i class="fa fa-phone"></i>'

                                                                   ]
                                                               ]
                                                           ])->widget(\yii\widgets\MaskedInput::classname(), [
                                                                    'mask' => '999-999-9999',
                                                                 	  ])?>

                             </div>
                         </div><!-- .row -->
                     </div>
                 </div>
               <?php endforeach; ?>
           </div><!--container-items-->
           </div><!--panel body-->
     </div><!-- .panel -->
     <?php DynamicFormWidget::end(); ?>
	   <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
       </div>

 </div><!-- .panel -->
 <?php Pjax::begin(['id' => 'pjax-asegurados']);?><div></div><?php Pjax::end(); ?>
 <?php Pjax::begin(['id' => 'pajax-vendedor']);?><div></div><?php Pjax::end(); ?>
 <?php ActiveForm::end(); ?>

</div>

<?php

$string = <<<EXP2

$(document).on("change", "input:radio[name='Persona[tipo]']", (function(e) {
	//<label class="control-label" for="persona-nombre">Nombre</label>
   if($("input:radio[name='Persona[tipo]']:checked").val()=='J'){
	    $("label[for='persona-nombre']").text('Nombre de la Empresa');
		$("label[for='persona-fecha_nacimiento']").text('Fecha registro');
	    $('#row-juridico').removeClass("ocultar");
		$('#snombre,#apellidos,#otros').addClass("ocultar");


		/*$('#persona-preidentificacion').siblings('select').children('option').each(function() {
          if ( $(this).val() === value ) {
            $(this).attr('disabled', true).siblings().removeAttr('disabled');
          }
        });*/


   }else{
	  $("label[for='persona-nombre']").text('Nombre');
		$("label[for='persona-fecha_nacimiento']").text('Fecha de Nacimiento');
	   	$('#row-juridico').addClass("ocultar");
		$('#snombre,#apellidos,#otros').removeClass("ocultar");
   }

}));

$("form#dynamic-form").on("beforeSubmit", function(e) {
    console.log('pasando por aqui');
        var form = $(this);
        var params="";
        var out='';
        if(form.attr("action")=="/sapis4u/admin/vendedor/create"){
              params="?submit=true";

        }else{
            params="&submit=true";
        }

        $.post(
            form.attr("action")+params,form.serialize()
        )
        .done(function(result) {
            //form.parent().html(result.message);

		  if(result.message=='Exito'){
    			$('#modal').modal('hide');
				  $.pjax.reload({container:"#pajax-vendedor"});

			}

        });
        return false;
  }).on("submit", function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        return false;
  });

EXP2;

$this->registerJs($string, \yii\web\View::POS_READY);

?>

<?php
$script=<<< JS

$(".dynamicform_wrapper").on('afterInsert', function(e, item){
    var datePickers = $(this).find('[data-krajee-kvdatepicker]');
	   datePickers.each(function(index, el){
  	   $(this).parent().removeData().kvDatepicker('remove');
  	   $(this).parent().kvDatepicker(eval($(this).attr('data-krajee-kvdatepicker')));
	   });
});


$(".dynamicform_wrapper").on("beforeInsert", function(e, item) {
		console.log("beforeInsert");
});


$(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
		if (! confirm("Are you sure you want to delete this item?")) {
			return false;
    }
		return true;
});

$(".dynamicform_wrapper").on("afterDelete", function(e) {
		console.log("Deleted item!");
});

$(".dynamicform_wrapper").on("limitReached", function(e, item) {
		    alert("Limit reached");
});
$('select.select2').select2('destroy');
$('.select-select2').select2('destroy');
$('#selectbox').select2('destroy');

JS;
$this->registerJs($script, \yii\web\View::POS_READY);
//https://github.com/wbraganca/yii2-dynamicform/issues/41
$string="
#last{
    overflow:auto;
    display:block;
}
.inlineInput{
    float:left;
}
 .ocultar{
	  display:none;
 }
";
$this->registerCss($string);
?>
