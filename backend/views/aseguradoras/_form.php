<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use backend\models\Pais;

/* @var $this yii\web\View */
/* @var $model backend\models\Aseguradoras */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="aseguradoras-form">

    <?php $form = ActiveForm::begin(); ?>


	<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
    <div class="box-header with-border">
         <h4 class="box-title"><!--<i class="glyphicon glyphicon-user"></i>--> Registro de Aseguradora</h4>
    </div>

		<div class="box-body">
			<div class="container-items"><!-- widgetBody -->
        <div class="row">
           <div class="col-sm-5">
                <?=$form->field($model,"id_pais",[
                                              'addon' => [
                                                  'prepend' => [
                                                      'content' => '<i class="fa fa-globe"></i>'

                                                  ]
                                              ]
                                          ])->widget(Select2::classname(), [
                                            'data' =>ArrayHelper::map(Pais::find()->all(),'id_pais', 'nombre'),
                                           'language' => 'en',
                                            'options' => ['placeholder' => 'Seleccione el Pais'],
                                            'pluginOptions' => [
                                               'allowClear' => true
                                           ],
                   ]);
                 ?>
          </div>
       </div>
				<div class="row">
					<div class="col-sm-3">
						<?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
					</div>
					<div class="col-sm-3">

						 <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-6">
					<?= $form->field($model, 'direccion')->textarea(['rows' => 6]) ?>
					</div>

				</div>

				<div class="row">
					<div class="col-sm-2">
						<?php
						//echo $form->field($model, 'estatus')->dropDownList([1 => 'Activo', 0 => 'Inactivo']);
						echo $form->field($model, 'estatus')->widget(Select2::classname(), [
								'data' => array("1"=>"Activo","0"=>"Inactivo"),
								'language' => 'es',
								//'options' => ['placeholder' => 'Seleccione un Esatdo ...'],
								'pluginOptions' => [
								'allowClear' => true
								],
						]);
						?>
					</div>
				</div>




			</div>
		</div>
    </div>

	<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
		<div class="box-header with-border">
			 <h4 class="box-title"><!--<i class="glyphicon glyphicon-user"></i>--> Ramos</h4>
		</div>
		<div class="box-body">
			<div class="container-items"><!-- widgetBody -->
				<div class="row">
							<div class="col-sm-6">
								 <?php
									$opciones = \yii\helpers\ArrayHelper::map($tipoRamos, 'id_ramo', 'nombre');
									echo $form->field($model, 'ramos')->checkboxList($opciones, ['unselect'=>NULL]);
								?>
							</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
					</div>
				</div>
			</div>
		</div>

	</div>


    <?php ActiveForm::end(); ?>

</div>
