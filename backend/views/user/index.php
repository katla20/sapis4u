<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title ='';// Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <?php
        $gridColumns = [
		['class' => 'kartik\grid\SerialColumn'],
		[
			'class' => 'kartik\grid\EditableColumn',
			'attribute' => 'name',
			'pageSummary' => 'Page Total',
			'vAlign'=>'middle',
			'headerOptions'=>['class'=>'kv-sticky-column'],
			'contentOptions'=>['class'=>'kv-sticky-column'],
			'editableOptions'=>['header'=>'Name', 'size'=>'md']
		],
		[
			'attribute'=>'color',
			'value'=>function ($model, $key, $index, $widget) {
				return "<span class='badge' style='background-color: {$model->color}'> </span>  <code>" . 
					$model->color . '</code>';
			},
			'filterType'=>GridView::FILTER_COLOR,
			'vAlign'=>'middle',
			'format'=>'raw',
			'width'=>'150px',
			'noWrap'=>true
		],
		[
			'class'=>'kartik\grid\BooleanColumn',
			'attribute'=>'estatus', 
			'vAlign'=>'middle',
		],
		[
			'class' => 'kartik\grid\ActionColumn',
		
			'dropdown' => true,
			'vAlign'=>'middle',
			'urlCreator' => function($action, $model, $key, $index) { return '#'; },
			//'viewOptions'=>['title'=>$viewMsg, 'data-toggle'=>'tooltip'],
			//'updateOptions'=>['title'=>$updateMsg, 'data-toggle'=>'tooltip'],
			//'deleteOptions'=>['title'=>$deleteMsg, 'data-toggle'=>'tooltip'], 
		],
		['class' => 'kartik\grid\CheckboxColumn']
	];
    
    	echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'columns' => $gridColumns,
		'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            
			[
				'attribute'=>'id', 
				'value'=>'id',
				'width'=>'100px',
			],	
			'username',
			'nombre',
            'apellido',
            'cedula',
            //'monto',
			[
			 'attribute' => 'status',
			 'value'=>'status',
			 'filterType'=>GridView::FILTER_SELECT2,
			 'filter' => array(""=>"","10"=>"Activo","0"=>"Inactivo"),
			],

            [
			 'class' => 'yii\grid\ActionColumn',
			 'template' => '{view} {update}',
             'buttons' => [
               'seguimiento' => function ($url,$model, $key) {
                   return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update'], [
                       'id' => 'activity-index-link',
                       'title' => Yii::t('app', 'Actualizar'),
                       'data-pjax' => '0',
                   ]);
               },
             ],
			],
        ],
		'toolbar' => [
			[
				'content'=>
					Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],[
						//'type'=>'button', 
						'title'=>Yii::t('app', 'Reclamo Automovil'), 
						'class'=>'btn btn-success'
					]) . ' '.
					Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
						'class' => 'btn btn-default', 
						'title' => Yii::t('app', 'Reset Grid')
					]),
				
			],
			'{export}',
			'{toggleData}'
		],
		 
		'pjax' => false,
		'pjaxSettings' => [
			'options' => ['id' => 'user-pjax-id'],// UNIQUE PJAX CONTAINER ID
		],
		'bordered' => false,
		'resizableColumns'=>true,
		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
		'striped' => true,
		'condensed' => false,
		'responsive' => true,
		'hover' => true,
		//'floatHeader' => true,
		//'floatHeaderOptions' => ['scrollingTop' => '50'],
		'showPageSummary' => false,
		'panel' => [
			'type' => GridView::TYPE_PRIMARY
		],
    ]);
    
    ?>

    <?/*= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'auth_key',
            'password_hash',
            'password_reset_token',
            // 'email:email',
            // 'status',
            // 'created_at',
            // 'updated_at',
            // 'rol_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); */?>

</div>
