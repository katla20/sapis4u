<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
//use yii\helpers\Html;
//use yii\bootstrap\ActiveForm;
//use yii\bootstrap\ActiveField;
use backend\models\Rol;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\depdrop\Depdrop; //http://demos.krajee.com/widget-details/depdrop
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
    <div class="box-header with-border">
         <h4 class="box-title"><!--<i class="glyphicon glyphicon-user"></i>--> Crear Usuario</h4>
    </div>
    
        <div class="box-body">
			<div class="container-items"><!-- widgetBody -->
            
                 <div class="row">
            					  <div class="col-sm-4">
       					             <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
            					  </div>
            					  <div class="col-sm-4">
            						 <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            					  </div>
                 </div>
                 
                 <div class="row">
            					  <div class="col-sm-4">
       					              <?= $form->field($model, 'password_hash')->passwordInput() ?>
            					  </div>
            					  <div class="col-sm-4">
            						 <?php
                						echo $form->field($model,"rol_id")->widget(Select2::classname(), [
                						'data' => ArrayHelper::map(Rol::find()->all(), 'id', 'nombre'),
                						'language' => 'en',
                						'options' => ['placeholder' => 'Select ...'],
                						'pluginOptions' => [
                						'allowClear' => true
                						],
                						 ]);
                
          						      ?>
            					  </div>
                 </div>
                 
                 <div class="row">
            					  <div class="col-sm-4">
       					             <?= $form->field($model, 'cedula')->textInput(['maxlength' => true]) ?>
            					  </div>
            					  <div class="col-sm-4">
            						
            					  </div>
                  </div>
    
                <div class="row">
            					  <div class="col-sm-4">
            					  <?= $form->field($model, 'nombre',[
                                                      'addon' => [
                                                          'prepend' => [
                                                              'content' => '<i class="fa fa-pencil"></i>'
            
                                                          ]
                                                      ]
                                                  ])->textInput(['maxlength' => true]) ?>
            					  </div>
            					  <div class="col-sm-4">
            						  <?= $form->field($model, 'nombre2',[
                                                        'addon' => [
                                                            'prepend' => [
                                                                'content' => '<i class="fa fa-pencil"></i>'
            
                                                            ]
                                                        ]
                                                    ])->textInput(['maxlength' => true]) ?>
            					  </div>
                    </div>
                    <div class="row">
            					  <div class="col-sm-4">
            						  <?= $form->field($model, 'apellido',[
                                                        'addon' => [
                                                            'prepend' => [
                                                                'content' => '<i class="fa fa-pencil"></i>'
            
                                                            ]
                                                        ]
                                                    ])->textInput(['maxlength' => true]) ?>
            					  </div>
            					  <div class="col-sm-4">
            						  <?= $form->field($model, 'apellido2',[
                                                        'addon' => [
                                                            'prepend' => [
                                                                'content' => '<i class="fa fa-pencil"></i>'
            
                                                            ]
                                                        ]
                                                    ])->textInput(['maxlength' => true]) ?>
            					  </div>
                    </div>
                    
                    
                    
                    <div class="row">
                        <div class="col-sm-3">
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>
                                        
                    </div>
            </div>
        </div>    
    </div>   

    

    <?php ActiveForm::end(); ?>

</div>
