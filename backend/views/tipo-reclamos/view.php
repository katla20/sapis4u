<?php

use yii\helpers\Html;
//use yii\widgets\DetailView;
use kartik\detail\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\TipoReclamos */

$this->title = '';//$model->id_tipo_reclamo;
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
            <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
            <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Tipos de Reclamos')?></div></a>
            <a href="#" class="btn btn-default"><div>Detalle del tipo de reclamo</div></a>
</div>
</br></br>
<div class="tipo-reclamos-view">

	<?php
	$attributes = [

    [
        'columns' => [
		    [
                'attribute'=>'id_tipo_reclamo',
				'label'=>'Código',
                'format'=>'raw',
                'value'=>'<kbd>'.$model->id_tipo_reclamo.'</kbd>',
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ],
            [
                'attribute'=>'nombre',
                'label'=>'Tipo Reclamo',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],
			[
                'attribute'=>'id_ramo',
                'label'=>'Ramo',
				'value'=> $model->idRamos->nombre,
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],

        ],
    ],
	];
//print_r($model);
	// View file rendering the widget

	echo DetailView::widget([
		'model' => $model,
		'attributes' => $attributes,
		'mode' => 'view',
		'enableEditMode'=> false,
		'responsive' => 'responsive',
		'deleteOptions'=>[ // your ajax delete parameters
			'params' => ['id' => 1000, 'kvdelete'=>true],
		],
		'panel'=>[
        'heading'=>'Tipo Reclamo',
        'type'=>DetailView::TYPE_INFO,

        ],
		'container' => ['id'=>'kv-demo'],
		//'formOptions' => ['action' => Url::current(['update' => 'kv-demo'])] // your action to delete
	]);


    ?>

</div>
