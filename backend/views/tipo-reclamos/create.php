<?php

use yii\helpers\Html;
use kartik\icons\Icon;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\TipoReclamos */

$this->title = '';
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
          <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
          <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Tipos de reclamos')?></div></a>
          <a href="#" class="btn btn-default"><div>Crear Tipo de reclamo</div></a>
</div>
</br></br>
<div class="tipo-reclamos-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
