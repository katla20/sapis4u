<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;//keyla bullon
use backend\models\Ramos;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\TipoReclamos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tipo-reclamos-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
    <div class="box-header with-border">
         <h4 class="box-title"><!--<i class="glyphicon glyphicon-user"></i>--> Crear Tipo de Reclamo</h4>
    </div>

		<div class="box-body">
			<div class="row">
				<div class="col-sm-3">
					<?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
				</div>
				
				 <div class="col-sm-3">
                <?=$form->field($model,"id_ramo")->widget(Select2::classname(), [
							'data' => ArrayHelper::map(Ramos::find()->all(), 'id_ramo', 'nombre'),
							'language' => 'en',
							'options' => ['placeholder' => 'Select ...'],
							'pluginOptions' => [
							'allowClear' => true
							],
						]);
				 ?>
            </div>
			</div>

		   <div class="row">
					<div class="col-sm-3">
						<?php
						//echo $form->field($model, 'estatus')->dropDownList([1 => 'Activo', 0 => 'Inactivo']);
						echo $form->field($model, 'estatus')->widget(Select2::classname(), [
								'data' => array("1"=>"Activo","0"=>"Inactivo"),
								'language' => 'es',
								//'options' => ['placeholder' => 'Seleccione un Esatdo ...'],
								'pluginOptions' => [
								'allowClear' => true
								],
						]);	
						?>
					</div> 
				</div>

			<div class="form-group">
				<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			</div>
		</div>
	</div>
    <?php ActiveForm::end(); ?>

</div>
