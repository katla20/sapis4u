<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\Poliza;
use backend\models\Aseguradoras;
use kartik\icons\Icon;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PolizaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//http://www.w3schools.com/colors/colors_picker.asp colores

$this->title = "";

?>
 <div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
            <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
</div>
</br></br>
<div class="poliza-flota-index">

    <?php
  	$model = new Poliza();
  	$gridColumns = [
  	//	['class' => 'yii\grid\SerialColumn'],
    		[
    			'attribute'=>'numero_poliza',
    			'value'=>'numero_poliza',
    			'width'=>'180px',
        ],
    		[
              'attribute'=>'id_aseguradora',
              'width'=>'200px',
              'value'=>'aseguradora',
              'filterType'=>GridView::FILTER_SELECT2,
              'filter'=>ArrayHelper::map(Aseguradoras::find()->orderBy('nombre')->asArray()->all(), 'id_aseguradora', 'nombre'),
              'filterWidgetOptions'=>[
                'pluginOptions'=>['allowClear'=>true],
        ],
          'filterInputOptions'=>['placeholder'=>'Aseguradoras','id' => 'aseguradoras-select2-id'],
          //'group'=>true,  // enable grouping
        ],
		  [ //'class' => 'kartik\grid\DataColumn',
			'attribute'=>'estatus_poliza',
			'filterType'=>GridView::FILTER_SELECT2,
			'filter'=>ArrayHelper::map(Poliza::find()->orderBy('estatus_poliza')->asArray()->all(), 'estatus_poliza', 'estatus_poliza'),
			'filterWidgetOptions'=>[
			  'pluginOptions'=>['allowClear'=>true],
	        ],
				'vAlign'=>'middle',
				'width'=>'150px',
				'value'=>function ($model, $key, $index, $widget) {
  					if($model->estatus_poliza=='Activa'){
    				       return "<span class='label label-success'>".$model->estatus_poliza .'</span>';
  					}else if($model->estatus_poliza=='Vencida'){
  					   return "<span class='label label-danger'>".$model->estatus_poliza .'</span>';
  					}else if($model->estatus_poliza=='Pendiente'){
  					   return "<span class='label label-warning'>".$model->estatus_poliza .'</span>';
  					}
  			},
  			 'format'=>'raw',
  			 'noWrap'=>false,
		],
          ['class' => 'kartik\grid\DataColumn',
		   'attribute'=>'fecha_vigenciadesde',
		   'width'=>'200px',
		    'value'=>function ($model) {
                 return date("d-m-Y", strtotime($model->fecha_vigenciadesde));
            },
		   'filterType'=>GridView::FILTER_DATE,
		   'filterWidgetOptions' => [
			  'pluginOptions'=>[
			  'format' => 'dd-mm-yyyy',
			  'autoWidget' => true,
			  'autoclose' => true,
			  //'todayBtn' => true,
			  ],

			],
       ],
      ['class' => 'kartik\grid\DataColumn',
       'attribute'=>'fecha_vigenciahasta',
       'width'=>'200px',
       'value'=>function ($model) {
                 return date("d-m-Y", strtotime($model->fecha_vigenciahasta));
        },
       'filterType'=>GridView::FILTER_DATE,
       'filterWidgetOptions' => [
          'pluginOptions'=>[
          'format' => 'dd-mm-yyyy',
          'autoWidget' => true,
          'autoclose' => true,
          //'todayBtn' => true,
          ],

        ],
      ],
	  [
  		   'attribute'=>'nombre_completo',
  		   'width'=>'300px',
  		   'value'=>'nombre_completo'
      ],
      [
        'class'=>'kartik\grid\ExpandRowColumn',
        'width'=>'50px',
        'value'=>function ($model, $key, $index, $column) {
                    return GridView::ROW_COLLAPSED;
        },
        'detail'=>function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('recibos', ['poliza'=>$model->id_poliza]);
        },
        'headerOptions'=>['class'=>'kartik-sheet-style'],
        'expandOneOnly'=>true
      ],

      [
       'class' => 'yii\grid\ActionColumn',
	      'contentOptions' => ['style' => 'width:150px;'],
       'header'=>'GESTION DE RECIBOS',
       'template' => '{view} {update} {renovacion} {inclusion} {exclusion} {anulacion}',
             'buttons' => [
               'view' => function ($url,$model, $key) {
                 return Html::a('<i class="fa fa-binoculars fa-lg" aria-hidden="true"></i>', ['poliza-colectivo/view','id' => $model->id_poliza], [
                     'id' => 'activity-index-link',
                     'title' => Yii::t('app', 'Ver Colectivo'),'rel'=>'tooltip',
                     'data-pjax' => '0',
                 ]);
               },
			          'update' => function ($url,$model, $key) {
                   return Html::a('<i class="fa fa-pencil fa-lg" aria-hidden="true"></i>', ['poliza/update','id' => $model->id_poliza], [
                       'id' => 'activity-index-link',
                       'title' => Yii::t('app', 'Actualizar Poliza'),'rel'=>'tooltip',
                       'data-pjax' => '0',
                   ]);
                  },

               'renovacion' => function ($url,$model, $key) {
                   return Html::a('<i class="fa fa-retweet fa-lg" aria-hidden="true"></i>', ['renovacion-colectivo/update','id' => $model->id_poliza], [
                       'id' => 'activity-index-link',
                       'title' => Yii::t('app', 'Renovacion Colectivo'),'rel'=>'tooltip',
                       'data-pjax' => '0',
                   ]);
                  },
                'inclusion' => function ($url,$model, $key) {
                   return Html::a('<i class="fa fa-plus-circle fa-lg" aria-hidden="true"></i>', ['renovacion-colectivo/inclusion','id' => $model->id_poliza], [
                       'id' => 'activity-index-link',
                       'title' => Yii::t('app', 'Inclusion de Colectivo'),'rel'=>'tooltip',
                       'data-pjax' => '0',
                   ]);
                  },
                 'exclusion' => function ($url,$model, $key) {
                   return Html::a('<i class="fa fa-minus-circle fa-lg" aria-hidden="true"></i>', ['renovacion-colectivo/exclusion','id' => $model->id_poliza], [
                       'id' => 'activity-index-link',
                       'title' => Yii::t('app', 'Exclusion de Colectivo'),'rel'=>'tooltip',
                       'data-pjax' => '0',
                   ]);
                  },
               'anulacion' => function ($url,$model, $key) {
                         return Html::a('<i class="fa fa-trash fa-lg" aria-hidden="true"></i>', ['anulacion','id' => $model->id_poliza], [
                             'id' => 'activity-index-link-anular',
                             'title' => Yii::t('app','Anulacion de Poliza'),'rel'=>'tooltip',
                             'data-pjax' => '0',
							               'data-confirm'=>'Tiene '.PolizaAnulacion::CantReclamos($model->id_poliza).' Reclamos Asociada a la poliza',
                         ]);
                 }
             ]
      ],

  	];

  	echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
  		'columns' => $gridColumns,
		//'showPageSummary' => true,
  		'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
  		'toolbar' => [
  			[
  				'content'=>
  					Html::a('<i class="glyphicon glyphicon-plus"></i> Registrar', ['create'],[
  						//'type'=>'button',
  						'title'=>Yii::t('app', 'Registrar Colectivo'),
  						'class'=>'btn btn-success'
  					]) . ' '.
  					Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
  						'class' => 'btn btn-default',
  						'title' => Yii::t('app', 'Actualizar Grid')
  					]),

  			],
  			'{export}',
  			'{toggleData}'
  		],
  		'exportConfig' => [
  			GridView::EXCEL => [],
  			GridView::TEXT => [],
  			GridView::PDF => [],
           ],


  		'pjax' => false,
  		'pjaxSettings' => [
  			'options' => ['id' => 'poliza-pjax-id'],// UNIQUE PJAX CONTAINER ID
  		],
  		'bordered' => false,
  		'resizableColumns'=>true,
  		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
  		'striped' => true,
  		'condensed' => false,
  		'responsive' => true,
  		'hover' => true,
    	'panel' => [
          'heading'=>'<h2 class="panel-title" style="font-weight:bold;">'.Icon::show('car', ['class' => 'fa-2x']).'&nbsp; &nbsp;REGISTRAR COLECTIVO</h2>',
    			'type' => GridView::TYPE_INFO
    		],
      ]);
  	?>

</div>
<?php
$string = <<<EXP2
$("[rel=tooltip]").tooltip({ placement: 'top'});
EXP2;
$this->registerJs($string, \yii\web\View::POS_READY);
?>
<?php
$string='.nav-bar_opcion {
  position: fixed;
  top: 200;
  left: 0;
  right: 10;
  z-index: 9999;
  width: 50%;
  height: 50px;
}';
$this->registerCss($string);
?>
<?php
$string='
.fa{margin:0px 4px 0px 0px;}
 .fa-trash{
     color:  #d9534f;
 }
 .fa-plus{
     color:  #5cb85c;
 }
 .fa-retweet{
     color:  #5bc0de;
 }
 .fa-binoculars{
     color:  #39CCCC;
 }';
$this->registerCss($string);
?>
