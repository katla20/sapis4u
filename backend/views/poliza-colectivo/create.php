<?php

use yii\helpers\Html;

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;
use kartik\icons\Icon;

Icon::map($this);

/* @var $this yii\web\View */
/* @var $model backend\models\Poliza */

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Polizas'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
$this->title="";
?>
<div class="poliza-salud-create">
</br>
  <div class="bg-aqua">
      <?php echo Icon::show('ambulance', ['class' => 'fa-3x']);?><?php echo Icon::show('users', ['class' => 'fa-3x'], Icon::FA);?>
      <span style="color:white;font-size:18px;font-weight:bold;"><?=Yii::t('app', 'Crear Poliza Salud Individual');?></span>
 </div>
    <?= $this->render('_form', [
        'model' => $model,'documentos' => $documentos,'recibo' => $recibo,
    ]) ?>
</div>
