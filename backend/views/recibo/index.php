<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\Poliza;
use backend\models\Aseguradoras;
use kartik\icons\Icon;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PolizaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//http://www.w3schools.com/colors/colors_picker.asp colores

$this->title = "";

?>
 <div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
            <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
</div>
</br></br>
<div class="poliza-index">

    <?php
  	$model = new Poliza();
  	$gridColumns = [
  	//	['class' => 'yii\grid\SerialColumn'],
    		[
    			'attribute'=>'numero_poliza',
    			'value'=>'numero_poliza',
    			'width'=>'180px',
        ],
    		[
              'attribute'=>'id_aseguradora',
              'width'=>'200px',
              'value'=>'aseguradora',
              'filterType'=>GridView::FILTER_SELECT2,
              'filter'=>ArrayHelper::map(Aseguradoras::find()->orderBy('nombre')->asArray()->all(), 'id_aseguradora', 'nombre'),
              'filterWidgetOptions'=>[
                'pluginOptions'=>['allowClear'=>true],
        ],
          'filterInputOptions'=>['placeholder'=>'Aseguradoras','id' => 'aseguradoras-select2-id'],
          //'group'=>true,  // enable grouping
        ],
		  [ //'class' => 'kartik\grid\DataColumn',
			'attribute'=>'estatus_poliza',
			'filterType'=>GridView::FILTER_SELECT2,
			'filter'=>ArrayHelper::map(Poliza::find()->orderBy('estatus_poliza')->asArray()->all(), 'estatus_poliza', 'estatus_poliza'),
			'filterWidgetOptions'=>[
			  'pluginOptions'=>['allowClear'=>true],
	        ],
				'vAlign'=>'middle',
				'width'=>'150px',
				'value'=>function ($model, $key, $index, $widget) {
  					if($model->estatus_poliza=='Activa'){
    				       return "<span class='label label-success'>".$model->estatus_poliza .'</span>';
  					}else if($model->estatus_poliza=='Vencida'){
  					   return "<span class='label label-danger'>".$model->estatus_poliza .'</span>';
  					}else if($model->estatus_poliza=='Pendiente'){
  					   return "<span class='label label-warning'>".$model->estatus_poliza .'</span>';
  					}
  			},
  			 'format'=>'raw',
  			 'noWrap'=>false,
		],
        ['class' => 'kartik\grid\DataColumn',
		   'attribute'=>'fecha_vigenciadesde',
		   'width'=>'200px',
		   'value'=>'fecha_vigenciadesde',
		   'filterType'=>GridView::FILTER_DATE,
		   'filterWidgetOptions' => [
			  'pluginOptions'=>[
			  'format' => 'yyyy-mm-dd',
			  'autoWidget' => true,
			  'autoclose' => true,
			  //'todayBtn' => true,
			  ],

			],
       ],
      ['class' => 'kartik\grid\DataColumn',
       'attribute'=>'fecha_vigenciahasta',
       'width'=>'200px',
       'value'=>'fecha_vigenciadesde',
       'filterType'=>GridView::FILTER_DATE,
       'filterWidgetOptions' => [
          'pluginOptions'=>[
          'format' => 'yyyy-mm-dd',
          'autoWidget' => true,
          'autoclose' => true,
          //'todayBtn' => true,
          ],

        ],
      ],
	    [
  		   'attribute'=>'nombre_completo',
  		   'width'=>'300px',
  		   'value'=>'nombre_completo'
      ],
	 /* [
		'class'=>'kartik\grid\BooleanColumn',
		'filterType'=>GridView::FILTER_SELECT2,
		'attribute'=>'estatus',
		'vAlign'=>'middle',
		'width'=>'150px',
	   ],*/
  		//['class' => 'yii\grid\ActionColumn'],
      [
       'class' => 'yii\grid\ActionColumn',
       'template' => '{view} {update} {renovacion} {recibos} {anulacion}',
             'buttons' => [
               'renovacion' => function ($url,$model, $key) {
                   return Html::a('<i class="fa fa-retweet" aria-hidden="true"></i>', ['renovacion/create','id' => $model->id_poliza], [
                       'id' => 'activity-index-link',
                       'title' => Yii::t('app', 'Renovacion de Poliza'),
                       'data-pjax' => '0',
                   ]);
                  },
                'recibos' => function ($url,$model, $key) {
                         return Html::a('<i class="fa fa-bars" aria-hidden="true"></i>', ['recibo/create','id' => $model->id_poliza], [
                             'id' => 'activity-index-link',
                             'title' => Yii::t('app', 'Gestion de Recibos'),
                             'data-pjax' => '0',
                         ]);
                 },
               'anulacion' => function ($url,$model, $key) {
                         return Html::a('<i class="fa fa-trash" aria-hidden="true"></i>', '#', [
                            'id' => 'activity-index-link',
                            'title' => Yii::t('app','Anulacion de Recibos'),
                            'data-toggle' => 'modal',
                            'data-target' => '#modal',
                            'data-url' => Url::to(['anulacion', 'id' => $model->id_poliza]),
                            'data-pjax' => '0',
                        ]);
                     }
             ]
      ],

  	];

  	echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
  		'columns' => $gridColumns,
		//'showPageSummary' => true,
  		'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
  		'toolbar' => [
  			[
  				'content'=>
  					Html::a('<i class="glyphicon glyphicon-plus"></i> Registrar', ['create'],[
  						//'type'=>'button',
  						'title'=>Yii::t('app', 'Registrar Poliza Automovil'),
  						'class'=>'btn btn-success'
  					]) . ' '.
  					Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
  						'class' => 'btn btn-default',
  						'title' => Yii::t('app', 'Actualizar Grid')
  					]),

  			],
  			'{export}',
  			'{toggleData}'
  		],
  		'exportConfig' => [
  			GridView::EXCEL => [],
  			GridView::TEXT => [],
  			GridView::PDF => [],
           ],


  		'pjax' => false,
  		'pjaxSettings' => [
  			'options' => ['id' => 'poliza-pjax-id'],// UNIQUE PJAX CONTAINER ID
  		],
  		'bordered' => false,
  		'resizableColumns'=>true,
  		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
  		'striped' => true,
  		'condensed' => false,
  		'responsive' => true,
  		'hover' => true,
    	'panel' => [
          'heading'=>'<h2 class="panel-title" style="font-weight:bold;">'.Icon::show('car', ['class' => 'fa-2x']).'&nbsp; &nbsp;POLIZAS DE AUTOMOVIL INDIVIDUAL</h2>',
    			'type' => GridView::TYPE_INFO
    		],
      ]);
  	?>

</div>
<?php
$string='';
$this->registerCss($string);
?>
