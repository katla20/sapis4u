<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Poliza */

$this->title = $model->id_poliza;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Polizas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poliza-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id_poliza], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id_poliza], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_poliza',
            'numero_poliza',
            'id_tipo',
            'fecha_vigenciadesde',
            'fecha_vigenciahasta',
            'estatus',
            'id_contratante',
            'fecha_registro',
            'fecha_actualizacion',
            'id_user_registro',
            'id_user_actualizacion',
        ],
    ]) ?>

</div>
