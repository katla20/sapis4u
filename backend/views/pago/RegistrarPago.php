<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Pago */
/* @var $form ActiveForm */
?>
<div class="RegistrarPago">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'id_forma_pago') ?>
        <?= $form->field($model, 'id_banco') ?>
        <?= $form->field($model, 'id_modalidad_pago') ?>
        <?= $form->field($model, 'monto') ?>
        <?= $form->field($model, 'id_user_registro') ?>
        <?= $form->field($model, 'id_user_actualizacion') ?>
        <?= $form->field($model, 'fecha_pago') ?>
        <?= $form->field($model, 'fecha_vencimiento') ?>
        <?= $form->field($model, 'fecha_actualizacion') ?>
        <?= $form->field($model, 'fecha_registro') ?>
        <?= $form->field($model, 'estatus') ?>
        <?= $form->field($model, 'estatus_pago') ?>
        <?= $form->field($model, 'mes_pago') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- RegistrarPago -->
