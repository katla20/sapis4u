<?php
use yii\helpers\Html;
//use yii\bootstrap\ActiveForm;
//use yii\bootstrap\ActiveField;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use kartik\select2\Select2;
use kartik\widgets\DepDrop;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use yii\bootstrap\Tabs;
use kartik\icons\Icon;
use kartik\tabs\TabsX;
use kartik\nav\NavX;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\web\Response;
use kartik\money\MaskMoney;
use backend\models\Aseguradoras;
use backend\models\Cliente;
use backend\models\Persona;
use backend\models\Poliza;
use backend\models\Recibo;
use backend\models\FormaPago;
use backend\models\Banco;
use backend\controllers\PagoController;

$contado="disabled";
$fraccionado="disabled";
$financiado="disabled";
switch ($modalidad) {
  case 1:
  $contado="active";
   break;
  case 2:
  $fraccionado="active";
   break;
  case 3:
  $financiado="active";
   break;

}
?>

<?php $form = ActiveForm::begin(['id' => 'pago-form']);?>
<div class="pago-form'">
    <div class="box box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
    <div class="box-header">
        <div class="row">
            <div class="col-md-12">
              <input type="hidden" id='id_recibo' name="id_recibo" value="<?=$id_recibo?>">
              <input type="hidden" id='prima_total' name="prima_total" value="<?=$prima?>">
              <input type="hidden" id='id_modalidad_pago' name="id_modalidad_pago" value="<?=$modalidad?>">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <div class="btn-group nav-tabs" data-toggle="buttons-radio">
           				   <button type="button" class="btn btn-info <?=$contado?>">Contado</button>
           				   <button type="button" class="btn btn-info <?=$fraccionado?>">Fraccionado</button>
           				   <button type="button" class="btn btn-info <?=$financiado?>">Financiado</button>
         			    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <?php
                   echo PagoController::getDatos($id_recibo);
                  ?>
                </div><!-- /.box-body -->

              </div><!-- /.box -->
            </div><!-- ./col -->
       </div>
    </div>
    <div class="box-body">
  	    <div class="container-items"><!-- widgetBody -->
         <div class="row">
            <div class="col-sm-5 cmpo" id="id_banco">
                 <?=$form->field($model,"id_banco",[
                                               'addon' => [
                                                   'prepend' => [
                                                       'content' => '<i class="fa fa-university"></i>'

                                                   ]
                                               ]
                                           ])->widget(Select2::classname(), [
                       'data' =>   ArrayHelper::map(Banco::find()->where('estatus=:status')
                                        ->addParams([':status' => 1])
                                        ->all(),'id_banco', 'nombre_banco'),
                      'language' => 'en',
                       'options' => ['placeholder' => 'Seleccione ...'],
                       'pluginOptions' => [
                          'allowClear' => true
                      ],
                    ]);
                  ?>
      </div>
    </div>
    <div class="row">
	    <div class="col-sm-3 cmpo" id="monto_inicial">
	        <? echo $form->field($model, 'monto_inicial',[
   										  'addon' => [
   											  'prepend' => [
   												  'content' => '<i class="fa fa-money" aria-hidden="true"></i>'

   											  ]
   										  ]
   									  ])->widget(MaskMoney::classname(), [
   																		  'pluginOptions' => [
   																			  'prefix' => '',
   																			  'suffix' => '',
   																			  'allowNegative' => false
   																			]
   								]);?>
	    </div>
	    <div class="col-sm-2 cmpo" id="fecha_vencimiento">
          <?=$form->field($model, 'fecha_vencimiento',[
                                        'addon' => [
                                            'prepend' => [
                                                'content' => '<i class="glyphicon glyphicon-calendar"></i>'

                                            ]
                                        ]
                                    ])->widget(\yii\widgets\MaskedInput::className(),
                                                             [
                                                             'clientOptions' => ['alias' =>  'date']
                                                             ]) ?>
       </div>
    </div>
    <div class="row">
        <div class="col-sm-2 cmpo" id="monto">
         <?/*$model->monto=Recibo::findReciboPrima($id_recibo);*/?>
         <?=$form->field($model, 'monto',[
       												  'addon' => [
       													  'prepend' => [
       														  'content' => '<i class="fa fa-money" aria-hidden="true"></i>'

       													  ]
       												  ]
       											  ])->widget(MaskMoney::classname(), [
       																				  'pluginOptions' => [
       																					  'prefix' => '',
       																					  'suffix' => '',
       																					  'allowNegative' => false
       																					]
       										]);?>

        </div>
	    <div class="col-sm-3 cmpo" id="total_cuotas">
         <?=$form->field($model,"total_cuotas",[
                                           'addon' => [
                                               'prepend' => ['content'=>'<i class="glyphicon glyphicon-list"></i>'],
                                               'append'=>['content'=>Html::a('<i class="fa fa-plus"></i>Generar Cuotas', '#', [
                                                                              'id' => 'generar-cuotas',
                                                                              'class' => 'btn btn-success',
                                                                              'data-pjax' => '0',
                                                                          ]), 'asButton'=>true],
                                           ]
                                ])->widget(Select2::classname(), [
                                              'data' => array_combine(range(2,10),range(2,10)),
                                              'language' => 'en',
                                               'options' => ['placeholder' => 'Seleccione ...'],
                                               'pluginOptions' => [
                                                  'allowClear' => true
                                              ],
                                            ]);
          ?>
      </div>
      <div class="col-sm-2 cmpo" id="id_forma_pago">
         <?=$form->field($model,"id_forma_pago",[
                                       'addon' => [
                                           'prepend' => [
                                               'content' => '<i class="fa fa-credit-card" aria-hidden="true"></i>'

                                           ]
                                       ]
                                   ])->widget(Select2::classname(), [
                                              'data' => ArrayHelper::map(FormaPago::find()->where('estatus=:status')
                                                               ->addParams([':status' => 1])
                                                               ->all(),'id_forma_pago', 'nombre_forma_pago'),
                                              'language' => 'en',
                                               'options' => ['placeholder' => 'Seleccione ...'],
                                               'pluginOptions' => [
                                                  'allowClear' => true
                                              ],
                                            ]);
          ?>
    </div>
    <div class="col-sm-2 cmpo" id="referencia">
         <?= $form->field($model, 'referencia',[
                                       'addon' => [
                                           'prepend' => [
                                               'content' =>'<i class="fa fa-hashtag" aria-hidden="true"></i>'

                                           ]
                                       ]
                                   ])->textInput(['maxlength' => true]) ?>

    </div>
    <div class="col-sm-2 cmpo" id="fecha_pago">
          <?=$form->field($model, 'fecha_pago',[
                                        'addon' => [
                                            'prepend' => [
                                                'content' => '<i class="glyphicon glyphicon-calendar"></i>'

                                            ]
                                        ]
                                    ])->widget(\yii\widgets\MaskedInput::className(),
                                                             [
                                                             'clientOptions' => ['alias' =>  'date']
                                                             ]) ?>

    </div>
    </div>
    <div class="row">
      <div class="col-sm-2 cmpo" id="dia_pago">
        <?
        echo $form->field($model,"dia_pago",[
                                      'addon' => [
                                          'prepend' => [
                                              'content' => '<i class="fa fa-calendar-plus-o" aria-hidden="true"></i>'

                                          ]
                                      ]
                                  ])->widget(Select2::classname(), [
                                             'data' => array_combine(range(1,30),range(1,30)),
                                             'language' => 'en',
                                              'options' => ['placeholder' => 'Seleccione ...'],
                                              'pluginOptions' => [
                                                 'allowClear' => true
                                             ],
                                           ]);
         ?>
       </div>

    </div>
	<div class="row">

	    <div class="col-sm-6 cmpo" id="observacion">
         <?= $form->field($model, 'observacion',[
                                       'addon' => [
                                           'prepend' => [
                                               'content' => '<i class="fa fa-money" aria-hidden="true"></i>'

                                           ]
                                       ]
                                   ])->textarea(['maxlength' => true]) ?>

        </div>

	</div>
    <div class="row" id="tabla-pagos" style="display:none;">
        <input type="hidden" id="pago-contador" name="pago-contador" value="0">
        <div class="col-sm-8">
          <div class="datagrid">
            <table class="table table-condensed">
                <thead><tr><th>Nro Cuota</th><th>Fecha Vencimiento</th><th>Monto</th></thead>
                <tbody id="datopagos">
                </tbody>
           </table>
          </div>
        </div>
    </div>

  </div><!--CONTAINER-->
    <?/*= Html::Button(Icon::show('plus').'[Agregar Pago]', ['class' => 'btn btn-success','id'=>'btn-agregar']) */?>
  </div><!--BOX BODY-->
  </div><!--BOX-->

  <div class="form-group">
      <?= Html::submitButton(Icon::show('save').'[Registrar Pago]', ['class' => 'btn btn-primary']) ?>
      <?= Html::resetButton(Icon::show('eraser').'Limpiar Campos', ['class' => 'btn btn-default']) ?>
  </div>
 </div>
    <?php ActiveForm::end();?>
<?php

$string = <<<EXP2

var out='';

$(".cmpo,#tabla-pagos,button[type='submit']").hide();


var modalidad=$('#id_modalidad_pago').val();

     if(modalidad==1){//contado
         $(".cmpo").hide();
         $("#fecha_pago,#monto,#observacion,button[type='submit']").show();//contado campois
     }else if(modalidad==2){//credito
          $(".cmpo").hide();
          $("#total_cuotas,#monto,#fecha_vencimiento,#monto_inicial").show();//credito campois
     }else if(modalidad==3){//financiado
          $(".cmpo").hide();
          $("#total_cuotas,#monto,#fecha_vencimiento").show();//financiado campois
     }


$("#pago-form").on("keyup", "input[name='pago-monto-disp']", function(event) {

  var monto=Number($(this).formatearMonto());
  var prima=Number($('input[name="prima_total"]').val());
  var cuotas=$("select[name='Pago[total_cuotas]']").val();

  /*if(cuotas==""){
      alert('Debe seleccionar el numero de cuotas');
      $(this).val("");
    }*/

   if(monto > prima){
      alert('El monto Referencial no puede Superar el Monto de la Prima');
      $(this).val("");
   }

});

$("#btn-agregar").on("click", function(e) {

  var modalidad=$('#id_modalidad_pago').val();
  var out="";
  if($("#pago-id_banco option:selected").val()===""){
        alert("Debe seleccionar un banco");
  }else if($("input[name='Pago[monto]']").val()===""){
        alert("Debe incluir un monto");
  }else{

    $("#pago-contador").val(parseInt($("#pago-contador").val())+1);
      var id_cmpo=$("#pago-contador").val();


     out='<tr><td><input type="hidden" id="_fp" name="_fp['+id_cmpo+']" value="'+$("#pago-id_forma_pago option:selected").val()+'">'+$("#pago-id_forma_pago option:selected").html()+'</td>';
     out+='<td><input type="hidden" id="_bnco" name="_bnco['+id_cmpo+']" value="'+$("#pago-id_banco option:selected").val()+'">'+$("#pago-id_banco option:selected").html()+'</td>';
     out+='<td><input type="hidden" id="_ref"  name="_ref['+id_cmpo+']" value="'+$("input[name='Pago[referencia]']").val()+'">'+$("input[name='Pago[referencia]']").val()+'</td>';
	 out+='<td><input type="hidden" id="_fchp" name="_fchp['+id_cmpo+']" value="'+$("input[name='Pago[fecha_pago]']").val()+'">'+$("input[name='Pago[fecha_pago]']").val()+'</td>';
     out+='<td><input type="hidden" id="_mnto" name="_mnto['+id_cmpo+']" value="'+$("input[name='Pago[monto]']").val()+'">'+$("input[name='Pago[monto]']").val()+'</td>';
     out+='<td><a id="borrar-fp"><i class="fa fa-trash fa-lg"></i></a></td>';
     out+='</tr>';


	// out+='<td><textarea id="_obs"  name="_obs['+i+']" cols="40" rows="1">'+$("input[name='Pago[observacion]']").text()+'</textarea></td>';

      $("#datopagos").append(out);
      $("#pago-form").resetear();


  }
});//fin change

$(document).on('click', '#borrar-fp', (function() {
   $(this).borraritem();
}));//FIN DOCUMENT CLICK


$("#pago-form").on("keyup", "input[name*='_mnto']", function(event) {

var monto_total=$("input[name*='_mnto']").sumarPorcentaje();
var prima=Number($('input[name="prima_total"]').val());



  if(prima >= monto_total || modalidad==1){
        $("button[type='submit']").show();
  }else{
        $("button[type='submit']").hide();
  }

  $("input[id='_mntoTotal']").val(monto_total);

});


$(document).on('click', '#generar-cuotas', (function() {


 //MAQUETANDO EL HTML DE LAS CUOTAS
  var numero=$("select[name='Pago[total_cuotas]']").val();
  var out='';
  var i=0;
  var cadena_fecha=$("input[name='Pago[fecha_vencimiento]']").val();
  var monto=$("input[name='Pago[monto]']").val();
  var monto_inicial=$("input[name='Pago[monto_inicial]']").val();

  if(cadena_fecha!='' && numero!='' && monto!='' ){

     $("#tabla-pagos").show();

      var array_fecha = cadena_fecha.split("/");
      var anio = parseInt(array_fecha[2]);
      var mes = parseInt(array_fecha[1]);
      var dia  = parseInt(array_fecha[0]);

      var F = new Date(anio,mes,dia);
      var fecha_vencimiento=cadena_fecha;

    $("#datopagos").empty();
    
    
    //datos de la primera fraccion
    if(modalidad==2){//fraccionamiento
       out+='<tr><td>1</td>';
    		out+='<td><input type="text" id="_fchv" name="_fchv[0]" value="'+fecha_vencimiento+'"></td>';
    		out+='<td><input type="text" id="_mnto" name="_mnto[0]" value="'+monto_inicial+'"></td>';
    		out+='</tr>'; 
        i=1;
    }
    

      for (i;i < numero;i++) {
        var cuota=i+1;
  		    fecha_vencimiento=cadena_fecha;
          var dias_mes=getMonthDays(F.getMonth(), F.getFullYear());
           F.setDate(F.getDate()+dias_mes);
           mes=F.getMonth();
           dia=F.getDate();
           dia = (dia < 10) ? ("0" + dia) : dia;
           mes = (mes < 10) ? ("0" + mes) : mes;
           mes = (mes == "00") ? 12 : mes;
           fecha_vencimiento= dia + '/' + mes+ '/' + F.getFullYear();

        out+='<tr><td>'+cuota+'</td>';
    		out+='<td><input type="text" id="_fchv" name="_fchv['+i+']" value="'+fecha_vencimiento+'"></td>';
    		out+='<td><input type="text" id="_mnto" name="_mnto['+i+']" value="'+monto+'"></td>';
    		out+='</tr>';
       }//  for (var i = 0; i < numero; i++)

        out+='<tr><td></td>';
    		out+='<td></td>';
    		out+='<td><input type="text" id="_mntoTotal" value="" readOnly="readOnly"></td>';
    		out+='</tr>';

        $("#datopagos").html(out);
        $("#pago-contador").val(parseInt($("#pago-contador").val())+i);
        
        $("#pago-form").resetear();
        var monto=$("input[name*='_mnto']").sumarPorcentaje();

        $("input[id='_mntoTotal']").val(monto);
        //$("input[id='_mnto']").maskMoney({thousands:'', decimal:','});

        if(Number($('input[name="prima_total"]').val()) >= Number(monto)){
              $('button[type="submit"]').show();
        }else{
              $('button[type="submit"]').hide();
        }

  }else{
     alert('debe colocar datos referenciales para inicializar las cuotas');
  }

}));


jQuery.fn.resetear = function () {
  $(this).each (function() { this.reset(); });
}

jQuery.fn.borraritem = function() {
  $(this).parent().parent().fadeTo(400, 0, function () {
        $(this).remove();
    });
    return false;
};

function getMonthDays (month, year)//NUMERO DE DIAS DE UN MES
{
    return (month === 1 && year % 4 === 0 && (year % 100 !== 0 || year % 400 === 0)) ? 29 : [31,28,31,30,31,30,31,31,30,31,30,31][month];
}


EXP2;

$this->registerJs($string, \yii\web\View::POS_READY);

$string="
       body{overflow-y: scroll;
              overflow-x: hidden;
         }
         body{overflow-y: hidden;
              overflow-x: scroll;
         }
.datagrid table { border-collapse: collapse; text-align: left; width: 100%; } .datagrid {font: normal 12px/150% Arial, Helvetica, sans-serif; background: #fff; overflow: hidden; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; }.datagrid table td, .datagrid table th { padding: 3px 10px; }.datagrid table thead th {background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #B2B9BF), color-stop(1, #B2B8BF) );background:-moz-linear-gradient( center top, #B2B9BF 5%, #B2B8BF 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#B2B9BF', endColorstr='#B2B8BF');background-color:#B2B9BF; color:#FFFFFF; font-size: 13px; font-weight: bold; border-left: 0px solid #EEF6FF; } .datagrid table thead th:first-child { border: none; }.datagrid table tbody td { color: #8D9296; border-left: 1px solid #EEF6FF;font-size: 12px;font-weight: bold; }.datagrid table tbody .alt td { background: #CCD3DB; color: #5F6366; }.datagrid table tbody td:first-child { border-left: none; }.datagrid table tbody tr:last-child td { border-bottom: none; }";
$this->registerCss($string);
?>
