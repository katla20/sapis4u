<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\Poliza;
use backend\models\Recibo;
use backend\models\Modelos;
use kartik\icons\Icon;
use kartik\form\ActiveField;
use yii\helpers\Json;
use kartik\select2\Select2;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;
use yii\web\Response;
use kartik\money\MaskMoney;
use backend\models\Pago;
use backend\models\search\PagoSearch;
use backend\controllers\PagoController;

Icon::map($this, Icon::WHHG); // Maps the Elusive icon font framework
Icon::map($this, Icon::FA); // Maps the Elusive icon font framework

//http://www.w3schools.com/colors/colors_picker.asp colores

$this->title = "";
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
           <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
</div>
</br></br>
<div class="pago-index">
<?php

 $template='{contado} {credito} {financiado}';
  	$gridColumns = [

      [
  	    'class' => 'kartik\grid\DataColumn',
  			'attribute'=>'ramo',
  			'hAlign'=>'center',
  			'vAlign'=>'middle',
  			'value'=>'ramo',
  			'width'=>'200px',
      ],
      [
	        'class' => 'kartik\grid\DataColumn',
			    'attribute'=>'aseguradora',
			    'value'=>'aseguradora',
			    'width'=>'250px',
      ],
      [
	        'class' => 'kartik\grid\DataColumn',
			    'attribute'=>'nro_recibo',
			    'value'=>'nro_recibo',
			    'width'=>'200px',
      ],
      [
	        'class' => 'kartik\grid\DataColumn',
    			'attribute'=>'numero_poliza',
    			'value'=>'numero_poliza',
    			'width'=>'200px',
      ],
      [
  		   'attribute'=>'fecha_vigenciahasta',
  		   'width'=>'250px',
  		   'value'=>'fecha_vigenciahasta'
      ],
      [
  		   'attribute'=>'nombre_completo',
  		   'width'=>'300px',
  		   'value'=>'nombre_completo'
      ],
      [
        'class'=>'kartik\grid\ExpandRowColumn',
        'width'=>'50px',
        'value'=>function ($model, $key, $index, $column) {
              $datos=PagoController::getDatosPago($model->id_recibo,1);
              if(count($datos) > 0  && ($datos[0]['id_modalidad_pago']==2 || $datos[0]['id_modalidad_pago']==3 )){
                    return GridView::ROW_COLLAPSED;
              }else{
                return '<i class="fa fa-ban" aria-hidden="true" title="Sin Pagos" rel="tooltip" ></i>';
              }

        },
        'detail'=>function ($model, $key, $index, $column) {
            $datos=PagoController::getDatosPago($model->id_recibo,1);
            if(count($datos) > 0  && ($datos[0]['id_modalidad_pago']==2 || $datos[0]['id_modalidad_pago']==3 )){
                return Yii::$app->controller->renderPartial('pagos', ['model'=>$model]);
            }
        },
        'headerOptions'=>['class'=>'kartik-sheet-style'],
        'expandOneOnly'=>false,
      ],
      ['class' => 'yii\grid\ActionColumn',
        'contentOptions' => ['style' => 'width:100px;'],
        'header'=>'Operaciones',
        'template' => $template,
        'buttons' => [
            //view button
            'contado' => function ($url, $model) {
                if(count(PagoController::getDatosPago($model->id_recibo,1)) == 0){
                  return Html::a('<i class="fa fa-money fa-lg" aria-hidden="true"></i>',Url::to(['pago/create','modalidad' => '1','id_recibo'=>$model->id_recibo]), [
                              'title' => Yii::t('app', 'Registrar Contado'),'rel'=>'tooltip',
                              //'class'=>'btn btn-primary btn-xs',
                  ]);
                }else{
                  return '';
                }

            },
            'credito' => function ($url, $model) {
                if(count(PagoController::getDatosPago($model->id_recibo,2)) == 0){
                  return Html::a('<i class="fa fa-credit-card fa-lg" aria-hidden="true"></i>',Url::to(['pago/create','modalidad' => '2','id_recibo'=>$model->id_recibo]), [
                              'title' => Yii::t('app', 'Registrar Fraccionamiento'),'rel'=>'tooltip',
                              //'class'=>'btn btn-primary btn-xs',
                  ]);
                }else{
                  return '';
                }
            },
            'financiado' => function ($url, $model) {
                if(count(PagoController::getDatosPago($model->id_recibo,3)) == 0){
                    return Html::a('<i class="fa fa-bars fa-lg" aria-hidden="true"></i>',Url::to(['pago/create','modalidad' => '3','id_recibo'=>$model->id_recibo]), [
                              'title' => Yii::t('app', 'Registrar Financiamiento'),'rel'=>'tooltip',
                              'class'=>'icon icon-warning',
                  ]);
                }else{
                  return '';
                }
            },
        ],
      ]

  ];


  	echo GridView::widget([
     'dataProvider' => $dataProvider,
     'filterModel' => $searchModel,
     'showPageSummary'=>true,
  	 'columns' => $gridColumns,
  		'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
  		'toolbar' => [
  			[

  					Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
  						'class' => 'btn btn-default',
  						'title' => Yii::t('app', 'Actualizar Grid')
  					]),

  			],
  			'{export}',
  			'{toggleData}',
  		],
		  'exportConfig' => [
			GridView::EXCEL => [],
			GridView::TEXT => [],
			GridView::PDF => [],
		  ],

  		'pjax' => false,
  		'pjaxSettings' => [
  			'options' => ['id' => 'pago-pjax-id'],// UNIQUE PJAX CONTAINER ID
  		],
  		'bordered' => false,
  		'resizableColumns'=>true,
  		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
  		'striped' => true,
  		'condensed' => false,
  		'responsive' => true,
  		'hover' => true,

  		'showPageSummary' => false,
    		'panel' => [
          'heading'=>'<h2 class="panel-title" style="font-weight:bold;">'.Icon::show('cashregister', ['class'=>'icon'], Icon::WHHG).'&nbsp; &nbsp; PAGO DE POLIZAS</h2>',
    			'type' => GridView::TYPE_INFO
    		],
      ]);
      //http://www.webhostinghub.com/glyphs/ PARA EL ICONO DE CAJA REGISTRADORA NO OLVIDAR
?>

<?

   /*Html::a('<i class="glyphicon glyphicon-plus"></i> Hinzufügen', '#', [
                'class' => 'btn btn-success','id'=>'add-new-calculation',
                'onclick'=>"
                $.ajax({
                type     :'POST',
                cache    : false,
                data : {'t_magazine_id':'".$model->t_magazine_id."','t_field_id':$('#magazinecalc-t_field_id').val(),'calc_from':$('#magazinecalc-calc_from').val()},
                url  : '".Url::to(['magazine/createnewcalc'])."',
                success  : function(response) {
                    $.pjax.reload({container: '#magazineCalc-grid-pjax', timeout: 3000});
                }
                });return false;",
            ])*/

?>
</div>

<?php

$string = <<<EXP2
$("[rel=tooltip]").tooltip({ placement: 'top'});

EXP2;
$this->registerJs($string, \yii\web\View::POS_READY);

?>

<?php

$string='
@brand-success: #5cb85c;
@brand-info:    #5bc0de;
@brand-warning: #f0ad4e;
@brand-danger:  #d9534f;

.icon-cashregister{
       font-size: 150%;

}
.fa{margin:0px 4px 0px 0px;}
 .fa-bars{
     color:  #d9534f;
 }
 .fa-card{
     color:  #5cb85c;
 }
 .fa-money{
     color:  #5bc0de;
 }

 ';
$this->registerCss($string);
?>
