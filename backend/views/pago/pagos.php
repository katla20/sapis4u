<?php
use yii\helpers\Html;
//use yii\bootstrap\ActiveForm;
//use yii\bootstrap\ActiveField;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use kartik\select2\Select2;
use kartik\widgets\DepDrop;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use yii\bootstrap\Tabs;
use kartik\icons\Icon;
use kartik\tabs\TabsX;
use kartik\nav\NavX;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\web\Response;
use kartik\money\MaskMoney;
use backend\models\Aseguradoras;
use backend\models\Cliente;
use backend\models\Persona;
use backend\models\Poliza;
use backend\models\Recibo;
use backend\models\FormaPago;
use backend\models\Banco;
use backend\controllers\PagoController;
use backend\models\Pago;

?>
<?php

$prima=Recibo::findReciboPrima($model->id_recibo);
$montoPagado= Pago::findMontoPagado($model->id_recibo);
$montoPendiente=$prima-$montoPagado;

?>
<div class="box box-default">
  <div class="box-header ">
        <div class="row">
            <div class="col-md-3">Prima del Recibo :<?=$prima?><input type="hidden" id='prima_total' name="prima_total" value="<?=$prima?>"></div>
            <div class="col-md-3">Monto pagado:<?=$montoPagado?></div>
            <div class="col-md-3">Monto Pendiente:<?=$montoPendiente?></div>
        </div>
  </div>
  <div class="box-body">
      <div class="container-items"><!-- widgetBody -->
         <div class="row">
           <div class="col-md-12">
             <?=PagoController::getPagos($model->id_recibo)?>
          </div>
        </div>
    </div>
</div>
</div>
<?php
$script=<<< JS

$("[rel=tooltip]").tooltip({ placement: 'top'});
$(".boton-aplicar").on("click", function(e) {

	  var id=this.id.substring(4,6);

		  if($('input[name="_fchp['+id+']"]').val()=="" || $('input[name="_obs['+id+']"]').val()=="" ){
			     alert("debe llenar todos los campos de la cuota Nro"+id);
		  }else{

			 $.get($('input[name="_pgo['+id+']"]').data('url'),
               {id_pago:$('input[name="_pgo['+id+']"]').val(),monto:$('input[name="_mnto['+id+']"]').val(),fecha_pago:$('input[name="_fchp['+id+']"]').val(),
								observacion:$('input[name="_obs['+id+']"]').val()
				},
				 function (result) {
					  $("#tr-"+id).empty();
					  $("#tr-"+id).append(result);
				 });//FIN GET

		  }

});

JS;
$this->registerJs($script);

?>
