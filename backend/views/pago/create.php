<?php

use yii\helpers\Html;

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;
use kartik\icons\Icon;
use yii\helpers\Url;

$this->title="";

switch ($_GET['modalidad']) {
  case 1:
   $icon="money";
    break;
  case 2:
     $icon="credit-card";
  break;
  case 3:
     $icon="bars";
  break;
}

?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
          <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
          <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Pagos')?></div></a>
          <a href="#" class="btn btn-default"><div>Crear Pago</div></a>
</div>
</br></br>
<div class="pago-create">
    <div class="bg-aqua top-modulo">
        <span class="icon-modulo"><?=Icon::show($icon, ['class' => 'fa-3x'])?></span>
        <span style="color:white;font-size:18px;font-weight:bold;"><?=Yii::t('app', 'Registro de Pago');?></span>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
        'modalidad'=>$_GET['modalidad'],
        'id_recibo'=>$_GET['id_recibo'],
		    'prima'=>$prima

    ]) ?>
</div>
<?php
$string=".top-modulo{padding:2px 2px 2px 8px;}
         .icon-modulo{margin:0px 4px 0px 0px}";
$this->registerCss($string);
?>
