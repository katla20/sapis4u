<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $model backend\models\Poliza */

$this->title ="";
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
          <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
          <a href="<?=Url::to(['poliza-internacional/index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Polizas')?></div></a>
          <a href="#" class="btn btn-default"><div>Renovacion Poliza Internacional</div></a>
</div>
</br></br>
<div class="poliza-update">
  <div class="bg-aqua top-modulo">
      <span class="icon-modulo"><?=Icon::show('ambulance', ['class' => 'fa-3x']);?><?php echo Icon::show('globe', ['class' => 'fa-3x'], Icon::FA);?></span>
      <span style="color:white;font-size:18px;font-weight:bold;"><?=Yii::t('app', 'Renovacion de Poliza Internacional');?></span>
  </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
<?php $string=".top-modulo{padding:2px 2px 2px 8px;}
          .icon-modulo{margin:0px 4px 0px 0px}";
      $this->registerCss($string);
?>
