<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use kartik\icons\Icon;
use yii\web\Response;
use kartik\checkbox\CheckboxX;
use kartik\money\MaskMoney;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model backend\models\Poliza */
?>


<?=$form->field($model, 'hi')->widget(CheckboxX::classname());?>
<?=$form->field($model, 'suma')->widget(MaskMoney::classname());?>

<div class="box box-solid box-default">
<div class="box-header">
     <h4 class="box-title"><?=Icon::show('dashboard',[],Icon::BSG)?> Coberturas</h4>
</div><!--box-header --->
<div class="box-body">
    <div class="container-items"><!-- widgetBody -->
        <div class="row">
          <div class="col-sm-7"><!--col-sm-12--->
             <?php
               $model->tipo = 0;
               $list = [0 => 'Consolidado', 1 => 'Detallado'];
               /* Display an inline checkbox list */
               echo $form->field($model, 'tipo')->radioList($list, ['inline'=>true]);
            ?>
          </div>
          <div class="col-sm-3"><!--col-sm-2--->
              <?php  echo $form->field($model, 'deducible',[
                                                        'addon' => [
                                                            'prepend' => [
                                                                'content' => 'Deducible Bs.',

                                                            ]
                                                        ]
                                                    ])->widget(MaskMoney::classname(), [
                                                                                        'pluginOptions' => [
                                                                                            'prefix' => '',
                                                                                            'suffix' => '',
                                                                                            'allowNegative' => false
                                                                                          ]
                                                                                       ])->label(false);?>

          </div><!--col-sm-2--->
         </div><!--row--->
          <div class="row"><!--row--->
          <div class="col-sm-10"><!--col-sm-10--->
                    <div class="table-responsive" id="contendor_coberturas">
                       <table class="table table-striped" id="tabla_coberturas" url_action="<?=Url::to(['poliza-salud/buscar-coberturas'])?>">
                            <thead><tr><th colspan="5"></th></tr></thead>
                            <tbody id="tbodycoberturas">
                              <tr style="text-align:center">
                              <th colspan="5">
                                <div class="callout callout-info lead">
                                  <h4>No ha Seleccionado Ningun producto!</h4>
                               </div>
                             </th>
                           </tr>
                         </tbody>
                       </table>
                  </div>
          </div><!--col-sm-12--->
        </div><!--row--->
      <div>
        <?//= Html::Button(Icon::show('arrow-circle-left').' Atras', ['class' => 'btn btn-info','id' =>'atras-cobertura']) ?>
        <?= Html::Button('Siguiente '.Icon::show('arrow-circle-right'), ['class' => 'btn btn-info','id' =>'siguiente-cobertura']) ?>
     </div>
    </div><!--container-items-->
</div><!--box-body-->
</div><!--box-->
<?php
$script=<<< JS

$("select#poliza-aseguradora").on("change", function(e) {
    if($("select#poliza-producto").val()==""){
      $("#tabla_coberturas,#tabla_coberturas_consol").empty();
      $("#tabla_coberturas").html('<tr><th colspan="4" style="color:red;">Debe seleccionar un producto</th></tr>');
    }
});

$("select#poliza-producto").on("change", function(e) {
     if($(this).val()!=null){
         ensamblar_coberturas($("input:radio[name='Poliza[tipo]']:checked").val());
     }
});

$("input[name='Poliza[tipo]'").on("change", function(e) {
    var tipo= $(this).val();
    ensamblar_coberturas(tipo);
});

$(document).on("keyup", "input[id*='prima_']", function(event) {
var monto_total=$("input[id*='prima_']").sumarPorcentaje();
  $("input[id='total_prima']").val(monto_total);
});

function ensamblar_coberturas(tipo){

    $("#tabla_coberturas").empty();
    $(".field-poliza-deducible").hide();

    var producto=$("select[name='Poliza[producto]']").val();
  	var aseguradora=$('select#poliza-id_aseguradora').val();
  	var url= $("#tabla_coberturas").attr("url_action");
  	var out="";
	//tipo 0 consolidado y 1 detallado
    if(producto!=''){
            $("#tabla_coberturas").empty();
          //  $.post(url, {id_recibo:id_recibo,tipo:tipo})
            $.post(url, { aseguradora: aseguradora, producto: producto,tipo: tipo })
            .done(function(result) {
              if(result==null){
                       inicializar_coberturas(tipo,"No ha Seleccionado Ningun producto!");
               }else{
                     var obj = eval(result);
                      var disabled="disabled";
                       out+='<thead><tr><th>#</th><th>Cobertura</th><th>Suma Asegurada</th><th>Tasa(%)</th><th>Prima</th></tr></thead>';

                       $.each(obj, function(key, value) {
        		             out+='<tr><td>';
        							   out+='<input type="checkbox" id="cobBox'+value.id_cobertura+'" name="cobBox['+value.id_cobertura+']" class="checkX" data-cob="'+value.id_cobertura+'" ></td>';
        							   out+='<td><label> '+value.cobertura+' </label></td>';
        							   out+='<td><div class="input-group"><span class="input-group-addon">Bs.</span>';
        							   out+='<input type="text" name="suma['+value.id_cobertura+']" value="'+value.suma_asegurada+'" class="form-control" id="suma_'+value.id_cobertura+'" class="checkboxes" '+disabled+' ></div></td>';
        							   out+='<td><div class="input-group"><span class="input-group-addon">%</span>';
        							   out+='<input type="text" name="tasa['+value.id_cobertura+']" value="" class="form-control" id="tasa_'+value.id_cobertura+'" maxlength="5" class="checkboxes" '+disabled+' ></div></td>';
        							   out+='<td><div class="input-group"><span class="input-group-addon">Bs.</span>';
        							   out+='<input type="text" name="prima['+value.id_cobertura+']" value="'+value.prima+'" class="form-control" id="prima_'+value.id_cobertura+'" class="checkboxes" '+disabled+' ></div></td>';
        							   out+='</tr>';
                       });
                       out+='<tr><td colspan="4"></td>';
                       out+='<td><div class="input-group"><span class="input-group-addon">Total Prima Bs.</span><input type="text" name="total_prima" class="form-control" id="total_prima" disabled="disabled" size="12"></div></td></tr>';

                     $("#tabla_coberturas").html(out);
                     $(".field-poliza-deducible").show();
                     $("input[name*='cobBox'").checkboxX({threeState: false, size:'md'});
                     $("input[id*='suma_'],input[id*='tasa_'],input[id*='prima_']").maskMoney({thousands:'.', decimal:','});
                     $("input[id='total_prima']").val($("input[id*='prima_']").sumarPorcentaje());

        					  if(tipo==0){//consolidado
        						 $("input[id*='suma_'],input[id*='prima_']").removeAttr('disabled');
        						 $("input[id*='tasa_']").attr('readonly','readonly');
								     $("input[id*='tasa_']").attr('disabled','disabled');

        					  }

                }//fin else

            });//fin post-done
    }else{
         inicializar_coberturas(tipo,"No ha Seleccionado Ningun producto!");
    }
}//fin function ensamblar_coberturas(tipo,producto){

function inicializar_coberturas(tipo,mensaje){

  var dat='<thead><tr><th colspan="5"></th></tr></thead>';
     dat+='<tbody><tr style="text-align:center">';
     dat+='<th colspan="5">';
     dat+='<div class="callout callout-info lead">';
     dat+='<h4>'+mensaje+'</h4>';
     dat+='</div></th>';
     dat+='</tr></tbody>';
    $("#tabla_coberturas").html(dat);

}

var checkboxValues = "";
var suma=0;

$(".field-poliza-hi,.field-poliza-suma,.field-poliza-deducible").hide();

$('#tabla_coberturas').on('change', ".checkX", function(event) {
    // Do something on click on an existent or future .dynamicElement
    var cob = $(this).data("cob");
    //  event.preventDefault();
    if(this.checked){
       $("input[name*='["+cob+"]'").removeAttr('disabled');
    }else{
      if(cob==5){
        $("input[id='total_prima']").val("");
      }
        $("input[name*='["+cob+"]'").val("");
        $("input[name*='["+cob+"]'").attr('disabled','disabled');
    }
     //checkboxValues += this.id +",";
     //checkboxValues = checkboxValues.substring(0, checkboxValues.length-1);
     //console.log(checkboxValues);
});

$('#tabla_coberturas').on('keyup', "input[id*='tasa_']", function(event) {
    // Do something on click on an existent or future .dynamicElement

    event.preventDefault();

    var id_tasa=this.id;
    var id= id_tasa.substring(5);
    var  suma=$("input[id*='suma_"+id+"']").val();
         suma=suma.replace('.','');
         suma=suma.replace(',', '.');

    var tasa=this.value;
        tasa=tasa.replace(',', '.');


    $("input[id*='prima_"+id+"']").val(100*(suma*(tasa/100)));
    $("input[id*='prima_"+id+"']").maskMoney({thousands:'.', decimal:','});


});

 //$('input[name="cobBox[]"]').serializeArray(); //array json los objetos del arreglo

JS;
$this->registerJs($script);
$this->registerCss(".btn span.glyphicon {
                    	opacity: 0;
                    }
                    .btn.active span.glyphicon {
                    	opacity: 1;
                    }

                    /* CSS REQUIRED */
                  .state-icon {
                      left: -5px;
                  }
                  .list-group-item-primary {
                      color: rgb(255, 255, 255);
                      background-color: rgb(66, 139, 202);
                  }

                  /* DEMO ONLY - REMOVES UNWANTED MARGIN */
                  .well .list-group {
                      margin-bottom: 0px;
                  }
                  #poliza-deducible-disp{
                    width:200px;
                  }
                  ")?>
