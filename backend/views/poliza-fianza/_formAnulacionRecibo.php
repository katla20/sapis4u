<?php
use yii\helpers\Html;
//use yii\bootstrap\ActiveForm;
//use yii\bootstrap\ActiveField;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use kartik\select2\Select2;
//use kartik\depdrop\Depdrop; //http://demos.krajee.com/widget-details/depdrop
use kartik\widgets\DepDrop;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use yii\bootstrap\Tabs;
use kartik\icons\Icon;
use kartik\tabs\TabsX;
use kartik\nav\NavX;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\web\Response;
use kartik\money\MaskMoney;
use yii\bootstrap\Nav;


backend\assets\LocateAsset::register($this); //importante para incluir el main.js donde estan los plugines
if($model->fecha_anulacion){
$model->fecha_anulacion=date("d-m-Y", strtotime($model->fecha_anulacion));
}

//print_r($model);exit();
//$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');

/* @var $this yii\web\View */
/* @var $model backend\models\Poliza */
/* @var $form yii\widgets\ActiveForm */
?>
<?php yii\widgets\Pjax::begin(['id' => 'anulacion-recibo']); ?>
<div class="anulacion-form">

    <?php //$form = ActiveForm::begin(); ?>
     <?php $form = ActiveForm::begin([
    'id' => 'anulacion-recibo',
    'enableAjaxValidation' => false,
    'enableClientScript' => true,
    'enableClientValidation' => true,
    ]); ?>

	<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
    <div class="box-header">
         <h4 class="box-title"><!--<i class="glyphicon glyphicon-user"></i>--> Anular Recibo</h4>
    </div>

		<div class="box-body">
			<div class="container-items"><!-- widgetBody -->
			<div class="row">
				<div class="col-sm-5">
						<?= $form->field($model, 'motivo_anulacion',[
                                                  'addon' => [
                                                      'prepend' => [
                                                          'content' => '<i class="glyphicon glyphicon-list-alt"></i>'

                                                      ]
                                                  ]
                                              ])->textarea(['rows' => 2]) ?>
				</div>


			</div>
			<div class="row">
				<div class="col-sm-2">
                     <?/*=$form->field($model, 'fecha_anulacion',[
                                                   'addon' => [
                                                       'prepend' => [
                                                           'content' => '<i class="glyphicon glyphicon-calendar"></i>'

                                                       ]
                                                   ]
                                               ])->widget(\yii\widgets\MaskedInput::className(),
                                                                        [
                                                                        'clientOptions' => ['alias' =>  'date']
                                                                        ]) */?>
					 <?=$form->field($model, 'fecha_anulacion',[
                                             'addon' => [
                                                 'prepend' => [
                                                     'content' => '<i class="glyphicon glyphicon-calendar"></i>'

                                                 ]
                                             ]
                                         ])->widget(\yii\widgets\MaskedInput::className(),
                                                                  [
                                                                  'clientOptions' => ['alias' =>  'date']
                                                                  ])?>

                   </div>


			</div>
            <div class="row">
                    <div class="col-sm-3">

						<?php
						//echo $form->field($model, 'estatus')->dropDownList([1 => 'Activo', 0 => 'Inactivo']);
						/*echo $form->field($model, 'estatus_poliza')->widget(Select2::classname(), [
								'data' => array("Anulada"=>"Anular","Eliminada"=>"Eliminar"),
								'language' => 'es',
								//'options' => ['placeholder' => 'Seleccione un Esatdo ...'],
								'pluginOptions' => [
								'allowClear' => true
								],
						]);	*/
						?>
					</div>
    	    </div>

            <div class="row">

                <div class="col-sm-3">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Anular', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>

			</div>
		</div>
    </div>

    <?php ActiveForm::end(); ?>
  <?php yii\widgets\Pjax::end() ?>	

</div>

<?php

$string = <<<EXP2

EXP2;

$this->registerJs($string, \yii\web\View::POS_READY);
?>

<?php
Modal::begin([
      'options' => [
         'id' => 'modal',
         'tabindex' => false // important for Select2 to work properly
     ],
]);
Modal::end();

$this->registerCss("");
?>
