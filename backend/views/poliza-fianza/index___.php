<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\Poliza;
use backend\models\PolizaAnulacion;
use backend\models\Aseguradoras;
use kartik\icons\Icon;
use kartik\select2\Select2;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PolizaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//http://www.w3schools.com/colors/colors_picker.asp colores

$this->title = "";

?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
            <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
</div>
</br></br>
<div class="poliza-index">
<?  $directoryasset2 = Yii::$app->assetManager->getPublishedUrl('@backend/dist/');?> 
 

    <?php
	
	$img='<img src="'.$directoryasset2.'/img/juanvi.png" width="200" height="70"> ';
	/*echo $url=Url::to('@web/dist/img/Logo1.png', true);
	
	echo $image = '<img src="'.$url.'" width="200" />'; 
    echo Yii::$app->homeUrl;*/
	
	
  	$model = new Poliza();
  	$gridColumns = [
  	//	['class' => 'yii\grid\SerialColumn'],
    		[
    			'attribute'=>'numero_poliza',
    			'value'=>'numero_poliza',
    			'width'=>'180px',
        ],
		
    		[
              'attribute'=>'id_aseguradora',
              'width'=>'200px',
              'value'=>'aseguradora',
              'filterType'=>GridView::FILTER_SELECT2,
              'filter'=>ArrayHelper::map(Aseguradoras::find()->orderBy('nombre')->asArray()->all(), 'id_aseguradora', 'nombre'),
              'filterWidgetOptions'=>[
                'pluginOptions'=>['allowClear'=>true],
        ],
          'filterInputOptions'=>['placeholder'=>'Aseguradoras','id' => 'aseguradoras-select2-id'],
          //'group'=>true,  // enable grouping
        ],
		  [ //'class' => 'kartik\grid\DataColumn',
			'attribute'=>'estatus_poliza',
			'filterType'=>GridView::FILTER_SELECT2,
             'filter'=>ArrayHelper::map(Poliza::find()->where("poliza.estatus = :status AND poliza.id_ramo = :ramo AND poliza.id_tipo= :tipo",[':status' => 1,':ramo'=> 1,':tipo' => 1])->select(["DISTINCT (CASE WHEN poliza.estatus_poliza ='Anulada' THEN poliza.estatus_poliza
								         WHEN poliza.estatus_poliza <> 'Anulada' and poliza.fecha_vigenciahasta < NOW() THEN 'Vencida'
 										 WHEN poliza.estatus_poliza <> 'Anulada' and poliza.fecha_vigenciahasta >= NOW() THEN 'Vigente'
										 ELSE poliza.estatus_poliza END) AS estatus_poliza"])->orderBy('estatus_poliza')->asArray()->all(), 'estatus_poliza', 'estatus_poliza'),
			//'filter'=>ArrayHelper::map(Poliza::find()->orderBy('estatus_poliza')->asArray()->all(), 'estatus_poliza', 'estatus_poliza'),
			'filterWidgetOptions'=>[
			  'pluginOptions'=>['allowClear'=>true],
	        ],
			'filterInputOptions'=>['placeholder'=>'Estatus','id' => 'estatus-select2-id'],
				'vAlign'=>'middle',
				'width'=>'150px',
			'value'=>function ($model, $key, $index, $widget) {
  					if($model->estatus_poliza=='Vigente'){
							return "<span class='label label-success'>".$model->estatus_poliza .'</span>';

  					}else if($model->estatus_poliza=='Anulada'){
  					   return "<span class='label label-danger'>".$model->estatus_poliza .'</span>';

  					}else if($model->estatus_poliza=='Vencida'){
  					   return "<span class='label label-warning'>".$model->estatus_poliza .'</span>';
  					}
  			},
  			 'format'=>'raw',
  			 'noWrap'=>false,
		],
        ['class' => 'kartik\grid\DataColumn',
		   'attribute'=>'fecha_vigenciadesde',
		   'width'=>'200px',
		    'value'=>function ($model) {
                 return date("d-m-Y", strtotime($model->fecha_vigenciadesde));
            },
		   'filterType'=>GridView::FILTER_DATE,
		   'filterWidgetOptions' => [
			  'pluginOptions'=>[
			  'format' => 'dd-mm-yyyy',
			  'autoWidget' => true,
			  'autoclose' => true,
			  //'todayBtn' => true,
			  ],

			],
       ],
      ['class' => 'kartik\grid\DataColumn',
       'attribute'=>'fecha_vigenciahasta',
       'width'=>'200px',
       'value'=>function ($model) {
                 return date("d-m-Y", strtotime($model->fecha_vigenciahasta));
        },
       'filterType'=>GridView::FILTER_DATE,
       'filterWidgetOptions' => [
          'pluginOptions'=>[
          'format' => 'dd-mm-yyyy',
          'autoWidget' => true,
          'autoclose' => true,
          //'todayBtn' => true,
          ],

        ],
      ],
	  [
  		   'attribute'=>'nombre_completo',
  		   'width'=>'300px',
  		   'value'=>'nombre_completo'
      ],
      [
        'class'=>'kartik\grid\ExpandRowColumn',
        'width'=>'50px',
        'value'=>function ($model, $key, $index, $column) {
                    return GridView::ROW_COLLAPSED;
        },
        'detail'=>function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('recibos', ['poliza'=>$model->id_poliza]);
        },

        'headerOptions'=>['class'=>'kartik-sheet-style'],
        'expandOneOnly'=>true
      ],

      [
       'class' => 'yii\grid\ActionColumn',
	   'contentOptions' => ['style' => 'width:150px;'],
       'header'=>'GESTION DE RECIBOS',
       'template' => '{view} {update} {renovacion} {recibos} {anulacion}',
        'buttons' => [
                'view' => function ($url,$model, $key) {
                  return Html::a('<i class="fa fa-binoculars fa-lg" aria-hidden="true"></i>', ['poliza/view','id' => $model->id_poliza], [
                      'id' => 'activity-index-link-view',
                      'title' => Yii::t('app', 'Ver poliza'),'rel'=>'tooltip',
                      'data-pjax' => '0',
                  ]);
                },
      			   'update' => function ($url,$model, $key) {
                         return Html::a('<i class="fa fa-pencil fa-lg" aria-hidden="true"></i>', ['poliza/update','id' => $model->id_poliza], [
                             'id' => 'activity-index-link-update',
                             'title' => Yii::t('app', 'Actualizar Poliza'),'rel'=>'tooltip',
                             'data-pjax' => '0',
                         ]);
                        },

               'renovacion' => function ($url,$model, $key) {
                   return Html::a('<i class="fa fa-retweet fa-lg" aria-hidden="true"></i>', ['renovacion/update','id' => $model->id_poliza], [
                       'id' => 'activity-index-link-renovar',
                       'title' => Yii::t('app', 'Renovacion de Poliza'),'rel'=>'tooltip',
                       'data-pjax' => '0',
                   ]);
                  },
                'recibos' => function ($url,$model, $key) {
                         return Html::a('<i class="fa fa-plus fa-lg" aria-hidden="true"></i>', ['recibo/update','id' => $model->id_poliza], [
                             'id' => 'activity-index-link-recibos',
                             'title' => Yii::t('app', 'Crear Recibo Adicional'),'rel'=>'tooltip',
                             'data-pjax' => '0',
                         ]);
                 },
				       'anulacion' => function ($url,$model, $key) {
                         return Html::a('<i class="fa fa-trash fa-lg" aria-hidden="true"></i>', ['anulacion','id' => $model->id_poliza], [
                             'id' => 'activity-index-link-anular',
                             'title' => Yii::t('app','Anulacion de Poliza'),'rel'=>'tooltip',
                             'data-pjax' => '0',
							               'data-confirm'=>'Tiene '.PolizaAnulacion::CantReclamos($model->id_poliza).' Reclamos Asociada a la poliza',
                         ]);
                 },

               /*'anulacion' => function ($url,$model, $key) {
                         return Html::a('<i class="fa fa-trash fa-lg" aria-hidden="true"></i>', '#', [
                            'id' => 'activity-index-link',
                            'title' => Yii::t('app','Anulacion de Recibos'),'rel'=>'tooltip',
                            'data-toggle' => 'modal',
                            'data-target' => '#modal',
                            'data-url' => Url::to(['anulacion', 'id' => $model->id_poliza]),
                            'data-pjax' => '0',
                        ]);
               }*/
             ]
      ],

  	];
	
	
	$pdfHeader = [
	  'L' => [
		'content' => $img,//'LEFT CONTENT (HEAD)',
	  ],
	  'C' => [
		'content' => '<h2 class="panel-title" style="font-weight:bold;">'.Icon::show('car', ['class' => 'fa-2x']).'&nbsp; &nbsp;POLIZAS DE AUTOMOVIL INDIVIDUAL</h2>',
		'font-size' => 10,
		'font-style' => 'B',
		'font-family' => 'arial',
		'color' => '#333333'
	  ],
	  'R' => [
		'content' =>'Generado el: ' . date("D, d-M-Y g:i a T"),
	  ],
	  'line' => true,
	];

	$pdfFooter = [
	  'L' => [
		'content' => 'LEFT CONTENT (FOOTER)',
		'font-size' => 10,
		'color' => '#333333',
		'font-family' => 'arial',
	  ],
	  'C' => [
		'content' => 'Listado de Polizas de Automovil',
	  ],
	  'R' => [
		'content' => '[ {PAGENO} ]',
		'font-size' => 10,
		'color' => '#333333',
		'font-family' => 'arial',
	  ],
	  'line' => true,
	];

  	echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
  		'columns' => $gridColumns,
		//'showPageSummary' => true,
  		'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
  		'toolbar' => [
  			[
  				'content'=>
				    '<section class="">'.
  					Html::a('<i class="glyphicon glyphicon-plus"></i> Registrar', ['create'],[
  						//'type'=>'button',
  						'title'=>Yii::t('app', 'Registrar Poliza Automovil'),
  						'class'=>'btn btn-success'
  					]) . ' '.
  					Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
  						'class' => 'btn btn-default',
  						'title' => Yii::t('app', 'Actualizar Grid')
  					]).'</section>',

  			],
  			'{export}',
  			'{toggleData}'
  		],
  		'exportConfig' => [
  			GridView::EXCEL => [],
  			GridView::TEXT => [],
  			GridView::PDF => [
				'filename' => 'Preceptors',
				'config' => [
				  'methods' => [
					'SetHeader' => [
					  ['odd' => $pdfHeader, 'even' => $pdfHeader]
					],
					'SetFooter' => [
					  ['odd' => $pdfFooter, 'even' => $pdfFooter]
					],
				  ],
				  'options' => [
					'title' => 'Preceptors',
					'subject' => 'Preceptors',
					'keywords' => 'pdf, preceptors, export, other, keywords, here'
				  ],
				]
			],
           ],


  		'pjax' => false,
  		'pjaxSettings' => [
  			'options' => ['id' => 'poliza-pjax-id'],// UNIQUE PJAX CONTAINER ID
  		],
  		'bordered' => false,
  		'resizableColumns'=>true,
  		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
  		'striped' => true,
  		'condensed' => false,
  		'responsive' => true,
  		'hover' => true,
    	'panel' => [
          'heading'=>'<h2 class="panel-title" style="font-weight:bold;">'.Icon::show('car', ['class' => 'fa-2x']).'&nbsp; &nbsp;POLIZAS DE AUTOMOVIL INDIVIDUAL</h2>',
    			'type' => GridView::TYPE_INFO
    		],
      ]);
  	?>

</div>
<?php
$string = <<<EXP2
$("[rel=tooltip]").tooltip({ placement: 'top'});
/*$(document).on('click', '#activity-index-link', (function() {
    $.get(
        $(this).data('url'),
        function (data) {
            $('.modal-body').html(data);
            $('#modal').modal();
        }
    );
}));
$('#modal .modal-dialog').css({
     width: '50%',
     heigth: 'auto'});*/
EXP2;
$this->registerJs($string, \yii\web\View::POS_READY);
?>
<?php
$string='.nav-bar_opcion {
  position: fixed;
  top: 200;
  left: 0;
  right: 10;
  z-index: 9999;
  width: 50%;
  height: 50px;
}';
$this->registerCss($string);
?>
<?php
$string='
.fa{margin:0px 4px 0px 0px;}
 .fa-trash{
     color:  #d9534f;
 }
 .fa-plus{
     color:  #5cb85c;
 }
 .fa-retweet{
     color:  #5bc0de;
 }
 .fa-binoculars{
     color:  #39CCCC;
 }';
$this->registerCss($string);
?>
<?php
/*Modal::begin([
 'options' => [
        'id' => 'modal',
        'tabindex' => false, // important for Select2 to work properly
  ],
'header' => '<h4 class="modal-title"></h4>',
]);
Modal::end();*/
?>
