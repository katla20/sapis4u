<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Operacion;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\depdrop\Depdrop; //http://demos.krajee.com/widget-details/depdrop

/* @var $this yii\web\View */
/* @var $model backend\models\Operacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="operacion-form">
 <?php $form = ActiveForm::begin(); ?>
    <div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
    <div class="box-header with-border">
         <h4 class="box-title"><!--<i class="glyphicon glyphicon-user"></i>--> Registrar Operacion</h4>
    </div>

		<div class="box-body">
		 <div class="container-items col-sm-10"><!-- widgetBody -->

			<div class="row">
						  <div class="col-sm-6">
						   <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
						  </div>
						  <div class="col-sm-6">
							<?= $form->field($model, 'rotulo')->textInput(['maxlength' => true]) ?>
						  </div>
			</div>
			<div class="row">
						  <div class="col-sm-6">
							<?= $form->field($model, 'url')->textarea(['rows' => 1]) ?>
						  </div>
						  <div class="col-sm-6">
                          	<?php
                				echo $form->field($model,"id_padre")->widget(Select2::classname(), [

                					'data' => ArrayHelper::map(Operacion::find()->where(['etiqueta' => '1'])->all(), 'id', 'rotulo'),
                					'language' => 'en',
                					 'options' => [
                					 'placeholder' => 'Seleccione el Padre ...',
                					 ],
                					//'options' => ['placeholder' => 'Seleccione el Padre ...'],
                					'pluginOptions' => [
                					'allowClear' => true
                					 ],
                				]);

                			?>

						  </div>
			</div>
			<div class="row">
			<div class="col-sm-6">
		          <? echo $form->field($model,"etiqueta")->widget(Select2::classname(), [

								'data' => [1 => 'SI', 2 => 'NO'],
								'language' => 'en',
								 'options' => [
								 'placeholder' => 'Seleccione ...',
								 ],
								 'value' => 'SI',
								'pluginOptions' => [
								'allowClear' => true
								 ],
							]);
							?>
			</div>
			<div class="col-sm-6">
				<?= $form->field($model, 'icono')->textInput(['maxlength' => true]) ?>
			</div>
			</div>
			<div class="row">

			<div class="col-sm-6">
                <?= $form->field($model, 'orden')->textInput(['maxlength' => true]) ?>

			</div>
            <div class="col-sm-6">
				 <? echo $form->field($model,"menu")->widget(Select2::classname(), [

								'data' => [1 => 'SI', 0 => 'NO'],
								'language' => 'en',
								 'options' => [
								 'placeholder' => 'Seleccione ...',
								 ],
								 'value' => 'SI',
								'pluginOptions' => [
								'allowClear' => true
								 ],
							]);
							?>
			</div>
			</div>
			<div class="form-group">
			<?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Editar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			</div>
		  </div><!-- /.box-body -->
		</div><!-- /.box -->
    </div>
    <?//= $form->field($model, 'id_padre')->textInput() ?>





    <?php ActiveForm::end(); ?>

</div>
