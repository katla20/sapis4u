<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use backend\models\Operacion;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\OperacionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '';
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
<a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
</div>
</br></br>
<div class="operacion-index">
	<?php
	$modelOperacion = new operacion();
	$gridColumns = [
      ['class' => 'yii\grid\SerialColumn'],
      'nombre',
      'rotulo',
      'url:ntext',
      [
      'attribute'=>'id_padre',
      'width'=>'310px',
      'value'=>'idPadre.nombre',
      'filterType'=>GridView::FILTER_SELECT2,
      'filter'=>ArrayHelper::map(Operacion::find()->where('id_padre=0')->orderBy('nombre')->asArray()->all(), 'id', 'nombre'),
      'filterWidgetOptions'=>[
        'pluginOptions'=>['allowClear'=>true],
      ],
      'filterInputOptions'=>['placeholder'=>'Operacion Padre','id' => 'recibo-select2-id'],
      //'group'=>true,  // enable grouping
     ],
      [
        'class'=>'kartik\grid\BooleanColumn',
        'filterType'=>GridView::FILTER_SELECT2,
        'attribute'=>'estatus',
        'vAlign'=>'middle',
        'width'=>'100px',
      ],
      [
       'class' => 'yii\grid\ActionColumn',
       'template' => '{view} {update}',
             'buttons' => [
               'view' => function ($url,$model, $key) {
                         return Html::a('<i class="fa fa-binoculars fa-lg" aria-hidden="true"></i>', ['view','id' => $model->id], [
                             'id' => 'activity-index-link',
                             'title' => Yii::t('app', 'Ver Operacion'),'rel'=>'tooltip',
                             'data-pjax' => '0',
                         ]);
                },
                'update' => function ($url, $model, $key) {
                    return Html::a('<i class="fa fa-pencil fa-lg" aria-hidden="true"></i>', ['update', 'id' => $model->id], [
                        'id' => 'activity-index-link',
                        'title' => Yii::t('app', 'Actualizar Operacion'),'rel'=>'tooltip',
                        'data-pjax' => '0',
                    ]);
                },
             ]

      ],
	];

	echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'columns' => $gridColumns,
		'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
		'toolbar' => [
			[
				'content'=>
					Html::a('<i class="glyphicon glyphicon-plus"></i>',['create'], [
						//'type'=>'button',
						'title'=>Yii::t('app', 'Registrar Operacion'),'rel'=>'tooltip',
						'class'=>'btn btn-success'
					]) . ' '.
					Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
						'class' => 'btn btn-default',
						'title' => Yii::t('app', 'Actualizar Grid'),'rel'=>'tooltip',
					]),

			],
			'{export}',
			'{toggleData}'
		],

		'pjax' => false,
		'pjaxSettings' => [
			'options' => ['id' => 'ciudad-pjax-id'],// UNIQUE PJAX CONTAINER ID
		],
		'bordered' => false,
		'resizableColumns'=>true,
		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
		'striped' => true,
		'condensed' => false,
		'responsive' => true,
		'hover' => true,
		//'floatHeader' => true,
		//'floatHeaderOptions' => ['scrollingTop' => '50'],
		'showPageSummary' => false,
		'panel' => [
			  'heading'=>'<h2 class="panel-title" style="font-weight:bold;">'.Icon::show('car', ['class' => 'fa-2x']).'&nbsp; &nbsp;OPERACIONES DEL SISTEMA</h2>',
			'type' => GridView::TYPE_INFO
		],
    ]);
	?>


</div>
<?php
$stringJS = <<<EXP2
$("[rel=tooltip]").tooltip({ placement: 'top'});
EXP2;
$this->registerJs($stringJS, \yii\web\View::POS_READY);

$stringCSS='
 .fa{margin:0px 4px 0px 0px;}
  .fa-trash{
      color:  #d9534f;
  }
  .fa-plus{
      color:  #5cb85c;
  }
  .fa-retweet{
      color:  #5bc0de;
  }
  .fa-binoculars{
      color:  #39CCCC;
  }';
 $this->registerCss($stringCSS);
 ?>
