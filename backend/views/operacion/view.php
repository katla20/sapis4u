<?php

use yii\helpers\Html;
//use yii\widgets\DetailView;
use kartik\detail\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Operacion */

$this->title ='';

?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
            <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
            <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Operaciones del sistema')?></div></a>
            <a href="#" class="btn btn-default"><div>Detalle del la operacion</div></a>
</div>
</br></br>
<div class="operacion-view">

		<?php

	$attributes = [

    [
        'columns' => [
		    [
                'attribute'=>'id',
				'label'=>'Código',
                'format'=>'raw',
                'value'=>'<kbd>'.$model->id.'</kbd>',
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ],
            [
                'attribute'=>'nombre',
                'label'=>'Rol',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],



        ],
    ],
	[
        'columns' => [
		    [
                'attribute'=>'rotulo',
				'label'=>'Titulo',
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ],
            [
                'attribute'=>'url',
                'label'=>'Url',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],



        ],
    ],
	[
        'columns' => [
		    [
                'attribute'=>'id_padre',
				'label'=>'Padre',
                'format'=>'raw',
                'value'=> $model->idPadre->nombre,
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ],

            [
                'attribute'=>'estatus',
                'label'=>'Estatus',
                'format'=>'raw',
                'type'=>DetailView::INPUT_SWITCH,
                'widgetOptions' => [
                    'pluginOptions' => [
                        'onText' => 'Activo',
                        'offText' => 'Inactivo',
                    ]
                ],
                'value'=>($model->estatus==1)? '<span class="label label-success">Activo</span>' : '<span class="label label-danger">Inactivo</span>',
                //'valueColOptions'=>['style'=>'width:10%']
            ],

        ],
    ],
	];
//print_r($model);
	// View file rendering the widget

	echo DetailView::widget([
		'model' => $model,
		'attributes' => $attributes,
		'mode' => 'view',
		'enableEditMode'=> false,
		'responsive' => 'responsive',
		'deleteOptions'=>[ // your ajax delete parameters
			'params' => ['id' => 1000, 'kvdelete'=>true],
		],
		'panel'=>[
        'heading'=>'Operaciones',
        'type'=>DetailView::TYPE_INFO,

        ],
		'container' => ['id'=>'kv-demo'],
		//'formOptions' => ['action' => Url::current(['update' => 'kv-demo'])] // your action to delete
	]);


    ?>


</div>
