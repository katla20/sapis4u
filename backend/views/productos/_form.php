<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json; //keyla bullon
use frontend\models\Ramos;
use kartik\select2\Select2;//keyla bullon
//use kartik\depdrop\DepDrop;//http://demos.krajee.com/widget-details/depdrop
use kartik\widgets\DepDrop;
use wbraganca\dynamicform\DynamicFormWidget;
/* @var $this yii\web\View */
/* @var $model frontend\models\Ciudad */
/* @var $form yii\widgets\ActiveForm */
?>
 <?php $form = ActiveForm::begin(); ?>

<div class="productos-form">

    <div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
    <div class="box-header with-border">
         <h4 class="box-title"><!--<i class="glyphicon glyphicon-user"></i>--> Registro de Productos</h4>
    </div>

		<div class="box-body">
			<div class="container-items"><!-- widgetBody -->
				<div class="row">
					<div class="col-sm-3">
						<?php

								$id_ramo = ArrayHelper::map(Ramos::find()->all(), 'id_ramo', 'nombre');

								echo $form->field($model, 'id_ramo')->widget(Select2::classname(), [
								'data' => $id_ramo,
								'language' => 'es',
								'options' => ['placeholder' => 'Seleccione un Ramo ...'],
								'pluginOptions' => [
								'allowClear' => true
							],
						]);
						?>
					</div>
					<div class="col-sm-3">
						<?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>	
					</div>
				</div>	
				<div class="row">
					<div class="col-sm-2">
						<?php
						//echo $form->field($model, 'estatus')->dropDownList([1 => 'Activo', 0 => 'Inactivo']);
						echo $form->field($model, 'estatus')->widget(Select2::classname(), [
								'data' => array("1"=>"Activo","0"=>"Inactivo"),
								'language' => 'es',
								//'options' => ['placeholder' => 'Seleccione un Esatdo ...'],
								'pluginOptions' => [
								'allowClear' => true
								],
						]);	
						?>
					</div> 
				</div>
			
			</div>
		</div>
    </div>
	<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
		<div class="box-header with-border">
			 <h4 class="box-title"><!--<i class="glyphicon glyphicon-user"></i>--> Aseguradoras</h4>
		</div>
		<div class="box-body">
			<div class="container-items"><!-- widgetBody -->
				<div class="row">
							<div class="col-sm-6">
								 <?php
								 
									$opciones = ArrayHelper::map($taseguradoras, 'id_aseguradora', 'nombre');
									//print_r($opciones);
									echo $form->field($model, 'aseguradoras')->checkboxList($opciones, ['unselect'=>NULL]);
								?>
							</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
							<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
					</div>
				</div>
			</div>
		</div>

	</div>
 


    <?php ActiveForm::end(); ?>

</div>


