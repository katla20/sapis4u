<?php


/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json; //keyla bullon
use frontend\models\Pais;
use kartik\select2\Select2;//keyla bullon
//use kartik\depdrop\DepDrop;//http://demos.krajee.com/widget-details/depdrop
use kartik\widgets\DepDrop;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $model frontend\models\Estado */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="estado-form">

    <?php $form = ActiveForm::begin(); ?>

	<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
    <div class="box-header with-border">
         <h4 class="box-title"><!--<i class="glyphicon glyphicon-user"></i>--> Registro de Estado</h4>
    </div>

		<div class="box-body">
			<div class="container-items"><!-- widgetBody -->
				<div class="row">
					<div class="col-sm-3">
						<?php

								$id_pais = ArrayHelper::map(Pais::find()->all(), 'id_pais', 'nombre');

								echo $form->field($model, 'id_pais')->widget(Select2::classname(), [
								'data' => $id_pais,
								'language' => 'es',
								'options' => ['placeholder' => 'Seleccione una Pais ...'],
								'pluginOptions' => [
								'allowClear' => true
							],
						]);
						?>
					</div>
					<div class="col-sm-3">
						<?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>	
					</div>
				</div>	
				<div class="row">
					<div class="col-sm-2">
						<?php
						//echo $form->field($model, 'estatus')->dropDownList([1 => 'Activo', 0 => 'Inactivo']);
						echo $form->field($model, 'estatus')->widget(Select2::classname(), [
								'data' => array("1"=>"Activo","0"=>"Inactivo"),
								'language' => 'es',
								//'options' => ['placeholder' => 'Seleccione un Esatdo ...'],
								'pluginOptions' => [
								'allowClear' => true
								],
						]);	
						?>
					</div> 
				</div>
			
				<div class="row">
					<div class="col-sm-6">
						 <?= $form->field($model, 'region')->textInput(['maxlength' => true]) ?>
					</div> 
				</div>
				<div class="row">
					<div class="col-sm-6">
						 <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>
					</div> 
					
				</div>
				
				<div class="row">
					<div class="col-sm-6">
						<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
					</div>
				</div>

			
			</div>
		</div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
