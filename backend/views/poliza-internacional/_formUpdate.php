<?php
use yii\helpers\Html;
//use yii\bootstrap\ActiveForm;
//use yii\bootstrap\ActiveField;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use kartik\select2\Select2;
//use kartik\depdrop\Depdrop; //http://demos.krajee.com/widget-details/depdrop
use kartik\widgets\DepDrop;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use yii\bootstrap\Tabs;
use kartik\icons\Icon;
use kartik\tabs\TabsX;
use kartik\nav\NavX;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\web\Response;
use kartik\money\MaskMoney;
use backend\models\Aseguradoras;
use backend\models\InterAsegRamo;
use backend\models\InterAsegProducto;
use backend\models\Productos;
use backend\models\Cliente;
use backend\models\Persona;
use backend\models\Intermediario;
use backend\models\Automovil;
use backend\models\Version;
use backend\models\Modelo;
use backend\models\Marca;
use yii\bootstrap\Nav;




/* @var $this yii\web\View */
/* @var $model backend\models\Poliza */
/* @var $form yii\widgets\ActiveForm */

$model->fecha_vigenciadesde=date("d-m-Y", strtotime($model->fecha_vigenciadesde));
$model->fecha_vigenciahasta=date("d-m-Y", strtotime($model->fecha_vigenciahasta));
?>
<?php $form = ActiveForm::begin(['id' => 'poliza-form','enableAjaxValidation' =>true,'enableClientValidation' => false,'validateOnSubmit' => true]);?>
<div class="poliza-form"><!--INICIO DEL MAQUETADO-->
    <div class="nav-stacked"><!--nav-stacked-->
               <div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
               -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
               box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);"><!--INICIO BOX-PANEL 1-->
               <div class="box-header">
                    <h4 class="box-title"><i class="glyphicon glyphicon-user"></i> Datos de la Poliza</h4>
               </div>
               <div class="box-body">
                  <div class="container-items"><!-- widgetBody -->
                    <div class="row"><!--INICIO ROW-->
                       <div class="col-sm-5">
                            <?=$form->field($model,"id_aseguradora",[
                                                          'addon' => [
                                                              'prepend' => [
                                                                  'content' => '<i class="fa fa-university"></i>'

                                                              ]
                                                          ]
                                                      ])->widget(Select2::classname(), [
                                 'data' =>   ArrayHelper::map(Aseguradoras::find()->joinwith('interAsegRamos')
                                                   ->where('inter_aseg_ramo.id_ramo = :ramo and aseguradoras.estatus=:status')
                                                   ->addParams([':ramo' => 1])
                                                   ->addParams([':status' => 1])
                                                   ->all(),'id_aseguradora', 'nombre'),
                                 'language' => 'en',
                                  'options' => ['placeholder' => 'Seleccione ...'],
                                  'pluginOptions' => [
                                     'allowClear' => true
                                 ],
                               ]);
                             ?>
                      </div>
                      <?php
                      if (!$model->isNewRecord) {
                           echo Html::hiddenInput('productoH', $model->producto, ['id'=>'productoH']);
                      }

                       ?>

                     <div class="col-sm-5">
                                 <?=$form->field($model, "producto",[
                                                               'addon' => [
                                                                   'prepend' => [
                                                                       'content' => '<i class="fa fa-cart-plus"></i>'

                                                                   ]
                                                               ]
                                                           ])->widget(DepDrop::classname(), [
                                                        'data'=> [''=>'Seleccione '],
                                                        'options'=>['placeholder'=>'Selecione ...'],
                                                        'type' => DepDrop::TYPE_SELECT2,
                                                        'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                                                        'pluginOptions'=>[
                                                        'depends'=>["poliza-id_aseguradora"],
                                                        'initialize'=>true,
                                                        'placeholder' => 'Seleccione ...',
                                                           'url'=>Url::to(['dependent-dropdown/child-account-join', 'db'=>Productos::classname(),'join'=>'interAsegProductos','cmpo_dep'=>'inter_aseg_producto.id_aseguradora','cmpo_mostrar'=>'productos.nombre','id_cmpo'=>'productos.id_producto','cmpo_adic'=>'productos.id_ramo','valor_adic'=>1]),
                                                        'params'=>['productoH'] ///SPECIFYING THE PARAM
                                                         ]

                                                 ]);
                                ?>
                     </div>
               </div><!--FIN ROW-->
               <div class="row"><!--INICIO ROW-->
                   <div class="col-sm-2">
                    <?= $form->field($model, 'numero_poliza',[
                                                  'addon' => [
                                                      'prepend' => [
                                                          'content' => '<i class="glyphicon glyphicon-list-alt"></i>'

                                                      ]
                                                  ]
                                              ])->textInput(['maxlength' => true]) ?>

                   </div>
                   <div class="col-sm-2">
                     <?=$form->field($model, 'fecha_vigenciadesde',[
                                                   'addon' => [
                                                       'prepend' => [
                                                           'content' => '<i class="glyphicon glyphicon-calendar"></i>'

                                                       ]
                                                   ]
                                               ])->widget(\yii\widgets\MaskedInput::className(),
                                                                        [
                                                                        'clientOptions' => ['alias' =>  'date']
                                                                        ]) ?>

                   </div>
                   <div class="col-sm-2">
                      <?=$form->field($model, 'fecha_vigenciahasta',[
                                                    'addon' => [
                                                        'prepend' => [
                                                            'content' => '<i class="glyphicon glyphicon-calendar"></i>'

                                                        ]
                                                    ]
                                                ])->widget(\yii\widgets\MaskedInput::className(),
                                                                         [
                                                                         'clientOptions' => ['alias' =>  'date']
                                                                         ]) ?>
                   </div>
               </div><!--FIN ROW-->
               <div class="row"><!--INICIO ROW-->
                 <?php Pjax::begin();?>
                   <div class="col-sm-4" id="w0">
                    <?=$form->field($model,'id_contratante', [
                                                                  'addon' => [
                                                                      'prepend' => ['content'=>'<i class="glyphicon glyphicon-user"></i>'],
                                                                      'append'=>['content'=>Html::a('<i class="fa fa-user-plus"></i> Nuevo', '#', [
                                                                                                     'id' => 'activity-index-link',
                                                                                                     'class' => 'btn btn-success',
                                                                                                     'data-toggle' => 'modal',
                                                                                                     'data-target' => '#modal',
                                                                                                     'data-url' => Url::to(['persona/create'], true),
                                                                                                     'data-pjax' => '0',
                                                                                                 ]), 'asButton'=>true],
                                                                  ]
                                           ])->widget(Select2::classname(), [
                                                       'data' =>ArrayHelper::map( Cliente::find()
                                                                                    ->joinwith('idPersona')
                                                                                    ->where("cliente.estatus = :status", [':status' => 1])
                                                                                    ->select("(nombre||' '||segundo_nombre||' '||apellido|| ' ' ||segundo_apellido || ' ' || identificacion) AS nombre_completo ,id_cliente")
                                                                                    ->all(),'id_cliente', 'nombre_completo'),
                                                       'language' => 'en',
                                                        'options' => ['placeholder' => 'Seleccione ...','id'=>'id_contratante'],
                                                        'pluginOptions' => [
                                                           'allowClear' => true
                                                       ],
                                       ]);
                     ?>



                   </div>
                   <?php Pjax::end(); ?>
                     <?php
                      $model->id_contratante_hidden=$model->id_contratante;
                      echo $form->field($model, 'id_contratante_hidden')->hiddenInput()->label(false);?>
                     <div class="col-sm-6" id="datoscontratante"></div>
                </div><!--FIN ROW-->
        </div><!--fin  widgetBody -->
        <div class="form-group" style="float:right"><!--INICIO BOTONES DEL PANEL-->
        <?= Html::submitButton(Icon::show('save').'[Registrar Poliza]', ['class' => 'btn btn-success']) ?>
          <?= Html::resetButton(Icon::show('eraser').'Limpiar Campos', ['class' => 'btn btn-default']) ?>
          </div><!--FIN BOTONES DEL PANEL-->
      </div><!--FIN BOX BODY-->

      </div><!--FIN BOX-PANEL 1-->
    </div><!--FIN TABBABLE-->
  </div><!--FIN DEL MAQUETADO-->
  <?php ActiveForm::end();?>

<?php

$string = <<<EXP2

sumaFecha = function(d, fecha)
{
 var Fecha = new Date();
 var sFecha = fecha || (Fecha.getDate() + "/" + (Fecha.getMonth() +1) + "/" + Fecha.getFullYear());
 var sep = sFecha.indexOf('/') != -1 ? '/' : '-';
 var aFecha = sFecha.split(sep);
 var fecha = aFecha[2]+'/'+aFecha[1]+'/'+aFecha[0];
 fecha= new Date(fecha);
 fecha.setDate(fecha.getDate()+parseInt(d));
 var anno=fecha.getFullYear();
 var mes= fecha.getMonth()+1;
 var dia= fecha.getDate();
 mes = (mes < 10) ? ("0" + mes) : mes;
 dia = (dia < 10) ? ("0" + dia) : dia;
 var fechaFinal = dia+sep+mes+sep+anno;
 return (fechaFinal);
 }


$("input[name='Poliza[fecha_vigenciadesde]']").change(function(){
      $("input[name='Poliza[fecha_vigenciahasta]']").val(sumaFecha(365,$(this).val()));
});//$("input[name='Poliza[fecha_vigenciadesde]']")

$('#id_contratante').change(function(){
  var out="";
  var id =$("#id_contratante").val();
  $('#poliza-id_contratante_hidden').val(id);
  $("#datoscontratante").empty();

     $.post("buscar-cliente", { id: id })
     .done(function(result) {
       var datos=result[0];
        if(datos!=null){
              $("#datoscontratante").empty();
                  var out='<div class="datagrid"><table class="table table-condensed"><thead><tr><th>Nombre Completo</th><th>Identificacion</th></tr></thead>';
                      out+='<tbody><tr><td>'+datos.nombre_completo+'</td><td>'+datos.identificacion+'</td></tr></tbody></table></div>';
         }
    $("#datoscontratante").html(out);
  });//.donefunction(result)



});//fin change

$('#id_automovil').change(function(){
  var id=$(this).val();
  var out="";
  var id =$("#id_automovil").val();
  $('#poliza-id_automovil_hidden').val(id);
  $("#datosautomovil").empty();

  $.post("buscar-auto", { id: id })
  .done(function(result) {
    var datos=result[0];
     if(datos!=null){
       var out='<div class="datagrid"><table class="table table-condensed"><thead><tr><th>Placa</th><th>Modelo</th><th>Version</th><th>Año</th></tr></thead>';
          out+='<tbody><tr><td>'+datos.placa+'</td><th>'+datos.nombre+'</th><th>Version</th><th>'+datos.anio+'</th></tr></tbody></table></div>';
      }
     $("#datosautomovil").html(out);
  });//.donefunction(result)

});//fin change

$('#id_vendedor').change(function(){
  var id=$(this).val();
  $('#poliza-id_vendedor_hidden').val(id);
  $("#datosvendedor").empty();
     $.post("buscar-vendedor", { id: id })
     .done(function(result) {
       var datos=result[0];
        if(datos!=null){
              $('#poliza-comision_porcentaje').val(datos.comision);
              $('#poliza-comision_vendedor').val(parseFloat((datos.comision/100)*$('#poliza-comision_total').val()).toFixed(2));
              var out='<div class="datagrid"><table class="table table-condensed"><thead><tr><th>Identificacion</th><th>Nombre Completo</th></tr></thead>';
                  out+='<tbody><tr><td>'+$("#id_vendedor option:selected").html()+'</td><td>hgghgh</td></tr></tbody></table></div>';
              $("#datosvendedor").html(out);
        }
     });//.donefunction(result)
});//fin change

$( "input[name='poliza-comision_total-disp']").keyup(function(event) {

  event.preventDefault();
  var porcentaje=0;
  var comision=0;
  var valor=$(this).val();
      valor=valor.replace('.', '');
      valor=parseFloat(valor.replace(',', '.'));

  if($('#poliza-comision_porcentaje').val()!=''){
      porcentaje=parseFloat($('#poliza-comision_porcentaje').val()/100);
  }

    $('#poliza-comision_vendedor').val(parseFloat(valor*porcentaje).toFixed(2));
});


$("#tab-cobertura,#tab-comision").children('a').on("click", function(e) {//DESABILITANDO EL EVENTO CLICK DEL ANCHOR DEL TAB DE RECIBO
    //e.stopImmediatePropagation();
	e.preventDefault();
});

$("a[href='#tabs-pane-poliza'],a[href='#tabs-pane-recibo']").on("click", function(e) {//DESABILITANDO EL EVENTO CLICK DEL ANCHOR DEL TAB PRO

    e.preventDefault();
    e.stopImmediatePropagation();

});




$("#siguiente-auto").on("click", function(e) {
    console.log('0');

    //$("#tab-cobertura,").children('a').prop('disabled', true);//PARA DOCUMENTACION

     $("#tab-cobertura").addClass('active');
	 $("#tab-cobertura").removeClass('disabled');

     $("#tab-auto").addClass('disabled');
     $("#w2-tab1").addClass("in active");
     $("#w2-tab0").removeClass('in active');

});

$("#atras-cobertura").on("click", function(e) {

    // $("#tab-cobertura").addClass('disabled');
	//$("#tab-auto").removeClass('disabled');


	 $("#tab-cobertura").removeClass('disabled');
	 $("#tab-cobertura").addClass('active');
     $("#w2-tab1").removeClass('in active');
     $("#w2-tab0").addClass("in active");

});

$("#siguiente-cobertura").on("click", function(e) {
	  // $("#tab-cobertura").addClass('disabled');

     $("#tab-comision").removeClass('disabled');
     $("#tab-comision").addClass('active');
     $("#w2-tab2").addClass("in active");
     $("#w2-tab1").removeClass('in active');

});

$("#atras-comision").on("click", function(e) {

    // $("#tab-comision").addClass('disabled');
    // $("#tab-cobertura").removeClass('disabled');
     $("#w2-tab1").addClass("in active");
     $("#w2-tab2").removeClass('in active');

});

//TAB PRINCIPAL PARA SELECCIONAR POLIZA Y RECIBO

$("#siguiente-recibo").on("click", function(e) {

     $("#tab-poliza").removeClass('disabled');//para inicializar en caso de que se duplique


	 $("#tab-poliza").removeClass('active');
	 $("#tab-poliza").addClass('disabled');
	 $("#tabs-pane-poliza").removeClass("in active");


	 $("#tab-recibo").removeClass('disabled');
     $("#tab-recibo").addClass('active');
	 $("#tabs-pane-recibo").addClass("in active");





});

$("#atras-poliza").on("click", function(e) {

	 $("#tab-recibo").removeClass('disabled');//para inicializar en caso de que se duplique

	 $("#tab-recibo").removeClass('active');
	 $("#tab-recibo").addClass('disabled');
	 $("#tabs-pane-recibo").removeClass("in active");


	 $("#tab-poliza").removeClass('disabled');
     $("#tab-poliza").addClass('active');
	 $("#tabs-pane-poliza").addClass("in active");

});


EXP2;


$this->registerJs($string, \yii\web\View::POS_READY);

$this->registerJs(
    "$(document).on('click', '#activity-index-link', (function() {
        $.get(
            $(this).data('url'),
            function (data) {
                $('.modal-body').html(data);
                $('#modal').modal();
            }
        );
    }));"
);
?>

<?php
Modal::begin([
      'options' => [
         'id' => 'modal',
         'tabindex' => false // important for Select2 to work properly
     ],
    //'header' => '<h4 class="modal-title">Crear Cliente</h4>',
    //'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Cerrar</a>',
]);

//echo "<div class='well'></div>";

Modal::end();

$string="body{overflow-y: scroll;
              overflow-x: hidden;
         }
         body{overflow-y: hidden;
              overflow-x: scroll;
         }
         .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
             color:black;
           background-color:#72AFD2;
         }

         .datagrid table { border-collapse: collapse; text-align: left; width: 100%; } .datagrid {font: normal 12px/150% Arial, Helvetica, sans-serif; background: #fff; overflow: hidden; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; }.datagrid table td, .datagrid table th { padding: 3px 10px; }.datagrid table thead th {background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #B2B9BF), color-stop(1, #B2B8BF) );background:-moz-linear-gradient( center top, #B2B9BF 5%, #B2B8BF 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#B2B9BF', endColorstr='#B2B8BF');background-color:#B2B9BF; color:#FFFFFF; font-size: 13px; font-weight: bold; border-left: 0px solid #EEF6FF; } .datagrid table thead th:first-child { border: none; }.datagrid table tbody td { color: #8D9296; border-left: 1px solid #EEF6FF;font-size: 12px;font-weight: bold; }.datagrid table tbody .alt td { background: #CCD3DB; color: #5F6366; }.datagrid table tbody td:first-child { border-left: none; }.datagrid table tbody tr:last-child td { border-bottom: none; }";
$this->registerCss($string);
?>
