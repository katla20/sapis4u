<?php
use yii\helpers\Html;
//use yii\bootstrap\ActiveForm;
//use yii\bootstrap\ActiveField;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use kartik\select2\Select2;
//use kartik\depdrop\Depdrop; //http://demos.krajee.com/widget-details/depdrop
use kartik\widgets\DepDrop;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use yii\bootstrap\Tabs;
use kartik\icons\Icon;
use kartik\tabs\TabsX;
use kartik\nav\NavX;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\web\Response;
use kartik\money\MaskMoney;
use backend\models\Aseguradoras;
use backend\models\InterAsegRamo;
use backend\models\InterAsegProducto;
use backend\models\Productos;
use backend\models\Cliente;
use backend\models\Persona;
use backend\models\Intermediario;
use backend\models\CargaFamiliar;
use backend\models\Pais;
use backend\models\CargaFamiliarView;
use kartik\checkbox\CheckboxX;
use kartik\widgets\SwitchInput;

backend\assets\LocateAsset::register($this); //importante para incluir el main.js donde estan los plugines
?>
<?php $form = ActiveForm::begin(['id' => 'poliza-form','enableAjaxValidation' =>false]);?>
<div class="poliza-form">
        <div class="tab-pane active" id="tabs-pane-poliza"><!--FORMULARIO DE POLIZA-->
          <div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
          -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
          box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
          <div class="box-header">
               <h4 class="box-title"><i class="glyphicon glyphicon-user"></i> Datos de la Poliza</h4>
          </div>
          <div class="box-body">
        	    <div class="container-items"><!-- widgetBody -->
                <div class="row">
                   <div class="col-sm-5">
                        <?=$form->field($model,"id_pais",[
                                                      'addon' => [
                                                          'prepend' => [
                                                              'content' => '<i class="fa fa-globe"></i>'

                                                          ]
                                                      ]
                                                  ])->widget(Select2::classname(), [
                                                    'data' =>ArrayHelper::map(Pais::find()->joinwith('aseguradoras')
                                               ->where('aseguradoras.estatus=:status AND aseguradoras.id_pais!=:id_pais')
                                               //->addParams([':ramo' => 2])
                                               ->addParams([':id_pais' => 10])
                                               ->addParams([':status' => 1])
                                               ->all(),'id_pais', 'nombre'),
                             'language' => 'en',
                              'options' => ['placeholder' => 'Seleccione el Pais'],
                              'pluginOptions' => [
                                 'allowClear' => true
                             ],
                           ]);
                         ?>
                  </div>
               </div>
                <div class="row">
                   <div class="col-sm-5">
                               <?=$form->field($model, "id_aseguradora",[
                                                             'addon' => [
                                                                 'prepend' => [
                                                                     'content' => '<i class="fa fa-university"></i>'

                                                                 ]
                                                             ]
                                                         ])->widget(DepDrop::classname(), [
                                           										'data'=> [''=>'Seleccione '],
                                           										'options'=>['placeholder'=>'Selecione ...'],
                                           										'type' => DepDrop::TYPE_SELECT2,
                                           										'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                                           										'pluginOptions'=>[
                                           										'depends'=>["poliza-id_pais"],
                                           										'placeholder' => 'Seleccione la Aseguradora',
                                                                'url'=>Url::to(['dependent-dropdown/child-account', 'db'=>Aseguradoras::classname(),'cmpo_dep'=>'id_pais','cmpo_mostrar'=>'nombre','id_cmpo'=>'id_aseguradora']),
                                           									   ]
               									               ]);
               								?>
                   </div>
                   <div class="col-sm-5">
                               <?=$form->field($model, "producto",[
                                                             'addon' => [
                                                                 'prepend' => [
                                                                     'content' => '<i class="fa fa-cart-plus"></i>'

                                                                 ]
                                                             ]
                                                         ])->widget(DepDrop::classname(), [
                                   										'data'=> [''=>'Seleccione '],
                                   										'options'=>['placeholder'=>'Selecione ...'],
                                   										'type' => DepDrop::TYPE_SELECT2,
                                   										'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                                   										'pluginOptions'=>[
                                   										'depends'=>["poliza-id_aseguradora"],
                                   										'placeholder' => 'Seleccione ...',
                                   										  'url'=>Url::to(['dependent-dropdown/child-account-join', 'db'=>Productos::classname(),'join'=>'interAsegProductos','cmpo_dep'=>'inter_aseg_producto.id_aseguradora','cmpo_mostrar'=>'productos.nombre','id_cmpo'=>'productos.id_producto','cmpo_adic'=>'productos.id_ramo','valor_adic'=>2]),
                                   									   ]
               									               ]);
               								?>
                   </div>
         </div>
        <div class="row">
           <div class="col-sm-2">
             <?=$form->field($model,"tipo_recibo",[
                                           'addon' => [
                                               'prepend' => [
                                                   'content' => '<i class="fa fa-university"></i>'

                                               ]
                                           ]
                                       ])->widget(Select2::classname(), [
                                                  'data' => ['Nuevo' => 'Nuevo', 'Renovacion' => 'Renovacion'] ,
                                                  'language' => 'en',
                                                   'options' => ['placeholder' => 'Seleccione ...'],
                                                   'pluginOptions' => [
                                                      'allowClear' => true
                                                  ],
                                                ]);
              ?>
            </div>
            <div class="col-sm-2">
             <?= $form->field($model, 'numero_poliza',[
                                           'addon' => [
                                               'prepend' => [
                                                   'content' => '<i class="glyphicon glyphicon-list-alt"></i>'

                                               ]
                                           ]
                                       ])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-2">
              <?=$form->field($model, 'fecha_vigenciadesde',[
                                            'addon' => [
                                                'prepend' => [
                                                    'content' => '<i class="glyphicon glyphicon-calendar"></i>'

                                                ]
                                            ]
                                        ])->widget(\yii\widgets\MaskedInput::className(),
                                                                 [
                                                                 'clientOptions' => ['alias' =>  'date']
                                                                 ]) ?>
            </div>
            <div class="col-sm-2">
               <?=$form->field($model, 'fecha_vigenciahasta',[
                                             'addon' => [
                                                 'prepend' => [
                                                     'content' => '<i class="glyphicon glyphicon-calendar"></i>'

                                                 ]
                                             ]
                                         ])->widget(\yii\widgets\MaskedInput::className(),
                                                                  [
                                                                  'clientOptions' => ['alias' =>  'date']
                                                                  ]) ?>
            </div>
           </div>
        <div class="row">
            <div class="col-sm-5">
              <div class="nonboxy-widget">
                 <div class="widget-content">
                    <?php Pjax::begin(['id' => 'pjax-persona']);?>
                     <div class="widget-box">
                        <fieldset>
                          <legend><h4><i class="glyphicon glyphicon-user"></i> Contratante</h4></legend>
                          <?php
                                echo $form->field($model,'id_contratante', [
                                                           'addon' => [
                                                               'prepend' => ['content'=>'<i class="glyphicon glyphicon-user"></i>'],
                                                               'append'=>['content'=>Html::a('<i class="fa fa-user-plus"></i> Nuevo', '#', [
                                                                                              'id' => 'm_contratante',
                                                                                              'class' => 'btn btn-success',
                                                                                              'data-toggle' => 'modal',
                                                                                              'data-target' => '#modal',
                                                                                              'data-url' => Url::to(['persona/create'], true),
                                                                                              'data-pjax' => '0',
                                                                                          ]), 'asButton'=>true],
                                                           ]
                                    ])->widget(Select2::classname(), [
                                                'data' =>ArrayHelper::map( Cliente::find()
                                                                             ->joinwith('idPersona')
                                                                             ->where("cliente.estatus = :status", [':status' => 1])
                                                                             ->select("(nombre||' '||apellido|| ' ' || ' ' || identificacion) AS nombre_completo ,id_cliente")
                                                                             ->all(),'id_cliente', 'nombre_completo'),
                                                'language' => 'en',
                                                 'options' => ['placeholder' => 'Seleccione el Contratante','id'=>'id_contratante'],
                                                 'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                ])->label(false);
                            ?>

                        </fieldset>
                    </div>
              <?php Pjax::end(); ?>
              <div>
              <label>Es titular?</label>
              <input type="checkbox" id="isTitular" name="isTitular" class="checkX">
              <?/*=$form->field($model, 'isTitular')->widget(SwitchInput::classname(),[
                                              'type' => SwitchInput::CHECKBOX,
                                              'inlineLabel' => true,
                                              'pluginOptions' => [
                                                'onText' => 'SI',
                                                'offText' => 'NO',
                                               ]
                                            ]);*/?>
             </div>
             </div></div></div>
            <div class="col-sm-5">
              </br></br></br>
              <p id="datoscontratante"></p>
              <?=$form->field($model, 'id_contratante_hidden')->hiddenInput()->label(false);?>
            </div>
            </div>
            <div class="row">
            <div class="col-sm-10">
                  <div class="nonboxy-widget">
                    <div class="widget-content">
                         <div class="widget-box">
                           <fieldset>
                              <legend><h4><?=Icon::show('group')?>Asegurados <span class="lead text-yellow">Importante: * debe agregar inicialmente al titular para añadir mas asegurados</span></h4></legend>
                              <div class="row">
                                <?php Pjax::begin(['id' => 'pjax-asegurados']);?>
								                   <div class="col-sm-7">
											               <?=$form->field($model, "asegurados",[
																		   'addon' => [
																			   'prepend' => ['content'=>'<i class="glyphicon glyphicon-user"></i>'],
																			   'append'=>['content'=>
            															 '<div class="ui-group-buttons">
            																		 <button type="button" class="btn btn-primary" id="m_carga_familiar" data-toggle="modal" data-target="#modal2" data-pjax="0" data-url="'.Url::to(['persona/carga-familiar'], true).'" data-urlup="'.Url::to(['persona/carga-familiar-update'], true).'" data-depend="#id_contratante"><i class="fa fa-user-plus"></i> Familia</button>
            																		 <div class="or"></div>
            															  </div>'.
              															Html::a('<i class="fa fa-user-plus"></i> Otro', '#', [
              																													 'id' => 'm_cliente',
              																													 'class' => 'button btn btn-success',
              																													 'data-toggle' => 'modal',
              																													 'data-target' => '#modal',
              																													 'data-url' => Url::to(['persona/create'], true),
              																													 'data-pjax' => '0',
              															]).Html::a('<i class="fa fa-user-plus"></i> Titular', '#', [
                																													 'id' => 'm_titular',
                																													 'class' => 'button btn btn-success',
                																													 'data-toggle' => 'modal',
                																													 'data-target' => '#modal',
                																													 'data-url' => Url::to(['persona/create'], true),
                																													 'data-pjax' => '0',
                														]), 'asButton'=>true],
              																							   ]
              																						   ])->widget(Select2::classname(), [
              																	 'data' =>ArrayHelper::map( Cliente::find()
              																								  ->joinwith('idPersona')
              																								  ->where("cliente.estatus = :status", [':status' => 1])
              																								  ->select("(nombre||' '||apellido||' '|| identificacion) AS nombre_completo ,id_cliente")
              																								  ->all(),'id_cliente', 'nombre_completo'),
																	 'language' => 'en',
																	  'options' => ['placeholder' => 'Seleccione ...','id'=>'id_asegurado'],
																	  'pluginOptions' => [
																		 'allowClear' => true
																	 ],
														  ])->label(false);?>
                              </div>
                              <?php Pjax::end(); ?>
                              <div class="col-sm-2"><?= Html::a(Icon::show('plus').' Agregar al listado de asegurados ','#', ['class'=>'btn btn-primary','id' => 'add-asegurados', 'data-url' => Url::to(['poliza-salud/buscar-asegurados'], true),]) ?></div>
                              </div>
                             </div>
                             <div class="datagrid">
                               <table class="table table-condensed" id="table-asegurados">
                                 <thead>
                                 <tr><th>Cedula</th><th>Nombres y Apellidos</th><th>Edad</th><th>Parentezco</th><th>Operacion</th></tr>
                               </thead>
                               <tbody></tbody>
                               </table>
                             </div>
                          </fieldset>
                         </div>
                    </div>
                </div>

            </div>
            </div><!--CONTAINER-->
            <div class="form-group"><!--INICIO BOTONES DEL PANEL-->
               <?= Html::Button('RECIBO '.Icon::show('arrow-circle-right'), ['class' => 'btn btn-primary','id' =>'siguiente-recibo']) ?>
            </div><!--FIN BOTONES DEL PANEL-->
            </div><!--BOX BODY-->
        </div><!--BOX-->
    </div><!--FIN FORMULARIO DE POLIZA-->
    <div class="tab-pane ocultar" id="tabs-pane-recibo"><!--INICIO FORMULARIO DE RECIBO-->
      <div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
      -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
      box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
      <div class="box-header">
           <h4 class="box-title"><?=Icon::show('newspaper-o')?>Datos del Recibo</h4>
      </div>
        <div class="box-body">
            <div class="container-items"><!-- widgetBody -->
              <div class="row">
                     <div class="col-sm-3">
                       <?= $form->field($model, 'nro_recibo',[
                                                     'addon' => [
                                                         'prepend' => [
                                                             'content' => '<i class="glyphicon glyphicon-list-alt"></i>'
                                                         ]
                                                     ]
                                                 ])->textInput(['maxlength' => true]) ?>
                     </div>
                     <div class="col-sm-3">
                       <?= $form->field($model, 'nro_certificado',[
                                                     'addon' => [
                                                         'prepend' => [
                                                             'content' => '<i class="glyphicon glyphicon-list-alt"></i>'
                                                         ]
                                                     ]
                                                 ])->textInput(['maxlength' => true]) ?>
                     </div>
              </div>
             <div class="row">
                <div class="col-sm-12">
                 <?php
                    $items = [
                        [
                            'label'=>'Paso 1 - '.Icon::show('group').' Cargar los Beneficiarios',
                            'content'=>$this->render('beneficiarios', ['model' => $model, 'form' => $form]),
                            'headerOptions' => ['style'=>'font-weight:bold','id' => 'tab-benef'],
                        ],
                        [
                            'label'=>' Paso 2 Coberturas <i class="glyphicon glyphicon-chevron-right"></i>',
                            'encode'=>false,
                            'content'=>$this->render('coberturas_det', ['model' => $model, 'form' => $form]),
                            'headerOptions' => ['class'=>'disabled','id' => 'tab-cobertura'],
                        ],
                        [
                            'label'=>'Paso 3 - '.Icon::show('user').' Cargar las Comisiones y el Vendedor',
                            'encode'=>false,
                            'content'=>$this->render('vendedor', ['model' => $model, 'form' => $form]),
                            'headerOptions' => ['class'=>'disabled','id' => 'tab-comision'],
                        ],
                    ];

                    echo TabsX::widget([
                        'position' => TabsX::POS_ABOVE,
                        'align' => TabsX::ALIGN_LEFT,
                        'items' => $items,
                        'encodeLabels'=>false,
                        'options' => ['class' =>'nav nav-pills'], // set this to nav-tab to get tab-styled navigation
                    ]);

                    ?>
                  </div>
              </div>
              <div class="form-group" style="float:left"><!--INICIO BOTONES DEL PANEL-->
   					   <?= Html::Button(Icon::show('arrow-circle-left').' POLIZA', ['class' => 'btn btn-primary','id' =>'atras-poliza']) ?>
   				    </div><!--FIN BOTONES DEL PANEL-->
              <div class="form-group" style="float:right"><!--INICIO BOTONES DEL PANEL-->
   					   <?= Html::submitButton(Icon::show('save').'[Registrar Poliza]', ['class' => 'btn btn-success']) ?>
   				     <?= Html::resetButton(Icon::show('eraser').'Limpiar Campos', ['class' => 'btn btn-default']) ?>
   				     </div><!--FIN BOTONES DEL PANEL-->
            </div><!--CONTAINER-->

          </div><!--BOX BODY-->
        </div><!--BOX-->
</div><!--FIN FORMULARIO DE RECIBO-->

    <?php ActiveForm::end();?>
<?php



$string = <<<EXP2

$('#m_cliente,#m_carga_familiar,.or').hide();

$('#isTitular').change(function() {
  var out='';
     if($('#poliza-id_contratante_hidden').val()==''){
        alert('Debe seleccionar el Contratante');
        $('#isTitular').removeAttr('checked');

     }else{
          $("#table-asegurados").children("tbody").empty();
          if($(this).prop('checked')==true){
             $.get(
                 $('#add-asegurados').data('url'),{id:$('#poliza-id_contratante_hidden').val()},
                 function (result) {
                    // console.log(result);
                      out+='<tr id="'+result[0].identificacion+'"><td><input type="hidden" name="asegurado['+result[0].id_cliente+']" id="asegurado" value="'+result[0].id_cliente+'">'+result[0].identificacion+'</td><td>'+result[0].nombre_completo+'</td><td>'+result[0].anios+'</td><td>Titular</td><td><a id="borrar-asegurados-t"><i class="fa fa-trash fa-lg"></i></a></td></tr>';
                      $("#table-asegurados").children("tbody").append(out);
                      $('#m_carga_familiar').show();
                      $('#m_titular').hide();
              });//FIN GET
          }else{
              $('#m_titular').show();
              $('#m_carga_familiar').hide();
          }
     }

});

//PAsANDOLE INFORMACION AL MODAL
$(document).on('click', '#m_carga_familiar', (function(e) {

  //var depend=$(this).data('depend');
	if($("input[name*='asegurado']").countElements() > 0){
    var titular=$("input[name*='asegurado']:first").val();
		$.get(
			$(this).data('url'),{'depend':titular,'tipo_persona':'Familiar','tipo_poliza':'salud'},
			function (data) {
				$('.modal-body').html(data);
				$('#modal').modal({
					backdrop: true,
					keyboard: true,
					show:true

				}).css({
					 width: '100%',
					 heigth: 'auto'

				});
			}
       );
	}else{
		alert('Para cargar un familiar debe agregar al titular a la lista');
	}
}));

//PASANDOLE INFORMACION AL MODAL
$(document).on('click', '#m_cliente', (function(e) {
	$.get($(this).data('url'),{'depend':'','tipo_persona':'Otro','tipo_poliza':'salud'},
        function (data) {
            $('.modal-body').html(data);
            $('#modal').modal({
				backdrop: true,
				keyboard: true
			}).css({
				 width: '100%',
				 heigth: 'auto'

			});
        }
    );
}));

//PASANDOLE INFORMACION AL MODAL
$(document).on('click', '#m_titular', (function(e) {
	$.get($(this).data('url'),{'depend':'','tipo_persona':'Titular','tipo_poliza':'salud'},
        function (data) {
            $('.modal-body').html(data);
            $('#modal').modal({
				backdrop: true,
				keyboard: true
			}).css({
				 width: '100%',
				 heigth: 'auto'

			});
        }
    );
}));

$(document).on('click', '#m_contratante', (function(e) {
	$.get($(this).data('url'),{'depend':'','tipo_persona':'Contratante','tipo_poliza':'salud'},
      function (data) {
            $('.modal-body').html(data);
            $('#modal').modal({
				backdrop: true,
				keyboard: true
			}).css({
				 width: '100%',
				 heigth: 'auto'

			});
        }
    );
}));

$('#poliza-form').on('change', "select[name='Poliza[id_contratante]']", function(event) {
  var out="";
  var id =$("#id_contratante").val();
  $('#poliza-id_contratante_hidden').val(id);
  $("#datoscontratante").empty();
    $.post("buscar-cliente", { id: id })
     .done(function(result) {
       var datos=result[0];
        if(datos!=null){
              $("#datoscontratante").empty();
                  var out='<div class="datagrid"><table class="table table-condensed"><thead><tr><th>Nombre Completo</th><th>Identificacion</th></tr></thead>';
                      out+='<tbody><tr><td>'+datos.nombre_completo+'</td><td>'+datos.identificacion+'</td></tr></tbody></table></div>';
         }
    $("#datoscontratante").html(out);
  });//.donefunction(result)

});//fin change

$('#poliza-form').on('change', "select[name='Poliza[id_vendedor]']", function(event) {
  var id=$(this).val();
     $.post("buscar-vendedor", { id: id })
     .done(function(result) {
       var datos=result[0];

        if(result[0]==null){
              $("#datosvendedor").empty();
        }else{
              $("#datosvendedor").empty();
              $('#poliza-id_vendedor_hidden').val(datos.id_intermediario);
              $('#poliza-comision_porcentaje').val(datos.comision);
              $('#poliza-comision_vendedor').val((datos.comision/100)*$('#poliza-comision_total').val());

              var out='<div class="datagrid"><table class="table table-condensed"><thead><tr><th>Cedula</th><th>Nombre Completo</th><th>Codigo</th></tr></thead>';
                  out+='<tbody><tr><td>'+datos.identificacion+'</td><td>'+datos.nombre_completo+'</td><td>'+datos.codigo+'</td></tr></tbody></table></div>';
              $("#datosvendedor").html(out);
        }
     });//.donefunction(result)

});//fin change

$(document).on('click', '#add-asegurados', (function() {
  var out='';
  var titular="";
  var id_borrar='borrar-asegurados';
  var parentesco="";
  var asegurado=$("select[name='Poliza[asegurados]']").val();

  if(asegurado==""){
     alert("debe seleccionar de la lista un asegurado");
  }else{

      /*incluir preguntar si el elemento Seleccionado ya es carga familiar del titular
      si es carga familiar agregarlo normalmente
      sino levantar la modal de carga familiar con los datos del asegurado para que agregue su parentezco*/
      if($("input[name*='asegurado']").countElements() > 0){//cuando es carga familiar
           var titular=$("input[name*='asegurado']:first").val();
      }else{
            id_borrar='borrar-asegurados-t';
      }

      if($("input[name*='asegurado']").verificarIdentidad(asegurado)=='ok'){
         $.get(
             $(this).data('url'),{id:asegurado,id_titular:titular},
             function (result) {

                if(result.length==0){//si eres familiar pero sin asociar

                      if($("input[name*='asegurado']").countElements() > 0){
                         var titular=$("input[name*='asegurado']:first").val();
            							$.get(
            							  $("#m_carga_familiar").data('urlup'),{id:asegurado,'depend':titular,'tipo_persona':'Familiar'},
            								  function (data) {
            									$('.modal-body').html(data);
            									$('#modal').modal({
            									  backdrop: true,
            									  keyboard: true,
            									  show:true

            									}).css({
            									   width: '100%',
            									   heigth: 'auto'

            									});
            								  }
            								);
                      }else{
                        alert('Para cargar un familiar debe agregar al titular a la lista');
                        return false;
                      }

                }else{

                   var parentesco=result[0].parentesco;

                   out+='<tr id="'+result[0].identificacion+'"><td><input type="hidden" name="asegurado['+result[0].id_cliente+']" id="asegurado" value="'+result[0].id_cliente+'">'+result[0].identificacion+'</td><td>'+result[0].nombre_completo+'</td><td>'+result[0].anios+'</td><td>'+parentesco+'</td><td><a id="'+id_borrar+'"><i class="fa fa-trash fa-lg"></i></a></td></tr>';
                   $("#table-asegurados").children("tbody").append(out);

                    if($("input[name*='asegurado']").countElements() > 0){//si eres titular
                        $('#m_carga_familiar').show();
                        $('#m_titular').hide();
                    }

                }

             });//FIN GET

       }else{
         alert("El asegurado ya se agrego a la lista");
       }
  }

}));//FIN DOCUMENT CLICK

$(document).on('click', '#add-beneficiarios', (function() {
  var out='';
  if($("select[name='Poliza[beneficiarios]']").val()==""){
     alert("debe seleccionar de la lista un beneficiario");
  }else{
     if($("input[name*='porcentaje']").sumarPorcentaje()==100){
      alert("porcentaje de los beneficiarios completados");
  }

    if($("input[name*='beneficiario']").verificarIdentidad($("select[name='Poliza[beneficiarios]']").val())=='ok'){
             $.get(
                 $(this).data('url'),{id:$("select[name='Poliza[beneficiarios]']").val()},
                 function (result) {
                   out+='<tr id="'+result[0].identificacion+'"><td><input type="hidden" name="beneficiario['+result[0].id_cliente+']" id="beneficiario" value="'+result[0].id_cliente+'">'+result[0].identificacion+'</td><td>'+result[0].nombre_completo+'</td><td><input type="text" name="porcentaje['+result[0].id_cliente+']" id="porcentaje" size="5"></td><td><a id="borrar-beneficiarios"><i class="fa fa-trash fa-lg"></i></a></td></tr>';
                      $("#table-beneficiarios").children("tbody").append(out);
                 });//FIN GET

      }else{
             alert("El beneficiario ya se agrego a la lista");
      }


  }

}));//FIN DOCUMENT CLICK

$('#table-beneficiarios').on('keyup', "input[id*='porcentaje']", function(event) {
    // Do something on click on an existent or future .dynamicElement
      var SumaPorc=$("input[name*='porcentaje']").sumarPorcentaje();
      //console.log(SumaPorc);
      if(SumaPorc > 100){
         alert('La Suma de los Porcentajes no debe superar el 100%');
         $(this).val("");
      }else if (SumaPorc==100) {
        alert('Porcentaje completado al 100%');
      }

});

//$("input[name*='porcentaje']").maskMoney({thousands:'.', decimal:','});

$("#siguiente-benef").on("click", function(e) {
    //$("#tab-cobertura,").children('a').prop('disabled', true);//PARA DOCUMENTACION
     $("#tab-cobertura").removeClass('disabled');
     $("#tab-cobertura").addClass('active');
    $("#tab-benef").addClass('disabled');
     $("#w0-tab1").addClass("in active");
     $("#w0-tab0").removeClass('in active');

});

$("#atras-cobertura").on("click", function(e) {

	   $("#tab-cobertura").removeClass('disabled');
	   $("#tab-cobertura").addClass('active');
     $("#w0-tab1").removeClass('in active');
     $("#w0-tab0").addClass("in active");

});

$("#siguiente-cobertura").on("click", function(e) {
    //$("#tab-comision").removeClass('disabled');
    $("#tab-comision").addClass('active');
    $("#tab-cobertura").addClass('disabled');
    $("#w0-tab2").addClass("in active");
    $("#w0-tab1").removeClass('in active');

});

$("#atras-comision").on("click", function(e) {

    $("#tab-comision").addClass('disabled');
    $("#tab-cobertura").removeClass('disabled');
    $("#w0-tab1").addClass("in active");
    $("#w0-tab2").removeClass('in active');

});

$( "#poliza-form").submit(function( event ) {//validando campos jquery
  //var coberturas = [1,3];
  var coberturas = [];
  var mensajes=[];

  if ($("input[name*='cobBox']").verificarCoberturas(coberturas)!= "ok" ) {
     mensajes[0]=$("input[name*='cobBox']").verificarCoberturas(coberturas);
     //event.preventDefault();
  }

  if(mensajes.lenght==0){//si mensaje esta vacio todo ha sido validado
     return;
  }else{

           $.each(mensajes, function(key, value) {
              	$.notify({
              	icon: 'glyphicon glyphicon-star',
              	message: value,
              	type:'warning'
              	/*,template:''*/
                });
          });//fin each

  }

//http://fiddle.jshell.net/H3CK2/7/light/

});



EXP2;

$this->registerJs($string, \yii\web\View::POS_READY);
?>

<?php
/*Modal::begin([
 'options' => [
        'id' => 'modal2',
        'tabindex' => false, // important for Select2 to work properly
  ],
]);
Modal::end();*/
?>

<?php
Modal::begin([
 'options' => [
        'id' => 'modal',
        'tabindex' => false, // important for Select2 to work properly
  ],
]);
Modal::end();
?>

<?php
$this->registerCss(".ui-group-buttons .or{position:relative;float:left;width:.3em;height:1.3em;z-index:3;font-size:12px}
.ui-group-buttons .or:before{position:absolute;top:50%;left:50%;content:'u';background-color:#5a5a5a;margin-top:-.1em;margin-left:-.9em;width:1.8em;height:1.8em;line-height:1.55;color:#fff;font-style:normal;font-weight:400;text-align:center;border-radius:500px;-webkit-box-shadow:0 0 0 1px rgba(0,0,0,0.1);box-shadow:0 0 0 1px rgba(0,0,0,0.1);-webkit-box-sizing:border-box;-moz-box-sizing:border-box;-ms-box-sizing:border-box;box-sizing:border-box}
.ui-group-buttons .or:after{position:absolute;top:0;left:0;content:' ';width:.3em;height:2.84em;background-color:rgba(0,0,0,0);border-top:.6em solid #5a5a5a;border-bottom:.6em solid #5a5a5a}
.ui-group-buttons .or.or-lg{height:1.3em;font-size:16px}
.ui-group-buttons .or.or-lg:after{height:2.85em}
.ui-group-buttons .or.or-sm{height:1em}
.ui-group-buttons .or.or-sm:after{height:2.5em}
.ui-group-buttons .or.or-xs{height:.25em}
.ui-group-buttons .or.or-xs:after{height:1.84em;z-index:-1000}
.ui-group-buttons{display:inline-block;vertical-align:middle}
.ui-group-buttons:after{content:".";display:block;height:0;clear:both;visibility:hidden}
.ui-group-buttons .btn{float:left;border-radius:0}
.ui-group-buttons .btn:first-child{margin-left:0;border-top-left-radius:.25em;border-bottom-left-radius:.25em;padding-right:15px}
.ui-group-buttons .btn:last-child{border-top-right-radius:.25em;border-bottom-right-radius:.25em;padding-left:15px}
.hide{display:none;}");
?>
