<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\Response;
use kartik\select2\Select2;
use kartik\depdrop\Depdrop; //http://demos.krajee.com/widget-details/depdrop
use wbraganca\dynamicform\DynamicFormWidget;
use yii\widgets\MaskedInput;
use kartik\icons\Icon;
use yii\widgets\Pjax;
use kartik\checkbox\CheckboxX;
use kartik\money\MaskMoney;
use backend\models\Cliente;
use backend\models\Persona;
?>

<div class="box box-solid box-default">
<div class="box-header ">
       <h4 class="box-title"><?=Icon::show('car')?> Comisiones y Vendedor</h4>
</div><!--box-header --->
<div class="box-body"><!--box-body --->
    <div class="container-items"><!-- widgetBody -->
       <div class="row">
       <div class="col-sm-10">
             <div class="nonboxy-widget">
               <div class="widget-content">
                    <div class="widget-box">
                      <fieldset>
                         <legend><h4><?=Icon::show('group')?>Beneficiarios <span class="lead text-yellow">Importante: Persona(s) que reciben la indemnizacion en caso de muerte del titular</span></h4></h4></legend>
                         <div class="row">
                           <?php Pjax::begin(['id' => 'pjax-persona']);?>
                           <div class="col-sm-5"><!--col-sm-4-->
                             <?=$form->field($model,'beneficiarios', [
                                                                           'addon' => [
                                                                               'prepend' => ['content'=>'<i class="glyphicon glyphicon-user"></i>'],
                                                                               'append'=>['content'=>Html::a('Agregar', '#', [
                                                                                                              'id' => 'activity-index-link',
                                                                                                              'class' => 'btn btn-success',
                                                                                                              'data-toggle' => 'modal',
                                                                                                              'data-target' => '#modal',
                                                                                                              'data-url' => Url::to(['persona/create'], true),
                                                                                                              'data-pjax' => '0',
                                                                                                          ]), 'asButton'=>true],
                                                                           ]
                                                    ])->widget(Select2::classname(), [
                                                                'data' =>ArrayHelper::map( Cliente::find()
                                                                                             ->joinwith('idPersona')
                                                                                             ->where("cliente.estatus = :status", [':status' => 1])
                                                                                             ->select("(nombre||' '||apellido||' '|| identificacion) AS nombre_completo ,id_cliente")
                                                                                             ->all(),'id_cliente', 'nombre_completo'),
                                                                'language' => 'en',
                                                                 'options' => ['placeholder' => 'Seleccione ...','id'=>'beneficiarios'],
                                                                 'pluginOptions' => [
                                                                    'allowClear' => true
                                                                ],
                                                ])->label(false);
                              ?>
                          </div>
                          <?php Pjax::end(); ?>
                         <div class="col-sm-2"><?= Html::a(Icon::show('plus'). 'Agregar a la lista ','#' , ['class'=>'btn btn-primary','id' => 'add-beneficiarios','data-url' => Url::to(['poliza-salud/buscar-beneficiarios'], true),]) ?></div>
                         </div>
                        </div>
                        <div class="datagrid">
                          <table class="table table-condensed" id="table-beneficiarios">
                            <thead>
                              <tr><th>Cedula</th><th>Nombres y Apellidos</th><th>Porcentaje</th><th>Operacion</th></tr>
                            </thead>
                            <tbody></tbody>
                          </table>
                        </div>
                     </fieldset>
                    </div>
               </div>
           </div>
       </div>
       <div><?= Html::Button('Siguiente '.Icon::show('arrow-circle-right'), ['class' => 'btn btn-info','id' =>'siguiente-benef']) ?></div>
   </div><!-- widgetBody -->
</div><!--box-body --->
</div><!--box--->
