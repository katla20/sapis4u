<?php
use yii\helpers\Html;
//use yii\bootstrap\ActiveForm;
//use yii\bootstrap\ActiveField;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use kartik\select2\Select2;
//use kartik\depdrop\Depdrop; //http://demos.krajee.com/widget-details/depdrop
use kartik\widgets\DepDrop;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use yii\bootstrap\Tabs;
use kartik\icons\Icon;
use kartik\tabs\TabsX;
use kartik\nav\NavX;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\web\Response;
use kartik\money\MaskMoney;
use yii\bootstrap\Nav;


backend\assets\LocateAsset::register($this); //importante para incluir el main.js donde estan los plugines
if($model->fecha_anulacion){
$model->fecha_anulacion=date("d-m-Y", strtotime($model->fecha_anulacion));
}


//$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');

/* @var $this yii\web\View */
/* @var $model backend\models\Poliza */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="anulacion-form">

    <?php $form = ActiveForm::begin(); ?>


	<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
    <div class="box-header">
         <!--<h4 class="box-title">Anular poliza</h4>-->
    </div>

		<div class="box-body">
			<div class="container-items"><!-- widgetBody -->
			<div class="row">
				<div class="col-sm-5">
						<?= $form->field($model, 'motivo_anulacion',[
                                                  'addon' => [
                                                      'prepend' => [
                                                          'content' => '<i class="glyphicon glyphicon-list-alt"></i>'

                                                      ]
                                                  ]
                                              ])->textarea(['rows' => 2]) ?>
				</div>


			</div>
			<div class="row">
				<div class="col-sm-2">
                     <?=$form->field($model, 'fecha_anulacion',[
                                                   'addon' => [
                                                       'prepend' => [
                                                           'content' => '<i class="glyphicon glyphicon-calendar"></i>'

                                                       ]
                                                   ]
                                               ])->widget(\yii\widgets\MaskedInput::className(),
                                                                        [
                                                                        'clientOptions' => ['alias' =>  'date']
                                                                        ]) ?>

                   </div>


			</div>
            <div class="row">
                    <div class="col-sm-3">

						<?php
						//echo $form->field($model, 'estatus')->dropDownList([1 => 'Activo', 0 => 'Inactivo']);
						echo $form->field($model, 'estatus_poliza')->widget(Select2::classname(), [
								'data' => array("Anulada"=>"Anular","Eliminada"=>"Eliminar"),
								'language' => 'es',
								//'options' => ['placeholder' => 'Seleccione un Esatdo ...'],
								'pluginOptions' => [
								'allowClear' => true
								],
						]);
						?>
					</div>
    	    </div>

            <div class="row">

                <div class="col-sm-3">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Anular', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>

			</div>
		</div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

$string = <<<EXP2

$('#poliza-form').on('change', "select[name='Poliza[id_contratante]']", function(event) {
  var out="";
  var id =$("#id_contratante").val();
  $('#poliza-id_contratante_hidden').val(id);
  $("#datoscontratante").empty();

     $.post("buscar-cliente", { id: id })
     .done(function(result) {
       var datos=result[0];
        if(datos!=null){
              $("#datoscontratante").empty();
                  var out='<div class="datagrid"><table class="table table-condensed"><thead><tr><th>Nombre Completo</th><th>Identificacion</th></tr></thead>';
                      out+='<tbody><tr><td>'+datos.nombre_completo+'</td><td>'+datos.identificacion+'</td></tr></tbody></table></div>';
         }
    $("#datoscontratante").html(out);
  });//.donefunction(result)



});//fin change


$('#poliza-form').on('change', "select[name='Poliza[id_asegurado]']", function(event) {

  var out="";
  var id =$("#id_asegurado").val();
  $('#poliza-id_asegurado_hidden').val(id);
  $("#datosasegurado").empty();

     $.post("buscar-cliente", { id: id })
     .done(function(result) {
       var datos=result[0];
        if(datos!=null){
              $("#datosasegurado").empty();
                  var out='<div class="datagrid"><table class="table table-condensed"><thead><tr><th>Nombre Completo</th><th>Identificacion</th></tr></thead>';
                      out+='<tbody><tr><td>'+datos.nombre_completo+'</td><td>'+datos.identificacion+'</td></tr></tbody></table></div>';
         }
    $("#datosasegurado").html(out);
  });//.donefunction(result)

});//fin change

$('#poliza-form').on('change', "select[name='Poliza[id_automovil]']", function(event) {

  var out="";
  var id =$("#id_automovil").val();

  $('#poliza-id_automovil_hidden').val(id);
  $("#datosautomovil").empty();

  $.post("buscar-auto", { id: id })
  .done(function(result) {
     if(result.placa!=null){
       var out='<div class="datagrid"><table class="table table-condensed"><thead><tr><th>Placa</th><th>Modelo</th><th>Version</th><th>Año</th><th>Uso</th><th>Tipo Vehiculo</th></tr></thead>';
          out+='<tbody><tr><td>'+result.placa+'</td><td>'+result.modelo+'</td><td>'+result.version+'</td><td>'+result.anio+'</td><td>'+result.uso+'</td><td>'+result.tipo_vehiculo+'</td></tr></tbody></table></div>';
      }
     $("#datosautomovil").html(out);
  });//.donefunction(result)

});//fin change

$('#poliza-form').on('change', "select[name='Poliza[id_vendedor]']", function(event) {
  var id=$(this).val();
     $.post("buscar-vendedor", { id: id })
     .done(function(result) {
       var datos=result[0];

        if(result[0]==null){
              $("#datosvendedor").empty();
        }else{
              $("#datosvendedor").empty();
              $('#poliza-id_vendedor_hidden').val(datos.id_intermediario);
              $('#poliza-comision_porcentaje').val(datos.comision);
              $('#poliza-comision_vendedor').val((datos.comision/100)*$('#poliza-comision_total').val());

              var out='<div class="datagrid"><table class="table table-condensed"><thead><tr><th>Cedula</th><th>Nombre Completo</th><th>Codigo</th></tr></thead>';
                  out+='<tbody><tr><td>'+datos.identificacion+'</td><td>'+datos.nombre_completo+'</td><td>'+datos.codigo+'</td></tr></tbody></table></div>';
              $("#datosvendedor").html(out);
        }
     });//.donefunction(result)

});//fin change


$("#siguiente-auto").on("click", function(e) {
    //$("#tab-cobertura,").children('a').prop('disabled', true);//PARA DOCUMENTACION
     $("#tab-cobertura").removeClass('disabled');
     $("#tab-cobertura").addClass('active');
     $("#tab-auto").addClass('disabled');
     $("#w0-tab1").addClass("in active");
     $("#w0-tab0").removeClass('in active');

});

$("#atras-cobertura").on("click", function(e) {

	   $("#tab-cobertura").removeClass('disabled');
	   $("#tab-cobertura").addClass('active');
     $("#w0-tab1").removeClass('in active');
     $("#w0-tab0").addClass("in active");

});

$("#siguiente-cobertura").on("click", function(e) {
    //$("#tab-comision").removeClass('disabled');
    $("#tab-comision").addClass('active');
    $("#tab-cobertura").addClass('disabled');
    $("#w0-tab2").addClass("in active");
    $("#w0-tab1").removeClass('in active');

});

$("#atras-comision").on("click", function(e) {

    $("#tab-comision").addClass('disabled');
    $("#tab-cobertura").removeClass('disabled');
    $("#w0-tab1").addClass("in active");
    $("#w0-tab2").removeClass('in active');

});

EXP2;

$this->registerJs($string, \yii\web\View::POS_READY);
?>

<?php
Modal::begin([
      'options' => [
         'id' => 'modal',
         'tabindex' => false // important for Select2 to work properly
     ],
]);
Modal::end();

$this->registerCss("");
?>
