<?php

use yii\helpers\Html;
//use yii\widgets\DetailView;
use kartik\detail\DetailView;
use backend\models\Modelos;
use backend\models\Poliza;
use yii\helpers\ArrayHelper;
use kartik\icons\Icon;
use yii\helpers\Url;

$this->title ='';
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
          <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
          <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Polizas')?></div></a>
          <a href="#" class="btn btn-default"><div>Poliza Internacional</div></a>
</div>
</br></br>
<div class="poliza-view">
<?php

	$modelAsegurados=Poliza::asegurados($model->id_recibo)->asArray()->all();
	$modelBeneficiarios=Poliza::beneficiarios($model->id_recibo)->asArray()->all();
	$modelRecibos=Poliza::recibos($model->id_poliza);

	$attributes = [
	[
        'columns' => [
		    [
                'attribute'=>'numero_poliza',
				        'label'=>'Número Poliza',
                'format'=>'raw',
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ],
            [
                //'attribute'=>'fecha_vigenciahasta',
				'label'=>'Fecha Vigencia',
				'value'=> date($model->fecha_vigenciadesde).' - '.date($model->fecha_vigenciahasta),
                'format'=>'raw',
				/*'widgetOptions' => [
                    'pluginOptions'=>['format'=>'yyyy-mm-dd']
                ],*/
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ],
        ],
    ],
    [
        'columns' => [
		    [
                'attribute'=>'aseguradora',
                'label'=>'Aseguradora',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],

			[
                'attribute'=>'producto',
                'label'=>'Producto',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],

        ],
    ],
	[
        'group'=>true,
        'label'=>'DATOS DEL VENDEDOR',
        'rowOptions'=>['class'=>'success']
    ],
	[
        'columns' => [
		    [
               // 'attribute'=>'nro_recibo',
				'label'=>'Vendedor',
                'format'=>'raw',
                'value'=>$model->nombre_ven.'  '.$model->apellido_ven,
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ],
            [
                //'attribute'=>'comision_total',
				'label'=>'Código',
                'format'=>'raw',
                'value'=>'<kbd>'.$model->codigo.'</kbd>',
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ],
        ],
    ],

	[
        'columns' => [
            [
                'attribute'=>'comision_vendedor',
                'label'=>'Comision Vendedor',
                'displayOnly'=>true,
               // 'valueColOptions'=>['style'=>'width:30%']
            ],

        ],
    ],

	[
        'group'=>true,
        'label'=>'DATOS DEL CONTRATANTE',
        'rowOptions'=>['class'=>'success']
    ],

	[
        'columns' => [
		    [
                'attribute'=>'identificacion',
				'label'=>'Identificacón',
                'format'=>'raw',
                'displayOnly'=>true
            ],

        ],
    ],

	[
        'columns' => [
		    [
                'attribute'=>'nombre_cli',
				'label'=>'Nombres',
                'format'=>'raw',
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ],
            [
                'attribute'=>'apellido_cli',
				'label'=>'Apellidos',
                'format'=>'raw',
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ],
        ],
    ],

	[
        'group'=>true,
        'label'=>'ASEGURADOS',
        'rowOptions'=>['class'=>'success']
    ],

	];

	$attributes[]=
               [

				'columns' => [
						[
							//'attribute'=>'fecha_vigenciahasta',
							'label'=>'Identificacón',
							'format'=>'raw',
							'value'=> '',
							'valueColOptions'=>['style'=>'display:none'],
							'labelColOptions'=>['style'=>'width:10%'],
							'displayOnly'=>true
						],



						[

							'label'=>'Nombres',
							'value'=>'',
							'format'=>'raw',
							'valueColOptions'=>['style'=>'display:none'],
							'labelColOptions'=>['style'=>'width:30%'],
							'displayOnly'=>true
						],
						[
							'value'=>'',
							'label'=>'Apellidos',
							'format'=>'raw',
							'valueColOptions'=>['style'=>'display:none'],
							'labelColOptions'=>['style'=>'width:30%'],
							'displayOnly'=>true
						],
					],
			    ];



	foreach ($modelAsegurados as $bene) {


	 $attributes[]=
               [

				'columns' => [
						[
							//'attribute'=>'fecha_vigenciahasta',
							'label'=>'Identificacón',
							'format'=>'raw',
							'value'=> $bene['identificacion'],
							'valueColOptions'=>['style'=>'width:10%'],
							'labelColOptions'=>['style'=>'display:none'],
							'displayOnly'=>true
						],
						[

							'label'=>'Nombres',
							'value'=>$bene['nombre_cli'],
							'format'=>'raw',
							'valueColOptions'=>['style'=>'width:30%'],
							'labelColOptions'=>['style'=>'display:none'],
							'displayOnly'=>true
						],
						[
							'value'=>$bene['apellido_cli'],
							'label'=>'Apellidos',
							'format'=>'raw',
							'valueColOptions'=>['style'=>'width:30%'],
							'labelColOptions'=>['style'=>'display:none'],
							'displayOnly'=>true
						],
					],
			    ];


     }

	 $attributes []= [

        'group'=>true,
        'label'=>'BENEFICIARIOS',
        'rowOptions'=>['class'=>'success']

	];

	$attributes[]=
               [

				'columns' => [
						[
							//'attribute'=>'fecha_vigenciahasta',
							'label'=>'Identificacón',
							'format'=>'raw',
							'value'=> '',
							'valueColOptions'=>['style'=>'display:none'],
							'labelColOptions'=>['style'=>'width:10%'],
							'displayOnly'=>true
						],



						[

							'label'=>'Nombres',
							'value'=>'',
							'format'=>'raw',
							'valueColOptions'=>['style'=>'display:none'],
							'labelColOptions'=>['style'=>'width:30%'],
							'displayOnly'=>true
						],
						[
							'value'=>'',
							'label'=>'Apellidos',
							'format'=>'raw',
							'valueColOptions'=>['style'=>'display:none'],
							'labelColOptions'=>['style'=>'width:30%'],
							'displayOnly'=>true
						],
					],
			    ];




	 foreach ($modelBeneficiarios as $ase) {


	 $attributes[]=
               [

				'columns' => [
						[
							//'attribute'=>'fecha_vigenciahasta',
							'label'=>'Identificacón',
							'format'=>'raw',
							'value'=> $ase['identificacion'],
							'valueColOptions'=>['style'=>'width:10%'],
							'labelColOptions'=>['style'=>'display:none'],
							'displayOnly'=>true
						],



						[

							'label'=>'Nombres',
							'value'=>$ase['nombre_cli'],
							'format'=>'raw',
							'valueColOptions'=>['style'=>'width:30%'],
							'labelColOptions'=>['style'=>'display:none'],
							'displayOnly'=>true
						],
						[
							'value'=>$ase['apellido_cli'],
							'label'=>'Apellidos',
							'format'=>'raw',
							'valueColOptions'=>['style'=>'width:30%'],
							'labelColOptions'=>['style'=>'display:none'],
							'displayOnly'=>true
						],
					],
			    ];


     }

    // print_r($attributes);

		$attributes[]=
        [
                    'group'=>true,
                    'label'=>'RECIBOS',
                    'rowOptions'=>['class'=>'success']
        ];


    foreach ($modelRecibos as $re) {


			$attributes[]=
				 [
				'columns' => [
					[
						//'attribute'=>'numero_poliza',
						'label'=>'Nro Recibo',
						'format'=>'raw',
						'value'=>$re['nro_recibo'],
						'valueColOptions'=>['style'=>'width:10%'],
						'labelColOptions'=>['style'=>'width:10%'],
						'displayOnly'=>true
					],


					[
						//'attribute'=>'fecha_vigenciahasta',
						'label'=>'Vigencia',
						'format'=>'raw',
						'value'=> date("d-m-Y", strtotime($re['fecha_vigenciadesde'])).' - '.date("d-m-Y", strtotime($re['fecha_vigenciahasta'])),
						'valueColOptions'=>['style'=>'width:10%'],
						'labelColOptions'=>['style'=>'width:10%'],
						'displayOnly'=>true
					],

					[
						//'attribute'=>'fecha_vigenciahasta',
						'label'=>'Tipo',
						'format'=>'raw',
						'value'=> $re['tipo_recibo'],
						'valueColOptions'=>['style'=>'width:10%'],
						'labelColOptions'=>['style'=>'width:10%'],
						'displayOnly'=>true
					],
					[
						//'attribute'=>'fecha_vigenciahasta',
						'label'=>'Comision',
						'format'=>['decimal', 2],
						'value'=> $re['comision_total'],
						'valueColOptions'=>['style'=>'width:10%;text-align: right;'],
						'labelColOptions'=>['style'=>'width:10%;text-align: right;'],
						'displayOnly'=>true
					],
				],
			];

			$attributes[]=
				[
							'group'=>true,
							'label'=>'COBERTURAS',
							'rowOptions'=>['class'=>'success']
				];

	$modelCoberturas=Poliza::coberturas($re['id_recibo'])->asArray()->all();
	$total=0;
	$attributes[]=
             [
			'columns' => [
				[
					//'attribute'=>'numero_poliza',
					'label'=>'',
					'format'=>'raw',
					'value'=>'',
					'valueColOptions'=>['style'=>'display:none;'],
					'labelColOptions'=>['style'=>'width:30%'],
					'displayOnly'=>true
				],
				[
					//'attribute'=>'fecha_vigenciahasta',
					'label'=>'Suma Asegurada',
					'format'=>'raw',
					'value'=>'',
					'valueColOptions'=>['style'=>'display:none;'],
					'labelColOptions'=>['style'=>'width:20%;text-align: right;'],
					'displayOnly'=>true
				],
				[
					//'attribute'=>'fecha_vigenciahasta',
					'label'=>'Prima',
					'format'=>'raw',
					'value'=>'',
					'valueColOptions'=>['style'=>'display:none;'],
                    'labelColOptions'=>['style'=>'width:20%;text-align: right;'],
					'displayOnly'=>true
				],
			],
		];

	foreach ($modelCoberturas as $cober) {

	  $total+=$cober['prima'];
	  $attributes[]=
             [
			'columns' => [
				[
					//'attribute'=>'numero_poliza',
					'label'=>'Cobertura',
					'format'=>'raw',
					'value'=>$cober['coberturas'],
					'valueColOptions'=>['style'=>'width:30%'],
					'labelColOptions'=>['style'=>'display:none;'],
					'displayOnly'=>true
				],
				[
					//'attribute'=>'fecha_vigenciahasta',
					'label'=>'Suma Asegurada',
					'format'=>['decimal', 2],
					'value'=> $cober['suma_asegurada'],
					'valueColOptions'=>['style'=>'width:20%;text-align: right;'],
					'labelColOptions'=>['style'=>'display:none;'],
					'displayOnly'=>true
				],
				[
					//'attribute'=>'fecha_vigenciahasta',
					'label'=>'Prima',
					'format'=>['decimal', 2],
					'value'=> $cober['prima'],
					'valueColOptions'=>['style'=>'width:20%;text-align: right;'],
                    'labelColOptions'=>['style'=>'display:none;'],
					'displayOnly'=>true
				],
			],
		];

       }


	  $attributes[]=
             [
			'columns' => [

				[
					//'attribute'=>'fecha_vigenciahasta',
					'label'=>'Total',
					'format'=>['decimal', 2],
					'value'=> $total,
					'valueColOptions'=>['style'=>'width:20%;text-align: right;'],
                    'labelColOptions'=>['style'=>'width:50%;text-align: right;'],
					'displayOnly'=>true
				],
			],
		];



     }

	// View file rendering the widget
		echo DetailView::widget([
		'model' => $model,
		'attributes' => $attributes,
		'mode' => 'view',
		'enableEditMode'=> false,
		//'bordered' => 'bordered',
		//'striped' => $striped,
		//'condensed' => $condensed,
		'responsive' => 'responsive',
		//'hover' => $hover,
		//'hAlign'=>$hAlign,
		//'vAlign'=>$vAlign,
		//'fadeDelay'=>$fadeDelay,
		'deleteOptions'=>[ // your ajax delete parameters
			'params' => ['id' => 1000, 'kvdelete'=>true],
		],
		'panel'=>[
        'heading'=>'DETALLE POLIZA INTERNACIONAL ['.$model->tipo_recibo.']',
        'type'=>DetailView::TYPE_INFO,

        ],
		'container' => ['id'=>'kv-demo'],
		//'formOptions' => ['action' => Url::current(['update' => 'kv-demo'])] // your action to delete
	]);


	$string="

         .etica{
			background-color:#B2B9BF;
			color:#FFFFFF;
			font-size: 13px;
			font-weight: bold;
			border-left: 0px solid #EEF6FF;
		 }";
      $this->registerCss($string);
	?>



</div>
