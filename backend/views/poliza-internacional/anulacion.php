<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;
use kartik\icons\Icon;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Poliza */

$this->title = "";
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
          <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
          <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Polizas')?></div></a>
          <a href="#" class="btn btn-default"><div>Anular Poliza</div></a>
</div>
</br></br>
<div class="poliza-update">
  <div class="bg-aqua top-modulo">
      <?php echo Icon::show('ambulance', ['class' => 'fa-3x']);?><?php echo Icon::show('users', ['class' => 'fa-3x'], Icon::FA);?>
      <span style="color:white;font-size:18px;font-weight:bold;"><?=Yii::t('app', 'Anular Poliza Salud Individual');?></span>
  </div>
    <?= $this->render('_formAnulacion', [
        'model' => $model,
    ]) ?>
</div>
<?php $string=".top-modulo{padding:2px 2px 2px 8px;}
          .icon-modulo{margin:0px 4px 0px 0px}";
      $this->registerCss($string);
?>
