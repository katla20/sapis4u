<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;
use kartik\icons\Icon;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Poliza */

$this->title = "";
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
          <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
          <a href="<?=Url::to(['poliza-flota/index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Polizas')?></div></a>
          <a href="#" class="btn btn-default"><div>Exclusiòn Flota</div></a>
</div>
</br></br>
<div class="poliza-update">
  <div class="bg-aqua top-modulo">
      <span class="icon-modulo"><?=Icon::show('car', ['class' => 'fa-3x'])?></span>
      <span style="color:white;font-size:18px;font-weight:bold;"><?=Yii::t('app', 'EXCLUSIÒN EN FLOTA');?></span>
  </div>
    <?= $this->render('_form3', [
        'model' => $model,
                'documentos' => $documentos,
                'recibo' => $recibo,
    ]) ?>
</div>
<?php $string=".top-modulo{padding:2px 2px 2px 8px;}
          .icon-modulo{margin:0px 4px 0px 0px}";
      $this->registerCss($string);
?>