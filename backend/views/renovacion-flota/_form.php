<?php
use yii\helpers\Html;
//use yii\bootstrap\ActiveForm;
//use yii\bootstrap\ActiveField;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use kartik\select2\Select2;
//use kartik\depdrop\Depdrop; //http://demos.krajee.com/widget-details/depdrop
use kartik\widgets\DepDrop;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use yii\bootstrap\Tabs;
use kartik\icons\Icon;
use kartik\tabs\TabsX;
use kartik\nav\NavX;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\web\Response;
use kartik\money\MaskMoney;
use backend\models\Aseguradoras;
use backend\models\InterAsegRamo;
use backend\models\InterAsegProducto;
use backend\models\Productos;
use backend\models\Cliente;
use backend\models\Persona;
use backend\models\Intermediario;
use backend\models\Automovil;
use backend\models\Version;
use backend\models\Modelo;
use backend\models\Marca;
use kartik\widgets\FileInput;
use yii\web\UploadedFile;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model backend\models\Poliza */
/* @var $form yii\widgets\ActiveForm */
$script = <<<SCRIPT
function(){
   var obj = [];
        obj[recibo]=0;
    if($('#poliza-nro_recibo').val()!=""){
         obj[recibo]= $('#poliza-nro_recibo').val();
    }
    return obj;
}
SCRIPT;

$model->fecha_vigenciadesde=date("d-m-Y", strtotime ( '+1 year' ,strtotime($model->fecha_vigenciadesde)));
$model->fecha_vigenciahasta=date("d-m-Y", strtotime ( '+1 year' ,strtotime($model->fecha_vigenciahasta)));

?>
<?php $form = ActiveForm::begin([
      'id' => 'flota-form',
	  'options' => ["enctype" => "multipart/form-data"],
      'enableAjaxValidation' => false,
      //'enableClientScript' => true,
      //'enableClientValidation' => true,
      ]);
   ?>
<div class="flota-form">

    <div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
    <div class="box-header with-border">
         <h4 class="box-title"><i class="glyphicon glyphicon-user"></i>Datos de la Poliza</h4>
    </div>
    <div class="box-body">
  	    <div class="container-items"><!-- widgetBody -->
         <div class="row">
            <div class="col-sm-4">
                            <?=$form->field($model,"id_aseguradora",[
                                                          'addon' => [
                                                              'prepend' => [
                                                                  'content' => '<i class="fa fa-university"></i>'

                                                              ]
                                                          ]
                                                      ])->widget(Select2::classname(), [
                                 'data' =>   ArrayHelper::map(Aseguradoras::find()->joinwith('interAsegRamos')
                                                   ->where('inter_aseg_ramo.id_ramo = :ramo and aseguradoras.estatus=:status')
                                                   ->addParams([':ramo' => 1])
                                                   ->addParams([':status' => 1])
                                                   ->all(),'id_aseguradora', 'nombre'),
                                  'language' => 'en',
                                  'options' => ['placeholder' => 'Seleccione ...'],
								  'disabled' => 'disabled',
                                  'pluginOptions' => [
                                     'allowClear' => true
                                 ],
                               ]);
                             ?>
                      </div>
      <div class="col-sm-5" style="display:none;">
                  <?=$form->field($model, "producto",[
                                                'addon' => [
                                                    'prepend' => [
                                                        'content' => '<i class="fa fa-cart-plus"></i>'

                                                    ]
                                                ]
                                            ])->widget(DepDrop::classname(), [
                      										'data'=> [''=>'Seleccione '],
                      										'options'=>['placeholder'=>'Selecione ...'],
                      										'type' => DepDrop::TYPE_SELECT2,
                      										'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                      										'pluginOptions'=>[
                      										'depends'=>["poliza-id_aseguradora"],
                      										'placeholder' => 'Seleccione ...',
                                            'url'=>Url::to(['dependent-dropdown/child-account-join', 'db'=>Productos::classname(),'join'=>'interAsegProductos','cmpo_dep'=>'inter_aseg_producto.id_aseguradora','cmpo_mostrar'=>'productos.nombre','id_cmpo'=>'productos.id_producto','cmpo_adic'=>'productos.id_ramo','valor_adic'=>1]),
                      									   ]
  									               ]);
  								?>
      </div>


     
        </div>
        <div class="row">
        <div class="col-sm-2">

                    <?
                    $model->tipo_recibo = 'Renovacion';
                    echo $form->field($model,"tipo_recibo",[
                                                  'addon' => [
                                                      'prepend' => [
                                                          'content' => '<i class="fa fa-university"></i>'

                                                      ]
                                                  ]
                                              ])->widget(Select2::classname(), [
                                                         'data' => ['Renovacion' => 'Renovacion'] ,
                                                         'language' => 'en',
                                                          'options' => ['placeholder' => 'Seleccione ...'],
														  'disabled' => 'disabled',
                                                          'pluginOptions' => [
                                                             'allowClear' => true
                                                         ],
                                                       ]);
                     ?>
                   </div>
                   <div class="col-sm-2">
                    <?= $form->field($model, 'numero_poliza',[
                                                  'addon' => [
                                                      'prepend' => [
                                                          'content' => '<i class="glyphicon glyphicon-list-alt"></i>'

                                                      ]
                                                  ]
                                              ])->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

                   </div>
   
        <div class="col-sm-2">
          <?=$form->field($model, 'fecha_vigenciadesde',[
                                        'addon' => [
                                            'prepend' => [
                                                'content' => '<i class="glyphicon glyphicon-calendar"></i>'

                                            ]
                                        ]
                                    ])->widget(\yii\widgets\MaskedInput::className(),
                                                             [
                                                             'clientOptions' => ['alias' =>  'date']
                                                             ]) ?>

        </div>
        <div class="col-sm-2">
           <?=$form->field($model, 'fecha_vigenciahasta',[
                                         'addon' => [
                                             'prepend' => [
                                                 'content' => '<i class="glyphicon glyphicon-calendar"></i>'

                                             ]
                                         ]
                                     ])->widget(\yii\widgets\MaskedInput::className(),
                                                              [
                                                              'clientOptions' => ['alias' =>  'date']
                                                              ]) ?>
        </div>
    </div>
    
         <div class="row">
    
        <div class="col-sm-2">
          <?= $form->field($model, 'nro_recibo',[
                                        'addon' => [
                                            'prepend' => [
                                                'content' => '<i class="glyphicon glyphicon-list-alt"></i>'
                                            ]
                                        ]
                                    ])->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-2">
          <?=$form->field($recibo, 'fecha_vigenciadesde',[
                                        'addon' => [
                                            'prepend' => [
                                                'content' => '<i class="glyphicon glyphicon-calendar"></i>'

                                            ]
                                        ]
                                    ])->widget(\yii\widgets\MaskedInput::className(),
                                                             [
                                                             'clientOptions' => ['alias' =>  'date']
                                                             ]) ?>

        </div>
        <div class="col-sm-2">
           <?=$form->field($recibo, 'fecha_vigenciahasta',[
                                         'addon' => [
                                             'prepend' => [
                                                 'content' => '<i class="glyphicon glyphicon-calendar"></i>'

                                             ]
                                         ]
                                     ])->widget(\yii\widgets\MaskedInput::className(),
                                                              [
                                                              'clientOptions' => ['alias' =>  'date']
                                                              ]) ?>
        </div>
    </div>

    <div class="row">
      <?php Pjax::begin();?>
        <div class="col-sm-4" id="w0">
         <?=$form->field($model,'id_contratante'/*, [
                                                       'addon' => [
                                                           'prepend' => ['content'=>'<i class="glyphicon glyphicon-user"></i>'],
                                                           'append'=>['content'=>Html::a('<i class="fa fa-user-plus"></i> Nuevo', '#', [
                                                                                          'id' => 'activity-index-link',
                                                                                          'class' => 'btn btn-success',
                                                                                          'data-toggle' => 'modal',
                                                                                          'data-target' => '#modal',
                                                                                          'data-url' => Url::to(['persona/create'], true),
                                                                                          'data-pjax' => '0',
                                                                                      ]), 'asButton'=>true],
                                                       ]
                                ]*/)->widget(Select2::classname(), [
                                            'data' =>ArrayHelper::map( Cliente::find()
                                                                         ->joinwith('idPersona')
                                                                         ->where("cliente.estatus = :status", [':status' => 1])
                                                                         ->select("(nombre||' '||apellido|| '  ' || identificacion) AS nombre_completo ,id_cliente")
                                                                         ->all(),'id_cliente', 'nombre_completo'),
                                            'language' => 'en',
                                             'options' => ['placeholder' => 'Seleccione ...','id'=>'id_contratante'],
                                             'disabled' => 'disabled',
                                             'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                            ]);
          ?>



        </div>
        <?php Pjax::end(); ?>
          <?=$form->field($model, 'id_contratante_hidden')->hiddenInput()->label(false);?>
          <div class="col-sm-6" id="datoscontratante"></div>
        </div>

        <div class="row">
          <?php Pjax::begin();?>
            <div class="col-sm-4" id="w0">
             <?=$form->field($model,'id_subcontratante'/*, [
                                                           'addon' => [
                                                               'prepend' => ['content'=>'<i class="glyphicon glyphicon-user"></i>'],
                                                               'append'=>['content'=>Html::a('<i class="fa fa-user-plus"></i> Nuevo', '#', [
                                                                                              'id' => 'activity-index-link',
                                                                                              'class' => 'btn btn-success',
                                                                                              'data-toggle' => 'modal',
                                                                                              'data-target' => '#modal',
                                                                                              'data-url' => Url::to(['persona/create'], true),
                                                                                              'data-pjax' => '0',
                                                                                          ]), 'asButton'=>true],
                                                           ]
                                    ]*/)->widget(Select2::classname(), [
                                                'data' =>ArrayHelper::map( Cliente::find()
                                                                             ->joinwith('idPersona')
                                                                             ->where("cliente.estatus = :status", [':status' => 1])
                                                                             ->select("(nombre||'  '||apellido|| '  ' || identificacion) AS nombre_completo ,id_cliente")
                                                                             ->all(),'id_cliente', 'nombre_completo'),
                                                'language' => 'en',
                                                 'options' => ['placeholder' => 'Seleccione ...','id'=>'id_subcontratante'],
                                                 'disabled' => 'disabled',
                                                 'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                ]);
              ?>
            </div>
            <?php Pjax::end(); ?>
              <?=$form->field($model, 'id_subcontratante_hidden')->hiddenInput()->label(false);?>
              <div class="col-sm-6" id="datossubcontratante"></div>
            </div>

        </div><!--CONTAINER-->
        </div><!--BOX BODY-->
  </div><!--BOX-->


  <div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
  -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
  box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
  <div class="box-header with-border">
       <h4 class="box-title"><?=Icon::show('newspaper-o')?>Datos de Comision</h4>
       <?php /*echo NavX::widget([
         'options' => ['class' => 'nav nav-pills '],
         'items' => [
             ['label' => Icon::show('plus').' Crear Recibo Consolidado', 'url' => '#'],
             ['label' => Icon::show('plus').' Crear Recibo Detallado', 'url' => '#'],
         ],
         'encodeLabels' => false
       ]); */?>
  </div>
  <div class="box-body">
      <div class="container-items"><!-- widgetBody -->
        <div class="row">

               <div class="col-sm-3" style="display:none;">
                 <?= $form->field($model, 'nro_certificado',[
                                               'addon' => [
                                                   'prepend' => [
                                                       'content' => '<i class="glyphicon glyphicon-list-alt"></i>'
                                                   ]
                                               ]
                                           ])->textInput(['maxlength' => true,'value'=>0]) ?>
               </div>

        </div>
        <div class="row">
            <div class="col-sm-3">
            <?php  echo $form->field($model, 'comision_total',[
                                  'addon' => [
                                    'prepend' => [
                                      'content' => 'Bs.',

                                    ]
                                  ]
                                ])->widget(MaskMoney::classname(), [
                                                  'pluginOptions' => [
                                                    'prefix' => '',
                                                    'suffix' => '',
                                                    'allowNegative' => false
                                                  ]
                                                 ]);?>

            </div>
			<?/*php Pjax::begin();*/?>
            <div class="col-sm-4" id="w0"><!--col-sm-4-->
            <?=$form->field($model,'id_vendedor', [
                                                          'addon' => [
                                                              'prepend' => ['content'=>'<i class="glyphicon glyphicon-user"></i>'],
                                                              'append'=>['content'=>Html::a('<i class="fa fa-user-plus"></i> Nuevo', '#', [
                                                                                             'id' => 'activity-index-link',
                                                                                             'class' => 'btn btn-success',
                                                                                             'data-toggle' => 'modal',
                                                                                             'data-target' => '#modal',
                                                                                             'data-url' => Url::to(['vendedor/create'], true),
                                                                                             'data-pjax' => '0',
                                                                                         ]), 'asButton'=>true]
                                                          ]
                                   ])->widget(Select2::classname(), [
                                               'data' =>   ArrayHelper::map( Intermediario::find()
                                                                            ->joinwith('idPersona')
                                                                            ->where("intermediario.estatus = :status AND id_tipo_intermediario=2", [':status' => 1])
                                                                            ->select("(nombre||' '||segundo_nombre||' '||apellido|| ' ' ||segundo_apellido || ' ' || identificacion) AS nombre_completo ,id_intermediario")
                                                                            ->all(),'id_intermediario', 'nombre_completo'),
                                               'language' => 'en',
                                                'options' => ['placeholder' => 'Seleccione ...','id'=>'id_vendedor'],
                                                'pluginOptions' => [
                                                   'allowClear' => true
                                               ],
                               ]);
             ?>
            </div>
            <?/*php Pjax::end();*/?>
            <?=$form->field($model, 'id_vendedor_hidden')->hiddenInput()->label(false);?>
            <div class="col-sm-2"><!--col-sm-2-->
               <?php  echo $form->field($model, 'comision_porcentaje',[
                                  'addon' => [
                                    'prepend' => [
                                      'content' => '%',

                                    ]
                                  ]
                                ])->textInput(['readonly' => true]) ;?>
            </div><!--col-sm-2-->
            <div class="col-sm-2"><!--col-sm-2-->
             <?php  echo $form->field($model, 'comision_vendedor',[
                                  'addon' => [
                                    'prepend' => [
                                      'content' => 'Bs.',

                                    ]
                                  ]
                                ])->textInput(['readonly' => true]);?>

            </div><!--col-sm-2-->
        </div>
        <div id="datosvendedor"></div>
	    <div class="row">
        <!--http://plugins.krajee.com/file-input#events-->
			<div class="col-sm-12">
				<?=$form->field($documentos, 'doc_files')->widget(FileInput::classname(),[
											   'options'=>[
												   'multiple'=>false,
												   'accept'=>'*',
											   ],

											     /*  'pluginOptions' => [
														'initialPreview'=>[
															"../uploads/AA-RRHH_0a702a4538f88f743ed532540e848d26.jpg",

														],
														'initialPreviewAsData'=>true,
														'initialCaption'=>"hola",
														'initialPreviewConfig' => [
															['caption' => 'AA-RRHH_0a702a4538f88f743ed532540e848d26.jpg', 'size' => '873727'],

														],
														'overwriteInitial'=>false,
														'maxFileSize'=>2800
													]*/
											   'pluginOptions' => [
												   'showUpload' => true,
												   'dropZoneEnabled'=>false,
												   'showCaption' => true,
												   'showPreview' => true,//false
												   'uploadUrl' => Url::to(['poliza-flota/upload-file']),
												   'allowedFileExtensions'=>['xlsx'],
												   //'uploadExtraData' =>new JsExpression($script)
                          // ['recibo' => new JsExpression($script)],


											   ]
											])->label(false);
				?>

			</div>
        </div>
      </div><!--CONTAINER-->
  </div><!--BOX BODY-->
  </div><!--BOX-->

  <div class="form-group">
      <?= Html::submitButton(Icon::show('save').'[Registrar Flota]', ['class' => 'btn btn-primary']) ?>
      <?= Html::resetButton(Icon::show('eraser').'Limpiar Campos', ['class' => 'btn btn-default']) ?>
  </div>
 </div>
    <?php ActiveForm::end();?>
<?php

$string = <<<EXP2
sumaFecha = function(d, fecha)
{
 var Fecha = new Date();
 var sFecha = fecha || (Fecha.getDate() + "/" + (Fecha.getMonth() +1) + "/" + Fecha.getFullYear());
 var sep = sFecha.indexOf('/') != -1 ? '/' : '-';
 var aFecha = sFecha.split(sep);
 var fecha = aFecha[2]+'/'+aFecha[1]+'/'+aFecha[0];
 fecha= new Date(fecha);
 fecha.setDate(fecha.getDate()+parseInt(d));
 var anno=fecha.getFullYear();
 var mes= fecha.getMonth()+1;
 var dia= fecha.getDate();
 mes = (mes < 10) ? ("0" + mes) : mes;
 dia = (dia < 10) ? ("0" + dia) : dia;
 var fechaFinal = dia+sep+mes+sep+anno;
 return (fechaFinal);
 }


$("input[name='Poliza[fecha_vigenciadesde]']").change(function(){
      $("input[name='Poliza[fecha_vigenciahasta]']").val(sumaFecha(365,$(this).val()));
});//$("input[name='Poliza[fecha_vigenciadesde]']")

$('#id_contratante').change(function(){
  var out="";
  var id =$("#id_contratante").val();
  $('#poliza-id_contratante_hidden').val(id);
  $("#datoscontratante").empty();

     $.post("buscar-cliente", { id: id })
     .done(function(result) {
       var datos=result[0];
        if(datos!=null){
              $("#datoscontratante").empty();
                  var out='<div class="datagrid"><table class="table table-condensed"><thead><tr><th>Nombre Completo</th><th>Identificacion</th></tr></thead>';
                      out+='<tbody><tr><td>'+datos.nombre_completo+'</td><td>'+datos.identificacion+'</td></tr></tbody></table></div>';
         }
    $("#datoscontratante").html(out);
  });//.donefunction(result)



});//fin change

$('#id_subcontratante').change(function(){
  var out="";
  var id =$("#id_subcontratante").val();
  $('#poliza-id_subcontratante_hidden').val(id);
  $("#datossubcontratante").empty();

     $.post("buscar-cliente", { id: id })
     .done(function(result) {
       var datos=result[0];
        if(datos!=null){
              $("#datossubcontratante").empty();
                  var out='<div class="datagrid"><table class="table table-condensed"><thead><tr><th>Nombre Completo</th><th>Identificacion</th></tr></thead>';
                      out+='<tbody><tr><td>'+datos.nombre_completo+'</td><td>'+datos.identificacion+'</td></tr></tbody></table></div>';
         }
    $("#datossubcontratante").html(out);
  });//.donefunction(result)



});//fin change


$('#id_vendedor').change(function(){
  var id=$(this).val();
  $('#poliza-id_vendedor_hidden').val(id);
  $("#datosvendedor").empty();
     $.post("buscar-vendedor", { id: id })
     .done(function(result) {
       var datos=result[0];
        if(datos!=null){
              $('#poliza-comision_porcentaje').val(datos.comision);
              $('#poliza-comision_vendedor').val(parseFloat((datos.comision/100)*$('#poliza-comision_total').val()).toFixed(2));
              var out='<div class="datagrid"><table class="table table-condensed"><thead><tr><th>Identificacion</th><th>Nombre Completo</th></tr></thead>';
                  out+='<tbody><tr><td>'+$("#id_vendedor option:selected").html()+'</td><td>hgghgh</td></tr></tbody></table></div>';
              $("#datosvendedor").html(out);
        }
     });//.donefunction(result)
});//fin change

$( "input[name='poliza-comision_total-disp']").keyup(function(event) {

  event.preventDefault();
  var porcentaje=0;
  var comision=0;
  var valor=$(this).val();
      valor=valor.replace('.', '');
      valor=parseFloat(valor.replace(',', '.'));

  if($('#poliza-comision_porcentaje').val()!=''){
      porcentaje=parseFloat($('#poliza-comision_porcentaje').val()/100);
  }

    $('#poliza-comision_vendedor').val(parseFloat(valor*porcentaje).toFixed(2));
});

EXP2;


$this->registerJs($string, \yii\web\View::POS_READY);

$this->registerJs(
    "$(document).on('click', '#activity-index-link', (function() {
        $.get(
            $(this).data('url'),
            function (data) {
                $('.modal-body').html(data);
                $('#modal').modal();
            }
        );
    }));"
);
?>

<?php
Modal::begin([
      'options' => [
         'id' => 'modal',
         'tabindex' => false // important for Select2 to work properly
     ],
    //'header' => '<h4 class="modal-title">Crear Cliente</h4>',
    //'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Cerrar</a>',
]);

//echo "<div class='well'></div>";

Modal::end();

$string=".datagrid table { border-collapse: collapse; text-align: left; width: 100%; } .datagrid {font: normal 12px/150% Arial, Helvetica, sans-serif; background: #fff; overflow: hidden; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; }.datagrid table td, .datagrid table th { padding: 3px 10px; }.datagrid table thead th {background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #B2B9BF), color-stop(1, #B2B8BF) );background:-moz-linear-gradient( center top, #B2B9BF 5%, #B2B8BF 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#B2B9BF', endColorstr='#B2B8BF');background-color:#B2B9BF; color:#FFFFFF; font-size: 13px; font-weight: bold; border-left: 0px solid #EEF6FF; } .datagrid table thead th:first-child { border: none; }.datagrid table tbody td { color: #8D9296; border-left: 1px solid #EEF6FF;font-size: 12px;font-weight: bold; }.datagrid table tbody .alt td { background: #CCD3DB; color: #5F6366; }.datagrid table tbody td:first-child { border-left: none; }.datagrid table tbody tr:last-child td { border-bottom: none; }";
$this->registerCss($string);
?>
