<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\Poliza;
use kartik\icons\Icon;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PolizaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//http://www.w3schools.com/colors/colors_picker.asp colores

$this->title = "";
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- graceful degrade -->
<!--<div id="breadcrumb2">
    <ul class="crumbs2">
        <li class="first"><a href="#">Blog Home</a></li>
        <li><a href="#">Archives</a></li>
        <li><a href="#">2011 Writing</a></li>
        <li class="last"><a href="#">Tips for jQuery Development in HTML5</a></li>
    </ul>
</div>-->

<?php //echo Url::current();?>
<?php //echo '---'.Url::to();
/*@app: Your application root directory (either backend or backend or console depending on where you access it from)
@vendor: Your vendor directory on your root app install directory
@runtime: Your application files runtime/cache storage folder
@web: Your application base url path
@webroot: Your application web root
@tests: Your console tests directory
@common: Alias for your common root folder on your root app install directory
@backend: Alias for your backend root folder on your root app install directory
@backend: Alias for your backend root folder on your root app install directory
@console: Alias for your console root folder on your root app install directory*/
?>
<?php //echo Yii::getAlias('@web');?>
<div class="poliza-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
  //	$modelEstado = new Modelos();
  	$gridColumns = [
  		['class' => 'kartik\grid\SerialColumn'],
		[
			'class'=>'kartik\grid\CheckboxColumn',
			'headerOptions'=>['class'=>'kartik-sheet-style'],

		],
	    'aseguradora',
        [
          'attribute'=>'numero_poliza',
          'value'=>'numero_poliza',
          'width'=>'100px',
        ],
		[
          'attribute'=>'id_recibo',
          'value'=>'id_recibo',
          'width'=>'100px',
        ],
	    [
          'attribute'=>'nro_recibo',
          'value'=>'nro_recibo',
          'width'=>'100px',
        ],
        'fecha_vigenciadesde',
		/*[
            'attribute' => 'rango_fecha',
            'value' => 'fecha_vigenciadesde',
            'format'=>'raw',
            'options' => ['style' => 'width: 25%;'],
            'filter' => DateRangePicker::widget([
                'model' => $searchModel,
                'attribute' => 'rango_fecha',
                'useWithAddon'=>false,
                'convertFormat'=>true,
                'pluginOptions'=>[
                    'locale'=>['format'=>'Y-m-d']
                ],
            ])
        ],*/

        'comision_total',

       /*  [
           'class' => 'kartik\grid\ActionColumn',
           'template' => '',
           'buttons' => [
               'update' => function ($url, $model, $key) {
                   return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                       'id' => 'activity-index-link',
                       'title' => Yii::t('app', 'Actualizar Cliente'),
                       'data-toggle' => 'modal',
                       'data-target' => '#modal',
                       'data-url' => Url::to(['update', 'id' => $model->id_persona]),
                       'data-pjax' => '0',
                   ]);
               },
           ]
         ],*/
  	];


  	echo GridView::widget([
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
  		'columns' => $gridColumns,
  		'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
  		'toolbar' => [
  			/*[
  				'content'=>
  					Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['recibos'], [
  						'class' => 'btn btn-default',
  						'title' => Yii::t('app', 'Actualizar Grid')
  					]),

  			],*/
  			'{export}',
  			'{toggleData}'
  		],

  		'pjax' => true,
  		'pjaxSettings' => [
  			'options' => ['id' => 'recibos-pjax-id'],// UNIQUE PJAX CONTAINER ID
  		],
  		'bordered' => false,
  		'resizableColumns'=>true,
  		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
  		'striped' => true,
  		'condensed' => false,
  		'responsive' => true,
  		'hover' => true,
  		'showPageSummary' => false,
    		'panel' => [
          'heading'=>'<h2 class="panel-title">&nbsp; &nbsp;Recibos</h2>',
    			'type' => GridView::TYPE_INFO
    		],
      ]);
  	?>
</div>

<?php

$string='';
$this->registerCss($string);
?>
<?php Pjax::begin(['id' => 'recibos']) ?> inicio del widget y <?php Pjax::end() ?>
