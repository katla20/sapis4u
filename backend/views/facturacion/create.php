<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\icons\Icon;
use yii\helpers\Url;

$this->title = '';
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
          <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
          <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Facturacion')?></div></a>
          <a href="#" class="btn btn-default"><div>Facturar Comisiones</div></a>
</div>
</br></br>
<div class="facturacion-create">
    <div class="bg-aqua top-modulo">
        <span class="icon-modulo"><i class="fa fa-list-alt fa-3x" aria-hidden="true"></i></span>
        <span style="color:white;font-size:18px;font-weight:bold;"><?=Yii::t('app', 'Crear Factura por Comisiones');?></span>
    </div>

    <?= $this->render('_form',[
                'model' => $model,
				'modelDetalle'=>$modelDetalle,
    ]); ?>

</div>
<?php $string=".top-modulo{padding:2px 2px 2px 8px;}
          .icon-modulo{margin:0px 4px 0px 0px}";
      $this->registerCss($string);
?>
