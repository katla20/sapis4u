<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use backend\models\Aseguradoras;
use kartik\select2\Select2;//keyla bullon
use kartik\widgets\DepDrop;
//use kartik\depdrop\DepDrop;//http://demos.krajee.com/widget-details/depdrop
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\icons\Icon;
use yii\web\Response;
use kartik\money\MaskMoney;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\widgets\Pjax;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\Facturacion */
/* @var $form yii\widgets\ActiveForm */
//['readonly' => !$model->isNewRecord]
?>

<div class="facturacion-form">

	  <?php $form = ActiveForm::begin([
		'id' => 'dynamic-form',
		]); ?>

    	<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
    <div class="box-header">
         <h4 class="box-title">Datos Basicos de Facturacion</h4>
    </div>

		<div class="box-body">
			<div class="container-items"><!-- widgetBody -->
				<div class="row">
					<div class="col-sm-2">
						<?= $form->field($model, 'numero_factura',[
  									'addon' => [
  										'prepend' => [
  											'content' => '<i class="fa fa-pencil"></i>'

  										]
  									]
  								])->textInput() ?>
					</div>
					<div class="col-sm-2">

						 <?= $form->field($model, 'numero_control',[
   									'addon' => [
   										'prepend' => [
   											'content' => '<i class="fa fa-pencil"></i>'

   										]
   									]
   								])->textInput() ?>
					</div>
                <div class="col-sm-2">
              <?=$form->field($model, 'fecha_factura',[
                                            'addon' => [
                                                'prepend' => [
                                                    'content' => '<i class="glyphicon glyphicon-calendar"></i>'

                                                ]
                                            ]
                                        ])->widget(\yii\widgets\MaskedInput::className(),
                                                                 [
                                                                 'clientOptions' => ['alias' =>  'date']
                                                                 ]) ?>

          </div>
          <div class="col-sm-2" style="display:none;">
               <?=$form->field($model, 'fecha_recepcion',[
                                             'addon' => [
                                                 'prepend' => [
                                                     'content' => '<i class="glyphicon glyphicon-calendar"></i>'

                                                 ]
                                             ]
                                         ])->widget(\yii\widgets\MaskedInput::className(),
                                                                  [
                                                                  'clientOptions' => ['alias' =>  'date']
                                                                  ]) ?>
        </div>
        </div>
        <div class="row" >
		    <div class="col-sm-2" style="display:none;">
					<?=$form->field($model, 'monto_iva',[
										  'addon' => [
											'prepend' => [
											  'content' => 'Bs.',

											]
										  ]
										])->widget(MaskMoney::classname(), [
														  'pluginOptions' => [
															'prefix' => '',
															'suffix' => '',
															'allowNegative' => false
														  ]
					  ]);?>
			</div>

		    <div class="col-sm-2" style="display:none;">
				<?=$form->field($model, 'monto_islr',[
									  'addon' => [
										'prepend' => [
										  'content' => 'Bs.',

										]
									  ]
									])->widget(MaskMoney::classname(), [
													  'pluginOptions' => [
														'prefix' => '',
														'suffix' => '',
														'allowNegative' => false
													  ]
				  ]);?>
			</div>

		</div>
		<div class="row">
				<div class="col-sm-3">
					<?php

						$id_aseguradora = ArrayHelper::map(Aseguradoras::find()->all(), 'id_aseguradora', 'nombre');

						echo $form->field($model, 'id_aseguradora',[
                                          'addon' => [
                                              'prepend' => [
                                                  'content' => '<i class="fa fa-university"></i>'

                                              ]
                                          ]
                                      ])->widget(Select2::classname(), [
							'data' => $id_aseguradora,
							'language' => 'es',
							'options' => ['placeholder' => 'Seleccione un Aseguradora ...'],
							'pluginOptions' => [
							'allowClear' => true
							],
						]);
					?>
                     <?=$form->field($model, 'recibos')->hiddenInput()->label(false);?>
				</div>
				<div class="col-sm-6"><div id="datosaseguradora"></div></div>
	    </div>
		<div class="row">
          <div class="col-sm-6">

            <?= $form->field($model, 'recibos_numero',[
                        'addon' => [
                            'prepend' => ['content'=>'<i class="glyphicon glyphicon-user"></i>'],
                            'append'=>['content'=>Html::a('<i class="fa fa-plus"></i> Recibos', '#', [
                                                           'id' => 'm_recibos',
                                                           'class' => 'btn btn-success',
                                                           //'data-toggle' => 'modal',
                                                           //'data-target' => '#modal',
                                                           'data-url' => Url::to(['recibos']),
                                                           'data-pjax' => '0',
                                                       ]), 'asButton'=>true],
                        ]
                  ])->textInput(['readonly' =>true,'placeholder' => 'Agregue los recibos a facturar en el boton Recibos']) ?>
          </div>
		  <div class="col-sm-2">
          <?=$form->field($model, 'monto_recibos',[
        												  'addon' => [
        													  'prepend' => [
        														  'content' => 'Bs.',

        													  ]
        												  ]
        											  ])->widget(MaskMoney::classname(), [
        																				  'pluginOptions' => [
        																					  'prefix' => '',
        																					  'suffix' => '',
        																					  'allowNegative' => false
        																					]
        			]);?>
					</div>
			</div>
		<div class="row">
			<div class="col-sm-8">
			 <?= $form->field($model, 'descripcion',[
							'addon' => [
								'prepend' => [
									'content' => '<i class="fa fa-pencil"></i>'

								]
							]
						])->textarea(['rows' => 2]) ?>
			</div>

		</div>

		<div class="row">
		  <div class="col-sm-10">
		<?php DynamicFormWidget::begin([
			   'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
			   'widgetBody' => '.container-items', // required: css class selector
			   'widgetItem' => '.item', // required: css class
			   'limit' => 4, // the maximum times, an element can be added (default 999)
			   'min' => 0, // 0 or 1 (default 1)
			   'insertButton' => '.add-item', // css class
			   'deleteButton' => '.remove-item', // css class
			   'model' => $modelDetalle[0],
			   'formId' => 'dynamic-form',
			   'formFields' => [
				   'descripcion',
				   'monto',
			   ],
			 ]);?>

				 <div class="box box-default">
				 <div class="box-header">
						 <h4>
							 <i class="glyphicon glyphicon-envelope"></i> Items Adicionales de la Factura
							 <button type="button" class="add-item btn btn-success btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i>Agregar Item</button>
						 </h4>
				 </div>
				 <div class="box-body" >
					 <div class="container-items"><!-- widgetBody -->
					 <?php foreach ($modelDetalle as $i => $modelDetalle): ?>
						 <div class="item panel panel-default"><!-- widgetItem -->
							 <div class="panel-heading">
								 <h5 class="panel-title pull-left">Detalle del Item</h5>
								 <div class="pull-right">
									 <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
								 </div>
								 <div class="clearfix"></div>
							 </div>
							 <div class="panel-body">
							  <div class="row">
									 <div class="col-sm-8">

										 <?= $form->field($modelDetalle, "[{$i}]descripcion",[
														'addon' => [
															'prepend' => [
																'content' => '<i class="fa fa-pencil"></i>'

															]
														]
													])->textarea(['rows' => 2]) ?>
									 </div>
									 <div class="col-sm-2">

									 <?= $form->field($modelDetalle, "[{$i}]monto",[
																		 'addon' => [
																			 'prepend' => [
																				 'content' => 'Bs.'

																			 ]
																		 ]
																	 ])->widget(MaskMoney::classname(), [
        																				  'pluginOptions' => [
        																					  'prefix' => '',
        																					  'suffix' => '',
        																					  'allowNegative' => false
        																					]
        																				 ]); ?>
									 </div>

								</div>



							 </div>
						 </div>
					   <?php endforeach; ?>

				   </div><!--container-items-->

				   </div><!--panel body-->
			 </div><!-- .panel -->
			 <?php DynamicFormWidget::end(); ?>

		  </div>
		  </div>
			<div class="row">
				 <div class="col-sm-3">
					 <?= $form->field($model, "monto_subtotal",[
														 'addon' => [
															 'prepend' => [
																 'content' => 'Bs.'

															 ]
														 ]
													 ])->textInput(['readonly' =>true,'value'=>'0.00']) ?>
                
				 </div>
				 <div class="col-sm-3">

				 <?= $form->field($model, "monto_iva",[
													 'addon' => [
														 'prepend' => [
															 'content' => 'Bs.'

														 ]
													 ]
												 ])->widget(MaskMoney::classname(), [
																				'pluginOptions' => [
																					'prefix' => '',
																					'suffix' => '',
																					'allowNegative' => false
																				],
																				
																			 ]); ?>
				 </div>
				 <div class="col-sm-3">

				 <?= $form->field($model, "monto_factura",[
													 'addon' => [
														 'prepend' => [
															 'content' => 'Bs.'

														 ]
													 ],
												])->textInput(['readonly' =>true,'value'=>'0.00']) ?>
				 </div>

			</div>
			<div class="row">
				 <div class="col-sm-6">
					 <?= Html::submitButton($model->isNewRecord ? 'Crear Factura' : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
				 </div>
			</div>
	    </div>
		</div>
    </div>

	<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1); display:none;">
    <div class="box-header">
         <h4 class="box-title">Datos Adicionales de la Factura</h4>
    </div>
		<div class="box-body" style="display:none;">

			<div class="row">
				<div class="col-sm-5">
					<?= $form->field($model, 'comentario',[
                 'addon' => [
                   'prepend' => [
                     'content' => '<i class="fa fa-pencil"></i>'

                   ]
                 ]
               ])->textarea(['rows' => 2]) ?>
				</div>
		        <div class="col-sm-3">
				  <?=$form->field($model, 'otro_monto',[
										'addon' => [
										  'prepend' => [
											'content' => 'Bs.',

										  ]
										]
									  ])->widget(MaskMoney::classname(), [
														'pluginOptions' => [
														  'prefix' => '',
														  'suffix' => '',
														  'allowNegative' => false
														]
					]);?>
				</div>
			</div>
		</div>
    </div>
		<?php ActiveForm::end(); ?>
</div>
<?php

$string = <<<EXP2



$(document).on("keyup", "input[id='facturacion-monto_recibos-disp'],input[id*='monto-disp']", function(event) {
   
   var monto_factura=$("input[id='facturacion-monto_recibos-disp'],input[id*='monto-disp']").sumarPorcentaje();
   
  $("input[id*='facturacion-monto_subtotal'],input[id*='facturacion-monto_factura']").val(parseFloat(monto_factura).toFixed(2));
 
  
});

$(document).on('click', '#m_recibos', (function(e) {
	var id_aseguradora=$('#facturacion-id_aseguradora').val();
	if(id_aseguradora!=""){

		$.get($(this).data('url'),{'tipo':'facturacion-comisiones','id':id_aseguradora},
          function (data) {
            $('.modal-body').html(data);
            $('#modal').modal({
				backdrop: true,
				keyboard: true
			});
        }
    );

	}else{
	   $('#modal').hide();
	   alert("Debe Seleccionar la Aseguradora");
	}

}));

$('#modal .modal-dialog').css({
     width: '40%',
     heigth: '50%'});

EXP2;

$this->registerJs($string, \yii\web\View::POS_READY);

?>

<?php
Modal::begin([
 'options' => [
        'id' => 'modal',
        'tabindex' => false, // important for Select2 to work properly
  ],
]);
Modal::end();
?>
