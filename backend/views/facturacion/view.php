<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Facturacion */

$this->title = $model->id_facturacion;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Facturacions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="facturacion-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id_facturacion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id_facturacion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_facturacion',
            'numero_factura',
            'numero_control',
            'monto_iva',
            'monto_islr',
            'monto_subtotal',
            'monto_recibos',
            'id_user_registro',
            'id_user_actualizacion',
            'fecha_registro',
            'fecha_actualizacion',
            'descripcion:ntext',
        ],
    ]) ?>

</div>
