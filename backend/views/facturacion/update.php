<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Facturacion */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Facturacion',
]) . ' ' . $model->id_facturacion;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Facturacions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_facturacion, 'url' => ['view', 'id' => $model->id_facturacion]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="facturacion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
