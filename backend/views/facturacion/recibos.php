﻿<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\Poliza;
use kartik\icons\Icon;
use kartik\daterange\DateRangePicker;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PolizaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//http://www.w3schools.com/colors/colors_picker.asp colores
$this->title = "";
?>
<?php Pjax::begin(['id' => 'recibos-pjax-id','timeout' => false,'enablePushState' => false]) ?>
<div class="recibos-modal-index">
    <?php
  	$gridColumns = [
  	//['class' => 'kartik\grid\SerialColumn'],
		[
			 'class'=>'kartik\grid\CheckboxColumn',
       'checkboxOptions' => function ($model, $key, $index, $column) {
          return ['value' => $model->numero_poliza];
       },
		],
    [
          'class' => 'kartik\grid\DataColumn',
          'attribute'=>'numero_poliza',
          'value'=>'numero_poliza',
          'width'=>'200px',
    ],
	 /* [
            'class' => 'kartik\grid\DataColumn',
            'attribute'=>'aseguradora',
            'value'=>'aseguradora',
            'width'=>'300px',
     ],*/
	  [
          'class' => 'kartik\grid\DataColumn',
          'attribute'=>'nro_recibo',
          'value'=>'nro_recibo',
          'width'=>'200px',
    ],
		[
            'attribute' => 'fecha_vigenciadesde',
            'value' => 'fecha_vigenciadesde',
            'format'=>'raw',
            'options' => ['style' => 'width: 25%;'],
            'filter' => DateRangePicker::widget([
                'model' => $searchModel,
                'attribute' => 'fecha_vigenciadesde',
                'useWithAddon'=>false,
                'convertFormat'=>true,
                'pluginOptions'=>[
                    'locale'=>['format'=>'Y-m-d']
                ],
            ])
        ],
        [
              'class' => 'kartik\grid\DataColumn',
              'attribute'=>'comision_total',
              'value'=> 'comision_total',
              'width'=>'200px',
        ],
  	];
 ?>


 <?php

  	echo GridView::widget([
	   'id'=>'recibos-gridview-id',
       'dataProvider' => $dataProvider,
       'filterModel' => $searchModel,
  		'columns' => $gridColumns,
  		'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
        'rowOptions' => function ($model, $key, $index, $grid) {
           return ['id' =>$key,'data-recibo'=>$model->id_recibo,'class' => GridView::TYPE_INFO];
        },//'rbo-'+$key
  		'toolbar' => [
  			'{export}',
  			'{toggleData}'
  		],

  	/*'pjax' => true,
		'pjaxSettings'=>[
           'options'=>[
            'enablePushState'=>false,
            'id'=>'recibos-pjax-id',// UNIQUE PJAX CONTAINER ID
            'formSelector'=>'ddd',
          ],
      ],*/
  		'bordered' => false,
  		'resizableColumns'=>true,
  		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
  		'striped' => true,
  		'condensed' => false,
  		'responsive' => true,
  		'hover' => true,
  		'showPageSummary' => false,
    		'panel' => [
          'heading'=>'<h2 class="panel-title">&nbsp; &nbsp;Recibos</h2>',
    			'type' => GridView::TYPE_INFO
    		],
      ]);
 ?>

<?php Pjax::end() ?>
</div>
<div class="form-group" style="float:left"><!--INICIO BOTONES DEL PANEL-->
<?= Html::Button(Icon::show('more').'Incluir Recibos en la Facturacion', ['class' => 'btn btn-primary','id' =>'cargar-recibos']) ?>
</div><!--FIN BOTONES DEL PANEL-->


<?php
$string = <<<EXP2

$("#cargar-recibos").on("click", function() {
        var keys = $("#recibos-gridview-id").yiiGridView('getSelectedRows');
        var recibos="";
        $.each(keys,function( k, v ) {
           recibos += v+',';
        });
        recibos=recibos.slice(0,-1);
        if(recibos!=''){
           $("#facturacion-recibos").val(recibos);
           $.post("cargando-recibos", { keylist: recibos })
           .done(function(result) {
			   
              $("#facturacion-recibos_numero").val(result.recibos);
            
				
			  $("input[id*='monto_']").not( "#facturacion-monto_iva-disp,#facturacion-monto_iva").val(parseFloat(result.comision).toFixed(2));
				
			 
			  
           });//.donefunction(result)
         $('#modal').modal('hide');
        }else{
          alert('Debe seleccionar al menos un recibo');
        }

});
//data-col-seq
EXP2;
$this->registerJs($string, \yii\web\View::POS_READY);
$string='';
$this->registerCss($string);
?>
