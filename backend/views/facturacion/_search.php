<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\FacturacionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="facturacion-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_facturacion') ?>

    <?= $form->field($model, 'numero_factura') ?>

    <?= $form->field($model, 'numero_control') ?>

    <?= $form->field($model, 'monto_iva') ?>

    <?= $form->field($model, 'monto_islr') ?>
	
	<?= $form->field($model, 'numero_poliza') ?>
	
	<?= $form->field($model, 'nro_recibo') ?>
	
	<?= $form->field($model, 'fecha_vigenciadesde') ?>

    <?php // echo $form->field($model, 'monto_subtotal') ?>

    <?php // echo $form->field($model, 'monto_total') ?>

    <?php // echo $form->field($model, 'id_user_registro') ?>

    <?php // echo $form->field($model, 'id_user_actualizacion') ?>

    <?php // echo $form->field($model, 'fecha_registro') ?>

    <?php // echo $form->field($model, 'fecha_actualizacion') ?>

    <?php // echo $form->field($model, 'descripcion') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
