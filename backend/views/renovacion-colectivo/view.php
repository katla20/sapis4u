<?php

use yii\helpers\Html;
//use yii\widgets\DetailView;
use kartik\detail\DetailView;
use backend\models\Modelos;
use backend\models\Poliza;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\Poliza */

$this->title ='';// $model->id_poliza;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Polizas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poliza-view">

    <h1><?= Html::encode($this->title) ?></h1>

	
	<?php
	$modelCoberturas=Poliza::coberturas($model->id_recibo)->asArray()->all();
	
	
	
	$attributes = [
    /*[
        'group'=>true,
        'label'=>'Version ' .$model->id_version,
        'rowOptions'=>['class'=>'info']
    ],*/
	[
        'columns' => [
		    [
                'attribute'=>'numero_poliza', 
				'label'=>'Número Poliza',
                'format'=>'raw', 
                //'value'=>'<kbd>'.$model->id_version.'</kbd>',
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ], 
            [
                'attribute'=>'fecha_vigenciahasta', 
				'label'=>'Fecha Vigencia',
                'format'=>'raw', 
                //'value'=> $model->idModelos->nombre,
                'valueColOptions'=>['style'=>'width:30%'], 
                'displayOnly'=>true
            ],  
        ],
    ],
    [
        'columns' => [
		    [
                'attribute'=>'aseguradora', 
                'label'=>'Aseguradora',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],
			
			[
                'attribute'=>'producto', 
                'label'=>'Producto',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],
            
        ],
    ],
	[
        'group'=>true,
        'label'=>'DATOS DEL RECIBO',
        'rowOptions'=>['class'=>'success']
    ],
	
	[
        'columns' => [
		    [
                'attribute'=>'nro_recibo', 
				'label'=>'Recibo',
                'format'=>'raw', 
                //'value'=>'<kbd>'.$model->id_version.'</kbd>',
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ], 
            [
                'attribute'=>'comision_total', 
				'label'=>'Comision Total',
                'format'=>'raw', 
                //'value'=> $model->idModelos->nombre,
                'valueColOptions'=>['style'=>'width:30%'], 
                'displayOnly'=>true
            ],  
        ],
    ],
	
	[
        'columns' => [
		    [
               // 'attribute'=>'nro_recibo', 
				'label'=>'Vendedor',
                'format'=>'raw', 
                'value'=>$model->nombre_ven.'  '.$model->apellido_ven,
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ], 
            [
                //'attribute'=>'comision_total', 
				'label'=>'Código',
                'format'=>'raw', 
                'value'=>'<kbd>'.$model->codigo.'</kbd>',
                'valueColOptions'=>['style'=>'width:30%'], 
                'displayOnly'=>true
            ],  
        ],
    ],
	
	[
        'columns' => [
            [
                'attribute'=>'comision_vendedor', 
                'label'=>'Comision Vendedor',
                'displayOnly'=>true,
               // 'valueColOptions'=>['style'=>'width:30%']
            ],
            
        ],
    ],
	
	[
        'group'=>true,
        'label'=>'DATOS DEL CONTRATANTE',
        'rowOptions'=>['class'=>'success']
    ],
	
	[
        'columns' => [
		    [
                'attribute'=>'identificacion', 
				'label'=>'Identificacón',
                'format'=>'raw', 
                //'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ], 
            
        ],
    ],
	
	[
        'columns' => [
		    [
                'attribute'=>'nombre_cli', 
				'label'=>'Nombres',
                'format'=>'raw', 
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ], 
            [
                'attribute'=>'apellido_cli', 
				'label'=>'Apellidos',
                'format'=>'raw', 
                'valueColOptions'=>['style'=>'width:30%'], 
                'displayOnly'=>true
            ],  
        ],
    ],
	
	[
        'group'=>true,
        'label'=>'DATOS DEL VEHICULO',
        'rowOptions'=>['class'=>'success']
    ],
	
	  [
        'columns' => [
		    [
                'attribute'=>'placa',
                'label'=>'Placa',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],
     
            [
                'attribute'=>'marca', 
                'label'=>'Marca',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],
            
        ],
    ],
    [
        'columns' => [
            [
                'attribute'=>'modelo', 
                'label'=>'Modelo',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],
            [
                'attribute'=>'version', 
                'label'=>'Version',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],
              
        ],
    ],
	[
        'columns' => [
            [
                'attribute'=>'anio', 
                'label'=>'Año',
                'displayOnly'=>true,
               // 'valueColOptions'=>['style'=>'width:30%']
            ],
            
        ],
    ],
	[
        'group'=>true,
        'label'=>'COBERTURAS',
        'rowOptions'=>['class'=>'success']
    ],
	];
	// print_r($attributes); 
	
	$total=0;
	foreach ($modelCoberturas as $cober) {
    
	  $total+=$cober['prima'];
	 $attributes[]= 
             [
			'columns' => [
				[
					//'attribute'=>'numero_poliza', 
					'label'=>'Cobertura',
					'format'=>'raw', 
					'value'=>$cober['coberturas'],
					'valueColOptions'=>['style'=>'width:20%'],
					'labelColOptions'=>['style'=>'width:5%'],
					'displayOnly'=>true
				], 
				[
					//'attribute'=>'fecha_vigenciahasta', 
					'label'=>'Suma Asegurada',
					'format'=>'raw', 
					'value'=> $cober['suma_asegurada'],
					'valueColOptions'=>['style'=>'width:10%'], 
					'labelColOptions'=>['style'=>'width:10%'],
					'displayOnly'=>true
				], 
				[
					//'attribute'=>'fecha_vigenciahasta', 
					'label'=>'Prima',
					'format'=>'raw', 
					'value'=> $cober['prima'],
					'valueColOptions'=>['style'=>'width:10%'],
                    'labelColOptions'=>['style'=>'width:10%'],					
					'displayOnly'=>true
				], 	
			],
		];
      
     }
	 
	 $attributes[]= 
             [
			'columns' => [
			
				[
					//'attribute'=>'fecha_vigenciahasta', 
					'label'=>'Total',
					'format'=>'raw', 
					'value'=> $total,
					'valueColOptions'=>['style'=>'width:10%'],
                    'labelColOptions'=>['style'=>'width:55%'],					
					'displayOnly'=>true
				], 	
			],
		];
	 
	// print_r($attributes);  
	
	
//print_r($model);
	// View file rendering the widget
		echo DetailView::widget([
		'model' => $model,
		'attributes' => $attributes,
		'mode' => 'view',
		'enableEditMode'=> false,
		//'bordered' => 'bordered',
		//'striped' => $striped,
		//'condensed' => $condensed,
		'responsive' => 'responsive',
		//'hover' => $hover,
		//'hAlign'=>$hAlign,
		//'vAlign'=>$vAlign,
		//'fadeDelay'=>$fadeDelay,
		'deleteOptions'=>[ // your ajax delete parameters
			'params' => ['id' => 1000, 'kvdelete'=>true],
		],
		'panel'=>[
        'heading'=>'DETALLE POLIZA ['.$model->tipo_recibo.']',
        'type'=>DetailView::TYPE_INFO,
		
        ],
		'container' => ['id'=>'kv-demo'],
		//'formOptions' => ['action' => Url::current(['update' => 'kv-demo'])] // your action to delete
	]);
	
	
	$string="

         .etica{
			background-color:#B2B9BF;
			color:#FFFFFF;
			font-size: 13px;
			font-weight: bold;
			border-left: 0px solid #EEF6FF; 
		 }";
      $this->registerCss($string);
	
	

	?>

</div>

