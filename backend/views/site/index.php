
<?php

/* @var $this yii\web\View */
//https://almsaeedstudio.com/preview
use yii\helpers\Html;
use yii\helpers\BaseHtml;
use kartik\icons\Icon;
use common\models\ConsultaPoliza;
use yii\helpers\Url;//linea para el asistente de url keyla bullon
use backend\models\Graficas;
use yii\web\Response;
use backend\models\ConsultaReclamos;


$this->title = '';

$renov=ConsultaPoliza::getRenovaciones();
$periodo_g=ConsultaPoliza::getPerido_gracia();
$vencidas=ConsultaPoliza::getVencidas();
$grafica=Graficas::getTotales();

//echo $grafica;exit('salio');

$reclaIns=ConsultaReclamos::getReclamos('PENDIENTE POR INSPECCION');
$reclaRe=ConsultaReclamos::getReclamos('POR RECAUDOS');
$reclaOr=ConsultaReclamos::getReclamos('POR ORDEN');


$pro='[]';
$torta='[]';
$ase='[]';
$re='[]';

  if($grafica > 0){

      $torta=[];
      $ase=[];
      $pro=[];
      $re=[];

      foreach ($grafica as $ase) {

       $aseguradoras[]=$ase['aseguradora'];
       $produccion[]=$ase['comision'];
       $reclamos[]=$ase['reclamo'];
       $torta[] = ['name' => $ase['aseguradora'], 'y' => (float)$ase['comision']];

      }

      $torta=json_encode($torta);
      $ase=json_encode($aseguradoras);
      $pro=str_replace('"','',json_encode($produccion));
      $re=str_replace('"','',json_encode($reclamos));

  }

?>

  <section class="content">
	<!--<div class="row">
		<div class="col-md-12">
			 <div id="container6"></div>
		</div>
	</div>-->
  <div class="row">
    <div class="col-sm-3">
      <div class="jumbotron">
        <a href="<?=Url::toRoute('poliza/renovacion')?>">
            <div class="info-box">
                         <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o" ></i></span>
                         <div class="info-box-content">
                           <span class="info-box-text">Renovaciones</span>
                           <span class="info-box-number"><?=$renov?></span>
                         </div><!-- /.info-box-content -->
            </div>
        </a>
      </div>
    </div>
    <div class="col-sm-3">
      <div class="jumbotron">
        <a href="<?=Url::toRoute('poliza/periodo')?>">
            <div class="info-box">
                         <span class="info-box-icon bg-teal"><i class="fa fa-hourglass-o"></i></span>
                         <div class="info-box-content">
                           <span class="info-box-text">Periodo de Gracia</span>
                           <span class="info-box-number"><?=$periodo_g?></span>

                         </div><!-- /.info-box-content -->
            </div>
        </a>
      </div>
    </div>
    <!--<div class="col-sm-3">
      <div class="jumbotron">
          <div class="info-box">
                       <span class="info-box-icon bg-yellow"><i class="fa fa-money"></i></span>
                       <div class="info-box-content">
                         <span class="info-box-text">Facturacion Pendiente</span>
                         <span class="info-box-number">15</span>
                       </div>
          </div>
      </div>
    </div>-->
    <div class="col-sm-3">
      <div class="jumbotron">
        <a href="<?=Url::toRoute('poliza/vencidas')?>">
            <div class="info-box">
                         <span class="info-box-icon bg-red"><i class="fa fa-warning"></i></span>
                         <div class="info-box-content">
                           <span class="info-box-text">Vencidas</span>
                           <span class="info-box-number"><?=$vencidas?></span>
                         </div><!-- /.info-box-content -->
            </div>
        </a>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
         <div id="container3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
	<div class="col-sm-6">
         <div id="container5" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
  </div>
  <!--<div class="row">
    <div class="col-sm-4 col-md-3">
      <div class="jumbotron">
        <a href="<?=Url::toRoute('reclamos/reclamos-estatus?estatus=PENDIENTE POR INSPECCION')?>">
            <div class="info-box">
                         <span class="info-box-icon bg-aqua"><i class="fa fa-bullhorn" ></i></span>
                         <div class="info-box-content">
                           <span class="info-box-text">Reclamos Por Inspeccion</span>
                           <span class="info-box-number"><?=$reclaIns?></span>
                         </div>
            </div>
        </a>
      </div>
    </div>
     <div class="col-sm-4 col-md-3">
      <div class="jumbotron">
        <a href="<?=Url::toRoute('reclamos/reclamos-estatus?estatus=POR RECAUDOS')?>">
            <div class="info-box">
                         <span class="info-box-icon bg-teal"><i class="fa fa-newspaper-o"></i></span>
                         <div class="info-box-content">
                           <span class="info-box-text">Por Recaudos</span>
                           <span class="info-box-number"><?=$reclaRe?></span>

                         </div>
            </div>
        </a>
      </div>
    </div>
    <div class="col-sm-4 col-md-3">
      <div class="jumbotron">
           <a href="<?=Url::toRoute('reclamos/reclamos-estatus?estatus=POR ORDEN')?>">
          <div class="info-box">
                       <span class="info-box-icon bg-yellow"><i class="fa fa-money"></i></span>
                       <div class="info-box-content">
                         <span class="info-box-text">Por Orden</span>
                         <span class="info-box-number"><?=$reclaOr?></span>
                       </div>
          </div>
      </div>
    </div>
  </div>-->
  <!--<section>
   <div class="sprite sprite-detective"></div>
 </section>-->

  <!--<div class="row">
    <div class="col-sm-4 col-md-6">
         <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>-->
    <!--<div class="col-sm-4 col-md-6">
         <div id="container2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
  </div>-->

  <!--<div class="row">
    <div class="col-sm-4 col-md-6">
      <blockquote class="quote-box">
        <p class="quotation-mark">
          “
        </p>
        <p class="quote-text">
          Don't believe anything that you read on the internet, it may be fake.
        </p>
        <hr>
        <div class="blog-post-actions">
          <p class="blog-post-bottom pull-left">
            Abraham Lincoln
          </p>
        </div>
      </blockquote>
    </div>
  </div>-->
  <!--<div class="row">
    <div class="col-sm-4 col-md-6">
      <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" />
         <div class="container">
 	         <div class="row">
               <div class="col-md-4 col-md-offset-4">
                 <div class="material-button-anim">
                   <ul class="list-inline" id="options">
                     <li class="option">
                       <button class="material-button option1" type="button">
                         <span class="fa fa-phone" aria-hidden="true"></span>
                       </button>
                     </li>
                     <li class="option">
                       <button class="material-button option2" type="button">
                         <span class="fa fa-envelope-o" aria-hidden="true"></span>
                       </button>
                     </li>
                     <li class="option">
                       <button class="material-button option3" type="button">
                         <span class="fa fa-pencil" aria-hidden="true"></span>
                       </button>
                     </li>
                   </ul>
                   <button class="material-button material-button-toggle" type="button">
                     <span class="fa fa-plus" aria-hidden="true"></span>
                   </button>
                 </div>
               </div>
 	         </div>
 	      </div>
    </div>
  </div>-->
  <!--<div class="row">
      <div class="col-sm-4 col-md-6">
          <?php
          /*$wizard_config = [
                  'id' => 'stepwizard',
                  'steps' => [
                      1 => [
                          'title' => 'Cargar Auto',
                          'icon' => 'fa fa-car',
                          'content' => '<h3>Step 1</h3>This is step 1',
                          'buttons' => [
                              'next' => [
                                  'title' => 'Forward',
                                  'options' => [
                                      'class' => 'disabled'
                                  ],
                               ],
                           ],
                      ],
                      2 => [
                          'title' => 'Cargar Coberturas',
                          'icon' => 'glyphicon glyphicon-plus',
                          'content' => '<h3>Step 2</h3>This is step 2',
                          //'skippable' => true,
                      ],
                      3 => [
                          'title' => 'Cargar Comisiones',
                          'icon' => 'glyphicon glyphicon-piggy-bank',
                          'content' => '<h3>Step 3</h3>This is step 3',
                      ],
                  ],
                  'complete_content' => "You are done!", // Optional final screen
                  'start_step' => 1, // Optional, start with a specific step
              ];*/
          ?>

<?//= \drsdre\wizardwidget\WizardWidget::widget($wizard_config); ?>

   </div>
  </div>-->

  <!--<div class="row">
    <div class="col-sm-4 col-md-6">

    <div class="container">
      	<div class="row">
      		<section>
              <div class="wizard">
                  <div class="wizard-inner">
                      <div class="connecting-line"></div>
                      <ul class="nav nav-tabs" role="tablist">

                          <li role="presentation" class="active">
                              <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                                  <span class="round-tab">
                                      <i class="glyphicon glyphicon-folder-open"></i>
                                  </span>
                              </a>
                          </li>

                          <li role="presentation" class="disabled">
                              <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                                  <span class="round-tab">
                                      <i class="glyphicon glyphicon-pencil"></i>
                                  </span>
                              </a>
                          </li>
                          <li role="presentation" class="disabled">
                              <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                                  <span class="round-tab">
                                      <i class="glyphicon glyphicon-picture"></i>
                                  </span>
                              </a>
                          </li>

                          <li role="presentation" class="disabled">
                              <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                                  <span class="round-tab">
                                      <i class="glyphicon glyphicon-ok"></i>
                                  </span>
                              </a>
                          </li>
                      </ul>
                  </div>

                  <form role="form">
                      <div class="tab-content">
                          <div class="tab-pane active" role="tabpanel" id="step1">
                              <h3>Step 1</h3>
                              <p>This is step 1</p>
                              <ul class="list-inline pull-right">
                                  <li><button type="button" class="btn btn-primary next-step">Save and continue</button></li>
                              </ul>
                          </div>
                          <div class="tab-pane" role="tabpanel" id="step2">
                              <h3>Step 2</h3>
                              <p>This is step 2</p>
                              <ul class="list-inline pull-right">
                                  <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                                  <li><button type="button" class="btn btn-primary next-step">Save and continue</button></li>
                              </ul>
                          </div>
                          <div class="tab-pane" role="tabpanel" id="step3">
                              <h3>Step 3</h3>
                              <p>This is step 3</p>
                              <ul class="list-inline pull-right">
                                  <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                                  <li><button type="button" class="btn btn-default next-step">Skip</button></li>
                                  <li><button type="button" class="btn btn-primary btn-info-full next-step">Save and continue</button></li>
                              </ul>
                          </div>
                          <div class="tab-pane" role="tabpanel" id="complete">
                              <h3>Complete</h3>
                              <p>You have successfully completed all steps.</p>
                          </div>
                          <div class="clearfix"></div>
                      </div>
                  </form>
              </div>
          </section>
         </div>
      </div>
    </div>
  </div>-->

  <!--<div class="row">
    <div class="col-sm-4 col-md-6">
         <div id="container4" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
  </div>-->

          <!-- START ACCORDION & CAROUSEL-->
         <!-- <h2 class="page-header">Bootstrap Carousel</h2>
          <div class="row">
            <div class="col-md-12">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Carousel</h3>
                </div>
                <div class="box-body">
                  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                      <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                      <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                      <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                    </ol>
                    <div class="carousel-inner">
                      <div class="item active">
                        <img src="http://placehold.it/900x500/39CCCC/ffffff&text=I+Love+Bootstrap" alt="First slide">
                        <div class="carousel-caption">
                          First Slide
                        </div>
                      </div>
                      <div class="item">
                        <img src="http://placehold.it/900x500/3c8dbc/ffffff&text=I+Love+Bootstrap" alt="Second slide">
                        <div class="carousel-caption">
                          Second Slide
                        </div>
                      </div>
                      <div class="item">
                        <img src="http://placehold.it/900x500/f39c12/ffffff&text=I+Love+Bootstrap" alt="Third slide">
                        <div class="carousel-caption">
                          Third Slide
                        </div>
                      </div>
                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                      <span class="fa fa-angle-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                      <span class="fa fa-angle-right"></span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>-->
    </section>
<?php

$string = <<<EXP2
$(function () {
    $('#container').highcharts({
        title: {
            text: 'Monthly Average Temperature',
            x: -20 //center
        },
        subtitle: {
            text: 'Source: WorldClimate.com',
            x: -20
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: 'Temperature (°C)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: '°C'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Tokyo',
            data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
        }, {
            name: 'New York',
            data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
        }, {
            name: 'Berlin',
            data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
        }, {
            name: 'London',
            data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
        }]
    });
});
$(function () {
    $('#container2').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'Average Monthly Temperature and Rainfall in Tokyo'
        },
        subtitle: {
            text: 'Source: WorldClimate.com'
        },
        xAxis: [{
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}°C',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: 'Temperature',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }, { // Secondary yAxis
            title: {
                text: 'Rainfall',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value} mm',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 120,
            verticalAlign: 'top',
            y: 100,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [{
            name: 'Rainfall',
            type: 'column',
            yAxis: 1,
            data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
            tooltip: {
                valueSuffix: ' mm'
            }

        }, {
            name: 'Temperature',
            type: 'spline',
            data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6],
            tooltip: {
                valueSuffix: '°C'
            }
        }]
    });
});

$(function () {
    $('#container3').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Produccion vs Reclamo'
        },
        subtitle: {
            text: 'fuente:cefois.com'
        },
        xAxis: {
            categories: $ase,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'cantidad'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} bs</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'produccion',
            data: $pro

        }, {
            name: 'Reclamos',
            data: $re

        }]
    });
});

$(function () {
    $('#container4').highcharts({
        chart: {
            backgroundColor: 'white',
            events: {
                load: function () {

                    // Draw the flow chart
                    var ren = this.renderer,
                        colors = Highcharts.getOptions().colors,
                        rightArrow = ['M', 0, 0, 'L', 100, 0, 'L', 95, 5, 'M', 100, 0, 'L', 95, -5],
                        leftArrow = ['M', 100, 0, 'L', 0, 0, 'L', 5, 5, 'M', 0, 0, 'L', 5, -5];



                    // Separator, client from service
                    ren.path(['M', 120, 40, 'L', 120, 330])
                        .attr({
                            'stroke-width': 2,
                            stroke: 'silver',
                            dashstyle: 'dash'
                        })
                        .add();

                    // Separator, CLI from service
                    ren.path(['M', 420, 40, 'L', 420, 330])
                        .attr({
                            'stroke-width': 2,
                            stroke: 'silver',
                            dashstyle: 'dash'
                        })
                        .add();

                    // Headers
                    ren.label('Web client', 20, 40)
                        .css({
                            fontWeight: 'bold'
                        })
                        .add();
                    ren.label('Web service / CLI', 220, 40)
                        .css({
                            fontWeight: 'bold'
                        })
                        .add();
                    ren.label('Command line client', 440, 40)
                        .css({
                            fontWeight: 'bold'
                        })
                        .add();

                    // SaaS client label
                    ren.label('SaaS client<br/>(browser or<br/>script)', 10, 82)
                        .attr({
                            fill: colors[0],
                            stroke: 'white',
                            'stroke-width': 2,
                            padding: 5,
                            r: 5
                        })
                        .css({
                            color: 'white'
                        })
                        .add()
                        .shadow(true);

                    // Arrow from SaaS client to Phantom JS
                    ren.path(rightArrow)
                        .attr({
                            'stroke-width': 2,
                            stroke: colors[3]
                        })
                        .translate(95, 95)
                        .add();

                    ren.label('POST options in JSON', 90, 75)
                        .css({
                            fontSize: '10px',
                            color: colors[3]
                        })
                        .add();

                    ren.label('PhantomJS', 210, 82)
                        .attr({
                            r: 5,
                            width: 100,
                            fill: colors[1]
                        })
                        .css({
                            color: 'white',
                            fontWeight: 'bold'
                        })
                        .add();

                    // Arrow from Phantom JS to Batik
                    ren.path(['M', 250, 110, 'L', 250, 185, 'L', 245, 180, 'M', 250, 185, 'L', 255, 180])
                        .attr({
                            'stroke-width': 2,
                            stroke: colors[3]
                        })
                        .add();

                    ren.label('SVG', 255, 120)
                        .css({
                            color: colors[3],
                            fontSize: '10px'
                        })
                        .add();

                    ren.label('Batik', 210, 200)
                        .attr({
                            r: 5,
                            width: 100,
                            fill: colors[1]
                        })
                        .css({
                            color: 'white',
                            fontWeight: 'bold'
                        })
                        .add();

                    // Arrow from Batik to SaaS client
                    ren.path(['M', 235, 185, 'L', 235, 155, 'C', 235, 130, 235, 130, 215, 130,
                              'L', 95, 130, 'L', 100, 125, 'M', 95, 130, 'L', 100, 135])
                        .attr({
                            'stroke-width': 2,
                            stroke: colors[3]
                        })
                        .add();

                    ren.label('Rasterized image', 100, 110)
                        .css({
                            color: colors[3],
                            fontSize: '10px'
                        })
                        .add();

                    // Browser label
                    ren.label('Browser<br/>running<br/>Highcharts', 10, 180)
                        .attr({
                            fill: colors[0],
                            stroke: 'white',
                            'stroke-width': 2,
                            padding: 5,
                            r: 5
                        })
                        .css({
                            color: 'white',
                            width: '100px'
                        })
                        .add()
                        .shadow(true);



                    // Arrow from Browser to Batik
                    ren.path(rightArrow)
                        .attr({
                            'stroke-width': 2,
                            stroke: colors[1]
                        })
                        .translate(95, 205)
                        .add();

                    ren.label('POST SVG', 110, 185)
                        .css({
                            color: colors[1],
                            fontSize: '10px'
                        })
                        .add();

                    // Arrow from Batik to Browser
                    ren.path(leftArrow)
                        .attr({
                            'stroke-width': 2,
                            stroke: colors[1]
                        })
                        .translate(95, 215)
                        .add();

                    ren.label('Rasterized image', 100, 215)
                        .css({
                            color: colors[1],
                            fontSize: '10px'
                        })
                        .add();

                    // Script label
                    ren.label('Script', 450, 82)
                        .attr({
                            fill: colors[2],
                            stroke: 'white',
                            'stroke-width': 2,
                            padding: 5,
                            r: 5
                        })
                        .css({
                            color: 'white',
                            width: '100px'
                        })
                        .add()
                        .shadow(true);

                    // Arrow from Script to PhantomJS
                    ren.path(leftArrow)
                        .attr({
                            'stroke-width': 2,
                            stroke: colors[2]
                        })
                        .translate(330, 90)
                        .add();

                    ren.label('Command', 340, 70)
                        .css({
                            color: colors[2],
                            fontSize: '10px'
                        })
                        .add();

                    // Arrow from PhantomJS to Script
                    ren.path(rightArrow)
                        .attr({
                            'stroke-width': 2,
                            stroke: colors[2]
                        })
                        .translate(330, 100)
                        .add();

                    ren.label('Rasterized image', 330, 100)
                        .css({
                            color: colors[2],
                            fontSize: '10px'
                        })
                        .add();


                }
            }
        },
        title: {
            text: 'Highcharts export server overview',
            style: {
                color: 'black'
            }
        }

    });
});

$(function () {

    // Radialize the colors
    Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
        };
    });

    // Build the chart
    $('#container5').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Produccion por Aseguradora'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}% </br> monto: {point.y:.1f} Bs.</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                    connectorColor: 'silver'
                }
            }
        },
        series: [{
            name: 'Porcentaje',
            data: $torta
        }]
    });
});

$(function () {

    /**
     * Get the current time
     */
    function getNow() {
        var now = new Date();

        return {
            hours: now.getHours() + now.getMinutes() / 60,
            minutes: now.getMinutes() * 12 / 60 + now.getSeconds() * 12 / 3600,
            seconds: now.getSeconds() * 12 / 60
        };
    }

    /**
     * Pad numbers
     */
    function pad(number, length) {
        // Create an array of the remaining length + 1 and join it with 0's
        return new Array((length || 2) + 1 - String(number).length).join(0) + number;
    }

    var now = getNow();

    // Create the chart
    $('#container6').highcharts({

        chart: {
            type: 'gauge',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false,
            height: 130
        },

        credits: {
            enabled: false
        },

        title: {
            text: ''
        },

        pane: {
            background: [{
                // default background
            }, {
                // reflex for supported browsers
                backgroundColor: Highcharts.svg ? {
                    radialGradient: {
                        cx: 0.5,
                        cy: -0.4,
                        r: 1.9
                    },
                    stops: [
                        [0.5, 'rgba(255, 255, 255, 0.2)'],
                        [0.5, 'rgba(200, 200, 200, 0.2)']
                    ]
                } : null
            }]
        },

        yAxis: {
            labels: {
                distance: -20
            },
            min: 0,
            max: 12,
            lineWidth: 0,
            showFirstLabel: false,

            minorTickInterval: 'auto',
            minorTickWidth: 1,
            minorTickLength: 5,
            minorTickPosition: 'inside',
            minorGridLineWidth: 0,
            minorTickColor: '#666',

            tickInterval: 1,
            tickWidth: 2,
            tickPosition: 'inside',
            tickLength: 10,
            tickColor: '#666',
            title: {
                text: '',
                style: {
                    color: '#BBB',
                    fontWeight: 'normal',
                    fontSize: '5px',
                    lineHeight: '8px'
                },
                y: 10
            }
        },
		xAxis: {
			labels: {
                enabled: false,
            },
            title: {
                text: '',
                style: {
                    color: '#BBB',
                    fontWeight: 'normal',
                    fontSize: '5px',
                    lineHeight: '8px'
                },
            }
        },

        tooltip: {
            formatter: function () {
                return this.series.chart.tooltipText;
            }
        },

        series: [{
            data: [{
                id: 'hour',
                y: now.hours,
                dial: {
                    radius: '60%',
                    baseWidth: 4,
                    baseLength: '95%',
                    rearLength: 0
                }
            }, {
                id: 'minute',
                y: now.minutes,
                dial: {
                    baseLength: '95%',
                    rearLength: 0
                }
            }, {
                id: 'second',
                y: now.seconds,
                dial: {
                    radius: '100%',
                    baseWidth: 1,
                    rearLength: '20%'
                }
            }],
            animation: false,
            dataLabels: {
                enabled: false
            }
        }]
    },

        // Move
        function (chart) {
            setInterval(function () {

                now = getNow();

                var hour = chart.get('hour'),
                    minute = chart.get('minute'),
                    second = chart.get('second'),
                    // run animation unless we're wrapping around from 59 to 0
                    animation = now.seconds === 0 ?
                        false :
                        {
                            easing: 'easeOutBounce'
                        };

                // Cache the tooltip text
                chart.tooltipText =
                    pad(Math.floor(now.hours), 2) + ':' +
                    pad(Math.floor(now.minutes * 5), 2) + ':' +
                    pad(now.seconds * 5, 2);

                hour.update(now.hours, true, animation);
                minute.update(now.minutes, true, animation);
                second.update(now.seconds, true, animation);

            }, 1000);

        });
});

/**
 * Easing function from https://github.com/danro/easing-js/blob/master/easing.js
 */
Math.easeOutBounce = function (pos) {
    if ((pos) < (1 / 2.75)) {
        return (7.5625 * pos * pos);
    }
    if (pos < (2 / 2.75)) {
        return (7.5625 * (pos -= (1.5 / 2.75)) * pos + 0.75);
    }
    if (pos < (2.5 / 2.75)) {
        return (7.5625 * (pos -= (2.25 / 2.75)) * pos + 0.9375);
    }
    return (7.5625 * (pos -= (2.625 / 2.75)) * pos + 0.984375);
};

EXP2;

$this->registerJs($string, \yii\web\View::POS_READY);

$string2 = <<<EXP2
$(document).ready(function () {
    $('.material-button-toggle').click(function () {
        $(this).toggleClass('open');
        $('.option').toggleClass('scale-on');
    });
});
EXP2;
$this->registerJs($string2, \yii\web\View::POS_READY);



$string3 = <<<EXP3
$(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();

    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var target = $(e.target);

    });

    $(".next-step").click(function (e) {

        var active = $('.wizard .nav-tabs li.active');
        active.next().removeClass('disabled');
        nextTab(active);

    });
    $(".prev-step").click(function (e) {

        var active = $('.wizard .nav-tabs li.active');
        prevTab(active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}
EXP3;
//$this->registerJs($string3, \yii\web\View::POS_READY);

$this->registerCss('blockquote{
    border-left:none
 }
.quote-badge{
    background-color: rgba(0, 0, 0, 0.2);
}

.quote-box{

    overflow: hidden;
    margin-top: -50px;
    padding-top: -100px;
    border-radius: 17px;
    background-color: #4ADFCC;
    margin-top: 25px;
    color:white;
    width: 325px;
    box-shadow: 2px 2px 2px 2px #E0E0E0;

}

.quotation-mark{

    margin-top: -10px;
    font-weight: bold;
    font-size:100px;
    color:white;
    font-family: "Times New Roman", Georgia, Serif;

}

.quote-text{

    font-size: 19px;
    margin-top: -65px;
}


/*opciones en azul*/
/*-------------------------
Please follow me @maridlcrmn
/*-------------------------*/

.material-button-anim {
  position: relative;
  padding: 127px 15px 27px;
  text-align: center;
  max-width: 320px;
  margin: 0 auto 20px;
}

.material-button {
    position: relative;
    top: 0;
    z-index: 1;
    width: 70px;
    height: 70px;
    font-size: 1.5em;
    color: #fff;
    background: #2C98DE;
    border: none;
    border-radius: 50%;
    box-shadow: 0 3px 6px rgba(0,0,0,.275);
    outline: none;
}
.material-button-toggle {
    z-index: 3;
    width: 90px;
    height: 90px;
    margin: 0 auto;
}
.material-button-toggle span {
    -webkit-transform: none;
    transform:         none;
    -webkit-transition: -webkit-transform .175s cubic-bazier(.175,.67,.83,.67);
    transition:         transform .175s cubic-bazier(.175,.67,.83,.67);
}
.material-button-toggle.open {
    -webkit-transform: scale(1.3,1.3);
    transform:         scale(1.3,1.3);
    -webkit-animation: toggleBtnAnim .175s;
    animation:         toggleBtnAnim .175s;
}
.material-button-toggle.open span {
    -webkit-transform: rotate(45deg);
    transform:         rotate(45deg);
    -webkit-transition: -webkit-transform .175s cubic-bazier(.175,.67,.83,.67);
    transition:         transform .175s cubic-bazier(.175,.67,.83,.67);
}

#options {
  height: 70px;
}
.option {
    position: relative;
}
.option .option1,
.option .option2,
.option .option3 {
    filter: blur(5px);
    -webkit-filter: blur(5px);
    -webkit-transition: all .175s;
    transition:         all .175s;
}
.option .option1 {
    -webkit-transform: translate3d(90px,90px,0) scale(.8,.8);
    transform:         translate3d(90px,90px,0) scale(.8,.8);
}
.option .option2 {
    -webkit-transform: translate3d(0,90px,0) scale(.8,.8);
    transform:         translate3d(0,90px,0) scale(.8,.8);
}
.option .option3 {
    -webkit-transform: translate3d(-90px,90px,0) scale(.8,.8);
    transform:         translate3d(-90px,90px,0) scale(.8,.8);
}
.option.scale-on .option1,
.option.scale-on .option2,
.option.scale-on .option3 {
    filter: blur(0);
    -webkit-filter: blur(0);
    -webkit-transform: none;
    transform:         none;
    -webkit-transition: all .175s;
    transition:         all .175s;
}
.option.scale-on .option2 {
    -webkit-transform: translateY(-28px) translateZ(0);
    transform:         translateY(-28px) translateZ(0);
    -webkit-transition: all .175s;
    transition:         all .175s;
}

@keyframes toggleBtnAnim {
    0% {
        -webkit-transform: scale(1,1);
        transform:         scale(1,1);
    }
    25% {
        -webkit-transform: scale(1.4,1.4);
        transform:         scale(1.4,1.4);
    }
    75% {
        -webkit-transform: scale(1.2,1.2);
        transform:         scale(1.2,1.2);
    }
    100% {
        -webkit-transform: scale(1.3,1.3);
        transform:         scale(1.3,1.3);
    }
}
@-webkit-keyframes toggleBtnAnim {
    0% {
        -webkit-transform: scale(1,1);
        transform:         scale(1,1);
    }
    25% {
        -webkit-transform: scale(1.4,1.4);
        transform:         scale(1.4,1.4);
    }
    75% {
        -webkit-transform: scale(1.2,1.2);
        transform:         scale(1.2,1.2);
    }
    100% {
        -webkit-transform: scale(1.3,1.3);
        transform:         scale(1.3,1.3);
    }
}
/*fin de opciones en azul*/


/*barra de reccorido*/
.wizard {
    margin: 20px auto;
    background: #fff;
}

    .wizard .nav-tabs {
        position: relative;
        margin: 40px auto;
        margin-bottom: 0;
        border-bottom-color: #e0e0e0;
    }

    .wizard > div.wizard-inner {
        position: relative;
    }

.connecting-line {
    height: 2px;
    background: #e0e0e0;
    position: absolute;
    width: 80%;
    margin: 0 auto;
    left: 0;
    right: 0;
    top: 50%;
    z-index: 1;
}

.wizard .nav-tabs > li.active > a, .wizard .nav-tabs > li.active > a:hover, .wizard .nav-tabs > li.active > a:focus {
    color: #555555;
    cursor: default;
    border: 0;
    border-bottom-color: transparent;
}

span.round-tab {
    width: 70px;
    height: 70px;
    line-height: 70px;
    display: inline-block;
    border-radius: 100px;
    background: #fff;
    border: 2px solid #e0e0e0;
    z-index: 2;
    position: absolute;
    left: 0;
    text-align: center;
    font-size: 25px;
}
span.round-tab i{
    color:#555555;
}
.wizard li.active span.round-tab {
    background: #fff;
    border: 2px solid #5bc0de;

}
.wizard li.active span.round-tab i{
    color: #5bc0de;
}

span.round-tab:hover {
    color: #333;
    border: 2px solid #333;
}

.wizard .nav-tabs > li {
    width: 25%;
}

.wizard li:after {
    content: " ";
    position: absolute;
    left: 46%;
    opacity: 0;
    margin: 0 auto;
    bottom: 0px;
    border: 5px solid transparent;
    border-bottom-color: #5bc0de;
    transition: 0.1s ease-in-out;
}

.wizard li.active:after {
    content: " ";
    position: absolute;
    left: 46%;
    opacity: 1;
    margin: 0 auto;
    bottom: 0px;
    border: 10px solid transparent;
    border-bottom-color: #5bc0de;
}

.wizard .nav-tabs > li a {
    width: 70px;
    height: 70px;
    margin: 20px auto;
    border-radius: 100%;
    padding: 0;
}

    .wizard .nav-tabs > li a:hover {
        background: transparent;
    }

.wizard .tab-pane {
    position: relative;
    padding-top: 50px;
}

.wizard h3 {
    margin-top: 0;
}

@media( max-width : 585px ) {

    .wizard {
        width: 90%;
        height: auto !important;
    }

    span.round-tab {
        font-size: 16px;
        width: 50px;
        height: 50px;
        line-height: 50px;
    }

    .wizard .nav-tabs > li a {
        width: 50px;
        height: 50px;
        line-height: 50px;
    }

    .wizard li.active:after {
        content: " ";
        position: absolute;
        left: 35%;
    }
}

/*barra de reccorido*/



');
?>
