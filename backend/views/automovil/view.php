<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use backend\models\Modelos;
use backend\models\Automovil;
use yii\helpers\ArrayHelper;
use kartik\icons\Icon;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\models\Automovil */

$directoryUploads = '/uploads/automovil/';
$directoryUploadsAll = $ruta.'/uploads/automovil/';



$this->title = '';
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
            <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
            <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Vehiculo')?></div></a>
            <a href="#" class="btn btn-default"><div>Datos del vehiculo</div></a>
</div>
</br></br>
<div class="automovil-view">
    <?php
	$attributes = [
    /*[
        'group'=>true,
        'label'=>'Aseguradora',
        'rowOptions'=>['class'=>'info']
    ],*/
    [
        'columns' => [

          [
                'attribute'=>'marca',
                'label'=>'Marca',
				        'format'=>'raw',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
          ],

			    [
				        'attribute'=>'placa',
                'label'=>'Placa',
				        'format'=>'raw',
                'value'=>'<kbd>'.$model->placa.'</kbd>',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
          ],

        ],
    ],
    [
        'columns' => [
            [
                'attribute'=>'modelo',
                'label'=>'Modelo',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],
            [
                'attribute'=>'color',
                'label'=>'Color',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],

        ],
    ],
	[
        'columns' => [
            [
				 'attribute'=>'version',
                'label'=>'Versión',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],
            [
                'attribute'=>'serial_motor',
                'label'=>'Serial del Motor',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],

        ],

    ],

	[
        'columns' => [
            [
				'attribute'=>'anio',
                'label'=>'Año',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],
            [
                'attribute'=>'serial_carroceria',
                'label'=>'Serial de la Carroceria',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],

        ],
    ],

	[
        'columns' => [
            [
				'attribute'=>'tipo_vehiculo',
                'label'=>'Tipo de Vehiculo',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],
            [
				'attribute'=>'uso',
                'label'=>'Uso',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],

        ],
    ],
   /* [
        'group'=>true,
        'label'=>'DOCUMENTOS',
        'rowOptions'=>['class'=>'success']
    ],*/
    
    ];
    
    if(count($datosDocumentos)){
 	
		 	$options = [
		    'title' => 'Documento',
		    'target' => '_blank',
		    'alt' => 'enlase a Documento',
		    ];
            $attributes[]=
				[
							'group'=>true,
							'label'=>'DOCUMENTOS',
							'rowOptions'=>['class'=>'success']
				];
        
   
    $options = [
    'title' => 'Documento',
    'target' => '_blank',
    'alt' => 'enlase a Documento',
    ];
    
 	foreach ($datosDocumentos as $documento) {
 
         $attributes []= 
         [
    
            'columns' => [
                [
    				'attribute'=>'tipo_vehiculo',
                    'label'=>'Descripcion Documento',
                    'value'=>$documento['ruta'],
                    'displayOnly'=>true,
                    'valueColOptions'=>['style'=>'width:40%']
                ],
                [
    				'label'=>'',
    				'format'=>'raw', 
    				'value'=>"<a href='".Url::toRoute(["automovil/download", "file" => $documento['ruta']])."' 'target'= '_blank' >Descargar archivo</a>",
                    'format' => ['raw'],
    				'valueColOptions'=>['style'=>'width:50%'],
    				'labelColOptions'=>['style'=>'display:none;'],
    				'displayOnly'=>true
    			], 
    
            ],
        ];
    
 	}
 	
    }

	// View file rendering the widget

	echo DetailView::widget([
		'model' => $model,
		'attributes' => $attributes,
		'mode' => 'view',
		'enableEditMode'=> false,
		//'bordered' => 'bordered',
		//'striped' => $striped,
		//'condensed' => $condensed,
		'responsive' => 'responsive',
		//'hover' => $hover,
		//'hAlign'=>$hAlign,
		//'vAlign'=>$vAlign,
		//'fadeDelay'=>$fadeDelay,
		'deleteOptions'=>[ // your ajax delete parameters
			'params' => ['id' => 1000, 'kvdelete'=>true],
		],
		'panel'=>[
        'heading'=>'DATOS DEL VEHICULO',
        'type'=>DetailView::TYPE_INFO,

        ],
		'container' => ['id'=>'kv-demo'],
		//'formOptions' => ['action' => Url::current(['update' => 'kv-demo'])] // your action to delete
	]);


    ?>


</div>
