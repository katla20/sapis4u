<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\ArrayHelper;
use backend\models\Modelos;
use backend\models\Marcas;
use backend\models\Version;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\SearchAutomovil */
/* @var $dataProvider yii\data\ActiveDataProvider */
/*https://github.com/kartik-v/yii2-widget-select2/issues/86 */

$this->title = '';//Yii::t('app', 'Automoviles');
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
<a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
</div>
</br></br>
<div class="automovil-index">
    <?  $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        [
          'attribute'=>'id_marca',
          'width'=>'250px',
          'value'=>'marca',
          'filterType'=>GridView::FILTER_SELECT2,
          'filter'=>ArrayHelper::map(Marcas::find()->orderBy('nombre')->asArray()->all(), 'id_marca', 'nombre'),
          'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
          ],
          'filterInputOptions'=>['placeholder'=>'Mrcas','id' => 'marca-select2-id'],
          //'group'=>true,  // enable grouping
        ],
        [
          'attribute'=>'id_modelo',
          'width'=>'250px',
          'value'=>'modelo',
          'filterType'=>GridView::FILTER_SELECT2,
          'filter'=>ArrayHelper::map(Modelos::find()->orderBy('nombre')->asArray()->all(), 'id_modelo', 'nombre'),
          'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
          ],
          'filterInputOptions'=>['placeholder'=>'Modelos','id' => 'modelo-select2-id'],
          //'group'=>true,  // enable grouping
        ],
        [
          'attribute'=>'version',
          'width'=>'350px',
          'value'=>'version',
        //  'filterType'=>GridView::FILTER_SELECT2,
        //  'filter'=>ArrayHelper::map(Version::find()->orderBy('nombre')->asArray()->all(), 'id_version', 'nombre'),
          //'filterWidgetOptions'=>[
            //'pluginOptions'=>['allowClear'=>true],
          //],
        ],
        'idVersion.anio',
        'id_automovil',

        [
          'attribute'=>'placa',
          'width'=>'150px',
          'value'=>'placa',
        ],
        'serial_motor:ntext',
        [
          'class'=>'kartik\grid\BooleanColumn',
          'attribute'=>'estatus',
          'vAlign'=>'middle',
        ],
        [
          'class' => 'yii\grid\ActionColumn',
          'template' => '{view} {update}',
          'buttons' => [
              'update' => function ($url, $model, $key) {
                  return Html::a('<i class="fa fa-pencil fa-lg" aria-hidden="true"></i>', '#', [
                      'id' => 'activity-index-link',
                      'title' => Yii::t('app', 'Actualizar Vehiculo'),'rel'=>'tooltip',
                      'data-toggle' => 'modal',
                      'data-target' => '#modal',
                      'data-url' => Url::to(['update', 'id' => $model->id_automovil]),
                      'data-pjax' => '0',
                  ]);
              },
			  'view' => function ($url,$model, $key) {
                  return Html::a('<i class="fa fa-binoculars fa-lg" aria-hidden="true"></i>', ['view','id' => $model->id_automovil], [
                      'id' => 'activity-index-link',
                      'title' => Yii::t('app', 'Ver Vehiculo'),'rel'=>'tooltip',
                      'data-pjax' => '0',
                  ]);
               },
          ]
        ]
      ];

      echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
        'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
        'toolbar' => [
          [
            'content'=>
                  Html::a('<i class="glyphicon glyphicon-plus"></i> Registrar', '#', [
                'id' => 'activity-index-link',
                'title'=>Yii::t('app', 'Registrar Vehiculo'),'rel'=>'tooltip',
                'class' => 'btn btn-success',
                'data-toggle' => 'modal',
                'data-target' => '#modal',
                'data-url' => Url::to(['create']),
                'data-pjax' => '0',
                ]). ' '.
              Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
                'class' => 'btn btn-default',
                'title' => Yii::t('app', 'Actualizar Grid'),'rel'=>'tooltip',
              ]),

          ],
          '{export}',
          '{toggleData}'
        ],

        'pjax' => true,
        'pjaxSettings' => [
          'options' => ['id' => 'pajax-automovil'],// UNIQUE PJAX CONTAINER ID
        ],
        'bordered' => false,
        'resizableColumns'=>true,
        'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
        'striped' => true,
        'condensed' => false,
        'responsive' => true,
        'hover' => true,
        //'floatHeader' => true,
        //'floatHeaderOptions' => ['scrollingTop' => '50'],
        'showPageSummary' => false,
          'panel' => [
            'heading'=>'<h2 class="panel-title">'.Icon::show('car', ['class' => 'fa-2x']).'&nbsp; &nbsp;VEHICULOS</h2>',
            'type' => GridView::TYPE_INFO
          ],
        ]);
      ?>



</div>
<?php
$string = <<<EXP2
$("[rel=tooltip]").tooltip({ placement: 'top'});
EXP2;
$this->registerJs($string, \yii\web\View::POS_READY);
?>
<?php
$this->registerJs(
"$(document).on('click', '#activity-index-link', (function() {
    $.get(
        $(this).data('url'),
        function (data) {
            $('.modal-body').html(data);
            $('#modal').modal();
        }
    );
}));"
); ?>

<?php
Modal::begin([
    'options' => [
        'id' => 'modal',
        'tabindex' => false // important for Select2 to work properly
    ],
]);

Modal::end();
?>

<?php
//https://github.com/kartik-v/yii2-widgets/issues/202
//https://github.com/kartik-v/yii2-widget-select2/issues/19#issuecomment-81402573
$stringJS = <<<EXP2

/*  $("#id_contenedor_auto").on("pjax:complete", function() {
  var el = $("#automovil-id_marca"),
      options = el.data('pluginOptions'); // select2 plugin settings saved
   jQuery.when(el.select2(window[options])).done(initSelect2Loading('automovil-id_marca'));
 });*/

EXP2;
$this->registerJs($stringJS);

$stringCSS='
.fa{margin:0px 4px 0px 0px;}
 .fa-trash{
     color:  #d9534f;
 }
 .fa-plus{
     color:  #5cb85c;
 }
 .fa-retweet{
     color:  #5bc0de;
 }
 .fa-binoculars{
     color:  #39CCCC;
 }';
$this->registerCss($stringCSS);
?>
