<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use frontend\models\Marcas;
use frontend\models\Modelos;
use frontend\models\Version;
use frontend\models\Anio;
use frontend\models\Uso;
use frontend\models\Tipo_vehiculo;
use kartik\select2\Select2;
//use kartik\depdrop\Depdrop; //http://demos.krajee.com/widget-details/depdrop
use kartik\widgets\DepDrop;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use kartik\widgets\FileInput;
use yii\widgets\Pjax;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model frontend\models\Automovil */
/* @var $form yii\widgets\ActiveForm


*/
?>

<div class="automovil-form">

    <?php $form = ActiveForm::begin([
      'id' => 'automovil-form',
	    'options' => ["enctype" => "multipart/form-data"],
      'enableAjaxValidation' => true,
      'enableClientScript' => true,
      'enableClientValidation' => true,
      ]);
   ?>


	<div class="row">

		<div class="col-sm-6">
      <?=$form->field($model,"id_marca")->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Marcas::find()->all(), 'id_marca', 'nombre'),
                'language' => 'en',
                'options' => ['placeholder' => 'Select ...'],
                'pluginOptions' => [
                'allowClear' => true
                ],
            ]);
     ?>
		 </div>

		 <div class="col-sm-6">
            <?

            if (!$model->isNewRecord) {
                  echo Html::hiddenInput('modelo', $model->id_modelo, ['id'=>'modelo']);
            }

            echo $form->field($model, "id_modelo")->widget(DepDrop::classname(), [
          					'data'=> [''=>'Seleccione'],
          					'options'=>['placeholder'=>'Seleccione ...'],
          					'type' => DepDrop::TYPE_SELECT2,
          					'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
          					'pluginOptions'=>[
          					'depends'=>["automovil-id_marca"],
                    'initialize'=>true,
          					'placeholder' => 'Seleccione ...',
          					'url'=>Url::to(['dependent-dropdown/child-account', 'db'=>Modelos::classname(),'cmpo_dep'=>'id_marca','cmpo_mostrar'=>'nombre','id_cmpo'=>'id_modelo']),
          					'loadingText' => 'Leyendo ...',
                    'params'=>['modelo'] ///SPECIFYING THE PARAM
          					]
          				]);

			?>
		 </div>
    </div>

	<div class="row">

		<div class="col-sm-6">
             <?=$form->field($model, "id_anio")->widget(DepDrop::classname(), [
        					'data'=> [''=>'Seleccione '],
        					'options'=>['placeholder'=>'Selecione ...'],
        					'type' => DepDrop::TYPE_SELECT2,
        					'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
        					'pluginOptions'=>[
        					'depends'=>["automovil-id_modelo"],
        					'placeholder' => 'Seleccione ...',
        					'url'=>Url::to(['dependent-dropdown/anio','db'=>Modelos::classname()]),
        					'loadingText' => 'Leyendo ...',
        					]
        				]);

			?>
		 </div>
		 <div class="col-sm-6">
            <?=$form->field($model, "id_version")->widget(DepDrop::classname(), [
            					'data'=> [''=>'Seleccione '],
            					'options'=>['placeholder'=>'Selecione ...'],
            					'type' => DepDrop::TYPE_SELECT2,
            					'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
            					'pluginOptions'=>[
            					'depends'=>["automovil-id_anio"],
            					'placeholder' => 'Seleccione ...',
            					'url'=>Url::to(['dependent-dropdown/version','db'=>$model::classname()]),
            					'loadingText' => 'Leyendo ...',
            					]
            				]);

			?>

		 </div>
	</div>

	<div class="row">

		 <div class="col-sm-6">
			 <?= $form->field($model, 'placa')->textInput(['maxlength' => true]) ?>
		 </div>

		 <div class="col-sm-6">
			<?= $form->field($model, 'color')->textInput(['maxlength' => true]) ?>
		 </div>
	</div>

	<div class="row">

		 <div class="col-sm-6">
		     <?= $form->field($model, 'serial_motor')->textarea(['rows' => 1]) ?>
		 </div>

		 <div class="col-sm-6">
		    <?= $form->field($model, 'serial_carroceria')->textInput(['maxlength' => true]) ?>
		 </div>
    </div>

	<div class="row">

		<div class="col-sm-6">
             <?php
				      echo $form->field($model,"id_tipo_vehiculo")->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Tipo_vehiculo::find()->all(), 'id_tipo_vehiculo', 'nombre'),
                'language' => 'en',
                'options' => ['placeholder' => 'Seleccione ...'],
                'pluginOptions' => [
                'allowClear' => true
                ],
                 ]);

             ?>
		 </div>
		 <div class="col-sm-6">
             <?php
				echo $form->field($model,"id_uso")->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Uso::find()->all(), 'id_uso', 'nombre'),
                'language' => 'en',
                'options' => ['placeholder' => 'Seleccione ...'],
                'pluginOptions' => [
                'allowClear' => true
                ],
                 ]);

             ?>
		 </div>
    </div>
    <div class="row">
     <!--http://plugins.krajee.com/file-input#events-->
      <div class="col-sm-12">
       <?=$form->field($documentos, 'doc_files[]')->widget(FileInput::classname(),[
                                       'options'=>[
                                           'multiple'=>true,
                                           'accept'=>'*',
                                       ],
                                       'pluginOptions' => [
                                                       'uploadAsync'=>false,
                                                       'showUpload' => false,
                                                       'dropZoneEnabled'=>false,
                                                        'uploadUrl' => Url::to(['automovil/upload-file']),
                                                        'allowedFileExtensions'=>['pdf','jpg'],
                                                        'uploadExtraData' => new JsExpression('function() {
                                                         var obj = {};
                                                          $("form#automovil-form").find("input").each(function() {
                                                             var id = $(this).attr("name"), val = $(this).val();
                                                             obj[id] = val;
                                                         });
                                                         return obj;
                                                       }'),
                                                       /*'initialPreview'=>[
                                                                   "http://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/FullMoon2010.jpg/631px-FullMoon2010.jpg",
                                                                   "http://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Earth_Eastern_Hemisphere.jpg/600px-Earth_Eastern_Hemisphere.jpg"
                                                        ],
                                                         'initialPreviewAsData'=>true,
                                                         'initialCaption'=>"The Moon and the Earth",
                                                         'initialPreviewConfig' => [
                                                                   ['caption' => 'Moon.jpg', 'size' => '873727'],
                                                                   ['caption' => 'Earth.jpg', 'size' => '1287883'],
                                                               ],
                                                       'overwriteInitial'=>false,*/
                                                   ]
                                  
                                    ])->label(false);
     ?>


      </div>

    </div>
    <div class="row">
      <div class="col-sm-12" style="color:red;">Debe presionar el boton <i class="glyphicon glyphicon-upload text-info"></i> por cada imagen que este en la bandeja de imagenes,una vez que la imagen indique <i class="glyphicon glyphicon-ok-sign text-success"></i> ya esta preparada para guardar</div></br>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$string = <<<EXP2
//USAGE: $("#form").serializefiles();
  (function($) {
  $.fn.serializefiles = function() {
      var obj = $(this);
      /* ADD FILE TO PARAM AJAX */
      var formData = new FormData();
      $.each($(obj).find("#documentos-doc_files"), function(i, tag) {
          $.each($(tag)[0].files, function(i, file) {
              formData.append(tag.name, file);
          });
      });
      var params = $(obj).serializeArray();
      $.each(params, function (i, val) {
          formData.append(val.name, val.value);
      });
      return formData;
  };
  })(jQuery);

  $.fn.modal.Constructor.prototype.enforceFocus = function() {};


EXP2;
$this->registerJs($string);

?>

<?php

//https://github.com/kartik-v/yii2-widget-select2/issues/41 no se puede tipear en el select
//https://github.com/select2/select2/issues/1436



$stringJs = <<<EXP2
// obtener la id del formulario y establecer el manejador de eventos

     $("button.kv-file-upload").hide();

    $("form#automovil-form").on("beforeSubmit", function(e) {
        var form = $(this);
        var params="";
        var out="";
        $("#documentos-doc_files").fileinput("upload");

        if(form.attr("action")=="/sapis4u/admin/automovil/create"){
            params="?submit=true";
        }else{
            params="&submit=true";
        }
        $.post(
            form.attr("action")+params,form.serialize()
        )
        .done(function(result) {
  
    			if(result.message=='Exito'){

    			      $('#modal').modal('hide');
    				  $.pjax.reload({container:"#pajax-automovil"});
    				  $("#datosautomovil").empty();
    				 var container=$("#datosautomovil");
    				if ( typeof container !== "undefined" && container) {//solo si esta definido

                 out+='<div class="datagrid"><table class="table table-condensed"><thead><tr><th>Placa</th><th>Modelo</th><th>Version</th><th>Año</th><th>Uso</th><th>Tipo Vehiculo</th></tr></thead>';
                 out+='<tbody><tr><td>'+result.placa+'</td><td>'+result.modelo+'</td><td>'+result.version+'</td><td>'+result.anio+'</td><td>'+result.uso+'</td><td>'+result.tipo_vehiculo+'</td></tr></tbody></table></div>';
    				  $("#datosautomovil").html(out);
    				  $("#poliza-id_automovil_hidden").val(result.id_automovil);
    				}
    			}

        });
        return false;
    }).on("submit", function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        return false;
    });


EXP2;
$this->registerJs($stringJs);
$stringCss=".datagrid table { border-collapse: collapse; text-align: left; width: 100%; } .datagrid {font: normal 12px/150% Arial, Helvetica, sans-serif; background: #fff; overflow: hidden; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; }.datagrid table td, .datagrid table th { padding: 3px 10px; }.datagrid table thead th {background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #B2B9BF), color-stop(1, #B2B8BF) );background:-moz-linear-gradient( center top, #B2B9BF 5%, #B2B8BF 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#B2B9BF', endColorstr='#B2B8BF');background-color:#B2B9BF; color:#FFFFFF; font-size: 13px; font-weight: bold; border-left: 0px solid #EEF6FF; } .datagrid table thead th:first-child { border: none; }.datagrid table tbody td { color: #8D9296; border-left: 1px solid #EEF6FF;font-size: 12px;font-weight: bold; }.datagrid table tbody .alt td { background: #CCD3DB; color: #5F6366; }.datagrid table tbody td:first-child { border-left: none; }.datagrid table tbody tr:last-child td { border-bottom: none; }";
$this->registerCss($stringCss);
?>
