<?php

use yii\helpers\Html;
use kartik\icons\Icon;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model frontend\models\Automovil */

$this->title = "";

?>
<div class="bg-aqua top-modulo">
      <span class="icon-modulo"><?=Icon::show('car', ['class' => 'fa-3x'])?></span>
      <span style="color:white;font-size:18px;font-weight:bold;"><?=Yii::t('app', 'Crear Automovil');?></span>
</div>
<br/>
<div class="automovil-create">

    <?= $this->render('_form', [
        'model' => $model,
        'documentos'=>$documentos,
    ]) ?>

</div>
