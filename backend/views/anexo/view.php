<?php

use yii\helpers\Html;
//use yii\widgets\DetailView;
use kartik\detail\DetailView;
use backend\models\Modelos;
use backend\models\Poliza;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Anexo */

$this->title ='';// $model->id_anexo;
?>
<div id="bc1" class="btn-group btn-breadcrumb" style="float:right;">
            <a href="#" class="btn btn-default"><i class="fa fa-home"></i></a>
            <a href="<?=Url::to(['index'], true)?>" class="btn btn-default"><div><?=Yii::t('app', 'Anexos')?></div></a>
            <a href="#" class="btn btn-default"><div>Detalles del Anexo</div></a>
</div>
</br></br>
<div class="anexo-view">

    	<?php

	$attributes = [

    [
        'columns' => [
		    [
                'attribute'=>'id_anexo',
				'label'=>'Código',
                'format'=>'raw',
                'value'=>'<kbd>'.$model->id_anexo.'</kbd>',
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ],
            [
                'attribute'=>'id_marca',
				'label'=>'Recibo',
                'format'=>'raw',
                'value'=> $model->idRecibo->nro_recibo,
                'valueColOptions'=>['style'=>'width:30%'],
                'displayOnly'=>true
            ],



        ],
    ],
	[
        'columns' => [

			[
                'attribute'=>'titulo',
                'label'=>'Titulo',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],
            [
                'attribute'=>'descripcion',
                'label'=>'Descripcion',
                'displayOnly'=>true,
                'valueColOptions'=>['style'=>'width:30%']
            ],

        ],
    ],
	];
//print_r($model);
	// View file rendering the widget

	echo DetailView::widget([
		'model' => $model,
		'attributes' => $attributes,
		'mode' => 'view',
		'enableEditMode'=> false,
		'responsive' => 'responsive',
		'deleteOptions'=>[ // your ajax delete parameters
			'params' => ['id' => 1000, 'kvdelete'=>true],
		],
		'panel'=>[
        'heading'=>'Anexo',
        'type'=>DetailView::TYPE_INFO,

        ],
		'container' => ['id'=>'kv-demo'],
		//'formOptions' => ['action' => Url::current(['update' => 'kv-demo'])] // your action to delete
	]);


    ?>

</div>
