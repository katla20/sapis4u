<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\TipoReclamos;
use kartik\file\FileInput;
use kartik\select2\Select2;
use kartik\widgets\DepDrop;
use backend\models\Poliza;
use backend\models\Recibo;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use backend\models\Cliente;
use backend\models\Persona;
use kartik\checkbox\CheckboxX;
/* @var $this yii\web\View */
/* @var $model backend\models\Anexo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="anexo-form">

    <?php $form = ActiveForm::begin(); ?>


	<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
    <div class="box-header with-border">
         <h4 class="box-title"><!--<i class="glyphicon glyphicon-user"></i>--> Registro de Anexo</h4>
    </div>

		<div class="box-body">
			<div class="container-items"><!-- widgetBody -->
				<div class="row">
            <div class="col-sm-3">
                <?php

							  echo $form->field($model,"id_poliza")->widget(Select2::classname(), [
                             'data' => ArrayHelper::map(Poliza::find()->innerJoin('cliente', 'cliente.id_cliente = poliza.id_contratante')
													                                         ->innerJoin('persona', 'persona.id_persona = cliente.id_persona')
                                                                     ->where("poliza.estatus = :status AND poliza.id_ramo= 1", [':status' => 1])
                                                                     ->select("(numero_poliza||'--'||identificacion) AS numero_poliza ,id_poliza")
                                                                     ->all(), 'id_poliza', 'numero_poliza'),
                            'language' => 'en',
                            'options' => ['placeholder' => 'Select a Poliza'],
                            'pluginOptions' => [
                            'allowClear' => true
                            ],
                    ]);
                ?>
            </div>
            <div class="col-sm-3">

						<?php
                 if (!$model->isNewRecord) {
                      echo Html::hiddenInput('recibo', $model->id_recibo, ['id'=>'recibo']);
                 }

                 echo $form->field($model, "id_recibo")->widget(DepDrop::classname(), [
                							'data'=> [0=>'Seleccione'],
                							'options'=>['placeholder'=>'Selecione ...'],
                							'type' => DepDrop::TYPE_SELECT2,
                							'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                							'pluginOptions'=>[
                							'depends'=>["anexo-id_poliza"],
                              'initialize'=>true,
                							'placeholder' => 'Seleccione ...',
                							'url'=>Url::to(['dependent-dropdown/child-account', 'db'=>Recibo::classname(),'cmpo_dep'=>'id_poliza','cmpo_mostrar'=>'nro_recibo','id_cmpo'=>'id_recibo']),
                							'loadingText' => 'Leyendo ...',
                              'params'=>['recibo'] ///SPECIFYING THE PARAM
                							]
							]);

						?>
                </div>

				</div>
                <div class="row">
                    <div class="col-sm-6">
                         <?= $form->field($model, 'titulo')->textInput(['maxlength' => true]) ?>

    					</div>
    			    </div>
				<div class="row">

					<div class="col-sm-6">
						<?= $form->field($model, 'descripcion')->textarea(['rows' => 3]) ?>
					</div>
				</div>


            <div class="row">

                <div class="col-sm-3">
                    <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>

			</div>
		</div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
