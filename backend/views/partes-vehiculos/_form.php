<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\PartesVehiculos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partes-vehiculos-form">

    <?php $form = ActiveForm::begin(); ?>
	<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
    <div class="box-header with-border">
         <h4 class="box-title"><!--<i class="glyphicon glyphicon-user"></i>--> Registro de Parte de vehiculo</h4>
    </div>

		<div class="box-body">
			<div class="container-items"><!-- widgetBody -->
				<div class="row">
					<div class="col-sm-4">
						<?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-2">
						<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
					</div>
				</div>

			</div>
		</div>
    </div>



    <?//= $form->field($model, 'fecha_registro')->textInput() ?>

    <?//= $form->field($model, 'fecha_actualizacion')->textInput() ?>

    <?//= $form->field($model, 'id_user_registro')->textInput() ?>

    <?//= $form->field($model, 'id_user_actualizacion')->textInput() ?>




    <?php ActiveForm::end(); ?>

</div>
