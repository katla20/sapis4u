<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Convenio */

$this->title = '';//Yii::t('app', 'Create Convenio');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Convenios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="convenio-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelDetalle' => (empty($modelDetalle)) ? [new ConvenioDetalle] : $modelDetalle,
    ]) ?>

</div>
