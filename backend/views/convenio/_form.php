<?php

use yii\helpers\Html;
//use yii\bootstrap\ActiveForm;
//use yii\bootstrap\ActiveField;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use backend\models\Aseguradoras;
use backend\models\Ramos;
use backend\models\Productos;
use kartik\select2\Select2;
use kartik\widgets\DepDrop;
//use kartik\depdrop\Depdrop; //http://demos.krajee.com/widget-details/depdrop
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;
use kartik\widgets\DatePicker;
use kartik\money\MaskMoney;

/* @var $this yii\web\View */
/* @var $model backend\models\Convenio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="convenio-form">

    <?php //$form = ActiveForm::begin(); ?>
	  <?php $form = ActiveForm::begin([
    'id' => 'dynamic-form',
    ]); ?>
	
	
<div class="box box-solid box-default" style="-webkit-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    -moz-box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);
    box-shadow: 0px 2px 8px -1px rgba(143,141,143,1);">
		<div class="box-header with-border">
			 <h4 class="box-title"><!--<i class="glyphicon glyphicon-user"></i>--> Registra Convenio o Concurso</h4>
		</div>
		<div class="box-body">
		
		<div class="box box-default">
		  <div class="box-header with-border">
			<h4 class="box-title"><i class="glyphicon glyphicon-user"></i> Datos del Convenio o Concurso</h4>
		 
			<div class="box-tools pull-right">
			  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			</div><!-- /.box-tools -->
		  </div><!-- /.box-header -->
		  <div class="box-body">
			 <div class="container-items"><!-- widgetBody -->
			 
				<div class="row">
							  <div class="col-sm-3">
								   <?php
								//echo $form->field($model, 'estatus')->dropDownList([1 => 'Activo', 0 => 'Inactivo']);
								echo $form->field($model, 'tipo')->widget(Select2::classname(), [
										'data' => array("1"=>"Convenio","2"=>"Concurso"),
										'language' => 'es',
										//'options' => ['placeholder' => 'Seleccione un Esatdo ...'],
										'pluginOptions' => [
										'allowClear' => true
										],
								]);	
								?>
							  </div>
							  <div class="col-sm-3">
							  <?=$form->field($model,"id_aseguradora",[
																  'addon' => [
																	  'prepend' => [
																		  'content' => '<i class="fa fa-university"></i>'

																	  ]
																  ]
															  ])->widget(Select2::classname(), [
										 'data' =>   ArrayHelper::map(Aseguradoras::find()->all(),'id_aseguradora', 'nombre'),
										 'language' => 'en',
										  'options' => ['placeholder' => 'Seleccione ...'],
										  'pluginOptions' => [
											 'allowClear' => true
										 ],
									   ]);
									 ?>
							  </div>
				</div>
				
				<div class="row">
							  
							  <div class="col-sm-3">
							  <?=$form->field($model, "fecha_desde",[
															'addon' => [
																'prepend' => [
																	'content' => '<i class="glyphicon glyphicon-calendar"></i>'

																]
															]
														])->widget(MaskedInput::className(), [
							  'clientOptions' => ['alias' =>  'date']
							   ]) ?>
							  </div>
							  <div class="col-sm-3">
								<?=$form->field($model, "fecha_hasta",[
															'addon' => [
																'prepend' => [
																	'content' => '<i class="glyphicon glyphicon-calendar"></i>'

																]
															]
														])->widget(MaskedInput::className(), [
							  'clientOptions' => ['alias' =>  'date']
							   ]) ?> 
							  </div>
				</div>

				
				<div class="row">
							  <div class="col-sm-3">
								<?php  echo $form->field($model, 'monto',[
																  'addon' => [
																	  'prepend' => [
																		  'content' => 'Bs.',

																	  ]
																  ]
															  ])->widget(MaskMoney::classname(), [
																								  'pluginOptions' => [
																									  'prefix' => '',
																									  'suffix' => '',
																									  'allowNegative' => false
																									]
																								 ]);?>
								   <?//= $form->field($model, 'monto')->textInput() ?>
							  </div>
							  <div class="col-sm-3">
								  
				
								
							  </div>
				</div>
				  <div class="row">
							  <div class="col-sm-6">
								
							   </div>
				  </div><!--container-items-->

		  </div><!-- /.box-body -->
		</div><!-- /.box -->

			<?php DynamicFormWidget::begin([
			   'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
			   'widgetBody' => '.container-items', // required: css class selector
			   'widgetItem' => '.item', // required: css class
			   'limit' => 4, // the maximum times, an element can be added (default 999)
			   'min' => 0, // 0 or 1 (default 1)
			   'insertButton' => '.add-item', // css class
			   'deleteButton' => '.remove-item', // css class
			   'model' => $modelDetalle[0],
			   'formId' => 'dynamic-form',
			   'formFields' => [
				   'id_ramo',
				   'id_producto',
				   'cantidad'
			   ],
			 ]);?>
                
				 <div class="box box-default">
				 <div class="box-header with-border">
					 <h4>
						 <i class="glyphicon glyphicon-envelope"></i> Detalle
						 <button type="button" class="add-item btn btn-success btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i>Agregar</button>
					 </h4>

				 </div>
				 <div class="box-body">
					 <div class="container-items"><!-- widgetBody -->
					 <?php foreach ($modelDetalle as $i => $modelDetalle): ?>
						 <div class="item panel panel-default"><!-- widgetItem -->
							 <div class="panel-heading">
								 <h5 class="panel-title pull-left">Detalle</h5>
								 <div class="pull-right">
									 <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
								 </div>
								 <div class="clearfix"></div>
							 </div>
							 <div class="panel-body">
								 <?php
									 // necessary for update action.
									 if (! $modelDetalle->isNewRecord) {
									   //http://stackoverflow.com/questions/28925624/yii2kartik-depdrop-widget-default-value-on-update
										 ///echo Html::activeHiddenInput($modelDetalle, "[{$i}]id");

										// echo CHtml::hiddenField('name' , 'value', array('id' => 'hiddenInput'));[$i]
										 //ADDITIONAL PARAM ID YOU MAY USE TO SELECT A DEFAULT VALUE OF YOUR MODEL IN YOUR DEPDROP WHEN YOU WANT TO UPDATE:
										  //echo Html::hiddenInput('model_idEstado', $modelDetalle->id, ['id'=>'model_idEstado']);
										  // echo Html::hiddenInput('model_idEstado',"[$i]id", ['id'=>'model_idEstado']);
									 }
								 ?>
							  <div class="row">

								  <div class="col-sm-3">
								  <?//=$form->field($model, 'id_ase')->hiddenInput(['value'=> ''])->label(false); 
								  echo Html::hiddenInput('id_ase',"[$i]id_ase", ['id'=>'id_ase']);?>
								  <?=$form->field($modelDetalle, "[{$i}]id_ramo",[
													  'addon' => [
														  'prepend' => [
															  'content' => '<i class="fa fa-map-marker"></i>'

														  ]
													  ]
												  ])->widget(DepDrop::classname(), [
												'data'=> [''=>'Seleccione '],
												'options'=>['placeholder'=>'Selecione ...'],
												'type' => DepDrop::TYPE_SELECT2,
												'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
												'pluginOptions'=>[
												 'initialize'=>true,
												 'depends'=>["convenio-id_aseguradora"],
												 'placeholder' => 'Seleccione ...',
												 'url'=>Url::to(['dependent-dropdown/child-account-ramo2', 'db'=>Ramos::classname(),'cmpo_dep'=>'id_aseguradora','cmpo_mostrar'=>'nombre','id_cmpo'=>'id_ramo']),
												 'loadingText' => 'Leyendo ...',
											   ]
											   ]);

										 ?>
								 <?/*=$form->field($modelDetalle,"[{$i}]id_ramo",[
																  'addon' => [
																	  'prepend' => [
																		  'content' => '<i class="fa fa-university"></i>'

																	  ]
																  ]
															  ])->widget(Select2::classname(), [
										 'data' =>   ArrayHelper::map(Ramos::find()->all(),'id_ramo', 'nombre'),
										 'language' => 'en',
										  'options' => ['placeholder' => 'Seleccione ...'],
										  'pluginOptions' => [
											 'allowClear' => true
										 ],
									   ]);*/
									 ?>	
									 </div>			

									 <div class="col-sm-3">
										<?=$form->field($modelDetalle, "[{$i}]id_producto",[
													  'addon' => [
														  'prepend' => [
															  'content' => '<i class="fa fa-map-marker"></i>'

														  ]
													  ]
												  ])->widget(DepDrop::classname(), [
												'data'=> [''=>'Seleccione '],
												'options'=>['placeholder'=>'Selecione ...'],
												'type' => DepDrop::TYPE_SELECT2,
												'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
												'pluginOptions'=>[
												 'depends'=>["conveniodetalle-$i-id_ramo"],
												 'placeholder' => 'Seleccione ...',
												 'url'=>Url::to(['dependent-dropdown/child-account', 'db'=>Productos::classname(),'cmpo_dep'=>'id_ramo','cmpo_mostrar'=>'nombre','id_cmpo'=>'id_producto']),
												 'loadingText' => 'Leyendo ...',
											   ]
											   ]);

										 ?>
									 </div>
		

									 <div class="col-sm-3">

									 <?= $form->field($modelDetalle, "[{$i}]cantidad",[
																		 'addon' => [
																			 'prepend' => [
																				 'content' => '<i class="fa fa-phone"></i>'

																			 ]
																		 ]
																	 ])->textInput(['maxlength' => true]) ?>
									 </div>

								</div>
								 
							
			  
							 </div>
						 </div>
					   <?php endforeach; ?>
				   </div><!--container-items-->
				   </div><!--panel body-->
			 </div><!-- .panel -->
			
			   <div class="form-group">
				<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			   </div>
			   </div><!--panel body-->
		 </div><!-- .panel -->
		 <?php DynamicFormWidget::end(); ?>
		 <?php ActiveForm::end(); ?>

		</div>
	</div>	
</div>


<?php
$script=<<< JS

$(".dynamicform_wrapper").on('afterInsert', function(e, item){
    var datePickers = $(this).find('[data-krajee-kvdatepicker]');
	   datePickers.each(function(index, el){
  	   $(this).parent().removeData().kvDatepicker('remove');
  	   $(this).parent().kvDatepicker(eval($(this).attr('data-krajee-kvdatepicker')));
	   });
});


$(".dynamicform_wrapper").on("beforeInsert", function(e, item) {
		console.log("beforeInsert");
});


$(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
		if (! confirm("Are you sure you want to delete this item?")) {
			return false;
    }
		return true;
});

$(".dynamicform_wrapper").on("afterDelete", function(e) {
		console.log("Deleted item!");
});

$(".dynamicform_wrapper").on("limitReached", function(e, item) {
		    alert("Limit reached");
});

$("select[name='Convenio[id_aseguradora]']").change(function(){

      $("input[name='id_ase").val($(this).val());
	  
});
JS;
$this->registerJs($script, \yii\web\View::POS_READY);
//https://github.com/wbraganca/yii2-dynamicform/issues/41
$string=".identificacion{display:none}";
$this->registerCss($string);
?>
