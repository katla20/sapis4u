<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Convenio */

$this->title ='';/* Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Convenio',
]) . $model->id_convenio;*/
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Convenios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_convenio, 'url' => ['view', 'id' => $model->id_convenio]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="convenio-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelDetalle' => (empty($modelDetalle)) ? [new ConvenioDetalle] : $modelDetalle,
    ]) ?>

</div>
